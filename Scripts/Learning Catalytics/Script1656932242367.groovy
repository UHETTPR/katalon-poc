import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Dimension as Dimension
import org.openqa.selenium.Point as Point
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions as ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType

Date todaysDate = new Date()

def gScreenFormattedDate = todaysDate.format('MM/dd/yyyy')

def gPreviousDate = todaysDate.plus(-2).format('MM/dd/yyyy')

//System.out.println(gPreviousDate)
//Session initiation and Instructor Login
WebDriver InstructorSession = openChromeBrowserPlain()

DriverFactory.changeWebDriver(InstructorSession)

WebUI.navigateToUrl(GlobalVariable.mel_URL)

WebUI.waitForPageLoad(0)

if (GlobalVariable.executionProfile == 'DEV') {
    WebUI.setText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_User name_UserName'), 'xl')

    WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_Password'), 
        'yBw77zEB3K3+kSSsja7LDA==')

    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_submitButton'))
}

WebUI.setText(findTestObject('Object Repository/LC/Page_Pearson Sign In/input_Username_username'), GlobalVariable.mel_Instructor)

WebUI.setEncryptedText(findTestObject('Object Repository/LC/Page_Pearson Sign In/input_Password_password'), GlobalVariable.Password)

WebUI.click(findTestObject('Object Repository/LC/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Launch - Hiranthi Insnew/a_Enter MyLab Math Global'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Instructor Home - Hiranthi Insnew/h1_Instructor Home'), 
    0)

//Session initiation and Student Login
WebDriver StudentSession = openChromeBrowserPlain()

DriverFactory.changeWebDriver(StudentSession)

WebUI.navigateToUrl(GlobalVariable.mel_URL)

WebUI.waitForPageLoad(0)

if (GlobalVariable.executionProfile == 'DEV') {
    WebUI.setText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_User name_UserName'), 'xl')

    WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_Password'), 
        'yBw77zEB3K3+kSSsja7LDA==')

    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_submitButton'))
}

WebUI.setText(findTestObject('Object Repository/LC/Page_Pearson Sign In/input_Username_username'), GlobalVariable.mel_Student)

WebUI.setEncryptedText(findTestObject('Object Repository/LC/Page_Pearson Sign In/input_Password_password'), GlobalVariable.Password)

WebUI.click(findTestObject('Object Repository/LC/Page_Pearson Sign In/button_Sign in'))

DriverFactory.changeWebDriver(InstructorSession)

//Create Econ Test Course
WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Course Manager'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Course Manager - Prabhashi ins1/a_Create or copy a course'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Sample1/Page_New Course/select_-- Choose --StandardCoordinatorMember'), 
    'Standard', true)

WebUI.setText(findTestObject('Object Repository/Sample1/Page_New Course/input_Course name_ctl00ctl00InsideFormMaste_92e586'), 
    GlobalVariable.mel_CourseName)

WebUI.selectOptionByValue(findTestObject('Object Repository/Sample1/Page_New Course/select_-- Choose Book --   Levine Statistic_0673c2'), 
    GlobalVariable.mel_BookMaterial, true)

WebUI.click(findTestObject('Object Repository/Instructor/CreateCourse/Page_New Course/a_Next'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCourse/Page_New Course/span_Standard'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCourse/Page_New Course/span_PortTestCourse'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCourse/Page_New Course/span_NextGen Media Math Test BookRH'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/CreateCourse/Page_New Course/input_RadDatePicker_ctl00ctl00InsideFormMas_0b85be'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Instructor/CreateCourse/Page_New Course/input_RadDatePicker_ctl00ctl00InsideFormMas_0b85be'), 
    gPreviousDate)

WebUI.click(findTestObject('Object Repository/Instructor/CreateCourse/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/img_Expand All_ctl00_ctl00_InsideForm_Maste_c03b4e'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_New Course/a_Save'))

WebUI.sleep(8000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/h2_Course Settings Summary'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/a_XL44-E1JP-40L9-74V2'), 
    0)

CourseID = WebUI.getText(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/a_XL44-E1JP-40L9-74V2'))

WebUI.click(findTestObject('Object Repository/Sample1/Page_Course Settings Summary - Prabhashi ins1/input_Browser Check_ctl00ctl00InsideFormMas_e7786d'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCourse/Page_Course Manager - Prabhashi ins1/h2_Course Manager'), 
    0)

GlobalVariable.CourseID = this.CourseID

DriverFactory.changeWebDriver(StudentSession)

WebUI.callTestCase(findTestCase('Student/StudentEnrollment'), [:], FailureHandling.STOP_ON_FAILURE)

//Assignment Creation
DriverFactory.changeWebDriver(InstructorSession)

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Instructor Home'))

String WindowTitle = WebUI.getWindowTitle()

System.out.println(WindowTitle)

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/h1_Instructor Home'))

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Learning Catalytics Real-time learning'))

WebUI.switchToWindowTitle('Enable Learning Catalytics - Prabhashi ins1')

WebUI.click(findTestObject('Object Repository/LC/Page_Enable Learning Catalytics - Prabhashi ins1/input_Review results in real time to identi_c6d241'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Welcome, Prabhashi'), 0)

WebUI.sleep(3000)

WebUI.switchToWindowTitle(WindowTitle)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics                _456321'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Create new module'))

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.setText(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Name_lecturename'), 'Testing LC Test 1')

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Team-Based Assessment_commit'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Add a question from the library'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/body_iframe srchttpswww.googletagmanager.co_6d9e45'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_multiple choice    Which of the following_1b5f00'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/span_Add to module'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Which of the following will happen if a_f2dc5b'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Team-Based Assessment_commit'))

WebUI.sleep(3000)

WebUI.click(findTestObject('LC/Page_Learning Catalytics/td_Sample'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Start session'))

WebUI.switchToWindowTitle('Learning Catalytics - student window')

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics - student window/div_Testing LC Test 1 (42405192)'), 
    0)

WebUI.switchToWindowTitle(WindowTitle)

WebUI.refresh()

while (!(WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics        End Sess_a744e4'), 
    0, FailureHandling.OPTIONAL))) {
    WebUI.refresh()

    WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics        End Sess_a744e4'), 
        0, FailureHandling.OPTIONAL)
}

//Assignment Taking
DriverFactory.changeWebDriver(StudentSession)

String StudentWindowTitle = WebUI.getWindowTitle()

WebUI.refresh()

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi Hettiarachchi/button_Join LC Session'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi Hettiarachchi/a_Do Homework'))

WebUI.verifyElementPresent(findTestObject('LC/Page_Homework and Tests - Prabhashi Hettiarachchi/th_Learning CatalyticsTesting LC Test 1 (42_c980e6'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Homework and Tests - Prabhashi Hettiarachchi/a_Course Home'))

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi Hettiarachchi/button_Join LC Session'))

WebUI.switchToWindowTitle('Wait for Next Question')

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Wait for Next Question/div_Log out_title'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Wait for Next Question/div_Session 42405192                       _83ed8b'), 
    0)

//Deliver Question
DriverFactory.changeWebDriver(InstructorSession)

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/button_End Session in Progress'))

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Deliver'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Round 1                           Revea_ba84a6'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_0 responses, 0 correct'), 0)

def answer = WebUI.getText(findTestObject('Object Repository/LC/Page_Learning Catalytics/b_C'))

WebUI.switchToWindowTitle('Learning Catalytics - student window')

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics - student window/div_Which of the following will happen if a_0e835d'), 
    0)

//Select incorrect answer
DriverFactory.changeWebDriver(StudentSession)

WebUI.switchToWindowTitle('Multiple Choice Question')

def incorrectAnswer

if (answer == 'A') {
    incorrectAnswer = 'B'
} else if (answer == 'B') {
    incorrectAnswer = 'A'
} else if (answer == 'C') {
    incorrectAnswer = 'A'
} else {
    incorrectAnswer = 'A'
}

System.out.println(incorrectAnswer)

WebUI.click(findTestObject('LC/Page_Multiple Choice Question/a_C. The gross domestic product of India wi_155103', [('incorrectAnswer') : incorrectAnswer]))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Multiple Choice Question/div_Your response has been hidden for priva_8cf62a'), 
    0)

//Stop Session
DriverFactory.changeWebDriver(InstructorSession)

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_1 response, 0 correct'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/span_Active now 1  Total joined 1'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Stop session'))

WebUI.acceptAlert()

DriverFactory.changeWebDriver(StudentSession)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Score Summary/div_Of the 1 question with a correct answer_987f6b'), 
    0)

//Close out LC windows
DriverFactory.changeWebDriver(InstructorSession)

WebUI.closeWindowTitle('Learning Catalytics')

WebUI.switchToWindowTitle(WindowTitle)

WebUI.refresh()

while (!(WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics                _456321'), 
    0, FailureHandling.OPTIONAL))) {
    WebUI.refresh()

    WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics                _456321'), 
        0, FailureHandling.OPTIONAL)
}

//Close out LC windows
DriverFactory.changeWebDriver(StudentSession)

WebUI.closeWindowTitle('Score Summary')

WebUI.switchToWindowTitle(StudentWindowTitle)

WebUI.refresh()

WebUI.verifyElementNotPresent(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi Hettiarachchi/button_Join LC Session'), 
    0)

//GB Integration
DriverFactory.changeWebDriver(InstructorSession)

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_Assignments'))

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_All Assignments'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/th_Testing LC Test 1 (963315'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/td_0'), 0)

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_Course Home'))

String WindowTitle2 = WebUI.getWindowTitle()

WebUI.sleep(6000)

WebUI.waitForElementClickable(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi ins1/img'), 0)

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi ins1/img'))

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Instructor-Led Synchronous_1803907'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Edit'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Add Questions and Customize Module'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Final score  100 Correctness  0 Participation'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Final score  100 Correctness  0 Participation'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.dragAndDropToObject(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Final score  100 Correctness  0 Participation'), 
    findTestObject('LC/Page_Learning Catalytics/div_Final score  0 Correctness  100 Participation'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Copy or move checked questions_commit'))

WebUI.switchToWindowTitle(WindowTitle2)

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_Assignments'))

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_All Assignments'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/th_Testing LC Test 1 (963315'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/td_100'), 0)

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_Course Home'))

WebUI.sleep(6000)

WebUI.waitForElementClickable(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi ins1/img'), 0)

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi ins1/img'))

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Instructor-Led Synchronous_1803907'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Edit'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/label_Gradebook transfer'), 0)

WebUI.uncheck(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Gradebook transfer_lecturegrade_transfer'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Copy or move checked questions_commit'))

WebUI.switchToWindowTitle(WindowTitle2)

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_Assignments'))

WebUI.click(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/a_All Assignments'))

WebUI.verifyElementNotPresent(findTestObject('Object Repository/LC/Page_Gradebook - Prabhashi ins1/th_Testing LC Test 1 (963315'), 
    0)

//Grade resend
WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Instructor Home'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics                _456321'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Create new module'))

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.setText(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Name_lecturename'), 'Testing LC Test 2')

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Team-Based Assessment_commit'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Add a question from the library'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/body_iframe srchttpswww.googletagmanager.co_6d9e45'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_multiple choice    Which of the following_1b5f00'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/span_Add to module'))

WebUI.uncheck(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Gradebook transfer_lecturegrade_transfer'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Which of the following will happen if a_f2dc5b'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/input_Team-Based Assessment_commit'))

WebUI.sleep(3000)

WebUI.click(findTestObject('LC/Page_Learning Catalytics/td_Sample2'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Start session'))

//Assignment Taking
DriverFactory.changeWebDriver(StudentSession)

WebUI.switchToWindowTitle(StudentWindowTitle)

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi Hettiarachchi/a_Do Homework'))

WebUI.refresh()

while (!(WebUI.verifyElementPresent(findTestObject('LC/Page_Homework and Tests - Prabhashi Hettiarachchi/th_Learning CatalyticsTesting LC Test 2 (42_c980e6'), 
    0, FailureHandling.OPTIONAL))) {
    WebUI.refresh()

    WebUI.verifyElementPresent(findTestObject('LC/Page_Homework and Tests - Prabhashi Hettiarachchi/th_Learning CatalyticsTesting LC Test 2 (42_c980e6'), 
        0, FailureHandling.OPTIONAL)
}

WebUI.click(findTestObject('Object Repository/LC/Page_Homework and Tests - Prabhashi Hettiarachchi/a_Course Home'))

WebUI.click(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi Hettiarachchi/button_Join LC Session'))

WebUI.switchToWindowTitle('Wait for Next Question')

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Wait for Next Question/div_Log out_title'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Wait for Next Question/div_Session 42405192                       _83ed8b'), 
    0)

//Deliver Question
DriverFactory.changeWebDriver(InstructorSession)

WebUI.switchToWindowTitle(WindowTitle)

WebUI.refresh()

while (!(WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics        End Sess_a744e4'), 
    0, FailureHandling.OPTIONAL))) {
    WebUI.refresh()

    WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics        End Sess_a744e4'), 
        0, FailureHandling.OPTIONAL)
}

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/button_End Session in Progress'))

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Deliver'))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_Round 1                           Revea_ba84a6'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_0 responses, 0 correct'), 0)

def correctAnswer = WebUI.getText(findTestObject('Object Repository/LC/Page_Learning Catalytics/b_C'))

System.out.println(correctAnswer)

WebUI.switchToWindowTitle('Learning Catalytics - student window')

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics - student window/div_Which of the following will happen if a_0e835d'), 
    0)

//Select Correct answer
DriverFactory.changeWebDriver(StudentSession)

WebUI.switchToWindowTitle('Multiple Choice Question')

WebUI.click(findTestObject('LC/Page_Multiple Choice Question/a_A. The gross domestic product of U.S. wil_7833ab', [('correctAnswer') : correctAnswer]))

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Multiple Choice Question/div_Your response has been hidden for priva_8cf62a'), 
    0)

//Stop Session
DriverFactory.changeWebDriver(InstructorSession)

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.verifyElementPresent(findTestObject('LC/Page_Learning Catalytics/div_1 response, 100 correct'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Learning Catalytics/span_Active now 1  Total joined 1'), 
    0)

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Stop session'))

WebUI.acceptAlert()

DriverFactory.changeWebDriver(StudentSession)

WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Score Summary/div_Of the 1 question with a correct answer_987f6b'), 
    0)

//Close out LC windows
DriverFactory.changeWebDriver(InstructorSession)

WebUI.closeWindowTitle('Learning Catalytics')

WebUI.switchToWindowTitle(WindowTitle)

WebUI.refresh()

while (!(WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics                _456321'), 
    0, FailureHandling.OPTIONAL))) {
    WebUI.refresh()

    WebUI.verifyElementPresent(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/section_Learning Catalytics                _456321'), 
        0, FailureHandling.OPTIONAL)
}

//Close out LC windows
DriverFactory.changeWebDriver(StudentSession)

WebUI.closeWindowTitle('Score Summary')

WebUI.switchToWindowTitle(StudentWindowTitle)

WebUI.refresh()

WebUI.verifyElementNotPresent(findTestObject('Object Repository/LC/Page_Course Home - Prabhashi Hettiarachchi/button_Join LC Session'), 
    0)

ChromeDriver openChromeBrowserPlain() {
    return openChromeBrowser(new ChromeOptions())
}

ChromeDriver openChromeBrowserInIncognitoMode() {
    ChromeOptions options = new ChromeOptions()

    options.addArguments('--incognito')

    return openChromeBrowser(options)
}

ChromeDriver openChromeBrowser(ChromeOptions options) {
    System.setProperty('webdriver.chrome.driver', DriverFactory.getChromeDriverPath())

    return new ChromeDriver(options)
}

