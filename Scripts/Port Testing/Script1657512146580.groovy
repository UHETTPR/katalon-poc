import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.chrome.ChromeOptions as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions as ChromeOptions

for (i = 3; i < 4; i++) {
    WebDriver instructorChrome = openChromeBrowserPlain()

    DriverFactory.changeWebDriver(instructorChrome)

    WebUI.navigateToUrl(findTestData('PortURLs').getValue(1, i))

    WebUI.waitForPageLoad(0)

    WebUI.setText(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Pearson Sign In/input_Username_username'), 
        GlobalVariable.mxl_Instructor)

    WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Pearson Sign In/input_Password_password'), 
        GlobalVariable.Password)

    WebUI.click(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Pearson Sign In/button_Sign in'))

    WebUI.click(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Launch - Hiranthi Insnew/a_Enter MyLab Math Global'))

    WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Instructor Home - Hiranthi Insnew/h1_Instructor Home'), 
        0)

    WebUI.waitForPageLoad(0)

    WebUI.callTestCase(findTestCase('Instructor/Instructor Page Navigation'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Instructor/Create Standard Course'), [('CourseID') : '', (GlobalVariable.mxl_CourseName) : 'PortTestCourse'], 
        FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Instructor/Navigate to Course'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Instructor/Create Homework Assignment'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Instructor/Create Test Assignment'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Instructor/Create Quiz Assignment'), [:], FailureHandling.STOP_ON_FAILURE)

    WebDriver studentChrome = openChromeBrowserPlain()

    DriverFactory.changeWebDriver(studentChrome)

    WebUI.navigateToUrl(findTestData('PortURLs').getValue(1, i))

    WebUI.setText(findTestObject('Object Repository/StudentEnrollment/Page_Pearson Sign In/input_Username_username'), GlobalVariable.mxl_Student)

    WebUI.setEncryptedText(findTestObject('Object Repository/StudentEnrollment/Page_Pearson Sign In/input_Password_password'), 
        GlobalVariable.Password)

    WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Pearson Sign In/button_Sign in'))

    WebUI.waitForPageLoad(0)

    WebUI.callTestCase(findTestCase('Student/StudentEnrollment'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Student/Student Page Navigation'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Student/Take Homework Assignment and Review'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Student/Take Test Assignment and Review'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Student/Take Quiz Assignment and Review'), [:], FailureHandling.STOP_ON_FAILURE)

    DriverFactory.changeWebDriver(instructorChrome)

    WebUI.callTestCase(findTestCase('Instructor/Validate Gradebook Results'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('Instructor/Delete Course'), [:], FailureHandling.STOP_ON_FAILURE)

	DriverFactory.changeWebDriver(instructorChrome)
	WebUI.closeBrowser()
	
	DriverFactory.changeWebDriver(studentChrome)
	WebUI.closeBrowser()
}

ChromeDriver openChromeBrowserPlain() {
    return openChromeBrowser(new ChromeOptions())
}

ChromeDriver openChromeBrowser(ChromeOptions options) {
    System.setProperty('webdriver.chrome.driver', DriverFactory.getChromeDriverPath())

    return new ChromeDriver(options)
}

