import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Instructor Home - Prabhashi ins1/a_Course Home'))

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Course Home - Prabhashi ins1/a_Entire Course To Date'))

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Course Home - Prabhashi ins1/a_Current'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Course Home - Prabhashi ins1/h3_Announcements (0)No announcements during_65337b'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Course Home - Prabhashi ins1/a_View Instructor Home'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Instructor Home - Prabhashi ins1/h1_Instructor Home'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Instructor Home - Prabhashi ins1/a_View Student Home'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Course Home - Prabhashi ins1/img__mainBandLogo'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Course Home - Prabhashi ins1/a_Study Plan'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Study Plan - Prabhashi ins1/h2_Study Plan'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Study Plan - Prabhashi ins1/a_Calendar'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Calendar - Prabhashi ins1/h2_Calendar'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Calendar - Prabhashi ins1/a_Homework and Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Homework and Tests - Prabhashi ins1/h2_Homework and Tests'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Homework and Tests - Prabhashi ins1/a_Results'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Results - Prabhashi ins1/h2_Results'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Instructor Home - Prabhashi ins1/a_Course Manager'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Course Manager - Prabhashi ins1/h2_Course Manager'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Course Manager - Prabhashi ins1/a_Home Page Manager'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Home Page Manager - Prabhashi ins1/h2_Home Page Manager'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Home Page Manager - Prabhashi ins1/a_Assignment Manager'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Assignment Manager - Prabhashi ins1/h2_Assignment Manager'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Assignment Manager - Prabhashi ins1/a_Study Plan Manager'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Study Plan Manager - Prabhashi ins1/h2_Study Plan Manager'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Study Plan Manager - Prabhashi ins1/a_Gradebook'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/PageNav/Page_Gradebook - Prabhashi ins1/h2_Gradebook'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/PageNav/Page_Gradebook - Prabhashi ins1/a_Instructor Home'))

