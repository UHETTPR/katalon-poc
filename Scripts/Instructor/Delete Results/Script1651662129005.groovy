import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.mxl_URL)

boolean IsDev = WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/h1_Dev Auth'), 
    0, FailureHandling.OPTIONAL)

if (IsDev == true) {
    WebUI.setText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_User name_UserName'), 'xl')

    WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_Password'), 
        'yBw77zEB3K3+kSSsja7LDA==')

    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_submitButton'))
}

WebUI.setText(findTestObject('Instructor/InstructorLogin/Page_Pearson Sign In/input_Username_username'), GlobalVariable.mxl_Instructor)

WebUI.setEncryptedText(findTestObject('Instructor/InstructorLogin/Page_Pearson Sign In/input_Password_password'), GlobalVariable.Password)

WebUI.click(findTestObject('Instructor/InstructorLogin/Page_Pearson Sign In/button_Sign in'))

boolean IsCountryPage = WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/SampleDev/Page_Add Country/h1_Welcome, Prabhashi'), 
    0, FailureHandling.OPTIONAL)

if (IsCountryPage == true) {
    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Add Country/button_Add and continue'))
}

WebUI.click(findTestObject('Instructor/InstructorLogin/Page_Launch - Hiranthi Insnew/a_Enter MyLab Math Global'))

WebUI.verifyElementPresent(findTestObject('Instructor/InstructorLogin/Page_Instructor Home - Hiranthi Insnew/h1_Instructor Home'), 
    0)


WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Instructor Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Gradebook - Prabhashi ins1/a_More Tools'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Gradebook - Prabhashi ins1/a_Delete Results'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Delete Results - Prabhashi ins1/input_Other_SHAll'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Delete Results - Prabhashi ins1/a_Delete Selected Results'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Delete Results - Prabhashi ins1/button_Permanently Delete Results'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Delete Results - Prabhashi ins1/a_OK'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Delete Results - Prabhashi ins1/h2_Delete Results'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Delete Results - Prabhashi ins1/a_Prabhashi ins1'))

WebUI.click(findTestObject('Object Repository/Student/TakeQuiz/Page_Delete Results - Prabhashi ins1/a_Log Out'))

WebUI.closeBrowser()

