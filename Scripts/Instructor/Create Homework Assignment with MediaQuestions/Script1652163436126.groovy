import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Instructor Home - Prabhashi ins1/a_Assignment Manager'))

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Assignment Manager - Prabhashi ins1/a_Create Assignment'))

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Assignment Manager - Prabhashi ins1/a_Create Homework'))

WebUI.setText(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/input_Homework Name_ctl00ctl00InsideFormMas_dbdf37'), 
    'TestingHomeworkAssignmentwithMediaQuestions1')

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/a_Next'))

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/h2_New Homework'))

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/a_Media'))

for (def i = 1; i < 6; i++) {
    System.out.println(findTestData('AssignmentData/MediaQuestions').getValue(GlobalVariable.executionProfile, i))

    WebUI.click(findTestObject('Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/input_Using the Distributive Property to Re_233d91', 
            [('AssignmentID') : findTestData('AssignmentData/MediaQuestions').getValue(GlobalVariable.executionProfile, i)]))
}

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/a_Add'))

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/a_Next'))

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Create Homework and Tests - Prabhashi ins1/a_Save  Assign'))

WebUI.click(findTestObject('Object Repository/Instructor/HWwithMQ/Page_Assignment Manager - Prabhashi ins1/h2_Assignment Manager'))

