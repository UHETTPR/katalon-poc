import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Instructor/ValidateGB/Page_Instructor Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/h2_Gradebook'))

WebUI.click(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/a_Assignments'))

WebUI.click(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/a_All Assignments'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/th_Show class metrics'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/th_TestingHomeworkAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/th_TestingQuizAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/th_TestingTestAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/td_80'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/td_60'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/td_100'), 
    0)

