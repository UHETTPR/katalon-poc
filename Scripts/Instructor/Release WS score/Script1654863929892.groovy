import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Instructor Home - Prabhashi ppe_ins1/a_Gradebook'))

String WindowTitle = WebUI.getWindowTitle()

System.out.println(WindowTitle)

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Gradebook - Prabhashi ppe_ins1/a_Assignments'))

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Gradebook - Prabhashi ppe_ins1/a_Homework'))

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Gradebook - Prabhashi ins1/i_Next Page_xlicon-arrow-last-page'), 
    FailureHandling.OPTIONAL)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Gradebook - Prabhashi ppe_ins1/img'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Gradebook - Prabhashi ppe_ins1/img'))

WebUI.switchToWindowTitle('Writing Solutions NG')

WebUI.waitForPageLoad(0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/h3_TestingWSAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/div_Completed by100 of class11 students'), 
    0)

WebUI.switchToWindowTitle(WindowTitle)

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Instructor Home - Prabhashi ppe_ins1/a_Gradebook'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Gradebook - Prabhashi ppe_ins1/a_Work needs grading (1)'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Gradebook - Prabhashi ppe_ins1/a_Work needs grading (1)'))

String WindowTitle2 = WebUI.getWindowTitle()

System.out.println(WindowTitle2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Alerts - Prabhashi ppe_ins1/td_Work needs grading                      _44e3ad'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Alerts - Prabhashi ppe_ins1/td_TestingWSAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Alerts - Prabhashi ppe_ins1/td_1'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Alerts - Prabhashi ppe_ins1/a_TestingWSAssignment'))

WebUI.switchToWindowTitle('Writing Solutions NG')

WebUI.sleep(10000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/h3_TestingWSAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/div_Completed by100 of class11 students'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/svg_Submitted_sc-bdnxRM cKGAYK pe-icon--mor_1bf6f3'))

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/button_View submission'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/h2_REVIEW ASSIGNMENT'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/span_Assignment TestingWSAssignment'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/p_Student Answer'), 
    0)

WebUI.setText(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/input_Submission_scoreInput'), 
    '78')

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/button_Release Score'))

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/button_Confirm'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/div_Score Released'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/ReleaseWSScore/Page_Writing Solutions NG/a_Refresh and save grade'))

WebUI.switchToWindowTitle(WindowTitle2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Alerts - Prabhashi ppe_ins1/td_Work needs grading                      _3a89d0'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Alerts - Prabhashi ppe_ins1/img_There are no alerts for the selected vi_e83310'))

