import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys



WebUI.click(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Instructor Home - Prabhashi ppe_ins1/a_Homework and Tests'))
String WindowTitle = WebUI.getWindowTitle()
System.out.println(WindowTitle)
WebUI.click(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Homework and Tests - Prabhashi ppe_ins1/a_TestingWSAssignment'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowTitle('Writing Solutions NG')

WebUI.sleep(5000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/h2_TestingWSAssignment'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/p_Question Description'))

WebUI.click(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/p'))

WebUI.setText(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/div_Instructor Answer'), 
    'Instructor Answer')

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/span_Saved'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/a_Close Assignment'))

WebUI.switchToWindowTitle(WindowTitle)

WebUI.click(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Homework and Tests - Prabhashi ppe_ins1/a_TestingWSAssignment'))

WebUI.sleep(5000)

WebUI.switchToWindowTitle('Writing Solutions NG')

WebUI.sleep(5000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/p_Instructor Answer'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/TakeWSasInstructor/Page_Writing Solutions NG/a_Close Assignment'))

