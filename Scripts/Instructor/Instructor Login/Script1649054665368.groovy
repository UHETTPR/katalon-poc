import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RC

//def executionProfile = RC.getExecutionProfile()
WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.mxl_URL)

WebUI.waitForPageLoad(0)

if (GlobalVariable.executionProfile == 'DEV') {
    WebUI.setText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_User name_UserName'), 'xl')

    WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_Password'),
        'yBw77zEB3K3+kSSsja7LDA==')

    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_submitButton'))
}

WebUI.setText(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Pearson Sign In/input_Username_username'), 
    GlobalVariable.mxl_Instructor)

WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Pearson Sign In/input_Password_password'), 
    GlobalVariable.Password)

WebUI.click(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Pearson Sign In/button_Sign in'))

//boolean IsCountryPage = WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/SampleDev/Page_Add Country/h1_Welcome, Prabhashi'), 
//    0, FailureHandling.OPTIONAL)
//
//if (IsCountryPage == true) {
//    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Add Country/button_Add and continue'))
//}
WebUI.click(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Launch - Hiranthi Insnew/a_Enter MyLab Math Global'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/InstructorLogin/Page_Instructor Home - Hiranthi Insnew/h1_Instructor Home'), 
    0)

