import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Instructor Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Gradebook - Prabhashi ins1/a_Export Data'))

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Quick Export - Prabhashi ins1/a_Advanced'))

WebUI.setText(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/input_(examplemath150)_ctl00ctl00InsideForm_70c019'), 
    'AdvancedExportTest1')

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/select_-- Choose --Student Assignment Resul_4fd34c'), 
    '0', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/select_-- Choose --MyLabXL formatBlackboard_6176b8'), 
    '0', true)

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/input_Percent format_ctl00ctl00InsideFormMa_5b38b3'))

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/input_Include median time spent_ctl00ctl00I_316a41'))

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/a_Request Export'))

WebUI.switchToWindowTitle('Export Confirmation')

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Export Confirmation/a_Close'))

WebUI.switchToWindowTitle('Advanced Export - Prabhashi ins1')

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/a_Done'))

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Gradebook - Prabhashi ins1/a_Export Data'))

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Quick Export - Prabhashi ins1/a_Advanced'))

WebUI.setText(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/input_(examplemath150)_ctl00ctl00InsideForm_70c019'), 
    'AdvancedExportTest2')

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/select_-- Choose --Student Assignment Resul_4fd34c'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/a_Request Export'))

WebUI.switchToWindowTitle('Export Confirmation')

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Export Confirmation/a_Close'))

WebUI.switchToWindowTitle('Advanced Export - Prabhashi ins1')

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Advanced Export - Prabhashi ins1/a_Done'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Gradebook - Prabhashi ins1/h2_Gradebook'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Gradebook - Prabhashi ins1/a_Export Data'))

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Quick Export - Prabhashi ins1/a_Retrieve Advanced Exports'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Retrieve Advanced Exports - Prabhashi ins1/td_AdvancedExportTest1'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Retrieve Advanced Exports - Prabhashi ins1/td_AdvancedExportTest2'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Retrieve Advanced Exports - Prabhashi ins1/a_Done'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/AdvancedExport/Page_Gradebook - Prabhashi ins1/h2_Gradebook'), 
    0)

