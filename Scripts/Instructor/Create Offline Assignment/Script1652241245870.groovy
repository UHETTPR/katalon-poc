import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.mxl_URL)

WebUI.setText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Pearson Sign In/input_Username_username'), GlobalVariable.mxl_Instructor)

WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Pearson Sign In/input_Password_password'), 
    GlobalVariable.Password)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Launch - Prabhashi ins1/a_Enter MyLab Math Global'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Instructor Home - Prabhashi ins1/a_Assignment Manager'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Assignment Manager - Prabhashi ins1/a_Create Assignment'))

WebUI.click(findTestObject('Instructor/OfflineItem/Page_Assignment Manager - Prabhashi ins1/a_Add Offline Item'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Next'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/button_-- Choose --'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Test'))

WebUI.setText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/input_Item name_ctl00ctl00InsideFormMasterC_e2cea6'), 
    'OfflineTest')

WebUI.setText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/input_Points Total_step2-points'), 
    '15')

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Next_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Save'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/h2_Offline Item Summary'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/td_Test'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/td_OfflineTest'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/td_15'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/a_OK'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Assignment Manager - Prabhashi ins1/h2_Assignment Manager'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Instructor Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Gradebook - Prabhashi ins1/a_Offline Items'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Gradebook - Prabhashi ins1/a_Add Items'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/h2_Add Offline Item'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Next'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/button_-- Choose --'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Quiz'))

WebUI.setText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/input_Item name_ctl00ctl00InsideFormMasterC_e2cea6'), 
    'OfflineQuiz')

WebUI.setText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/input_Points Total_step2-points'), 
    '20')

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Next_1'))

WebUI.sleep(2000)

WebUI.setText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/input_prabhashi_stu1_ctl00ctl00InsideFormMa_d209cc'), 
    '17.5')

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/td_87.5'))

WebUI.verifyElementText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/td_87.5'), '87.5%')

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Add Offline Item/a_Save'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/h2_Offline Item Summary'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/td_Quiz'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/td_OfflineQuiz'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/td_20'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/a_OK'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Gradebook - Prabhashi ins1/h2_Gradebook'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Gradebook - Prabhashi ins1/a_Offline Items'))

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Gradebook - Prabhashi ins1/a_Manage Items'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Manage Offline Items - Prabhashi ins1/h2_Manage Offline Items'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Manage Offline Items - Prabhashi ins1/td_OfflineTest'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Manage Offline Items - Prabhashi ins1/td_OfflineQuiz'), 
    0)

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/OfflineItem/Page_Manage Offline Items - Prabhashi ins1/select_-- Choose --Change ScoresEdit Item I_a68bb0'), 
    'Change Scores', true)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Manage Offline Items - Prabhashi ins1/a_Go'))

WebUI.sleep(4000)

WebUI.setText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Edit Offline Item/input_prabhashi_stu1_ctl00ctl00InsideFormMa_d209cc'), 
    '8')

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Edit Offline Item/td_53.33'))

WebUI.verifyElementText(findTestObject('Object Repository/Instructor/OfflineItem/Page_Edit Offline Item/td_53.33'), '53.33%')

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Edit Offline Item/a_Save'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/h2_Offline Item Summary'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/OfflineItem/Page_Offline Item Summary - Prabhashi ins1/a_OK'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/OfflineItem/Page_Manage Offline Items - Prabhashi ins1/h2_Manage Offline Items'), 
    0)

