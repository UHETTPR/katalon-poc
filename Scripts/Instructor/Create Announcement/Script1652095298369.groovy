import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://mylabglobal.pearson.com/login_global.htm')

WebUI.setText(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Pearson Sign In/input_Username_username'), 
    'prabhashi_ins1')

WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Pearson Sign In/input_Password_password'), 
    'p4y+y39Ir5Oy1MY8jPt0uQ==')

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Launch - Prabhashi ins1/a_Enter MyLab Math Global'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Instructor Home - Prabhashi ins1/a_Home Page Manager'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Home Page Manager - Prabhashi ins1/a_Home Page Settings'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Home Page Settings - Prabhashi ins1/h2_Home Page Settings'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Home Page Settings - Prabhashi ins1/a_Done'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Home Page Manager - Prabhashi ins1/a_Create Announcement'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Add Announcement - Prabhashi ins1/td_Title'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Add Announcement - Prabhashi ins1/td_Body'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Add Announcement - Prabhashi ins1/a_Save Announcement'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateAnnouncement/Page_Home Page Manager - Prabhashi ins1/h2_Home Page Manager'))

