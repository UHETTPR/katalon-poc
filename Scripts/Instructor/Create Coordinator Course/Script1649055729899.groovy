import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.mxl_URL)

WebUI.setText(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Pearson Sign In/input_Username_username'), 
    GlobalVariable.mxl_Instructor)

WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Pearson Sign In/input_Password_password'), 
    GlobalVariable.Password)

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Launch - Prabhashi ins1/a_Enter MyLab Math Global'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Instructor Home - Prabhashi ins1/a_Course Manager'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Course Manager - Prabhashi ins1/a_Create or copy a course'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/select_-- Choose --StandardCoordinatorMember'), 
    'Coordinator', true)

WebUI.setText(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/input_Course name_ctl00ctl00InsideFormMaste_92e586'), 
    GlobalVariable.CoordCourseName)

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/select_-- Choose Book --  AdamsEssex Calcul_16ae9d'), 
    GlobalVariable.mxl_BookMaterial, true)

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/a_Next'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/span_Coordinator'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/input_Member Settings_ctl00ctl00InsideFormM_f8caf3'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/a_Next'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/a_Save'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_New Course/a_OK'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Course Settings Summary - Prabhashi ins1/h2_Course Settings Summary'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Course Settings Summary - Prabhashi ins1/span_GlobalCoordTestCourse'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Course Settings Summary - Prabhashi ins1/span_MML Global Test Book'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Course Settings Summary - Prabhashi ins1/input_Browser Check_ctl00ctl00InsideFormMas_e7786d'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateCoordinatorCourse/Page_Course Manager - Prabhashi ins1/h2_Course Manager'), 
    0)

