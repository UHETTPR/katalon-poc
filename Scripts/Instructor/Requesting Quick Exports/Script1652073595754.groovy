import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Instructor Home - Prabhashi ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Gradebook - Prabhashi ins1/a_Export Data'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/h2_Export Data'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/a_Quick'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/select_--Choose--HomeworkQuizQuiz MeTestSam_942f0f'), 
    '2', true)

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/input_Export percentage scores formatted as_054caf'))

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/a_Export Data'))

WebUI.switchToWindowTitle('Export')

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Export/a_Detailed_Homework_Results.csv'))

WebUI.switchToWindowTitle('Quick Export - Prabhashi ins1')

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/select_--Choose--HomeworkQuizQuiz MeTestSam_942f0f'), 
    '4', true)

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/a_Export Data'))

WebUI.switchToWindowTitle('Export')

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Export/a_Detailed_Quiz_Results.csv'))

WebUI.switchToWindowTitle('Quick Export - Prabhashi ins1')

WebUI.selectOptionByValue(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/select_--Choose--HomeworkQuizQuiz MeTestSam_942f0f'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/input_Export percentage scores formatted as_509df3'))

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/a_Export Data'))

WebUI.switchToWindowTitle('Export')

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Export/a_Detailed_Test_Results.csv'))

WebUI.switchToWindowTitle('Quick Export - Prabhashi ins1')

WebUI.click(findTestObject('Object Repository/Instructor/QuickExport/Page_Quick Export - Prabhashi ins1/a_Done'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/QuickExport/Page_Gradebook - Prabhashi ins1/h2_Gradebook'), 
    0)

