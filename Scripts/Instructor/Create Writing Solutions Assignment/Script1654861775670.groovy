import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Date todaysDate = new Date()

def gScreenFormattedDate = todaysDate.format('MM/dd/yyyy')

def gFutureDate = todaysDate.plus(2).format('MM/dd/yyyy')

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Gradebook - Prabhashi ppe_ins1/a_Assignment Manager'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Assignment Manager - Prabhashi ppe_ins1/a_Create Assignment'))

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Assignment Manager - Prabhashi ppe_ins1/a_Create Writing Assignment'))

WebUI.setText(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/input_Assignment Name_ctl00ctl00InsideFormM_34b02f'), 
    'TestingWSAssignment')

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/a_Next'))

WebUI.sleep(10000)

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/input_Assignment title (required)_assignment-title'), 
    0)

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/input_Start_start-date (1)'))

WebUI.setText(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/input_Start_start-date (1)'), 
    gScreenFormattedDate)

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/input_Due_due-date (1)'))

WebUI.setText(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/input_Due_due-date (1)'), 
    gFutureDate)

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/div_Question Description'))

WebUI.setText(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/div_Question Description'), 
    'Question Description')

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/label_Manual scoring -  You can create or e_a5c807'), 
    0)

WebUI.sleep(2000)

WebUI.scrollToElement(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/h2_New Writing Assignment'), 
    3)

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/button_Save'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/button_Done'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/button_Done'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/td_TestingWSAssignment'), 
    0)

WebUI.click(findTestObject('Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/a_Save  Assign'))

