import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.navigateToUrl('https://mylab.pearson.com/login_mel.htm')

WebUI.setText(findTestObject('Object Repository/Instructor/Page_Pearson Sign In/input_Username_username'), 'prabhashi_ins1')

WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/Page_Pearson Sign In/input_Password_password'), 'p4y+y39Ir5Oy1MY8jPt0uQ==')

WebUI.click(findTestObject('Object Repository/Instructor/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/Instructor/Page_Launch - Prabhashi ins1/a_Enter MyLab Economics'))

WebUI.click(findTestObject('Object Repository/Instructor/Page_Instructor Home - Prabhashi ins1/a_Prabhashi ins1'))

WebUI.click(findTestObject('Object Repository/Instructor/Page_Instructor Home - Prabhashi ins1/a_Log Out'))

