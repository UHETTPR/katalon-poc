import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C:/Users/UHETTPR/git/uhettpr/katalon-poc/3PL Users.xlsx', 
    GlobalVariable.Product, true)

InstructorData = excelData.getValue(2, 1)

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.mxl_URL)

WebUI.setText(findTestObject('3PL/3plLogin/Page_Log In to Canvas/input_Email_pseudonym_sessionunique_id'), InstructorData)

WebUI.setEncryptedText(findTestObject('Object Repository/3PL/3plLogin/Page_Log In to Canvas/input_Password_pseudonym_sessionpassword'), 
    GlobalVariable.Password)

WebUI.click(findTestObject('Object Repository/3PL/3plLogin/Page_Log In to Canvas/button_Log In'))

WebUI.verifyElementPresent(findTestObject('Object Repository/3PL/3plLogin/Page_Dashboard/span_Dashboard'), 0)

