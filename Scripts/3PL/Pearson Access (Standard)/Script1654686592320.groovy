import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def product = GlobalVariable.Product
boolean isFound = product.contains('standard')
boolean isFoundppe = product.contains("ppe")
boolean isFoundcert = product.contains("cert")
boolean isFoundprod = product.contains("prod")

if (isFound == true) {
    boolean myLabMasteringNav = WebUI.verifyElementPresent(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_Aut_canvas_course_20200626-052049/a_MyLab and Mastering'), 
        0, FailureHandling.OPTIONAL)

    if (myLabMasteringNav == true) {
        WebUI.click(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_Aut_canvas_course_20200626-052049/a_MyLab and Mastering'))

        WebUI.click(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_MyLab and Mastering/button_Open MyLab  Mastering'))

        WebUI.sleep(8000)
    } else {
		WebUI.click(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_Aut_canvas_course_20220202-000440/a_Pearson Access'))
        WebUI.sleep(8000)

        WebUI.verifyElementPresent(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_Pearson Access/div_Select Open Pearson to see your content_28468c'), 
            5)

        WebUI.click(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_Pearson Access/span_Open Pearson'))

        WebUI.switchToWindowTitle('Pearson MyLab and Mastering')

        WebUI.sleep(5000)

        WebUI.click(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_Pearson MyLab and Mastering/button_Open MyLab  MasteringOpens in a new tab'))
    }
    
    WebUI.switchToWindowTitle('Course Home')

    WebUI.sleep(2000)

    WebUI.verifyElementPresent(findTestObject('Object Repository/3PL/StandardXLLaunch/Page_Course Home/h1_Instructor Home'), 
        0)
}



