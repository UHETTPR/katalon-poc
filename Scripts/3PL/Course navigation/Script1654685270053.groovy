import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C:/Users/UHETTPR/git/uhettpr/katalon-poc/3PL Users.xlsx', 
    GlobalVariable.Product, true)

CourseData = excelData.getValue(2, 3)

GlobalVariable.mxl_CourseName = this.CourseData

WebUI.click(findTestObject('Object Repository/3PL/Course_Nav/Page_Dashboard/span_Aut_canvas_course_20220202-000440'))

WebUI.verifyElementText(findTestObject('Object Repository/3PL/Course_Nav/Page_Aut_canvas_course_20220202-000440/li_Aut_canvas_course_20220202-000440'), 
    GlobalVariable.mxl_CourseName)

