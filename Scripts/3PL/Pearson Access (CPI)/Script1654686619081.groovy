import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def product = GlobalVariable.Product
boolean isFound = product.contains("cpi")
boolean isFoundppe = product.contains("ppe")
boolean isFoundcert = product.contains("cert")
boolean isFoundprod = product.contains("prod")

if((isFound==true) && (isFoundppe == true)) {
WebUI.click(findTestObject('Object Repository/3PL/CPIXLLaunch/Page_Aut_canvas_course_20220523-212756/a_Modules'))

WebUI.click(findTestObject('Object Repository/3PL/CPIXLLaunch/Page_Course Modules Aut_canvas_course_20220_516e60/a_PCP NFT'))
WebUI.sleep(5000)
WebUI.click(findTestObject('Object Repository/3PL/CPIXLLaunch/Page_PCP NFT/button_Access now'))
WebUI.sleep(10000)
WebUI.click(findTestObject('Object Repository/3PL/CPIXLLaunch/Page_PCP NFT/button_Open MyLab  Mastering'))

WebUI.switchToWindowTitle('Course Home')

WebUI.verifyElementPresent(findTestObject('Object Repository/3PL/CPIXLLaunch/Page_Course Home/h1_Aut_canvas_course_20220523-212756'), 
    0)

WebUI.click(findTestObject('Object Repository/3PL/CPIXLLaunch/Page_Course Home/a_Dashboard'))
WebUI.sleep(5000)
WebUI.verifyElementPresent(findTestObject('Object Repository/3PL/CPIXLLaunch/Page_Dashboard/h1_Instructor Home'), 0)
}
