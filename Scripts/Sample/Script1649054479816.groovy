import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://mylab.pearson.com/login_mel.htm')

WebUI.setText(findTestObject('Object Repository/LC/Page_Pearson Sign In/input_Username_username'), 'prabhashi_ins1')

WebUI.setEncryptedText(findTestObject('Object Repository/LC/Page_Pearson Sign In/input_Password_password'), 'p4y+y39Ir5Oy1MY8jPt0uQ==')

WebUI.click(findTestObject('Object Repository/LC/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Object Repository/LC/Page_Launch - Prabhashi ins1/a_Enter MyLab Economics'))

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Learning Catalytics Real-time learning'))

WebUI.switchToWindowTitle('Learning Catalytics')

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/td_Instructor-Led Synchronous'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic product of U.S. will _3c748b'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic product of U.S. will _54a1ca'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic product of India will_6072ce'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic  product of India wil_c7cb92'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/span_Start session'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/a_Deliver'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_A. 0'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_B. 0'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_C. 0'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/div_D. 0'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic product of U.S. will _3c748b'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic product of U.S. will _54a1ca'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic product of India will_6072ce'))

WebUI.click(findTestObject('Object Repository/LC/Page_Learning Catalytics/li_The gross domestic  product of India wil_c7cb92'))

WebUI.getText(findTestObject('Object Repository/LC/Page_Learning Catalytics/b_C'))

WebUI.switchToWindowTitle('Instructor Home - Prabhashi ins1')

WebUI.click(findTestObject('Object Repository/LC/Page_Instructor Home - Prabhashi ins1/a_Course Manager'))

