import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Dimension as Dimension
import org.openqa.selenium.Point as Point
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions as ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType

// open 2 Chrome browser, one in normal mode on the left, another in incognito mode on the right
WebDriver normalChrome = openChromeBrowserPlain()

//resizeHorizontalHalfLocateLeft(normalChrome)
DriverFactory.changeWebDriver(normalChrome)

WebUI.navigateToUrl(GlobalVariable.mxl_URL)

WebUI.waitForPageLoad(10)

WebDriver incognitoChrome = openChromeBrowserInIncognitoMode()

//resizeHorizontalHalfLocateRight(incognitoChrome)
DriverFactory.changeWebDriver(incognitoChrome)

WebUI.navigateToUrl(GlobalVariable.mxl_URL)

WebUI.waitForPageLoad(10)

// in the normal Chrome, login as instructor, create course and HW assignment
DriverFactory.changeWebDriver(normalChrome)

if (GlobalVariable.executionProfile == 'DEV') {
    WebUI.setText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_User name_UserName'), 'xl')

    WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_Password'), 
        'yBw77zEB3K3+kSSsja7LDA==')

    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_submitButton'))
}

WebUI.setText(findTestObject('Instructor/InstructorLogin/Page_Pearson Sign In/input_Username_username'), GlobalVariable.mxl_Instructor)

WebUI.setEncryptedText(findTestObject('Instructor/InstructorLogin/Page_Pearson Sign In/input_Password_password'), GlobalVariable.Password)

WebUI.click(findTestObject('Instructor/InstructorLogin/Page_Pearson Sign In/button_Sign in'))

WebUI.click(findTestObject('Instructor/InstructorLogin/Page_Launch - Hiranthi Insnew/a_Enter MyLab Math Global'))

WebUI.verifyElementPresent(findTestObject('Instructor/InstructorLogin/Page_Instructor Home - Hiranthi Insnew/h1_Instructor Home'), 
    0)

WebUI.callTestCase(findTestCase('Instructor/Create Standard Course'), [('CourseID') : '', (GlobalVariable.mxl_CourseName) : 'WBTestingCourse'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Instructor/Create Homework Assignment'), [:], FailureHandling.STOP_ON_FAILURE)

// in the incoginto Chrome, login as student, enroll to course and do WB
DriverFactory.changeWebDriver(incognitoChrome)

if (GlobalVariable.executionProfile == 'DEV') {
    WebUI.setText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_User name_UserName'), 'xl')

    WebUI.setEncryptedText(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_Password'), 
        'yBw77zEB3K3+kSSsja7LDA==')

    WebUI.click(findTestObject('Object Repository/Instructor/SampleDev/Page_Login/input_Password_submitButton'))
}

WebUI.setText(findTestObject('Object Repository/StudentEnrollment/Page_Pearson Sign In/input_Username_username'), GlobalVariable.mxl_Student)

WebUI.setEncryptedText(findTestObject('Object Repository/StudentEnrollment/Page_Pearson Sign In/input_Password_password'), 
    GlobalVariable.Password)

WebUI.click(findTestObject('Object Repository/StudentEnrollment/Page_Pearson Sign In/button_Sign in'))

WebUI.callTestCase(findTestCase('Student/StudentEnrollment'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Course Home - Prabhashi Hettiarachchi/a_Homework and Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Homework and Tests - Prabhashi Hettiarachchi/h2_Homework and Tests'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Homework and Tests - Prabhashi Hettiarachchi/span_All Assignments'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Homework and Tests - Prabhashi Hettiarachchi/a_TestingHomeworkAssignment'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/h2_Do Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/span_TestingHomeworkAssignment'), 
    0)

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/a_Start'))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_'))

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer'))

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question'))

WebUI.sleep(3000)

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button__1'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button__1'))

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1'))

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1'))

WebUI.sleep(3000)

// in the normal Chrome, validate GB as instructor
DriverFactory.changeWebDriver(normalChrome)

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Instructor Home - Prabhashi ppe_ins1/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Gradebook - Prabhashi ppe_ins1/a_Assignments'))

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Gradebook - Prabhashi ppe_ins1/a_Homework'))

WebUI.click(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Gradebook - Prabhashi ins1/i_Next Page_xlicon-arrow-last-page'), 
    FailureHandling.OPTIONAL)

while (!(WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Gradebook - Prabhashi ppe_ins1/td_78'), 
    0, FailureHandling.OPTIONAL))) {
    WebUI.delay(300)

    WebUI.refresh()

    WebUI.verifyElementPresent(findTestObject('Object Repository/Instructor/GBWSValidation/Page_Gradebook - Prabhashi ppe_ins1/td_78'), 
        0, FailureHandling.OPTIONAL)
}

// in the incoginto Chrome, continue HW as student
DriverFactory.changeWebDriver(incognitoChrome)

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button__1_2'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button__1_2'))

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1_2'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/td_Apply correct answer_1_2'))

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1_2'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Next question_1_2'))

WebUI.sleep(3000)

WebUI.waitForElementClickable(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Save'), 
    5)

WebUI.click(findTestObject('StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/button_Save'))

WebUI.sleep(3000)

// in the normal Chrome, validate GB again as instructor and delete course
DriverFactory.changeWebDriver(normalChrome)

WebUI.refresh()

WebUI.verifyElementPresent(findTestObject('Instructor/ValidateGB/Page_Gradebook - Prabhashi ins1/td_60'), 0)

WebUI.callTestCase(findTestCase('Instructor/Delete Course'), [:], FailureHandling.STOP_ON_FAILURE)

// close 2 browser windows
DriverFactory.changeWebDriver(normalChrome)

WebUI.closeBrowser()

WebUI.delay(1)

DriverFactory.changeWebDriver(incognitoChrome)

WebUI.closeBrowser( /**
 * opens a Chrome browser with nothing special
 * returns the ChromeDriver instance that is assocated with the window
 * @return
 */ /**
 * opens a Chrome browser with -incoginito mode,
 * returns the ChromeDriver instance that is associated with the window
 */ ) /**
 * opens a ChromeBrowser with the ChromeOptions given.
 * returns the ChromeDriver instance that is associated with the window
 * @param options
 * @return
 */
/**
 * resize the browser window to horizontal half, and move it to the right side
 * @param driver
 * @returns Dimension of the window
 */
/**
 * resize the browser window to horizontal half, and move it to the left side
 *
 * @param driver
 * @returns Dimension of the window
 */
/**
 * resize the browser window to half-width tile;
 * width=half of full screen, height=height of full screen
 *
 * @param driver
 * @return
 */

ChromeDriver openChromeBrowserPlain() {
    return openChromeBrowser(new ChromeOptions())
}

ChromeDriver openChromeBrowserInIncognitoMode() {
    ChromeOptions options = new ChromeOptions()

    options.addArguments('--incognito')

    return openChromeBrowser(options)
}

ChromeDriver openChromeBrowser(ChromeOptions options) {
    System.setProperty('webdriver.chrome.driver', DriverFactory.getChromeDriverPath())

    return new ChromeDriver(options)
}

//Dimension resizeHorizontalHalfLocateLeft(WebDriver driver) {
//    Dimension d = resizeToHorizontalHalf(driver)
//
//    driver.manage().window().setPosition(new Point(0, 0))
//
//    return d
//}
//
//Dimension resizeHorizontalHalfLocateRight(WebDriver driver) {
//    Dimension d = resizeToHorizontalHalf(driver)
//
//    driver.manage().window().setPosition(new Point(d.getWidth(), 0))
//
//    return d
//}
//
//Dimension resizeToHorizontalHalf(WebDriver driver) {
//    driver.manage().window().maximize()
//
//    Dimension maxDim = driver.manage().window().getSize()
//
//    Dimension curDim = new Dimension(((maxDim.getWidth() / 2) as Integer), maxDim.getHeight())
//
//    driver.manage().window().setSize(curDim)
//
//    return curDim
//}