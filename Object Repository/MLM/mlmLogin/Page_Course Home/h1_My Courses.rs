<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_My Courses</name>
   <tag></tag>
   <elementGuidId>bed9e695-d0dc-412a-ba84-189aa3c96f50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Browser size is too small to add or modify categories. Expand the browser window.'])[1]/following::h1[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.page-title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>144ef8f2-219a-42d7-8f8a-731848f1dbd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page-title</value>
      <webElementGuid>cd7cc5da-b55e-4a63-9f57-7112ad3c7510</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My Courses</value>
      <webElementGuid>948771f3-34c1-40a3-a957-3b45460ada97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;crs-home&quot;]/main[1]/ng-view[1]/div[1]/div[1]/div[@class=&quot;fluid-container&quot;]/div[@class=&quot;container-align page-header-panel&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;page-header-container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-9 col-sm-4&quot;]/h1[@class=&quot;page-title&quot;]</value>
      <webElementGuid>12a449fb-23bd-49d4-b9a2-0fec3ceb2064</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Browser size is too small to add or modify categories. Expand the browser window.'])[1]/following::h1[1]</value>
      <webElementGuid>01fa0a35-45b8-4985-b6ff-c8662501cdfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date:'])[1]/following::h1[1]</value>
      <webElementGuid>42f3f190-e5f1-4d62-8f4c-07b3e712166a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create/copy course'])[1]/preceding::h1[1]</value>
      <webElementGuid>f5c88e13-619b-42be-9daa-0e503361277a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create/Copy Course'])[1]/preceding::h1[1]</value>
      <webElementGuid>d9429277-a0b0-470e-8161-d6cc99b99c7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My Courses']/parent::*</value>
      <webElementGuid>6f0f39ad-20f6-4241-82df-74389e799575</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div/div/div/h1</value>
      <webElementGuid>dcfff5e8-d58a-45b3-88be-0a00415c2d23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'My Courses' or . = 'My Courses')]</value>
      <webElementGuid>590cf874-2f09-40e2-b869-30d953be66c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
