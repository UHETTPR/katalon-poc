<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Hiranthi Yashoda</name>
   <tag></tag>
   <elementGuidId>ed79d9b7-7c37-42e7-a3d5-12f0b317352a</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;NavigationLinkKey_49&quot;)/a[@class=&quot;dropdown-toggle my_account_name&quot;][count(. | //*[@href = '#']) = count(//*[@href = '#'])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.dropdown-toggle.my_account_name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>caadac4c-1d75-4612-95b8-2e1040ac2e54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>3637c81a-d36f-41df-92d5-d37c583d4f9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>267d7d44-4537-4f77-8931-300292dff7cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-toggle my_account_name</value>
      <webElementGuid>ea370fe1-dfc8-4a50-963c-59b07c1290e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>6c581131-b30c-4802-b35d-768a128b7fd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0be0fbbb-fbac-4791-a557-7695e78e1287</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Hiranthi Yashoda</value>
      <webElementGuid>72e14b96-4e58-4e20-9c65-8daa2eff20a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;NavigationLinkKey_49&quot;)/a[@class=&quot;dropdown-toggle my_account_name&quot;]</value>
      <webElementGuid>029d729d-4988-45b3-b16c-6cc7be5b26b5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='NavigationLinkKey_49']/a</value>
      <webElementGuid>e0b35a1f-8bbd-4f0b-9dd6-a1a5a0805eff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Hiranthi Yashoda')]</value>
      <webElementGuid>94d0373d-ffec-428b-8bf8-20d0ed62c060</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::a[1]</value>
      <webElementGuid>d74d2c7b-bacb-4175-b7c2-ba6cb01e4a4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hide Navigation'])[1]/following::a[3]</value>
      <webElementGuid>a9b78eee-feb1-4817-807f-448f5c886a9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enrol in Course'])[1]/preceding::a[1]</value>
      <webElementGuid>e802f9bb-e320-4092-9bfa-777870e5477a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit Account'])[1]/preceding::a[2]</value>
      <webElementGuid>d62b1c96-02a0-4f64-8f0f-b9c2f0d341de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Hiranthi Yashoda']/parent::*</value>
      <webElementGuid>beec20bc-7fc7-49c4-9876-bcd104cef3c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[2]</value>
      <webElementGuid>5f031779-77fa-4377-a611-d8ffda5bba28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>3c76e460-8fde-4d9c-94d1-5a063f9b1a21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'Hiranthi Yashoda' or . = 'Hiranthi Yashoda')]</value>
      <webElementGuid>8ee312c8-c27c-4e6c-8d2c-2ace95ebcebf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
