<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Select Open Pearson to see your content_28468c</name>
   <tag></tag>
   <elementGuidId>2198a0c2-ac7f-4d4a-b1b8-5006b1ea2088</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#openerContent</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='openerContent']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0e5657c9-ed93-4923-849b-4809980fd2d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>openerContent</value>
      <webElementGuid>ceb9decf-1977-40d5-b8dd-798761a3af79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row opener-content</value>
      <webElementGuid>dc6a4b5a-0ee8-4739-a500-cde67c85e8ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                
            
            
                Select Open Pearson to see your content.
            
             
                Select Pearson in {0} to open your content again.
            
            
                
                    Open Pearson
                
            
        </value>
      <webElementGuid>dbd7dcbe-ddce-4009-9d5c-4eeff478e2c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;openerContent&quot;)</value>
      <webElementGuid>d5c0a417-0aea-43ad-9abd-cffb3eca0a3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/3PL/StandardXLLaunch/Page_Pearson Access/iframe_The following content is partner pro_7c67b7</value>
      <webElementGuid>271aef17-f3cc-4e21-946e-ae2018bdda26</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='openerContent']</value>
      <webElementGuid>b94a25f1-c0df-4550-896a-39daae1f6ac3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='openerContentComponent']/section/div</value>
      <webElementGuid>9be5c16e-6252-463c-8e29-8dcdd9f4ac2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading Pearson content. Please wait.'])[1]/following::div[1]</value>
      <webElementGuid>8037e676-a680-406d-954d-bd9e8bbd1223</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'openerContent', '&quot;', ')')])[1]/preceding::div[5]</value>
      <webElementGuid>acf6d44a-0ecc-4bcb-a200-26bfd3ebaae0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/section/div</value>
      <webElementGuid>5bb280b7-29bb-4d66-9218-00f9db975536</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'openerContent' and (text() = '
            
                
            
            
                Select Open Pearson to see your content.
            
             
                Select Pearson in {0} to open your content again.
            
            
                
                    Open Pearson
                
            
        ' or . = '
            
                
            
            
                Select Open Pearson to see your content.
            
             
                Select Pearson in {0} to open your content again.
            
            
                
                    Open Pearson
                
            
        ')]</value>
      <webElementGuid>15613f78-af38-4fcb-9acd-f55010e0d773</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
