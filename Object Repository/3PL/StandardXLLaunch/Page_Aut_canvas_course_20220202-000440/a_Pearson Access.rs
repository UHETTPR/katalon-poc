<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Pearson Access</name>
   <tag></tag>
   <elementGuidId>5d3d38f1-dec7-4815-887f-c2707dedcb1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Pearson Access')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.context_external_tool_972486</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>444d402f-6469-4d4f-999a-76eba52e7ac3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/courses/3457405/external_tools/972486</value>
      <webElementGuid>19ef79ec-1549-465e-bac4-0676d487d915</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>context_external_tool_972486</value>
      <webElementGuid>c1aae608-7a2f-4ac7-9412-7bd0c50dd673</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>4ff48e65-afe3-4572-b02c-e04c04ebbdf7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pearson Access</value>
      <webElementGuid>32300aa4-18a7-4b26-b283-7718c6f6528b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;section-tabs&quot;)/li[@class=&quot;section&quot;]/a[@class=&quot;context_external_tool_972486&quot;]</value>
      <webElementGuid>74814930-e6da-40ea-bce0-95ea3153a649</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='section-tabs']/li[9]/a</value>
      <webElementGuid>fe346440-2431-44ae-b8a6-292c1e65fb39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Pearson Access')]</value>
      <webElementGuid>ea8e0c27-bd0a-473c-9f6f-a26694c334e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Files'])[1]/following::a[1]</value>
      <webElementGuid>159fa320-7787-4399-8bbc-7503e03a0298</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pages'])[1]/following::a[2]</value>
      <webElementGuid>b1ba109a-036b-45a2-98b7-c267ac9fec91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Syllabus'])[1]/preceding::a[1]</value>
      <webElementGuid>33d1611f-4f48-4c33-83cf-c79caa574271</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Outcomes'])[1]/preceding::a[2]</value>
      <webElementGuid>614aeae6-58bd-4926-9ead-8c9eee5227be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pearson Access']/parent::*</value>
      <webElementGuid>ace97ae6-b6de-4d08-badd-e7addde01ace</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/courses/3457405/external_tools/972486')]</value>
      <webElementGuid>7ce1eab4-1a1f-4a82-b4ee-24952fb9c105</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[9]/a</value>
      <webElementGuid>125d8b56-6cf7-40cf-8820-75815c9562aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/courses/3457405/external_tools/972486' and (text() = 'Pearson Access' or . = 'Pearson Access')]</value>
      <webElementGuid>089a1651-7c64-4b78-9309-8db0d61bc567</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
