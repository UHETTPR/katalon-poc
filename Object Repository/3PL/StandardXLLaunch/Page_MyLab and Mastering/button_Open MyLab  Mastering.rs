<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Open MyLab  Mastering</name>
   <tag></tag>
   <elementGuidId>7d95ea27-3db2-4bb9-91f3-a3c5692aa5c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#mobileBtnOpenMLM</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id = 'mobileBtnOpenMLM' and (text() = 'Open MyLab &amp; Mastering' or . = 'Open MyLab &amp; Mastering')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>688c7bc3-1961-4c24-b5cb-ac7961b0e27b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mobileBtnOpenMLM</value>
      <webElementGuid>2dbf35b4-b8d9-431b-9cd5-3498050a9a97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui primary button main__buttonColor___2yAKO</value>
      <webElementGuid>39cfb2cf-54fe-4598-aa5f-bcc27ef80a66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c076ce49-e60c-4aaf-bcf8-0591e244b9ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Open MyLab &amp; Mastering</value>
      <webElementGuid>6dbe41a8-ad80-4418-849e-8d31359214f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mobileBtnOpenMLM&quot;)</value>
      <webElementGuid>ac87a4c0-cd88-4116-8b9f-1c8e6c64d22e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/3PL/StandardXLLaunch/Page_MyLab and Mastering/iframe_The following content is partner pro_7c67b7</value>
      <webElementGuid>fccee277-537b-4570-9ed5-c1209fa31bb4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='mobileBtnOpenMLM']</value>
      <webElementGuid>f4dbb7b3-a5c7-4eaf-bf2a-186c29664526</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div[3]/div[2]/div/div[2]/div/button</value>
      <webElementGuid>98197726-7062-4aa2-8f1e-bb7c641636bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Opens in a new tab'])[1]/preceding::button[1]</value>
      <webElementGuid>39547539-e6a4-49ab-90fa-e247f0cb6962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Open MyLab &amp; Mastering']/parent::*</value>
      <webElementGuid>d0289d20-9e15-4805-8dca-93d038b92fc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>43ab4576-3e53-4ea7-b479-7fc4c269cd9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'mobileBtnOpenMLM' and (text() = 'Open MyLab &amp; Mastering' or . = 'Open MyLab &amp; Mastering')]</value>
      <webElementGuid>39077b48-0825-446e-8b95-308c0465834e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
