<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Instructor Home</name>
   <tag></tag>
   <elementGuidId>5f8a537a-1444-481d-8823-4f185e7d89f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-6 > h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>245048ec-2f2f-4599-8049-badf75efb6d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Instructor Home</value>
      <webElementGuid>050e436c-4145-4d48-9430-dfe15cd6947d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/header[1]/div[@class=&quot;pilot row&quot;]/div[@class=&quot;col-xs-6&quot;]/h1[1]</value>
      <webElementGuid>5b263a64-adb0-4068-a08e-d7a24621959b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/3PL/StandardXLLaunch/Page_Course Home/iframe_Manage View_centerIframe</value>
      <webElementGuid>28859fa9-487e-407e-abad-6f3cd8bdc519</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/header/div[2]/div/h1</value>
      <webElementGuid>31902b0b-8c26-4e89-9740-8254dc3cd66f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blitzer: Algebra and Trigonometry, 6e'])[1]/following::h1[1]</value>
      <webElementGuid>36dce729-2978-4e48-bb40-b209e7473f14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='canvas626 ins15'])[3]/following::h1[1]</value>
      <webElementGuid>41c8dc5d-f5bd-4435-afb5-f27a6d70e609</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Student Home'])[1]/preceding::h1[1]</value>
      <webElementGuid>8da85b01-0d03-4f01-937a-b98127cf05b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue to My Course'])[1]/preceding::h1[1]</value>
      <webElementGuid>3a681c69-eae2-434d-8db0-3c00d001d8e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h1</value>
      <webElementGuid>c50f183f-a6c6-4b33-91bf-1b946d29dd9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      <webElementGuid>d6bb892d-8588-46c1-850c-e332d3253602</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
