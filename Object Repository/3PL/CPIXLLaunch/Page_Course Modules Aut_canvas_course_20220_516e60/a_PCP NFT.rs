<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_PCP NFT</name>
   <tag></tag>
   <elementGuidId>f5d28e87-49a2-4909-b2f4-3b63ce36f4e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='context_module_item_83705940']/div/div[2]/div/span/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.ig-title.title.item_link</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>397cb2da-74ae-46ea-a348-0fe3e5f248fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>PCP NFT</value>
      <webElementGuid>e2428fc2-1090-48ff-8775-163086f9dda9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ig-title title item_link</value>
      <webElementGuid>d9c64598-e7f2-4cfe-ba81-33dd6dc398c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/courses/3527644/modules/items/83705940</value>
      <webElementGuid>1328dc93-b566-4a77-a017-ce2dd39d3573</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              PCP NFT
            </value>
      <webElementGuid>d81d475b-6e59-492b-b6f3-c3144e32797d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;context_module_item_83705940&quot;)/div[@class=&quot;ig-row  ig-published&quot;]/div[@class=&quot;ig-info&quot;]/div[@class=&quot;module-item-title&quot;]/span[@class=&quot;item_name&quot;]/a[@class=&quot;ig-title title item_link&quot;]</value>
      <webElementGuid>2da1fd2f-6e26-4a6b-9c21-ae099c255517</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='context_module_item_83705940']/div/div[2]/div/span/a</value>
      <webElementGuid>6c0f8a95-82a9-4b25-a4cc-24e1b4430733</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'PCP NFT')])[2]</value>
      <webElementGuid>ff070bd1-a3b2-4fac-a751-c74eb22a86de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='External Tool'])[1]/following::a[1]</value>
      <webElementGuid>92b18736-69c4-427a-bbcc-94be25eb5bfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PCP NFT'])[1]/following::a[1]</value>
      <webElementGuid>15e77ca6-d7ef-4142-8415-aa4c0275c618</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PCP NFT'])[3]/preceding::a[1]</value>
      <webElementGuid>9c5476e5-e2ab-4d2f-ac10-6b55eedde047</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://gateway-cpg-nft.pearson.com/launch-ui'])[1]/preceding::a[1]</value>
      <webElementGuid>872b6685-1c96-4962-98d4-1262ac2b0f33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/courses/3527644/modules/items/83705940')])[2]</value>
      <webElementGuid>eeb38d23-947d-4350-aabe-b919daf26bc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/a</value>
      <webElementGuid>38bb8a9c-c6eb-4533-891b-f0bf9f9a4d0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@title = 'PCP NFT' and @href = '/courses/3527644/modules/items/83705940' and (text() = '
              PCP NFT
            ' or . = '
              PCP NFT
            ')]</value>
      <webElementGuid>52948988-bbd6-48f4-b7e6-21ea3fa538a0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
