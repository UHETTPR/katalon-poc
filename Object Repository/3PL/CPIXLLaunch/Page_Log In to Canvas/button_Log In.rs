<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Log In</name>
   <tag></tag>
   <elementGuidId>1b6caab0-7def-4748-a138-a1a7b8bc6d1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.Button.Button--login</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5dca77c9-d6cf-4d21-8aed-29d63af7b50f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>e8ef64ae-5bb3-466b-9062-2015a0014564</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Button Button--login</value>
      <webElementGuid>43682fb0-73e2-4a67-a152-f92855c47ef0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Log In
                </value>
      <webElementGuid>95a36888-5488-4602-9b42-b6d5b833c0f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login_form&quot;)/div[@class=&quot;ic-Login__actions&quot;]/div[@class=&quot;ic-Form-control ic-Form-control--login&quot;]/button[@class=&quot;Button Button--login&quot;]</value>
      <webElementGuid>0280930e-94ad-4f4b-bede-161ecd62d065</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>33267aa8-a05e-4048-980d-831808aed09d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login_form']/div[3]/div[2]/button</value>
      <webElementGuid>215dd226-b560-4b50-b56a-3fa49819b096</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stay signed in'])[1]/following::button[1]</value>
      <webElementGuid>4949dd79-59ee-456a-b387-69bea5e91890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]</value>
      <webElementGuid>1e96a4d1-4380-44ca-965d-ea2b59cb8e57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password?'])[2]/preceding::button[1]</value>
      <webElementGuid>9a791bf6-ab88-485f-a426-8df89dd96364</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[2]/preceding::button[1]</value>
      <webElementGuid>0687bb0f-194c-47d4-9b09-8273e23c8f1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>a5395985-9343-42d2-ab76-d6c95bf89efc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = '
                  Log In
                ' or . = '
                  Log In
                ')]</value>
      <webElementGuid>dc960270-c5a4-4805-a395-a2e93bafeb64</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
