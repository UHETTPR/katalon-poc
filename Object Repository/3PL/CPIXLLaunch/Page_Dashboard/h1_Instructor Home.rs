<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Instructor Home</name>
   <tag></tag>
   <elementGuidId>da1a99af-05db-4784-b120-7e2e1387c577</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-6 > h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>15a262ec-d0fc-4202-a39d-7569c559bc5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Instructor Home</value>
      <webElementGuid>447ee3e4-fcff-436a-ba5b-b089bb4a9fa9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/header[1]/div[@class=&quot;pilot row&quot;]/div[@class=&quot;col-xs-6&quot;]/h1[1]</value>
      <webElementGuid>e2f09451-4713-4b1b-aa59-cfc2973b766c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/3PL/CPIXLLaunch/Page_Dashboard/iframe_Manage View_centerIframe</value>
      <webElementGuid>b63de327-b4db-4cfc-9c93-696e970f655f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/header/div[2]/div/h1</value>
      <webElementGuid>83c04a4e-6973-4835-9b4c-e8f478c92585</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Trigsted: College Algebra 1e'])[1]/following::h1[1]</value>
      <webElementGuid>59fae27d-4d03-4c52-b50b-c1b8c4fbacf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='mmndafterfixnewppe PPEins'])[3]/following::h1[1]</value>
      <webElementGuid>406499b5-f330-43fe-a8e7-03be2583dda6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Student Home'])[1]/preceding::h1[1]</value>
      <webElementGuid>070feb42-eb6d-4d71-bedf-248bea60c2b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue to My Course'])[1]/preceding::h1[1]</value>
      <webElementGuid>e758bb0a-6ee4-4f2b-964d-4879d2b30de6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h1</value>
      <webElementGuid>1f5dc231-fcca-44f0-ac8e-2b7a059b7f71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      <webElementGuid>a81b9d25-a2a4-4c65-8cfc-69bed702b2f5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
