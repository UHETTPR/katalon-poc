<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Aut_canvas_course_20220523-212756</name>
   <tag></tag>
   <elementGuidId>e1326877-18f3-4509-aed7-5ba6ebdd2065</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='DashboardCard_Container']/div/div/div/div/div/a/div/h3/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3.ic-DashboardCard__header-title.ellipsis > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>53bd4ded-f4ed-4885-8803-6922a39aa0c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Aut_canvas_course_20220523-212756</value>
      <webElementGuid>31cb0eeb-f6d3-42ac-aaa6-94d0856c5071</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DashboardCard_Container&quot;)/div[@class=&quot;unpublished_courses_redesign&quot;]/div[@class=&quot;ic-DashboardCard__box&quot;]/div[@class=&quot;ic-DashboardCard__box__container&quot;]/div[@class=&quot;ic-DashboardCard&quot;]/div[@class=&quot;ic-DashboardCard__header&quot;]/a[@class=&quot;ic-DashboardCard__link&quot;]/div[@class=&quot;ic-DashboardCard__header_content&quot;]/h3[@class=&quot;ic-DashboardCard__header-title ellipsis&quot;]/span[1]</value>
      <webElementGuid>796dc5f7-2fbe-44f8-96a9-57a2439a65a4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='DashboardCard_Container']/div/div/div/div/div/a/div/h3/span</value>
      <webElementGuid>195e01b8-5a11-4a96-a7c4-96fcad3079b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course card color region for Aut_canvas_course_20220523-212756'])[1]/following::span[1]</value>
      <webElementGuid>f54134bb-1db2-47e6-b073-66f42b00aef4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Published Courses (1)'])[1]/following::span[2]</value>
      <webElementGuid>a3ea891d-b8de-4e13-a697-08bf845a2311</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aut_canvas_course_20220523-212756'])[2]/preceding::span[1]</value>
      <webElementGuid>c17026e9-bd60-492b-ba14-0b9af04297ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a color or course nickname or move course card for Aut_canvas_course_20220523-212756'])[1]/preceding::span[1]</value>
      <webElementGuid>c242ba00-a0b2-4b7d-98f5-103d21a4ed1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Aut_canvas_course_20220523-212756']/parent::*</value>
      <webElementGuid>fcd9d662-32d8-4f62-bb2e-928f09e181ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3/span</value>
      <webElementGuid>1059c90c-f5be-468f-a413-e0a48a0ea253</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Aut_canvas_course_20220523-212756' or . = 'Aut_canvas_course_20220523-212756')]</value>
      <webElementGuid>6ba79de6-b396-40f9-8ec9-74ddda135221</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
