<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Log In</name>
   <tag></tag>
   <elementGuidId>bbbfc3c9-c0eb-44a8-b2ca-a79ec617ecbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.Button.Button--login</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0c5f8231-c68a-4134-8ac1-0671de4310d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>5f75a217-1b2c-4555-b3b4-2cb82f507bcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Button Button--login</value>
      <webElementGuid>e5265699-f085-47b0-90c4-e5cc89c2201c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Log In
                </value>
      <webElementGuid>987551e0-eaa0-4f71-90ba-998b519d853c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login_form&quot;)/div[@class=&quot;ic-Login__actions&quot;]/div[@class=&quot;ic-Form-control ic-Form-control--login&quot;]/button[@class=&quot;Button Button--login&quot;]</value>
      <webElementGuid>6f3a9b93-7407-4e55-a392-602a0581841a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>724403bd-9fc8-4329-8a18-c3e4684dce8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login_form']/div[3]/div[2]/button</value>
      <webElementGuid>0c8e2c3c-e2a5-4dc9-aa82-c48cbcb7aa29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stay signed in'])[1]/following::button[1]</value>
      <webElementGuid>bfe61804-b19b-4cee-8c92-b832ceae7d6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]</value>
      <webElementGuid>2922e574-4cac-45c3-ba20-4205edec5867</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password?'])[2]/preceding::button[1]</value>
      <webElementGuid>40a4259a-643e-4575-8bf1-5003821af501</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[2]/preceding::button[1]</value>
      <webElementGuid>285fde37-9118-4961-a3db-3bbe7eff3c76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>dfdb6f22-b7aa-448a-abd6-1b5a7330499d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = '
                  Log In
                ' or . = '
                  Log In
                ')]</value>
      <webElementGuid>36ad36ae-8003-4aa4-9b8f-40e4306f9b58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
