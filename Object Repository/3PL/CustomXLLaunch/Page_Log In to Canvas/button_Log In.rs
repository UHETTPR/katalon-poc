<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Log In</name>
   <tag></tag>
   <elementGuidId>23be50bd-9cdc-4aff-a24f-6304c353931b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.Button.Button--login</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>642555cc-45e6-47dd-9523-c304b0b00746</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>1199a476-8468-45a1-a6e4-d3e04168ad11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Button Button--login</value>
      <webElementGuid>db4bc6ad-a316-44ff-8e6f-672e1a7e0013</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Log In
                </value>
      <webElementGuid>01e0bb77-d67c-48bf-a947-8f56768407cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login_form&quot;)/div[@class=&quot;ic-Login__actions&quot;]/div[@class=&quot;ic-Form-control ic-Form-control--login&quot;]/button[@class=&quot;Button Button--login&quot;]</value>
      <webElementGuid>bf59e0d2-1fe9-41ba-954b-0432eed95ef3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>04896d67-80d8-45e0-bc6d-2a17d3a7b5a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login_form']/div[3]/div[2]/button</value>
      <webElementGuid>6fac781e-c028-4757-bd66-3d3c87fd9d5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stay signed in'])[1]/following::button[1]</value>
      <webElementGuid>f9ece35d-6992-4f57-a7f7-ac4c82226480</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]</value>
      <webElementGuid>cdda6948-2cea-4bf1-ae60-762758c0d5a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password?'])[2]/preceding::button[1]</value>
      <webElementGuid>3715a81c-85f8-4f1c-8b51-04f1a5e8f4ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[2]/preceding::button[1]</value>
      <webElementGuid>09f173f2-d5ae-43f9-a74e-e80bebad1d27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>6146a38b-1afd-4d01-be54-c6f709faf082</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = '
                  Log In
                ' or . = '
                  Log In
                ')]</value>
      <webElementGuid>7ab3a9e5-7de8-4973-8b2e-97261034534a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
