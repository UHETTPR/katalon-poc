<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Aut_canvas_course_20210923-125149 1   _64c879</name>
   <tag></tag>
   <elementGuidId>4f365d56-e20d-4386-9361-b40fdaf0015d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_BreadCrumb1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ctl00_ctl00_InsideForm_BreadCrumb1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>efdd057c-4979-4782-a370-5e35c682353d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_BreadCrumb1</value>
      <webElementGuid>56780e79-c5be-443d-8041-971e70c73960</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>breadcrumb</value>
      <webElementGuid>7d707639-7031-4f2e-924b-b98271fbd633</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


    
        
            Aut_canvas_course_20210923-125149 [1]
            
        
        
          
              
              Manage Course List
          
          
          Aut_canvas_course_20210923-125149 [1]
        
    




     
    

</value>
      <webElementGuid>d5de23d8-3dcf-4e5c-896e-9f801071132a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_BreadCrumb1&quot;)</value>
      <webElementGuid>fa6d757f-11a1-4f37-a67e-8f8a86bf39ab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_BreadCrumb1']</value>
      <webElementGuid>ca3afbcf-8209-47d6-b8d9-44eb6b460716</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_PnlBreadCrumb']/span[2]</value>
      <webElementGuid>d7323386-91aa-458e-a6b8-554b92b044b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Calendar'])[1]/following::span[2]</value>
      <webElementGuid>c818e4a0-a627-4ef0-a478-e7443e4bbb0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook'])[1]/following::span[2]</value>
      <webElementGuid>7bfae056-394e-4c3d-9bfe-51d5d37b9128</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>3dcdd2b5-ea98-4d01-8105-4fa6b9636823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ctl00_ctl00_InsideForm_BreadCrumb1' and (text() = '


    
        
            Aut_canvas_course_20210923-125149 [1]
            
        
        
          
              
              Manage Course List
          
          
          Aut_canvas_course_20210923-125149 [1]
        
    




     
    

' or . = '


    
        
            Aut_canvas_course_20210923-125149 [1]
            
        
        
          
              
              Manage Course List
          
          
          Aut_canvas_course_20210923-125149 [1]
        
    




     
    

')]</value>
      <webElementGuid>2d7a931e-4ec4-487e-8457-f1ff165e81a3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
