<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_MyMathLab GradeBook</name>
   <tag></tag>
   <elementGuidId>03bef2d5-0832-43d6-a9d9-7f95d077f3d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#context_module_item_77280486 > div.ig-row.ig-published > div.ig-info > div.module-item-title > span.item_name > a.ig-title.title.item_link</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@title = 'MyMathLab GradeBook' and @href = '/courses/3372572/modules/items/77280486' and (text() = '
              MyMathLab GradeBook
            ' or . = '
              MyMathLab GradeBook
            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>27b0dd72-c9ed-41c4-b902-2f97a4250207</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>MyMathLab GradeBook</value>
      <webElementGuid>47fee468-f1ce-477f-b538-9078118a63d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ig-title title item_link</value>
      <webElementGuid>4ad05675-f79b-4899-8c46-d280216a2ee1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/courses/3372572/modules/items/77280486</value>
      <webElementGuid>7686c549-f755-40bd-91a0-5c86317af356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              MyMathLab GradeBook
            </value>
      <webElementGuid>4d8c8712-2369-4d7b-839d-192ea26b5cc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;context_module_item_77280486&quot;)/div[@class=&quot;ig-row  ig-published&quot;]/div[@class=&quot;ig-info&quot;]/div[@class=&quot;module-item-title&quot;]/span[@class=&quot;item_name&quot;]/a[@class=&quot;ig-title title item_link&quot;]</value>
      <webElementGuid>1e7725ab-acea-4544-85d9-59995846dcb2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='context_module_item_77280486']/div/div[2]/div/span/a</value>
      <webElementGuid>4aca341a-f45d-43ae-82ee-8a28389fb586</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'MyMathLab GradeBook')])[2]</value>
      <webElementGuid>575b7b7e-e6ef-4f62-9738-92264566bb94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='External Tool'])[5]/following::a[1]</value>
      <webElementGuid>576cdaae-f03b-42d0-9ecd-f8f13809affa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MyMathLab GradeBook'])[1]/following::a[1]</value>
      <webElementGuid>14abd378-905e-40d6-90b5-062904f13ae1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MyMathLab GradeBook'])[3]/preceding::a[1]</value>
      <webElementGuid>09bec93f-2799-49d9-a49d-b5b21867a8e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://tpippe.pearsoncmg.com/tpi/lti/basic/mml_xl/1.0/?custom_targetId=classgrades'])[1]/preceding::a[1]</value>
      <webElementGuid>78f4076b-cb0b-4f93-9540-e8c6c258dc25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/courses/3372572/modules/items/77280486')])[2]</value>
      <webElementGuid>64ce5b73-109f-4626-bab5-093c3dc1dd64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/ul/li[2]/div/div[2]/div/span/a</value>
      <webElementGuid>7dc1cf47-d243-4531-9f97-e8fb92158956</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@title = 'MyMathLab GradeBook' and @href = '/courses/3372572/modules/items/77280486' and (text() = '
              MyMathLab GradeBook
            ' or . = '
              MyMathLab GradeBook
            ')]</value>
      <webElementGuid>f22e08d3-395e-4378-afd8-9ab193a0e510</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
