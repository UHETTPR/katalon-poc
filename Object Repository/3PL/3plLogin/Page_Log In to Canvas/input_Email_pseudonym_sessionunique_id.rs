<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email_pseudonym_sessionunique_id</name>
   <tag></tag>
   <elementGuidId>67aeb35a-9430-4c67-82d7-10d97398f7bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#pseudonym_session_unique_id</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='pseudonym_session_unique_id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a9c9c371-e816-434a-ba00-3c791a2af048</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ic-Input text</value>
      <webElementGuid>99c655c8-10af-4343-9c98-eb0da07233f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autofocus</name>
      <type>Main</type>
      <value>autofocus</value>
      <webElementGuid>386aa522-1aa8-40d7-942e-cd45cacb5488</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>7812ce73-76bb-4366-8717-d467a48ef41b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>pseudonym_session[unique_id]</value>
      <webElementGuid>48705ca2-6004-4802-80d1-757224b3a807</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>pseudonym_session_unique_id</value>
      <webElementGuid>572e1d60-e005-4320-8720-65981de35a4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;pseudonym_session_unique_id&quot;)</value>
      <webElementGuid>69bd2c30-153b-4f23-840e-9f5b803a45d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c93decdb-ff28-47e9-aff7-f77c339604a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ic-Input text</value>
      <webElementGuid>306184a2-57d0-4529-a77f-e7537d082418</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autofocus</name>
      <type>Main</type>
      <value>autofocus</value>
      <webElementGuid>5357b1b0-22b5-4e35-b161-c445fc510d71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>5e9088dc-f39e-492c-9214-c2a56b1b26cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>pseudonym_session[unique_id]</value>
      <webElementGuid>4225b523-7ba9-430e-bf39-6a4b664e4d43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>pseudonym_session_unique_id</value>
      <webElementGuid>997c5793-1dad-4212-9545-93f8c0be527d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;pseudonym_session_unique_id&quot;)</value>
      <webElementGuid>f64ab71a-8da0-44d4-b019-210cc3437b70</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='pseudonym_session_unique_id']</value>
      <webElementGuid>b16f2f8f-96ca-4bae-8c79-bf32939f9a5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login_form']/div/input</value>
      <webElementGuid>e1e3feae-33d3-49d1-89d1-7334aa69a147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>4391242f-ab0b-4f7f-9b84-07aeabd7b919</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'pseudonym_session[unique_id]' and @id = 'pseudonym_session_unique_id']</value>
      <webElementGuid>7f9104d6-a51a-442a-b3d9-a16c9472420b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
