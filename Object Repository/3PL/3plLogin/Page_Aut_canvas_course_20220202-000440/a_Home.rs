<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Home</name>
   <tag></tag>
   <elementGuidId>b44e7bb8-e4ef-4bb7-94a8-bdafcaf66599</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.home.active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='section-tabs']/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>778b24bc-bec0-43a0-8464-907bbdc247dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/courses/3457405</value>
      <webElementGuid>400ffcba-839a-42f7-9398-a734dfc1f007</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-current</name>
      <type>Main</type>
      <value>page</value>
      <webElementGuid>9062cfaf-0627-4582-91d8-c500434bc6aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>home active</value>
      <webElementGuid>c98b53a1-b734-41e8-ba80-3dbeaad78d6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>3e4c7487-fff7-4a0f-b5a2-0c06e6260943</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Home</value>
      <webElementGuid>2c84c67c-d422-4d43-8f50-382dd753d5ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;section-tabs&quot;)/li[@class=&quot;section&quot;]/a[@class=&quot;home active&quot;]</value>
      <webElementGuid>76bbe38a-9666-4892-b6ad-3406f4a25657</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='section-tabs']/li/a</value>
      <webElementGuid>8fa62bb4-fd03-4701-88f7-bfedf4724113</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Home')]</value>
      <webElementGuid>17ac3ee2-2f64-4b09-a1d0-e80a9b74f70c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expand tutorial tray'])[1]/following::a[2]</value>
      <webElementGuid>101ccaec-e13a-4087-9d25-e18802b17784</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Announcements'])[1]/preceding::a[1]</value>
      <webElementGuid>de76b18f-670a-4060-acf8-329d3b8de820</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[3]/preceding::a[2]</value>
      <webElementGuid>a058552e-b921-4e11-8a98-a7613056171f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Home']/parent::*</value>
      <webElementGuid>4c372c1f-5c73-483b-af05-f2b748d6bc77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/courses/3457405')])[5]</value>
      <webElementGuid>c4a5753f-17ed-4f1a-a12b-6c1dfda99958</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/nav/ul/li/a</value>
      <webElementGuid>45507e9d-8fc9-4d40-8a7f-cdc08d7bd9f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/courses/3457405' and (text() = 'Home' or . = 'Home')]</value>
      <webElementGuid>f47c7151-6aa3-41c8-bdfe-d217c463409b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
