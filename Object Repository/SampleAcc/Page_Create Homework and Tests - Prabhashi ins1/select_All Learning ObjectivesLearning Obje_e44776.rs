<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All Learning ObjectivesLearning Obje_e44776</name>
   <tag></tag>
   <elementGuidId>1cb3f8a9-ef17-4b3f-83fe-b678c216a25c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>03b99344-e388-4077-b21e-be20485f195e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>df23c253-f4ff-43be-863b-18199c2304df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>3a115490-774e-4628-9713-c5cc0d50b557</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>e0fb92cc-606c-4e20-9e12-1ed66490e56c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>var selValue = this.options[this.selectedIndex].value; ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection',''); ExSelectionsIntf.ChangedSection(selValue); }); return false;</value>
      <webElementGuid>3409c88b-1ad4-4848-a00f-037dc3207d2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All Learning Objectives
		Learning Objective 2.1: Essays1-Indents-Spreadsheets
		Learning Objective 2.2: Essays2-Indents-Spreadsheets
		Learning Objective 2.3: Math Palette
		Learning Objective 2.4: Absolute Value

	</value>
      <webElementGuid>8a2c2303-829b-4d65-b070-3e025be91f6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>41288fa9-8a4f-4aa0-99cb-6a802fdd270b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>42b44c39-7baa-472e-9d2e-08b3794d395b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>ad992668-ff5b-4af4-b83f-bfe98c88e171</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Objective'])[1]/following::select[1]</value>
      <webElementGuid>78c05151-a1b3-4fc8-a259-be94d2232483</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter'])[1]/following::select[2]</value>
      <webElementGuid>44a5e170-dcb6-4d9c-a130-69b1d40315a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[1]</value>
      <webElementGuid>989a5976-a9f9-46cf-81cd-b7f99cd51a76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>edbe96e2-1b53-4d26-9250-473562c29fdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All Learning Objectives
		Learning Objective 2.1: Essays1-Indents-Spreadsheets
		Learning Objective 2.2: Essays2-Indents-Spreadsheets
		Learning Objective 2.3: Math Palette
		Learning Objective 2.4: Absolute Value

	' or . = '
		All Learning Objectives
		Learning Objective 2.1: Essays1-Indents-Spreadsheets
		Learning Objective 2.2: Essays2-Indents-Spreadsheets
		Learning Objective 2.3: Math Palette
		Learning Objective 2.4: Absolute Value

	')]</value>
      <webElementGuid>bc08de0c-ca36-4008-bcc8-952edfbfbe56</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
