<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_S4-9 (similar to)_chk990079.1.4.20.18.0</name>
   <tag></tag>
   <elementGuidId>b0305795-d90a-48c3-8dcb-62d0a84691c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990079.1.4.20.18.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>35da453d-7c58-4a88-b3a6-b80a82367598</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>b5f5130f-3f75-416e-9bdd-661c1f4d00a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990079.1.4.20.18.0</value>
      <webElementGuid>380f5580-c347-4d09-910c-3447e071b5ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>S4-10 (similar to) (Learning Objective 1.4: Classify assets and liabilities as current or long-term)</value>
      <webElementGuid>60a42dae-a8b0-4dab-97be-07012bb5f1b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990079.1.4.20.18.0&quot;)</value>
      <webElementGuid>597a6a56-4146-48b3-9487-4697a260bd0b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990079.1.4.20.18.0']</value>
      <webElementGuid>f2861c88-8a92-4f20-a6ed-85c10a16f942</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990079.1.4.20.18.0']/div/div[2]/input</value>
      <webElementGuid>d0ee353f-ac5e-4909-85ef-944e05cf5092</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[18]/div/div[2]/input</value>
      <webElementGuid>767673c2-51f0-4c1e-b585-2d606462c857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990079.1.4.20.18.0' and @title = 'S4-10 (similar to) (Learning Objective 1.4: Classify assets and liabilities as current or long-term)']</value>
      <webElementGuid>323cf978-f0e2-426a-9f9f-fac0ed172b09</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
