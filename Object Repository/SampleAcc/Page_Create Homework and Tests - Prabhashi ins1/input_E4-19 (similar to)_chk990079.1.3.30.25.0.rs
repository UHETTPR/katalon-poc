<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_E4-19 (similar to)_chk990079.1.3.30.25.0</name>
   <tag></tag>
   <elementGuidId>fdf4aadc-570c-473b-9315-0827e1150138</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990079.1.3.30.25.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>884b6d58-3c1b-4685-b1b9-4ce14bd0b51e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>4cb7e30f-cc63-4b3e-b0dd-05abc22e3fb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990079.1.3.30.25.0</value>
      <webElementGuid>bf7c40a6-b767-4cb5-9157-c940ee5e3a9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>E4-20 (similar to) (Learning Objective 1.3: Close the revenue, expense, and withdrawal accounts)</value>
      <webElementGuid>81ceba64-b185-4a43-9d19-b373378312fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990079.1.3.30.25.0&quot;)</value>
      <webElementGuid>173c4f3e-1b79-44dc-9911-c79c6a658710</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990079.1.3.30.25.0']</value>
      <webElementGuid>fc836449-1973-437c-8539-0e59d33a9ecd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990079.1.3.30.25.0']/div/div[2]/input</value>
      <webElementGuid>7865239c-c59f-4dd6-98f7-55b70cf8ad36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[25]/div/div[2]/input</value>
      <webElementGuid>82771f57-78c0-4e5e-ad1f-6be6a1f40bf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990079.1.3.30.25.0' and @title = 'E4-20 (similar to) (Learning Objective 1.3: Close the revenue, expense, and withdrawal accounts)']</value>
      <webElementGuid>d7e2103a-c386-48d1-bcf2-08516e33b63d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
