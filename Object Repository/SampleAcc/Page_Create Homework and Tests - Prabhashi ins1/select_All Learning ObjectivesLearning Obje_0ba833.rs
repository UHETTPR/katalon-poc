<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All Learning ObjectivesLearning Obje_0ba833</name>
   <tag></tag>
   <elementGuidId>246bbc89-6687-4079-a562-abfb17ed0b09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>c430327b-6658-47be-b9be-b0a6b4f1a195</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>02e0fc1a-ea01-4e89-92fb-255927950599</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>6c94478d-add4-4345-9671-b846ef8eccb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>667c919d-8e26-457a-8287-7ba98a31e9fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>var selValue = this.options[this.selectedIndex].value; ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection',''); ExSelectionsIntf.ChangedSection(selValue); }); return false;</value>
      <webElementGuid>ea5d1460-f87d-49e7-8128-601386b1a2e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>91023af1-7d38-4787-8b79-55b8475ac2ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				All Learning Objectives
				
				
				
				
				

			Learning Objective 1.1: Prepare an accounting work sheetLearning Objective 1.2: Use the work sheetLearning Objective 1.3: Close the revenue, expense, and withdrawal accountsLearning Objective 1.4: Classify assets and liabilities as current or long-termLearning Objective 1.5: Use the current ratio and debt ratio to evaluate a company</value>
      <webElementGuid>e8303955-e763-434e-b15a-a5413b0c4afe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>882a6e48-4cc0-44da-8b8a-76e2ca5ee32c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>888096ee-7f44-49e0-ad32-b3ed49c86187</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>a7d3e43f-4dda-4078-99b2-5ef7c37ee9f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Objective'])[1]/following::select[1]</value>
      <webElementGuid>4f85218e-7e85-45ef-8ed6-ce84415d3b5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter'])[1]/following::select[2]</value>
      <webElementGuid>cedbe441-2cf1-4594-b6dc-e7771f90ddcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[1]</value>
      <webElementGuid>e70a5224-2502-4f20-bc65-e2ab995b650c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>c09a3f97-2593-42d1-9c1f-c72184fcb948</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
				All Learning Objectives
				
				
				
				
				

			Learning Objective 1.1: Prepare an accounting work sheetLearning Objective 1.2: Use the work sheetLearning Objective 1.3: Close the revenue, expense, and withdrawal accountsLearning Objective 1.4: Classify assets and liabilities as current or long-termLearning Objective 1.5: Use the current ratio and debt ratio to evaluate a company' or . = '
				All Learning Objectives
				
				
				
				
				

			Learning Objective 1.1: Prepare an accounting work sheetLearning Objective 1.2: Use the work sheetLearning Objective 1.3: Close the revenue, expense, and withdrawal accountsLearning Objective 1.4: Classify assets and liabilities as current or long-termLearning Objective 1.5: Use the current ratio and debt ratio to evaluate a company')]</value>
      <webElementGuid>32c54a5e-8306-4bde-86e8-0f51b5ec5b2c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
