<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All Learning ObjectivesLearning Obje_0ba833_1</name>
   <tag></tag>
   <elementGuidId>4228470f-fd54-4649-92c0-e829a877ce68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>ae6c0f7f-feec-4936-8ba6-18b3c727a897</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>87ee1c55-578d-4517-bc66-856351b73ff1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>2c1f27f3-87cb-4cb3-9b9f-a308e1f5e8d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>ed1f441f-782f-4f56-8755-efb61d9e2795</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>var selValue = this.options[this.selectedIndex].value; ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection',''); ExSelectionsIntf.ChangedSection(selValue); }); return false;</value>
      <webElementGuid>d7e463db-ad26-4c0f-9355-4e999b3d9cc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All Learning Objectives
		Learning Objective 1.1: Prepare an accounting work sheet
		Learning Objective 1.2: Use the work sheet
		Learning Objective 1.3: Close the revenue, expense, and withdrawal accounts
		Learning Objective 1.4: Classify assets and liabilities as current or long-term
		Learning Objective 1.5: Use the current ratio and debt ratio to evaluate a company

	</value>
      <webElementGuid>417e9910-0caf-4775-a691-37885627e72a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>f19e9a7d-c340-4c63-9db2-0751d6b67b1b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>c288a483-e0e3-48c8-99be-d32c95ab3317</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>19774c2b-1495-4099-ac9d-4b217ed6b7dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Objective'])[1]/following::select[1]</value>
      <webElementGuid>2b315abd-da56-432f-8822-1f4605632aba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter'])[1]/following::select[2]</value>
      <webElementGuid>fd502957-1e57-4cc4-8492-a5c02384b9f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[1]</value>
      <webElementGuid>12aa1cfc-9d9c-4110-b68e-a300c97cb953</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>33dfbf33-65f8-45c4-9db6-6d631f974e99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All Learning Objectives
		Learning Objective 1.1: Prepare an accounting work sheet
		Learning Objective 1.2: Use the work sheet
		Learning Objective 1.3: Close the revenue, expense, and withdrawal accounts
		Learning Objective 1.4: Classify assets and liabilities as current or long-term
		Learning Objective 1.5: Use the current ratio and debt ratio to evaluate a company

	' or . = '
		All Learning Objectives
		Learning Objective 1.1: Prepare an accounting work sheet
		Learning Objective 1.2: Use the work sheet
		Learning Objective 1.3: Close the revenue, expense, and withdrawal accounts
		Learning Objective 1.4: Classify assets and liabilities as current or long-term
		Learning Objective 1.5: Use the current ratio and debt ratio to evaluate a company

	')]</value>
      <webElementGuid>b910dbd7-431f-478a-b887-a339f4c04f5b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
