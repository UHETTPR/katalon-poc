<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_E4-14 (similar to)_chk990079.1.2.26.21.0</name>
   <tag></tag>
   <elementGuidId>3acbdb06-744a-4f4a-a66f-59b4d87d3306</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990079.1.2.26.21.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>55e73129-4f7b-4d83-a291-bb72b7d14131</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>615cc353-1a1f-4f85-89ab-ef05c1e26b32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990079.1.2.26.21.0</value>
      <webElementGuid>7ba51a7b-a1ba-447c-a49a-79c025a20e40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>E4-16 (similar to) (Learning Objective 1.2: Use the work sheet)</value>
      <webElementGuid>84e26178-dc88-4d2e-810f-0efa06460934</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990079.1.2.26.21.0&quot;)</value>
      <webElementGuid>cfefe1ea-687c-418a-be85-97ba51a53ce9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990079.1.2.26.21.0']</value>
      <webElementGuid>d68e4c62-d362-4097-b144-77d1fd5e0f02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990079.1.2.26.21.0']/div/div[2]/input</value>
      <webElementGuid>418e80d8-2b5d-455e-90e4-9b3ec804cc16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[21]/div/div[2]/input</value>
      <webElementGuid>06254790-6981-441f-8daf-c5fcda61ad26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990079.1.2.26.21.0' and @title = 'E4-16 (similar to) (Learning Objective 1.2: Use the work sheet)']</value>
      <webElementGuid>99987420-6209-4659-ac8c-46549b5b07c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
