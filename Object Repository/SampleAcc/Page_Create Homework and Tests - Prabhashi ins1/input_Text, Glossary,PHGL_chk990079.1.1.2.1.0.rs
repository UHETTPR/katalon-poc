<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Text, Glossary,PHGL_chk990079.1.1.2.1.0</name>
   <tag></tag>
   <elementGuidId>8e2f5d41-5111-4eac-acc2-50c1c4b39851</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990079.1.1.2.1.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fb974525-2f66-4898-b667-6ebec642f47f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>4a64cc3a-c6ba-4859-93c1-13031869cc4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990079.1.1.2.1.0</value>
      <webElementGuid>2bced010-d886-4789-ac1a-66ced50c0900</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Text, KC, TechH, Rev, Ex H (Learning Objective 1.1: Prepare an accounting work sheet)</value>
      <webElementGuid>aa2e16d1-8ecb-4668-99eb-8046fd38b223</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990079.1.1.2.1.0&quot;)</value>
      <webElementGuid>afe118d9-7e46-4f6b-b742-6fb056708fd2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990079.1.1.2.1.0']</value>
      <webElementGuid>662df30c-7577-4485-95c7-9597e2e23f51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990079.1.1.2.1.0']/div/div[2]/input</value>
      <webElementGuid>163d2244-835c-4add-a2bf-804c16082da6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/div[2]/input</value>
      <webElementGuid>739d607f-2b49-42db-99b6-2eec18ebef83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990079.1.1.2.1.0' and @title = 'Text, KC, TechH, Rev, Ex H (Learning Objective 1.1: Prepare an accounting work sheet)']</value>
      <webElementGuid>372f0978-8a87-4a12-af13-21925b140b84</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
