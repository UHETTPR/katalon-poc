<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Minimum mastery points required_ctl00_02668a</name>
   <tag></tag>
   <elementGuidId>a6b7a230-816b-4359-b9f9-6acac6c5110b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9e91b4a6-fca3-4f6b-bde1-45f22d769037</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints</value>
      <webElementGuid>5f02767e-746c-403e-987a-900f18cfc693</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$TextBoxMinimumMasteryPoints</value>
      <webElementGuid>8238770d-c936-4b6b-a1ca-64087ade1b85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>riTextBox riEnabled</value>
      <webElementGuid>120bc738-7b99-4749-8ff0-f610c7131e63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>decimaldigits</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>d9ff776b-9212-4606-8031-22ac3f19c34a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>a08d3889-5d8f-46e1-b9e9-637500034904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>901f7121-ffad-4bf8-9f04-82a159a55b4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints&quot;)</value>
      <webElementGuid>b39ea85f-cfcf-47fb-b0c8-f6199b453caf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints']</value>
      <webElementGuid>5befc276-418b-4055-856b-28edcf5472e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints_wrapper']/input</value>
      <webElementGuid>48b65a98-3286-4359-a07c-74f09706da98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[2]/span/input</value>
      <webElementGuid>c544810b-ea47-46f7-9f22-45304dc6fec0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints' and @name = 'ctl00$ctl00$InsideForm$MasterContent$TextBoxMinimumMasteryPoints' and @type = 'text']</value>
      <webElementGuid>e7fdf2d8-e61e-4817-adf7-d634e1475a76</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
