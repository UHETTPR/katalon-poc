<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1. Completing the Accounting Cycle2._9fdde9_1</name>
   <tag></tag>
   <elementGuidId>daea41d0-1279-4edd-a024-fadae226e186</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListChapter</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListChapter']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>cf5bc666-f917-4fef-b33c-4a28ba4142e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter</value>
      <webElementGuid>71d8aa7b-eab5-4e37-bf4e-a6445f634883</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter\',\'\')', 0)</value>
      <webElementGuid>077ddd64-2c23-4413-8aff-6688263a1cca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListChapter</value>
      <webElementGuid>b5f8041d-782e-4a29-84f2-87fde7540541</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>896c6d2f-d4b4-4117-84f9-5d5182cd05c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		1. Completing the Accounting Cycle
		2. Content for QA
		3. Content for QA Too

	</value>
      <webElementGuid>067dc526-f66c-4293-81ec-b79fd23b7254</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListChapter&quot;)</value>
      <webElementGuid>440b96cd-0149-453d-93b1-662bd7560d11</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListChapter']</value>
      <webElementGuid>dd1943af-8f72-476c-a490-04695d6a2f88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrChapter']/td[2]/div/select</value>
      <webElementGuid>0d6007b3-3583-4f20-a210-0a03f82b5c9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter'])[1]/following::select[1]</value>
      <webElementGuid>78e3387a-c9a5-477d-9a1c-a3731aa96e43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change...'])[1]/following::select[1]</value>
      <webElementGuid>3e66d5fa-8aeb-45f9-81a3-2781c9f06f7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Objective'])[1]/preceding::select[1]</value>
      <webElementGuid>0719c0c2-81d1-4a9f-aeee-409b3f35011d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>3f615c23-40c3-407b-9a5d-4890cd8c7dd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>eadf5d66-fdc3-41fb-bdbd-0e0be9c6b440</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter' and @id = 'DropDownListChapter' and (text() = '
		1. Completing the Accounting Cycle
		2. Content for QA
		3. Content for QA Too

	' or . = '
		1. Completing the Accounting Cycle
		2. Content for QA
		3. Content for QA Too

	')]</value>
      <webElementGuid>7e3fb224-b33a-4a1a-8b7b-ec16294730bd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
