<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create Homework</name>
   <tag></tag>
   <elementGuidId>f3c4d423-cce8-4cce-8910-ffc19996ef55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > ul > li > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1e8822f9-d75e-4155-9528-42c31371ca49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:GoCreate('homework')</value>
      <webElementGuid>362082b3-ea2d-41d5-9ac7-0683ab9744b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Homework</value>
      <webElementGuid>a8df2bfc-849f-4872-b813-2768f35e6f88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_hwAndTestManagerToolbar&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[1]/a[1]</value>
      <webElementGuid>a2032c28-b7fd-43e3-b32d-294438ca6d01</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li/a</value>
      <webElementGuid>76b38cd6-6c88-4520-beb2-4a7ba6c3320b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create Homework')]</value>
      <webElementGuid>1e75aabc-9c25-474d-90a1-cc845c3ed6d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::a[2]</value>
      <webElementGuid>b96a3582-47b7-4606-84cf-ae3d691a24da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Assignment Manager'])[1]/following::a[3]</value>
      <webElementGuid>c850af57-6298-4b92-b517-85f89b7d50f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Quiz'])[1]/preceding::a[1]</value>
      <webElementGuid>5232f6e3-df55-40ba-a0d2-16285d48b2b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Test'])[1]/preceding::a[2]</value>
      <webElementGuid>052afc02-751b-47a6-a1b3-9d45a8c13946</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create Homework']/parent::*</value>
      <webElementGuid>b10291d0-7523-4e6f-b037-add3d79605d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:GoCreate('homework')&quot;)]</value>
      <webElementGuid>e326fa4d-3d8a-4f66-9d5e-00ec178fefcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>df027842-a26d-47c7-9c28-b9b8a097635c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:GoCreate(&quot; , &quot;'&quot; , &quot;homework&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Create Homework' or . = 'Create Homework')]</value>
      <webElementGuid>ebe4293d-059b-457d-92a6-5aef61a6d312</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
