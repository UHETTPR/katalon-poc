<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Switch to single box for pasting your Course ID</name>
   <tag></tag>
   <elementGuidId>ca0d56d1-cce1-4837-9a4e-46c7564359c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='switchToFull();']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a825a5d4-d96e-468f-b118-c065ea64d0f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onkeypress</name>
      <type>Main</type>
      <value>if(isEnterOrSpace(event)){switchToFull()}</value>
      <webElementGuid>6261f1d8-aa29-4ed6-bec9-4729aaf52306</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>switchToFull();</value>
      <webElementGuid>6850d319-8706-44ec-b3d3-d8328cc81fe4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>b3750372-2240-4bce-a7a9-ea77b7ea990d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Switch to single box for pasting your Course ID</value>
      <webElementGuid>98ac8c43-6d93-4397-8e48-a938cefb64ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;courseIDparts&quot;)/div[@class=&quot;row&quot;]/a[1]</value>
      <webElementGuid>1c59f523-525b-4971-b15b-36de2a42c06d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='switchToFull();']</value>
      <webElementGuid>01c8ce49-3d03-4510-b40c-59d610f8386d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='courseIDparts']/div[2]/a</value>
      <webElementGuid>415be165-9752-49a7-aa50-26fadc237a6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Switch to single box for pasting your Course ID')]</value>
      <webElementGuid>29a04a83-fbce-49f9-b01a-afe4a7c67ea0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[3]/following::a[1]</value>
      <webElementGuid>e7de5e51-3ad7-4068-a263-b6b7a2af10a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[2]/following::a[1]</value>
      <webElementGuid>05412025-8b4f-4f5b-8859-47457351734f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Switch to multiple boxes for typing your Course ID'])[1]/preceding::a[1]</value>
      <webElementGuid>307cda21-8b24-4bc2-b3d6-25d3d8738e1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Switch to single box for pasting your Course ID']/parent::*</value>
      <webElementGuid>cbb6ecee-73d6-41bf-8f14-2d3bdaabf55b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[2]/a</value>
      <webElementGuid>10957309-1138-45ff-99c3-51bcd4c5a08f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'Switch to single box for pasting your Course ID' or . = 'Switch to single box for pasting your Course ID')]</value>
      <webElementGuid>51391377-f075-4146-8784-b2b07419b95f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
