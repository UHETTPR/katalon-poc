<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Testing LC Test 1 (42405192)</name>
   <tag></tag>
   <elementGuidId>60f62739-194c-41ae-b886-d74bad5a74f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#delivery_heading</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='delivery_heading']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b643d2a0-1933-4691-9f47-4636707dfaa5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>delivery_heading</value>
      <webElementGuid>12f1ff54-d2dd-45d7-92a5-c2a5fc08fda2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    Testing LC Test 1 (42405192)
  </value>
      <webElementGuid>ad7a069b-0677-4fdb-9453-5472c4f504aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;delivery_heading&quot;)</value>
      <webElementGuid>b7141db2-9265-4f0f-bc24-a231a0042073</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='delivery_heading']</value>
      <webElementGuid>97751162-89aa-4947-b01c-c597f559fd6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'delivery_heading', '&quot;', ')')])[1]/preceding::div[2]</value>
      <webElementGuid>e54df754-3f36-40c8-8ccb-2cb3b5278fe2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Testing LC Test 1 (']/parent::*</value>
      <webElementGuid>c7e686d9-d0d1-4a75-9c7c-b70f9e0a439d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]</value>
      <webElementGuid>ae53e3b3-9b34-4eba-9d76-92b410b66021</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'delivery_heading' and (text() = '
    
    Testing LC Test 1 (42405192)
  ' or . = '
    
    Testing LC Test 1 (42405192)
  ')]</value>
      <webElementGuid>f028241d-290e-4316-924e-821056fd749a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
