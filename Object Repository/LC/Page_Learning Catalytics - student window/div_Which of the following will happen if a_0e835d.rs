<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Which of the following will happen if a_0e835d</name>
   <tag></tag>
   <elementGuidId>07275c67-1075-4db8-be46-d2e590674131</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.deliver_wrapper</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d025bdbb-f4f0-4f6e-9f79-5f1833047c0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>deliver_wrapper</value>
      <webElementGuid>65b1af2b-c4ac-4bc0-b82d-7f9853916b21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  Which of the following will happen if an American tourist buys a painting from India?

  
















      
    The gross domestic product of U.S. will increase.
      
    The gross domestic product of U.S. will remain unchanged.
      
    The gross domestic product of India will increase.
      
    The gross domestic  product of India will decrease.



</value>
      <webElementGuid>287d320e-8724-490d-b261-19a1f4cb3661</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;deliver_wrapper&quot;]</value>
      <webElementGuid>9d833aa3-7db5-4bc3-aebf-db2c667f2625</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div</value>
      <webElementGuid>fc0c0efa-2283-4cf6-87e6-ec55517e63a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>dccacca1-221f-4383-bcb0-26a6abc1a8e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
  Which of the following will happen if an American tourist buys a painting from India?

  
















      
    The gross domestic product of U.S. will increase.
      
    The gross domestic product of U.S. will remain unchanged.
      
    The gross domestic product of India will increase.
      
    The gross domestic  product of India will decrease.



' or . = '
  Which of the following will happen if an American tourist buys a painting from India?

  
















      
    The gross domestic product of U.S. will increase.
      
    The gross domestic product of U.S. will remain unchanged.
      
    The gross domestic product of India will increase.
      
    The gross domestic  product of India will decrease.



')]</value>
      <webElementGuid>934e5035-f342-4cef-95ce-867dcdca2788</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
