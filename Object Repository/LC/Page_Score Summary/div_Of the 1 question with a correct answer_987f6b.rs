<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Of the 1 question with a correct answer_987f6b</name>
   <tag></tag>
   <elementGuidId>8eb043f0-7a5c-44f5-bec3-5ce28e00ed10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='score_summary']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#score_summary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a6f25d16-e182-4807-aee1-12d2c8f11552</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>score_summary</value>
      <webElementGuid>a179099a-aaa8-4c21-88f6-1084700ad3fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>











Of the 1 question with a correct answer, you answered 0 correctly; you responded to 0 of the 0 questions that had no correct answer.



    $(document).ready(function() {
        document.title = 'Score Summary';
    });
</value>
      <webElementGuid>af04e129-7215-4dd0-a385-7af681a10e0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;score_summary&quot;)</value>
      <webElementGuid>ab8f24ec-a669-49af-91b3-c997f924ec24</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='score_summary']</value>
      <webElementGuid>98c4c864-dd34-434a-b0ed-25fe1945043a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[4]</value>
      <webElementGuid>4326b913-7ff2-426b-a78a-35ed49fe25fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Session 17026978 (Testing LC Test 1) has ended'])[1]/following::div[3]</value>
      <webElementGuid>c4bfc62f-a2f0-4599-850d-f4f6cef8d408</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log out'])[1]/following::div[8]</value>
      <webElementGuid>38494c99-01a6-44a9-ac8d-afa66d609e2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Join another session'])[1]/preceding::div[1]</value>
      <webElementGuid>ae4f243a-576a-4506-9d9f-8c97aa7865ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2022 Pearson or its affiliate(s). All rights reserved.'])[1]/preceding::div[6]</value>
      <webElementGuid>bd675e44-0131-45ff-8b16-1f48d14c502c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Of the 1 question with a correct answer, you answered 0 correctly; you responded to 0 of the 0 questions that had no correct answer.']/parent::*</value>
      <webElementGuid>f8db39ca-3b6d-4539-982a-837dafed65dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[4]</value>
      <webElementGuid>8515514f-581b-4434-8458-8a8fdd00b55e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'score_summary' and (text() = concat(&quot;











Of the 1 question with a correct answer, you answered 0 correctly; you responded to 0 of the 0 questions that had no correct answer.



    $(document).ready(function() {
        document.title = &quot; , &quot;'&quot; , &quot;Score Summary&quot; , &quot;'&quot; , &quot;;
    });
&quot;) or . = concat(&quot;











Of the 1 question with a correct answer, you answered 0 correctly; you responded to 0 of the 0 questions that had no correct answer.



    $(document).ready(function() {
        document.title = &quot; , &quot;'&quot; , &quot;Score Summary&quot; , &quot;'&quot; , &quot;;
    });
&quot;))]</value>
      <webElementGuid>e39a1857-e1ce-4f67-9e17-87fe0930493a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
