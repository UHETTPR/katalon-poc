<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Session 42405192                       _83ed8b</name>
   <tag></tag>
   <elementGuidId>98d0ae9f-ddc7-426f-aa4b-5fbb913e5d61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4265fc58-bd30-44b9-9e95-d20081a06beb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>content</value>
      <webElementGuid>73a0ffd1-3171-451f-b722-eebeeedb752c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      Session 42405192
      
        
      
      
      
    






































  Please wait for the next question to become available.



    $(document).ready(function() {
        document.title = 'Wait for Next Question';
    });



    $(document).ready(function () {
        $(document).on('click', '#item a.response', wait);
        $(document).on('submit', '#item form', wait);
        $('#refresh').show().click(refresh);
    });

    function wait() {
        $('#loading').show();
        $('#item').html('');
        $('#status').val('refreshing');
    }

    function refresh() {
        wait();
        $.get('/class_sessions/42405192/current_item', function (data) {
            var to_send;
            if (data.students) {
                to_send = _.extend({round_id: data.round_id}, data.students['UrVYB9_v47EYsa7nTi3b']);
            } else {
                to_send = {round_id: data.round_id, status: data.status};
            }
            if (data.templates) {
                to_send.html = data.templates[to_send.status];
            }
            update(to_send);
        });
    }

    function connected() {
        $('#refresh').click();
    }

    function update(data) {
        $('#loading').hide();
        if (data.status == 'terminated') {
            location.href = '/class_sessions/42405192/terminated';
        }
        if (data.round_id != $('#round_id').val() || data.status != $('#status').val() || data.reveal_answer != $('#reveal_answer').val()) {
            $('#round_id').val(data.round_id);
            $('#status').val(data.status);
            $('#reveal_answer').val(data.reveal_answer);
            $('#item').html(data.html);
            $('.fill_from_template').each(function () {
                var content = data[$(this).attr('id')];
                if (content) {
                    $(this).html(content).show();
                }
            })

            // $('#item').hide(); //Hide raw equation untill enriched equation appears
            setTimeout(function () {
                if (!$('#item').children(&quot;.mathjax_parsed&quot;).length) {
                    $('#item').append(&quot;&lt;div class=\&quot;mathjax_parsed\&quot;>&quot;);
                    ears.mathJaxTypeset($('#item'));
                }
                $('#item').show();
            }, 500);
            ears.rescrollMobile();
        }
    }

    var canReact = true;

    function react() {
        if (canReact) {
            canReact = false;
            $.get(ears.socket_io.full_host() + '/react', {
                class_session_id: 42405192,
                student_id: 2516760
            }, function () {
                $('#react').addClass('reacted');
                setTimeout(function () {
                    $('#react').removeClass('reacted');
                    canReact = true;
                }, 61000);
            });
        }
    }






      
     Refresh
  



  
     Send a message to the instructor
  

  
    
    
       Send
       Cancel
    
  

  
     Join another session
  







    var authenticationToken = 'UrVYB9_v47EYsa7nTi3b';

    var showConnectivityDialog = function() {
        var modality = &quot;synchronous&quot;;
        if (modality === &quot;grouped&quot;) {
            if (ears.isMobile()) {
                alert(&quot;Your device is not communicating with the Learning Catalytics server. Please refresh the page or try a different device. If the problem persists, please contact Pearson Support&quot;);
            } else {
                $( &quot;#no-connectivity&quot; ).dialog({
                    resizable: false,
                    height:200,
                    width: 400,
                    modal: true,
                    buttons: {
                        &quot;Refresh Page&quot;: function() {
                            $( this ).dialog( &quot;close&quot; );
                            document.location.reload(true);
                        }
                    }
                });
            }
        }
    }

    $(document).ready(function() {
        $('#flash .notice').html('');
        $('#message_button').attr(&quot;aria-expanded&quot;,&quot;false&quot;);
    });

    $(window).load(function() {
        ears.socket_io.setup(_.extend({
            role: 'student',
            classSessionId: 42405192,
            authenticationToken: authenticationToken,
            id: 2516760
        }, {}), function(socket) {

            if (socket == null) {
                $('#loading').hide();
                showConnectivityDialog();
                return;
            }
            if (typeof connected === 'function') {
                connected();
            }
            socket.on('error', function(err) {
                showConnectivityDialog();
            });
            socket.on('message', function(data) {
                $('#loading').hide();
                update(data);
                switch (data.type) {
                    case 'active_students':
                        var otherStudents = data.activeStudents - 1;
                        $('#active_students').html(otherStudents + ' student' + (otherStudents == 1 ? '' : 's'));
                        // $('#active_students').html(&quot;Student(s) Active &quot; + message.activeStudents + ' : Joined ' + message.joinedStudents);
                        break;
                    default:
                        break;
                }
            });
        });
    });

    function toggleMessage() {
        if ($('#compose_message').is(':visible')) {
            $('#message_button').attr(&quot;aria-expanded&quot;,&quot;false&quot;);
            $('#compose_message').slideUp();
        } else {
            $('#compose_message').slideDown().position({ my: 'right top', at: 'right bottom', of: '#message_button', offset: '0 5' });
            $('#message_button').attr(&quot;aria-expanded&quot;,&quot;true&quot;);

            $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

            if($.browser.device){
                var isMobile = {
                    Android: function() {
                        return navigator.userAgent.match(/Android/i);
                    }
                }
                if( isMobile.Android() ) $('#compose_message textarea').focus();
            }else{
                $('#compose_message textarea').focus();
                $('#message_button').focus();
            }

            $('html').animate({
                scrollTop: $(&quot;#compose_message&quot;).offset().top
            }, 2000);

            if($.browser.device){
                setTimeout(function(){ $('#message_button').focus(); }, 5000);
            }
        }
    }

    function validateResponse(f) {
        $('form').submit(function() {
            if ($('#text_format_response').length == 0 || $('#text_format_response').is(':hidden')) {
                return f.call();
            }
        });
    }

    function sendMessage() {
        $.post('/messages/send', {
            round_id: $('#round_id').val(),
            class_session_id: 42405192,
            message: $('#compose_message textarea').val()
        })
        $('#compose_message').slideUp();
        $('#compose_message textarea').val('');
        $('#message_button').attr(&quot;aria-expanded&quot;,&quot;false&quot;);
    }



  Your device is not communicating with the Learning Catalytics server. Please refresh the page or try a different device. If the problem persists, please Contact Pearson Support.


    </value>
      <webElementGuid>ec1ec409-59d0-4927-b9ee-d53376a030b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)</value>
      <webElementGuid>7f7be46e-6529-4ca3-86b4-82b8613f8c6a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='content']</value>
      <webElementGuid>25754ca4-700e-4f86-8e4f-8865ed417fde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content_wrapper']/div</value>
      <webElementGuid>25811749-eb36-4647-85d4-a024446caff5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log out'])[1]/following::div[4]</value>
      <webElementGuid>cce984be-8021-4c85-a292-e4ec6eee5c6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prabhashi Hettiarachchi'])[1]/following::div[4]</value>
      <webElementGuid>30b4c852-d4f6-4bde-8df3-13591ea762d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[5]/div</value>
      <webElementGuid>803432e6-fc0b-4b44-92d2-3bee282334e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'content' and (text() = concat(&quot;
      Session 42405192
      
        
      
      
      
    






































  Please wait for the next question to become available.



    $(document).ready(function() {
        document.title = &quot; , &quot;'&quot; , &quot;Wait for Next Question&quot; , &quot;'&quot; , &quot;;
    });



    $(document).ready(function () {
        $(document).on(&quot; , &quot;'&quot; , &quot;click&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;#item a.response&quot; , &quot;'&quot; , &quot;, wait);
        $(document).on(&quot; , &quot;'&quot; , &quot;submit&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;#item form&quot; , &quot;'&quot; , &quot;, wait);
        $(&quot; , &quot;'&quot; , &quot;#refresh&quot; , &quot;'&quot; , &quot;).show().click(refresh);
    });

    function wait() {
        $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).show();
        $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).html(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
        $(&quot; , &quot;'&quot; , &quot;#status&quot; , &quot;'&quot; , &quot;).val(&quot; , &quot;'&quot; , &quot;refreshing&quot; , &quot;'&quot; , &quot;);
    }

    function refresh() {
        wait();
        $.get(&quot; , &quot;'&quot; , &quot;/class_sessions/42405192/current_item&quot; , &quot;'&quot; , &quot;, function (data) {
            var to_send;
            if (data.students) {
                to_send = _.extend({round_id: data.round_id}, data.students[&quot; , &quot;'&quot; , &quot;UrVYB9_v47EYsa7nTi3b&quot; , &quot;'&quot; , &quot;]);
            } else {
                to_send = {round_id: data.round_id, status: data.status};
            }
            if (data.templates) {
                to_send.html = data.templates[to_send.status];
            }
            update(to_send);
        });
    }

    function connected() {
        $(&quot; , &quot;'&quot; , &quot;#refresh&quot; , &quot;'&quot; , &quot;).click();
    }

    function update(data) {
        $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).hide();
        if (data.status == &quot; , &quot;'&quot; , &quot;terminated&quot; , &quot;'&quot; , &quot;) {
            location.href = &quot; , &quot;'&quot; , &quot;/class_sessions/42405192/terminated&quot; , &quot;'&quot; , &quot;;
        }
        if (data.round_id != $(&quot; , &quot;'&quot; , &quot;#round_id&quot; , &quot;'&quot; , &quot;).val() || data.status != $(&quot; , &quot;'&quot; , &quot;#status&quot; , &quot;'&quot; , &quot;).val() || data.reveal_answer != $(&quot; , &quot;'&quot; , &quot;#reveal_answer&quot; , &quot;'&quot; , &quot;).val()) {
            $(&quot; , &quot;'&quot; , &quot;#round_id&quot; , &quot;'&quot; , &quot;).val(data.round_id);
            $(&quot; , &quot;'&quot; , &quot;#status&quot; , &quot;'&quot; , &quot;).val(data.status);
            $(&quot; , &quot;'&quot; , &quot;#reveal_answer&quot; , &quot;'&quot; , &quot;).val(data.reveal_answer);
            $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).html(data.html);
            $(&quot; , &quot;'&quot; , &quot;.fill_from_template&quot; , &quot;'&quot; , &quot;).each(function () {
                var content = data[$(this).attr(&quot; , &quot;'&quot; , &quot;id&quot; , &quot;'&quot; , &quot;)];
                if (content) {
                    $(this).html(content).show();
                }
            })

            // $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).hide(); //Hide raw equation untill enriched equation appears
            setTimeout(function () {
                if (!$(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).children(&quot;.mathjax_parsed&quot;).length) {
                    $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).append(&quot;&lt;div class=\&quot;mathjax_parsed\&quot;>&quot;);
                    ears.mathJaxTypeset($(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;));
                }
                $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).show();
            }, 500);
            ears.rescrollMobile();
        }
    }

    var canReact = true;

    function react() {
        if (canReact) {
            canReact = false;
            $.get(ears.socket_io.full_host() + &quot; , &quot;'&quot; , &quot;/react&quot; , &quot;'&quot; , &quot;, {
                class_session_id: 42405192,
                student_id: 2516760
            }, function () {
                $(&quot; , &quot;'&quot; , &quot;#react&quot; , &quot;'&quot; , &quot;).addClass(&quot; , &quot;'&quot; , &quot;reacted&quot; , &quot;'&quot; , &quot;);
                setTimeout(function () {
                    $(&quot; , &quot;'&quot; , &quot;#react&quot; , &quot;'&quot; , &quot;).removeClass(&quot; , &quot;'&quot; , &quot;reacted&quot; , &quot;'&quot; , &quot;);
                    canReact = true;
                }, 61000);
            });
        }
    }






      
     Refresh
  



  
     Send a message to the instructor
  

  
    
    
       Send
       Cancel
    
  

  
     Join another session
  







    var authenticationToken = &quot; , &quot;'&quot; , &quot;UrVYB9_v47EYsa7nTi3b&quot; , &quot;'&quot; , &quot;;

    var showConnectivityDialog = function() {
        var modality = &quot;synchronous&quot;;
        if (modality === &quot;grouped&quot;) {
            if (ears.isMobile()) {
                alert(&quot;Your device is not communicating with the Learning Catalytics server. Please refresh the page or try a different device. If the problem persists, please contact Pearson Support&quot;);
            } else {
                $( &quot;#no-connectivity&quot; ).dialog({
                    resizable: false,
                    height:200,
                    width: 400,
                    modal: true,
                    buttons: {
                        &quot;Refresh Page&quot;: function() {
                            $( this ).dialog( &quot;close&quot; );
                            document.location.reload(true);
                        }
                    }
                });
            }
        }
    }

    $(document).ready(function() {
        $(&quot; , &quot;'&quot; , &quot;#flash .notice&quot; , &quot;'&quot; , &quot;).html(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
        $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;false&quot;);
    });

    $(window).load(function() {
        ears.socket_io.setup(_.extend({
            role: &quot; , &quot;'&quot; , &quot;student&quot; , &quot;'&quot; , &quot;,
            classSessionId: 42405192,
            authenticationToken: authenticationToken,
            id: 2516760
        }, {}), function(socket) {

            if (socket == null) {
                $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).hide();
                showConnectivityDialog();
                return;
            }
            if (typeof connected === &quot; , &quot;'&quot; , &quot;function&quot; , &quot;'&quot; , &quot;) {
                connected();
            }
            socket.on(&quot; , &quot;'&quot; , &quot;error&quot; , &quot;'&quot; , &quot;, function(err) {
                showConnectivityDialog();
            });
            socket.on(&quot; , &quot;'&quot; , &quot;message&quot; , &quot;'&quot; , &quot;, function(data) {
                $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).hide();
                update(data);
                switch (data.type) {
                    case &quot; , &quot;'&quot; , &quot;active_students&quot; , &quot;'&quot; , &quot;:
                        var otherStudents = data.activeStudents - 1;
                        $(&quot; , &quot;'&quot; , &quot;#active_students&quot; , &quot;'&quot; , &quot;).html(otherStudents + &quot; , &quot;'&quot; , &quot; student&quot; , &quot;'&quot; , &quot; + (otherStudents == 1 ? &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; : &quot; , &quot;'&quot; , &quot;s&quot; , &quot;'&quot; , &quot;));
                        // $(&quot; , &quot;'&quot; , &quot;#active_students&quot; , &quot;'&quot; , &quot;).html(&quot;Student(s) Active &quot; + message.activeStudents + &quot; , &quot;'&quot; , &quot; : Joined &quot; , &quot;'&quot; , &quot; + message.joinedStudents);
                        break;
                    default:
                        break;
                }
            });
        });
    });

    function toggleMessage() {
        if ($(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:visible&quot; , &quot;'&quot; , &quot;)) {
            $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;false&quot;);
            $(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).slideUp();
        } else {
            $(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).slideDown().position({ my: &quot; , &quot;'&quot; , &quot;right top&quot; , &quot;'&quot; , &quot;, at: &quot; , &quot;'&quot; , &quot;right bottom&quot; , &quot;'&quot; , &quot;, of: &quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;, offset: &quot; , &quot;'&quot; , &quot;0 5&quot; , &quot;'&quot; , &quot; });
            $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;true&quot;);

            $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

            if($.browser.device){
                var isMobile = {
                    Android: function() {
                        return navigator.userAgent.match(/Android/i);
                    }
                }
                if( isMobile.Android() ) $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).focus();
            }else{
                $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).focus();
                $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).focus();
            }

            $(&quot; , &quot;'&quot; , &quot;html&quot; , &quot;'&quot; , &quot;).animate({
                scrollTop: $(&quot;#compose_message&quot;).offset().top
            }, 2000);

            if($.browser.device){
                setTimeout(function(){ $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).focus(); }, 5000);
            }
        }
    }

    function validateResponse(f) {
        $(&quot; , &quot;'&quot; , &quot;form&quot; , &quot;'&quot; , &quot;).submit(function() {
            if ($(&quot; , &quot;'&quot; , &quot;#text_format_response&quot; , &quot;'&quot; , &quot;).length == 0 || $(&quot; , &quot;'&quot; , &quot;#text_format_response&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:hidden&quot; , &quot;'&quot; , &quot;)) {
                return f.call();
            }
        });
    }

    function sendMessage() {
        $.post(&quot; , &quot;'&quot; , &quot;/messages/send&quot; , &quot;'&quot; , &quot;, {
            round_id: $(&quot; , &quot;'&quot; , &quot;#round_id&quot; , &quot;'&quot; , &quot;).val(),
            class_session_id: 42405192,
            message: $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).val()
        })
        $(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).slideUp();
        $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).val(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
        $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;false&quot;);
    }



  Your device is not communicating with the Learning Catalytics server. Please refresh the page or try a different device. If the problem persists, please Contact Pearson Support.


    &quot;) or . = concat(&quot;
      Session 42405192
      
        
      
      
      
    






































  Please wait for the next question to become available.



    $(document).ready(function() {
        document.title = &quot; , &quot;'&quot; , &quot;Wait for Next Question&quot; , &quot;'&quot; , &quot;;
    });



    $(document).ready(function () {
        $(document).on(&quot; , &quot;'&quot; , &quot;click&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;#item a.response&quot; , &quot;'&quot; , &quot;, wait);
        $(document).on(&quot; , &quot;'&quot; , &quot;submit&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;#item form&quot; , &quot;'&quot; , &quot;, wait);
        $(&quot; , &quot;'&quot; , &quot;#refresh&quot; , &quot;'&quot; , &quot;).show().click(refresh);
    });

    function wait() {
        $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).show();
        $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).html(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
        $(&quot; , &quot;'&quot; , &quot;#status&quot; , &quot;'&quot; , &quot;).val(&quot; , &quot;'&quot; , &quot;refreshing&quot; , &quot;'&quot; , &quot;);
    }

    function refresh() {
        wait();
        $.get(&quot; , &quot;'&quot; , &quot;/class_sessions/42405192/current_item&quot; , &quot;'&quot; , &quot;, function (data) {
            var to_send;
            if (data.students) {
                to_send = _.extend({round_id: data.round_id}, data.students[&quot; , &quot;'&quot; , &quot;UrVYB9_v47EYsa7nTi3b&quot; , &quot;'&quot; , &quot;]);
            } else {
                to_send = {round_id: data.round_id, status: data.status};
            }
            if (data.templates) {
                to_send.html = data.templates[to_send.status];
            }
            update(to_send);
        });
    }

    function connected() {
        $(&quot; , &quot;'&quot; , &quot;#refresh&quot; , &quot;'&quot; , &quot;).click();
    }

    function update(data) {
        $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).hide();
        if (data.status == &quot; , &quot;'&quot; , &quot;terminated&quot; , &quot;'&quot; , &quot;) {
            location.href = &quot; , &quot;'&quot; , &quot;/class_sessions/42405192/terminated&quot; , &quot;'&quot; , &quot;;
        }
        if (data.round_id != $(&quot; , &quot;'&quot; , &quot;#round_id&quot; , &quot;'&quot; , &quot;).val() || data.status != $(&quot; , &quot;'&quot; , &quot;#status&quot; , &quot;'&quot; , &quot;).val() || data.reveal_answer != $(&quot; , &quot;'&quot; , &quot;#reveal_answer&quot; , &quot;'&quot; , &quot;).val()) {
            $(&quot; , &quot;'&quot; , &quot;#round_id&quot; , &quot;'&quot; , &quot;).val(data.round_id);
            $(&quot; , &quot;'&quot; , &quot;#status&quot; , &quot;'&quot; , &quot;).val(data.status);
            $(&quot; , &quot;'&quot; , &quot;#reveal_answer&quot; , &quot;'&quot; , &quot;).val(data.reveal_answer);
            $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).html(data.html);
            $(&quot; , &quot;'&quot; , &quot;.fill_from_template&quot; , &quot;'&quot; , &quot;).each(function () {
                var content = data[$(this).attr(&quot; , &quot;'&quot; , &quot;id&quot; , &quot;'&quot; , &quot;)];
                if (content) {
                    $(this).html(content).show();
                }
            })

            // $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).hide(); //Hide raw equation untill enriched equation appears
            setTimeout(function () {
                if (!$(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).children(&quot;.mathjax_parsed&quot;).length) {
                    $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).append(&quot;&lt;div class=\&quot;mathjax_parsed\&quot;>&quot;);
                    ears.mathJaxTypeset($(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;));
                }
                $(&quot; , &quot;'&quot; , &quot;#item&quot; , &quot;'&quot; , &quot;).show();
            }, 500);
            ears.rescrollMobile();
        }
    }

    var canReact = true;

    function react() {
        if (canReact) {
            canReact = false;
            $.get(ears.socket_io.full_host() + &quot; , &quot;'&quot; , &quot;/react&quot; , &quot;'&quot; , &quot;, {
                class_session_id: 42405192,
                student_id: 2516760
            }, function () {
                $(&quot; , &quot;'&quot; , &quot;#react&quot; , &quot;'&quot; , &quot;).addClass(&quot; , &quot;'&quot; , &quot;reacted&quot; , &quot;'&quot; , &quot;);
                setTimeout(function () {
                    $(&quot; , &quot;'&quot; , &quot;#react&quot; , &quot;'&quot; , &quot;).removeClass(&quot; , &quot;'&quot; , &quot;reacted&quot; , &quot;'&quot; , &quot;);
                    canReact = true;
                }, 61000);
            });
        }
    }






      
     Refresh
  



  
     Send a message to the instructor
  

  
    
    
       Send
       Cancel
    
  

  
     Join another session
  







    var authenticationToken = &quot; , &quot;'&quot; , &quot;UrVYB9_v47EYsa7nTi3b&quot; , &quot;'&quot; , &quot;;

    var showConnectivityDialog = function() {
        var modality = &quot;synchronous&quot;;
        if (modality === &quot;grouped&quot;) {
            if (ears.isMobile()) {
                alert(&quot;Your device is not communicating with the Learning Catalytics server. Please refresh the page or try a different device. If the problem persists, please contact Pearson Support&quot;);
            } else {
                $( &quot;#no-connectivity&quot; ).dialog({
                    resizable: false,
                    height:200,
                    width: 400,
                    modal: true,
                    buttons: {
                        &quot;Refresh Page&quot;: function() {
                            $( this ).dialog( &quot;close&quot; );
                            document.location.reload(true);
                        }
                    }
                });
            }
        }
    }

    $(document).ready(function() {
        $(&quot; , &quot;'&quot; , &quot;#flash .notice&quot; , &quot;'&quot; , &quot;).html(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
        $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;false&quot;);
    });

    $(window).load(function() {
        ears.socket_io.setup(_.extend({
            role: &quot; , &quot;'&quot; , &quot;student&quot; , &quot;'&quot; , &quot;,
            classSessionId: 42405192,
            authenticationToken: authenticationToken,
            id: 2516760
        }, {}), function(socket) {

            if (socket == null) {
                $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).hide();
                showConnectivityDialog();
                return;
            }
            if (typeof connected === &quot; , &quot;'&quot; , &quot;function&quot; , &quot;'&quot; , &quot;) {
                connected();
            }
            socket.on(&quot; , &quot;'&quot; , &quot;error&quot; , &quot;'&quot; , &quot;, function(err) {
                showConnectivityDialog();
            });
            socket.on(&quot; , &quot;'&quot; , &quot;message&quot; , &quot;'&quot; , &quot;, function(data) {
                $(&quot; , &quot;'&quot; , &quot;#loading&quot; , &quot;'&quot; , &quot;).hide();
                update(data);
                switch (data.type) {
                    case &quot; , &quot;'&quot; , &quot;active_students&quot; , &quot;'&quot; , &quot;:
                        var otherStudents = data.activeStudents - 1;
                        $(&quot; , &quot;'&quot; , &quot;#active_students&quot; , &quot;'&quot; , &quot;).html(otherStudents + &quot; , &quot;'&quot; , &quot; student&quot; , &quot;'&quot; , &quot; + (otherStudents == 1 ? &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; : &quot; , &quot;'&quot; , &quot;s&quot; , &quot;'&quot; , &quot;));
                        // $(&quot; , &quot;'&quot; , &quot;#active_students&quot; , &quot;'&quot; , &quot;).html(&quot;Student(s) Active &quot; + message.activeStudents + &quot; , &quot;'&quot; , &quot; : Joined &quot; , &quot;'&quot; , &quot; + message.joinedStudents);
                        break;
                    default:
                        break;
                }
            });
        });
    });

    function toggleMessage() {
        if ($(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:visible&quot; , &quot;'&quot; , &quot;)) {
            $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;false&quot;);
            $(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).slideUp();
        } else {
            $(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).slideDown().position({ my: &quot; , &quot;'&quot; , &quot;right top&quot; , &quot;'&quot; , &quot;, at: &quot; , &quot;'&quot; , &quot;right bottom&quot; , &quot;'&quot; , &quot;, of: &quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;, offset: &quot; , &quot;'&quot; , &quot;0 5&quot; , &quot;'&quot; , &quot; });
            $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;true&quot;);

            $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

            if($.browser.device){
                var isMobile = {
                    Android: function() {
                        return navigator.userAgent.match(/Android/i);
                    }
                }
                if( isMobile.Android() ) $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).focus();
            }else{
                $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).focus();
                $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).focus();
            }

            $(&quot; , &quot;'&quot; , &quot;html&quot; , &quot;'&quot; , &quot;).animate({
                scrollTop: $(&quot;#compose_message&quot;).offset().top
            }, 2000);

            if($.browser.device){
                setTimeout(function(){ $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).focus(); }, 5000);
            }
        }
    }

    function validateResponse(f) {
        $(&quot; , &quot;'&quot; , &quot;form&quot; , &quot;'&quot; , &quot;).submit(function() {
            if ($(&quot; , &quot;'&quot; , &quot;#text_format_response&quot; , &quot;'&quot; , &quot;).length == 0 || $(&quot; , &quot;'&quot; , &quot;#text_format_response&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:hidden&quot; , &quot;'&quot; , &quot;)) {
                return f.call();
            }
        });
    }

    function sendMessage() {
        $.post(&quot; , &quot;'&quot; , &quot;/messages/send&quot; , &quot;'&quot; , &quot;, {
            round_id: $(&quot; , &quot;'&quot; , &quot;#round_id&quot; , &quot;'&quot; , &quot;).val(),
            class_session_id: 42405192,
            message: $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).val()
        })
        $(&quot; , &quot;'&quot; , &quot;#compose_message&quot; , &quot;'&quot; , &quot;).slideUp();
        $(&quot; , &quot;'&quot; , &quot;#compose_message textarea&quot; , &quot;'&quot; , &quot;).val(&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
        $(&quot; , &quot;'&quot; , &quot;#message_button&quot; , &quot;'&quot; , &quot;).attr(&quot;aria-expanded&quot;,&quot;false&quot;);
    }



  Your device is not communicating with the Learning Catalytics server. Please refresh the page or try a different device. If the problem persists, please Contact Pearson Support.


    &quot;))]</value>
      <webElementGuid>8a545823-92bc-40c7-a56e-4ff171d2e9c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
