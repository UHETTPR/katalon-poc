<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Your response has been hidden for priva_8cf62a</name>
   <tag></tag>
   <elementGuidId>9ebbd4fe-49ce-4a8a-8930-3a9a5f3cfd34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id = 'why_hide_response' and (text() = '
    Your response has been hidden for privacy.
      Click Show response below to see it, or click Change response if you want to
      replace your current response with a new one.
  ' or . = '
    Your response has been hidden for privacy.
      Click Show response below to see it, or click Change response if you want to
      replace your current response with a new one.
  ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#why_hide_response</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>87f6995a-b4d2-491e-8e0b-e3bf81622360</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>why_hide_response</value>
      <webElementGuid>56120435-5955-491f-b380-fe56914fdc0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>alert_message</value>
      <webElementGuid>bd5543c8-b749-458d-851f-b704d4de4caf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>dc8d2275-bbb9-4c13-b190-50849db2703d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Your response has been hidden for privacy.
      Click Show response below to see it, or click Change response if you want to
      replace your current response with a new one.
  </value>
      <webElementGuid>400258a7-4304-4965-a68f-a6404d34a431</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;why_hide_response&quot;)</value>
      <webElementGuid>3257637b-e0b0-4d6f-80d3-c7c69d8b7a67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='why_hide_response']</value>
      <webElementGuid>c8f45f1e-f601-48ca-b6cb-e0877ff735f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='item']/div[3]</value>
      <webElementGuid>9bdc4154-faa7-4fac-b86a-9c0d206ce69f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='multiple choice question'])[1]/following::div[3]</value>
      <webElementGuid>8170ec1b-c986-4866-949d-21b3420db75e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Session 17026978'])[1]/following::div[9]</value>
      <webElementGuid>23972e20-8962-4876-99b9-10e9c06ecda8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[3]</value>
      <webElementGuid>20e8e720-932a-46e5-886b-79f1ed9c4e42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'why_hide_response' and (text() = '
    Your response has been hidden for privacy.
      Click Show response below to see it, or click Change response if you want to
      replace your current response with a new one.
  ' or . = '
    Your response has been hidden for privacy.
      Click Show response below to see it, or click Change response if you want to
      replace your current response with a new one.
  ')]</value>
      <webElementGuid>cc64e189-db24-445e-a84b-d22a847ec63b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
