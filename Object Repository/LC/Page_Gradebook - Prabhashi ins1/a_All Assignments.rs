<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_All Assignments</name>
   <tag></tag>
   <elementGuidId>fd0de790-23d3-43c6-9830-73b72738dd9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > ul > li > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'All Assignments')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>398869d8-4456-4951-9a63-62230ef0ea3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView(AllAssignments);</value>
      <webElementGuid>32f6cb54-e14b-4a85-8a1b-87a5c4bd4e05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Assignments</value>
      <webElementGuid>2bd9b9dc-f701-4f9e-b643-c6e63b0f16ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/div[@class=&quot;xlbootstrap3&quot;]/app-root[1]/app-gradebook[1]/app-instructor-links[1]/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[1]/li[1]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[1]/a[1]</value>
      <webElementGuid>4817c8ce-7773-4448-9036-4557d712ccc3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]/app-root/app-gradebook/app-instructor-links/table/tbody/tr/td/ul/li/div/div[2]/ul/li/a</value>
      <webElementGuid>dca6d901-df0c-4681-884b-52d007e97a07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'All Assignments')]</value>
      <webElementGuid>f336f214-d671-4d3e-8bb6-99cb52f57730</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/following::a[1]</value>
      <webElementGuid>ae3ac35f-26f9-4fba-8b6a-8fefc5310170</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Results By'])[1]/following::a[2]</value>
      <webElementGuid>674d68d0-18bd-4075-8e44-0752db68e667</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>69dd0a09-3e18-48d7-9faa-d25b50aff0bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quizzes'])[1]/preceding::a[2]</value>
      <webElementGuid>98327bad-322d-4cfb-a9ea-271a50ade126</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Assignments']/parent::*</value>
      <webElementGuid>3938d6c5-06d5-4ef6-bb1e-3f7caaf8cc1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:goView(AllAssignments);')]</value>
      <webElementGuid>8324af54-fddd-49a0-bfb7-f8d035b378bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>58c63c27-de7b-4347-8189-8f72773b72e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:goView(AllAssignments);' and (text() = 'All Assignments' or . = 'All Assignments')]</value>
      <webElementGuid>049639e1-bc06-42bd-92e3-777e0cac8800</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
