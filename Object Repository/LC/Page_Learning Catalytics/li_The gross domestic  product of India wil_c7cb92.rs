<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_The gross domestic  product of India wil_c7cb92</name>
   <tag></tag>
   <elementGuidId>aee276ee-ed37-4fec-becf-d3b0dc7050b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='item_display']/div/div[4]/ol/li[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>bdb454c1-4053-454d-bd62-43f080b97a01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>incorrect</value>
      <webElementGuid>291fb291-f5c7-4ac2-9b0e-810b551516ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    The gross domestic  product of India will decrease.</value>
      <webElementGuid>642c34fa-6e02-40de-8c06-2437df06e611</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item_display&quot;)/div[@class=&quot;item_view clearfix&quot;]/div[4]/ol[@class=&quot;lettered&quot;]/li[@class=&quot;incorrect&quot;]</value>
      <webElementGuid>265f7990-506d-4857-b1aa-c985b4f5bfa6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='item_display']/div/div[4]/ol/li[4]</value>
      <webElementGuid>750fe5dc-0b38-4125-ac59-f625655d56c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of India will increase.'])[2]/following::li[1]</value>
      <webElementGuid>5fdfbdcf-9dbe-4947-b5f1-7b48392a7c85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of U.S. will remain unchanged.'])[2]/following::li[2]</value>
      <webElementGuid>39862672-97c4-41f0-85df-0dbb9d1e01e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Answer'])[1]/preceding::li[1]</value>
      <webElementGuid>2e00aea5-5b89-4bbc-8a51-90d345c1cbcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='C'])[1]/preceding::li[1]</value>
      <webElementGuid>27ec19f7-db20-44a8-aaab-6de69ae1d41e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/ol/li[4]</value>
      <webElementGuid>e6d9c060-588b-4e4c-9702-c7178b60f5f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
    The gross domestic  product of India will decrease.' or . = '
    The gross domestic  product of India will decrease.')]</value>
      <webElementGuid>6a1a4b3c-5af0-4dd0-b7d0-939c1e5c5482</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
