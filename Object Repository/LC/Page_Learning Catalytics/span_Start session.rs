<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Start session</name>
   <tag></tag>
   <elementGuidId>564619f6-d628-4217-98d9-25ed7a5848e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='toolbar']/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>93a7757b-6827-4137-a65f-cd70c9e0448a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Start session</value>
      <webElementGuid>f4a0030c-2fb6-4e3c-8c94-4e1cc2246a12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;toolbar&quot;)/a[@class=&quot;start-session-link key_button iconic_link&quot;]/span[1]</value>
      <webElementGuid>e778146a-8f9a-48b9-9345-b9ae4013ef72</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='toolbar']/a/span</value>
      <webElementGuid>ba565524-9d2e-4fed-9c8a-2f30abbfef1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Testing LC Test 2'])[1]/following::span[1]</value>
      <webElementGuid>2ae571c5-e571-471d-b168-9901a04978d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EconLabTestCourse'])[1]/following::span[2]</value>
      <webElementGuid>6d0abeaf-eb85-41bd-ac4a-d948d35cd049</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/preceding::span[1]</value>
      <webElementGuid>81d4c9e2-dab1-427a-b799-729918c98666</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Review results'])[1]/preceding::span[2]</value>
      <webElementGuid>f7f7bc9d-af8b-45d2-a42a-98594aed6009</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Start session']/parent::*</value>
      <webElementGuid>0f05bf68-ae6f-4819-aeee-6e139eee307b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/span</value>
      <webElementGuid>b0c89c56-26a9-427f-9413-aec912b05a83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Start session' or . = 'Start session')]</value>
      <webElementGuid>7e479f16-7305-4861-ae4c-2a74800fca78</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
