<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_1 response, 100 correct</name>
   <tag></tag>
   <elementGuidId>8ede1e90-ed16-4f38-88ea-5d5a7c22c945</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.response_stats</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
    
    1 response, 100% correct
  ' or . = '
    
    1 response, 100% correct
  ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>01ad959c-9887-4675-9424-30466af8c758</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>response_stats</value>
      <webElementGuid>0eafc4a6-857b-477f-aaa0-455ba2470b26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    1 response, 0% correct
  </value>
      <webElementGuid>b031449f-9771-479f-9849-9f3012854e70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_wrapper_for_round_6119924&quot;)/div[@class=&quot;response_stats&quot;]</value>
      <webElementGuid>49765b9a-dd1a-4e39-8ac1-f3fc745d7cf5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='results_wrapper_for_round_6119924']/div[2]</value>
      <webElementGuid>fd55e11b-64d2-4abf-a597-87f18e5b1df8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Without revealing correct answer to students'])[2]/following::div[1]</value>
      <webElementGuid>da1a40d9-511e-4b2d-a836-6b7f03bb92e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(default)'])[2]/following::div[1]</value>
      <webElementGuid>3ba2b6ac-9f9b-41ed-b713-d75e307b7339</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div[2]</value>
      <webElementGuid>b6df503d-d1bd-42ff-8451-42e181692d07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    1 response, 0% correct
  ' or . = '
    
    1 response, 0% correct
  ')]</value>
      <webElementGuid>dd0cf807-bc89-40da-ab45-5403068bcf19</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
