<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Start session</name>
   <tag></tag>
   <elementGuidId>1c1dea75-b198-4a3a-adc1-4c6caa81ae53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[text() = ' Start session' or . = ' Start session']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>29ed1cd1-c30b-4ad7-90e0-3bf9018276ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>start-session-link key_button iconic_link  </value>
      <webElementGuid>8abca99e-f473-463d-b285-df82b1660eba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/courses/779503/modules/1800200/start_session</value>
      <webElementGuid>a79f951a-98cb-41e2-ac4a-5a0a93766c03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Start session</value>
      <webElementGuid>0bcc5b3b-c5b3-4939-b341-3be9a3c59062</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;toolbar&quot;)/a[@class=&quot;start-session-link key_button iconic_link&quot;]</value>
      <webElementGuid>c8d30ed9-99ad-45c1-833b-d58e889f1a60</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='toolbar']/a</value>
      <webElementGuid>c79e136d-9516-4bc3-bbba-195afab685fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Testing LC Test 1'])[1]/following::a[1]</value>
      <webElementGuid>417e8679-5a80-4684-8301-18cacdc6e0dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PostBvtTestCourse'])[1]/following::a[2]</value>
      <webElementGuid>4a7918a0-0bb5-4f8b-b403-c46ae2f1677c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/preceding::a[1]</value>
      <webElementGuid>ff5fc0bc-82da-4199-91e1-e67a6a4d0a6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/courses/779503/modules/1800200/start_session')]</value>
      <webElementGuid>d11cf797-f94b-4760-ad43-4549f7d81dbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/a</value>
      <webElementGuid>f0f981fc-969f-4ec9-bc5d-ab92f32ac51a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/courses/779503/modules/1800200/start_session' and (text() = ' Start session' or . = ' Start session')]</value>
      <webElementGuid>74a22940-cfa4-4178-8e4e-7873bf09a22d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
