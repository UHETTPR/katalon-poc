<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_C. 0</name>
   <tag></tag>
   <elementGuidId>f4859025-5321-4705-b5ea-2c1fc3d6c45d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.line.correct</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='results_for_round_6128581']/div/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b27f5736-1ff9-4f81-9541-25ec13b216fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>line correct</value>
      <webElementGuid>edc2060d-d7be-4490-9be4-16da6d4b89f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
      C. 0%
    
  </value>
      <webElementGuid>82d95cb6-d3e7-4132-bb9d-3f343c00afca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_for_round_6128581&quot;)/div[@class=&quot;bar_graph&quot;]/div[@class=&quot;line correct&quot;]</value>
      <webElementGuid>b20a8207-e9fc-4311-971a-6fa291d0abf6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='results_for_round_6128581']/div/div[3]</value>
      <webElementGuid>4a598f24-9d4b-4896-b836-7fbdea56633d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]</value>
      <webElementGuid>db244ca3-a34f-498a-885e-77759aa702b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
      C. 0%
    
  ' or . = '
    
      C. 0%
    
  ')]</value>
      <webElementGuid>acaef101-804a-4dce-aa12-70db6fcc2196</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
