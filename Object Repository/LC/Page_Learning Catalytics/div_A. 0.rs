<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_A. 0</name>
   <tag></tag>
   <elementGuidId>78050e05-5b29-4ad8-8da0-2d42a3ae1093</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.line.incorrect</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='results_for_round_6128581']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b15ba590-f395-4b0a-8fd9-786137af35d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>line incorrect</value>
      <webElementGuid>22bc3ed7-f4df-4ac0-9791-7942fb53f62e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
      A. 0%
    
  </value>
      <webElementGuid>17b01efd-b448-4cab-a3ac-a199fc0a7250</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_for_round_6128581&quot;)/div[@class=&quot;bar_graph&quot;]/div[@class=&quot;line incorrect&quot;]</value>
      <webElementGuid>de1cbb03-f958-4ed6-a934-918115cabe04</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='results_for_round_6128581']/div/div</value>
      <webElementGuid>bad53b41-3868-48de-bedf-ac64d68017c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Without revealing correct answer to students'])[2]/following::div[4]</value>
      <webElementGuid>ac16d908-844e-4c64-a79e-f88ba49df3ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(default)'])[2]/following::div[4]</value>
      <webElementGuid>5f53138b-9324-4a42-8e6f-8d4720e1313e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div</value>
      <webElementGuid>8e8c7ee2-e548-41ba-8d29-da23573a5f8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
      A. 0%
    
  ' or . = '
    
      A. 0%
    
  ')]</value>
      <webElementGuid>2ca06b2b-ed03-46e2-80aa-86d12eba318c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
