<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_B. 0</name>
   <tag></tag>
   <elementGuidId>69a870ce-96be-49c4-8997-eb2b891ad8cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='results_for_round_6128581']/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>47e76a62-7dba-4353-be55-a1d20d8e583c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>line incorrect</value>
      <webElementGuid>3d4c4630-0eb3-4506-8a2e-343bff23f947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
      B. 0%
    
  </value>
      <webElementGuid>b6ea9744-e5ec-4dc7-83ec-bf7acfb3b75d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_for_round_6128581&quot;)/div[@class=&quot;bar_graph&quot;]/div[@class=&quot;line incorrect&quot;]</value>
      <webElementGuid>c98476bc-2f4b-4526-ac01-b366b8b7afd0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='results_for_round_6128581']/div/div[2]</value>
      <webElementGuid>2671c94e-0c0f-4bd7-9f75-4d9571eef037</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Without revealing correct answer to students'])[2]/following::div[6]</value>
      <webElementGuid>082ca57a-6530-4a42-9b38-9c09eaef0e2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]</value>
      <webElementGuid>d57577cd-1e26-4f92-adbf-458bc729dcd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
      B. 0%
    
  ' or . = '
    
      B. 0%
    
  ')]</value>
      <webElementGuid>be60fbf0-99fa-47fe-9935-333606fa11dc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
