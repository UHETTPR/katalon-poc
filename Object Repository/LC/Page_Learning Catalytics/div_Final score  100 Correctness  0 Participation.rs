<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Final score  100 Correctness  0 Participation</name>
   <tag></tag>
   <elementGuidId>ca38cd83-9b7e-49fb-943a-79312fdb4e04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.participation-weight-legend</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Final score = 100% Correctness + 0% Participation' or . = 'Final score = 100% Correctness + 0% Participation')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ee638ae3-d881-4074-bb40-293dcfa893c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>participation-weight-legend</value>
      <webElementGuid>558a8dd9-cdd0-4a8b-bd99-582550dba419</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Final score = 100% Correctness + 0% Participation</value>
      <webElementGuid>3714645e-69f6-4701-9b45-6f8378187cae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;lecture_participation_weight_input&quot;)/div[@class=&quot;participation-weight-legend&quot;]</value>
      <webElementGuid>28202187-6c12-4c85-a751-e5bf0c8e3913</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='lecture_participation_weight_input']/div</value>
      <webElementGuid>f4aa4231-b658-4253-96f6-3b102d90774a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Participation weight'])[1]/following::div[1]</value>
      <webElementGuid>028cffd1-0959-410c-8305-f59595868b91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hide sessions for this module from students'])[1]/following::div[1]</value>
      <webElementGuid>80261704-3a9b-4575-874c-11ce3adeb654</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Students receive creditonly for correct responses'])[1]/preceding::div[1]</value>
      <webElementGuid>c2635d4e-3d99-4f25-9014-3a8f13b6e3f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Students receive creditfor any response'])[1]/preceding::div[2]</value>
      <webElementGuid>47af65cf-a0a9-4958-9b37-b107ddfe0509</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Final score = 100% Correctness + 0% Participation']/parent::*</value>
      <webElementGuid>5e552238-5377-49ee-a74c-4704e57485c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/div</value>
      <webElementGuid>1fa52666-9bb9-41a1-83a2-d323124f22c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Final score = 100% Correctness + 0% Participation' or . = 'Final score = 100% Correctness + 0% Participation')]</value>
      <webElementGuid>9715ef0c-f5d8-4cf9-8be8-bcc579970357</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
