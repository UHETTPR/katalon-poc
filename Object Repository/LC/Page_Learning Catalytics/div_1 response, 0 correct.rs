<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_1 response, 0 correct</name>
   <tag></tag>
   <elementGuidId>773783bb-3ecf-467b-bde2-84e5674bc750</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
    
    1 response, 0% correct
  ' or . = '
    
    1 response, 0% correct
  ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.response_stats</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9a860b14-6a6c-4764-ac70-9757bd8e8037</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>response_stats</value>
      <webElementGuid>c38fda91-7244-4676-b1b5-55279fb7212a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    1 response, 0% correct
  </value>
      <webElementGuid>779cac38-7439-4d2b-be1f-ebdbe7976ade</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_wrapper_for_round_6119924&quot;)/div[@class=&quot;response_stats&quot;]</value>
      <webElementGuid>5ea1ca8f-883c-4364-b639-0bd2d7d9bcdb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='results_wrapper_for_round_6119924']/div[2]</value>
      <webElementGuid>330b4482-30a2-4b65-9298-1d803a3355fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Without revealing correct answer to students'])[2]/following::div[1]</value>
      <webElementGuid>89312b94-df19-4301-b9cf-5090527a7cca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(default)'])[2]/following::div[1]</value>
      <webElementGuid>d6368c8d-74e0-4606-84b6-97c4b49893c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div[2]</value>
      <webElementGuid>952f6ab4-81a2-42f3-a7d3-fb492411f7ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    1 response, 0% correct
  ' or . = '
    
    1 response, 0% correct
  ')]</value>
      <webElementGuid>cea37c03-5f14-479f-affa-f70d075633be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
