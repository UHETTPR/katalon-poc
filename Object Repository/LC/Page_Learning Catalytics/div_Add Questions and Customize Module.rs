<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Add Questions and Customize Module</name>
   <tag></tag>
   <elementGuidId>cb9afa81-c8f9-4535-910f-f65d05db79f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Add Questions and Customize Module' or . = 'Add Questions and Customize Module')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4775b0e1-af2d-438e-884f-f923497df4db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title</value>
      <webElementGuid>e53e26f6-d224-4861-95fd-edad448bf82a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Questions and Customize Module</value>
      <webElementGuid>3996694d-e7f4-43c3-9c9a-b8c3615189b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page_title&quot;)/div[@class=&quot;title&quot;]</value>
      <webElementGuid>328d1ea9-de88-4dd8-880a-e0ee5ef7e353</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page_title']/div</value>
      <webElementGuid>de920b58-b96e-44f3-9c9c-a1e618e902e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Testing LC Test 1'])[1]/following::div[4]</value>
      <webElementGuid>9e4ff66e-20e2-4038-a6a8-1b1ab1d4904f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EconLabTestCourse'])[1]/following::div[4]</value>
      <webElementGuid>1bbd818e-a781-45fb-bf7f-8a8618b80967</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::div[2]</value>
      <webElementGuid>31318ab8-966e-445e-aae7-9a97eb0798a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add Questions and Customize Module']/parent::*</value>
      <webElementGuid>5f35c008-5d4c-42a7-a0ca-5418e85df575</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[4]/div</value>
      <webElementGuid>632cbbc2-7bd8-487d-a4c7-3c4709e9c554</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Add Questions and Customize Module' or . = 'Add Questions and Customize Module')]</value>
      <webElementGuid>02543b85-3f7f-4556-9a4a-f3ee90f96a99</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
