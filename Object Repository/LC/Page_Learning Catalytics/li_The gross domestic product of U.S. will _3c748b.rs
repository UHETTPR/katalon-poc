<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_The gross domestic product of U.S. will _3c748b</name>
   <tag></tag>
   <elementGuidId>de6315dc-ad33-44ca-9a68-b895cc468482</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='item_display']/div/div[4]/ol/li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>c2924d6b-8d88-4bbc-a1a8-cafd3db1e17f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>incorrect</value>
      <webElementGuid>9f595528-c4ad-40f7-9eb1-5c99f01d1f24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    The gross domestic product of U.S. will increase.</value>
      <webElementGuid>74268ab9-4a37-4f11-9aca-c3fb8634ef1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item_display&quot;)/div[@class=&quot;item_view clearfix&quot;]/div[4]/ol[@class=&quot;lettered&quot;]/li[@class=&quot;incorrect&quot;]</value>
      <webElementGuid>4cd0cd5e-cd9a-4e2e-a622-d62b22f7b553</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='item_display']/div/div[4]/ol/li</value>
      <webElementGuid>8964fdb0-4f80-41f7-91a6-360bc322a5c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='multiple choice'])[1]/following::li[1]</value>
      <webElementGuid>2a2d0842-60d9-47b3-af4c-cfe22d7f6369</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of U.S. will remain unchanged.'])[2]/preceding::li[1]</value>
      <webElementGuid>894ca652-2643-4d1a-a2cb-4d816f4a7f16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of India will increase.'])[2]/preceding::li[2]</value>
      <webElementGuid>49a19ec4-c666-4bb6-95a3-02e8f4a76fb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/ol/li</value>
      <webElementGuid>f56dc335-ccfa-4942-888b-837ee2dfc405</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
    The gross domestic product of U.S. will increase.' or . = '
    The gross domestic product of U.S. will increase.')]</value>
      <webElementGuid>01d8ffed-20d8-426d-ac8d-111af517044b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
