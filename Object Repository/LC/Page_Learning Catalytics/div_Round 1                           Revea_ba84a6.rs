<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Round 1                           Revea_ba84a6</name>
   <tag></tag>
   <elementGuidId>2afc9aba-e8a5-425b-8067-db6c2d61c103</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#results_84746</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='item_display']/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>441a5b4f-6e2d-465f-9328-c5a40e78e936</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>item_results</value>
      <webElementGuid>4cb4eab2-c8f7-4829-a387-a58ec4b3eb2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>results_84746</value>
      <webElementGuid>5b4d890f-95de-457b-9926-fe77a2aabedf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    












  Round 1
  
      
        
           Revealing correct answer to students (default)
           Without revealing correct answer to students
    
  
  
    
    0 responses, 0% correct
  
  

























  
    
      A. 0%
    
  
  
    
      B. 0%
    
  
  
    
      C. 0%
    
  
  
    
      D. 0%
    
  




  
    $(document).ready(function() {
      $('.response_tally[data-id=6119924]').html('0 responses, 0% correct');
      ears.lectures.updateResponseRate($('.response_rate[data-id=6119924]'), 0.0);
      if (ears.popupExists() &amp;&amp; typeof ears.currentPopup.$ != 'undefined' &amp;&amp; !$.browser.msie) {
        ears.lectures.updateResponseRate($(ears.currentPopup.$('#response_rate_chart')), 0.0);
      }
    })
  

  



    
         $(document).ready(function() {

            $('.show_specific_results').each(function() {
             var round = ( $(this).attr('data-pulldown-id'));
             if (round){
              var roundnum = round.replace('show_results_','');
              $(this).attr(&quot;aria-label&quot;, 'Reveal response type round ' + roundnum);
             }
            });
        });
    
</value>
      <webElementGuid>01fd77d1-e584-4f89-8465-ec1e5af674ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_84746&quot;)</value>
      <webElementGuid>c7c1d17c-b3aa-4a98-979e-c4b85d88eade</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='results_84746']</value>
      <webElementGuid>4fa4d325-ef3d-4885-a967-5c4ed3848e82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='item_display']/div/div[2]</value>
      <webElementGuid>069f9d1c-09cc-4142-94d9-0fd855f73e17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hide results'])[1]/following::div[1]</value>
      <webElementGuid>d7957f08-d821-4303-a7c4-d75827dfdd3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Without revealing correct answer to students'])[1]/following::div[1]</value>
      <webElementGuid>a2fb63ae-ac61-4f54-ac4e-05a6d057bdec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div[2]</value>
      <webElementGuid>cdb7f21c-f7a0-486a-a34c-b513ecc328a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'results_84746' and (text() = concat(&quot;
    












  Round 1
  
      
        
           Revealing correct answer to students (default)
           Without revealing correct answer to students
    
  
  
    
    0 responses, 0% correct
  
  

























  
    
      A. 0%
    
  
  
    
      B. 0%
    
  
  
    
      C. 0%
    
  
  
    
      D. 0%
    
  




  
    $(document).ready(function() {
      $(&quot; , &quot;'&quot; , &quot;.response_tally[data-id=6119924]&quot; , &quot;'&quot; , &quot;).html(&quot; , &quot;'&quot; , &quot;0 responses, 0% correct&quot; , &quot;'&quot; , &quot;);
      ears.lectures.updateResponseRate($(&quot; , &quot;'&quot; , &quot;.response_rate[data-id=6119924]&quot; , &quot;'&quot; , &quot;), 0.0);
      if (ears.popupExists() &amp;&amp; typeof ears.currentPopup.$ != &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; &amp;&amp; !$.browser.msie) {
        ears.lectures.updateResponseRate($(ears.currentPopup.$(&quot; , &quot;'&quot; , &quot;#response_rate_chart&quot; , &quot;'&quot; , &quot;)), 0.0);
      }
    })
  

  



    
         $(document).ready(function() {

            $(&quot; , &quot;'&quot; , &quot;.show_specific_results&quot; , &quot;'&quot; , &quot;).each(function() {
             var round = ( $(this).attr(&quot; , &quot;'&quot; , &quot;data-pulldown-id&quot; , &quot;'&quot; , &quot;));
             if (round){
              var roundnum = round.replace(&quot; , &quot;'&quot; , &quot;show_results_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
              $(this).attr(&quot;aria-label&quot;, &quot; , &quot;'&quot; , &quot;Reveal response type round &quot; , &quot;'&quot; , &quot; + roundnum);
             }
            });
        });
    
&quot;) or . = concat(&quot;
    












  Round 1
  
      
        
           Revealing correct answer to students (default)
           Without revealing correct answer to students
    
  
  
    
    0 responses, 0% correct
  
  

























  
    
      A. 0%
    
  
  
    
      B. 0%
    
  
  
    
      C. 0%
    
  
  
    
      D. 0%
    
  




  
    $(document).ready(function() {
      $(&quot; , &quot;'&quot; , &quot;.response_tally[data-id=6119924]&quot; , &quot;'&quot; , &quot;).html(&quot; , &quot;'&quot; , &quot;0 responses, 0% correct&quot; , &quot;'&quot; , &quot;);
      ears.lectures.updateResponseRate($(&quot; , &quot;'&quot; , &quot;.response_rate[data-id=6119924]&quot; , &quot;'&quot; , &quot;), 0.0);
      if (ears.popupExists() &amp;&amp; typeof ears.currentPopup.$ != &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; &amp;&amp; !$.browser.msie) {
        ears.lectures.updateResponseRate($(ears.currentPopup.$(&quot; , &quot;'&quot; , &quot;#response_rate_chart&quot; , &quot;'&quot; , &quot;)), 0.0);
      }
    })
  

  



    
         $(document).ready(function() {

            $(&quot; , &quot;'&quot; , &quot;.show_specific_results&quot; , &quot;'&quot; , &quot;).each(function() {
             var round = ( $(this).attr(&quot; , &quot;'&quot; , &quot;data-pulldown-id&quot; , &quot;'&quot; , &quot;));
             if (round){
              var roundnum = round.replace(&quot; , &quot;'&quot; , &quot;show_results_&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
              $(this).attr(&quot;aria-label&quot;, &quot; , &quot;'&quot; , &quot;Reveal response type round &quot; , &quot;'&quot; , &quot; + roundnum);
             }
            });
        });
    
&quot;))]</value>
      <webElementGuid>c91e7873-2b5b-4f40-9786-2549184fedbd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
