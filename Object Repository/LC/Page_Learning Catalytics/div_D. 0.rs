<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_D. 0</name>
   <tag></tag>
   <elementGuidId>dd270175-4f72-480c-98c3-b4bbee708d79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='results_for_round_6128581']/div/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>efe54855-4a0f-436c-8977-2542e497f914</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>line incorrect</value>
      <webElementGuid>8cd5c84f-0357-4390-aa78-240738c43fc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
      D. 0%
    
  </value>
      <webElementGuid>9ec7dba1-585b-4213-80db-6641c1cb8c64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_for_round_6128581&quot;)/div[@class=&quot;bar_graph&quot;]/div[@class=&quot;line incorrect&quot;]</value>
      <webElementGuid>4301524d-b068-490c-9ff9-35a4993fc0a2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='results_for_round_6128581']/div/div[4]</value>
      <webElementGuid>faddcd59-fc3f-4b17-aa12-583f25e8dac3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='multiple choice'])[1]/preceding::div[3]</value>
      <webElementGuid>28f80aeb-bf8c-422d-85f8-2dc81fc08271</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[4]</value>
      <webElementGuid>bb83516c-fa49-4d02-af15-0b2dc6c65ade</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
      D. 0%
    
  ' or . = '
    
      D. 0%
    
  ')]</value>
      <webElementGuid>bd7ee72c-4995-4b5f-80a4-29a86fed7cec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
