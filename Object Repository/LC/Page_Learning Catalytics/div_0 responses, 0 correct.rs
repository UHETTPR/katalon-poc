<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_0 responses, 0 correct</name>
   <tag></tag>
   <elementGuidId>29c19e2c-2cc8-4985-be5c-d45b4bd53473</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
    
    0 responses, 0% correct
  ' or . = '
    
    0 responses, 0% correct
  ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.response_stats</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5664a793-3778-404a-a829-7e3fb3e3562b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>response_stats</value>
      <webElementGuid>e4a711e7-5ca3-4269-867a-61084b78eedb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    0 responses, 0% correct
  </value>
      <webElementGuid>42878331-a1e8-437a-9a86-c15ce6d82a43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;results_wrapper_for_round_6119924&quot;)/div[@class=&quot;response_stats&quot;]</value>
      <webElementGuid>2b02751f-5c8d-46ba-84a5-1e29c949e264</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='results_wrapper_for_round_6119924']/div[2]</value>
      <webElementGuid>fa241b7f-77d9-4836-9b8f-f93bc636d411</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Without revealing correct answer to students'])[2]/following::div[1]</value>
      <webElementGuid>cf0ffbda-a639-4ce6-8c76-ac1f63678390</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(default)'])[2]/following::div[1]</value>
      <webElementGuid>2549d6b2-cb47-4f29-b56a-719ee94ca44b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div[2]</value>
      <webElementGuid>88076bbd-8b38-4649-9334-050def30f883</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    0 responses, 0% correct
  ' or . = '
    
    0 responses, 0% correct
  ')]</value>
      <webElementGuid>77b806be-a7f3-4f68-b83e-708b19018d2a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
