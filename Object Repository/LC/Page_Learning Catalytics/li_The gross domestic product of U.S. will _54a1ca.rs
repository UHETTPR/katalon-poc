<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_The gross domestic product of U.S. will _54a1ca</name>
   <tag></tag>
   <elementGuidId>0f098983-8734-4e71-b9a8-d19823837501</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='item_display']/div/div[4]/ol/li[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>900d4e7b-3b36-4b73-ac6b-22e85e523d59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>incorrect</value>
      <webElementGuid>6cf3dae6-61f7-4538-b160-dc758624a837</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    The gross domestic product of U.S. will remain unchanged.</value>
      <webElementGuid>9b612d16-b698-40aa-8185-bb7c35eb92e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item_display&quot;)/div[@class=&quot;item_view clearfix&quot;]/div[4]/ol[@class=&quot;lettered&quot;]/li[@class=&quot;incorrect&quot;]</value>
      <webElementGuid>374e4210-007e-47d0-95e5-c11c566710ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='item_display']/div/div[4]/ol/li[2]</value>
      <webElementGuid>c7a8a59a-46b9-4c9a-9232-65a7f2bbb246</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of U.S. will increase.'])[2]/following::li[1]</value>
      <webElementGuid>cf7bcb5c-0c8d-4e7c-b7cb-db82338c8986</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='multiple choice'])[1]/following::li[2]</value>
      <webElementGuid>ecb9dc60-ea1e-4f58-9c8c-fd2110dea9f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of India will increase.'])[2]/preceding::li[1]</value>
      <webElementGuid>3535c430-58db-4f83-a70c-1d9703d6f295</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/ol/li[2]</value>
      <webElementGuid>5b65ed7f-f6e9-4522-95af-a8ec74dfa9f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
    The gross domestic product of U.S. will remain unchanged.' or . = '
    The gross domestic product of U.S. will remain unchanged.')]</value>
      <webElementGuid>82bb72a2-f3fa-4f1d-a9f3-6e25066b35d1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
