<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_The gross domestic product of India will_6072ce</name>
   <tag></tag>
   <elementGuidId>9c8d3d7f-ee28-4d9a-852d-768b7b92f574</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='item_display']/div/div[4]/ol/li[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>6b9cf680-71fd-471b-be1a-cf69e88702f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>correct</value>
      <webElementGuid>4b58d18b-301e-416b-b253-128912ee326f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    The gross domestic product of India will increase.</value>
      <webElementGuid>a5f48141-6a9e-4ee7-a2a2-55e25bc4b17e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item_display&quot;)/div[@class=&quot;item_view clearfix&quot;]/div[4]/ol[@class=&quot;lettered&quot;]/li[@class=&quot;correct&quot;]</value>
      <webElementGuid>204aaedf-450e-4598-968c-df2e2eb64200</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='item_display']/div/div[4]/ol/li[3]</value>
      <webElementGuid>d30d8c2f-5035-4fcb-936a-e0c077fed553</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of U.S. will remain unchanged.'])[2]/following::li[1]</value>
      <webElementGuid>82eb94ad-4877-438d-b64a-8d9e9895e3f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The gross domestic product of U.S. will increase.'])[2]/following::li[2]</value>
      <webElementGuid>e98666eb-bb17-4534-8eb4-e56e72fbf09d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Answer'])[1]/preceding::li[2]</value>
      <webElementGuid>710aba26-d699-4ab4-925d-e3a225d6f1f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/ol/li[3]</value>
      <webElementGuid>59daa53c-ef72-4e4a-92e9-ed9ffe6beeec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
    The gross domestic product of India will increase.' or . = '
    The gross domestic product of India will increase.')]</value>
      <webElementGuid>91f58ebb-8086-4975-ade2-94b88901cf7c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
