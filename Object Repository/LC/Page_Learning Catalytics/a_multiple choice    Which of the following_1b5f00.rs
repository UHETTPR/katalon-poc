<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_multiple choice    Which of the following_1b5f00</name>
   <tag></tag>
   <elementGuidId>1710da8e-0dee-48da-b40f-93850910e07e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.item_search_results_result.prompt.tip.widetip.ql_indent > a.header_link</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='item_search_results']/tbody/tr/td/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d84cc199-a259-4540-805c-6682111ae043</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/courses/779503/modules/1800200/questions/84746</value>
      <webElementGuid>bd057a44-6b4f-4805-b570-b38221d7de81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header_link</value>
      <webElementGuid>4a4b40bb-fbe1-4803-a801-0e05b3a26a67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
   
  multiple choice
  
  













Which of the following will happen if an American tourist buys a pain...
    </value>
      <webElementGuid>1abc19b3-a5ec-4d91-a00f-41e0fd275e19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item_search_results&quot;)/tbody[1]/tr[@class=&quot;odd&quot;]/td[@class=&quot;sorting_1&quot;]/div[@class=&quot;item_search_results_result prompt tip widetip  ql_indent&quot;]/a[@class=&quot;header_link&quot;]</value>
      <webElementGuid>764d52ff-40b0-4e4a-8f8b-73eb2eb9e030</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='item_search_results']/tbody/tr/td/div/a</value>
      <webElementGuid>9336988b-030b-4398-ba38-979c8d713b66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question'])[1]/following::a[2]</value>
      <webElementGuid>5e0b8e01-195e-49ca-9afc-e11e5efd6ba3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Processing...'])[1]/following::a[2]</value>
      <webElementGuid>e30d2861-5dfb-4892-9214-2b4311c048a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/courses/779503/modules/1800200/questions/84746')])[2]</value>
      <webElementGuid>11ea7e83-7ac3-4312-9e6a-36a92239672b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/a</value>
      <webElementGuid>4ec25693-2aab-4cf1-86ac-dc66c5d3b4fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/courses/779503/modules/1800200/questions/84746' and (text() = '
   
  multiple choice
  
  













Which of the following will happen if an American tourist buys a pain...
    ' or . = '
   
  multiple choice
  
  













Which of the following will happen if an American tourist buys a pain...
    ')]</value>
      <webElementGuid>f5b0dc5a-f6bb-48b8-9a4e-7e57258408c8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
