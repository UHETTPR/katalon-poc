<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Stop session</name>
   <tag></tag>
   <elementGuidId>d8d6dbd7-83cb-4a40-a5ec-4ac207a970cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[text() = ' Stop session' or . = ' Stop session']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6c723bf5-3f98-4455-8565-f68337b348c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>stop-session-link button iconic_link  </value>
      <webElementGuid>c3073884-149c-4ed8-80fa-6ffa002f0aac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/courses/898476/modules/1800830/stop_session</value>
      <webElementGuid>0f9b3c9f-e1bd-460c-9f69-a05fbda35aa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Stop session</value>
      <webElementGuid>644d2ed9-3858-4a22-ab56-786af23bc5dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;toolbar&quot;)/a[@class=&quot;stop-session-link button iconic_link&quot;]</value>
      <webElementGuid>f66c489c-2c2d-416e-87a5-6d17ab713302</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='toolbar']/a</value>
      <webElementGuid>fd71f89a-be4a-4691-a141-7e9c5ebe864a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Testing LC Test 1'])[1]/following::a[1]</value>
      <webElementGuid>768df9a6-7dbc-464f-80c1-0dbd6df8ad76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EconLabTestCourse'])[1]/following::a[2]</value>
      <webElementGuid>5116dfab-e3b1-4750-8915-bf92d63bce95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/preceding::a[1]</value>
      <webElementGuid>85b8522f-491e-4e9b-9195-e8f631d6fa08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/courses/898476/modules/1800830/stop_session')]</value>
      <webElementGuid>abfd0799-6c95-46ff-8c2e-b9f9c0556ed7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/a</value>
      <webElementGuid>a8e07820-5568-40b0-b994-45372f5ad4ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/courses/898476/modules/1800830/stop_session' and (text() = ' Stop session' or . = ' Stop session')]</value>
      <webElementGuid>39317e58-faf5-4ea6-9388-de4573a9354b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
