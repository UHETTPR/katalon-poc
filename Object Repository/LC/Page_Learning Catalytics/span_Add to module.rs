<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add to module</name>
   <tag></tag>
   <elementGuidId>b0726128-7605-4e8a-afac-151b2bee933e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[(text() = 'Add to module' or . = 'Add to module')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8f1e73c0-3f5f-4859-86a8-b21b640158a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to module</value>
      <webElementGuid>263ace21-0068-48ee-a7af-e8171f192d6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;toolbar&quot;]/a[@class=&quot;iconic_link&quot;]/span[1]</value>
      <webElementGuid>65b4f6c8-4e79-4850-b12c-da907113d73f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[4]/a[2]/span</value>
      <webElementGuid>a93cdce0-41ad-4116-8ddf-f884c1b7ae21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search again'])[1]/following::span[1]</value>
      <webElementGuid>1f8e7069-d6d5-4c35-8c90-100bcc0caf92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 84746'])[1]/following::span[2]</value>
      <webElementGuid>16e33339-8849-4da0-80fd-787b7b7caf20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This question is provided by Pearson, © 2022.'])[1]/preceding::span[1]</value>
      <webElementGuid>02b6e05a-ae5b-4ed0-a28a-5adac0861390</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Historical Performance'])[1]/preceding::span[1]</value>
      <webElementGuid>7dc01d88-6068-4663-afb0-aa9b080f44d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add to module']/parent::*</value>
      <webElementGuid>af85e338-7e23-4fc7-8fe9-0bb77525ffad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]/span</value>
      <webElementGuid>cd20d193-3509-47e7-9b04-9a0b1f86ba8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Add to module' or . = 'Add to module')]</value>
      <webElementGuid>a6eae146-5439-4b75-8b06-4f568a8ff451</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
