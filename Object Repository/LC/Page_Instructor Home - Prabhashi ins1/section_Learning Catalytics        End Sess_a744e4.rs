<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Learning Catalytics        End Sess_a744e4</name>
   <tag></tag>
   <elementGuidId>e3bba227-8096-4c63-9fa3-93855cab17e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>section.well.lc-pod</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[(text() = '
     Learning Catalytics

    
    End Session in Progress
  ' or . = '
     Learning Catalytics

    
    End Session in Progress
  ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>c881f92b-e846-4398-98fa-4aed3ec044a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>well lc-pod</value>
      <webElementGuid>13890b93-b383-41af-9e56-26daf0d13757</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
     Learning Catalytics

    
    End Session in Progress
  </value>
      <webElementGuid>5552c13a-9148-4145-8e7d-ddc772c2e4ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/app-root[@class=&quot;pilot&quot;]/app-instructor-home[1]/div[@class=&quot;row&quot;]/app-lc-pod-tile[1]/div[@class=&quot;col-xs-12 col-md-6&quot;]/section[@class=&quot;well lc-pod&quot;]</value>
      <webElementGuid>abf6ebdd-02c8-4ebd-ab02-cad8c840df2b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/app-root/app-instructor-home/div/app-lc-pod-tile/div/section</value>
      <webElementGuid>ee1e879a-7fff-4b05-a891-d2c7689d619f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Catalytics: Real-time learning'])[1]/following::section[1]</value>
      <webElementGuid>2ea9a30b-a870-4b44-97cc-fe0a91983378</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook and assignment enhancements'])[1]/following::section[1]</value>
      <webElementGuid>20c1cd2e-5901-48a4-b934-a9b27c19d1a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-lc-pod-tile/div/section</value>
      <webElementGuid>c5ca96dc-2e1e-4fde-9624-b712b4f91944</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = '
     Learning Catalytics

    
    End Session in Progress
  ' or . = '
     Learning Catalytics

    
    End Session in Progress
  ')]</value>
      <webElementGuid>ee8bf875-3309-4c8b-8068-179f394ef783</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
