<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Instructor Home</name>
   <tag></tag>
   <elementGuidId>dbb52bed-3798-460f-baf3-03cba80d0291</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-6 > h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>b49207b4-a722-44c3-a160-3413f257fdf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Instructor Home</value>
      <webElementGuid>762f7393-962f-4980-8b3d-232af96242cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/header[1]/div[@class=&quot;pilot row&quot;]/div[@class=&quot;col-xs-6&quot;]/h1[1]</value>
      <webElementGuid>03e2e0a1-a375-458a-a005-33d5b606a3a4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/header/div[2]/div/h1</value>
      <webElementGuid>c0a899b8-3b23-46b4-bdf5-1a37ecf253e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Miller: Economics Today, 20e'])[1]/following::h1[1]</value>
      <webElementGuid>c81f8cb8-0026-430b-a14c-8836e30b8865</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prabhashi ins1'])[3]/following::h1[1]</value>
      <webElementGuid>230dd1bf-b51e-46c5-9b46-a4e9030af47e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Student Home'])[1]/preceding::h1[1]</value>
      <webElementGuid>217afc8b-3eb7-4b4c-bbd8-316f3c9a8137</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue to My Course'])[1]/preceding::h1[1]</value>
      <webElementGuid>6b8b3dca-ac02-4852-9b10-0744f4e4ee40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h1</value>
      <webElementGuid>b117b38a-ef4d-43b4-a8e2-90d8e0f36c18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      <webElementGuid>de0f0f80-80bf-4ea8-93cd-2cde55ba999e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
