<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create new module</name>
   <tag></tag>
   <elementGuidId>67931363-f4bc-47c0-b89a-717b434cab7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>section.well.lc-pod > ul.chevrons > li > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Create new module')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b70cc10d-f32c-4bcc-9a25-e744654508e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:ShowLearningCatalytics(&quot;createmodule&quot;)</value>
      <webElementGuid>0757cc8c-a150-4359-8924-dbc73c2a1ecc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create new module</value>
      <webElementGuid>48d0b068-a98e-4758-b23b-3694918e1603</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/app-root[@class=&quot;pilot&quot;]/app-instructor-home[1]/div[@class=&quot;row&quot;]/app-lc-pod-tile[1]/div[@class=&quot;col-xs-12 col-md-6&quot;]/section[@class=&quot;well lc-pod&quot;]/ul[@class=&quot;chevrons&quot;]/li[1]/a[1]</value>
      <webElementGuid>a1548185-7ecd-4089-9594-471190e0d34b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/app-root/app-instructor-home/div/app-lc-pod-tile/div/section/ul/li/a</value>
      <webElementGuid>08a0b9e0-ed1f-40a4-83d9-73849a529814</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create new module')]</value>
      <webElementGuid>ab67d097-79d8-41dd-ad3d-71279854ef50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Catalytics: Real-time learning'])[1]/following::a[1]</value>
      <webElementGuid>f7bd0c13-06a3-438b-94f4-cdc7c243c5a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start new session'])[1]/preceding::a[1]</value>
      <webElementGuid>57bde0b6-f2f5-4dce-bc4a-ffc8c2382071</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Review previous sessions'])[1]/preceding::a[2]</value>
      <webElementGuid>5a2c36a1-511c-413e-a995-8cb84d74ea8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create new module']/parent::*</value>
      <webElementGuid>4fe8e01e-bc16-4608-920f-2e0c991c94b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:ShowLearningCatalytics(&quot;createmodule&quot;)')]</value>
      <webElementGuid>d838973c-b82d-4c14-8278-24817ac39458</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-lc-pod-tile/div/section/ul/li/a</value>
      <webElementGuid>d96eb00a-b886-4a81-abee-cd3d2caa7897</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:ShowLearningCatalytics(&quot;createmodule&quot;)' and (text() = 'Create new module' or . = 'Create new module')]</value>
      <webElementGuid>41214c5f-9402-4b2e-b9b5-b4a4481006c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
