<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Learning Catalytics                _456321</name>
   <tag></tag>
   <elementGuidId>3236abd6-5cf4-4ff8-8200-fd80179e7b5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>section.well.lc-pod</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[(text() = '
     Learning Catalytics

    
      
        Create new module
      
        Start new session
      
        Review previous sessions
      
    
    
  ' or . = '
     Learning Catalytics

    
      
        Create new module
      
        Start new session
      
        Review previous sessions
      
    
    
  ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>24559ddf-102a-44bb-b657-395a4749f055</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>well lc-pod</value>
      <webElementGuid>8e35aaf9-75e8-45af-b05e-39a9992ef286</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
     Learning Catalytics

    
      
        Create new module
      
        Start new session
      
        Review previous sessions
      
    
    
  </value>
      <webElementGuid>d5a2308f-bb4b-4ac2-b71b-726f68351850</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/app-root[@class=&quot;pilot&quot;]/app-instructor-home[1]/div[@class=&quot;row&quot;]/app-lc-pod-tile[1]/div[@class=&quot;col-xs-12 col-md-6&quot;]/section[@class=&quot;well lc-pod&quot;]</value>
      <webElementGuid>2c61ccc0-7c14-469d-a462-8cc0d12af350</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/app-root/app-instructor-home/div/app-lc-pod-tile/div/section</value>
      <webElementGuid>41bc91dc-7c9b-4eb5-bda6-551c6e2c9187</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Catalytics: Real-time learning'])[1]/following::section[1]</value>
      <webElementGuid>ca5ecf08-559c-481b-a9a3-6d0674227717</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook and assignment enhancements'])[1]/following::section[1]</value>
      <webElementGuid>c4abcbb0-6631-4027-9b59-76e8f83c6b96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-lc-pod-tile/div/section</value>
      <webElementGuid>2b4a2995-4ee7-47fa-b58f-7ae6a89dc102</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = '
     Learning Catalytics

    
      
        Create new module
      
        Start new session
      
        Review previous sessions
      
    
    
  ' or . = '
     Learning Catalytics

    
      
        Create new module
      
        Start new session
      
        Review previous sessions
      
    
    
  ')]</value>
      <webElementGuid>d73890e1-6386-4377-924a-95d0d8980290</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
