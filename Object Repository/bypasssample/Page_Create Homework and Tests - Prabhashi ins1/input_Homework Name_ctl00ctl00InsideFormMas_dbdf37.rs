<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Homework Name_ctl00ctl00InsideFormMas_dbdf37</name>
   <tag></tag>
   <elementGuidId>7e8c8511-4e5c-47f6-af45-506e3a35c55e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_txtName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_txtName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c554f10f-696d-4135-9a29-e30b98a51800</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$txtName</value>
      <webElementGuid>c7ff775d-568b-4498-a63e-b930272e4a85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9e58175c-7029-4578-971f-f6c1c734f046</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>150</value>
      <webElementGuid>a3c385ef-93e5-4aa6-ae71-1ae832b0024e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_txtName</value>
      <webElementGuid>cddfc0f2-9238-4676-b587-8c4152c324e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>CheckAssignmentName();</value>
      <webElementGuid>c62407e5-6145-4648-94d3-a0870a578523</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>01346b71-284e-4cc1-8745-3425cb5e315f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_txtName&quot;)</value>
      <webElementGuid>fade2650-26b6-4a3f-84f9-b18d7c96139f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_txtName']</value>
      <webElementGuid>128359ab-2968-4753-80a7-f722b854048c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivPage1']/div/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input</value>
      <webElementGuid>aee975ee-be45-430f-a2a6-c3ae855602cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/input</value>
      <webElementGuid>73fa4a47-6e19-4d62-8be8-d2e20c375f9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ctl00$InsideForm$MasterContent$txtName' and @type = 'text' and @id = 'ctl00_ctl00_InsideForm_MasterContent_txtName']</value>
      <webElementGuid>e1200d19-de74-4019-ab59-8b6d1ee82840</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
