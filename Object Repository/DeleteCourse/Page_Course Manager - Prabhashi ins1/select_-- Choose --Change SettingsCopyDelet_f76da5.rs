<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose --Change SettingsCopyDelet_f76da5</name>
   <tag></tag>
   <elementGuidId>bb46b613-a79f-49bf-93bf-d5b4d2158f33</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='${GlobalVariable.CourseID}'])[1]/following::select[1] </value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68818</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>4fc26ce7-f6b3-40a5-acd0-8191ee67da2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$gridCourses$ctl04$da68818</value>
      <webElementGuid>ba2c11cd-b50e-4d33-bab6-6de4d1f6b101</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68818</value>
      <webElementGuid>7c58e3ca-0ea3-4cfb-a6eb-36885f77646d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>actionmenu</value>
      <webElementGuid>bb182ba8-752c-4ae4-90c5-c2a0662a71e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				--------------------------------
				Browser Check

			</value>
      <webElementGuid>b0be8da4-e08c-4888-aa27-d3fc03dff848</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68818&quot;)</value>
      <webElementGuid>14a1ceee-0412-4a59-9c25-33a9809db123</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68818']</value>
      <webElementGuid>69a9a0df-dae7-45af-bd7d-efda811d9219</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses']/tbody/tr[4]/td[9]/select</value>
      <webElementGuid>e861eb56-62b1-424b-b50b-791e4e2dc1b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ins1 (Primary)'])[2]/following::select[1]</value>
      <webElementGuid>eb7f1ded-886f-4968-8163-70243829f231</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL01-H13M-30L8-61X4'])[1]/following::select[1]</value>
      <webElementGuid>4b871317-cda8-4e36-b11c-1c5b4195686d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go'])[2]/preceding::select[1]</value>
      <webElementGuid>5ccde4ba-eaa3-4ad0-81fe-79fe20b91c30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SampleCourse'])[1]/preceding::select[1]</value>
      <webElementGuid>0b062a48-0d5d-4e32-b061-797ce1f67ee9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[9]/select</value>
      <webElementGuid>ed3d76c3-6b65-4c81-be50-3fa1720423de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$gridCourses$ctl04$da68818' and @id = 'ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68818' and (text() = '
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				--------------------------------
				Browser Check

			' or . = '
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				--------------------------------
				Browser Check

			')]</value>
      <webElementGuid>f6b52a4c-3c88-457a-801c-2e972de34b1c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
