<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_-- Choose --Change SettingsCopyDelete---_4cabf0</name>
   <tag></tag>
   <elementGuidId>c3fa9435-a089-44ac-bcb8-36370bacc890</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses']/tbody/tr[3]/td[9]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>78de5a2b-edb3-4fa1-8901-9c0c4a5fda3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Outside</value>
      <webElementGuid>8d064310-b9d1-4248-a593-58bea316b270</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>align</name>
      <type>Main</type>
      <value>right</value>
      <webElementGuid>c9de6100-43fa-4eb9-9a6d-cc2aadf2defd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>valign</name>
      <type>Main</type>
      <value>middle</value>
      <webElementGuid>74200114-3417-49ff-b7fe-8d99a56bae8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				--------------------------------
				Browser Check

			
                </value>
      <webElementGuid>80d0c871-bdab-4e8b-9139-670c90386bb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridCourses&quot;)/tbody[1]/tr[3]/td[@class=&quot;Outside&quot;]</value>
      <webElementGuid>00145aff-d649-438e-b2db-d325c46b09a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses']/tbody/tr[3]/td[9]</value>
      <webElementGuid>063c258b-adbe-4dfc-a1a8-fd851b338935</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ins1 (Primary)'])[1]/following::td[4]</value>
      <webElementGuid>78568fdf-327e-4905-abfb-6dc435bc06ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL01-H12N-00L8-11X4'])[1]/following::td[5]</value>
      <webElementGuid>cee0e7b2-2092-422d-af1b-41734cf45bef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go'])[1]/preceding::td[1]</value>
      <webElementGuid>e352ea5a-567c-4381-ad9c-43440021c71a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GlobalTestCourse'])[3]/preceding::td[4]</value>
      <webElementGuid>be90d7c8-3403-4917-a482-67d4dd29d494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[9]</value>
      <webElementGuid>2423215c-3eb3-4bee-b5d2-1d8fe4170ec9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                        
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				--------------------------------
				Browser Check

			
                ' or . = '
                        
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				--------------------------------
				Browser Check

			
                ')]</value>
      <webElementGuid>1f6a7d98-13b2-4ac2-bca5-124bc39cffdf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
