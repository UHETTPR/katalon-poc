<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_XL01-H12N-00L8-11X4</name>
   <tag></tag>
   <elementGuidId>4df5e61a-004b-414f-a19d-24abbff8c520</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses']/tbody/tr[3]/td[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>8c7eb222-9b64-427e-b752-a6b71e3d1dc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>valign</name>
      <type>Main</type>
      <value>top</value>
      <webElementGuid>3ded6064-29f2-49c7-9255-072100e96bd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    XL01-H12N-00L8-11X4
                </value>
      <webElementGuid>aaba1ca1-df25-4988-8dcd-a72c1dac7bb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridCourses&quot;)/tbody[1]/tr[3]/td[4]</value>
      <webElementGuid>4c6a67b7-21cd-4a85-bdef-1b86faecc808</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses']/tbody/tr[3]/td[4]</value>
      <webElementGuid>84e54410-fa12-4036-819c-1c28a5ecd184</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Current Courses'])[1]/following::td[4]</value>
      <webElementGuid>0212e3c3-b4c9-4c03-a8a1-916aaae20697</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/following::td[6]</value>
      <webElementGuid>fe515195-32de-4590-b48e-f57fa8cc2caa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ins1 (Primary)'])[1]/preceding::td[1]</value>
      <webElementGuid>1ea3f046-101a-45f2-9b3c-f2602a982b5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[4]</value>
      <webElementGuid>16e08aae-cf07-417f-a091-5618b63abcb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                    XL01-H12N-00L8-11X4
                ' or . = '
                    XL01-H12N-00L8-11X4
                ')]</value>
      <webElementGuid>0588342e-66f9-41dd-8591-bcf9db6299ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
