<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --  Adams Calculus, 6_c08406</name>
   <tag></tag>
   <elementGuidId>9c187268-af8c-4506-a55b-ed6ea09abf3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>870b50de-2882-492f-8046-b2ed65e0a082</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
      <webElementGuid>b04236f6-82e7-4841-b018-3705ce536d31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      <webElementGuid>85445238-d642-43e9-b8c9-fee056f307e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
      <webElementGuid>2e4e1cbf-5391-46b9-8d51-adc43518a5f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>f39480f0-5548-47ca-b383-2bf8bb2a9132</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				  -- Choose Book --  
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 6e ENHANCED
				ADP: Algebra II Online Course
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Akst: Basic Mathematics through Applications, 3e
				Akst: Fundamental Mathematics through Applications, 3e
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Intermediate Algebra through Applications
				Akst: Intermediate Algebra Through Applications, 2e
				Akst: Introductory &amp; Intermediate Algebra, 2e
				Akst: Introductory Algebra through Applications
				Akst: Introductory Algebra Through Applications, 2e
				Alec's Delta Book
				Alec's Delta Book:  Non-combo
				Algebra Review for Calculus
				Alicia's SBNet feature testing book
				Angel: A Survey of Mathematics with Applications, 7e
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, 9e
				Angel: A Survey of Mathematics with Applications, Expanded 7e
				Angel: A Survey of Mathematics with Applications, Expanded 8e
				Angel: Algebra for College Students, 3e
				Angel: Elementary Algebra for College Students Early Graphing, 3e
				Angel: Elementary Algebra for College Students, 7e
				Angel: Elementary and Intermediate Algebra for College Students, 3e
				Angel: Intermediate Algebra for College Students, 7e
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Austin CC: Basic College Math
				Automated Preload Master Course book
				Baker College Math 091: Basic College Mathematics
				Baker College: Basic College Mathematics (Tobey)
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Econ, Social &amp; Life Sciences, 10e
				Barnett: College Mathematics, 10e
				Barnett: College Mathematics, 11e
				Barnett: Finite Mathematics, 10e
				Barnett: Finite Mathematics, 11e
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Beecher: Algebra &amp; Trigonometry, 2e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 2e
				Beecher: College Algebra, 3e
				Beecher: Precalculus, 2e
				Beecher: Precalculus, 3e
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 3e
				Bennett: Using and Understanding Mathematics, 3e
				Bennett: Using and Understanding Mathematics, 4e
				Berenson: Basic Business Statistics, 11e
				Berenson: Basic Business Statistics, Australian Edition
				Berenson: Business Statistics, Australian Edition
				Bhead test book
				Billstein: Mathematics for Elementary School Teachers, 9e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 4e
				Bittinger: Basic College Mathematics, 12e
				Bittinger: Basic Mathematics with Early Integers
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 9e
				Bittinger: Calculus and Its Applications, 8e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: College Algebra: Graphs &amp; Models, 3e
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 6e
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 3e
				Bittinger: Elementary &amp; Intermediate Algebra: Graphs &amp; Models, 2e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra: Graphs &amp; Models
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3e
				Bittinger: Fundamental Mathematics, 4e
				Bittinger: Intermediate Algebra, 10e
				Bittinger: Intermediate Algebra, 9e
				Bittinger: Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 9e
				Bittinger: Introductory and Intermediate Algebra, 2e
				Bittinger: Introductory and Intermediate Algebra, 3e
				Bittinger: Prealgebra &amp; Introductory Algebra, 2e
				Bittinger: Prealgebra and Introductory Algebra
				Bittinger: Prealgebra, 4e
				Bittinger: Prealgebra, 5e
				Bittinger: Precalculus: Graphs &amp; Models, 3e
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Blair/Tobey/Slater: Prealgebra, 3e
				Blair: Intermediate Algebra
				Blair: Introductory Algebra
				Blitzer:  College Algebra, 3e
				Blitzer: Algebra &amp; Trigonometry, 2e
				Blitzer: Algebra &amp; Trigonometry, 3e
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach
				Blitzer: Algebra for College Students, 5e
				Blitzer: Algebra for College Students, 6e
				Blitzer: Algebra for College Students, 7e
				Blitzer: College Algebra Essentials, 2e
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra: An Early Functions Approach
				Blitzer: Essentials of Intermediate Algebra for College Students
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra
				Blitzer: Intermediate Algebra for College Students, 4e
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 3e
				Blitzer: Introductory Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 5e
				Blitzer: Introductory and Intermediate Algebra for College Students, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Precalculus Essentials, 2e
				Blitzer: Precalculus, 2e
				Blitzer: Precalculus, 3e
				Blitzer: Thinking Mathematically, 3e
				Blitzer: Thinking Mathematically, 4e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 3e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bock: Stats: Modeling the World
				Bock: Stats: Modeling the World, 2e
				Briggs/Cochran: Calculus, 2e
				California Algebra Readiness
				Campbell Biology: Concepts &amp; Connections, 9/e
				Carson: Elementary Algebra
				Carson: Elementary Algebra with Early Systems of Equations
				Carson: Elementary Algebra, 2e
				Carson: Elementary and Intermediate Algebra
				Carson: Elementary and Intermediate Algebra, 2e ENHANCED
				Carson: Intermediate Algebra
				Carson: Intermediate Algebra, 2e
				Carson: Prealgebra, 2e
				Carson: Prealgebra, 3e
				Chemistry: The Central Science, 14/e
				Chris T3 Test Feb
				Chroniques du Québec et du Canada, Des origines à 1840
				Chroniques-CSG Testing
				Cite foot note test Book
				Clark: Applied Basic Mathematics
				Cleaves:  Business Math, 8e
				Cleaves: Business Math, 8e Brief Edition
				Cleaves: Business Math, 8e DEMO
				Cleaves: College Mathematics 2009 Update with MyMathLab
				Cleaves: College Mathematics, 10e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math In Action: Intro to Algebraic, Graph, and Num Prob Solv, 3/e
				Consortium: Math In Action: Prealgebra Problem Solving, 2e
				Contemporary Business Mathematics with Canadian Applications, 11/e
				Croft: Foundation Maths, 4e
				Croft: Mathematics for Engineers, 3e
				CSU: ELM Exam Prep Course, version 2
				CUSTOM MML- Univ of Arkansas-Fayetteville MATH 2053: Finite Mathematics
				De Veaux: Intro Stats
				De Veaux: Intro Stats, 2e
				De Veaux: Intro Stats, 3e
				De Veaux: Stats: Data and Models
				De Veaux: Stats: Data and Models, 2e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				DeVry MAT190 (Washington)
				DNT: Beginning and Intermediate Algebra
				DNT: Beginning and Intermediate Algebra_Test
				Donnelly: Business Statistics, 2e
				Dugopolski: College Algebra &amp; Trigonometry, 4e
				Dugopolski: College Algebra, 4e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus, 4e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 3e
				Dugopolski: Trigonometry, 2e
				Early Childhood Education Today, 14/e
				ECPI MTH099: Beginning Algebra
				ECPI MTH115: Thinking Mathematically
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics
				EEK Trigsted College Algebra 2e IRA TEST
				eT1 Math Test Book
				eT1 Math Test Book - PPE
				eT1 Test Book
				eT2 CITE + Print Test Book - STG/PPE
				eT2 CITE Test Book - STG/PPE
				eT2 Math Test Book - PPE
				eT2 Math Test Book BXWAT0VP1H9
				eT2 Math Test RH - BXWAT0VP1H9
				eT2 PXE Math Test Book - PPE
				eT2 Staging - FXVIU9IDRCS
				Etext2 check 1 ppe
				ETWEB-7558_PXE Test Book
				Financial Management: Principles and Applications, 13/e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e
				Florida International University: Finite Math 1060
				Georgia Math IV  (Demana and Agresti)
				Georgia Math IV  (Sullivan and Agresti)
				Goldstein: Brief Calculus and Its Applications, 11e
				Goldstein: Calculus and Its Applications, 11e
				Goldstein: Finite Mathematics and Its Applications, 9e
				Goshaw: Concepts of Calculus with Applications
				Harshbarger: College Algebra in Context
				Harshbarger: College Algebra In Context, 2e
				Hass: University Calculus
				Hass: University Calculus Alternate Edition
				Hass: University Calculus Elements
				Hillsborough CC: Thomas' Calculus, 11e with Precalculus
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 4e
				Hornsby: A Graphical Approach to College Algebra, 4e
				Hornsby: A Graphical Approach to Precalculus with Limits, A Unit Circle App, 4e
				Hornsby: A Graphical Approach to Precalculus, 4e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Intellipro Test Book - StatCrunch
				Jacques: Mathematics for Economics and Business, 6e
				John Jay: Thinking Mathematically with Algebra (Blitzer)
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 4e
				Kaplan Math 103: College Mathematics
				Kaplan MM150 (Angel: Survey of Math)
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math test book
				Knewton Math test book2
				Knewton tiny test book-Ishara
				KU122: Introduction to Math Skills and Strategies
				Larson: Elementary Statistics: Picturing the World, 3e
				Larson: Elementary Statistics: Picturing the World, 4e
				Lay: Linear Algebra and Its Applications, 3e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications
				Lehmann: Intermediate Algebra: Functions and Authentic Applications, 3e
				Levine: Business Statistics: A First Course, 5e
				Lial: Algebra for College Students, 5e
				Lial: Algebra for College Students, 6e
				Lial: Basic College Mathematics, 7e
				Lial: Basic College Mathematics, 8e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning Algebra, 10e (WOW)
				Lial: Beginning Algebra, 9e
				Lial: Beginning and Intermediate Algebra, 3e
				Lial: Calculus with Applications, 7e
				Lial: Calculus with Applications, 8e
				Lial: Calculus with Applications, 9e
				Lial: Calculus with Applications, Brief Version 9e
				Lial: Calculus with Applications, Brief Version, 8e
				Lial: College Algebra and Trigonometry, 3e
				Lial: College Algebra and Trigonometry, 4e
				Lial: College Algebra, 10e
				Lial: College Algebra, 9e
				Lial: Developmental Mathematics, 4e PPE book
				Lial: Essential Mathematics, 2e
				Lial: Essentials of College Algebra
				Lial: Essentials of College Algebra Alternate Edition
				Lial: Finite Mathematics &amp; Calculus with Applications, 8e
				Lial: Finite Mathematics and Calculus with Applications, 7e
				Lial: Finite Mathematics with Applications, 9e
				Lial: Finite Mathematics, 7e
				Lial: Finite Mathematics, 8e
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 9e
				Lial: Introductory Algebra, 8e
				Lial: Introductory and Intermediate Algebra, 3e
				Lial: Mathematics with Applications, 9e
				Lial: Prealgebra and Introductory Algebra, 2e
				Lial: Prealgebra, 3e
				Lial: Prealgebra: An Integrated Approach, 1e Hardback
				Lial: Precalculus, 3e
				Lial: Precalculus, 4e
				Lial: Precalculus, 6e
				Lial: Trigonometry, 8e
				Lial: Trigonometry, 9e
				Lockdown Browser test PPE
				Long: Mathematical Reasoning for Elementary Teachers, 4e
				Long: Mathematical Reasoning for Elementary Teachers, 5e
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra: A Combined Approach
				Martin-Gay: Algebra: A Combined Approach, 3e
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics, 2e
				Martin-Gay: Basic College Mathematics, 3e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Developmental Mathematics
				Martin-Gay: Intermediate Algebra, 2e
				Martin-Gay: Intermediate Algebra, 3e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 4e
				Martin-Gay: Introductory Algebra, 2e
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra and Introductory Algebra
				Martin-Gay: Prealgebra, 4e
				Martin-Gay: Prealgebra, 5e
				Mathspace Test Book
				McClave:  Statistics for Business and Economics, 10e
				McClave: A First Course in Statistics, 10e
				McClave: Statistics, 10e
				McClave: Statistics, 11e
				McKenna/Kirk: Beginning &amp; Intermediate Algebra, 1e DEMO
				Miller: Business Mathematics, 10e
				Miller: Business Mathematics, 11e
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 10e
				Miller: Mathematical Ideas, Expanded 10e
				MXL Player Content Test Book
				MyMathLab test book A3 - Lilani 6/15
				MyMathLab test book B (from Briggs Calc)
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				NextGen Media Math PPE testbookDeletion
				NextGen Media Math Test Book--CITE
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL-FGPKCV9VIE3
				NextGen Media Math Test Book--REAL-H26F1QTODXL
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--REALFH3613LY014
				Nutrition for Life
				Nutrition: From Science to You
				O'Daffer: Mathematics for Elementary Teachers, 3e
				O'Daffer: Mathematics for Elementary Teachers, 4e
				Orientation Questions for Students
				Pirnot: Mathematics All Around, 3e
				Player Functional Testing book
				Player Testing--XL Player only
				PPE: QA uPDF DNT:
				PPE: QA uPDF DNT: MyMathLab for Developmental Mathematics: Basic Mathematics, B
				Prior: Basic Mathematics, 1e
				Queensborough CC Math 120: Algebra with Trigonometry for College Students, 2e
				Queensborough CC: Intermediate Algebra with Trigonometry (Blitzer)
				Ratti: College Algebra
				Ratti: College Algebra and Trigonometry
				Ratti: Precalculus
				Riverside CC custom course
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 3e
				Rockswold: Beginning Algebra with Applications &amp; Visualization
				Rockswold: Beginning Algebra with Apps and Visualization, 2e
				Rockswold: Beginning and Intermediate Algebra
				Rockswold: Beginning and Intermediate Algebra, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e
				Rockswold: College Algebra, 4e
				Rockswold: Essentials of College Algebra with Modeling and Visualization, 3e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 3e
				Rockswold: Precalculus with Modeling and Visualization, 3e
				Salzman: Mathematics for Business, 8e
				Sam_eT2_Test book
				Sharpe: Business Statistics
				Sharpe: Business Statistics Preview
				Sharpe: Business Statistics, 4e
				SI Notation testing (from sbtest)
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				SPPTesting
				StaCrunch Test Book 01
				StatCrunch Test Book for PPE
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data,1e copy
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 4e
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 5e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 7e
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: College Algebra w/ Graphing Utilities, 4e
				Sullivan: College Algebra with Graphing Utilities, 5e
				Sullivan: College Algebra, 7e
				Sullivan: College Algebra, 8e
				Sullivan: College Algebra: Concepts through Functions
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary Algebra
				Sullivan: Fundamentals of Statistics, 2e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Intermediate Algebra
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Precalculus w/ Graphing Utilities, 4e
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 7e
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle
				Sullivan: Precalculus: Concepts through Functions, Unit Circle
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 3e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5/e, Global Edition
				Sullivan: Trigonometry, 7e
				Sullivan: Trigonometry: A Right Triangle Approach, 5e
				Sullivan: Trigonometry: A Unit Circle Approach, 8e
				Test :Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data1e
				test3506_std
				test3506_withBrowser
				Thomas' Calc demo
				Thomas' Calculus 11e DEMO RETIRED
				Thomas' Calculus Early Transcendentals Media Upgrade, 11e
				Thomas' Calculus Media Upgrade, 11e
				Thomasson: Introductory &amp; Intermediate Algebra, 3e
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 5e
				Tobey: Basic College Mathematics, 6e
				Tobey: Basic College Mathematics, 8e
				Tobey: Beginning &amp; Intermediate Algebra, 2e
				Tobey: Beginning Algebra, 6e
				Tobey: Beginning Algebra: Early Graphing
				Tobey: Essentials of Basic College Mathematics
				Tobey: Essentials of Basic Collge Mathematics, 2e
				Tobey: Intermediate Algebra, 5e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trigsted: College Algebra 1e
				Trigsted: College Algebra 1e Chapter 5 DEMO
				Trigsted: College Algebra 1e DEMO
				Trigsted: College Algebra Interactive
				Trim: Calculus for Engineers, 4e
				Triola: Elementary Statistics Using Excel, 2e
				Triola: Elementary Statistics Using Excel, 3e
				Triola: Elementary Statistics Using the Graphing Calculator
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 2e
				Triola: Elementary Statistics, 10e
				Triola: Elementary Statistics, 11e
				Triola: Elementary Statistics, 9e
				Triola: Essentials of Statistics, 2e
				Triola: Essentials of Statistics, 3e
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				UCF: The Finite Companion 6e Revised (Angel)
				UDM: Intermediate Algebra: A STEM Approach, 1/e
				Univ of Florida: Precalculus 1140/1147/1114
				Universal Domain Graph Diagnostic Exercises
				Varberg: Calculus Early Transcendentals
				Varberg: Calculus, 9e
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics with Calculus, 9e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 6e
				Weiss: Elementary Statistics, 7e
				Weiss: Introductory Statistics, 7e
				Weiss: Introductory Statistics, 8e
				Woodbury: Elementary &amp; Intermediate Algebra
				Woodbury: Elementary &amp; Intermediate Algebra, 2e
				Woodbury: Elementary Algebra
				Woodbury: Intermediate Algebra
				XL Load Testbook
				Young: Finite Mathematics, 3e
				z MathXL Freelancer Test Book

			</value>
      <webElementGuid>77bdd1f0-82ea-4923-9605-060aa37eb018</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
      <webElementGuid>7ab7871f-ee05-432b-879d-38c703b71647</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      <webElementGuid>1082a2f2-a509-4a8a-8e4c-ff504a51ed84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
      <webElementGuid>4a2e51df-db6a-4697-b5d3-84161a2dd3d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
      <webElementGuid>6d51d4a2-c3e5-4b83-92fd-788b895cbdc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
      <webElementGuid>4d5036b1-acaf-410a-b5a3-f0c306522595</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>f43315c6-027f-4b6e-bbaa-7dcd61881233</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				  -- Choose Book --  
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 6e ENHANCED
				ADP: Algebra II Online Course
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Akst: Basic Mathematics through Applications, 3e
				Akst: Fundamental Mathematics through Applications, 3e
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Intermediate Algebra through Applications
				Akst: Intermediate Algebra Through Applications, 2e
				Akst: Introductory &amp; Intermediate Algebra, 2e
				Akst: Introductory Algebra through Applications
				Akst: Introductory Algebra Through Applications, 2e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book:  Non-combo
				Algebra Review for Calculus
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				Angel: A Survey of Mathematics with Applications, 7e
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, 9e
				Angel: A Survey of Mathematics with Applications, Expanded 7e
				Angel: A Survey of Mathematics with Applications, Expanded 8e
				Angel: Algebra for College Students, 3e
				Angel: Elementary Algebra for College Students Early Graphing, 3e
				Angel: Elementary Algebra for College Students, 7e
				Angel: Elementary and Intermediate Algebra for College Students, 3e
				Angel: Intermediate Algebra for College Students, 7e
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Austin CC: Basic College Math
				Automated Preload Master Course book
				Baker College Math 091: Basic College Mathematics
				Baker College: Basic College Mathematics (Tobey)
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Econ, Social &amp; Life Sciences, 10e
				Barnett: College Mathematics, 10e
				Barnett: College Mathematics, 11e
				Barnett: Finite Mathematics, 10e
				Barnett: Finite Mathematics, 11e
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Beecher: Algebra &amp; Trigonometry, 2e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 2e
				Beecher: College Algebra, 3e
				Beecher: Precalculus, 2e
				Beecher: Precalculus, 3e
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 3e
				Bennett: Using and Understanding Mathematics, 3e
				Bennett: Using and Understanding Mathematics, 4e
				Berenson: Basic Business Statistics, 11e
				Berenson: Basic Business Statistics, Australian Edition
				Berenson: Business Statistics, Australian Edition
				Bhead test book
				Billstein: Mathematics for Elementary School Teachers, 9e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 4e
				Bittinger: Basic College Mathematics, 12e
				Bittinger: Basic Mathematics with Early Integers
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 9e
				Bittinger: Calculus and Its Applications, 8e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: College Algebra: Graphs &amp; Models, 3e
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 6e
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 3e
				Bittinger: Elementary &amp; Intermediate Algebra: Graphs &amp; Models, 2e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra: Graphs &amp; Models
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3e
				Bittinger: Fundamental Mathematics, 4e
				Bittinger: Intermediate Algebra, 10e
				Bittinger: Intermediate Algebra, 9e
				Bittinger: Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 9e
				Bittinger: Introductory and Intermediate Algebra, 2e
				Bittinger: Introductory and Intermediate Algebra, 3e
				Bittinger: Prealgebra &amp; Introductory Algebra, 2e
				Bittinger: Prealgebra and Introductory Algebra
				Bittinger: Prealgebra, 4e
				Bittinger: Prealgebra, 5e
				Bittinger: Precalculus: Graphs &amp; Models, 3e
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Blair/Tobey/Slater: Prealgebra, 3e
				Blair: Intermediate Algebra
				Blair: Introductory Algebra
				Blitzer:  College Algebra, 3e
				Blitzer: Algebra &amp; Trigonometry, 2e
				Blitzer: Algebra &amp; Trigonometry, 3e
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach
				Blitzer: Algebra for College Students, 5e
				Blitzer: Algebra for College Students, 6e
				Blitzer: Algebra for College Students, 7e
				Blitzer: College Algebra Essentials, 2e
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra: An Early Functions Approach
				Blitzer: Essentials of Intermediate Algebra for College Students
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra
				Blitzer: Intermediate Algebra for College Students, 4e
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 3e
				Blitzer: Introductory Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 5e
				Blitzer: Introductory and Intermediate Algebra for College Students, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Precalculus Essentials, 2e
				Blitzer: Precalculus, 2e
				Blitzer: Precalculus, 3e
				Blitzer: Thinking Mathematically, 3e
				Blitzer: Thinking Mathematically, 4e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 3e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bock: Stats: Modeling the World
				Bock: Stats: Modeling the World, 2e
				Briggs/Cochran: Calculus, 2e
				California Algebra Readiness
				Campbell Biology: Concepts &amp; Connections, 9/e
				Carson: Elementary Algebra
				Carson: Elementary Algebra with Early Systems of Equations
				Carson: Elementary Algebra, 2e
				Carson: Elementary and Intermediate Algebra
				Carson: Elementary and Intermediate Algebra, 2e ENHANCED
				Carson: Intermediate Algebra
				Carson: Intermediate Algebra, 2e
				Carson: Prealgebra, 2e
				Carson: Prealgebra, 3e
				Chemistry: The Central Science, 14/e
				Chris T3 Test Feb
				Chroniques du Québec et du Canada, Des origines à 1840
				Chroniques-CSG Testing
				Cite foot note test Book
				Clark: Applied Basic Mathematics
				Cleaves:  Business Math, 8e
				Cleaves: Business Math, 8e Brief Edition
				Cleaves: Business Math, 8e DEMO
				Cleaves: College Mathematics 2009 Update with MyMathLab
				Cleaves: College Mathematics, 10e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math In Action: Intro to Algebraic, Graph, and Num Prob Solv, 3/e
				Consortium: Math In Action: Prealgebra Problem Solving, 2e
				Contemporary Business Mathematics with Canadian Applications, 11/e
				Croft: Foundation Maths, 4e
				Croft: Mathematics for Engineers, 3e
				CSU: ELM Exam Prep Course, version 2
				CUSTOM MML- Univ of Arkansas-Fayetteville MATH 2053: Finite Mathematics
				De Veaux: Intro Stats
				De Veaux: Intro Stats, 2e
				De Veaux: Intro Stats, 3e
				De Veaux: Stats: Data and Models
				De Veaux: Stats: Data and Models, 2e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				DeVry MAT190 (Washington)
				DNT: Beginning and Intermediate Algebra
				DNT: Beginning and Intermediate Algebra_Test
				Donnelly: Business Statistics, 2e
				Dugopolski: College Algebra &amp; Trigonometry, 4e
				Dugopolski: College Algebra, 4e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus, 4e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 3e
				Dugopolski: Trigonometry, 2e
				Early Childhood Education Today, 14/e
				ECPI MTH099: Beginning Algebra
				ECPI MTH115: Thinking Mathematically
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics
				EEK Trigsted College Algebra 2e IRA TEST
				eT1 Math Test Book
				eT1 Math Test Book - PPE
				eT1 Test Book
				eT2 CITE + Print Test Book - STG/PPE
				eT2 CITE Test Book - STG/PPE
				eT2 Math Test Book - PPE
				eT2 Math Test Book BXWAT0VP1H9
				eT2 Math Test RH - BXWAT0VP1H9
				eT2 PXE Math Test Book - PPE
				eT2 Staging - FXVIU9IDRCS
				Etext2 check 1 ppe
				ETWEB-7558_PXE Test Book
				Financial Management: Principles and Applications, 13/e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e
				Florida International University: Finite Math 1060
				Georgia Math IV  (Demana and Agresti)
				Georgia Math IV  (Sullivan and Agresti)
				Goldstein: Brief Calculus and Its Applications, 11e
				Goldstein: Calculus and Its Applications, 11e
				Goldstein: Finite Mathematics and Its Applications, 9e
				Goshaw: Concepts of Calculus with Applications
				Harshbarger: College Algebra in Context
				Harshbarger: College Algebra In Context, 2e
				Hass: University Calculus
				Hass: University Calculus Alternate Edition
				Hass: University Calculus Elements
				Hillsborough CC: Thomas&quot; , &quot;'&quot; , &quot; Calculus, 11e with Precalculus
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 4e
				Hornsby: A Graphical Approach to College Algebra, 4e
				Hornsby: A Graphical Approach to Precalculus with Limits, A Unit Circle App, 4e
				Hornsby: A Graphical Approach to Precalculus, 4e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Intellipro Test Book - StatCrunch
				Jacques: Mathematics for Economics and Business, 6e
				John Jay: Thinking Mathematically with Algebra (Blitzer)
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 4e
				Kaplan Math 103: College Mathematics
				Kaplan MM150 (Angel: Survey of Math)
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math test book
				Knewton Math test book2
				Knewton tiny test book-Ishara
				KU122: Introduction to Math Skills and Strategies
				Larson: Elementary Statistics: Picturing the World, 3e
				Larson: Elementary Statistics: Picturing the World, 4e
				Lay: Linear Algebra and Its Applications, 3e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications
				Lehmann: Intermediate Algebra: Functions and Authentic Applications, 3e
				Levine: Business Statistics: A First Course, 5e
				Lial: Algebra for College Students, 5e
				Lial: Algebra for College Students, 6e
				Lial: Basic College Mathematics, 7e
				Lial: Basic College Mathematics, 8e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning Algebra, 10e (WOW)
				Lial: Beginning Algebra, 9e
				Lial: Beginning and Intermediate Algebra, 3e
				Lial: Calculus with Applications, 7e
				Lial: Calculus with Applications, 8e
				Lial: Calculus with Applications, 9e
				Lial: Calculus with Applications, Brief Version 9e
				Lial: Calculus with Applications, Brief Version, 8e
				Lial: College Algebra and Trigonometry, 3e
				Lial: College Algebra and Trigonometry, 4e
				Lial: College Algebra, 10e
				Lial: College Algebra, 9e
				Lial: Developmental Mathematics, 4e PPE book
				Lial: Essential Mathematics, 2e
				Lial: Essentials of College Algebra
				Lial: Essentials of College Algebra Alternate Edition
				Lial: Finite Mathematics &amp; Calculus with Applications, 8e
				Lial: Finite Mathematics and Calculus with Applications, 7e
				Lial: Finite Mathematics with Applications, 9e
				Lial: Finite Mathematics, 7e
				Lial: Finite Mathematics, 8e
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 9e
				Lial: Introductory Algebra, 8e
				Lial: Introductory and Intermediate Algebra, 3e
				Lial: Mathematics with Applications, 9e
				Lial: Prealgebra and Introductory Algebra, 2e
				Lial: Prealgebra, 3e
				Lial: Prealgebra: An Integrated Approach, 1e Hardback
				Lial: Precalculus, 3e
				Lial: Precalculus, 4e
				Lial: Precalculus, 6e
				Lial: Trigonometry, 8e
				Lial: Trigonometry, 9e
				Lockdown Browser test PPE
				Long: Mathematical Reasoning for Elementary Teachers, 4e
				Long: Mathematical Reasoning for Elementary Teachers, 5e
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra: A Combined Approach
				Martin-Gay: Algebra: A Combined Approach, 3e
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics, 2e
				Martin-Gay: Basic College Mathematics, 3e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Developmental Mathematics
				Martin-Gay: Intermediate Algebra, 2e
				Martin-Gay: Intermediate Algebra, 3e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 4e
				Martin-Gay: Introductory Algebra, 2e
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra and Introductory Algebra
				Martin-Gay: Prealgebra, 4e
				Martin-Gay: Prealgebra, 5e
				Mathspace Test Book
				McClave:  Statistics for Business and Economics, 10e
				McClave: A First Course in Statistics, 10e
				McClave: Statistics, 10e
				McClave: Statistics, 11e
				McKenna/Kirk: Beginning &amp; Intermediate Algebra, 1e DEMO
				Miller: Business Mathematics, 10e
				Miller: Business Mathematics, 11e
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 10e
				Miller: Mathematical Ideas, Expanded 10e
				MXL Player Content Test Book
				MyMathLab test book A3 - Lilani 6/15
				MyMathLab test book B (from Briggs Calc)
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				NextGen Media Math PPE testbookDeletion
				NextGen Media Math Test Book--CITE
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL-FGPKCV9VIE3
				NextGen Media Math Test Book--REAL-H26F1QTODXL
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--REALFH3613LY014
				Nutrition for Life
				Nutrition: From Science to You
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 3e
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 4e
				Orientation Questions for Students
				Pirnot: Mathematics All Around, 3e
				Player Functional Testing book
				Player Testing--XL Player only
				PPE: QA uPDF DNT:
				PPE: QA uPDF DNT: MyMathLab for Developmental Mathematics: Basic Mathematics, B
				Prior: Basic Mathematics, 1e
				Queensborough CC Math 120: Algebra with Trigonometry for College Students, 2e
				Queensborough CC: Intermediate Algebra with Trigonometry (Blitzer)
				Ratti: College Algebra
				Ratti: College Algebra and Trigonometry
				Ratti: Precalculus
				Riverside CC custom course
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 3e
				Rockswold: Beginning Algebra with Applications &amp; Visualization
				Rockswold: Beginning Algebra with Apps and Visualization, 2e
				Rockswold: Beginning and Intermediate Algebra
				Rockswold: Beginning and Intermediate Algebra, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e
				Rockswold: College Algebra, 4e
				Rockswold: Essentials of College Algebra with Modeling and Visualization, 3e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 3e
				Rockswold: Precalculus with Modeling and Visualization, 3e
				Salzman: Mathematics for Business, 8e
				Sam_eT2_Test book
				Sharpe: Business Statistics
				Sharpe: Business Statistics Preview
				Sharpe: Business Statistics, 4e
				SI Notation testing (from sbtest)
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				SPPTesting
				StaCrunch Test Book 01
				StatCrunch Test Book for PPE
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data,1e copy
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 4e
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 5e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 7e
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: College Algebra w/ Graphing Utilities, 4e
				Sullivan: College Algebra with Graphing Utilities, 5e
				Sullivan: College Algebra, 7e
				Sullivan: College Algebra, 8e
				Sullivan: College Algebra: Concepts through Functions
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary Algebra
				Sullivan: Fundamentals of Statistics, 2e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Intermediate Algebra
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Precalculus w/ Graphing Utilities, 4e
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 7e
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle
				Sullivan: Precalculus: Concepts through Functions, Unit Circle
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 3e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5/e, Global Edition
				Sullivan: Trigonometry, 7e
				Sullivan: Trigonometry: A Right Triangle Approach, 5e
				Sullivan: Trigonometry: A Unit Circle Approach, 8e
				Test :Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data1e
				test3506_std
				test3506_withBrowser
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO RETIRED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomasson: Introductory &amp; Intermediate Algebra, 3e
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 5e
				Tobey: Basic College Mathematics, 6e
				Tobey: Basic College Mathematics, 8e
				Tobey: Beginning &amp; Intermediate Algebra, 2e
				Tobey: Beginning Algebra, 6e
				Tobey: Beginning Algebra: Early Graphing
				Tobey: Essentials of Basic College Mathematics
				Tobey: Essentials of Basic Collge Mathematics, 2e
				Tobey: Intermediate Algebra, 5e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trigsted: College Algebra 1e
				Trigsted: College Algebra 1e Chapter 5 DEMO
				Trigsted: College Algebra 1e DEMO
				Trigsted: College Algebra Interactive
				Trim: Calculus for Engineers, 4e
				Triola: Elementary Statistics Using Excel, 2e
				Triola: Elementary Statistics Using Excel, 3e
				Triola: Elementary Statistics Using the Graphing Calculator
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 2e
				Triola: Elementary Statistics, 10e
				Triola: Elementary Statistics, 11e
				Triola: Elementary Statistics, 9e
				Triola: Essentials of Statistics, 2e
				Triola: Essentials of Statistics, 3e
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				UCF: The Finite Companion 6e Revised (Angel)
				UDM: Intermediate Algebra: A STEM Approach, 1/e
				Univ of Florida: Precalculus 1140/1147/1114
				Universal Domain Graph Diagnostic Exercises
				Varberg: Calculus Early Transcendentals
				Varberg: Calculus, 9e
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics with Calculus, 9e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 6e
				Weiss: Elementary Statistics, 7e
				Weiss: Introductory Statistics, 7e
				Weiss: Introductory Statistics, 8e
				Woodbury: Elementary &amp; Intermediate Algebra
				Woodbury: Elementary &amp; Intermediate Algebra, 2e
				Woodbury: Elementary Algebra
				Woodbury: Intermediate Algebra
				XL Load Testbook
				Young: Finite Mathematics, 3e
				z MathXL Freelancer Test Book

			&quot;) or . = concat(&quot;
				  -- Choose Book --  
				Adams: Calculus, 6e  DEMO
				Adams: Calculus, 6e ENHANCED
				ADP: Algebra II Online Course
				ADP: Algebra II Online Course RETIRED
				Agresti: Statistics: The Art and Science of Learning from Data
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Akst: Basic Mathematics through Applications, 3e
				Akst: Fundamental Mathematics through Applications, 3e
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Intermediate Algebra through Applications
				Akst: Intermediate Algebra Through Applications, 2e
				Akst: Introductory &amp; Intermediate Algebra, 2e
				Akst: Introductory Algebra through Applications
				Akst: Introductory Algebra Through Applications, 2e
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book:  Non-combo
				Algebra Review for Calculus
				Alicia&quot; , &quot;'&quot; , &quot;s SBNet feature testing book
				Angel: A Survey of Mathematics with Applications, 7e
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, 9e
				Angel: A Survey of Mathematics with Applications, Expanded 7e
				Angel: A Survey of Mathematics with Applications, Expanded 8e
				Angel: Algebra for College Students, 3e
				Angel: Elementary Algebra for College Students Early Graphing, 3e
				Angel: Elementary Algebra for College Students, 7e
				Angel: Elementary and Intermediate Algebra for College Students, 3e
				Angel: Intermediate Algebra for College Students, 7e
				ASU MAT 210/211: Brief Calculus/Mathematics for Business Analysis
				Austin CC: Basic College Math
				Automated Preload Master Course book
				Baker College Math 091: Basic College Mathematics
				Baker College: Basic College Mathematics (Tobey)
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Econ, Social &amp; Life Sciences, 10e
				Barnett: College Mathematics, 10e
				Barnett: College Mathematics, 11e
				Barnett: Finite Mathematics, 10e
				Barnett: Finite Mathematics, 11e
				Barrow: Statistics for Economics, Accounting and Business Studies, 5e
				Beecher: Algebra &amp; Trigonometry, 2e
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: College Algebra with Intermediate Algebra, 1e
				Beecher: College Algebra, 2e
				Beecher: College Algebra, 3e
				Beecher: Precalculus, 2e
				Beecher: Precalculus, 3e
				Bennett: Statistical Reasoning for Everyday Life, 2e
				Bennett: Statistical Reasoning For Everyday Life, 3e
				Bennett: Using and Understanding Mathematics, 3e
				Bennett: Using and Understanding Mathematics, 4e
				Berenson: Basic Business Statistics, 11e
				Berenson: Basic Business Statistics, Australian Edition
				Berenson: Business Statistics, Australian Edition
				Bhead test book
				Billstein: Mathematics for Elementary School Teachers, 9e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 4e
				Bittinger: Basic College Mathematics, 12e
				Bittinger: Basic Mathematics with Early Integers
				Bittinger: Basic Mathematics, 10e
				Bittinger: Basic Mathematics, 9e
				Bittinger: Calculus and Its Applications, 8e
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: College Algebra: Graphs &amp; Models, 3e
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 6e
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Elementary &amp; Intermediate Algebra Graphs &amp; Models, 3e
				Bittinger: Elementary &amp; Intermediate Algebra: Graphs &amp; Models, 2e
				Bittinger: Elementary Algebra, Concepts and Applications, 7e
				Bittinger: Elementary Algebra: Graphs &amp; Models
				Bittinger: Elementary and Intermediate Algebra, Concepts and Applications, 4e
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3e
				Bittinger: Fundamental Mathematics, 4e
				Bittinger: Intermediate Algebra, 10e
				Bittinger: Intermediate Algebra, 9e
				Bittinger: Intermediate Algebra, Concepts and Applications, 7e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 9e
				Bittinger: Introductory and Intermediate Algebra, 2e
				Bittinger: Introductory and Intermediate Algebra, 3e
				Bittinger: Prealgebra &amp; Introductory Algebra, 2e
				Bittinger: Prealgebra and Introductory Algebra
				Bittinger: Prealgebra, 4e
				Bittinger: Prealgebra, 5e
				Bittinger: Precalculus: Graphs &amp; Models, 3e
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Blair/Tobey/Slater: Prealgebra, 3e
				Blair: Intermediate Algebra
				Blair: Introductory Algebra
				Blitzer:  College Algebra, 3e
				Blitzer: Algebra &amp; Trigonometry, 2e
				Blitzer: Algebra &amp; Trigonometry, 3e
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra &amp; Trigonometry: An Early Functions Approach
				Blitzer: Algebra for College Students, 5e
				Blitzer: Algebra for College Students, 6e
				Blitzer: Algebra for College Students, 7e
				Blitzer: College Algebra Essentials, 2e
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra: An Early Functions Approach
				Blitzer: Essentials of Intermediate Algebra for College Students
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra
				Blitzer: Intermediate Algebra for College Students, 4e
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 3e
				Blitzer: Introductory Algebra for College Students, 4e
				Blitzer: Introductory Algebra for College Students, 5e
				Blitzer: Introductory and Intermediate Algebra for College Students, 2e
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Precalculus Essentials, 2e
				Blitzer: Precalculus, 2e
				Blitzer: Precalculus, 3e
				Blitzer: Thinking Mathematically, 3e
				Blitzer: Thinking Mathematically, 4e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 3e
				Blue Sky Virtual Algebra II, version 2
				Blue Sky Virtual: Algebra II
				Blue Sky Virtual: General Mathematics
				Blue Sky Virtual: Geometry
				Blue Sky Virtual: Prealgebra and Algebra I
				Bock: Stats: Modeling the World
				Bock: Stats: Modeling the World, 2e
				Briggs/Cochran: Calculus, 2e
				California Algebra Readiness
				Campbell Biology: Concepts &amp; Connections, 9/e
				Carson: Elementary Algebra
				Carson: Elementary Algebra with Early Systems of Equations
				Carson: Elementary Algebra, 2e
				Carson: Elementary and Intermediate Algebra
				Carson: Elementary and Intermediate Algebra, 2e ENHANCED
				Carson: Intermediate Algebra
				Carson: Intermediate Algebra, 2e
				Carson: Prealgebra, 2e
				Carson: Prealgebra, 3e
				Chemistry: The Central Science, 14/e
				Chris T3 Test Feb
				Chroniques du Québec et du Canada, Des origines à 1840
				Chroniques-CSG Testing
				Cite foot note test Book
				Clark: Applied Basic Mathematics
				Cleaves:  Business Math, 8e
				Cleaves: Business Math, 8e Brief Edition
				Cleaves: Business Math, 8e DEMO
				Cleaves: College Mathematics 2009 Update with MyMathLab
				Cleaves: College Mathematics, 10e
				Consortium: Math in Action: Algebraic, Graphical &amp; Trig. Problem Solving, 3e
				Consortium: Math In Action: Intro to Algebraic, Graph, and Num Prob Solv, 3/e
				Consortium: Math In Action: Prealgebra Problem Solving, 2e
				Contemporary Business Mathematics with Canadian Applications, 11/e
				Croft: Foundation Maths, 4e
				Croft: Mathematics for Engineers, 3e
				CSU: ELM Exam Prep Course, version 2
				CUSTOM MML- Univ of Arkansas-Fayetteville MATH 2053: Finite Mathematics
				De Veaux: Intro Stats
				De Veaux: Intro Stats, 2e
				De Veaux: Intro Stats, 3e
				De Veaux: Stats: Data and Models
				De Veaux: Stats: Data and Models, 2e
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				DeVry MAT190 (Washington)
				DNT: Beginning and Intermediate Algebra
				DNT: Beginning and Intermediate Algebra_Test
				Donnelly: Business Statistics, 2e
				Dugopolski: College Algebra &amp; Trigonometry, 4e
				Dugopolski: College Algebra, 4e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus, 2e
				Dugopolski: Precalculus, 4e
				Dugopolski: Precalculus: Functions and Graphs, 2e
				Dugopolski: Precalculus: Functions and Graphs, 3e
				Dugopolski: Trigonometry, 2e
				Early Childhood Education Today, 14/e
				ECPI MTH099: Beginning Algebra
				ECPI MTH115: Thinking Mathematically
				ECPI MTH125/MTH131: Intermediate Algebra
				ECPI MTH140: Statistics
				EEK Trigsted College Algebra 2e IRA TEST
				eT1 Math Test Book
				eT1 Math Test Book - PPE
				eT1 Test Book
				eT2 CITE + Print Test Book - STG/PPE
				eT2 CITE Test Book - STG/PPE
				eT2 Math Test Book - PPE
				eT2 Math Test Book BXWAT0VP1H9
				eT2 Math Test RH - BXWAT0VP1H9
				eT2 PXE Math Test Book - PPE
				eT2 Staging - FXVIU9IDRCS
				Etext2 check 1 ppe
				ETWEB-7558_PXE Test Book
				Financial Management: Principles and Applications, 13/e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e
				Florida International University: Finite Math 1060
				Georgia Math IV  (Demana and Agresti)
				Georgia Math IV  (Sullivan and Agresti)
				Goldstein: Brief Calculus and Its Applications, 11e
				Goldstein: Calculus and Its Applications, 11e
				Goldstein: Finite Mathematics and Its Applications, 9e
				Goshaw: Concepts of Calculus with Applications
				Harshbarger: College Algebra in Context
				Harshbarger: College Algebra In Context, 2e
				Hass: University Calculus
				Hass: University Calculus Alternate Edition
				Hass: University Calculus Elements
				Hillsborough CC: Thomas&quot; , &quot;'&quot; , &quot; Calculus, 11e with Precalculus
				Hornsby: A Graphical Approach to Algebra and Trigonometry, 4e
				Hornsby: A Graphical Approach to College Algebra, 4e
				Hornsby: A Graphical Approach to Precalculus with Limits, A Unit Circle App, 4e
				Hornsby: A Graphical Approach to Precalculus, 4e
				Hummelbrunner: Contemporary Business Math with Canadian Applications, 8e Update
				Hummelbrunner: Mathematics of Finance with Canadian Applications, 6e Update
				Hutchison: Mathematics for New Technologies
				Intellipro Test Book - StatCrunch
				Jacques: Mathematics for Economics and Business, 6e
				John Jay: Thinking Mathematically with Algebra (Blitzer)
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 4e
				Kaplan Math 103: College Mathematics
				Kaplan MM150 (Angel: Survey of Math)
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math test book
				Knewton Math test book2
				Knewton tiny test book-Ishara
				KU122: Introduction to Math Skills and Strategies
				Larson: Elementary Statistics: Picturing the World, 3e
				Larson: Elementary Statistics: Picturing the World, 4e
				Lay: Linear Algebra and Its Applications, 3e
				Lehmann: Elementary Algebra: Graphs and Authentic Applications
				Lehmann: Intermediate Algebra: Functions and Authentic Applications, 3e
				Levine: Business Statistics: A First Course, 5e
				Lial: Algebra for College Students, 5e
				Lial: Algebra for College Students, 6e
				Lial: Basic College Mathematics, 7e
				Lial: Basic College Mathematics, 8e
				Lial: Beginning &amp; Intermediate Algebra, 4e
				Lial: Beginning Algebra, 10e (WOW)
				Lial: Beginning Algebra, 9e
				Lial: Beginning and Intermediate Algebra, 3e
				Lial: Calculus with Applications, 7e
				Lial: Calculus with Applications, 8e
				Lial: Calculus with Applications, 9e
				Lial: Calculus with Applications, Brief Version 9e
				Lial: Calculus with Applications, Brief Version, 8e
				Lial: College Algebra and Trigonometry, 3e
				Lial: College Algebra and Trigonometry, 4e
				Lial: College Algebra, 10e
				Lial: College Algebra, 9e
				Lial: Developmental Mathematics, 4e PPE book
				Lial: Essential Mathematics, 2e
				Lial: Essentials of College Algebra
				Lial: Essentials of College Algebra Alternate Edition
				Lial: Finite Mathematics &amp; Calculus with Applications, 8e
				Lial: Finite Mathematics and Calculus with Applications, 7e
				Lial: Finite Mathematics with Applications, 9e
				Lial: Finite Mathematics, 7e
				Lial: Finite Mathematics, 8e
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e
				Lial: Intermediate Algebra, 11e
				Lial: Intermediate Algebra, 12e
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 9e
				Lial: Introductory Algebra, 8e
				Lial: Introductory and Intermediate Algebra, 3e
				Lial: Mathematics with Applications, 9e
				Lial: Prealgebra and Introductory Algebra, 2e
				Lial: Prealgebra, 3e
				Lial: Prealgebra: An Integrated Approach, 1e Hardback
				Lial: Precalculus, 3e
				Lial: Precalculus, 4e
				Lial: Precalculus, 6e
				Lial: Trigonometry, 8e
				Lial: Trigonometry, 9e
				Lockdown Browser test PPE
				Long: Mathematical Reasoning for Elementary Teachers, 4e
				Long: Mathematical Reasoning for Elementary Teachers, 5e
				Martin-Gay: Algebra Foundations, 1e
				Martin-Gay: Algebra: A Combined Approach
				Martin-Gay: Algebra: A Combined Approach, 3e
				Martin-Gay: Basic College Mathematics w/ Early Integers
				Martin-Gay: Basic College Mathematics, 2e
				Martin-Gay: Basic College Mathematics, 3e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Developmental Mathematics
				Martin-Gay: Intermediate Algebra, 2e
				Martin-Gay: Intermediate Algebra, 3e
				Martin-Gay: Intermediate Algebra, 4e
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 4e
				Martin-Gay: Introductory Algebra, 2e
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra and Introductory Algebra
				Martin-Gay: Prealgebra, 4e
				Martin-Gay: Prealgebra, 5e
				Mathspace Test Book
				McClave:  Statistics for Business and Economics, 10e
				McClave: A First Course in Statistics, 10e
				McClave: Statistics, 10e
				McClave: Statistics, 11e
				McKenna/Kirk: Beginning &amp; Intermediate Algebra, 1e DEMO
				Miller: Business Mathematics, 10e
				Miller: Business Mathematics, 11e
				Miller: Mathematical Ideas 11e and Expanded 11e
				Miller: Mathematical Ideas, 10e
				Miller: Mathematical Ideas, Expanded 10e
				MXL Player Content Test Book
				MyMathLab test book A3 - Lilani 6/15
				MyMathLab test book B (from Briggs Calc)
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				NextGen Media Math PPE testbookDeletion
				NextGen Media Math Test Book--CITE
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL-FGPKCV9VIE3
				NextGen Media Math Test Book--REAL-H26F1QTODXL
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--REALFH3613LY014
				Nutrition for Life
				Nutrition: From Science to You
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 3e
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 4e
				Orientation Questions for Students
				Pirnot: Mathematics All Around, 3e
				Player Functional Testing book
				Player Testing--XL Player only
				PPE: QA uPDF DNT:
				PPE: QA uPDF DNT: MyMathLab for Developmental Mathematics: Basic Mathematics, B
				Prior: Basic Mathematics, 1e
				Queensborough CC Math 120: Algebra with Trigonometry for College Students, 2e
				Queensborough CC: Intermediate Algebra with Trigonometry (Blitzer)
				Ratti: College Algebra
				Ratti: College Algebra and Trigonometry
				Ratti: Precalculus
				Riverside CC custom course
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Algebra and Trigonometry with Modeling and Visualization, 3e
				Rockswold: Beginning Algebra with Applications &amp; Visualization
				Rockswold: Beginning Algebra with Apps and Visualization, 2e
				Rockswold: Beginning and Intermediate Algebra
				Rockswold: Beginning and Intermediate Algebra, 2e
				Rockswold: College Algebra with Modeling and Visualization, 3e
				Rockswold: College Algebra, 4e
				Rockswold: Essentials of College Algebra with Modeling and Visualization, 3e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 3e
				Rockswold: Precalculus with Modeling and Visualization, 3e
				Salzman: Mathematics for Business, 8e
				Sam_eT2_Test book
				Sharpe: Business Statistics
				Sharpe: Business Statistics Preview
				Sharpe: Business Statistics, 4e
				SI Notation testing (from sbtest)
				Skuce: Analyzing Data and Making Decisions: Statistics for Business
				SPPTesting
				StaCrunch Test Book 01
				StatCrunch Test Book for PPE
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data,1e copy
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 4e
				Sullivan: Algebra &amp; Trigonometry w/ Graphing Utilities, 5e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 7e
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: College Algebra w/ Graphing Utilities, 4e
				Sullivan: College Algebra with Graphing Utilities, 5e
				Sullivan: College Algebra, 7e
				Sullivan: College Algebra, 8e
				Sullivan: College Algebra: Concepts through Functions
				Sullivan: Elementary &amp; Intermediate Algebra
				Sullivan: Elementary Algebra
				Sullivan: Fundamentals of Statistics, 2e
				Sullivan: Fundamentals of Statistics, 4e
				Sullivan: Intermediate Algebra
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Precalculus w/ Graphing Utilities, 4e
				Sullivan: Precalculus w/ Graphing Utilities, 5e
				Sullivan: Precalculus, 7e
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle
				Sullivan: Precalculus: Concepts through Functions, Unit Circle
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 3e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Statistics: Informed Decisions Using Data, 5/e, Global Edition
				Sullivan: Trigonometry, 7e
				Sullivan: Trigonometry: A Right Triangle Approach, 5e
				Sullivan: Trigonometry: A Unit Circle Approach, 8e
				Test :Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data1e
				test3506_std
				test3506_withBrowser
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO RETIRED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomasson: Introductory &amp; Intermediate Algebra, 3e
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 5e
				Tobey: Basic College Mathematics, 6e
				Tobey: Basic College Mathematics, 8e
				Tobey: Beginning &amp; Intermediate Algebra, 2e
				Tobey: Beginning Algebra, 6e
				Tobey: Beginning Algebra: Early Graphing
				Tobey: Essentials of Basic College Mathematics
				Tobey: Essentials of Basic Collge Mathematics, 2e
				Tobey: Intermediate Algebra, 5e
				Tomball College: Introductory &amp; Intermediate Algebra (Math 0308 &amp; 0310)
				Trigsted: College Algebra 1e
				Trigsted: College Algebra 1e Chapter 5 DEMO
				Trigsted: College Algebra 1e DEMO
				Trigsted: College Algebra Interactive
				Trim: Calculus for Engineers, 4e
				Triola: Elementary Statistics Using Excel, 2e
				Triola: Elementary Statistics Using Excel, 3e
				Triola: Elementary Statistics Using the Graphing Calculator
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 2e
				Triola: Elementary Statistics, 10e
				Triola: Elementary Statistics, 11e
				Triola: Elementary Statistics, 9e
				Triola: Essentials of Statistics, 2e
				Triola: Essentials of Statistics, 3e
				U of Cincinnati: Prealgebra &amp; Introductory Algebra, 2e ENHANCED
				UCF: The Finite Companion 6e Revised (Angel)
				UDM: Intermediate Algebra: A STEM Approach, 1/e
				Univ of Florida: Precalculus 1140/1147/1114
				Universal Domain Graph Diagnostic Exercises
				Varberg: Calculus Early Transcendentals
				Varberg: Calculus, 9e
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics with Calculus, 9e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 6e
				Weiss: Elementary Statistics, 7e
				Weiss: Introductory Statistics, 7e
				Weiss: Introductory Statistics, 8e
				Woodbury: Elementary &amp; Intermediate Algebra
				Woodbury: Elementary &amp; Intermediate Algebra, 2e
				Woodbury: Elementary Algebra
				Woodbury: Intermediate Algebra
				XL Load Testbook
				Young: Finite Mathematics, 3e
				z MathXL Freelancer Test Book

			&quot;))]</value>
      <webElementGuid>eef92df9-1e90-410f-af05-1439f8b0e81a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
