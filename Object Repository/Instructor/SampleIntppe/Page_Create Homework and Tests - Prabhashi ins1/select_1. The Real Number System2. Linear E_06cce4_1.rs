<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1. The Real Number System2. Linear E_06cce4_1</name>
   <tag></tag>
   <elementGuidId>7586c9d9-9ca2-47f1-991b-b7bfd17d910d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListChapter</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListChapter']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b20a1820-033b-4ce2-ad4a-f5f6240519b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter</value>
      <webElementGuid>1dd34244-2b5b-42fa-83a3-366b3a203dc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter\',\'\')', 0)</value>
      <webElementGuid>9268879d-5a95-4d90-b17d-a3712e00115b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListChapter</value>
      <webElementGuid>6d1160a2-dc10-4048-8c79-b59ba9d8be68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>87f80f3b-6509-41ac-a4a3-e9ca67f4ebac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	</value>
      <webElementGuid>968ae7d9-c86b-4951-bbc0-4d8db3db32e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListChapter&quot;)</value>
      <webElementGuid>79fb7cd8-7236-4914-a998-333c4cf8ef05</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListChapter']</value>
      <webElementGuid>606d6c2a-d28d-4c98-abe3-7d2eab175bbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrChapter']/td[2]/div/select</value>
      <webElementGuid>0aae31a8-1313-4b79-8938-08dfe469e9b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[1]</value>
      <webElementGuid>38238e1a-7e31-4e45-8cde-c6c77b1cb813</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change...'])[1]/following::select[1]</value>
      <webElementGuid>fe4c08e8-0563-4228-9a1a-f03fec471aa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/preceding::select[1]</value>
      <webElementGuid>cbb80c11-8584-4804-8ee0-1bcd03fdbc96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[2]</value>
      <webElementGuid>2eaff7d7-a142-4c83-88f9-67bce7c032a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>e21d08b9-98ef-47dd-a603-f1cc91a5307f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter' and @id = 'DropDownListChapter' and (text() = '
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	' or . = '
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	')]</value>
      <webElementGuid>e10c9c2c-e562-40cd-afc2-34871285fcae</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
