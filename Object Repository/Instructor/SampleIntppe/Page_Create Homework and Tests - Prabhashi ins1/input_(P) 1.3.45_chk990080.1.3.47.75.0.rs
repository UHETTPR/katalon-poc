<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 1.3.45_chk990080.1.3.47.75.0</name>
   <tag></tag>
   <elementGuidId>0d05d968-938c-4d3a-910e-f47947eec079</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.1.3.47.75.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0499f570-0c59-46b0-97bf-de02e0158d48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>62a8b02d-f469-4ba7-9100-6c9484f47622</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.1.3.47.75.0</value>
      <webElementGuid>d207643c-c673-4fc0-b2cf-90cb561b8599</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 1.3.47 (Write word phrases as algebraic expressions.)</value>
      <webElementGuid>daa28ca4-3f2b-4032-8dc3-486d0b8562c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.1.3.47.75.0&quot;)</value>
      <webElementGuid>bb6e9775-c30a-4331-8935-083bc22ebdd0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.1.3.47.75.0']</value>
      <webElementGuid>10323996-25cd-475d-85ad-69752f3d8ef1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.1.3.47.75.0']/div/div[2]/input</value>
      <webElementGuid>289eefc3-c576-4cdd-b625-299b8762863d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[15]/div/div[2]/input</value>
      <webElementGuid>f1b9d9f0-419c-49b5-a917-3b921dfa8a15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.1.3.47.75.0' and @title = '(P) 1.3.47 (Write word phrases as algebraic expressions.)']</value>
      <webElementGuid>6365a7f9-8920-414e-86c8-08dd10f65f42</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
