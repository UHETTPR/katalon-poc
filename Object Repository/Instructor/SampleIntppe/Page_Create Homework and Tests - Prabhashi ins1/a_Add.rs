<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Add</name>
   <tag></tag>
   <elementGuidId>ba1a1b4c-9e89-44de-af70-ddc3275e4191</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#lnkGenAdd</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='lnkGenAdd']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>231ec681-5427-4df0-8237-e5cb14ce1219</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>lnkGenAdd</value>
      <webElementGuid>2c17c4e9-8410-4aaf-8344-7a7dbe15f31c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>appendExercise();</value>
      <webElementGuid>d4a2885f-b02b-4310-abfe-f4a0800af8a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>30445c2a-59c2-476e-bfdd-ecc881a75620</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add
	</value>
      <webElementGuid>31c31456-6ad7-46fd-8458-baac3aa571b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;lnkGenAdd&quot;)</value>
      <webElementGuid>57055935-f0f6-44d8-bac9-cb7a523d3b5b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='lnkGenAdd']</value>
      <webElementGuid>c9cc29b7-a942-4193-848a-92d33ca7b442</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='tableSelections']/tbody/tr[3]/td[2]/div/a</value>
      <webElementGuid>d0ce59d6-32eb-4718-a776-bd756db2b01f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Add')]</value>
      <webElementGuid>9bcc7203-0dba-4994-b8b2-417689a73037</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sylva test q4'])[1]/following::a[1]</value>
      <webElementGuid>e8e5952b-fca0-47a4-b503-7aad017c3c96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sylva test q3'])[1]/following::a[2]</value>
      <webElementGuid>27359c28-2c4d-4655-9a21-8cfcb1650a72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove'])[1]/preceding::a[1]</value>
      <webElementGuid>1f1c3fb6-37dd-4e3c-ac50-d4f83163967c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pool'])[1]/preceding::a[2]</value>
      <webElementGuid>609ae16b-1f67-4f6c-92f7-c495a4d2ac42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add']/parent::*</value>
      <webElementGuid>0be59ec6-31d2-49b5-83bd-b30d214a1902</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[2]/div/a</value>
      <webElementGuid>f9b9f496-7d2e-49d3-8d8f-22886c0e391d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'lnkGenAdd' and (text() = 'Add
	' or . = 'Add
	')]</value>
      <webElementGuid>0df6d98d-ef4c-404a-9d9b-d685c1d2b777</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
