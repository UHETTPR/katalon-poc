<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 1.3.39_chk990080.1.3.43.73.0</name>
   <tag></tag>
   <elementGuidId>de804fa3-41f7-42e6-aa19-2fb44c48f937</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.1.3.43.73.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a417ecbf-1d2f-4129-9be4-7967ae1d60b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>2dc19987-9cd9-4f71-abec-34b4088f5e8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.1.3.43.73.0</value>
      <webElementGuid>b16fa1be-63a1-4b04-b901-db1423ea8905</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 1.3.43 (Write word phrases as algebraic expressions.)</value>
      <webElementGuid>99a5d1d5-7d9d-4e5c-a6bf-ff56eb717ba3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.1.3.43.73.0&quot;)</value>
      <webElementGuid>35b97bba-aa67-4843-9cbf-f8957804a173</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.1.3.43.73.0']</value>
      <webElementGuid>81a77ba4-067a-432d-ada2-fb008a38f6ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.1.3.43.73.0']/div/div[2]/input</value>
      <webElementGuid>bdb80eca-60bf-44ef-9f02-2f056ad6816d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[13]/div/div[2]/input</value>
      <webElementGuid>ce822f56-4159-496d-bee7-1f03a9d14092</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.1.3.43.73.0' and @title = '(P) 1.3.43 (Write word phrases as algebraic expressions.)']</value>
      <webElementGuid>08560b21-f2b3-47a9-8d95-eda9d2d7d9c5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
