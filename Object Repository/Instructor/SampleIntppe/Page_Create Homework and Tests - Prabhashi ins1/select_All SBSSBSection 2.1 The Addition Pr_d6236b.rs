<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 2.1 The Addition Pr_d6236b</name>
   <tag></tag>
   <elementGuidId>ee90dcb5-6efe-45f2-9b18-5547f4d32101</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>c862c17a-d8ac-49c6-90f7-fa072fc4b335</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>4cecdc24-4bfa-4919-afe1-ce87907f39c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>23bd9a03-2d4d-4368-941f-31da5c8cd9db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>ef18ebcc-4088-4c30-aa88-ff849c87898d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>983b7640-2bae-4b3c-9a01-fe306813e650</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	</value>
      <webElementGuid>50c03168-aeb2-471d-9864-8b418f9aea57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>90d2828b-e997-416d-b948-f8dc9d53f6ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>5289d47e-35b9-4e36-a5d1-1742d2bb65c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>e52f0a1d-13d9-4134-9fdc-c8b98632b202</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>4e7ef266-d419-4bfd-9444-0dd3a68cbb83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>490303c8-27a3-4135-916f-5ea9f74b6d91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>6f3a1a41-ceb3-4b45-9647-9026fc5e3e79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>8a870a0c-1062-44cb-a83f-4bb20c266bbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>89cd3a0c-95b2-49bc-aac8-ba5c61025352</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	' or . = '
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	')]</value>
      <webElementGuid>0f956fa7-05e0-420b-844c-e3d8f85b86a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
