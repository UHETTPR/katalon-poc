<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 1.1 This is a very _357309</name>
   <tag></tag>
   <elementGuidId>122f793f-c4fa-4dde-a50e-43e99fc2914f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>ef5032cc-1827-4578-9a40-16a236deed01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>f46a0707-300b-41dc-b341-c073aaf90c38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>ec06288d-208a-49f5-8476-4be34eabe205</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>6e89ef65-ec60-4374-8836-ebb801d0b338</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>3bc50a25-ab7f-4d31-93e6-aa6097822e39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>a546b40b-8afa-4c91-84e3-676fa7dd2d33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions</value>
      <webElementGuid>80beab7d-e952-4cb5-8685-20bb8d20f156</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>9d5ce6a1-6774-4f7f-915a-f2e74a286011</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>73908f74-adc9-4f05-8d3e-30080946c04e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>7229e1e1-b384-4285-8f53-4a10d05003f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>cc4dfefd-f786-451b-b92a-801b99a26c25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>a3186c12-4232-4b72-a06d-1948d7d49740</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>8e8dd524-1713-459c-93a2-deecf980f69b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>e6a79cb6-3dbc-4fb4-a700-e6eb5101382e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>6bcf6547-4b82-49d1-9c38-39123072c088</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions' or . = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions')]</value>
      <webElementGuid>fff83ae1-bfa6-4a2d-9683-ee6160573529</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
