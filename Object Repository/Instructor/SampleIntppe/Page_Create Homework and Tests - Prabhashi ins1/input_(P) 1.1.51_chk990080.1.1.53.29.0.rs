<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 1.1.51_chk990080.1.1.53.29.0</name>
   <tag></tag>
   <elementGuidId>542dbe45-b1b4-4c91-8823-890896d69e70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.1.1.53.29.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fda4d4e1-19e2-4df7-91ed-8ced117ab5b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>3a1ef82c-3b13-45ef-b301-26bd8f6dd7a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.1.1.53.29.0</value>
      <webElementGuid>3b255b59-612f-4a32-ad9a-33cbd0ab7ed2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 1.1.53 (Add and subtract fractions.)</value>
      <webElementGuid>d86df4e2-7f94-435c-ade1-3396d6326234</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.1.1.53.29.0&quot;)</value>
      <webElementGuid>0cdb02b8-a6f9-4f92-84e2-970fec991838</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.1.1.53.29.0']</value>
      <webElementGuid>decb7ccf-14e0-45cc-a4bf-56bd9e7adf08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.1.1.53.29.0']/div/div[2]/input</value>
      <webElementGuid>acaaaccf-64db-4db0-8811-43303c885119</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[12]/div/div[2]/input</value>
      <webElementGuid>c9ad4835-feaa-4eb8-b7c6-baf5c964b6ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.1.1.53.29.0' and @title = '(P) 1.1.53 (Add and subtract fractions.)']</value>
      <webElementGuid>bf23214f-e8e7-4bda-9ec2-4c2103378162</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
