<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 1.3.43_chk990080.1.3.45.74.0</name>
   <tag></tag>
   <elementGuidId>898b1297-a8d8-4abe-beb7-f76baaf60e21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.1.3.45.74.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5c2c1b81-f819-4d3c-a9ff-b36320a58c1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>eb615ff3-ee4c-46c5-ba20-04f3d152fbe3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.1.3.45.74.0</value>
      <webElementGuid>5df7bc7d-69ae-476f-8496-7efce29f6470</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 1.3.45 (Write word phrases as algebraic expressions.)</value>
      <webElementGuid>c73f8f5d-591c-43e8-b3ac-0ea6bf813cb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.1.3.45.74.0&quot;)</value>
      <webElementGuid>b8e2b4c8-8062-436d-bce8-91bf937c596e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.1.3.45.74.0']</value>
      <webElementGuid>e1d0b8e9-b0b9-4e21-a3c8-170861da8159</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.1.3.45.74.0']/div/div[2]/input</value>
      <webElementGuid>f94f5d28-f7b6-439f-97ca-5391c693a115</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[14]/div/div[2]/input</value>
      <webElementGuid>cd1fa383-c503-44d3-99d4-74424063b3b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.1.3.45.74.0' and @title = '(P) 1.3.45 (Write word phrases as algebraic expressions.)']</value>
      <webElementGuid>5a40084f-7ddf-4a73-bcc4-80c4f14a4584</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
