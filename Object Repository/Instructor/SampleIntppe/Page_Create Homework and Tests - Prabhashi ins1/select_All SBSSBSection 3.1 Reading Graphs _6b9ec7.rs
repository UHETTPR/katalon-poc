<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 3.1 Reading Graphs _6b9ec7</name>
   <tag></tag>
   <elementGuidId>b78c7ecf-5959-43a5-a47c-abe207c8b7b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>c9468c23-f89d-4ef2-8e64-c102ae5de9c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>6e0008fb-1c67-492e-b880-baa16ea014d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>7ad34b31-c8b6-4399-9cc1-cca5d5c07364</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>e2a2f8b5-6981-48df-a6b3-4fb21e72890d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>19b30be3-1b46-4e37-a11b-687ad985003d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All SBS
		SBSection 3.1: Reading Graphs; Linear Equations in Two Variables
		SBSection 3.2: Graphing Linear Equations in Two Variables
		SBSection 3.3: The Slope of a Line
		SBSection 3.4: Equations of a Line
		SBSection 3.5: Graphing Linear Inequalities in Two Variables
		SBSection 3.6: Introduction to Functions
		SBSection 3.7: MyFoundationsLab

	</value>
      <webElementGuid>ef2ceb01-fe34-4684-9338-0e8f1bdfb28f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>52eba991-e673-4fcd-aff1-cc494166f783</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>acc91e0b-9d4c-4262-8c8d-3202cf77c4bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>76aa1576-877e-45ec-b5d4-c5b68bfac023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>8abf8e0d-ce89-4abb-a1c5-e89f0a25e59a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>2fd867cf-b797-4be8-a221-7da20df1eca4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>0556fced-73dd-4b86-9ca5-e13d11fb035b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>e68c8a84-382e-42df-bf08-337925df6dc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>1bb88578-2019-4ee3-b4ee-04a6bdf103f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All SBS
		SBSection 3.1: Reading Graphs; Linear Equations in Two Variables
		SBSection 3.2: Graphing Linear Equations in Two Variables
		SBSection 3.3: The Slope of a Line
		SBSection 3.4: Equations of a Line
		SBSection 3.5: Graphing Linear Inequalities in Two Variables
		SBSection 3.6: Introduction to Functions
		SBSection 3.7: MyFoundationsLab

	' or . = '
		All SBS
		SBSection 3.1: Reading Graphs; Linear Equations in Two Variables
		SBSection 3.2: Graphing Linear Equations in Two Variables
		SBSection 3.3: The Slope of a Line
		SBSection 3.4: Equations of a Line
		SBSection 3.5: Graphing Linear Inequalities in Two Variables
		SBSection 3.6: Introduction to Functions
		SBSection 3.7: MyFoundationsLab

	')]</value>
      <webElementGuid>05c4036e-405b-4ad5-8c60-fc5500744f52</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
