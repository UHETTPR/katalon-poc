<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Minimum mastery points required_ctl00_02668a</name>
   <tag></tag>
   <elementGuidId>36ca6f7e-33e9-4017-99f2-5c5e40e9a4d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4b0ef694-e2bb-4e2f-8104-64c1ddc5af71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints</value>
      <webElementGuid>e7c0f8d1-d452-4a71-b1ab-30d7c0561c58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$TextBoxMinimumMasteryPoints</value>
      <webElementGuid>e22d808b-445f-4ea6-a1bb-fe95c0cfe643</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>riTextBox riEnabled</value>
      <webElementGuid>4131751a-05e8-4c13-8c72-899741ac46ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>decimaldigits</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>59a550e5-a712-4590-99ff-0be371f13f2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>0e75358f-4132-4d9c-8299-fbd556be8c73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>00137dd5-88e2-46f1-8655-026a68d1ef08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints&quot;)</value>
      <webElementGuid>113ae89f-6850-4728-b6a1-d719f48f4554</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints']</value>
      <webElementGuid>528b4ff2-0070-4832-ad1e-8635317715a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints_wrapper']/input</value>
      <webElementGuid>6b503d01-c1ae-435b-b263-6e42501cb779</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[2]/span/input</value>
      <webElementGuid>3d3128cd-7bb0-4f79-a36a-0a6fe33c1c83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints' and @name = 'ctl00$ctl00$InsideForm$MasterContent$TextBoxMinimumMasteryPoints' and @type = 'text']</value>
      <webElementGuid>113cd9e8-c745-474b-b270-0f747267cba0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
