<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 2.2.31_chk990080.2.2.35.34.0</name>
   <tag></tag>
   <elementGuidId>70b80ef7-3f92-4bca-ab3a-28ed0b1a51a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.2.2.35.34.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>748caa61-1e3a-42ec-a12b-7216e8a6e373</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>e8671d04-570a-4eb2-9fd0-936ab3b0df9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.2.2.35.34.0</value>
      <webElementGuid>049242a7-5658-4c9b-ae3b-0d5c531b8c39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 2.2.35 (Solve equations using the multiplication property.)</value>
      <webElementGuid>1e3e12b8-1bd8-4b04-871a-86d1ff85099d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.2.2.35.34.0&quot;)</value>
      <webElementGuid>739801da-06c9-465e-8b2f-4a4526b33b17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.2.2.35.34.0']</value>
      <webElementGuid>5f188dd9-1ebc-49fb-933c-25fbc1f5cfa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.2.2.35.34.0']/div/div[2]/input</value>
      <webElementGuid>4676d55b-40b7-4702-8bbf-e8ac4b412942</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[12]/div/div[2]/input</value>
      <webElementGuid>e0d3c6a4-6290-44d3-a28b-06197b9a57cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.2.2.35.34.0' and @title = '(P) 2.2.35 (Solve equations using the multiplication property.)']</value>
      <webElementGuid>745ba771-ccbd-43c9-b8fa-2c71d3f5dfd9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
