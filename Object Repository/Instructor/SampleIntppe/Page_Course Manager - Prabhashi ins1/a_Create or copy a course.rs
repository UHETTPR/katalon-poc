<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create or copy a course</name>
   <tag></tag>
   <elementGuidId>4ce92cf2-2c04-4b23-a8d7-9da4d27ea9a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#BtnCreateCopy</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='BtnCreateCopy']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e0250e64-dcad-4a72-b511-68b9b654c3fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0)</value>
      <webElementGuid>cbde838b-8c99-4bb7-ae84-53822d50ac9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BtnCreateCopy</value>
      <webElementGuid>185c6074-5f63-4492-8247-0796d863ffe1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return DoCreate();</value>
      <webElementGuid>a0b53cb8-f039-4319-92f4-33e6c50ad72c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>a36595f9-cc78-4225-9f3b-c7da2015a4a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>95fa3bf3-4d6c-4705-b3a0-0430c177330e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create or copy a course
</value>
      <webElementGuid>a362d0bd-6e65-4a99-97ea-eb57c3308945</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BtnCreateCopy&quot;)</value>
      <webElementGuid>893dbc3b-5ba3-4e88-8e93-8044b3e75289</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='BtnCreateCopy']</value>
      <webElementGuid>2f0fd2e3-c5ba-43e0-add4-a81841853fc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='ctl00_ctl00_InsideForm_MasterContent_LiBtnCreateCopy']/a</value>
      <webElementGuid>5ddf447b-41fa-48bb-bb7a-d0a862c66a73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create or copy a course')]</value>
      <webElementGuid>ff375a79-6db4-4402-b9be-e4fda6fa4fd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::a[1]</value>
      <webElementGuid>6414fe78-e323-4528-b6a5-5202f24d848a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Course Manager'])[1]/following::a[2]</value>
      <webElementGuid>bd5189f4-d032-464a-b196-ecd01a1cd921</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manage Course List'])[1]/preceding::a[1]</value>
      <webElementGuid>7b625d65-1022-447c-b993-3e2a44756647</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course Name'])[1]/preceding::a[1]</value>
      <webElementGuid>19c1afd2-b50f-4685-9b9b-bac60829a1aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create or copy a course']/parent::*</value>
      <webElementGuid>1da9204c-4e0e-4789-94b3-3dd1672800e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0)')])[5]</value>
      <webElementGuid>00cedd59-3d1e-4d4e-9029-57219fb61da1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/ul/li/a</value>
      <webElementGuid>d63d57ca-226a-4e49-9b6e-3dc968ad1fa3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0)' and @id = 'BtnCreateCopy' and (text() = 'Create or copy a course
' or . = 'Create or copy a course
')]</value>
      <webElementGuid>2e16dd79-1c42-4a89-bf46-bf06b38510cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
