<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --  AbelBernankeCrous_727e4a</name>
   <tag></tag>
   <elementGuidId>2ff2e310-aa65-477d-b911-01b9bbabbb84</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>64a1b726-961f-4081-9b74-11feb09f54be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
      <webElementGuid>4f6e41f8-17b4-46c2-9131-147c073ba0d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      <webElementGuid>a4c678e5-332c-4e1a-ae16-634afb747a53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
      <webElementGuid>347f8bd1-d7e0-453a-bb39-cf81910a6364</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>4ffe1db8-3969-4b1a-8ec8-9abf06a66874</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				  -- Choose Book --  
				Abel/Bernanke/Croushore: Macroeconomics 6e
				Amplifire Test Book
				Bade/Parkin 3e for Testing
				Bade/Parkin: Demo for Foundations 3e
				Bade/Parkin: Essential Foundations of Economics 3e
				Bade/Parkin: Essential Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics 3e
				Bade/Parkin: Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics AP* Edition
				Bade/Parkin: Foundations of Macroeconomics 3e
				Bade/Parkin: Foundations of Macroeconomics 4e
				Bade/Parkin: Foundations of Microeconomics 3e
				Bade/Parkin: Foundations of Microeconomics 4e
				Case/Fair: Principles of Economics 8e
				Case/Fair: Principles of Economics 9e
				Case/Fair: Principles of Macroeconomics 8e
				Case/Fair: Principles of Macroeconomics 9e
				Case/Fair: Principles of Microeconomics 8e
				Case/Fair: Principles of Microeconomics 9e
				Cohen/Howe: Economics for Life: Smart Choices for You
				DEMO-Parkin, Economics: Canada in the Global Environment, Seventh Edition
				Ekelund 7e Standalone Testing
				Ekelund/Ressler/Tollison: Economics 7e (new)
				Ekelund/Ressler/Tollison: Macroeconomics 7e (new)
				Ekelund/Ressler/Tollison: Microeconomics 7e (new)
				Fremgen Test Course for Alison Doucette
				Group Work Test Book
				Hubbard/O'Brien: Economics 2e
				Hubbard/O'Brien: Economics 2e UPDATE
				Hubbard/O'Brien: Essentials of Economics 1e
				Hubbard/O'Brien: Essentials of Economics 2e
				Hubbard/O'Brien: Macroeconomics 2e
				Hubbard/O'Brien: Macroeconomics 2e UPDATE
				Hubbard/O'Brien: Macroeconomics 6e
				Hubbard/O'Brien: Microeconomics 2e
				Hubbard: Macroeconomics 1e DEMO
				James: Macroeconomics, In-Class Edition
				James: Microeconomics and Macroeconomics, In-Class Edition - Chapter 4 only
				James: Microeconomics, In-Class Edition
				Knewton Econ test book - Ishara
				Krugman/Obstfeld: International Economics 7e
				Krugman/Obstfeld: International Economics 8e
				Leeds/von Allmen/Schiming: Economics, 1e (new)
				Leeds/von Allmen/Schiming: Macroeconomics, 1e (new)
				Leeds/von Allmen/Schiming: Microeconomics, 1e (new)
				Lipsey/Ragan/Storer: Economics 13e
				Lipsey/Ragan/Storer: Macroeconomics 13e
				Lipsey/Ragan/Storer: Microeconomics 13e
				Miller et al.,  Economics Today: The Macro View, Fourth Canadian Edition
				Miller et al., Economics Today: The Micro View, Fourth Canadian Edition
				Miller: Economics Today 13e
				Miller: Economics Today 13e
				Miller: Economics Today 14e
				Miller: Economics Today 14e Summer 2007
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 14e
				Miller: Economics Today: The Macro View 14e Summer 2007
				Miller: Economics Today: The Macro View 15e
				Miller: Economics Today: The Macro View, 3ce
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 14e
				Miller: Economics Today: The Micro View 14e Summer 2007
				Miller: Economics Today: The Micro View, 3ce
				Mishkin: The Economics of Money, Banking, and Financial Markets 8e
				Mishkin: The Economics of Money, Banking, and Financial Markets Alternate
				MyEconLab Experiments and Digital Interactives (Master Sample)
				MyEconLab Experiments for TESTING ONLY
				NextGen Media Econ Test Book--REAL
				O'Sullivan/Sheffrin/Perez: Economics 5e
				O'Sullivan/Sheffrin/Perez: Macroeconomics 5e
				O'Sullivan/Sheffrin/Perez: Microeconomics 5e
				O'Sullivan/Sheffrin/Perez: Survey of Economics 3e
				Parkin/Bade, Economics, Sixth Edition
				Parkin/Bade, Macroeconomics, Sixth Edition
				Parkin/Bade, Microeconomics, Sixth Edition
				Parkin: Economics 8e
				Parkin: Economics seventh european edition
				Parkin: Macroeconomics 8e
				Parkin: Microeconomics 8e
				Perloff: Microeconomics, 4e
				Perloff: Microeconomics, 4e Demo
				Perloff: Microeconomics, 5e
				Pindyck/Rubinfeld: Microeconomics, 7e
				Ragan/Lipsey, Economics, 12ce
				Ragan/Lipsey, Macroeconomics, 12ce
				Ragan/Lipsey, Microeconomics, 12ce
				Ritter/Silber/Udell: Principles of Money, Banking &amp; Financial Markets, 12e
				Sloman: Essentials of Economics 4e
				z-Dummy Book for Data Grapher Wizard
				zBPTest Econ Medterm

			</value>
      <webElementGuid>4b99de0a-9978-4ef1-960b-e92be8eb15b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
      <webElementGuid>5e86ec4b-2706-46ea-9a3b-c73ef3cbc366</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      <webElementGuid>5fd86e75-aa4d-49d7-8532-45b6150f043f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
      <webElementGuid>b0dd55f0-7fe3-4927-a561-b7c65be121d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
      <webElementGuid>0e64e0d0-93c5-4a32-bb42-fdb5ac335f62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
      <webElementGuid>5c5084dd-2e38-4a5d-94f2-156ce3e9f763</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>075e2bef-da8b-49b2-b8e9-20cca57305d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				  -- Choose Book --  
				Abel/Bernanke/Croushore: Macroeconomics 6e
				Amplifire Test Book
				Bade/Parkin 3e for Testing
				Bade/Parkin: Demo for Foundations 3e
				Bade/Parkin: Essential Foundations of Economics 3e
				Bade/Parkin: Essential Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics 3e
				Bade/Parkin: Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics AP* Edition
				Bade/Parkin: Foundations of Macroeconomics 3e
				Bade/Parkin: Foundations of Macroeconomics 4e
				Bade/Parkin: Foundations of Microeconomics 3e
				Bade/Parkin: Foundations of Microeconomics 4e
				Case/Fair: Principles of Economics 8e
				Case/Fair: Principles of Economics 9e
				Case/Fair: Principles of Macroeconomics 8e
				Case/Fair: Principles of Macroeconomics 9e
				Case/Fair: Principles of Microeconomics 8e
				Case/Fair: Principles of Microeconomics 9e
				Cohen/Howe: Economics for Life: Smart Choices for You
				DEMO-Parkin, Economics: Canada in the Global Environment, Seventh Edition
				Ekelund 7e Standalone Testing
				Ekelund/Ressler/Tollison: Economics 7e (new)
				Ekelund/Ressler/Tollison: Macroeconomics 7e (new)
				Ekelund/Ressler/Tollison: Microeconomics 7e (new)
				Fremgen Test Course for Alison Doucette
				Group Work Test Book
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 1e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 6e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Microeconomics 2e
				Hubbard: Macroeconomics 1e DEMO
				James: Macroeconomics, In-Class Edition
				James: Microeconomics and Macroeconomics, In-Class Edition - Chapter 4 only
				James: Microeconomics, In-Class Edition
				Knewton Econ test book - Ishara
				Krugman/Obstfeld: International Economics 7e
				Krugman/Obstfeld: International Economics 8e
				Leeds/von Allmen/Schiming: Economics, 1e (new)
				Leeds/von Allmen/Schiming: Macroeconomics, 1e (new)
				Leeds/von Allmen/Schiming: Microeconomics, 1e (new)
				Lipsey/Ragan/Storer: Economics 13e
				Lipsey/Ragan/Storer: Macroeconomics 13e
				Lipsey/Ragan/Storer: Microeconomics 13e
				Miller et al.,  Economics Today: The Macro View, Fourth Canadian Edition
				Miller et al., Economics Today: The Micro View, Fourth Canadian Edition
				Miller: Economics Today 13e
				Miller: Economics Today 13e
				Miller: Economics Today 14e
				Miller: Economics Today 14e Summer 2007
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 14e
				Miller: Economics Today: The Macro View 14e Summer 2007
				Miller: Economics Today: The Macro View 15e
				Miller: Economics Today: The Macro View, 3ce
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 14e
				Miller: Economics Today: The Micro View 14e Summer 2007
				Miller: Economics Today: The Micro View, 3ce
				Mishkin: The Economics of Money, Banking, and Financial Markets 8e
				Mishkin: The Economics of Money, Banking, and Financial Markets Alternate
				MyEconLab Experiments and Digital Interactives (Master Sample)
				MyEconLab Experiments for TESTING ONLY
				NextGen Media Econ Test Book--REAL
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Economics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Macroeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Microeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Survey of Economics 3e
				Parkin/Bade, Economics, Sixth Edition
				Parkin/Bade, Macroeconomics, Sixth Edition
				Parkin/Bade, Microeconomics, Sixth Edition
				Parkin: Economics 8e
				Parkin: Economics seventh european edition
				Parkin: Macroeconomics 8e
				Parkin: Microeconomics 8e
				Perloff: Microeconomics, 4e
				Perloff: Microeconomics, 4e Demo
				Perloff: Microeconomics, 5e
				Pindyck/Rubinfeld: Microeconomics, 7e
				Ragan/Lipsey, Economics, 12ce
				Ragan/Lipsey, Macroeconomics, 12ce
				Ragan/Lipsey, Microeconomics, 12ce
				Ritter/Silber/Udell: Principles of Money, Banking &amp; Financial Markets, 12e
				Sloman: Essentials of Economics 4e
				z-Dummy Book for Data Grapher Wizard
				zBPTest Econ Medterm

			&quot;) or . = concat(&quot;
				  -- Choose Book --  
				Abel/Bernanke/Croushore: Macroeconomics 6e
				Amplifire Test Book
				Bade/Parkin 3e for Testing
				Bade/Parkin: Demo for Foundations 3e
				Bade/Parkin: Essential Foundations of Economics 3e
				Bade/Parkin: Essential Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics 3e
				Bade/Parkin: Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics AP* Edition
				Bade/Parkin: Foundations of Macroeconomics 3e
				Bade/Parkin: Foundations of Macroeconomics 4e
				Bade/Parkin: Foundations of Microeconomics 3e
				Bade/Parkin: Foundations of Microeconomics 4e
				Case/Fair: Principles of Economics 8e
				Case/Fair: Principles of Economics 9e
				Case/Fair: Principles of Macroeconomics 8e
				Case/Fair: Principles of Macroeconomics 9e
				Case/Fair: Principles of Microeconomics 8e
				Case/Fair: Principles of Microeconomics 9e
				Cohen/Howe: Economics for Life: Smart Choices for You
				DEMO-Parkin, Economics: Canada in the Global Environment, Seventh Edition
				Ekelund 7e Standalone Testing
				Ekelund/Ressler/Tollison: Economics 7e (new)
				Ekelund/Ressler/Tollison: Macroeconomics 7e (new)
				Ekelund/Ressler/Tollison: Microeconomics 7e (new)
				Fremgen Test Course for Alison Doucette
				Group Work Test Book
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 1e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 6e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Microeconomics 2e
				Hubbard: Macroeconomics 1e DEMO
				James: Macroeconomics, In-Class Edition
				James: Microeconomics and Macroeconomics, In-Class Edition - Chapter 4 only
				James: Microeconomics, In-Class Edition
				Knewton Econ test book - Ishara
				Krugman/Obstfeld: International Economics 7e
				Krugman/Obstfeld: International Economics 8e
				Leeds/von Allmen/Schiming: Economics, 1e (new)
				Leeds/von Allmen/Schiming: Macroeconomics, 1e (new)
				Leeds/von Allmen/Schiming: Microeconomics, 1e (new)
				Lipsey/Ragan/Storer: Economics 13e
				Lipsey/Ragan/Storer: Macroeconomics 13e
				Lipsey/Ragan/Storer: Microeconomics 13e
				Miller et al.,  Economics Today: The Macro View, Fourth Canadian Edition
				Miller et al., Economics Today: The Micro View, Fourth Canadian Edition
				Miller: Economics Today 13e
				Miller: Economics Today 13e
				Miller: Economics Today 14e
				Miller: Economics Today 14e Summer 2007
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 14e
				Miller: Economics Today: The Macro View 14e Summer 2007
				Miller: Economics Today: The Macro View 15e
				Miller: Economics Today: The Macro View, 3ce
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 14e
				Miller: Economics Today: The Micro View 14e Summer 2007
				Miller: Economics Today: The Micro View, 3ce
				Mishkin: The Economics of Money, Banking, and Financial Markets 8e
				Mishkin: The Economics of Money, Banking, and Financial Markets Alternate
				MyEconLab Experiments and Digital Interactives (Master Sample)
				MyEconLab Experiments for TESTING ONLY
				NextGen Media Econ Test Book--REAL
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Economics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Macroeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Microeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Survey of Economics 3e
				Parkin/Bade, Economics, Sixth Edition
				Parkin/Bade, Macroeconomics, Sixth Edition
				Parkin/Bade, Microeconomics, Sixth Edition
				Parkin: Economics 8e
				Parkin: Economics seventh european edition
				Parkin: Macroeconomics 8e
				Parkin: Microeconomics 8e
				Perloff: Microeconomics, 4e
				Perloff: Microeconomics, 4e Demo
				Perloff: Microeconomics, 5e
				Pindyck/Rubinfeld: Microeconomics, 7e
				Ragan/Lipsey, Economics, 12ce
				Ragan/Lipsey, Macroeconomics, 12ce
				Ragan/Lipsey, Microeconomics, 12ce
				Ritter/Silber/Udell: Principles of Money, Banking &amp; Financial Markets, 12e
				Sloman: Essentials of Economics 4e
				z-Dummy Book for Data Grapher Wizard
				zBPTest Econ Medterm

			&quot;))]</value>
      <webElementGuid>9eb7c777-d6b0-4bab-9217-5dc7e8aaed80</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
