<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Start                Availability      _168934</name>
   <tag></tag>
   <elementGuidId>05363d46-9a2c-44d1-8d05-4964ce88a6b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_DivContainer</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivContainer']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3d6fac11-e553-419e-ae19-093f8ba5d355</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_DivContainer</value>
      <webElementGuid>48cb426f-95ae-46ff-b9cc-0cc69a98f1cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


    
    
    

	
    
            
                Start
                Availability
                Group Admin
                Course Access
                Coverage
                Learning Aids and Test Options
            
    


	
    
            
                Start
                Availability
                Course Access
                Coverage
                Learning Aids and Test Options
            
    


 


  

    
        
            Type of course to create
            
                
	-- Choose --
	Standard
	Coordinator
	Member

Standard
                Learn about course types
            
        
        
            Course name
            
                Sample
            
        
        
	Course Code
	
                
                
		                    
                                           
                        

		
                            
                            Contact the Pearson administrator if the code for
your template course is not showing on this list.
                        
                    
                    
                
	
            

        
        
	Book
	
                Amplifire Test Book
            


        
        
        
	
                
            


        
            Primary instructor
            
	Prabhashi ins1

Prabhashi ins1
        
        
	
	
                
		
                
                
                    
                        
                        
                            If you change the instructor to a user other than yourself, and you click Save in the last step of this wizard, you will not be able to access these management screens again.  If you do continue now, you will be able to complete all the steps in this wizard before the instructor change will take affect.

                            Note: Only the user who has been designated as the instructor can access the course management screens.  You can skip this step now by setting your name as the instructor and continuing to the other steps in this wizard.  Then, you can return later to access this screen and select another instructor.
                        
                    
                
                
	
            


        
    
    




	
    
		
    
        How would you like to create your course?
        
            
                
                
			
				
				Create a new course
			
			
				
				Create a new preloaded course
			
			
				
				Copy one of my existing courses
			
			
				
				Copy a course from another instructor at my school
			
			
				
				Copy a course by specifying its Course ID
			
		
		
            
            
                
                    
                        
                        
			
                            Choose the textbook for this course                        
                            
				  -- Choose Book --  
				Abel/Bernanke/Croushore: Macroeconomics 6e
				Amplifire Test Book
				Bade/Parkin 3e for Testing
				Bade/Parkin: Demo for Foundations 3e
				Bade/Parkin: Essential Foundations of Economics 3e
				Bade/Parkin: Essential Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics 3e
				Bade/Parkin: Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics AP* Edition
				Bade/Parkin: Foundations of Macroeconomics 3e
				Bade/Parkin: Foundations of Macroeconomics 4e
				Bade/Parkin: Foundations of Microeconomics 3e
				Bade/Parkin: Foundations of Microeconomics 4e
				Case/Fair: Principles of Economics 8e
				Case/Fair: Principles of Economics 9e
				Case/Fair: Principles of Macroeconomics 8e
				Case/Fair: Principles of Macroeconomics 9e
				Case/Fair: Principles of Microeconomics 8e
				Case/Fair: Principles of Microeconomics 9e
				Cohen/Howe: Economics for Life: Smart Choices for You
				DEMO-Parkin, Economics: Canada in the Global Environment, Seventh Edition
				Ekelund 7e Standalone Testing
				Ekelund/Ressler/Tollison: Economics 7e (new)
				Ekelund/Ressler/Tollison: Macroeconomics 7e (new)
				Ekelund/Ressler/Tollison: Microeconomics 7e (new)
				Fremgen Test Course for Alison Doucette
				Group Work Test Book
				Hubbard/O'Brien: Economics 2e
				Hubbard/O'Brien: Economics 2e UPDATE
				Hubbard/O'Brien: Essentials of Economics 1e
				Hubbard/O'Brien: Essentials of Economics 2e
				Hubbard/O'Brien: Macroeconomics 2e
				Hubbard/O'Brien: Macroeconomics 2e UPDATE
				Hubbard/O'Brien: Macroeconomics 6e
				Hubbard/O'Brien: Microeconomics 2e
				Hubbard: Macroeconomics 1e DEMO
				James: Macroeconomics, In-Class Edition
				James: Microeconomics and Macroeconomics, In-Class Edition - Chapter 4 only
				James: Microeconomics, In-Class Edition
				Knewton Econ test book - Ishara
				Krugman/Obstfeld: International Economics 7e
				Krugman/Obstfeld: International Economics 8e
				Leeds/von Allmen/Schiming: Economics, 1e (new)
				Leeds/von Allmen/Schiming: Macroeconomics, 1e (new)
				Leeds/von Allmen/Schiming: Microeconomics, 1e (new)
				Lipsey/Ragan/Storer: Economics 13e
				Lipsey/Ragan/Storer: Macroeconomics 13e
				Lipsey/Ragan/Storer: Microeconomics 13e
				Miller et al.,  Economics Today: The Macro View, Fourth Canadian Edition
				Miller et al., Economics Today: The Micro View, Fourth Canadian Edition
				Miller: Economics Today 13e
				Miller: Economics Today 13e
				Miller: Economics Today 14e
				Miller: Economics Today 14e Summer 2007
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 14e
				Miller: Economics Today: The Macro View 14e Summer 2007
				Miller: Economics Today: The Macro View 15e
				Miller: Economics Today: The Macro View, 3ce
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 14e
				Miller: Economics Today: The Micro View 14e Summer 2007
				Miller: Economics Today: The Micro View, 3ce
				Mishkin: The Economics of Money, Banking, and Financial Markets 8e
				Mishkin: The Economics of Money, Banking, and Financial Markets Alternate
				MyEconLab Experiments and Digital Interactives (Master Sample)
				MyEconLab Experiments for TESTING ONLY
				NextGen Media Econ Test Book--REAL
				O'Sullivan/Sheffrin/Perez: Economics 5e
				O'Sullivan/Sheffrin/Perez: Macroeconomics 5e
				O'Sullivan/Sheffrin/Perez: Microeconomics 5e
				O'Sullivan/Sheffrin/Perez: Survey of Economics 3e
				Parkin/Bade, Economics, Sixth Edition
				Parkin/Bade, Macroeconomics, Sixth Edition
				Parkin/Bade, Microeconomics, Sixth Edition
				Parkin: Economics 8e
				Parkin: Economics seventh european edition
				Parkin: Macroeconomics 8e
				Parkin: Microeconomics 8e
				Perloff: Microeconomics, 4e
				Perloff: Microeconomics, 4e Demo
				Perloff: Microeconomics, 5e
				Pindyck/Rubinfeld: Microeconomics, 7e
				Ragan/Lipsey, Economics, 12ce
				Ragan/Lipsey, Macroeconomics, 12ce
				Ragan/Lipsey, Microeconomics, 12ce
				Ritter/Silber/Udell: Principles of Money, Banking &amp; Financial Markets, 12e
				Sloman: Essentials of Economics 4e
				z-Dummy Book for Data Grapher Wizard
				zBPTest Econ Medterm

			
                        
		
                        
			
                            Create a new preloaded course                        
                            
				  -- Choose Course --  
				MASTER Hubbard Macro 6e

			
                        
		
                        
			
                            
                        
		
                        
			
                            Choose a course to copy
                            
				  -- Choose Course --  
				

			TestCourse
                            
				 
			
                            
				Note: Your new member course will be a copy of this coordinator course.
			
                        
		
                        
			
                            
                            
				  -- Choose Course --  
				

			Econ_Course:Ins Jayani
                            
				
			
                            
				Note: Your new member course will be a copy of this coordinator course.
			
                        
		
                        
			
                            
                                                                                        
                            


    var tbA, tbB, tbC, tbD, tbFull;
    var onClientValidKeyPressed;
    var onClientCourseIDValidated;
    var onClientInvalidCourseID;
    var onClientIncompleteCourseID;

    var CourseIDEntry = {
        IsModified: false,

        Init: function () {
            tbA = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDA&quot;);
            tbB = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDB&quot;);
            tbC = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDC&quot;);
            tbD = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDD&quot;);
            tbFull = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseID&quot;);
            onClientValidKeyPressed = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientValidKeyPressed&quot;).value;
            onClientCourseIDValidated = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientCourseIDValidated&quot;).value;
            onClientInvalidCourseID = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientInvalidCourseID&quot;).value;
            onClientIncompleteCourseID = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientIncompleteCourseID&quot;).value;

            if (onClientInvalidCourseID == &quot;&quot;) {
                alert(&quot;OnClientInvalidCourseID handler not set&quot;);
                return false;
            }

        },
        GetCourseID: function () {
            return tbFull.value;
        },

        SyncAllTextBoxes: function () {
            if (tbFull.value != &quot;&quot; &amp;&amp; tbA.value == &quot;&quot;)
                copyFullToParts();
            else if (tbFull.value == &quot;&quot; &amp;&amp; tbA.value != &quot;&quot;)
                copyPartsToFull();
        },

        ValidateFullCourseID: function () {
            if (tbFull.value != '' &amp;&amp; onClientCourseIDValidated != '') {
                setTimeout(onClientCourseIDValidated + &quot;('&quot; + tbFull.value + &quot;')&quot;, 100);
            }
        }
    }


    var isShowingParts = true;

    eval(function (p, a, c, k, e, r) { e = function (c) { return (c &lt; a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] }]; e = function () { return '\\w+' }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]); return p }('q C(y){4 x=y.k(/-/g,\'\').k(/\\s/g,\'\');h(!t(x)||x.j(5)!=&quot;1&quot;)7-1;4 c=l(x.e(2,5)+x.e(6,8),m);4 b=l(x.e(9,n)+x.e(v,w),m);4 a=((o.p(c/f)+(c%f)+o.p(b/f)+(b%f))%A).B();h(a.u&lt;2)a=&quot;0&quot;+a;h(a!=x.j(8)+x.j(n))7-1;7 b}q t(y){4 x=y.k(/-/g,\'\').k(/\\s/g,\'\');4 r=/(D|E)(\\d|[a-z]){3}\\d(\\d|[a-z]){2}\\d(\\d|[a-z]){3}\\d(\\d|[a-z]){2}\\d/i;h(x.u==F&amp;&amp;x.j(5)==&quot;1&quot;&amp;&amp;r.G(x))7 H;7 I}q J(y){4 x=y.k(/-/g,\'\').k(/\\s/g,\'\');h(!t(x)||x.j(5)!=&quot;1&quot;)7-1;4 c=l(x.e(2,5)+x.e(6,8),m);4 b=l(x.e(9,n)+x.e(v,w),m);4 a=((o.p(c/f)+(c%f)+o.p(b/f)+(b%f))%A).B();h(a.u&lt;2)a=&quot;0&quot;+a;h(a!=x.j(8)+x.j(n))7-1;7 c}', 46, 46, '||||var|||return|||||||substring|10000||if||charAt|replace|parseInt|36|12|Math|floor|function||u2010|isxlid|length|13|15||||100|toString|gbid|XL|AM|16|test|true|false|gcid'.split('|'), 0, {}))



    function syncCourseIDs() {
        if (isShowingParts) {
            copyPartsToFull();
        }
        else {
            copyFullToParts();
        }
    }

    function copyPartsToFull() {
        // copy parts to full
        tbFull.value = tbA.value;
        if (tbB.value.length > 0) {
            tbFull.value += &quot;-&quot; + tbB.value;
            if (tbC.value.length > 0) {
                tbFull.value += &quot;-&quot; + tbC.value;
                if (tbD.value.length > 0)
                    tbFull.value += &quot;-&quot; + tbD.value;
            }
        }
    }

    function copyFullToParts() {
        // copy full to parts
        var str = tbFull.value.replace(/-/g, &quot;&quot;);
        if (str.length == 16) {
            tbA.value = str.substring(0, 4);
            tbB.value = str.substring(4, 8);
            tbC.value = str.substring(8, 12);
            tbD.value = str.substring(12, 16);
        }
        if (str.length == 0) {
            tbA.value = tbB.value = tbC.value = tbD.value = &quot;&quot;;
        }
    }

    function validateCourseIdSyntax() {
        var isValid = isxlid(tbFull.value);
        return isValid;
    }

    var check;
    function onFullFocus(tb) {
        check = setInterval(function () { checkFull(tb); }, 10);
    }

    function onFullBlur(tb) {
        tb.value = Trim(tb.value).toUpperCase();
        clearInterval(check);
        check = undefined;
    }

    function doFullKeyUp(evt, tb) {
        var keycode;
        if (window.event) {
            keycode = window.event.keyCode;
        } else if (evt) {
            keycode = evt.which;
        } else {
            return true;
        }

        if (onClientValidKeyPressed != '')
            eval(onClientValidKeyPressed);

        if ((keycode >= 48 &amp;&amp; keycode &lt;= 57)
            || (keycode >= 65 &amp;&amp; keycode &lt;= 90)
            || (keycode >= 96 &amp;&amp; keycode &lt;= 122)
            || keycode == 8
            || keycode == 46
            || keycode == 91)
            //|| keycode == 17) // Ignore ctrl key to prevent CheckEnrollmentByXlCourseID get called twice
        {
            checkFull(tb);
        }
    }

    var lastIdChecked = undefined;
    function checkFull(tb) {
        var upperCase = Trim(tb.value).toUpperCase();
        if (upperCase != tb.value)
            tb.value = upperCase;

        CourseIDEntry.IsModified = true;

        var len = tb.value.replace(/-/g, '').replace(/\u2010/g, '').length;
        if (len == 16) {
            if (validateCourseIdSyntax()) {
                syncCourseIDs();
                if (lastIdChecked != tb.value) {
                    CourseIDEntry.ValidateFullCourseID();
                    lastIdChecked = tb.value;
                }
            }
            else {
                if (onClientInvalidCourseID != &quot;&quot;)
                    eval(onClientInvalidCourseID);
            }
        }
        else if (len > 16) {
            if (onClientInvalidCourseID != &quot;&quot;)
                eval(onClientInvalidCourseID);
        }
        else {
            if (onClientIncompleteCourseID != &quot;&quot;)
                eval(onClientIncompleteCourseID);
            if (len == 0)
                syncCourseIDs();
        }
    }


    function doKeyUp(evt, box, tb) {
        var keycode
        if (window.event) {
            keycode = window.event.keyCode;
        } else if (evt) {
            keycode = evt.which;
        } else {
            return true;
        }

        if ((keycode >= 48 &amp;&amp; keycode &lt;= 57)
            || (keycode >= 65 &amp;&amp; keycode &lt;= 90)
            || (keycode >= 96 &amp;&amp; keycode &lt;= 122)
            || keycode == 8
            || keycode == 46
            || keycode == 91)
            //|| keycode == 17) // Ignore ctrl key to prevent CheckEnrollmentByXlCourseID get called twice
        {
            tb.value = Trim(tb.value).replace(/-/g, '').toUpperCase();

            if (onClientValidKeyPressed != '')
                eval(onClientValidKeyPressed);

            CourseIDEntry.IsModified = true;


            if (tb.value.length >= 4
                &amp;&amp; keycode != 8
                &amp;&amp; keycode != 46
                &amp;&amp; keycode != 91
                &amp;&amp; keycode != 17) {
                if (box == 'A') {
                    if (tb.value.length == 14 &amp;&amp; tb.value.substring(0, 2) != 'XL')
                        tb.value = &quot;XL&quot; + tb.value;

                    if (tb.value.length == 16) {
                        var str = tb.value;
                        tbA.value = str.substring(0, 4);
                        tbB.value = str.substring(4, 8);
                        tbC.value = str.substring(8, 12);
                        tbD.value = str.substring(12, 16);
                        tbD.focus();
                    }
                    else {
                        tbB.focus();
                    }
                }
                else if (box == 'B')
                    tbC.focus();
                else if (box == 'C')
                    tbD.focus();
            }

            if (tbA.value.length == 4 &amp;&amp; tbB.value.length == 4 &amp;&amp; tbC.value.length == 4 &amp;&amp; tbD.value.length == 4) {
                syncCourseIDs();
                if (validateCourseIdSyntax()) {
                    CourseIDEntry.ValidateFullCourseID();
                }
                else {
                    if (onClientInvalidCourseID != &quot;&quot;)
                        eval(onClientInvalidCourseID);
                }
            }
            else {
                if (onClientIncompleteCourseID != &quot;&quot;)
                    eval(onClientIncompleteCourseID);
            }
        }
    }

    function switchToParts() {
        isShowingParts = true;
        document.getElementById(&quot;courseIDfull&quot;).style.display = &quot;none&quot;;
        document.getElementById(&quot;courseIDparts&quot;).style.display = &quot;&quot;;
    }

    function switchToFull() {
        isShowingParts = false;
        document.getElementById(&quot;courseIDparts&quot;).style.display = &quot;none&quot;;
        document.getElementById(&quot;courseIDfull&quot;).style.display = &quot;&quot;;
    }

    //Check whether the pressed key is either &quot;Enter&quot; or &quot;Space&quot;
    function isEnterOrSpace(e) {
        var charCode = (typeof e.which == &quot;number&quot;) ? e.which : e.keyCode
        return (charCode == 13 || charCode == 32)
    }


    
        
        -
        
        -
        
        -
        
        
    
    
        Switch to single box for pasting your Course ID
    


    
          
        
    
    
        Switch to multiple boxes for typing your Course ID
    

        




                            
                                Sample Course ID:  XLAB-C1JK-2MNO-0YZ3 
                            
                            
                                Is this the course you want to copy?
                                
                                
                            
                            Note: Your new member course will be a copy of this coordinator course.
                            
                                
                                    
                                        
                                            
                                        
                                        
                                             
                                        
                                    
                                
                            
                            
                        
		
                    
                    
                    
                
            
        
    
    
	




    
    
        
            Course Availability
            
                
	                
                
                
                    
		
		This course is available to students
	
	
                    
                        
                            
                                
		Starting:
		
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;July 2022>>>
	

									
								
							
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

						
					
				
			
		
                                    
		   Ending:
		
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;July 2022>>>
	

									
								
							
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

						
					
				
			
		
                                    
	
	
                                
		Starting:
		
		   
		Ending:
		
	
	
                            
                        
                    
                    
                    
		
		This course is NOT available to students
	
	
                    
		
		Choose this option for coordinator course or courses used as templates for copying.
	
	
                
                

                
	
                
		
			
			This course is not available to students
		
	
	
                
		
			
			This course is available to students
		
		
			
			
                            
                                
                                    Starting:
                                    
                                       
                                    Ending:
                                    
                                
                            
                        
		
	
	                
                

            
        
        
	


        
	Assignment Availability
	
                
                    
                        Use Course Availability dates to schedule assignments
                    
                    Use alternate dates to schedule assignments:                    
                    
                        
                            
                                
                                    Starting:
					                
		
			
				RadDatePicker
			
				
					
				
			
				
					
						
							Calendar
						
							
								
									
										Title and navigation
									
										
											
										
									
	
		&lt;&lt;&lt;July 2022>>>
	

								
							
						
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

					
				
			
		
	
                                    
                                       
                                    
		
			
				RadDatePicker
			
				
					
				
			
				
					
						
							Calendar
						
							
								
									
										Title and navigation
									
										
											
										
									
	
		&lt;&lt;&lt;July 2022>>>
	

								
							
						
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

					
				
			
		
	
                                    
                                
                            
                        
                    
                
                
            

    
        
	Days Allowed
	
                
                    Sunday
                        Monday
                        Tuesday
                        Wednesday
                    
                    Thursday
                        Friday
                        Saturday
                         
                    
                
            

    
        
        
            Time Zone
            
                
                    
                        
                            
	(UTC-05:00) Eastern Time (US &amp; Canada)
	(UTC-05:00) Indiana (East)
	(UTC-06:00) Central Time (US &amp; Canada)
	(UTC-07:00) Mountain Time (US &amp; Canada)
	(UTC-07:00) Arizona
	(UTC-08:00) Pacific Time (US &amp; Canada)
	(UTC-08:00) Tijuana, Baja California
	(UTC-09:00) Alaska
	(UTC-10:00) Hawaii
	(UTC-12:00) International Date Line West
	(UTC-07:00) Chihuahua, La Paz, Mazatlan
	(UTC-11:00) Midway Island, Samoa
	(UTC-06:00) Central America
	(UTC-06:00) Guadalajara, Mexico City, Monterrey
	(UTC-06:00) Saskatchewan
	(UTC-05:00) Bogota, Lima, Quito, Rio Branco
	(UTC-04:30) Caracas
	(UTC-04:00) Atlantic Time (Canada)
	(UTC-04:00) La Paz
	(UTC-04:00) Manaus
	(UTC-04:00) Santiago
	(UTC-03:30) Newfoundland
	(UTC-03:00) Brasilia
	(UTC-03:00) Buenos Aires
	(UTC-03:00) Georgetown
	(UTC-03:00) Greenland
	(UTC-03:00) Montevideo
	(UTC-02:00) Mid-Atlantic
	(UTC-01:00) Azores
	(UTC-01:00) Cape Verde Is.
	(UTC) Casablanca
	(UTC) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London
	(UTC) Monrovia, Reykjavik
	(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna
	(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague
	(UTC+01:00) Brussels, Copenhagen, Madrid, Paris
	(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb
	(UTC+01:00) West Central Africa
	(UTC+02:00) Amman
	(UTC+02:00) Athens, Bucharest, Istanbul
	(UTC+02:00) Beirut
	(UTC+02:00) Cairo
	(UTC+02:00) Harare, Pretoria
	(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius
	(UTC+02:00) Jerusalem
	(UTC+02:00) Minsk
	(UTC+02:00) Windhoek
	(UTC+03:00) Baghdad
	(UTC+03:00) Kuwait, Riyadh
	(UTC+03:00) Moscow, St. Petersburg, Volgograd
	(UTC+03:00) Nairobi
	(UTC+03:00) Tbilisi
	(UTC+03:30) Tehran
	(UTC+04:00) Abu Dhabi, Muscat
	(UTC+04:00) Baku
	(UTC+04:00) Caucasus Standard Time
	(UTC+04:00) Port Louis
	(UTC+04:00) Yerevan
	(UTC+04:30) Kabul
	(UTC+05:00) Ekaterinburg
	(UTC+05:00) Islamabad, Karachi
	(UTC+05:00) Tashkent
	(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi
	(UTC+05:30) Sri Jayawardenepura
	(UTC+05:45) Kathmandu
	(UTC+06:00) Astana, Dhaka
	(UTC+06:00) Almaty, Novosibirsk
	(UTC+06:30) Yangon (Rangoon)
	(UTC+07:00) Bangkok, Hanoi, Jakarta
	(UTC+07:00) Krasnoyarsk
	(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi
	(UTC+08:00) Irkutsk, Ulaan Bataar
	(UTC+08:00) Perth
	(UTC+08:00) Kuala Lumpur, Singapore
	(UTC+08:00) Taipei
	(UTC+09:00) Osaka, Sapporo, Tokyo
	(UTC+09:00) Seoul
	(UTC+09:00) Yakutsk
	(UTC+09:30) Adelaide
	(UTC+09:30) Darwin
	(UTC+10:00) Brisbane
	(UTC+10:00) Canberra, Melbourne, Sydney
	(UTC+10:00) Guam, Port Moresby
	(UTC+10:00) Hobart
	(UTC+10:00) Vladivostok
	(UTC+11:00) Magadan, Solomon Is., New Caledonia
	(UTC+12:00) Auckland, Wellington
	(UTC+12:00) Fiji, Kamchatka, Marshall Is.
	(UTC+13:00) Nuku'alofa


                            
                                Adjust Automatically for Daylight Savings Time
                            
                        
                    
                    

                    
                        
                            Update in member courses
                        
                    

                    
                        Current course time: 9:51am
                    
                    
                    
	
                             
                            If you change your course time zone, go to the Change Dates &amp; Assign Status page to review and adjust your assignment dates and times.
                        


                
            

        
        
            
	


            
	Date Format
	
                    
                        
                            
                                
		MM/DD/YYYY
		DD/MM/YYYY

	
                            
                        
                    
                


            
	


            
	Copying
	Allow other instructors to copy this course.


        
    


    
    
    
    
        
	
                
                This is a standard course and is not part of a course group.
                If no students or section instructors are enrolled, you can change this course to a coordinator course by returning to Step 1 and changing the course Type to Coordinator.  More about coordinator and member courses.
                
            


        
	
                
                    
                        You are creating a coordinator course.  You can use this course to administer assignments for multiple member courses.  As members join your course group, they are listed below.  
                        Go to Edit Roster in the Gradebook to modify course access for individual instructors.
                        More about coordinator and member courses.
                    
                    Member SettingsAllow member courses to remove themselves from the group
                    
                        
		
			
				Course typeCourse nameCourse InstructorsCourse AccessRemove from Group
			
				Coordinator Courses
                                    None
                                
                                    
                                
                                    
                                    Full instructor
                                    
                                
                                        
                                
			
				Member Courses
                                    None
                                
                                    
                                
                                    
                                    
                                    
                                
                                        
                                
			
		
	
                    
                
            


        
	
                
                    
                        You are creating a member course within a coordinated course group.  The coordinator and the other members of your course group are listed below.  
                        Go to Edit Roster in the Gradebook to modify course access for individual instructors.
                        Learn more about coordinator and member courses.
                    
                    
                        

	
                    
                
            


    


    

    
	
        
              
        
        
        
        


var T = true;
var F = false;
//var _s = { ID: -1, Checked: T, Children: new Array() }; // standards array
var _partiallyIncludedValuePaths = new Array();
var _partiallyIncludedValues = new Array();

var chapters=new Array();
chapters[1]=T;

var sections=new Array();
sections[1]=new Array();
sections[1][1]=T;
sections[1][2]=T;

var objectives=new Array();
objectives[1]=new Array();
objectives[1][1]=new Array();
objectives[1][2]=new Array();


var UnCheckedCh=new Array();
var UnCheckedSec=new Array();
var UnCheckedObj=new Array();
var UnCheckedStandards=new Array();

var CheckedCh=new Array();
var CheckedSec=new Array();
var CheckedObj=new Array();
var CheckedStandards=new Array();

var Ccs = {
    UsePartiallyIncludedLabels: true,
    PartiallyIncludedLabel: ' &amp;nbsp;(partially included)',
    UsesObjectives: false, //
    IsForCompanionStudyPlan: false,
    SupportsPostHeightMessage: false,   
    PostHeightMessageContainerID: null,
    
    //

    SaveLists: function() {
        UnCheckedCh = new Array();
        UnCheckedSec = new Array();
        UnCheckedObj = new Array();
        CheckedCh = new Array();
        CheckedSec = new Array();
        CheckedObj = new Array();
        GetUncheckedChapters();
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenChapters&quot;).value = UnCheckedCh.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenSections&quot;).value = UnCheckedSec.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenObjectives&quot;).value = UnCheckedObj.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedChaptersHiddenField&quot;).value = CheckedCh.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedSectionsHiddenField&quot;).value = CheckedSec.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedObjectivesHiddenField&quot;).value = CheckedObj.join(&quot;,&quot;);

        if (typeof(_s) != 'undefined' &amp;&amp; _s != null)
            Ccs.SaveStandards();

        if (typeof(BookContentAreaChaptersSelector) != &quot;undefined&quot; &amp;&amp; BookContentAreaChaptersSelector != null)
            BookContentAreaChaptersSelector.Save();
    },
    SaveStandards: function() {
        var hdn = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenStandardItems&quot;);
        var selectedStandardItemsHiddenField = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedStandardItemsHiddenField&quot;);

        UnCheckedStandards = new Array();
        CheckedStandards = new Array();

        if (_s.Children.length > 0) {
            Ccs.SaveStandardsChildren(_s);
            hdn.value = UnCheckedStandards.join(&quot;,&quot;);
            selectedStandardItemsHiddenField.value = CheckedStandards.join(&quot;,&quot;);
        } else {
            hdn.value = '';
            selectedStandardItemsHiddenField.value = '';
        }
    },
    SaveStandardsChildren: function(item) {

        if (item.ID > 0) {
            if (!item.Checked)
                UnCheckedStandards.push(item.ID);
            else
                CheckedStandards.push(item.ID);
        }
        for (var i = 0; i &lt; item.Children.length; i++) {
            Ccs.SaveStandardsChildren(item.Children[i]);
        }
    },
    SetSelectedChapterCoverage: function(csvList) {
        Ccs.SetChapterCoverage(csvList, T);
    },

    SetHiddenChapterCoverage: function(csvList) {
        Ccs.SetChapterCoverage(csvList, F);
    },

    SetChapterCoverage: function(csvList, boolValue) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(',');
        for (var i = 0; i &lt; arr.length; i++) {
            var parts = arr[i].split('/');
            if (parts.length == 1) // chapter
                chapters[Number(parts[0])] = boolValue;
            else if (parts.length == 2) // section
                sections[Number(parts[0])][Number(parts[1])] = boolValue;
            else if (parts.length == 3) // objective
                objectives[Number(parts[0])][Number(parts[1])][Number(parts[2])] = boolValue;
        }
        ApplyCoverage();
    },


    Check: function (strBookPart) {
        this.SetChecked(strBookPart, T);
    },
    UnCheck: function (strBookPart) {
        this.SetChecked(strBookPart, F);
    },
    SetChecked: function (strBookPart, boolValue) {
        if (strBookPart == null || strBookPart.length == 0)
            return;
       
        var parts = strBookPart.split(&quot;.&quot;);

        if (this.UsesObjectives)
            SetObjectiveValue(Number(parts[0]), Number(parts[1]), Number(parts[2]), boolValue);
        else
            SetSectionValue(Number(parts[0]), Number(parts[1]), boolValue);

        ApplyCoverage();
        UpdateArrayValues();
    },
    CheckSelected: function (csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(',');
        arr = removeEmptyElementsFromArray(arr);

        for (var i = 0; i &lt; arr.length; i++) {
            var parts = arr[i].split('.');
            if (parts.length == 1) // chapter
                SetChapterValue(Number(parts[0]), T);
            else if (parts.length == 2) // section
                SetSectionValue(Number(parts[0]), Number(parts[1]), T);
            else if (parts.length == 3) // objective
                SetObjectiveValue(Number(parts[0]), Number(parts[1]), Number(parts[2]), T);
        }

        ApplyCoverage();
        UpdateArrayValues();
    },
    CheckAll: function () {
        SetAllArrayValues(true);
        UpdateArrayValues();
    },
    UnCheckAll: function () {
        SetAllArrayValues(false);
        UpdateArrayValues();
    },
    UpdateArrayValues: function () {
        UpdateArrayValues();
    },
    GetTotalCheckedAtLowestLevel: function () {
        if (this.UsesObjectives)
            return CheckedObj.length;
        return CheckedSec.length;
    },
    SetSelectedChaptersHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedChaptersHiddenField&quot;).value = csvList;
    },
    SetSelectedSectionsHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedSectionsHiddenField&quot;).value = csvList;
    },
    SetSelectedObjectivesHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedObjectivesHiddenField&quot;).value = csvList;
    },

    SetHiddenStandards: function(csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(',');
        for (var i = 0; i &lt; arr.length; i++) {
            HideStandard(Number(arr[i]), _s);
        }
        ApplyCoverageStandardView();
    },
    SetSelectedStandards: function(csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(',');
        for (var i = 0; i &lt; arr.length; i++) {
            ShowStandard(arr[i]);
        }
        ApplyCoverageStandardView();
    },
    SetOverallCoverageChangedFlag: function(isChanged) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_OverallCoverageChangedHiddenField&quot;).value = isChanged ? &quot;1&quot; : &quot;0&quot;;
    },

    SetTabSwitchedFlag: function() {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TabSwitchedHiddenField&quot;).value = &quot;1&quot;;
    },

    ClearTabSwitchedFlag: function() {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TabSwitchedHiddenField&quot;).value = &quot;0&quot;;
    },

    SetCoverageChangedFlag: function() {
        if (GetSelectedTab() == &quot;standard&quot;)
            document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_StandardCoverageChangedHiddenField&quot;).value = &quot;1&quot;;
        else
            document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_ChapterCoverageChangedHiddenField&quot;).value = &quot;1&quot;;
    },
    // &quot;standards&quot; or &quot;chapters&quot;
    GetCoverageBasis: function () {
        return $('#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input:checked').val();
    }
};

    function pageLoad(sender, args) {

        if (typeof(IsProgressControlDisplay) == &quot;function&quot; &amp;&amp; IsProgressControlDisplay()) {
            return;
        }
        OnClientPageLoad(sender, args);

        //
	    $(&quot;#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input&quot;).change(function() {
            DisableTreeBasedOnCoverageBasis();
	    });

        // 
        
        
        // During partial page loads (using UpdatePanels) we need to fix the scroll bars
	    var prm = Sys.WebForms.PageRequestManager.getInstance();
	    prm.add_pageLoaded(UpdatePanelRefreshed);

   	
        

        if (GetVisibleTree() != null)
        {
            
        
            var hiddenBookContentAreaChaptersSelectorClientPageLoadCode = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenBookContentAreaChaptersSelectorClientPageLoadCode&quot;);
            if (hiddenBookContentAreaChaptersSelectorClientPageLoadCode.value != '')
                eval(hiddenBookContentAreaChaptersSelectorClientPageLoadCode.value);

            if (GetSelectedTab() == &quot;standard&quot;)
            {
                ApplyCoverageStandardView();
                PartiallyIncludeValues();
            }
            else
            {
                ApplyCoverage();
                PartiallyIncludeValuePaths();
            }
     
            DisableTreeBasedOnCoverageBasis();

            Ccs.ClearTabSwitchedFlag();
        }  
    }

    // 
    function DisableTreeBasedOnCoverageBasis() {
        // 
        var coverageBasis = $('#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input:checked').val();
        
        var tree = GetVisibleTree();
        if (typeof (tree) == 'undefined' || tree == null || typeof (tree.RootNode) == 'undefined' || tree.RootNode == null)
            return false;
        
        var selectedTab = GetSelectedTab();
        if ((selectedTab == &quot;standard&quot; &amp;&amp; coverageBasis == &quot;chapters&quot;) || (selectedTab == &quot;chapter&quot; &amp;&amp; coverageBasis == &quot;standards&quot;))
            tree.CheckBoxesDisabled = true;
        else
            tree.CheckBoxesDisabled = false;

        SetRootCheckBoxesEnabled(tree.RootNode, !tree.CheckBoxesDisabled);
        
        return true;
    }


    function UpdatePanelRefreshed()
    {
        fixScrollbars();
        SubAllowSubmit();	    
    }

    function IPTreeView_NodeTextClicked(node) {
        if (GetSelectedTab() == &quot;standard&quot;) {
            if (node.Nodes.length > 0) // has children?
                return true;
        }
        else if ((node.Level() == 0) || (node.Level() == 1 &amp;&amp; Ccs.UsesObjectives))
            return true;
    }
    
    function ExpandAllClient()
    {
        var treeview = GetVisibleTree();

        if (typeof (treeview) == 'undefined' || treeview == null || typeof (treeview.RootNode) == 'undefined' || treeview.RootNode == null)
            return false;
        
        var startNode=treeview.RootNode;
        
        for (var i=0; i&lt;startNode.Nodes.length; i++) //chapters
        {
            startNode.Nodes[i].ExpandAll();
        }

        IPTreeView_OnAfterNodeIconClicked();

        fixScrollbars();
	}

	function ExpandAll()
    {
        Ccs.SaveLists();
        SetFormTracker(true);
	}
	
    // Note: Because treeview.js is loaded in PreRender by IPTreeView, these overrides have to be outside of
    // HeadContent or else they happen before the include gets there and get overriden themselves
    // So now we have a use for PreContent... or we find another way to deal with it
	function IPTreeView_NodeIconClicked(node) {
        return true;
	}

	function IPTreeView_OnAfterNodeIconClicked() {
	    PostHeightMessage();
	}

	function IPTreeView_OnAfterNodeTextClicked() {
	    PostHeightMessage();
	}

	function PostHeightMessage() {
	    if (Ccs.SupportsPostHeightMessage) {

	        var containerEl = document.getElementById(Ccs.PostHeightMessageContainerID);
	        if (containerEl) {

	            var scrollHeight = containerEl.scrollHeight;
	            if (parent &amp;&amp; parent.postMessage) {
	                parent.postMessage(scrollHeight, &quot;*&quot;);
	                fixScrollbars();
	            }
	        }
	    }
	}
      
    function UpdateNode(nodeId)
    {
    }
     
    // CA Returns either the tree JS instance or null if none is available - useful to tell if the tree has been rendered (e.g., Course Wizard after step 1)
    // NOTE: $find and document.getElementById do not work to retrieve the tree. 
    function GetVisibleTree() {
        if (GetSelectedTab() == &quot;standard&quot;) {
            return (typeof(ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeStandards) == &quot;undefined&quot;) ? null : ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeStandards;
        } else {
            return (typeof(ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeChapters) == &quot;undefined&quot;) ? null : ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeChapters;
        }
    }

    function IPTreeView_NodePopulated(pNode) {
        if (GetSelectedTab() == &quot;standard&quot;)
        {
            ApplyCoverageStandardView(pNode);
            PartiallyIncludeValues();
        }
        else
        {
            ApplyCoverage(pNode);
            PartiallyIncludeValuePaths();
        }

        IPTreeView_OnAfterNodeIconClicked();

        setTimeout('fixScrollbars();',100);
    }
    
    

    function IPTreeView_CheckboxClicked(node) {
        Ccs.SetCoverageChangedFlag();
        
        SetFormTracker(true);

        IPTreeView_OnClientCheckboxClicked();

        node.ToggleCheck();

        var ret = false;
        if (GetSelectedTab() == &quot;standard&quot;)
            ret = StandardViewCheckboxClicked(node);
        else
            ret = ChapterCoverageCheckboxClicked(node);

        // Req C14-8: Companion Study Plan
        if (Ccs.IsForCompanionStudyPlan) {
            Ccs.UpdateArrayValues();
            UpdateTotalMasteryPoints(Ccs.GetTotalCheckedAtLowestLevel());
        }

        return ret;
    }

    function UpdateTotalMasteryPoints(val)
    {
        if (parent &amp;&amp; parent.CREATEASSIGNMENT_UpdateTotalMasteryPoints)
            parent.CREATEASSIGNMENT_UpdateTotalMasteryPoints(val);
    }

    function COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked()  // CA prefix to avoid conflict with method called OnClientContentAreaCheckboxClicked on the page using this control.
    {
        Ccs.SetCoverageChangedFlag();
        
        OnClientContentAreaCheckboxClicked();
    }

    // CA Content Area filter handling
    function OnClientContentAreaSelected(chapterIDArray)
    {
        var tree = GetVisibleTree();

    if (typeof (tree) == 'undefined' || tree == null || typeof (tree.RootNode) == 'undefined' || tree.RootNode == null)
        return false;

        var chapterSelected = false;       

        for (var i = 0; i &lt; chapterIDArray.length; i++)
        {
            var node = tree.FindTopLevelNode(chapterIDArray[i]);
            if (node != null)
            {
                node.UnfilterRow();

                if (node.Checked)
                    chapterSelected = true;
            }
        }

        //
        
        if (!chapterSelected)
            for (var i = 0; i &lt; chapterIDArray.length; i++)
            {
                var node = tree.FindTopLevelNode(chapterIDArray[i]);
                if (node != null)
                {
                    NodeSetCheck(node, true);
                    ChapterCoverageCheckboxClicked(node);
                }
            }

        COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked();

        return true;
    }

    function OnClientContentAreaCleared(chapterIDArray)
    {
        var tree = GetVisibleTree();
        
        if (typeof (tree) == 'undefined' || tree == null || typeof (tree.RootNode) == 'undefined' || tree.RootNode == null)
            return false;

        for (var i = 0; i &lt; chapterIDArray.length; i++)
        {
            var node = tree.FindTopLevelNode(chapterIDArray[i]);
            if (node != null)
                node.FilterRow();
        }

        COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked();

        return true;
    }
    
    function LinkButtonWritingPracticeMoreInfo_onclick() {
        var url = 'http://www.mywritinglab.com/writingpractice.html';
        popupWindow(&quot;WritingPracticeMoreInfo&quot;,url,{Width:800,Height:500,AutoClose:true,Titlebar:true,Statusbar:false});
    }


 


		
        
        
        
    
    
    
        
    
    
    
    
    
    
    
    

    
    
    

    
    
    

    
    
    
			
        
        
                
                
                    
                        
                            Included sections                            
                            
                                Expand All
                            
                        
                    
                
                
                
				
					
						
							
								DSM. Dynamic Study Modules  
							
						
					
						
					
				
			
            
    
		
    
	
 
        

   

        
    
    
    
    
        
	
                LockDown Browser and Proctoring
            
	
                
                Use the Pearson LockDown Browser in this course  More Options
                
                    
                        
                        The Pearson LockDown Browser is not compatible with mobile devices or Chromebooks.
                    
                
                
                Use automated proctoring in this course
                
                    
		Select proctoring partner
		ProctorU Record+
		Respondus Monitor Proctoring

	
                
                    
                        
                        
                            
                                ProctorU Key:
                            
                                
                        
                        
                            
                                ProctorU Secret:
                            
                                
                        
                        
                    
                
                    
                        After you create your course, complete the Respondus Monitor Proctoring license setup.
                        Complete the Respondus Monitor Proctoring license setup
                    
                    
                
                


        
                
        
        
	
                IP Address Range
            
	
                Require students to take IP-restricted quizzes and tests within the following IP address range:
                
                    None  
                    Change...
                
            


        
       
        
                
        
        
            
                Multimedia Learning Aids
                
            
                Choose the multimedia learning aids that are available in this course.
            
        
    
    			
    
        
            
            
                Learning Aids
                
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                
	
	
	Ask My InstructorEdit Email Address
                                              
                                              prabhashi.hettiarachchi@pearson.com
                                              
	


                
            
        
    



    
        
            Section instructor access      
            
                Select below to choose a course level access setting.
                Go to Edit Roster in the Gradebook to modify course access for individual instructors.
            
        
        
            
            Grant full instructor access.
        
        
            
            Restrict access to the following access type:
        
        
            
            Section instructor
        
        
            
            Read-only
        
        
            
            Custom (No options selected)  Define...
            
        
    
    
    
        
            Other restricted access privileges
            Edit Ask My Instructor email address
        
        
            
            Edit course roster
        
    




    
        Cancel
Save
Next
Back

    
</value>
      <webElementGuid>9910a2ab-ecde-4ded-876a-a2efbf6aa568</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_DivContainer&quot;)</value>
      <webElementGuid>8c0f3051-741c-41d7-8a57-edd5e85a2492</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivContainer']</value>
      <webElementGuid>fb7cf570-ce02-42e2-8b09-5425decb2370</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]</value>
      <webElementGuid>324c88b3-cf68-46ad-a934-d2259dbd4174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::div[1]</value>
      <webElementGuid>97855b90-989a-4da5-9858-3a6d4efde428</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about New Course'])[1]/following::div[1]</value>
      <webElementGuid>6b7eb04f-e7dc-4ada-941c-ec3293b59d94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]</value>
      <webElementGuid>9835b439-e0e5-4607-a014-02e0650bad7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ctl00_ctl00_InsideForm_MasterContent_DivContainer' and (text() = concat(&quot;


    
    
    

	
    
            
                Start
                Availability
                Group Admin
                Course Access
                Coverage
                Learning Aids and Test Options
            
    


	
    
            
                Start
                Availability
                Course Access
                Coverage
                Learning Aids and Test Options
            
    


 


  

    
        
            Type of course to create
            
                
	-- Choose --
	Standard
	Coordinator
	Member

Standard
                Learn about course types
            
        
        
            Course name
            
                Sample
            
        
        
	Course Code
	
                
                
		                    
                                           
                        

		
                            
                            Contact the Pearson administrator if the code for
your template course is not showing on this list.
                        
                    
                    
                
	
            

        
        
	Book
	
                Amplifire Test Book
            


        
        
        
	
                
            


        
            Primary instructor
            
	Prabhashi ins1

Prabhashi ins1
        
        
	
	
                
		
                
                
                    
                        
                        
                            If you change the instructor to a user other than yourself, and you click Save in the last step of this wizard, you will not be able to access these management screens again.  If you do continue now, you will be able to complete all the steps in this wizard before the instructor change will take affect.

                            Note: Only the user who has been designated as the instructor can access the course management screens.  You can skip this step now by setting your name as the instructor and continuing to the other steps in this wizard.  Then, you can return later to access this screen and select another instructor.
                        
                    
                
                
	
            


        
    
    




	
    
		
    
        How would you like to create your course?
        
            
                
                
			
				
				Create a new course
			
			
				
				Create a new preloaded course
			
			
				
				Copy one of my existing courses
			
			
				
				Copy a course from another instructor at my school
			
			
				
				Copy a course by specifying its Course ID
			
		
		
            
            
                
                    
                        
                        
			
                            Choose the textbook for this course                        
                            
				  -- Choose Book --  
				Abel/Bernanke/Croushore: Macroeconomics 6e
				Amplifire Test Book
				Bade/Parkin 3e for Testing
				Bade/Parkin: Demo for Foundations 3e
				Bade/Parkin: Essential Foundations of Economics 3e
				Bade/Parkin: Essential Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics 3e
				Bade/Parkin: Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics AP* Edition
				Bade/Parkin: Foundations of Macroeconomics 3e
				Bade/Parkin: Foundations of Macroeconomics 4e
				Bade/Parkin: Foundations of Microeconomics 3e
				Bade/Parkin: Foundations of Microeconomics 4e
				Case/Fair: Principles of Economics 8e
				Case/Fair: Principles of Economics 9e
				Case/Fair: Principles of Macroeconomics 8e
				Case/Fair: Principles of Macroeconomics 9e
				Case/Fair: Principles of Microeconomics 8e
				Case/Fair: Principles of Microeconomics 9e
				Cohen/Howe: Economics for Life: Smart Choices for You
				DEMO-Parkin, Economics: Canada in the Global Environment, Seventh Edition
				Ekelund 7e Standalone Testing
				Ekelund/Ressler/Tollison: Economics 7e (new)
				Ekelund/Ressler/Tollison: Macroeconomics 7e (new)
				Ekelund/Ressler/Tollison: Microeconomics 7e (new)
				Fremgen Test Course for Alison Doucette
				Group Work Test Book
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 1e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 6e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Microeconomics 2e
				Hubbard: Macroeconomics 1e DEMO
				James: Macroeconomics, In-Class Edition
				James: Microeconomics and Macroeconomics, In-Class Edition - Chapter 4 only
				James: Microeconomics, In-Class Edition
				Knewton Econ test book - Ishara
				Krugman/Obstfeld: International Economics 7e
				Krugman/Obstfeld: International Economics 8e
				Leeds/von Allmen/Schiming: Economics, 1e (new)
				Leeds/von Allmen/Schiming: Macroeconomics, 1e (new)
				Leeds/von Allmen/Schiming: Microeconomics, 1e (new)
				Lipsey/Ragan/Storer: Economics 13e
				Lipsey/Ragan/Storer: Macroeconomics 13e
				Lipsey/Ragan/Storer: Microeconomics 13e
				Miller et al.,  Economics Today: The Macro View, Fourth Canadian Edition
				Miller et al., Economics Today: The Micro View, Fourth Canadian Edition
				Miller: Economics Today 13e
				Miller: Economics Today 13e
				Miller: Economics Today 14e
				Miller: Economics Today 14e Summer 2007
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 14e
				Miller: Economics Today: The Macro View 14e Summer 2007
				Miller: Economics Today: The Macro View 15e
				Miller: Economics Today: The Macro View, 3ce
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 14e
				Miller: Economics Today: The Micro View 14e Summer 2007
				Miller: Economics Today: The Micro View, 3ce
				Mishkin: The Economics of Money, Banking, and Financial Markets 8e
				Mishkin: The Economics of Money, Banking, and Financial Markets Alternate
				MyEconLab Experiments and Digital Interactives (Master Sample)
				MyEconLab Experiments for TESTING ONLY
				NextGen Media Econ Test Book--REAL
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Economics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Macroeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Microeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Survey of Economics 3e
				Parkin/Bade, Economics, Sixth Edition
				Parkin/Bade, Macroeconomics, Sixth Edition
				Parkin/Bade, Microeconomics, Sixth Edition
				Parkin: Economics 8e
				Parkin: Economics seventh european edition
				Parkin: Macroeconomics 8e
				Parkin: Microeconomics 8e
				Perloff: Microeconomics, 4e
				Perloff: Microeconomics, 4e Demo
				Perloff: Microeconomics, 5e
				Pindyck/Rubinfeld: Microeconomics, 7e
				Ragan/Lipsey, Economics, 12ce
				Ragan/Lipsey, Macroeconomics, 12ce
				Ragan/Lipsey, Microeconomics, 12ce
				Ritter/Silber/Udell: Principles of Money, Banking &amp; Financial Markets, 12e
				Sloman: Essentials of Economics 4e
				z-Dummy Book for Data Grapher Wizard
				zBPTest Econ Medterm

			
                        
		
                        
			
                            Create a new preloaded course                        
                            
				  -- Choose Course --  
				MASTER Hubbard Macro 6e

			
                        
		
                        
			
                            
                        
		
                        
			
                            Choose a course to copy
                            
				  -- Choose Course --  
				

			TestCourse
                            
				 
			
                            
				Note: Your new member course will be a copy of this coordinator course.
			
                        
		
                        
			
                            
                            
				  -- Choose Course --  
				

			Econ_Course:Ins Jayani
                            
				
			
                            
				Note: Your new member course will be a copy of this coordinator course.
			
                        
		
                        
			
                            
                                                                                        
                            


    var tbA, tbB, tbC, tbD, tbFull;
    var onClientValidKeyPressed;
    var onClientCourseIDValidated;
    var onClientInvalidCourseID;
    var onClientIncompleteCourseID;

    var CourseIDEntry = {
        IsModified: false,

        Init: function () {
            tbA = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDA&quot;);
            tbB = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDB&quot;);
            tbC = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDC&quot;);
            tbD = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDD&quot;);
            tbFull = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseID&quot;);
            onClientValidKeyPressed = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientValidKeyPressed&quot;).value;
            onClientCourseIDValidated = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientCourseIDValidated&quot;).value;
            onClientInvalidCourseID = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientInvalidCourseID&quot;).value;
            onClientIncompleteCourseID = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientIncompleteCourseID&quot;).value;

            if (onClientInvalidCourseID == &quot;&quot;) {
                alert(&quot;OnClientInvalidCourseID handler not set&quot;);
                return false;
            }

        },
        GetCourseID: function () {
            return tbFull.value;
        },

        SyncAllTextBoxes: function () {
            if (tbFull.value != &quot;&quot; &amp;&amp; tbA.value == &quot;&quot;)
                copyFullToParts();
            else if (tbFull.value == &quot;&quot; &amp;&amp; tbA.value != &quot;&quot;)
                copyPartsToFull();
        },

        ValidateFullCourseID: function () {
            if (tbFull.value != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; &amp;&amp; onClientCourseIDValidated != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                setTimeout(onClientCourseIDValidated + &quot;(&quot; , &quot;'&quot; , &quot;&quot; + tbFull.value + &quot;&quot; , &quot;'&quot; , &quot;)&quot;, 100);
            }
        }
    }


    var isShowingParts = true;

    eval(function (p, a, c, k, e, r) { e = function (c) { return (c &lt; a ? &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] }]; e = function () { return &quot; , &quot;'&quot; , &quot;\\w+&quot; , &quot;'&quot; , &quot; }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp(&quot; , &quot;'&quot; , &quot;\\b&quot; , &quot;'&quot; , &quot; + e(c) + &quot; , &quot;'&quot; , &quot;\\b&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;g&quot; , &quot;'&quot; , &quot;), k[c]); return p }(&quot; , &quot;'&quot; , &quot;q C(y){4 x=y.k(/-/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;).k(/\\s/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;);h(!t(x)||x.j(5)!=&quot;1&quot;)7-1;4 c=l(x.e(2,5)+x.e(6,8),m);4 b=l(x.e(9,n)+x.e(v,w),m);4 a=((o.p(c/f)+(c%f)+o.p(b/f)+(b%f))%A).B();h(a.u&lt;2)a=&quot;0&quot;+a;h(a!=x.j(8)+x.j(n))7-1;7 b}q t(y){4 x=y.k(/-/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;).k(/\\s/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;);4 r=/(D|E)(\\d|[a-z]){3}\\d(\\d|[a-z]){2}\\d(\\d|[a-z]){3}\\d(\\d|[a-z]){2}\\d/i;h(x.u==F&amp;&amp;x.j(5)==&quot;1&quot;&amp;&amp;r.G(x))7 H;7 I}q J(y){4 x=y.k(/-/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;).k(/\\s/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;);h(!t(x)||x.j(5)!=&quot;1&quot;)7-1;4 c=l(x.e(2,5)+x.e(6,8),m);4 b=l(x.e(9,n)+x.e(v,w),m);4 a=((o.p(c/f)+(c%f)+o.p(b/f)+(b%f))%A).B();h(a.u&lt;2)a=&quot;0&quot;+a;h(a!=x.j(8)+x.j(n))7-1;7 c}&quot; , &quot;'&quot; , &quot;, 46, 46, &quot; , &quot;'&quot; , &quot;||||var|||return|||||||substring|10000||if||charAt|replace|parseInt|36|12|Math|floor|function||u2010|isxlid|length|13|15||||100|toString|gbid|XL|AM|16|test|true|false|gcid&quot; , &quot;'&quot; , &quot;.split(&quot; , &quot;'&quot; , &quot;|&quot; , &quot;'&quot; , &quot;), 0, {}))



    function syncCourseIDs() {
        if (isShowingParts) {
            copyPartsToFull();
        }
        else {
            copyFullToParts();
        }
    }

    function copyPartsToFull() {
        // copy parts to full
        tbFull.value = tbA.value;
        if (tbB.value.length > 0) {
            tbFull.value += &quot;-&quot; + tbB.value;
            if (tbC.value.length > 0) {
                tbFull.value += &quot;-&quot; + tbC.value;
                if (tbD.value.length > 0)
                    tbFull.value += &quot;-&quot; + tbD.value;
            }
        }
    }

    function copyFullToParts() {
        // copy full to parts
        var str = tbFull.value.replace(/-/g, &quot;&quot;);
        if (str.length == 16) {
            tbA.value = str.substring(0, 4);
            tbB.value = str.substring(4, 8);
            tbC.value = str.substring(8, 12);
            tbD.value = str.substring(12, 16);
        }
        if (str.length == 0) {
            tbA.value = tbB.value = tbC.value = tbD.value = &quot;&quot;;
        }
    }

    function validateCourseIdSyntax() {
        var isValid = isxlid(tbFull.value);
        return isValid;
    }

    var check;
    function onFullFocus(tb) {
        check = setInterval(function () { checkFull(tb); }, 10);
    }

    function onFullBlur(tb) {
        tb.value = Trim(tb.value).toUpperCase();
        clearInterval(check);
        check = undefined;
    }

    function doFullKeyUp(evt, tb) {
        var keycode;
        if (window.event) {
            keycode = window.event.keyCode;
        } else if (evt) {
            keycode = evt.which;
        } else {
            return true;
        }

        if (onClientValidKeyPressed != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
            eval(onClientValidKeyPressed);

        if ((keycode >= 48 &amp;&amp; keycode &lt;= 57)
            || (keycode >= 65 &amp;&amp; keycode &lt;= 90)
            || (keycode >= 96 &amp;&amp; keycode &lt;= 122)
            || keycode == 8
            || keycode == 46
            || keycode == 91)
            //|| keycode == 17) // Ignore ctrl key to prevent CheckEnrollmentByXlCourseID get called twice
        {
            checkFull(tb);
        }
    }

    var lastIdChecked = undefined;
    function checkFull(tb) {
        var upperCase = Trim(tb.value).toUpperCase();
        if (upperCase != tb.value)
            tb.value = upperCase;

        CourseIDEntry.IsModified = true;

        var len = tb.value.replace(/-/g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).replace(/\u2010/g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).length;
        if (len == 16) {
            if (validateCourseIdSyntax()) {
                syncCourseIDs();
                if (lastIdChecked != tb.value) {
                    CourseIDEntry.ValidateFullCourseID();
                    lastIdChecked = tb.value;
                }
            }
            else {
                if (onClientInvalidCourseID != &quot;&quot;)
                    eval(onClientInvalidCourseID);
            }
        }
        else if (len > 16) {
            if (onClientInvalidCourseID != &quot;&quot;)
                eval(onClientInvalidCourseID);
        }
        else {
            if (onClientIncompleteCourseID != &quot;&quot;)
                eval(onClientIncompleteCourseID);
            if (len == 0)
                syncCourseIDs();
        }
    }


    function doKeyUp(evt, box, tb) {
        var keycode
        if (window.event) {
            keycode = window.event.keyCode;
        } else if (evt) {
            keycode = evt.which;
        } else {
            return true;
        }

        if ((keycode >= 48 &amp;&amp; keycode &lt;= 57)
            || (keycode >= 65 &amp;&amp; keycode &lt;= 90)
            || (keycode >= 96 &amp;&amp; keycode &lt;= 122)
            || keycode == 8
            || keycode == 46
            || keycode == 91)
            //|| keycode == 17) // Ignore ctrl key to prevent CheckEnrollmentByXlCourseID get called twice
        {
            tb.value = Trim(tb.value).replace(/-/g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).toUpperCase();

            if (onClientValidKeyPressed != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                eval(onClientValidKeyPressed);

            CourseIDEntry.IsModified = true;


            if (tb.value.length >= 4
                &amp;&amp; keycode != 8
                &amp;&amp; keycode != 46
                &amp;&amp; keycode != 91
                &amp;&amp; keycode != 17) {
                if (box == &quot; , &quot;'&quot; , &quot;A&quot; , &quot;'&quot; , &quot;) {
                    if (tb.value.length == 14 &amp;&amp; tb.value.substring(0, 2) != &quot; , &quot;'&quot; , &quot;XL&quot; , &quot;'&quot; , &quot;)
                        tb.value = &quot;XL&quot; + tb.value;

                    if (tb.value.length == 16) {
                        var str = tb.value;
                        tbA.value = str.substring(0, 4);
                        tbB.value = str.substring(4, 8);
                        tbC.value = str.substring(8, 12);
                        tbD.value = str.substring(12, 16);
                        tbD.focus();
                    }
                    else {
                        tbB.focus();
                    }
                }
                else if (box == &quot; , &quot;'&quot; , &quot;B&quot; , &quot;'&quot; , &quot;)
                    tbC.focus();
                else if (box == &quot; , &quot;'&quot; , &quot;C&quot; , &quot;'&quot; , &quot;)
                    tbD.focus();
            }

            if (tbA.value.length == 4 &amp;&amp; tbB.value.length == 4 &amp;&amp; tbC.value.length == 4 &amp;&amp; tbD.value.length == 4) {
                syncCourseIDs();
                if (validateCourseIdSyntax()) {
                    CourseIDEntry.ValidateFullCourseID();
                }
                else {
                    if (onClientInvalidCourseID != &quot;&quot;)
                        eval(onClientInvalidCourseID);
                }
            }
            else {
                if (onClientIncompleteCourseID != &quot;&quot;)
                    eval(onClientIncompleteCourseID);
            }
        }
    }

    function switchToParts() {
        isShowingParts = true;
        document.getElementById(&quot;courseIDfull&quot;).style.display = &quot;none&quot;;
        document.getElementById(&quot;courseIDparts&quot;).style.display = &quot;&quot;;
    }

    function switchToFull() {
        isShowingParts = false;
        document.getElementById(&quot;courseIDparts&quot;).style.display = &quot;none&quot;;
        document.getElementById(&quot;courseIDfull&quot;).style.display = &quot;&quot;;
    }

    //Check whether the pressed key is either &quot;Enter&quot; or &quot;Space&quot;
    function isEnterOrSpace(e) {
        var charCode = (typeof e.which == &quot;number&quot;) ? e.which : e.keyCode
        return (charCode == 13 || charCode == 32)
    }


    
        
        -
        
        -
        
        -
        
        
    
    
        Switch to single box for pasting your Course ID
    


    
          
        
    
    
        Switch to multiple boxes for typing your Course ID
    

        




                            
                                Sample Course ID:  XLAB-C1JK-2MNO-0YZ3 
                            
                            
                                Is this the course you want to copy?
                                
                                
                            
                            Note: Your new member course will be a copy of this coordinator course.
                            
                                
                                    
                                        
                                            
                                        
                                        
                                             
                                        
                                    
                                
                            
                            
                        
		
                    
                    
                    
                
            
        
    
    
	




    
    
        
            Course Availability
            
                
	                
                
                
                    
		
		This course is available to students
	
	
                    
                        
                            
                                
		Starting:
		
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;July 2022>>>
	

									
								
							
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

						
					
				
			
		
                                    
		   Ending:
		
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;July 2022>>>
	

									
								
							
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

						
					
				
			
		
                                    
	
	
                                
		Starting:
		
		   
		Ending:
		
	
	
                            
                        
                    
                    
                    
		
		This course is NOT available to students
	
	
                    
		
		Choose this option for coordinator course or courses used as templates for copying.
	
	
                
                

                
	
                
		
			
			This course is not available to students
		
	
	
                
		
			
			This course is available to students
		
		
			
			
                            
                                
                                    Starting:
                                    
                                       
                                    Ending:
                                    
                                
                            
                        
		
	
	                
                

            
        
        
	


        
	Assignment Availability
	
                
                    
                        Use Course Availability dates to schedule assignments
                    
                    Use alternate dates to schedule assignments:                    
                    
                        
                            
                                
                                    Starting:
					                
		
			
				RadDatePicker
			
				
					
				
			
				
					
						
							Calendar
						
							
								
									
										Title and navigation
									
										
											
										
									
	
		&lt;&lt;&lt;July 2022>>>
	

								
							
						
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

					
				
			
		
	
                                    
                                       
                                    
		
			
				RadDatePicker
			
				
					
				
			
				
					
						
							Calendar
						
							
								
									
										Title and navigation
									
										
											
										
									
	
		&lt;&lt;&lt;July 2022>>>
	

								
							
						
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

					
				
			
		
	
                                    
                                
                            
                        
                    
                
                
            

    
        
	Days Allowed
	
                
                    Sunday
                        Monday
                        Tuesday
                        Wednesday
                    
                    Thursday
                        Friday
                        Saturday
                         
                    
                
            

    
        
        
            Time Zone
            
                
                    
                        
                            
	(UTC-05:00) Eastern Time (US &amp; Canada)
	(UTC-05:00) Indiana (East)
	(UTC-06:00) Central Time (US &amp; Canada)
	(UTC-07:00) Mountain Time (US &amp; Canada)
	(UTC-07:00) Arizona
	(UTC-08:00) Pacific Time (US &amp; Canada)
	(UTC-08:00) Tijuana, Baja California
	(UTC-09:00) Alaska
	(UTC-10:00) Hawaii
	(UTC-12:00) International Date Line West
	(UTC-07:00) Chihuahua, La Paz, Mazatlan
	(UTC-11:00) Midway Island, Samoa
	(UTC-06:00) Central America
	(UTC-06:00) Guadalajara, Mexico City, Monterrey
	(UTC-06:00) Saskatchewan
	(UTC-05:00) Bogota, Lima, Quito, Rio Branco
	(UTC-04:30) Caracas
	(UTC-04:00) Atlantic Time (Canada)
	(UTC-04:00) La Paz
	(UTC-04:00) Manaus
	(UTC-04:00) Santiago
	(UTC-03:30) Newfoundland
	(UTC-03:00) Brasilia
	(UTC-03:00) Buenos Aires
	(UTC-03:00) Georgetown
	(UTC-03:00) Greenland
	(UTC-03:00) Montevideo
	(UTC-02:00) Mid-Atlantic
	(UTC-01:00) Azores
	(UTC-01:00) Cape Verde Is.
	(UTC) Casablanca
	(UTC) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London
	(UTC) Monrovia, Reykjavik
	(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna
	(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague
	(UTC+01:00) Brussels, Copenhagen, Madrid, Paris
	(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb
	(UTC+01:00) West Central Africa
	(UTC+02:00) Amman
	(UTC+02:00) Athens, Bucharest, Istanbul
	(UTC+02:00) Beirut
	(UTC+02:00) Cairo
	(UTC+02:00) Harare, Pretoria
	(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius
	(UTC+02:00) Jerusalem
	(UTC+02:00) Minsk
	(UTC+02:00) Windhoek
	(UTC+03:00) Baghdad
	(UTC+03:00) Kuwait, Riyadh
	(UTC+03:00) Moscow, St. Petersburg, Volgograd
	(UTC+03:00) Nairobi
	(UTC+03:00) Tbilisi
	(UTC+03:30) Tehran
	(UTC+04:00) Abu Dhabi, Muscat
	(UTC+04:00) Baku
	(UTC+04:00) Caucasus Standard Time
	(UTC+04:00) Port Louis
	(UTC+04:00) Yerevan
	(UTC+04:30) Kabul
	(UTC+05:00) Ekaterinburg
	(UTC+05:00) Islamabad, Karachi
	(UTC+05:00) Tashkent
	(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi
	(UTC+05:30) Sri Jayawardenepura
	(UTC+05:45) Kathmandu
	(UTC+06:00) Astana, Dhaka
	(UTC+06:00) Almaty, Novosibirsk
	(UTC+06:30) Yangon (Rangoon)
	(UTC+07:00) Bangkok, Hanoi, Jakarta
	(UTC+07:00) Krasnoyarsk
	(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi
	(UTC+08:00) Irkutsk, Ulaan Bataar
	(UTC+08:00) Perth
	(UTC+08:00) Kuala Lumpur, Singapore
	(UTC+08:00) Taipei
	(UTC+09:00) Osaka, Sapporo, Tokyo
	(UTC+09:00) Seoul
	(UTC+09:00) Yakutsk
	(UTC+09:30) Adelaide
	(UTC+09:30) Darwin
	(UTC+10:00) Brisbane
	(UTC+10:00) Canberra, Melbourne, Sydney
	(UTC+10:00) Guam, Port Moresby
	(UTC+10:00) Hobart
	(UTC+10:00) Vladivostok
	(UTC+11:00) Magadan, Solomon Is., New Caledonia
	(UTC+12:00) Auckland, Wellington
	(UTC+12:00) Fiji, Kamchatka, Marshall Is.
	(UTC+13:00) Nuku&quot; , &quot;'&quot; , &quot;alofa


                            
                                Adjust Automatically for Daylight Savings Time
                            
                        
                    
                    

                    
                        
                            Update in member courses
                        
                    

                    
                        Current course time: 9:51am
                    
                    
                    
	
                             
                            If you change your course time zone, go to the Change Dates &amp; Assign Status page to review and adjust your assignment dates and times.
                        


                
            

        
        
            
	


            
	Date Format
	
                    
                        
                            
                                
		MM/DD/YYYY
		DD/MM/YYYY

	
                            
                        
                    
                


            
	


            
	Copying
	Allow other instructors to copy this course.


        
    


    
    
    
    
        
	
                
                This is a standard course and is not part of a course group.
                If no students or section instructors are enrolled, you can change this course to a coordinator course by returning to Step 1 and changing the course Type to Coordinator.  More about coordinator and member courses.
                
            


        
	
                
                    
                        You are creating a coordinator course.  You can use this course to administer assignments for multiple member courses.  As members join your course group, they are listed below.  
                        Go to Edit Roster in the Gradebook to modify course access for individual instructors.
                        More about coordinator and member courses.
                    
                    Member SettingsAllow member courses to remove themselves from the group
                    
                        
		
			
				Course typeCourse nameCourse InstructorsCourse AccessRemove from Group
			
				Coordinator Courses
                                    None
                                
                                    
                                
                                    
                                    Full instructor
                                    
                                
                                        
                                
			
				Member Courses
                                    None
                                
                                    
                                
                                    
                                    
                                    
                                
                                        
                                
			
		
	
                    
                
            


        
	
                
                    
                        You are creating a member course within a coordinated course group.  The coordinator and the other members of your course group are listed below.  
                        Go to Edit Roster in the Gradebook to modify course access for individual instructors.
                        Learn more about coordinator and member courses.
                    
                    
                        

	
                    
                
            


    


    

    
	
        
              
        
        
        
        


var T = true;
var F = false;
//var _s = { ID: -1, Checked: T, Children: new Array() }; // standards array
var _partiallyIncludedValuePaths = new Array();
var _partiallyIncludedValues = new Array();

var chapters=new Array();
chapters[1]=T;

var sections=new Array();
sections[1]=new Array();
sections[1][1]=T;
sections[1][2]=T;

var objectives=new Array();
objectives[1]=new Array();
objectives[1][1]=new Array();
objectives[1][2]=new Array();


var UnCheckedCh=new Array();
var UnCheckedSec=new Array();
var UnCheckedObj=new Array();
var UnCheckedStandards=new Array();

var CheckedCh=new Array();
var CheckedSec=new Array();
var CheckedObj=new Array();
var CheckedStandards=new Array();

var Ccs = {
    UsePartiallyIncludedLabels: true,
    PartiallyIncludedLabel: &quot; , &quot;'&quot; , &quot; &amp;nbsp;(partially included)&quot; , &quot;'&quot; , &quot;,
    UsesObjectives: false, //
    IsForCompanionStudyPlan: false,
    SupportsPostHeightMessage: false,   
    PostHeightMessageContainerID: null,
    
    //

    SaveLists: function() {
        UnCheckedCh = new Array();
        UnCheckedSec = new Array();
        UnCheckedObj = new Array();
        CheckedCh = new Array();
        CheckedSec = new Array();
        CheckedObj = new Array();
        GetUncheckedChapters();
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenChapters&quot;).value = UnCheckedCh.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenSections&quot;).value = UnCheckedSec.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenObjectives&quot;).value = UnCheckedObj.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedChaptersHiddenField&quot;).value = CheckedCh.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedSectionsHiddenField&quot;).value = CheckedSec.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedObjectivesHiddenField&quot;).value = CheckedObj.join(&quot;,&quot;);

        if (typeof(_s) != &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; &amp;&amp; _s != null)
            Ccs.SaveStandards();

        if (typeof(BookContentAreaChaptersSelector) != &quot;undefined&quot; &amp;&amp; BookContentAreaChaptersSelector != null)
            BookContentAreaChaptersSelector.Save();
    },
    SaveStandards: function() {
        var hdn = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenStandardItems&quot;);
        var selectedStandardItemsHiddenField = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedStandardItemsHiddenField&quot;);

        UnCheckedStandards = new Array();
        CheckedStandards = new Array();

        if (_s.Children.length > 0) {
            Ccs.SaveStandardsChildren(_s);
            hdn.value = UnCheckedStandards.join(&quot;,&quot;);
            selectedStandardItemsHiddenField.value = CheckedStandards.join(&quot;,&quot;);
        } else {
            hdn.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            selectedStandardItemsHiddenField.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
        }
    },
    SaveStandardsChildren: function(item) {

        if (item.ID > 0) {
            if (!item.Checked)
                UnCheckedStandards.push(item.ID);
            else
                CheckedStandards.push(item.ID);
        }
        for (var i = 0; i &lt; item.Children.length; i++) {
            Ccs.SaveStandardsChildren(item.Children[i]);
        }
    },
    SetSelectedChapterCoverage: function(csvList) {
        Ccs.SetChapterCoverage(csvList, T);
    },

    SetHiddenChapterCoverage: function(csvList) {
        Ccs.SetChapterCoverage(csvList, F);
    },

    SetChapterCoverage: function(csvList, boolValue) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        for (var i = 0; i &lt; arr.length; i++) {
            var parts = arr[i].split(&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;);
            if (parts.length == 1) // chapter
                chapters[Number(parts[0])] = boolValue;
            else if (parts.length == 2) // section
                sections[Number(parts[0])][Number(parts[1])] = boolValue;
            else if (parts.length == 3) // objective
                objectives[Number(parts[0])][Number(parts[1])][Number(parts[2])] = boolValue;
        }
        ApplyCoverage();
    },


    Check: function (strBookPart) {
        this.SetChecked(strBookPart, T);
    },
    UnCheck: function (strBookPart) {
        this.SetChecked(strBookPart, F);
    },
    SetChecked: function (strBookPart, boolValue) {
        if (strBookPart == null || strBookPart.length == 0)
            return;
       
        var parts = strBookPart.split(&quot;.&quot;);

        if (this.UsesObjectives)
            SetObjectiveValue(Number(parts[0]), Number(parts[1]), Number(parts[2]), boolValue);
        else
            SetSectionValue(Number(parts[0]), Number(parts[1]), boolValue);

        ApplyCoverage();
        UpdateArrayValues();
    },
    CheckSelected: function (csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        arr = removeEmptyElementsFromArray(arr);

        for (var i = 0; i &lt; arr.length; i++) {
            var parts = arr[i].split(&quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;);
            if (parts.length == 1) // chapter
                SetChapterValue(Number(parts[0]), T);
            else if (parts.length == 2) // section
                SetSectionValue(Number(parts[0]), Number(parts[1]), T);
            else if (parts.length == 3) // objective
                SetObjectiveValue(Number(parts[0]), Number(parts[1]), Number(parts[2]), T);
        }

        ApplyCoverage();
        UpdateArrayValues();
    },
    CheckAll: function () {
        SetAllArrayValues(true);
        UpdateArrayValues();
    },
    UnCheckAll: function () {
        SetAllArrayValues(false);
        UpdateArrayValues();
    },
    UpdateArrayValues: function () {
        UpdateArrayValues();
    },
    GetTotalCheckedAtLowestLevel: function () {
        if (this.UsesObjectives)
            return CheckedObj.length;
        return CheckedSec.length;
    },
    SetSelectedChaptersHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedChaptersHiddenField&quot;).value = csvList;
    },
    SetSelectedSectionsHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedSectionsHiddenField&quot;).value = csvList;
    },
    SetSelectedObjectivesHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedObjectivesHiddenField&quot;).value = csvList;
    },

    SetHiddenStandards: function(csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        for (var i = 0; i &lt; arr.length; i++) {
            HideStandard(Number(arr[i]), _s);
        }
        ApplyCoverageStandardView();
    },
    SetSelectedStandards: function(csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        for (var i = 0; i &lt; arr.length; i++) {
            ShowStandard(arr[i]);
        }
        ApplyCoverageStandardView();
    },
    SetOverallCoverageChangedFlag: function(isChanged) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_OverallCoverageChangedHiddenField&quot;).value = isChanged ? &quot;1&quot; : &quot;0&quot;;
    },

    SetTabSwitchedFlag: function() {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TabSwitchedHiddenField&quot;).value = &quot;1&quot;;
    },

    ClearTabSwitchedFlag: function() {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TabSwitchedHiddenField&quot;).value = &quot;0&quot;;
    },

    SetCoverageChangedFlag: function() {
        if (GetSelectedTab() == &quot;standard&quot;)
            document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_StandardCoverageChangedHiddenField&quot;).value = &quot;1&quot;;
        else
            document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_ChapterCoverageChangedHiddenField&quot;).value = &quot;1&quot;;
    },
    // &quot;standards&quot; or &quot;chapters&quot;
    GetCoverageBasis: function () {
        return $(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input:checked&quot; , &quot;'&quot; , &quot;).val();
    }
};

    function pageLoad(sender, args) {

        if (typeof(IsProgressControlDisplay) == &quot;function&quot; &amp;&amp; IsProgressControlDisplay()) {
            return;
        }
        OnClientPageLoad(sender, args);

        //
	    $(&quot;#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input&quot;).change(function() {
            DisableTreeBasedOnCoverageBasis();
	    });

        // 
        
        
        // During partial page loads (using UpdatePanels) we need to fix the scroll bars
	    var prm = Sys.WebForms.PageRequestManager.getInstance();
	    prm.add_pageLoaded(UpdatePanelRefreshed);

   	
        

        if (GetVisibleTree() != null)
        {
            
        
            var hiddenBookContentAreaChaptersSelectorClientPageLoadCode = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenBookContentAreaChaptersSelectorClientPageLoadCode&quot;);
            if (hiddenBookContentAreaChaptersSelectorClientPageLoadCode.value != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                eval(hiddenBookContentAreaChaptersSelectorClientPageLoadCode.value);

            if (GetSelectedTab() == &quot;standard&quot;)
            {
                ApplyCoverageStandardView();
                PartiallyIncludeValues();
            }
            else
            {
                ApplyCoverage();
                PartiallyIncludeValuePaths();
            }
     
            DisableTreeBasedOnCoverageBasis();

            Ccs.ClearTabSwitchedFlag();
        }  
    }

    // 
    function DisableTreeBasedOnCoverageBasis() {
        // 
        var coverageBasis = $(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input:checked&quot; , &quot;'&quot; , &quot;).val();
        
        var tree = GetVisibleTree();
        if (typeof (tree) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree == null || typeof (tree.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree.RootNode == null)
            return false;
        
        var selectedTab = GetSelectedTab();
        if ((selectedTab == &quot;standard&quot; &amp;&amp; coverageBasis == &quot;chapters&quot;) || (selectedTab == &quot;chapter&quot; &amp;&amp; coverageBasis == &quot;standards&quot;))
            tree.CheckBoxesDisabled = true;
        else
            tree.CheckBoxesDisabled = false;

        SetRootCheckBoxesEnabled(tree.RootNode, !tree.CheckBoxesDisabled);
        
        return true;
    }


    function UpdatePanelRefreshed()
    {
        fixScrollbars();
        SubAllowSubmit();	    
    }

    function IPTreeView_NodeTextClicked(node) {
        if (GetSelectedTab() == &quot;standard&quot;) {
            if (node.Nodes.length > 0) // has children?
                return true;
        }
        else if ((node.Level() == 0) || (node.Level() == 1 &amp;&amp; Ccs.UsesObjectives))
            return true;
    }
    
    function ExpandAllClient()
    {
        var treeview = GetVisibleTree();

        if (typeof (treeview) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || treeview == null || typeof (treeview.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || treeview.RootNode == null)
            return false;
        
        var startNode=treeview.RootNode;
        
        for (var i=0; i&lt;startNode.Nodes.length; i++) //chapters
        {
            startNode.Nodes[i].ExpandAll();
        }

        IPTreeView_OnAfterNodeIconClicked();

        fixScrollbars();
	}

	function ExpandAll()
    {
        Ccs.SaveLists();
        SetFormTracker(true);
	}
	
    // Note: Because treeview.js is loaded in PreRender by IPTreeView, these overrides have to be outside of
    // HeadContent or else they happen before the include gets there and get overriden themselves
    // So now we have a use for PreContent... or we find another way to deal with it
	function IPTreeView_NodeIconClicked(node) {
        return true;
	}

	function IPTreeView_OnAfterNodeIconClicked() {
	    PostHeightMessage();
	}

	function IPTreeView_OnAfterNodeTextClicked() {
	    PostHeightMessage();
	}

	function PostHeightMessage() {
	    if (Ccs.SupportsPostHeightMessage) {

	        var containerEl = document.getElementById(Ccs.PostHeightMessageContainerID);
	        if (containerEl) {

	            var scrollHeight = containerEl.scrollHeight;
	            if (parent &amp;&amp; parent.postMessage) {
	                parent.postMessage(scrollHeight, &quot;*&quot;);
	                fixScrollbars();
	            }
	        }
	    }
	}
      
    function UpdateNode(nodeId)
    {
    }
     
    // CA Returns either the tree JS instance or null if none is available - useful to tell if the tree has been rendered (e.g., Course Wizard after step 1)
    // NOTE: $find and document.getElementById do not work to retrieve the tree. 
    function GetVisibleTree() {
        if (GetSelectedTab() == &quot;standard&quot;) {
            return (typeof(ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeStandards) == &quot;undefined&quot;) ? null : ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeStandards;
        } else {
            return (typeof(ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeChapters) == &quot;undefined&quot;) ? null : ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeChapters;
        }
    }

    function IPTreeView_NodePopulated(pNode) {
        if (GetSelectedTab() == &quot;standard&quot;)
        {
            ApplyCoverageStandardView(pNode);
            PartiallyIncludeValues();
        }
        else
        {
            ApplyCoverage(pNode);
            PartiallyIncludeValuePaths();
        }

        IPTreeView_OnAfterNodeIconClicked();

        setTimeout(&quot; , &quot;'&quot; , &quot;fixScrollbars();&quot; , &quot;'&quot; , &quot;,100);
    }
    
    

    function IPTreeView_CheckboxClicked(node) {
        Ccs.SetCoverageChangedFlag();
        
        SetFormTracker(true);

        IPTreeView_OnClientCheckboxClicked();

        node.ToggleCheck();

        var ret = false;
        if (GetSelectedTab() == &quot;standard&quot;)
            ret = StandardViewCheckboxClicked(node);
        else
            ret = ChapterCoverageCheckboxClicked(node);

        // Req C14-8: Companion Study Plan
        if (Ccs.IsForCompanionStudyPlan) {
            Ccs.UpdateArrayValues();
            UpdateTotalMasteryPoints(Ccs.GetTotalCheckedAtLowestLevel());
        }

        return ret;
    }

    function UpdateTotalMasteryPoints(val)
    {
        if (parent &amp;&amp; parent.CREATEASSIGNMENT_UpdateTotalMasteryPoints)
            parent.CREATEASSIGNMENT_UpdateTotalMasteryPoints(val);
    }

    function COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked()  // CA prefix to avoid conflict with method called OnClientContentAreaCheckboxClicked on the page using this control.
    {
        Ccs.SetCoverageChangedFlag();
        
        OnClientContentAreaCheckboxClicked();
    }

    // CA Content Area filter handling
    function OnClientContentAreaSelected(chapterIDArray)
    {
        var tree = GetVisibleTree();

    if (typeof (tree) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree == null || typeof (tree.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree.RootNode == null)
        return false;

        var chapterSelected = false;       

        for (var i = 0; i &lt; chapterIDArray.length; i++)
        {
            var node = tree.FindTopLevelNode(chapterIDArray[i]);
            if (node != null)
            {
                node.UnfilterRow();

                if (node.Checked)
                    chapterSelected = true;
            }
        }

        //
        
        if (!chapterSelected)
            for (var i = 0; i &lt; chapterIDArray.length; i++)
            {
                var node = tree.FindTopLevelNode(chapterIDArray[i]);
                if (node != null)
                {
                    NodeSetCheck(node, true);
                    ChapterCoverageCheckboxClicked(node);
                }
            }

        COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked();

        return true;
    }

    function OnClientContentAreaCleared(chapterIDArray)
    {
        var tree = GetVisibleTree();
        
        if (typeof (tree) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree == null || typeof (tree.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree.RootNode == null)
            return false;

        for (var i = 0; i &lt; chapterIDArray.length; i++)
        {
            var node = tree.FindTopLevelNode(chapterIDArray[i]);
            if (node != null)
                node.FilterRow();
        }

        COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked();

        return true;
    }
    
    function LinkButtonWritingPracticeMoreInfo_onclick() {
        var url = &quot; , &quot;'&quot; , &quot;http://www.mywritinglab.com/writingpractice.html&quot; , &quot;'&quot; , &quot;;
        popupWindow(&quot;WritingPracticeMoreInfo&quot;,url,{Width:800,Height:500,AutoClose:true,Titlebar:true,Statusbar:false});
    }


 


		
        
        
        
    
    
    
        
    
    
    
    
    
    
    
    

    
    
    

    
    
    

    
    
    
			
        
        
                
                
                    
                        
                            Included sections                            
                            
                                Expand All
                            
                        
                    
                
                
                
				
					
						
							
								DSM. Dynamic Study Modules  
							
						
					
						
					
				
			
            
    
		
    
	
 
        

   

        
    
    
    
    
        
	
                LockDown Browser and Proctoring
            
	
                
                Use the Pearson LockDown Browser in this course  More Options
                
                    
                        
                        The Pearson LockDown Browser is not compatible with mobile devices or Chromebooks.
                    
                
                
                Use automated proctoring in this course
                
                    
		Select proctoring partner
		ProctorU Record+
		Respondus Monitor Proctoring

	
                
                    
                        
                        
                            
                                ProctorU Key:
                            
                                
                        
                        
                            
                                ProctorU Secret:
                            
                                
                        
                        
                    
                
                    
                        After you create your course, complete the Respondus Monitor Proctoring license setup.
                        Complete the Respondus Monitor Proctoring license setup
                    
                    
                
                


        
                
        
        
	
                IP Address Range
            
	
                Require students to take IP-restricted quizzes and tests within the following IP address range:
                
                    None  
                    Change...
                
            


        
       
        
                
        
        
            
                Multimedia Learning Aids
                
            
                Choose the multimedia learning aids that are available in this course.
            
        
    
    			
    
        
            
            
                Learning Aids
                
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                
	
	
	Ask My InstructorEdit Email Address
                                              
                                              prabhashi.hettiarachchi@pearson.com
                                              
	


                
            
        
    



    
        
            Section instructor access      
            
                Select below to choose a course level access setting.
                Go to Edit Roster in the Gradebook to modify course access for individual instructors.
            
        
        
            
            Grant full instructor access.
        
        
            
            Restrict access to the following access type:
        
        
            
            Section instructor
        
        
            
            Read-only
        
        
            
            Custom (No options selected)  Define...
            
        
    
    
    
        
            Other restricted access privileges
            Edit Ask My Instructor email address
        
        
            
            Edit course roster
        
    




    
        Cancel
Save
Next
Back

    
&quot;) or . = concat(&quot;


    
    
    

	
    
            
                Start
                Availability
                Group Admin
                Course Access
                Coverage
                Learning Aids and Test Options
            
    


	
    
            
                Start
                Availability
                Course Access
                Coverage
                Learning Aids and Test Options
            
    


 


  

    
        
            Type of course to create
            
                
	-- Choose --
	Standard
	Coordinator
	Member

Standard
                Learn about course types
            
        
        
            Course name
            
                Sample
            
        
        
	Course Code
	
                
                
		                    
                                           
                        

		
                            
                            Contact the Pearson administrator if the code for
your template course is not showing on this list.
                        
                    
                    
                
	
            

        
        
	Book
	
                Amplifire Test Book
            


        
        
        
	
                
            


        
            Primary instructor
            
	Prabhashi ins1

Prabhashi ins1
        
        
	
	
                
		
                
                
                    
                        
                        
                            If you change the instructor to a user other than yourself, and you click Save in the last step of this wizard, you will not be able to access these management screens again.  If you do continue now, you will be able to complete all the steps in this wizard before the instructor change will take affect.

                            Note: Only the user who has been designated as the instructor can access the course management screens.  You can skip this step now by setting your name as the instructor and continuing to the other steps in this wizard.  Then, you can return later to access this screen and select another instructor.
                        
                    
                
                
	
            


        
    
    




	
    
		
    
        How would you like to create your course?
        
            
                
                
			
				
				Create a new course
			
			
				
				Create a new preloaded course
			
			
				
				Copy one of my existing courses
			
			
				
				Copy a course from another instructor at my school
			
			
				
				Copy a course by specifying its Course ID
			
		
		
            
            
                
                    
                        
                        
			
                            Choose the textbook for this course                        
                            
				  -- Choose Book --  
				Abel/Bernanke/Croushore: Macroeconomics 6e
				Amplifire Test Book
				Bade/Parkin 3e for Testing
				Bade/Parkin: Demo for Foundations 3e
				Bade/Parkin: Essential Foundations of Economics 3e
				Bade/Parkin: Essential Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics 3e
				Bade/Parkin: Foundations of Economics 4e
				Bade/Parkin: Foundations of Economics AP* Edition
				Bade/Parkin: Foundations of Macroeconomics 3e
				Bade/Parkin: Foundations of Macroeconomics 4e
				Bade/Parkin: Foundations of Microeconomics 3e
				Bade/Parkin: Foundations of Microeconomics 4e
				Case/Fair: Principles of Economics 8e
				Case/Fair: Principles of Economics 9e
				Case/Fair: Principles of Macroeconomics 8e
				Case/Fair: Principles of Macroeconomics 9e
				Case/Fair: Principles of Microeconomics 8e
				Case/Fair: Principles of Microeconomics 9e
				Cohen/Howe: Economics for Life: Smart Choices for You
				DEMO-Parkin, Economics: Canada in the Global Environment, Seventh Edition
				Ekelund 7e Standalone Testing
				Ekelund/Ressler/Tollison: Economics 7e (new)
				Ekelund/Ressler/Tollison: Macroeconomics 7e (new)
				Ekelund/Ressler/Tollison: Microeconomics 7e (new)
				Fremgen Test Course for Alison Doucette
				Group Work Test Book
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Economics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 1e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Essentials of Economics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 2e UPDATE
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Macroeconomics 6e
				Hubbard/O&quot; , &quot;'&quot; , &quot;Brien: Microeconomics 2e
				Hubbard: Macroeconomics 1e DEMO
				James: Macroeconomics, In-Class Edition
				James: Microeconomics and Macroeconomics, In-Class Edition - Chapter 4 only
				James: Microeconomics, In-Class Edition
				Knewton Econ test book - Ishara
				Krugman/Obstfeld: International Economics 7e
				Krugman/Obstfeld: International Economics 8e
				Leeds/von Allmen/Schiming: Economics, 1e (new)
				Leeds/von Allmen/Schiming: Macroeconomics, 1e (new)
				Leeds/von Allmen/Schiming: Microeconomics, 1e (new)
				Lipsey/Ragan/Storer: Economics 13e
				Lipsey/Ragan/Storer: Macroeconomics 13e
				Lipsey/Ragan/Storer: Microeconomics 13e
				Miller et al.,  Economics Today: The Macro View, Fourth Canadian Edition
				Miller et al., Economics Today: The Micro View, Fourth Canadian Edition
				Miller: Economics Today 13e
				Miller: Economics Today 13e
				Miller: Economics Today 14e
				Miller: Economics Today 14e Summer 2007
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 13e
				Miller: Economics Today: The Macro View 14e
				Miller: Economics Today: The Macro View 14e Summer 2007
				Miller: Economics Today: The Macro View 15e
				Miller: Economics Today: The Macro View, 3ce
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 13e
				Miller: Economics Today: The Micro View 14e
				Miller: Economics Today: The Micro View 14e Summer 2007
				Miller: Economics Today: The Micro View, 3ce
				Mishkin: The Economics of Money, Banking, and Financial Markets 8e
				Mishkin: The Economics of Money, Banking, and Financial Markets Alternate
				MyEconLab Experiments and Digital Interactives (Master Sample)
				MyEconLab Experiments for TESTING ONLY
				NextGen Media Econ Test Book--REAL
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Economics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Macroeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Microeconomics 5e
				O&quot; , &quot;'&quot; , &quot;Sullivan/Sheffrin/Perez: Survey of Economics 3e
				Parkin/Bade, Economics, Sixth Edition
				Parkin/Bade, Macroeconomics, Sixth Edition
				Parkin/Bade, Microeconomics, Sixth Edition
				Parkin: Economics 8e
				Parkin: Economics seventh european edition
				Parkin: Macroeconomics 8e
				Parkin: Microeconomics 8e
				Perloff: Microeconomics, 4e
				Perloff: Microeconomics, 4e Demo
				Perloff: Microeconomics, 5e
				Pindyck/Rubinfeld: Microeconomics, 7e
				Ragan/Lipsey, Economics, 12ce
				Ragan/Lipsey, Macroeconomics, 12ce
				Ragan/Lipsey, Microeconomics, 12ce
				Ritter/Silber/Udell: Principles of Money, Banking &amp; Financial Markets, 12e
				Sloman: Essentials of Economics 4e
				z-Dummy Book for Data Grapher Wizard
				zBPTest Econ Medterm

			
                        
		
                        
			
                            Create a new preloaded course                        
                            
				  -- Choose Course --  
				MASTER Hubbard Macro 6e

			
                        
		
                        
			
                            
                        
		
                        
			
                            Choose a course to copy
                            
				  -- Choose Course --  
				

			TestCourse
                            
				 
			
                            
				Note: Your new member course will be a copy of this coordinator course.
			
                        
		
                        
			
                            
                            
				  -- Choose Course --  
				

			Econ_Course:Ins Jayani
                            
				
			
                            
				Note: Your new member course will be a copy of this coordinator course.
			
                        
		
                        
			
                            
                                                                                        
                            


    var tbA, tbB, tbC, tbD, tbFull;
    var onClientValidKeyPressed;
    var onClientCourseIDValidated;
    var onClientInvalidCourseID;
    var onClientIncompleteCourseID;

    var CourseIDEntry = {
        IsModified: false,

        Init: function () {
            tbA = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDA&quot;);
            tbB = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDB&quot;);
            tbC = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDC&quot;);
            tbD = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseIDD&quot;);
            tbFull = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_TextBoxCourseID&quot;);
            onClientValidKeyPressed = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientValidKeyPressed&quot;).value;
            onClientCourseIDValidated = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientCourseIDValidated&quot;).value;
            onClientInvalidCourseID = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientInvalidCourseID&quot;).value;
            onClientIncompleteCourseID = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseIDEntry_HiddenOnClientIncompleteCourseID&quot;).value;

            if (onClientInvalidCourseID == &quot;&quot;) {
                alert(&quot;OnClientInvalidCourseID handler not set&quot;);
                return false;
            }

        },
        GetCourseID: function () {
            return tbFull.value;
        },

        SyncAllTextBoxes: function () {
            if (tbFull.value != &quot;&quot; &amp;&amp; tbA.value == &quot;&quot;)
                copyFullToParts();
            else if (tbFull.value == &quot;&quot; &amp;&amp; tbA.value != &quot;&quot;)
                copyPartsToFull();
        },

        ValidateFullCourseID: function () {
            if (tbFull.value != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; &amp;&amp; onClientCourseIDValidated != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;) {
                setTimeout(onClientCourseIDValidated + &quot;(&quot; , &quot;'&quot; , &quot;&quot; + tbFull.value + &quot;&quot; , &quot;'&quot; , &quot;)&quot;, 100);
            }
        }
    }


    var isShowingParts = true;

    eval(function (p, a, c, k, e, r) { e = function (c) { return (c &lt; a ? &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot; : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] }]; e = function () { return &quot; , &quot;'&quot; , &quot;\\w+&quot; , &quot;'&quot; , &quot; }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp(&quot; , &quot;'&quot; , &quot;\\b&quot; , &quot;'&quot; , &quot; + e(c) + &quot; , &quot;'&quot; , &quot;\\b&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;g&quot; , &quot;'&quot; , &quot;), k[c]); return p }(&quot; , &quot;'&quot; , &quot;q C(y){4 x=y.k(/-/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;).k(/\\s/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;);h(!t(x)||x.j(5)!=&quot;1&quot;)7-1;4 c=l(x.e(2,5)+x.e(6,8),m);4 b=l(x.e(9,n)+x.e(v,w),m);4 a=((o.p(c/f)+(c%f)+o.p(b/f)+(b%f))%A).B();h(a.u&lt;2)a=&quot;0&quot;+a;h(a!=x.j(8)+x.j(n))7-1;7 b}q t(y){4 x=y.k(/-/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;).k(/\\s/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;);4 r=/(D|E)(\\d|[a-z]){3}\\d(\\d|[a-z]){2}\\d(\\d|[a-z]){3}\\d(\\d|[a-z]){2}\\d/i;h(x.u==F&amp;&amp;x.j(5)==&quot;1&quot;&amp;&amp;r.G(x))7 H;7 I}q J(y){4 x=y.k(/-/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;).k(/\\s/g,\&quot; , &quot;'&quot; , &quot;\&quot; , &quot;'&quot; , &quot;);h(!t(x)||x.j(5)!=&quot;1&quot;)7-1;4 c=l(x.e(2,5)+x.e(6,8),m);4 b=l(x.e(9,n)+x.e(v,w),m);4 a=((o.p(c/f)+(c%f)+o.p(b/f)+(b%f))%A).B();h(a.u&lt;2)a=&quot;0&quot;+a;h(a!=x.j(8)+x.j(n))7-1;7 c}&quot; , &quot;'&quot; , &quot;, 46, 46, &quot; , &quot;'&quot; , &quot;||||var|||return|||||||substring|10000||if||charAt|replace|parseInt|36|12|Math|floor|function||u2010|isxlid|length|13|15||||100|toString|gbid|XL|AM|16|test|true|false|gcid&quot; , &quot;'&quot; , &quot;.split(&quot; , &quot;'&quot; , &quot;|&quot; , &quot;'&quot; , &quot;), 0, {}))



    function syncCourseIDs() {
        if (isShowingParts) {
            copyPartsToFull();
        }
        else {
            copyFullToParts();
        }
    }

    function copyPartsToFull() {
        // copy parts to full
        tbFull.value = tbA.value;
        if (tbB.value.length > 0) {
            tbFull.value += &quot;-&quot; + tbB.value;
            if (tbC.value.length > 0) {
                tbFull.value += &quot;-&quot; + tbC.value;
                if (tbD.value.length > 0)
                    tbFull.value += &quot;-&quot; + tbD.value;
            }
        }
    }

    function copyFullToParts() {
        // copy full to parts
        var str = tbFull.value.replace(/-/g, &quot;&quot;);
        if (str.length == 16) {
            tbA.value = str.substring(0, 4);
            tbB.value = str.substring(4, 8);
            tbC.value = str.substring(8, 12);
            tbD.value = str.substring(12, 16);
        }
        if (str.length == 0) {
            tbA.value = tbB.value = tbC.value = tbD.value = &quot;&quot;;
        }
    }

    function validateCourseIdSyntax() {
        var isValid = isxlid(tbFull.value);
        return isValid;
    }

    var check;
    function onFullFocus(tb) {
        check = setInterval(function () { checkFull(tb); }, 10);
    }

    function onFullBlur(tb) {
        tb.value = Trim(tb.value).toUpperCase();
        clearInterval(check);
        check = undefined;
    }

    function doFullKeyUp(evt, tb) {
        var keycode;
        if (window.event) {
            keycode = window.event.keyCode;
        } else if (evt) {
            keycode = evt.which;
        } else {
            return true;
        }

        if (onClientValidKeyPressed != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
            eval(onClientValidKeyPressed);

        if ((keycode >= 48 &amp;&amp; keycode &lt;= 57)
            || (keycode >= 65 &amp;&amp; keycode &lt;= 90)
            || (keycode >= 96 &amp;&amp; keycode &lt;= 122)
            || keycode == 8
            || keycode == 46
            || keycode == 91)
            //|| keycode == 17) // Ignore ctrl key to prevent CheckEnrollmentByXlCourseID get called twice
        {
            checkFull(tb);
        }
    }

    var lastIdChecked = undefined;
    function checkFull(tb) {
        var upperCase = Trim(tb.value).toUpperCase();
        if (upperCase != tb.value)
            tb.value = upperCase;

        CourseIDEntry.IsModified = true;

        var len = tb.value.replace(/-/g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).replace(/\u2010/g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).length;
        if (len == 16) {
            if (validateCourseIdSyntax()) {
                syncCourseIDs();
                if (lastIdChecked != tb.value) {
                    CourseIDEntry.ValidateFullCourseID();
                    lastIdChecked = tb.value;
                }
            }
            else {
                if (onClientInvalidCourseID != &quot;&quot;)
                    eval(onClientInvalidCourseID);
            }
        }
        else if (len > 16) {
            if (onClientInvalidCourseID != &quot;&quot;)
                eval(onClientInvalidCourseID);
        }
        else {
            if (onClientIncompleteCourseID != &quot;&quot;)
                eval(onClientIncompleteCourseID);
            if (len == 0)
                syncCourseIDs();
        }
    }


    function doKeyUp(evt, box, tb) {
        var keycode
        if (window.event) {
            keycode = window.event.keyCode;
        } else if (evt) {
            keycode = evt.which;
        } else {
            return true;
        }

        if ((keycode >= 48 &amp;&amp; keycode &lt;= 57)
            || (keycode >= 65 &amp;&amp; keycode &lt;= 90)
            || (keycode >= 96 &amp;&amp; keycode &lt;= 122)
            || keycode == 8
            || keycode == 46
            || keycode == 91)
            //|| keycode == 17) // Ignore ctrl key to prevent CheckEnrollmentByXlCourseID get called twice
        {
            tb.value = Trim(tb.value).replace(/-/g, &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;).toUpperCase();

            if (onClientValidKeyPressed != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                eval(onClientValidKeyPressed);

            CourseIDEntry.IsModified = true;


            if (tb.value.length >= 4
                &amp;&amp; keycode != 8
                &amp;&amp; keycode != 46
                &amp;&amp; keycode != 91
                &amp;&amp; keycode != 17) {
                if (box == &quot; , &quot;'&quot; , &quot;A&quot; , &quot;'&quot; , &quot;) {
                    if (tb.value.length == 14 &amp;&amp; tb.value.substring(0, 2) != &quot; , &quot;'&quot; , &quot;XL&quot; , &quot;'&quot; , &quot;)
                        tb.value = &quot;XL&quot; + tb.value;

                    if (tb.value.length == 16) {
                        var str = tb.value;
                        tbA.value = str.substring(0, 4);
                        tbB.value = str.substring(4, 8);
                        tbC.value = str.substring(8, 12);
                        tbD.value = str.substring(12, 16);
                        tbD.focus();
                    }
                    else {
                        tbB.focus();
                    }
                }
                else if (box == &quot; , &quot;'&quot; , &quot;B&quot; , &quot;'&quot; , &quot;)
                    tbC.focus();
                else if (box == &quot; , &quot;'&quot; , &quot;C&quot; , &quot;'&quot; , &quot;)
                    tbD.focus();
            }

            if (tbA.value.length == 4 &amp;&amp; tbB.value.length == 4 &amp;&amp; tbC.value.length == 4 &amp;&amp; tbD.value.length == 4) {
                syncCourseIDs();
                if (validateCourseIdSyntax()) {
                    CourseIDEntry.ValidateFullCourseID();
                }
                else {
                    if (onClientInvalidCourseID != &quot;&quot;)
                        eval(onClientInvalidCourseID);
                }
            }
            else {
                if (onClientIncompleteCourseID != &quot;&quot;)
                    eval(onClientIncompleteCourseID);
            }
        }
    }

    function switchToParts() {
        isShowingParts = true;
        document.getElementById(&quot;courseIDfull&quot;).style.display = &quot;none&quot;;
        document.getElementById(&quot;courseIDparts&quot;).style.display = &quot;&quot;;
    }

    function switchToFull() {
        isShowingParts = false;
        document.getElementById(&quot;courseIDparts&quot;).style.display = &quot;none&quot;;
        document.getElementById(&quot;courseIDfull&quot;).style.display = &quot;&quot;;
    }

    //Check whether the pressed key is either &quot;Enter&quot; or &quot;Space&quot;
    function isEnterOrSpace(e) {
        var charCode = (typeof e.which == &quot;number&quot;) ? e.which : e.keyCode
        return (charCode == 13 || charCode == 32)
    }


    
        
        -
        
        -
        
        -
        
        
    
    
        Switch to single box for pasting your Course ID
    


    
          
        
    
    
        Switch to multiple boxes for typing your Course ID
    

        




                            
                                Sample Course ID:  XLAB-C1JK-2MNO-0YZ3 
                            
                            
                                Is this the course you want to copy?
                                
                                
                            
                            Note: Your new member course will be a copy of this coordinator course.
                            
                                
                                    
                                        
                                            
                                        
                                        
                                             
                                        
                                    
                                
                            
                            
                        
		
                    
                    
                    
                
            
        
    
    
	




    
    
        
            Course Availability
            
                
	                
                
                
                    
		
		This course is available to students
	
	
                    
                        
                            
                                
		Starting:
		
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;July 2022>>>
	

									
								
							
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

						
					
				
			
		
                                    
		   Ending:
		
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;July 2022>>>
	

									
								
							
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

						
					
				
			
		
                                    
	
	
                                
		Starting:
		
		   
		Ending:
		
	
	
                            
                        
                    
                    
                    
		
		This course is NOT available to students
	
	
                    
		
		Choose this option for coordinator course or courses used as templates for copying.
	
	
                
                

                
	
                
		
			
			This course is not available to students
		
	
	
                
		
			
			This course is available to students
		
		
			
			
                            
                                
                                    Starting:
                                    
                                       
                                    Ending:
                                    
                                
                            
                        
		
	
	                
                

            
        
        
	


        
	Assignment Availability
	
                
                    
                        Use Course Availability dates to schedule assignments
                    
                    Use alternate dates to schedule assignments:                    
                    
                        
                            
                                
                                    Starting:
					                
		
			
				RadDatePicker
			
				
					
				
			
				
					
						
							Calendar
						
							
								
									
										Title and navigation
									
										
											
										
									
	
		&lt;&lt;&lt;July 2022>>>
	

								
							
						
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

					
				
			
		
	
                                    
                                       
                                    
		
			
				RadDatePicker
			
				
					
				
			
				
					
						
							Calendar
						
							
								
									
										Title and navigation
									
										
											
										
									
	
		&lt;&lt;&lt;July 2022>>>
	

								
							
						
	
		
	
		July 2022
	
		
			SMTWTFS
		
	
		
			262728293012
		
			3456789
		
			10111213141516
		
			17181920212223
		
			24252627282930
		
			31123456
		
	

	

					
				
			
		
	
                                    
                                
                            
                        
                    
                
                
            

    
        
	Days Allowed
	
                
                    Sunday
                        Monday
                        Tuesday
                        Wednesday
                    
                    Thursday
                        Friday
                        Saturday
                         
                    
                
            

    
        
        
            Time Zone
            
                
                    
                        
                            
	(UTC-05:00) Eastern Time (US &amp; Canada)
	(UTC-05:00) Indiana (East)
	(UTC-06:00) Central Time (US &amp; Canada)
	(UTC-07:00) Mountain Time (US &amp; Canada)
	(UTC-07:00) Arizona
	(UTC-08:00) Pacific Time (US &amp; Canada)
	(UTC-08:00) Tijuana, Baja California
	(UTC-09:00) Alaska
	(UTC-10:00) Hawaii
	(UTC-12:00) International Date Line West
	(UTC-07:00) Chihuahua, La Paz, Mazatlan
	(UTC-11:00) Midway Island, Samoa
	(UTC-06:00) Central America
	(UTC-06:00) Guadalajara, Mexico City, Monterrey
	(UTC-06:00) Saskatchewan
	(UTC-05:00) Bogota, Lima, Quito, Rio Branco
	(UTC-04:30) Caracas
	(UTC-04:00) Atlantic Time (Canada)
	(UTC-04:00) La Paz
	(UTC-04:00) Manaus
	(UTC-04:00) Santiago
	(UTC-03:30) Newfoundland
	(UTC-03:00) Brasilia
	(UTC-03:00) Buenos Aires
	(UTC-03:00) Georgetown
	(UTC-03:00) Greenland
	(UTC-03:00) Montevideo
	(UTC-02:00) Mid-Atlantic
	(UTC-01:00) Azores
	(UTC-01:00) Cape Verde Is.
	(UTC) Casablanca
	(UTC) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London
	(UTC) Monrovia, Reykjavik
	(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna
	(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague
	(UTC+01:00) Brussels, Copenhagen, Madrid, Paris
	(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb
	(UTC+01:00) West Central Africa
	(UTC+02:00) Amman
	(UTC+02:00) Athens, Bucharest, Istanbul
	(UTC+02:00) Beirut
	(UTC+02:00) Cairo
	(UTC+02:00) Harare, Pretoria
	(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius
	(UTC+02:00) Jerusalem
	(UTC+02:00) Minsk
	(UTC+02:00) Windhoek
	(UTC+03:00) Baghdad
	(UTC+03:00) Kuwait, Riyadh
	(UTC+03:00) Moscow, St. Petersburg, Volgograd
	(UTC+03:00) Nairobi
	(UTC+03:00) Tbilisi
	(UTC+03:30) Tehran
	(UTC+04:00) Abu Dhabi, Muscat
	(UTC+04:00) Baku
	(UTC+04:00) Caucasus Standard Time
	(UTC+04:00) Port Louis
	(UTC+04:00) Yerevan
	(UTC+04:30) Kabul
	(UTC+05:00) Ekaterinburg
	(UTC+05:00) Islamabad, Karachi
	(UTC+05:00) Tashkent
	(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi
	(UTC+05:30) Sri Jayawardenepura
	(UTC+05:45) Kathmandu
	(UTC+06:00) Astana, Dhaka
	(UTC+06:00) Almaty, Novosibirsk
	(UTC+06:30) Yangon (Rangoon)
	(UTC+07:00) Bangkok, Hanoi, Jakarta
	(UTC+07:00) Krasnoyarsk
	(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi
	(UTC+08:00) Irkutsk, Ulaan Bataar
	(UTC+08:00) Perth
	(UTC+08:00) Kuala Lumpur, Singapore
	(UTC+08:00) Taipei
	(UTC+09:00) Osaka, Sapporo, Tokyo
	(UTC+09:00) Seoul
	(UTC+09:00) Yakutsk
	(UTC+09:30) Adelaide
	(UTC+09:30) Darwin
	(UTC+10:00) Brisbane
	(UTC+10:00) Canberra, Melbourne, Sydney
	(UTC+10:00) Guam, Port Moresby
	(UTC+10:00) Hobart
	(UTC+10:00) Vladivostok
	(UTC+11:00) Magadan, Solomon Is., New Caledonia
	(UTC+12:00) Auckland, Wellington
	(UTC+12:00) Fiji, Kamchatka, Marshall Is.
	(UTC+13:00) Nuku&quot; , &quot;'&quot; , &quot;alofa


                            
                                Adjust Automatically for Daylight Savings Time
                            
                        
                    
                    

                    
                        
                            Update in member courses
                        
                    

                    
                        Current course time: 9:51am
                    
                    
                    
	
                             
                            If you change your course time zone, go to the Change Dates &amp; Assign Status page to review and adjust your assignment dates and times.
                        


                
            

        
        
            
	


            
	Date Format
	
                    
                        
                            
                                
		MM/DD/YYYY
		DD/MM/YYYY

	
                            
                        
                    
                


            
	


            
	Copying
	Allow other instructors to copy this course.


        
    


    
    
    
    
        
	
                
                This is a standard course and is not part of a course group.
                If no students or section instructors are enrolled, you can change this course to a coordinator course by returning to Step 1 and changing the course Type to Coordinator.  More about coordinator and member courses.
                
            


        
	
                
                    
                        You are creating a coordinator course.  You can use this course to administer assignments for multiple member courses.  As members join your course group, they are listed below.  
                        Go to Edit Roster in the Gradebook to modify course access for individual instructors.
                        More about coordinator and member courses.
                    
                    Member SettingsAllow member courses to remove themselves from the group
                    
                        
		
			
				Course typeCourse nameCourse InstructorsCourse AccessRemove from Group
			
				Coordinator Courses
                                    None
                                
                                    
                                
                                    
                                    Full instructor
                                    
                                
                                        
                                
			
				Member Courses
                                    None
                                
                                    
                                
                                    
                                    
                                    
                                
                                        
                                
			
		
	
                    
                
            


        
	
                
                    
                        You are creating a member course within a coordinated course group.  The coordinator and the other members of your course group are listed below.  
                        Go to Edit Roster in the Gradebook to modify course access for individual instructors.
                        Learn more about coordinator and member courses.
                    
                    
                        

	
                    
                
            


    


    

    
	
        
              
        
        
        
        


var T = true;
var F = false;
//var _s = { ID: -1, Checked: T, Children: new Array() }; // standards array
var _partiallyIncludedValuePaths = new Array();
var _partiallyIncludedValues = new Array();

var chapters=new Array();
chapters[1]=T;

var sections=new Array();
sections[1]=new Array();
sections[1][1]=T;
sections[1][2]=T;

var objectives=new Array();
objectives[1]=new Array();
objectives[1][1]=new Array();
objectives[1][2]=new Array();


var UnCheckedCh=new Array();
var UnCheckedSec=new Array();
var UnCheckedObj=new Array();
var UnCheckedStandards=new Array();

var CheckedCh=new Array();
var CheckedSec=new Array();
var CheckedObj=new Array();
var CheckedStandards=new Array();

var Ccs = {
    UsePartiallyIncludedLabels: true,
    PartiallyIncludedLabel: &quot; , &quot;'&quot; , &quot; &amp;nbsp;(partially included)&quot; , &quot;'&quot; , &quot;,
    UsesObjectives: false, //
    IsForCompanionStudyPlan: false,
    SupportsPostHeightMessage: false,   
    PostHeightMessageContainerID: null,
    
    //

    SaveLists: function() {
        UnCheckedCh = new Array();
        UnCheckedSec = new Array();
        UnCheckedObj = new Array();
        CheckedCh = new Array();
        CheckedSec = new Array();
        CheckedObj = new Array();
        GetUncheckedChapters();
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenChapters&quot;).value = UnCheckedCh.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenSections&quot;).value = UnCheckedSec.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenObjectives&quot;).value = UnCheckedObj.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedChaptersHiddenField&quot;).value = CheckedCh.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedSectionsHiddenField&quot;).value = CheckedSec.join(&quot;,&quot;);
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedObjectivesHiddenField&quot;).value = CheckedObj.join(&quot;,&quot;);

        if (typeof(_s) != &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; &amp;&amp; _s != null)
            Ccs.SaveStandards();

        if (typeof(BookContentAreaChaptersSelector) != &quot;undefined&quot; &amp;&amp; BookContentAreaChaptersSelector != null)
            BookContentAreaChaptersSelector.Save();
    },
    SaveStandards: function() {
        var hdn = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenStandardItems&quot;);
        var selectedStandardItemsHiddenField = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedStandardItemsHiddenField&quot;);

        UnCheckedStandards = new Array();
        CheckedStandards = new Array();

        if (_s.Children.length > 0) {
            Ccs.SaveStandardsChildren(_s);
            hdn.value = UnCheckedStandards.join(&quot;,&quot;);
            selectedStandardItemsHiddenField.value = CheckedStandards.join(&quot;,&quot;);
        } else {
            hdn.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            selectedStandardItemsHiddenField.value = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
        }
    },
    SaveStandardsChildren: function(item) {

        if (item.ID > 0) {
            if (!item.Checked)
                UnCheckedStandards.push(item.ID);
            else
                CheckedStandards.push(item.ID);
        }
        for (var i = 0; i &lt; item.Children.length; i++) {
            Ccs.SaveStandardsChildren(item.Children[i]);
        }
    },
    SetSelectedChapterCoverage: function(csvList) {
        Ccs.SetChapterCoverage(csvList, T);
    },

    SetHiddenChapterCoverage: function(csvList) {
        Ccs.SetChapterCoverage(csvList, F);
    },

    SetChapterCoverage: function(csvList, boolValue) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        for (var i = 0; i &lt; arr.length; i++) {
            var parts = arr[i].split(&quot; , &quot;'&quot; , &quot;/&quot; , &quot;'&quot; , &quot;);
            if (parts.length == 1) // chapter
                chapters[Number(parts[0])] = boolValue;
            else if (parts.length == 2) // section
                sections[Number(parts[0])][Number(parts[1])] = boolValue;
            else if (parts.length == 3) // objective
                objectives[Number(parts[0])][Number(parts[1])][Number(parts[2])] = boolValue;
        }
        ApplyCoverage();
    },


    Check: function (strBookPart) {
        this.SetChecked(strBookPart, T);
    },
    UnCheck: function (strBookPart) {
        this.SetChecked(strBookPart, F);
    },
    SetChecked: function (strBookPart, boolValue) {
        if (strBookPart == null || strBookPart.length == 0)
            return;
       
        var parts = strBookPart.split(&quot;.&quot;);

        if (this.UsesObjectives)
            SetObjectiveValue(Number(parts[0]), Number(parts[1]), Number(parts[2]), boolValue);
        else
            SetSectionValue(Number(parts[0]), Number(parts[1]), boolValue);

        ApplyCoverage();
        UpdateArrayValues();
    },
    CheckSelected: function (csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        arr = removeEmptyElementsFromArray(arr);

        for (var i = 0; i &lt; arr.length; i++) {
            var parts = arr[i].split(&quot; , &quot;'&quot; , &quot;.&quot; , &quot;'&quot; , &quot;);
            if (parts.length == 1) // chapter
                SetChapterValue(Number(parts[0]), T);
            else if (parts.length == 2) // section
                SetSectionValue(Number(parts[0]), Number(parts[1]), T);
            else if (parts.length == 3) // objective
                SetObjectiveValue(Number(parts[0]), Number(parts[1]), Number(parts[2]), T);
        }

        ApplyCoverage();
        UpdateArrayValues();
    },
    CheckAll: function () {
        SetAllArrayValues(true);
        UpdateArrayValues();
    },
    UnCheckAll: function () {
        SetAllArrayValues(false);
        UpdateArrayValues();
    },
    UpdateArrayValues: function () {
        UpdateArrayValues();
    },
    GetTotalCheckedAtLowestLevel: function () {
        if (this.UsesObjectives)
            return CheckedObj.length;
        return CheckedSec.length;
    },
    SetSelectedChaptersHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedChaptersHiddenField&quot;).value = csvList;
    },
    SetSelectedSectionsHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedSectionsHiddenField&quot;).value = csvList;
    },
    SetSelectedObjectivesHiddenField: function (csvList) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_SelectedObjectivesHiddenField&quot;).value = csvList;
    },

    SetHiddenStandards: function(csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        for (var i = 0; i &lt; arr.length; i++) {
            HideStandard(Number(arr[i]), _s);
        }
        ApplyCoverageStandardView();
    },
    SetSelectedStandards: function(csvList) {
        if (csvList == null || csvList.length == 0)
            return;

        var arr = csvList.split(&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;);
        for (var i = 0; i &lt; arr.length; i++) {
            ShowStandard(arr[i]);
        }
        ApplyCoverageStandardView();
    },
    SetOverallCoverageChangedFlag: function(isChanged) {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_OverallCoverageChangedHiddenField&quot;).value = isChanged ? &quot;1&quot; : &quot;0&quot;;
    },

    SetTabSwitchedFlag: function() {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TabSwitchedHiddenField&quot;).value = &quot;1&quot;;
    },

    ClearTabSwitchedFlag: function() {
        document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TabSwitchedHiddenField&quot;).value = &quot;0&quot;;
    },

    SetCoverageChangedFlag: function() {
        if (GetSelectedTab() == &quot;standard&quot;)
            document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_StandardCoverageChangedHiddenField&quot;).value = &quot;1&quot;;
        else
            document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_ChapterCoverageChangedHiddenField&quot;).value = &quot;1&quot;;
    },
    // &quot;standards&quot; or &quot;chapters&quot;
    GetCoverageBasis: function () {
        return $(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input:checked&quot; , &quot;'&quot; , &quot;).val();
    }
};

    function pageLoad(sender, args) {

        if (typeof(IsProgressControlDisplay) == &quot;function&quot; &amp;&amp; IsProgressControlDisplay()) {
            return;
        }
        OnClientPageLoad(sender, args);

        //
	    $(&quot;#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input&quot;).change(function() {
            DisableTreeBasedOnCoverageBasis();
	    });

        // 
        
        
        // During partial page loads (using UpdatePanels) we need to fix the scroll bars
	    var prm = Sys.WebForms.PageRequestManager.getInstance();
	    prm.add_pageLoaded(UpdatePanelRefreshed);

   	
        

        if (GetVisibleTree() != null)
        {
            
        
            var hiddenBookContentAreaChaptersSelectorClientPageLoadCode = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_HiddenBookContentAreaChaptersSelectorClientPageLoadCode&quot;);
            if (hiddenBookContentAreaChaptersSelectorClientPageLoadCode.value != &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
                eval(hiddenBookContentAreaChaptersSelectorClientPageLoadCode.value);

            if (GetSelectedTab() == &quot;standard&quot;)
            {
                ApplyCoverageStandardView();
                PartiallyIncludeValues();
            }
            else
            {
                ApplyCoverage();
                PartiallyIncludeValuePaths();
            }
     
            DisableTreeBasedOnCoverageBasis();

            Ccs.ClearTabSwitchedFlag();
        }  
    }

    // 
    function DisableTreeBasedOnCoverageBasis() {
        // 
        var coverageBasis = $(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_RadioButtonListCoverageBasis input:checked&quot; , &quot;'&quot; , &quot;).val();
        
        var tree = GetVisibleTree();
        if (typeof (tree) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree == null || typeof (tree.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree.RootNode == null)
            return false;
        
        var selectedTab = GetSelectedTab();
        if ((selectedTab == &quot;standard&quot; &amp;&amp; coverageBasis == &quot;chapters&quot;) || (selectedTab == &quot;chapter&quot; &amp;&amp; coverageBasis == &quot;standards&quot;))
            tree.CheckBoxesDisabled = true;
        else
            tree.CheckBoxesDisabled = false;

        SetRootCheckBoxesEnabled(tree.RootNode, !tree.CheckBoxesDisabled);
        
        return true;
    }


    function UpdatePanelRefreshed()
    {
        fixScrollbars();
        SubAllowSubmit();	    
    }

    function IPTreeView_NodeTextClicked(node) {
        if (GetSelectedTab() == &quot;standard&quot;) {
            if (node.Nodes.length > 0) // has children?
                return true;
        }
        else if ((node.Level() == 0) || (node.Level() == 1 &amp;&amp; Ccs.UsesObjectives))
            return true;
    }
    
    function ExpandAllClient()
    {
        var treeview = GetVisibleTree();

        if (typeof (treeview) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || treeview == null || typeof (treeview.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || treeview.RootNode == null)
            return false;
        
        var startNode=treeview.RootNode;
        
        for (var i=0; i&lt;startNode.Nodes.length; i++) //chapters
        {
            startNode.Nodes[i].ExpandAll();
        }

        IPTreeView_OnAfterNodeIconClicked();

        fixScrollbars();
	}

	function ExpandAll()
    {
        Ccs.SaveLists();
        SetFormTracker(true);
	}
	
    // Note: Because treeview.js is loaded in PreRender by IPTreeView, these overrides have to be outside of
    // HeadContent or else they happen before the include gets there and get overriden themselves
    // So now we have a use for PreContent... or we find another way to deal with it
	function IPTreeView_NodeIconClicked(node) {
        return true;
	}

	function IPTreeView_OnAfterNodeIconClicked() {
	    PostHeightMessage();
	}

	function IPTreeView_OnAfterNodeTextClicked() {
	    PostHeightMessage();
	}

	function PostHeightMessage() {
	    if (Ccs.SupportsPostHeightMessage) {

	        var containerEl = document.getElementById(Ccs.PostHeightMessageContainerID);
	        if (containerEl) {

	            var scrollHeight = containerEl.scrollHeight;
	            if (parent &amp;&amp; parent.postMessage) {
	                parent.postMessage(scrollHeight, &quot;*&quot;);
	                fixScrollbars();
	            }
	        }
	    }
	}
      
    function UpdateNode(nodeId)
    {
    }
     
    // CA Returns either the tree JS instance or null if none is available - useful to tell if the tree has been rendered (e.g., Course Wizard after step 1)
    // NOTE: $find and document.getElementById do not work to retrieve the tree. 
    function GetVisibleTree() {
        if (GetSelectedTab() == &quot;standard&quot;) {
            return (typeof(ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeStandards) == &quot;undefined&quot;) ? null : ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeStandards;
        } else {
            return (typeof(ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeChapters) == &quot;undefined&quot;) ? null : ctl00_ctl00_InsideForm_MasterContent_CourseCoverageSettings_TreeChapters;
        }
    }

    function IPTreeView_NodePopulated(pNode) {
        if (GetSelectedTab() == &quot;standard&quot;)
        {
            ApplyCoverageStandardView(pNode);
            PartiallyIncludeValues();
        }
        else
        {
            ApplyCoverage(pNode);
            PartiallyIncludeValuePaths();
        }

        IPTreeView_OnAfterNodeIconClicked();

        setTimeout(&quot; , &quot;'&quot; , &quot;fixScrollbars();&quot; , &quot;'&quot; , &quot;,100);
    }
    
    

    function IPTreeView_CheckboxClicked(node) {
        Ccs.SetCoverageChangedFlag();
        
        SetFormTracker(true);

        IPTreeView_OnClientCheckboxClicked();

        node.ToggleCheck();

        var ret = false;
        if (GetSelectedTab() == &quot;standard&quot;)
            ret = StandardViewCheckboxClicked(node);
        else
            ret = ChapterCoverageCheckboxClicked(node);

        // Req C14-8: Companion Study Plan
        if (Ccs.IsForCompanionStudyPlan) {
            Ccs.UpdateArrayValues();
            UpdateTotalMasteryPoints(Ccs.GetTotalCheckedAtLowestLevel());
        }

        return ret;
    }

    function UpdateTotalMasteryPoints(val)
    {
        if (parent &amp;&amp; parent.CREATEASSIGNMENT_UpdateTotalMasteryPoints)
            parent.CREATEASSIGNMENT_UpdateTotalMasteryPoints(val);
    }

    function COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked()  // CA prefix to avoid conflict with method called OnClientContentAreaCheckboxClicked on the page using this control.
    {
        Ccs.SetCoverageChangedFlag();
        
        OnClientContentAreaCheckboxClicked();
    }

    // CA Content Area filter handling
    function OnClientContentAreaSelected(chapterIDArray)
    {
        var tree = GetVisibleTree();

    if (typeof (tree) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree == null || typeof (tree.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree.RootNode == null)
        return false;

        var chapterSelected = false;       

        for (var i = 0; i &lt; chapterIDArray.length; i++)
        {
            var node = tree.FindTopLevelNode(chapterIDArray[i]);
            if (node != null)
            {
                node.UnfilterRow();

                if (node.Checked)
                    chapterSelected = true;
            }
        }

        //
        
        if (!chapterSelected)
            for (var i = 0; i &lt; chapterIDArray.length; i++)
            {
                var node = tree.FindTopLevelNode(chapterIDArray[i]);
                if (node != null)
                {
                    NodeSetCheck(node, true);
                    ChapterCoverageCheckboxClicked(node);
                }
            }

        COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked();

        return true;
    }

    function OnClientContentAreaCleared(chapterIDArray)
    {
        var tree = GetVisibleTree();
        
        if (typeof (tree) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree == null || typeof (tree.RootNode) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; || tree.RootNode == null)
            return false;

        for (var i = 0; i &lt; chapterIDArray.length; i++)
        {
            var node = tree.FindTopLevelNode(chapterIDArray[i]);
            if (node != null)
                node.FilterRow();
        }

        COURSECOVERAGESETTINGS_OnClientContentAreaCheckboxClicked();

        return true;
    }
    
    function LinkButtonWritingPracticeMoreInfo_onclick() {
        var url = &quot; , &quot;'&quot; , &quot;http://www.mywritinglab.com/writingpractice.html&quot; , &quot;'&quot; , &quot;;
        popupWindow(&quot;WritingPracticeMoreInfo&quot;,url,{Width:800,Height:500,AutoClose:true,Titlebar:true,Statusbar:false});
    }


 


		
        
        
        
    
    
    
        
    
    
    
    
    
    
    
    

    
    
    

    
    
    

    
    
    
			
        
        
                
                
                    
                        
                            Included sections                            
                            
                                Expand All
                            
                        
                    
                
                
                
				
					
						
							
								DSM. Dynamic Study Modules  
							
						
					
						
					
				
			
            
    
		
    
	
 
        

   

        
    
    
    
    
        
	
                LockDown Browser and Proctoring
            
	
                
                Use the Pearson LockDown Browser in this course  More Options
                
                    
                        
                        The Pearson LockDown Browser is not compatible with mobile devices or Chromebooks.
                    
                
                
                Use automated proctoring in this course
                
                    
		Select proctoring partner
		ProctorU Record+
		Respondus Monitor Proctoring

	
                
                    
                        
                        
                            
                                ProctorU Key:
                            
                                
                        
                        
                            
                                ProctorU Secret:
                            
                                
                        
                        
                    
                
                    
                        After you create your course, complete the Respondus Monitor Proctoring license setup.
                        Complete the Respondus Monitor Proctoring license setup
                    
                    
                
                


        
                
        
        
	
                IP Address Range
            
	
                Require students to take IP-restricted quizzes and tests within the following IP address range:
                
                    None  
                    Change...
                
            


        
       
        
                
        
        
            
                Multimedia Learning Aids
                
            
                Choose the multimedia learning aids that are available in this course.
            
        
    
    			
    
        
            
            
                Learning Aids
                
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                        
                    
                
	
	
	Ask My InstructorEdit Email Address
                                              
                                              prabhashi.hettiarachchi@pearson.com
                                              
	


                
            
        
    



    
        
            Section instructor access      
            
                Select below to choose a course level access setting.
                Go to Edit Roster in the Gradebook to modify course access for individual instructors.
            
        
        
            
            Grant full instructor access.
        
        
            
            Restrict access to the following access type:
        
        
            
            Section instructor
        
        
            
            Read-only
        
        
            
            Custom (No options selected)  Define...
            
        
    
    
    
        
            Other restricted access privileges
            Edit Ask My Instructor email address
        
        
            
            Edit course roster
        
    




    
        Cancel
Save
Next
Back

    
&quot;))]</value>
      <webElementGuid>c1b8c180-64a1-4832-87a0-a38b629c343f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
