<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_PortTestCourse</name>
   <tag></tag>
   <elementGuidId>9f224916-4353-410e-ac5b-3dc67be952f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_lblCourseName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_lblCourseName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>872f0008-752b-4784-83bf-4df00e2f0c82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_lblCourseName</value>
      <webElementGuid>119a30ff-6c70-4b18-97e7-fdcd861decb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PortTestCourse</value>
      <webElementGuid>0aec74e1-9643-470d-8dcf-c8b113b72e0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_lblCourseName&quot;)</value>
      <webElementGuid>0382f4cf-2b24-473d-b2ab-61290be0487f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a54f77c7-3194-4f65-a747-9711ab0aac20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_lblCourseName</value>
      <webElementGuid>959e64a5-1896-4093-8d65-d13993634125</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PortTestCourse</value>
      <webElementGuid>1a2c9c8d-29bf-4067-842a-1c3ed7e2a51e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_lblCourseName&quot;)</value>
      <webElementGuid>904ca062-e3a8-41af-a742-24dda09875d1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_lblCourseName']</value>
      <webElementGuid>7a505463-3347-481a-94be-84be398d2bed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivContainer']/table/tbody/tr[2]/td[2]/span</value>
      <webElementGuid>cb4aeb19-ee0e-479c-905a-3ae1bc29b397</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course name'])[1]/following::span[1]</value>
      <webElementGuid>0854c8bf-c86d-4305-8631-e27ae556ce6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about course types'])[1]/following::span[1]</value>
      <webElementGuid>5b9aa755-8b49-45a9-a944-47b094a2e081</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course Code'])[1]/preceding::span[1]</value>
      <webElementGuid>2ec0f8c5-8f06-4310-a708-f9f4c8bb05fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='PortTestCourse']/parent::*</value>
      <webElementGuid>10a8417f-86e5-4ac6-b35c-8cc96082b7a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[2]/span</value>
      <webElementGuid>d1c3db07-1c03-4681-aa0d-e8f7824b6d0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ctl00_ctl00_InsideForm_MasterContent_lblCourseName' and (text() = 'PortTestCourse' or . = 'PortTestCourse')]</value>
      <webElementGuid>660f7fe9-7c5e-4aa3-ab7c-d5be4d54a188</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
