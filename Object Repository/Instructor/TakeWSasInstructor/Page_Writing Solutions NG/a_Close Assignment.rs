<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Close Assignment</name>
   <tag></tag>
   <elementGuidId>af9b6275-a244-4b4a-8cc1-52c4d38b9248</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@href = '#' and (text() = 'Close Assignment' or . = 'Close Assignment')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.st-submit-actions > div.st-rub-link > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>094d9fe0-f371-4c42-9405-d6b872479b55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>c570e255-2137-492f-9823-53d4745d3798</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Close Assignment</value>
      <webElementGuid>2f9abfb8-025e-4670-b061-fbbada54f83b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wsol-wrapper&quot;]/div[@class=&quot;p-container&quot;]/div[@class=&quot;wsol-row&quot;]/div[@class=&quot;wsol-st-head-wrap&quot;]/div[@class=&quot;st-actions&quot;]/div[@class=&quot;st-submit-actions&quot;]/div[@class=&quot;st-rub-link&quot;]/a[1]</value>
      <webElementGuid>a6a46830-d169-469b-b6bd-bc95ff1dc2fa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div[2]/div/div[2]/div/div/a</value>
      <webElementGuid>3bb8c3b6-6d3b-49b5-9519-215e1fc3ed0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Close Assignment')]</value>
      <webElementGuid>0162186a-b460-419a-80be-0f967d4d861e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supporting files'])[1]/following::a[2]</value>
      <webElementGuid>6b8d1934-d19f-4775-9c80-d1b2614c82c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your score is based on:'])[1]/following::a[3]</value>
      <webElementGuid>8273a233-4206-4b3b-834b-1bb692535f72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saved'])[1]/preceding::a[4]</value>
      <webElementGuid>6d572f50-4d90-4e71-91a3-371699c68de6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Close Assignment']/parent::*</value>
      <webElementGuid>862eac4d-6dca-4e72-98ee-c720bd256dde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[2]</value>
      <webElementGuid>57c3030d-45b2-4d88-bd71-f904b77c0830</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/a</value>
      <webElementGuid>0b07f46a-479a-4f2a-928e-3ced7de76a4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'Close Assignment' or . = 'Close Assignment')]</value>
      <webElementGuid>06437403-be36-4a28-8841-795f3bbd8705</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
