<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p</name>
   <tag></tag>
   <elementGuidId>8b11282d-7765-49c0-b7ba-61091fa54ecc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='mce_0']/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#mce_0 > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>bb5f292e-ce07-433f-80e5-6f743683ff3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mce_0&quot;)/p[1]</value>
      <webElementGuid>70d07c91-aec3-4b4e-a7df-a07163328ff5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mce_0']/p</value>
      <webElementGuid>3dde0a87-824a-4920-a0be-711eba891b89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Table'])[1]/following::p[1]</value>
      <webElementGuid>c3b271a6-56db-4c24-9b2f-0610d6608d71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tools'])[1]/following::p[1]</value>
      <webElementGuid>f38a405a-be47-4e42-a774-c1310bfaebc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'mce_0', '&quot;', ')/p[1]')])[1]/preceding::p[3]</value>
      <webElementGuid>40ede2c0-5013-4da9-b02e-7fd51946f187</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/p</value>
      <webElementGuid>789daeda-1da6-4052-a849-5b628650b661</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
