<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Instructor Answer</name>
   <tag></tag>
   <elementGuidId>038b7112-bbd6-4641-86b1-741f00503d34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = 'Instructor Answer' or . = 'Instructor Answer')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#mce_0 > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>140fa68c-c185-48f2-ab5e-317d4eeb9659</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-mce-style</name>
      <type>Main</type>
      <value>outline: #ff0000 solid 2px;</value>
      <webElementGuid>462b0cc3-541f-41b1-bb47-30eabcc89c0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Instructor Answer</value>
      <webElementGuid>a74fb964-ca5b-4b94-8a19-70047a9ad3d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mce_0&quot;)/p[1]</value>
      <webElementGuid>70d94809-2c36-4ae8-9adf-36a86cbcef46</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mce_0']/p</value>
      <webElementGuid>b6945409-3c97-483f-8b02-9ba61cb13050</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Table'])[1]/following::p[1]</value>
      <webElementGuid>1c2b7645-3ee4-4d5b-8d91-ecbde10ea1dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tools'])[1]/following::p[1]</value>
      <webElementGuid>c9a01691-a06f-4059-bcf5-8a64b9fa3977</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'mce_0', '&quot;', ')/p[1]')])[1]/preceding::p[3]</value>
      <webElementGuid>f53f9230-b8ad-4df7-9497-10343260d71c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/p</value>
      <webElementGuid>75f1e4ec-7655-4e5f-a40c-24f095dabe82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Instructor Answer' or . = 'Instructor Answer')]</value>
      <webElementGuid>3ce83eac-a097-42e2-9a95-2b7db0406342</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
