<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Question Description</name>
   <tag></tag>
   <elementGuidId>bea4b74f-f136-48d7-b24b-bad2dfaeccef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = 'Question Description' or . = 'Question Description')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.st-instructions-val > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>3ab68b98-ac41-41be-b26e-c23710445b30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Question Description</value>
      <webElementGuid>21615fcc-13c9-419e-8a15-4136e624d5a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wsolHiddenInfo&quot;)/div[@class=&quot;wsol-stu-details-sub-info&quot;]/span[@class=&quot;st-instructions-val&quot;]/p[1]</value>
      <webElementGuid>5a39bc94-4d97-4aba-93fc-2be9bfc66455</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wsolHiddenInfo']/div/span[2]/p</value>
      <webElementGuid>2ad123c2-9204-4d11-ba18-2299030b61e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Instructions'])[1]/following::p[1]</value>
      <webElementGuid>a1515cfc-7f27-4f4e-879d-372fe4c4a85f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning objective'])[1]/following::p[2]</value>
      <webElementGuid>8530fdf6-11f6-4cce-bb0f-0c29dfb73437</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your score is based on:'])[1]/preceding::p[1]</value>
      <webElementGuid>8e9e4865-1814-4a71-a5ef-f3f3a54844ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supporting files'])[1]/preceding::p[1]</value>
      <webElementGuid>266de35d-69c6-48a9-9e45-4cfc6ea9c159</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Question Description']/parent::*</value>
      <webElementGuid>835c5076-82a5-43cc-abcc-c035182b287a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/p</value>
      <webElementGuid>f202e062-9ba6-4293-88b3-a825a01a9112</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Question Description' or . = 'Question Description')]</value>
      <webElementGuid>7dd7b250-8283-4a55-b1da-bf8ce71e2bd5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
