<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_password</name>
   <tag></tag>
   <elementGuidId>179cc0c2-4de9-4c0f-8c7f-8ecec7a6de45</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#password</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9405f90c-535a-4995-8bea-45613fb05c8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>cec6542f-457e-4321-8280-274b1ff303e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>7078f69c-8c51-41bc-a99e-128e22c124af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>ed7f4979-9a65-40b2-96a6-0f0f60ea0394</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>user.password</value>
      <webElementGuid>4a8a1a44-e41c-4b15-9230-2ca69b417a7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>hasError('password') ? 'pe-textInput--input_error' : 'pe-textInput'</value>
      <webElementGuid>6992fba1-48c1-4c7f-bb12-a52b77987ca6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>prsn-password-toggle</name>
      <type>Main</type>
      <value>fancy</value>
      <webElementGuid>5c3a2943-15a0-4662-8a42-71df066ccb1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>prsn-caps-lock-state</name>
      <type>Main</type>
      <value>fancy</value>
      <webElementGuid>23266c12-2750-45e9-9276-69b11b155d7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>237a411a-75f5-4be0-8a01-3908abe6aea5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>29923295-19da-47a8-8cbc-9492810ef237</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required pe-textInput</value>
      <webElementGuid>3640d90b-1a5c-4faa-91dc-13e4ddd59f12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>c73a4b3a-207d-40c2-8cab-c82f3c3cd883</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password&quot;)</value>
      <webElementGuid>d47af3b5-c189-4efc-ace1-5d7705a170d8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password']</value>
      <webElementGuid>5b4f6102-6b6b-4404-86e1-508e227eeea9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mainForm']/div[3]/input</value>
      <webElementGuid>80625d34-bace-4d04-8194-87ae0e53ad94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>881467c9-d9e0-41c2-9482-c7e981e5eaae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'password' and @name = 'password']</value>
      <webElementGuid>530c6732-31ab-4b21-804c-254533f50f42</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
