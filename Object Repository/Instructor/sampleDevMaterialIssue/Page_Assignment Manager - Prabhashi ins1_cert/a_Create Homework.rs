<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create Homework</name>
   <tag></tag>
   <elementGuidId>ca3b6290-5e01-4a4a-82aa-6482ffbaff4d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > ul > li > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>42c66b86-6088-41a6-834b-784ec63e8d48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:GoCreate('homework')</value>
      <webElementGuid>43ac6745-d847-40bb-92c1-949ee0b55613</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Homework</value>
      <webElementGuid>b658d2ef-3f11-4655-9d77-97b0856737f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_hwAndTestManagerToolbar&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[1]/a[1]</value>
      <webElementGuid>121c7d32-41e2-4e30-999d-e3dbd63904d0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_hwAndTestManagerToolbar']/ul/li/div/div[2]/ul/li/a</value>
      <webElementGuid>5bfad723-083a-4ec9-aecb-082aef943ee9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create Homework')]</value>
      <webElementGuid>38d7e716-6b2b-4bfd-a7f0-44cc316461f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::a[2]</value>
      <webElementGuid>c5fc7a60-4cc8-4ffe-a977-5cf1cd044203</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Assignment Manager'])[1]/following::a[3]</value>
      <webElementGuid>fde5b1df-2e6c-4369-bb98-ce9e6882e3a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Quiz'])[1]/preceding::a[1]</value>
      <webElementGuid>48f1f6c3-ef15-49ac-9940-2369dbba5d27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Test'])[1]/preceding::a[2]</value>
      <webElementGuid>a9be22d5-928b-4e2f-876e-54cadfa4da18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create Homework']/parent::*</value>
      <webElementGuid>81a7f012-848a-417c-a9a7-1204b8917591</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:GoCreate('homework')&quot;)]</value>
      <webElementGuid>3bc7530f-a21d-4e34-baf5-57309ea8282e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>bfbeda26-4c70-43e6-bfce-9604b329a5d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:GoCreate(&quot; , &quot;'&quot; , &quot;homework&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Create Homework' or . = 'Create Homework')]</value>
      <webElementGuid>78eeb00a-0e01-4502-a9b3-42e6a2ce79b1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
