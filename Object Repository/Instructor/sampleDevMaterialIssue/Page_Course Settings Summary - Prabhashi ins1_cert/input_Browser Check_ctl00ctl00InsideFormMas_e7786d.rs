<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Browser Check_ctl00ctl00InsideFormMas_e7786d</name>
   <tag></tag>
   <elementGuidId>3b6a5ef9-3cca-4ea6-8693-40e5f170ada5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='imgBtnBack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#imgBtnBack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b7aaf4e7-60ac-407f-85e8-c0df51b834c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b22e095c-5947-4d59-947c-c0387c03539c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$imgBtnBack</value>
      <webElementGuid>fac60517-770c-425f-b855-d2b239c37e34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Done</value>
      <webElementGuid>0317d97a-f203-49fd-a3bb-f8721fc3d98b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value> return GoBack(); ;__doPostBack('ctl00$ctl00$InsideForm$MasterContent$imgBtnBack','')</value>
      <webElementGuid>f8acd931-8c62-408d-a653-4eb65536f622</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>imgBtnBack</value>
      <webElementGuid>bf4221c8-1ad9-43e7-9f46-095a0f4dfe5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>b23680e2-f4e3-4200-96c0-5d6d73b49e4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;imgBtnBack&quot;)</value>
      <webElementGuid>b76aa045-be99-435e-b51b-4fbd6d033024</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='imgBtnBack']</value>
      <webElementGuid>4e780702-3ab6-4460-9120-f7ce5c43a843</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div[2]/div/input</value>
      <webElementGuid>86f1ae0d-19ab-4407-9845-2afca9d1cb0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/input</value>
      <webElementGuid>2b98d12e-53b6-4882-8ca3-ebd7ff672be9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button' and @name = 'ctl00$ctl00$InsideForm$MasterContent$imgBtnBack' and @id = 'imgBtnBack']</value>
      <webElementGuid>f148f909-bc63-464e-a501-07a6d995381c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
