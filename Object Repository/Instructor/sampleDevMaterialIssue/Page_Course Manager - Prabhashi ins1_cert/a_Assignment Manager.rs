<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Assignment Manager</name>
   <tag></tag>
   <elementGuidId>51fb7e7d-9109-4ac6-addc-7a8558ed2f13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='storeNavData(&quot;27&quot;)']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8f93493e-38e3-41f6-bc05-f90a34dc9a9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1</value>
      <webElementGuid>20ef2f29-d20f-447b-b7e7-be3da127d46b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>1a53db66-74cc-49d7-a975-d22d06050a25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>8e15b7f3-0729-4943-bb14-9a61b6ef51c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;27&quot;)</value>
      <webElementGuid>c49a9266-9f14-421a-97e3-5b922de3ddbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>e7db74f7-3405-4e41-b235-a200b5a04ae8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>6cb9da34-9fe2-4374-bc2d-aaa9e7cd48a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>a3f61b55-5823-4049-ae51-a56149fa656b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;27&quot;)']</value>
      <webElementGuid>8eb2ad54-7e93-45d4-a6e9-2d88368ace4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[12]/a</value>
      <webElementGuid>a3539466-d0fd-40c2-a073-364ca2e6b607</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Assignment Manager')]</value>
      <webElementGuid>4cfa065f-1eea-4bc2-9e7d-98e92078b825</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home Page Manager'])[1]/following::a[1]</value>
      <webElementGuid>3a4aafcf-4282-4a41-9720-09573cf2faf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course Manager'])[1]/following::a[2]</value>
      <webElementGuid>37095afa-f574-4844-93e5-10e98bd023f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan Manager'])[1]/preceding::a[1]</value>
      <webElementGuid>630bb058-4697-4fb5-95b1-e448b0d3d31e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook'])[1]/preceding::a[2]</value>
      <webElementGuid>0ff95f98-2de2-4728-97dc-0eedef8a15c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Assignment Manager']/parent::*</value>
      <webElementGuid>ce820ab1-d9ba-459f-b779-51595108cee0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1')]</value>
      <webElementGuid>2fc288f2-437e-478f-a005-ed17fff61937</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/a</value>
      <webElementGuid>52e0ba43-7e71-48b6-8aa7-c70e5e3fb3cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1' and @title = 'Assignment Manager' and (text() = 'Assignment Manager' or . = 'Assignment Manager')]</value>
      <webElementGuid>e56ed882-fb26-4c2b-adc0-235948785fd2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
