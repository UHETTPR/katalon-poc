<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Next</name>
   <tag></tag>
   <elementGuidId>f19b2520-6146-4928-9fd6-1a0d7dd51eeb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='imgBtnNext']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#imgBtnNext</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a11c331e-f06d-4c72-8234-837c5cfb561e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$imgBtnNext','')</value>
      <webElementGuid>780ed9fe-02cc-4e9b-8bef-3b5748b475b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>imgBtnNext</value>
      <webElementGuid>95e01733-2835-4596-b696-e24e5d015139</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>if(!SubCheckSubmit(this)) return false; return NextStep();</value>
      <webElementGuid>52c0c02a-3f11-42fe-830f-13780cd3bfd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>ef559650-dde5-471b-82cc-c04efb1caf2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next
</value>
      <webElementGuid>6c26d255-435c-4e4e-b7c1-102aed2222c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;imgBtnNext&quot;)</value>
      <webElementGuid>a9da569e-5fcf-4b34-b5bc-0533f3687ef4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='imgBtnNext']</value>
      <webElementGuid>63a61168-7b73-4d9a-b19f-f7e1095724fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='divbtnNext']/a</value>
      <webElementGuid>9e35ed86-6860-45ee-b016-33311862c837</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Next')]</value>
      <webElementGuid>2e10e893-c363-4aef-b612-1eab435fe9b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/following::a[1]</value>
      <webElementGuid>72b1c90e-4adc-466d-a1d3-aff2f5c2e855</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::a[2]</value>
      <webElementGuid>6714385e-fefe-4998-9c42-ef0f8e9be51a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::a[1]</value>
      <webElementGuid>6b27684f-f0c1-443f-887d-30694296f4b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[2]/preceding::a[2]</value>
      <webElementGuid>c0c5e202-feaa-4d9e-9542-573466f97abf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>213da000-3eef-46bb-a77e-727ecdcadabb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$imgBtnNext','')&quot;)]</value>
      <webElementGuid>27ca23e2-8109-4be5-bd75-6eaa316b5300</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div[3]/a</value>
      <webElementGuid>510c7dfb-a162-4b61-8b6c-db95030ee4fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:__doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$imgBtnNext&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)&quot;) and @id = 'imgBtnNext' and (text() = 'Next
' or . = 'Next
')]</value>
      <webElementGuid>bdcdf1e2-eb48-4a98-80a5-16763ab9bd82</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
