<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Next</name>
   <tag></tag>
   <elementGuidId>06353236-d08f-4ce3-bd41-41063834844d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='imgBtnNext']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#imgBtnNext</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b9d6036e-8412-4e15-95c7-65af370b386b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>imgBtnNext</value>
      <webElementGuid>ad32e63b-4386-41f6-ac09-311b55eead39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>NextStep();</value>
      <webElementGuid>e2fc3b20-7bc3-444f-8d2e-6f476e32c0fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>ccb7ae72-cdfb-4acb-890b-529332c9a61a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next
</value>
      <webElementGuid>4b52114a-304e-4c7e-8033-e9df140e2880</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;imgBtnNext&quot;)</value>
      <webElementGuid>45f06a24-c55c-425c-8fc3-a5f4538f0507</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='imgBtnNext']</value>
      <webElementGuid>21cf49e9-8269-4ced-8489-8f1db643b57d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='divbtnNext']/a</value>
      <webElementGuid>d1d44cc0-7f3d-4f8c-aca5-88cb85194b50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Next')]</value>
      <webElementGuid>05c027e8-e57f-4e7a-903d-b3dc7a2bad28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[4]/following::a[1]</value>
      <webElementGuid>a0f61894-33c4-484f-b89e-4ea511299ca3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply to Selected'])[2]/following::a[2]</value>
      <webElementGuid>097e9c79-cb05-48ba-90f1-fc4519b3958f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save &amp; Assign'])[1]/preceding::a[1]</value>
      <webElementGuid>26fc8e75-338d-431f-865b-6119d43ad143</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/preceding::a[2]</value>
      <webElementGuid>65a8348c-e4fb-434d-8f83-f8c8f6fce71a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>55339552-51f0-4946-bc81-a92c053fe54a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/a</value>
      <webElementGuid>e2fab699-3576-43c3-8963-4a256d943261</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'imgBtnNext' and (text() = 'Next
' or . = 'Next
')]</value>
      <webElementGuid>452f04c1-95a4-4a01-ada4-4fa0d51fc586</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
