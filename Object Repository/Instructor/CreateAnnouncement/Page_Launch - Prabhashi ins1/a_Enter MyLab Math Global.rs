<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Enter MyLab Math Global</name>
   <tag></tag>
   <elementGuidId>11895ceb-3213-47b7-a12f-762cf248bac0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='BtnEnter']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#BtnEnter</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>42a106e0-b34c-477e-a535-4524c2430606</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6594a02c-a705-404d-9f36-4b9b3fd1e22b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0)</value>
      <webElementGuid>ff240778-2657-489e-b2ab-875637c0c791</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BtnEnter</value>
      <webElementGuid>6de220a7-58a6-4180-9b89-90a0321fbf4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>LoadSite();return false</value>
      <webElementGuid>db508aeb-4844-4666-a519-cbad7a4e8dce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>c096b836-04b0-47f7-ace3-159ec69adc31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>27dbd74a-44af-4fc6-950b-d5d2440d4260</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Enter MyLab Math Global
</value>
      <webElementGuid>4ba1dfc4-c5ec-4d28-8ac3-4ed0a2062b60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BtnEnter&quot;)</value>
      <webElementGuid>934ff505-1ee7-4b4d-86f8-6289051357e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>07404a19-af01-4b06-bbea-1e3cf72dcb5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a1a88613-f178-45b7-b30a-236fea42f3ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0)</value>
      <webElementGuid>21a94ffe-99c5-433d-a3ae-69e14596bba9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BtnEnter</value>
      <webElementGuid>597f439e-482c-49b3-b342-c63d8b1dd05f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>LoadSite();return false</value>
      <webElementGuid>648a6736-6eb0-4235-8a53-375e03d29023</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>fa7a5f41-7771-4def-bc5b-9dbad4035bd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>5eeadddd-fb73-491d-a67d-62d4448430f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Enter MyLab Math Global
</value>
      <webElementGuid>244dd252-8783-49ce-9f78-7c061320a202</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BtnEnter&quot;)</value>
      <webElementGuid>86cea342-62d9-4b6b-9a87-a03cea496ea9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='BtnEnter']</value>
      <webElementGuid>391ff8ad-c245-4b2a-ac02-ff63d6b2c14d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[4]/div/div[3]/a</value>
      <webElementGuid>17f1b266-04bd-4f5a-b2d7-bcab94ea73c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Enter MyLab Math Global')]</value>
      <webElementGuid>0e89d2ef-6b37-4137-b693-11ef9c4257a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to MyLab Math Global!'])[1]/following::a[1]</value>
      <webElementGuid>c50d52c2-a8c5-4ada-8de1-9e55f78e2ffb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to MyLab Math Global Log In'])[1]/following::a[1]</value>
      <webElementGuid>7c59846a-d2bf-4d2c-9d4e-66816993e74d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='[+]'])[1]/preceding::a[1]</value>
      <webElementGuid>8a46a9cc-dd2e-4c90-bb31-0ea80e56a6dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'BtnEnter', '&quot;', ')')])[1]/preceding::a[1]</value>
      <webElementGuid>6767d7f3-1ad1-4806-ac0b-bf6cc926026a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Enter MyLab Math Global']/parent::*</value>
      <webElementGuid>ee4ac23d-f41f-4953-a3d8-6ff4cd3ef1e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0)')])[2]</value>
      <webElementGuid>0c4ab683-39d7-4cef-8e11-d060a689f54c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a</value>
      <webElementGuid>23e22859-e1b0-4d6d-af03-21a456ad996b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0)' and @id = 'BtnEnter' and (text() = 'Enter MyLab Math Global
' or . = 'Enter MyLab Math Global
')]</value>
      <webElementGuid>dd5613e5-408f-4d13-874b-163cf63ae042</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
