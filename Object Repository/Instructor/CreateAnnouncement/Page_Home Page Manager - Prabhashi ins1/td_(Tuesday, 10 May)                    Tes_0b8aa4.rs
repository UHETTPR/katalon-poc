<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_(Tuesday, 10 May)                    Tes_0b8aa4</name>
   <tag></tag>
   <elementGuidId>a3a73fa2-a2b0-4324-a73b-3f41d4d855ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                    (Tuesday, 10 May)
                    Test Announcement Creation
                    Test Announcement Body
                ' or . = '
                    (Tuesday, 10 May)
                    Test Announcement Creation
                    Test Announcement Body
                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.leftnone</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>7efcbeaa-4326-490d-b2d6-09c88de01448</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>leftnone</value>
      <webElementGuid>de0ad0ae-c916-48aa-8d21-307638aea9be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    (Tuesday, 10 May)
                    Test Announcement Creation
                    Test Announcement Body
                </value>
      <webElementGuid>47208675-c65f-4ead-901d-3b91f3b761e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_GridView1_35764&quot;)/td[@class=&quot;leftnone&quot;]</value>
      <webElementGuid>a34b94c4-5a76-4f2e-ac79-447a19ea1d08</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_GridView1_35764']/td[3]</value>
      <webElementGuid>8558b931-8f90-4dec-a4d9-c2d68a662c38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/following::td[3]</value>
      <webElementGuid>a55c9d7e-6379-4867-a1b3-cadd5745152b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove'])[1]/following::td[3]</value>
      <webElementGuid>09f74031-2c9f-4b49-a232-9ed525575e7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-- Select --'])[1]/preceding::td[4]</value>
      <webElementGuid>22165a93-a591-4ec9-aeeb-04d00971bb98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]</value>
      <webElementGuid>5043e05c-18ab-44e7-ab2d-e531b951a510</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                    (Tuesday, 10 May)
                    Test Announcement Creation
                    Test Announcement Body
                ' or . = '
                    (Tuesday, 10 May)
                    Test Announcement Creation
                    Test Announcement Body
                ')]</value>
      <webElementGuid>83db47be-d442-4998-8f99-8280694b31b4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
