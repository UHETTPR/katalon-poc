<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Welcome, Prabhashi</name>
   <tag></tag>
   <elementGuidId>223dea91-e142-4ba9-b8c5-16dc0fa3bd2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.pe-page-title.pe-page-title--small.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h1[(text() = 'Welcome, Prabhashi' or . = 'Welcome, Prabhashi')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>48eb5cdf-f075-4d37-8811-a20397a0e37c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-page-title pe-page-title--small ng-binding</value>
      <webElementGuid>3602215b-e3f1-4f5e-bb10-f4ec7a729f02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Welcome, Prabhashi</value>
      <webElementGuid>e74f9605-a6ee-48f9-91b7-ef606ae3ca1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainForm&quot;)/h1[@class=&quot;pe-page-title pe-page-title--small ng-binding&quot;]</value>
      <webElementGuid>521d7ef8-6269-4937-a314-402fb0ec1825</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mainForm']/h1</value>
      <webElementGuid>c0f39b8c-a8a2-4d0b-a9aa-e7dfee7f606b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open Help'])[2]/following::h1[1]</value>
      <webElementGuid>228b8080-2153-43e9-b01a-76bce9d5d812</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open Help'])[1]/following::h1[1]</value>
      <webElementGuid>3d90c734-67f0-4f53-a4ce-0714d1225817</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Country'])[1]/preceding::h1[1]</value>
      <webElementGuid>c5f53430-0222-4f43-b43c-9f8d5544680b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Welcome']/parent::*</value>
      <webElementGuid>341f1227-d79c-460c-a4d1-a4daa84effc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>ceb7bf10-e5de-43c6-a104-9fbc006733a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Welcome, Prabhashi' or . = 'Welcome, Prabhashi')]</value>
      <webElementGuid>847f2c03-3e72-4af4-811f-a7ef43005299</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
