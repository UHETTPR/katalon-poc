<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Open HelpWelcome, PrabhashiTo ensure we_567ce3</name>
   <tag></tag>
   <elementGuidId>6a7a6df1-1648-48fc-9cf6-37b14d24ab38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='e2e-comms-wrapper']/div/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.pe-template__double--main</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>410a2ffe-429c-4fc4-ba9f-bf736b602108</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-template__double--main</value>
      <webElementGuid>403bbeae-7dd0-4a38-ae32-a46eab918a4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Open HelpWelcome, PrabhashiTo ensure we meet regulations for your data privacy, please provide your country of residence and information about your age.Country Open Help
    
 Add and continue </value>
      <webElementGuid>634df802-b9e4-4a4c-a9be-e909ee998809</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;e2e-comms-wrapper&quot;)/div[@class=&quot;auth-layer-wrapper ng-scope&quot;]/div[@class=&quot;auth-layer ng-scope&quot;]/div[@class=&quot;pe-template__double&quot;]/div[@class=&quot;pe-template__double--main&quot;]</value>
      <webElementGuid>5bea8541-08e9-4aff-b6e4-61df476b355d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='e2e-comms-wrapper']/div/div/div/div[2]</value>
      <webElementGuid>ef508ba8-d4b3-455e-aef7-1a1013948e62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open Help'])[1]/following::div[1]</value>
      <webElementGuid>818ea8ae-23d5-4085-ab4a-44a491d9f2bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hello. Sign in and let the learning begin!'])[1]/following::div[1]</value>
      <webElementGuid>4311feae-b932-453d-bc2a-40dc1e161289</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[2]</value>
      <webElementGuid>ffd24e9f-d316-4d0e-862f-5ffadd99c071</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Open HelpWelcome, PrabhashiTo ensure we meet regulations for your data privacy, please provide your country of residence and information about your age.Country Open Help
    
 Add and continue ' or . = ' Open HelpWelcome, PrabhashiTo ensure we meet regulations for your data privacy, please provide your country of residence and information about your age.Country Open Help
    
 Add and continue ')]</value>
      <webElementGuid>42115df6-a329-4645-ae4c-48c1b50e09c6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
