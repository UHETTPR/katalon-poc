<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Dev Auth</name>
   <tag></tag>
   <elementGuidId>e6551fdf-9b0c-49e9-9abc-6de591c8eca8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h1[(text() = '
                    Dev Auth' or . = '
                    Dev Auth')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>8b6a0d77-b6fa-4360-95ce-9d3b1f931684</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Dev Auth</value>
      <webElementGuid>d28737fd-6b99-4660-9880-9a1ca892d98f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;title&quot;)/h1[1]</value>
      <webElementGuid>d5cfeedb-1ac7-41bb-90d4-6a79e2566df9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>3c45d048-84d4-496c-b362-897e15d96308</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Dev Auth</value>
      <webElementGuid>fd95a933-c5be-4532-965a-a6cb3b188e77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;title&quot;)/h1[1]</value>
      <webElementGuid>fdb1cc39-3aa8-4906-8323-fcc377b674a9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='title']/h1</value>
      <webElementGuid>7f92120d-be67-44dc-8f96-b833f72f22ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account Information'])[1]/preceding::h1[1]</value>
      <webElementGuid>2889a21d-c3c7-4aca-87cd-250e97dafc85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invalid Username/Password Combination'])[1]/preceding::h1[1]</value>
      <webElementGuid>0146203a-e55c-453d-8955-809c03ccea0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Dev Auth']/parent::*</value>
      <webElementGuid>664f1799-e373-487a-a79b-d62a4f3a9728</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>c485b3fe-8cff-4cf9-b697-3ca6b1c2cdf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = '
                    Dev Auth' or . = '
                    Dev Auth')]</value>
      <webElementGuid>40fa1711-6ad3-4cc4-838e-ef628b728d3a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
