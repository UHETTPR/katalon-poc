<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 1.3.17_chk990080.1.3.19.65.0</name>
   <tag></tag>
   <elementGuidId>ac735d53-28eb-4d62-9597-b3783d472711</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.1.3.19.65.0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9713f3d6-90cc-484e-be73-6976964755c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>4d5c501b-9141-4064-bea0-e81017d7595f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.1.3.19.65.0</value>
      <webElementGuid>5c02ed2a-ca33-4d5f-8c98-5b3aed5c9ae8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 1.3.19 (Evaluate algebraic expressions.)</value>
      <webElementGuid>169a3b6d-7c39-4f2d-840f-f326325c86d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.1.3.19.65.0&quot;)</value>
      <webElementGuid>d6371892-39f6-4c62-ab97-d144c5a65710</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.1.3.19.65.0']</value>
      <webElementGuid>53d5b822-becd-4356-abfa-8dc8b6fc31f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.1.3.19.65.0']/div/div[2]/input</value>
      <webElementGuid>acc38fe8-d781-4ab7-95a0-15092ac8f1b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/div/div[2]/input</value>
      <webElementGuid>76b3e3a0-8ad0-4903-8d9c-8d302eab01b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.1.3.19.65.0' and @title = '(P) 1.3.19 (Evaluate algebraic expressions.)']</value>
      <webElementGuid>0fb5ad53-8d87-4f3c-811f-403337ce4459</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
