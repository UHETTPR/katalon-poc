<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Minimum mastery points required_ctl00_02668a</name>
   <tag></tag>
   <elementGuidId>b8df38c6-7295-4cff-87e9-8ca2ce35dd3e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e31ea430-a992-4782-8156-2c4b03fe2959</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints</value>
      <webElementGuid>6bd59d85-8caa-4b59-b184-8c53a7b525eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$TextBoxMinimumMasteryPoints</value>
      <webElementGuid>c792c0bd-bff5-4bc1-bbda-c75ab14d412e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>riTextBox riEnabled</value>
      <webElementGuid>29307fb8-e86f-4c0a-a5e7-420b934dacd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>decimaldigits</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>b85ec274-5efe-4634-a5d2-5a59ec32e7fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>65ca4caa-0d76-45d5-93d4-8fa0e383d9df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>6a37aa2a-8400-463a-9c57-7f0fb4519f59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints&quot;)</value>
      <webElementGuid>d6533242-55b6-4703-97ed-e1b4d1f79de7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints']</value>
      <webElementGuid>6080575e-1e01-44f8-984f-43f82c69e1a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints_wrapper']/input</value>
      <webElementGuid>b119c378-1f45-415f-959d-45c139deb6b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[2]/span/input</value>
      <webElementGuid>d0ee3d3a-8fa9-4af3-90a5-61c526ffa939</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ctl00_ctl00_InsideForm_MasterContent_TextBoxMinimumMasteryPoints' and @name = 'ctl00$ctl00$InsideForm$MasterContent$TextBoxMinimumMasteryPoints' and @type = 'text']</value>
      <webElementGuid>391b3482-774a-48ac-a99b-2e75d65ae84f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
