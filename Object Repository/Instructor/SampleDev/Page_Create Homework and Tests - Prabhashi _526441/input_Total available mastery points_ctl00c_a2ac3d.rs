<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Total available mastery points_ctl00c_a2ac3d</name>
   <tag></tag>
   <elementGuidId>5c107810-257f-44fd-9755-27a7d36234df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxTotalMasteryPoints']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_TextBoxTotalMasteryPoints</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>324becb8-3aa7-42eb-86cf-9289bef7cf49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_TextBoxTotalMasteryPoints</value>
      <webElementGuid>ab9aa58c-c23f-445c-830f-c52b921e5cc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$TextBoxTotalMasteryPoints</value>
      <webElementGuid>ef10091d-e004-4813-bc66-bdff2a687260</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>riTextBox riEnabled</value>
      <webElementGuid>1366e68e-f6bd-49c3-94fd-5d4ce347ca4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>47e43d0b-2e40-4eed-b509-29f758ce1583</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TextBoxTotalMasteryPoints&quot;)</value>
      <webElementGuid>99f3539b-93ee-43ec-83b0-a7f5a4b87753</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxTotalMasteryPoints']</value>
      <webElementGuid>1525cf1f-36c3-4cca-98fb-663db8b5b653</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_TextBoxTotalMasteryPoints_wrapper']/input</value>
      <webElementGuid>07bebcf7-cc5a-4334-8645-0083cc6216d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/table/tbody/tr/td[2]/span/input</value>
      <webElementGuid>4a2c20c3-131f-4789-8fef-038d574f93e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ctl00_ctl00_InsideForm_MasterContent_TextBoxTotalMasteryPoints' and @name = 'ctl00$ctl00$InsideForm$MasterContent$TextBoxTotalMasteryPoints' and @type = 'text']</value>
      <webElementGuid>f15a5dab-c92c-480b-a5b9-2a73cbe8fb3d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
