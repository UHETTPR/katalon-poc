<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Prabhashi ins1_cert                     _244953</name>
   <tag></tag>
   <elementGuidId>71a64c0e-9e99-471f-a5eb-364465f62925</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='leftnavtable']/tbody/tr[2]/td</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>c0d95947-6df5-449a-aa46-e2e510f392dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>valign</name>
      <type>Main</type>
      <value>top</value>
      <webElementGuid>4f4f263c-f270-458f-961e-5ec59dd48036</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>100%</value>
      <webElementGuid>36d350be-81f7-4fd5-892a-486d7a86fb34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>100%</value>
      <webElementGuid>fb29a04a-5b96-4685-82eb-8470de964478</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                    
                                                                

                                                                
                                                                    
                                                                 
                                                                    
	                                                                   
                                                                        
                                                                        
                                                                        
                                                                 
                                                                        
                                                                    

                                                                  

                                                                

                                                                
	
		
                                                                            
                                                                            
                                                                                
                                                                                    Prabhashi ins1_cert
                                                                                    DevTestingCourse
                                                                                
                                                                                
                                                                                    Prabhashi ins1_cert
                                                                                    NextGen Media Math Test Book--REAL
                                                                                
                                                                                
                                                                                    06/03/22
                                                                                    12:30pm
                                                                                
                                                                            
                                                                            
                                                                            
                                                                        
	


                                                                
                                                                    
                                                                    
                                                                
                                                                
                                                                
	
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$InsideForm$MasterContent$ScriptManager1', 'aspnetForm', ['tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelTabs','','tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelBookFilter','','tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelCheckboxFilters',''], ['ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook','LinkChangeBook','ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter','DropDownListChapter','ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','DropDownListSection','ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListObjective','DropDownListObjective','ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode',''], [], 90, 'ctl00$ctl00');
//]]>




	
    








	learningAids = new Array();


		
			var learningAidguidedsolution = new Object();
			learningAidguidedsolution.name = 'guidedsolution';
			learningAidguidedsolution.displayText = &quot;SBHelp Me Solve This&quot;;
			learningAidguidedsolution.imageUrl = 'images/icons/coursemanager_icon_guidedsolution.gif';
			learningAidguidedsolution.enabled = true;
			learningAidguidedsolution.hasCustomLogic = false;
			learningAidguidedsolution.visible = true;
			learningAidguidedsolution.isExerciseSpecific = true;
			learningAidguidedsolution.exerciseExists = false;
			learningAidguidedsolution.playerParamName = 'showguidedsolution';
			//learningAidguidedsolution.initCapName = learningAidguidedsolution.name.substring(0, 1).toUpperCase() + learningAidguidedsolution.name.substring(1);
			learningAidguidedsolution.typeID = 10;

			learningAids.push(learningAidguidedsolution);


			function ViewSample_guidedsolution()
			{
				popupWindow(&quot;guidedsolution&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/images/helpmesolvethis700.jpg&quot;, {Width:700,Height:460,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidexample = new Object();
			learningAidexample.name = 'example';
			learningAidexample.displayText = &quot;SBView an Example&quot;;
			learningAidexample.imageUrl = 'images/icons/coursemanager_icon_sampleproblem.gif';
			learningAidexample.enabled = true;
			learningAidexample.hasCustomLogic = false;
			learningAidexample.visible = true;
			learningAidexample.isExerciseSpecific = true;
			learningAidexample.exerciseExists = false;
			learningAidexample.playerParamName = 'showexample';
			//learningAidexample.initCapName = learningAidexample.name.substring(0, 1).toUpperCase() + learningAidexample.name.substring(1);
			learningAidexample.typeID = 11;

			learningAids.push(learningAidexample);


			function ViewSample_example()
			{
				popupWindow(&quot;example&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/images/showexample700.jpg&quot;, {Width:700,Height:460,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidvideo = new Object();
			learningAidvideo.name = 'video';
			learningAidvideo.displayText = &quot;SBVideoooo&quot;;
			learningAidvideo.imageUrl = 'images/icons/coursemanager_icon_video.gif';
			learningAidvideo.enabled = true;
			learningAidvideo.hasCustomLogic = false;
			learningAidvideo.visible = true;
			learningAidvideo.isExerciseSpecific = true;
			learningAidvideo.exerciseExists = false;
			learningAidvideo.playerParamName = 'showvideo';
			//learningAidvideo.initCapName = learningAidvideo.name.substring(0, 1).toUpperCase() + learningAidvideo.name.substring(1);
			learningAidvideo.typeID = 1;

			learningAids.push(learningAidvideo);


			function ViewSample_video()
			{
				popupWindow(&quot;video&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/flash_video_player/player.html?/aw/aw_bittinger_eaconapp_8/video/bke08_0101e01&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidanimation = new Object();
			learningAidanimation.name = 'animation';
			learningAidanimation.displayText = &quot;SBAnimation&quot;;
			learningAidanimation.imageUrl = 'images/icons/coursemanager_icon_animation.gif';
			learningAidanimation.enabled = true;
			learningAidanimation.hasCustomLogic = false;
			learningAidanimation.visible = true;
			learningAidanimation.isExerciseSpecific = true;
			learningAidanimation.exerciseExists = false;
			learningAidanimation.playerParamName = 'showanimation';
			//learningAidanimation.initCapName = learningAidanimation.name.substring(0, 1).toUpperCase() + learningAidanimation.name.substring(1);
			learningAidanimation.typeID = 2;

			learningAids.push(learningAidanimation);


			function ViewSample_animation()
			{
				popupWindow(&quot;animation&quot;, &quot;http://media.pearsoncmg.com/aw/aw_mml_shared_1/precalculus/01020141/index.html&quot;, {Width:620,Height:370,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtextbook = new Object();
			learningAidtextbook.name = 'textbook';
			learningAidtextbook.displayText = &quot;SBTextbook&quot;;
			learningAidtextbook.imageUrl = 'images/icons/coursemanager_icon_textbook.gif';
			learningAidtextbook.enabled = true;
			learningAidtextbook.hasCustomLogic = false;
			learningAidtextbook.visible = true;
			learningAidtextbook.isExerciseSpecific = true;
			learningAidtextbook.exerciseExists = false;
			learningAidtextbook.playerParamName = 'showtextbook';
			//learningAidtextbook.initCapName = learningAidtextbook.name.substring(0, 1).toUpperCase() + learningAidtextbook.name.substring(1);
			learningAidtextbook.typeID = 5;

			learningAids.push(learningAidtextbook);


			function ViewSample_textbook()
			{
				popupWindow(&quot;textbook&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/mxl_ebook_view_sample/aps08_flash_main.html&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidconceptbook = new Object();
			learningAidconceptbook.name = 'conceptbook';
			learningAidconceptbook.displayText = &quot;Concept Review&quot;;
			learningAidconceptbook.imageUrl = 'images/icons/coursemanager_icon_conceptbook.png';
			learningAidconceptbook.enabled = true;
			learningAidconceptbook.hasCustomLogic = false;
			learningAidconceptbook.visible = true;
			learningAidconceptbook.isExerciseSpecific = true;
			learningAidconceptbook.exerciseExists = false;
			learningAidconceptbook.playerParamName = 'showconceptbook';
			//learningAidconceptbook.initCapName = learningAidconceptbook.name.substring(0, 1).toUpperCase() + learningAidconceptbook.name.substring(1);
			learningAidconceptbook.typeID = 31;

			learningAids.push(learningAidconceptbook);


			function ViewSample_conceptbook()
			{
				popupWindow(&quot;conceptbook&quot;, &quot;/support/learningaids/conceptbook.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtextbookextras = new Object();
			learningAidtextbookextras.name = 'textbookextras';
			learningAidtextbookextras.displayText = &quot;SBTextbook Extra&quot;;
			learningAidtextbookextras.imageUrl = 'images/icons/coursemanager_icon_textbookextras.png';
			learningAidtextbookextras.enabled = true;
			learningAidtextbookextras.hasCustomLogic = false;
			learningAidtextbookextras.visible = true;
			learningAidtextbookextras.isExerciseSpecific = true;
			learningAidtextbookextras.exerciseExists = false;
			learningAidtextbookextras.playerParamName = 'showtextbookextras';
			//learningAidtextbookextras.initCapName = learningAidtextbookextras.name.substring(0, 1).toUpperCase() + learningAidtextbookextras.name.substring(1);
			learningAidtextbookextras.typeID = 28;

			learningAids.push(learningAidtextbookextras);


			function ViewSample_textbookextras()
			{
				popupWindow(&quot;textbookextras&quot;, &quot;/support/learningaids/textbookextras.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidmathtools = new Object();
			learningAidmathtools.name = 'mathtools';
			learningAidmathtools.displayText = &quot;Math Tools&quot;;
			learningAidmathtools.imageUrl = 'images/icons/coursemanager_icon_mathtools.png';
			learningAidmathtools.enabled = true;
			learningAidmathtools.hasCustomLogic = false;
			learningAidmathtools.visible = true;
			learningAidmathtools.isExerciseSpecific = true;
			learningAidmathtools.exerciseExists = false;
			learningAidmathtools.playerParamName = 'showmathtools';
			//learningAidmathtools.initCapName = learningAidmathtools.name.substring(0, 1).toUpperCase() + learningAidmathtools.name.substring(1);
			learningAidmathtools.typeID = 32;

			learningAids.push(learningAidmathtools);


			function ViewSample_mathtools()
			{
				popupWindow(&quot;mathtools&quot;, &quot;/support/learningaids/mathtools.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidglossary = new Object();
			learningAidglossary.name = 'glossary';
			learningAidglossary.displayText = &quot;SBGlossary&quot;;
			learningAidglossary.imageUrl = 'images/icons/coursemanager_icon_glossary.gif';
			learningAidglossary.enabled = true;
			learningAidglossary.hasCustomLogic = false;
			learningAidglossary.visible = true;
			learningAidglossary.isExerciseSpecific = true;
			learningAidglossary.exerciseExists = false;
			learningAidglossary.playerParamName = 'showglossary';
			//learningAidglossary.initCapName = learningAidglossary.name.substring(0, 1).toUpperCase() + learningAidglossary.name.substring(1);
			learningAidglossary.typeID = 14;

			learningAids.push(learningAidglossary);


			function ViewSample_glossary()
			{
				popupWindow(&quot;glossary&quot;, &quot;/support/learningaids/glossary.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidreview = new Object();
			learningAidreview.name = 'review';
			learningAidreview.displayText = &quot;SBReview&quot;;
			learningAidreview.imageUrl = 'images/icons/coursemanager_icon_review.png';
			learningAidreview.enabled = true;
			learningAidreview.hasCustomLogic = false;
			learningAidreview.visible = true;
			learningAidreview.isExerciseSpecific = true;
			learningAidreview.exerciseExists = false;
			learningAidreview.playerParamName = 'showreviewinplayer';
			//learningAidreview.initCapName = learningAidreview.name.substring(0, 1).toUpperCase() + learningAidreview.name.substring(1);
			learningAidreview.typeID = 24;

			learningAids.push(learningAidreview);


			function ViewSample_review()
			{
				popupWindow(&quot;review&quot;, &quot;/support/learningaids/reviewinplayer.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidstatexplore = new Object();
			learningAidstatexplore.name = 'statexplore';
			learningAidstatexplore.displayText = &quot;StatCrunch&quot;;
			learningAidstatexplore.imageUrl = 'images/icons/coursemanager_icon_statcrunch.gif';
			learningAidstatexplore.enabled = true;
			learningAidstatexplore.hasCustomLogic = false;
			learningAidstatexplore.visible = true;
			learningAidstatexplore.isExerciseSpecific = true;
			learningAidstatexplore.exerciseExists = false;
			learningAidstatexplore.playerParamName = 'showstatexplore';
			//learningAidstatexplore.initCapName = learningAidstatexplore.name.substring(0, 1).toUpperCase() + learningAidstatexplore.name.substring(1);
			learningAidstatexplore.typeID = 3;

			learningAids.push(learningAidstatexplore);


			function ViewSample_statexplore()
			{
				popupWindow(&quot;statexplore&quot;, &quot;http://media.pearsoncmg.com/aw/aw_mml_shared_1/statexplore.gif&quot;, {Width:820,Height:560,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidgeogebra = new Object();
			learningAidgeogebra.name = 'geogebra';
			learningAidgeogebra.displayText = &quot;Geogebra\n&quot;;
			learningAidgeogebra.imageUrl = 'images/icons/coursemanager_icon_geogebra.png';
			learningAidgeogebra.enabled = true;
			learningAidgeogebra.hasCustomLogic = false;
			learningAidgeogebra.visible = true;
			learningAidgeogebra.isExerciseSpecific = true;
			learningAidgeogebra.exerciseExists = false;
			learningAidgeogebra.playerParamName = 'showgeogebra';
			//learningAidgeogebra.initCapName = learningAidgeogebra.name.substring(0, 1).toUpperCase() + learningAidgeogebra.name.substring(1);
			learningAidgeogebra.typeID = 26;

			learningAids.push(learningAidgeogebra);


			function ViewSample_geogebra()
			{
				popupWindow(&quot;geogebra&quot;, &quot;/support/learningaids/geogebra.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtechhelp = new Object();
			learningAidtechhelp.name = 'techhelp';
			learningAidtechhelp.displayText = &quot;Tech Help&quot;;
			learningAidtechhelp.imageUrl = 'images/icons/coursemanager_icon_techhelp.png';
			learningAidtechhelp.enabled = true;
			learningAidtechhelp.hasCustomLogic = false;
			learningAidtechhelp.visible = true;
			learningAidtechhelp.isExerciseSpecific = true;
			learningAidtechhelp.exerciseExists = false;
			learningAidtechhelp.playerParamName = 'showtechhelp';
			//learningAidtechhelp.initCapName = learningAidtechhelp.name.substring(0, 1).toUpperCase() + learningAidtechhelp.name.substring(1);
			learningAidtechhelp.typeID = 23;

			learningAids.push(learningAidtechhelp);


			function ViewSample_techhelp()
			{
				popupWindow(&quot;techhelp&quot;, &quot;/support/learningaids/techhelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidextrahelp = new Object();
			learningAidextrahelp.name = 'extrahelp';
			learningAidextrahelp.displayText = &quot;SBExtra Help&quot;;
			learningAidextrahelp.imageUrl = 'images/icons/coursemanager_icon_extrahelp.png';
			learningAidextrahelp.enabled = true;
			learningAidextrahelp.hasCustomLogic = false;
			learningAidextrahelp.visible = true;
			learningAidextrahelp.isExerciseSpecific = true;
			learningAidextrahelp.exerciseExists = false;
			learningAidextrahelp.playerParamName = 'showextrahelp';
			//learningAidextrahelp.initCapName = learningAidextrahelp.name.substring(0, 1).toUpperCase() + learningAidextrahelp.name.substring(1);
			learningAidextrahelp.typeID = 25;

			learningAids.push(learningAidextrahelp);


			function ViewSample_extrahelp()
			{
				popupWindow(&quot;extrahelp&quot;, &quot;/support/learningaids/extrahelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidcalculator = new Object();
			learningAidcalculator.name = 'calculator';
			learningAidcalculator.displayText = &quot;Calculator&quot;;
			learningAidcalculator.imageUrl = 'images/icons/coursemanager_icon_calc.gif';
			learningAidcalculator.enabled = false;
			learningAidcalculator.hasCustomLogic = false;
			learningAidcalculator.visible = false;
			learningAidcalculator.isExerciseSpecific = false;
			learningAidcalculator.exerciseExists = false;
			learningAidcalculator.playerParamName = 'showcalculator';
			//learningAidcalculator.initCapName = learningAidcalculator.name.substring(0, 1).toUpperCase() + learningAidcalculator.name.substring(1);
			learningAidcalculator.typeID = 8;

			learningAids.push(learningAidcalculator);


			function ViewSample_calculator()
			{
				popupWindow(&quot;calculator&quot;, &quot;../support/learningaids/calculator_sample.html&quot;, {Width:640,Height:480,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidaskinstructor = new Object();
			learningAidaskinstructor.name = 'askinstructor';
			learningAidaskinstructor.displayText = &quot;Ask My Instructor&quot;;
			learningAidaskinstructor.imageUrl = 'images/icons/coursemanager_icon_askmyinstrctr.gif';
			learningAidaskinstructor.enabled = true;
			learningAidaskinstructor.hasCustomLogic = true;
			learningAidaskinstructor.visible = true;
			learningAidaskinstructor.isExerciseSpecific = false;
			learningAidaskinstructor.exerciseExists = false;
			learningAidaskinstructor.playerParamName = 'showaskinstructor';
			//learningAidaskinstructor.initCapName = learningAidaskinstructor.name.substring(0, 1).toUpperCase() + learningAidaskinstructor.name.substring(1);
			learningAidaskinstructor.typeID = 6;

			learningAids.push(learningAidaskinstructor);


			function ViewSample_askinstructor()
			{
				popupWindow(&quot;askinstructor&quot;, &quot;sample_askmyinstructor.aspx&quot;, {Width:530,Height:430,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidconnecttotutor = new Object();
			learningAidconnecttotutor.name = 'connecttotutor';
			learningAidconnecttotutor.displayText = &quot;Connect to a Tutor&quot;;
			learningAidconnecttotutor.imageUrl = 'images/icons/coursemanager_icon_tutorvista.gif';
			learningAidconnecttotutor.enabled = false;
			learningAidconnecttotutor.hasCustomLogic = true;
			learningAidconnecttotutor.visible = false;
			learningAidconnecttotutor.isExerciseSpecific = false;
			learningAidconnecttotutor.exerciseExists = false;
			learningAidconnecttotutor.playerParamName = 'showconnecttotutor';
			//learningAidconnecttotutor.initCapName = learningAidconnecttotutor.name.substring(0, 1).toUpperCase() + learningAidconnecttotutor.name.substring(1);
			learningAidconnecttotutor.typeID = 33;

			learningAids.push(learningAidconnecttotutor);


			function ViewSample_connecttotutor()
			{
				popupWindow(&quot;connecttotutor&quot;, &quot;/support/learningaids/tutor_sample.html&quot;, {Width:800,Height:690,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidfasttrack = new Object();
			learningAidfasttrack.name = 'fasttrack';
			learningAidfasttrack.displayText = &quot;Fast Track&quot;;
			learningAidfasttrack.imageUrl = 'images/icons/coursemanager_icon_fasttrack.gif';
			learningAidfasttrack.enabled = false;
			learningAidfasttrack.hasCustomLogic = false;
			learningAidfasttrack.visible = false;
			learningAidfasttrack.isExerciseSpecific = true;
			learningAidfasttrack.exerciseExists = false;
			learningAidfasttrack.playerParamName = 'showfasttrack';
			//learningAidfasttrack.initCapName = learningAidfasttrack.name.substring(0, 1).toUpperCase() + learningAidfasttrack.name.substring(1);
			learningAidfasttrack.typeID = 4;

			learningAids.push(learningAidfasttrack);


			function ViewSample_fasttrack()
			{
				popupWindow(&quot;fasttrack&quot;, &quot;http://media.pearsoncmg.com/aw/aw_myeconlab/fasttrack/samplefasttrack.gif&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAiddemodoc = new Object();
			learningAiddemodoc.name = 'demodoc';
			learningAiddemodoc.displayText = &quot;DemoDocs Example&quot;;
			learningAiddemodoc.imageUrl = 'images/icons/coursemanager_icon_demodocs.gif';
			learningAiddemodoc.enabled = false;
			learningAiddemodoc.hasCustomLogic = false;
			learningAiddemodoc.visible = false;
			learningAiddemodoc.isExerciseSpecific = true;
			learningAiddemodoc.exerciseExists = false;
			learningAiddemodoc.playerParamName = 'showdemodoc';
			//learningAiddemodoc.initCapName = learningAiddemodoc.name.substring(0, 1).toUpperCase() + learningAiddemodoc.name.substring(1);
			learningAiddemodoc.typeID = 7;

			learningAids.push(learningAiddemodoc);


			function ViewSample_demodoc()
			{
				popupWindow(&quot;demodoc&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_mal_shared/sgdd1/index.html&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidkeyconcepts = new Object();
			learningAidkeyconcepts.name = 'keyconcepts';
			learningAidkeyconcepts.displayText = &quot;Key Concepts&quot;;
			learningAidkeyconcepts.imageUrl = 'images/icons/coursemanager_icon_demodocs.gif';
			learningAidkeyconcepts.enabled = false;
			learningAidkeyconcepts.hasCustomLogic = false;
			learningAidkeyconcepts.visible = false;
			learningAidkeyconcepts.isExerciseSpecific = true;
			learningAidkeyconcepts.exerciseExists = false;
			learningAidkeyconcepts.playerParamName = 'showkeyconcepts';
			//learningAidkeyconcepts.initCapName = learningAidkeyconcepts.name.substring(0, 1).toUpperCase() + learningAidkeyconcepts.name.substring(1);
			learningAidkeyconcepts.typeID = 9;

			learningAids.push(learningAidkeyconcepts);


			function ViewSample_keyconcepts()
			{
				popupWindow(&quot;keyconcepts&quot;, &quot;http://media.pearsoncmg.com/intl/ema/uk/accounting_mal/sample_anim/mal_animation_sample1.htm?centerwin=yes&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidgrapher = new Object();
			learningAidgrapher.name = 'grapher';
			learningAidgrapher.displayText = &quot;Grapher&quot;;
			learningAidgrapher.imageUrl = 'images/icons/coursemanager_icon_grapher.gif';
			learningAidgrapher.enabled = false;
			learningAidgrapher.hasCustomLogic = false;
			learningAidgrapher.visible = false;
			learningAidgrapher.isExerciseSpecific = true;
			learningAidgrapher.exerciseExists = false;
			learningAidgrapher.playerParamName = 'showgrapher';
			//learningAidgrapher.initCapName = learningAidgrapher.name.substring(0, 1).toUpperCase() + learningAidgrapher.name.substring(1);
			learningAidgrapher.typeID = 12;

			learningAids.push(learningAidgrapher);


			function ViewSample_grapher()
			{
				popupWindow(&quot;grapher&quot;, &quot;/support/learningaids/grapher.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidspreadsheet = new Object();
			learningAidspreadsheet.name = 'spreadsheet';
			learningAidspreadsheet.displayText = &quot;Spreadsheet&quot;;
			learningAidspreadsheet.imageUrl = 'images/icons/coursemanager_icon_spreadsheet.gif';
			learningAidspreadsheet.enabled = false;
			learningAidspreadsheet.hasCustomLogic = false;
			learningAidspreadsheet.visible = false;
			learningAidspreadsheet.isExerciseSpecific = true;
			learningAidspreadsheet.exerciseExists = false;
			learningAidspreadsheet.playerParamName = 'showspreadsheet';
			//learningAidspreadsheet.initCapName = learningAidspreadsheet.name.substring(0, 1).toUpperCase() + learningAidspreadsheet.name.substring(1);
			learningAidspreadsheet.typeID = 13;

			learningAids.push(learningAidspreadsheet);


			function ViewSample_spreadsheet()
			{
				popupWindow(&quot;spreadsheet&quot;, &quot;/support/learningaids/spreadsheet.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidannuitycalc = new Object();
			learningAidannuitycalc.name = 'annuitycalc';
			learningAidannuitycalc.displayText = &quot;Annuity Calculator&quot;;
			learningAidannuitycalc.imageUrl = 'images/icons/coursemanager_icon_annuitycalc.gif';
			learningAidannuitycalc.enabled = false;
			learningAidannuitycalc.hasCustomLogic = false;
			learningAidannuitycalc.visible = false;
			learningAidannuitycalc.isExerciseSpecific = true;
			learningAidannuitycalc.exerciseExists = false;
			learningAidannuitycalc.playerParamName = 'showannuitycalc';
			//learningAidannuitycalc.initCapName = learningAidannuitycalc.name.substring(0, 1).toUpperCase() + learningAidannuitycalc.name.substring(1);
			learningAidannuitycalc.typeID = 15;

			learningAids.push(learningAidannuitycalc);


			function ViewSample_annuitycalc()
			{
				popupWindow(&quot;annuitycalc&quot;, &quot;/support/learningaids/annuitycalc.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidschaums = new Object();
			learningAidschaums.name = 'schaums';
			learningAidschaums.displayText = &quot;Step-by-Step Example&quot;;
			learningAidschaums.imageUrl = 'images/icons/coursemanager_icon_schaum.gif';
			learningAidschaums.enabled = false;
			learningAidschaums.hasCustomLogic = false;
			learningAidschaums.visible = false;
			learningAidschaums.isExerciseSpecific = true;
			learningAidschaums.exerciseExists = false;
			learningAidschaums.playerParamName = 'showschaums';
			//learningAidschaums.initCapName = learningAidschaums.name.substring(0, 1).toUpperCase() + learningAidschaums.name.substring(1);
			learningAidschaums.typeID = 16;

			learningAids.push(learningAidschaums);


			function ViewSample_schaums()
			{
				popupWindow(&quot;schaums&quot;, &quot;/support/learningaids/schaums.htm&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidinstructortip = new Object();
			learningAidinstructortip.name = 'instructortip';
			learningAidinstructortip.displayText = &quot;Instructor Tip&quot;;
			learningAidinstructortip.imageUrl = 'images/icons/learnaid_view_tip.png';
			learningAidinstructortip.enabled = true;
			learningAidinstructortip.hasCustomLogic = false;
			learningAidinstructortip.visible = true;
			learningAidinstructortip.isExerciseSpecific = false;
			learningAidinstructortip.exerciseExists = false;
			learningAidinstructortip.playerParamName = 'showinstructortip';
			//learningAidinstructortip.initCapName = learningAidinstructortip.name.substring(0, 1).toUpperCase() + learningAidinstructortip.name.substring(1);
			learningAidinstructortip.typeID = 17;

			learningAids.push(learningAidinstructortip);


			function ViewSample_instructortip()
			{
				popupWindow(&quot;instructortip&quot;, &quot;/support/learningaids/instructortip.htm&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidboosterc = new Object();
			learningAidboosterc.name = 'boosterc';
			learningAidboosterc.displayText = &quot;Practice Book&quot;;
			learningAidboosterc.imageUrl = 'images/icons/learnaid_boosterc.png';
			learningAidboosterc.enabled = false;
			learningAidboosterc.hasCustomLogic = false;
			learningAidboosterc.visible = false;
			learningAidboosterc.isExerciseSpecific = true;
			learningAidboosterc.exerciseExists = false;
			learningAidboosterc.playerParamName = 'showboosterc';
			//learningAidboosterc.initCapName = learningAidboosterc.name.substring(0, 1).toUpperCase() + learningAidboosterc.name.substring(1);
			learningAidboosterc.typeID = 18;

			learningAids.push(learningAidboosterc);


			function ViewSample_boosterc()
			{
				popupWindow(&quot;boosterc&quot;, &quot;/support/learningaids/boosterc.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidpowerpoint = new Object();
			learningAidpowerpoint.name = 'powerpoint';
			learningAidpowerpoint.displayText = &quot;SBPowerPoint Slides1&quot;;
			learningAidpowerpoint.imageUrl = 'images/icons/learnaid_powerpoint.png';
			learningAidpowerpoint.enabled = false;
			learningAidpowerpoint.hasCustomLogic = false;
			learningAidpowerpoint.visible = false;
			learningAidpowerpoint.isExerciseSpecific = true;
			learningAidpowerpoint.exerciseExists = false;
			learningAidpowerpoint.playerParamName = 'showpowerpoint';
			//learningAidpowerpoint.initCapName = learningAidpowerpoint.name.substring(0, 1).toUpperCase() + learningAidpowerpoint.name.substring(1);
			learningAidpowerpoint.typeID = 19;

			learningAids.push(learningAidpowerpoint);


			function ViewSample_powerpoint()
			{
				popupWindow(&quot;powerpoint&quot;, &quot;/support/learningaids/powerpoint.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidfinancialcalc = new Object();
			learningAidfinancialcalc.name = 'financialcalc';
			learningAidfinancialcalc.displayText = &quot;Financial Calculator&quot;;
			learningAidfinancialcalc.imageUrl = 'images/icons/coursemanager_icon_financialcalc.gif';
			learningAidfinancialcalc.enabled = false;
			learningAidfinancialcalc.hasCustomLogic = false;
			learningAidfinancialcalc.visible = false;
			learningAidfinancialcalc.isExerciseSpecific = true;
			learningAidfinancialcalc.exerciseExists = false;
			learningAidfinancialcalc.playerParamName = 'showfinancialcalc';
			//learningAidfinancialcalc.initCapName = learningAidfinancialcalc.name.substring(0, 1).toUpperCase() + learningAidfinancialcalc.name.substring(1);
			learningAidfinancialcalc.typeID = 20;

			learningAids.push(learningAidfinancialcalc);


			function ViewSample_financialcalc()
			{
				popupWindow(&quot;financialcalc&quot;, &quot;http://media.pearsoncmg.com/aw/aw_myfinancelab/learningaids/financialcalc.jpg&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidomexplorertutor = new Object();
			learningAidomexplorertutor.name = 'omexplorertutor';
			learningAidomexplorertutor.displayText = &quot;OM Explorer Tutor&quot;;
			learningAidomexplorertutor.imageUrl = 'images/icons/learnaid_omexplorertutor.png';
			learningAidomexplorertutor.enabled = false;
			learningAidomexplorertutor.hasCustomLogic = false;
			learningAidomexplorertutor.visible = false;
			learningAidomexplorertutor.isExerciseSpecific = true;
			learningAidomexplorertutor.exerciseExists = false;
			learningAidomexplorertutor.playerParamName = 'showomexplorertutor';
			//learningAidomexplorertutor.initCapName = learningAidomexplorertutor.name.substring(0, 1).toUpperCase() + learningAidomexplorertutor.name.substring(1);
			learningAidomexplorertutor.typeID = 21;

			learningAids.push(learningAidomexplorertutor);


			function ViewSample_omexplorertutor()
			{
				popupWindow(&quot;omexplorertutor&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_moml_shared/la_samples/omexptutor.jpg&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAideconspreadsheet = new Object();
			learningAideconspreadsheet.name = 'econspreadsheet';
			learningAideconspreadsheet.displayText = &quot;Excel Spreadsheet&quot;;
			learningAideconspreadsheet.imageUrl = 'images/icons/coursemanager_icon_spreadsheet.gif';
			learningAideconspreadsheet.enabled = false;
			learningAideconspreadsheet.hasCustomLogic = false;
			learningAideconspreadsheet.visible = false;
			learningAideconspreadsheet.isExerciseSpecific = true;
			learningAideconspreadsheet.exerciseExists = false;
			learningAideconspreadsheet.playerParamName = 'showeconexcelspreadsheet';
			//learningAideconspreadsheet.initCapName = learningAideconspreadsheet.name.substring(0, 1).toUpperCase() + learningAideconspreadsheet.name.substring(1);
			learningAideconspreadsheet.typeID = 22;

			learningAids.push(learningAideconspreadsheet);


			function ViewSample_econspreadsheet()
			{
				popupWindow(&quot;econspreadsheet&quot;, &quot;/support/learningaids/econspreadsheet.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidcalculateexcel = new Object();
			learningAidcalculateexcel.name = 'calculateexcel';
			learningAidcalculateexcel.displayText = &quot;Calculate in Excel\n&quot;;
			learningAidcalculateexcel.imageUrl = 'images/icons/coursemanager_icon_spreadsheet.gif';
			learningAidcalculateexcel.enabled = false;
			learningAidcalculateexcel.hasCustomLogic = false;
			learningAidcalculateexcel.visible = false;
			learningAidcalculateexcel.isExerciseSpecific = true;
			learningAidcalculateexcel.exerciseExists = false;
			learningAidcalculateexcel.playerParamName = 'showcalculateexcel';
			//learningAidcalculateexcel.initCapName = learningAidcalculateexcel.name.substring(0, 1).toUpperCase() + learningAidcalculateexcel.name.substring(1);
			learningAidcalculateexcel.typeID = 27;

			learningAids.push(learningAidcalculateexcel);


			function ViewSample_calculateexcel()
			{
				popupWindow(&quot;calculateexcel&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_mal_shared/spreadsheet/scratchsheet.xls&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidmorehelp = new Object();
			learningAidmorehelp.name = 'morehelp';
			learningAidmorehelp.displayText = &quot;SBMore Help&quot;;
			learningAidmorehelp.imageUrl = 'images/icons/coursemanager_icon_morehelp.png';
			learningAidmorehelp.enabled = false;
			learningAidmorehelp.hasCustomLogic = false;
			learningAidmorehelp.visible = false;
			learningAidmorehelp.isExerciseSpecific = false;
			learningAidmorehelp.exerciseExists = false;
			learningAidmorehelp.playerParamName = 'showmorehelp';
			//learningAidmorehelp.initCapName = learningAidmorehelp.name.substring(0, 1).toUpperCase() + learningAidmorehelp.name.substring(1);
			learningAidmorehelp.typeID = 29;

			learningAids.push(learningAidmorehelp);


			function ViewSample_morehelp()
			{
				popupWindow(&quot;morehelp&quot;, &quot;/support/learningaids/morehelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtemplates = new Object();
			learningAidtemplates.name = 'templates';
			learningAidtemplates.displayText = &quot;Templates&quot;;
			learningAidtemplates.imageUrl = 'images/icons/coursemanager_icon_templates.png';
			learningAidtemplates.enabled = false;
			learningAidtemplates.hasCustomLogic = false;
			learningAidtemplates.visible = false;
			learningAidtemplates.isExerciseSpecific = true;
			learningAidtemplates.exerciseExists = false;
			learningAidtemplates.playerParamName = 'showtemplates';
			//learningAidtemplates.initCapName = learningAidtemplates.name.substring(0, 1).toUpperCase() + learningAidtemplates.name.substring(1);
			learningAidtemplates.typeID = 30;

			learningAids.push(learningAidtemplates);


			function ViewSample_templates()
			{
				popupWindow(&quot;templates&quot;, &quot;/support/learningaids/template.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidbillofrights = new Object();
			learningAidbillofrights.name = 'billofrights';
			learningAidbillofrights.displayText = &quot;Bill of Rights&quot;;
			learningAidbillofrights.imageUrl = 'images/icons/coursemanager_icon_billofrights.png';
			learningAidbillofrights.enabled = false;
			learningAidbillofrights.hasCustomLogic = false;
			learningAidbillofrights.visible = false;
			learningAidbillofrights.isExerciseSpecific = true;
			learningAidbillofrights.exerciseExists = false;
			learningAidbillofrights.playerParamName = 'showbillofrights';
			//learningAidbillofrights.initCapName = learningAidbillofrights.name.substring(0, 1).toUpperCase() + learningAidbillofrights.name.substring(1);
			learningAidbillofrights.typeID = 34;

			learningAids.push(learningAidbillofrights);


			function ViewSample_billofrights()
			{
				popupWindow(&quot;billofrights&quot;, &quot;http://media.pearsoncmg.com/ph/chet/chet_criminal_justice_1/resources/billofrights.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidusconstitution = new Object();
			learningAidusconstitution.name = 'usconstitution';
			learningAidusconstitution.displayText = &quot;U.S. Constitution&quot;;
			learningAidusconstitution.imageUrl = 'images/icons/coursemanager_icon_usconstitution.png';
			learningAidusconstitution.enabled = false;
			learningAidusconstitution.hasCustomLogic = false;
			learningAidusconstitution.visible = false;
			learningAidusconstitution.isExerciseSpecific = true;
			learningAidusconstitution.exerciseExists = false;
			learningAidusconstitution.playerParamName = 'showusconstitution';
			//learningAidusconstitution.initCapName = learningAidusconstitution.name.substring(0, 1).toUpperCase() + learningAidusconstitution.name.substring(1);
			learningAidusconstitution.typeID = 35;

			learningAids.push(learningAidusconstitution);


			function ViewSample_usconstitution()
			{
				popupWindow(&quot;usconstitution&quot;, &quot;http://media.pearsoncmg.com/ph/chet/chet_criminal_justice_1/resources/constitution.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidimage = new Object();
			learningAidimage.name = 'image';
			learningAidimage.displayText = &quot;Image&quot;;
			learningAidimage.imageUrl = 'images/icons/coursemanager_icon_textbookextras.png';
			learningAidimage.enabled = false;
			learningAidimage.hasCustomLogic = false;
			learningAidimage.visible = false;
			learningAidimage.isExerciseSpecific = true;
			learningAidimage.exerciseExists = false;
			learningAidimage.playerParamName = 'showimage';
			//learningAidimage.initCapName = learningAidimage.name.substring(0, 1).toUpperCase() + learningAidimage.name.substring(1);
			learningAidimage.typeID = 36;

			learningAids.push(learningAidimage);


			function ViewSample_image()
			{
				popupWindow(&quot;image&quot;, &quot;/support/learningaids/textbookextras.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidguidedlecture = new Object();
			learningAidguidedlecture.name = 'guidedlecture';
			learningAidguidedlecture.displayText = &quot;Guided Lecture&quot;;
			learningAidguidedlecture.imageUrl = 'images/icons/learnaid_powerpoint.png';
			learningAidguidedlecture.enabled = false;
			learningAidguidedlecture.hasCustomLogic = false;
			learningAidguidedlecture.visible = false;
			learningAidguidedlecture.isExerciseSpecific = true;
			learningAidguidedlecture.exerciseExists = false;
			learningAidguidedlecture.playerParamName = 'showguidedlecture';
			//learningAidguidedlecture.initCapName = learningAidguidedlecture.name.substring(0, 1).toUpperCase() + learningAidguidedlecture.name.substring(1);
			learningAidguidedlecture.typeID = 37;

			learningAids.push(learningAidguidedlecture);


			function ViewSample_guidedlecture()
			{
				popupWindow(&quot;guidedlecture&quot;, &quot;/support/learningaids/powerpoint.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidflashcards = new Object();
			learningAidflashcards.name = 'flashcards';
			learningAidflashcards.displayText = &quot;Flashcards&quot;;
			learningAidflashcards.imageUrl = 'images/icons/coursemanager_icon_review.png';
			learningAidflashcards.enabled = false;
			learningAidflashcards.hasCustomLogic = false;
			learningAidflashcards.visible = false;
			learningAidflashcards.isExerciseSpecific = true;
			learningAidflashcards.exerciseExists = false;
			learningAidflashcards.playerParamName = 'showflashcards';
			//learningAidflashcards.initCapName = learningAidflashcards.name.substring(0, 1).toUpperCase() + learningAidflashcards.name.substring(1);
			learningAidflashcards.typeID = 38;

			learningAids.push(learningAidflashcards);


			function ViewSample_flashcards()
			{
				popupWindow(&quot;flashcards&quot;, &quot;/support/learningaids/reviewinplayer.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidlabelingactivity = new Object();
			learningAidlabelingactivity.name = 'labelingactivity';
			learningAidlabelingactivity.displayText = &quot;Labeling Activity&quot;;
			learningAidlabelingactivity.imageUrl = 'images/icons/coursemanager_icon_morehelp.png';
			learningAidlabelingactivity.enabled = false;
			learningAidlabelingactivity.hasCustomLogic = false;
			learningAidlabelingactivity.visible = false;
			learningAidlabelingactivity.isExerciseSpecific = true;
			learningAidlabelingactivity.exerciseExists = false;
			learningAidlabelingactivity.playerParamName = 'showlabelingactivity';
			//learningAidlabelingactivity.initCapName = learningAidlabelingactivity.name.substring(0, 1).toUpperCase() + learningAidlabelingactivity.name.substring(1);
			learningAidlabelingactivity.typeID = 39;

			learningAids.push(learningAidlabelingactivity);


			function ViewSample_labelingactivity()
			{
				popupWindow(&quot;labelingactivity&quot;, &quot;/support/learningaids/morehelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	




	var learningAidTypeMap = new Object();
	for (var i = 0; i &lt; learningAids.length; i++) {
		learningAidTypeMap[learningAids[i].typeID] = learningAids[i];
	}

	function GetLearningAidsArray() {
		return learningAids;
	}

	function GetLearningAidByTypeId(typeId) {
		return learningAidTypeMap[typeId];
	}

	function GetLearningAidRowId(typeId) {
		return rowId[typeId]; //
			}

			function GetLearningAidCheckBoxId(typeId) {
				return cbId[typeId]; //
			}

			function IsLearningAidRowHidden(typeId) {
				var rowId = GetLearningAidRowId(typeId);
				var row = document.getElementById(rowId);
				return row.style.display == &quot;none&quot;;
			}

			function GetLearningAidCheckBox(typeId) {
				if (typeof(cbId[typeId]) == 'undefined' &amp;&amp; cbId[typeId] != null)
					return null;

				return document.getElementById(cbId[typeId]);
			}


			function UpdateLearningAids() {

				var LearningAidsListLabel = document.getElementById('ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel');

		// update text list of learning aids in Change Settings page view


		var foundExerciseSpecificAid = false;
		var foundVisibleAid = false;

		for (var j = 0; j &lt; learningAids.length; j++)
			if (learningAids[j].visible &amp;&amp; !learningAids[j].isExerciseSpecific)
				foundVisibleAid = true;

		var ulSel = document.getElementById(&quot;ulSelected&quot;);
		if (ulSel)
		{
			/// Reset existing exercise flags
			for (var j = 0; j &lt; learningAids.length; j++)
				learningAids[j].exerciseExists = false;


			for (var i = 0; i &lt; ulSel.childNodes.length; i++)
			{
				var selectedExercise = getSelectedExercise(ulSel.childNodes[i].id);
				if (selectedExercise.questionType != QuestionTypes.Question)
					continue; // skip questions from other books, custom questions, and media questions

				// CA The following loop checks for visible and aids tied to an exercise, setting flags
				// as appropriate. The flags are later verified to construct the list of aids on the
				// Settings page.

				for (var j = 0; j &lt; learningAids.length; j++)
					if (learningAids[j].visible)
					{
						// CA for exercise-specific learning aids, check to see if the selected exercise has the aid
						if (learningAids[j].isExerciseSpecific &amp;&amp; !learningAids[j].exerciseExists)
						{
							var hasAid = eval('selectedExercise.hasLearningAidType(' + learningAids[j].typeID + ')');

							if (hasAid)
							{
								learningAids[j].exerciseExists = true;
								foundExerciseSpecificAid = true;
							}

						}

						foundVisibleAid = true;
					}
			}
		}

		/// CA If no aids are found, display the None heading and error out
		if (!foundVisibleAid &amp;&amp; !foundExerciseSpecificAid)
		{
			handleNoLearningAids();
			return false;
		}


		// CA For the display string on the settings page, in order for an aid to appear
		// it must be visible according to course settings, enabled, and either:
		// - not exercise specific
		// - exercise specific and there exists an exercise associated with the learning aid
		var learningAidsDisplayString = '';


		for(var i = 0; i &lt; learningAids.length; i++)
			if (learningAids[i].visible &amp;&amp; learningAids[i].enabled
				&amp;&amp; (!learningAids[i].isExerciseSpecific || (learningAids[i].isExerciseSpecific &amp;&amp; learningAids[i].exerciseExists)))
			{
				if (learningAidsDisplayString == '')
					learningAidsDisplayString = learningAids[i].displayText;
				else
					learningAidsDisplayString += ', ' + learningAids[i].displayText;
			}

		if (learningAidsDisplayString == '')
			handleNoLearningAids();
		else
			handleExistingLearningAids(learningAidsDisplayString);

		var hasDisplayedRow = false;

		// CA show/hide corresponding table rows
		for(var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var tableRowClientID = eval('LATableRowID' + learningAids[i].name);
				var tableRow = document.getElementById(tableRowClientID);
				var checkBoxClientID = eval('LACheckBoxID' + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				// If the learning aid is enabled, ensure that the corresponding box is checked
				if (learningAids[i].enabled)
					checkBox.checked = true;
				else
					checkBox.checked = false;

				// The table row is displayed as per the conditions above, except that both enabled
				// and disabled exercises are displayed.
				if (!learningAids[i].isExerciseSpecific || (learningAids[i].isExerciseSpecific &amp;&amp; learningAids[i].exerciseExists))
				{
					tableRow.style.display = '';
					hasDisplayedRow = true;
				}
				else
					tableRow.style.display = 'none';
			}
        }

			    // CA if there are no displayed rows, we must hide the Change link
			    var learningAidsChangeSpan = document.getElementById('ctl00_ctl00_InsideForm_MasterContent_LearningAidsChangeSpan');
			    if (hasDisplayedRow)
			        learningAidsChangeSpan.style.display = '';
			    else
					learningAidsChangeSpan.style.display = 'none';

				UpdateReviewModeCheckboxText();
		
	}

    function  HandleFootNoteVisibility(){

     var statCrunchDataItem = learningAids.find(item=> item.name== 'statexplore')

         
    }




    function LearningAidsChangeLink_onclick() {
        HandleFootNoteVisibility();

        if (!isHW) {
            if (isUseLockdownBrowserChecked() &amp;&amp; isLearningAidReviewModeChecked()) {
				
				
                var enabledLearningAids = getEnabledLearningAids();
				var statCrunchEnabled = hasStatCrunchEnabled();
                var isNewGenStatCrunchFeaturesEnabled = &quot;false&quot;;
                if (statCrunchEnabled) {
                    isNewGenStatCrunchFeaturesEnabled = true;
                }
                if (isNewGenStatCrunchFeaturesEnabled) {
                    if (enabledLearningAids.length > 0 &amp;&amp; !statCrunchEnabled) {
                        alert(&quot;Learning Aids cannot be displayed on a test or quiz in LockDown Browser mode. They have been set to show in Review mode only.&quot;);
                    }
                    if (enabledLearningAids.length > 1 &amp;&amp; statCrunchEnabled) {
                        alert(&quot;StatCrunch is the only learning aid that can be displayed on a test or quiz in LockDown Browser mode. All other learning aids have been set to show in Review Mode only.&quot;);
                    }
                }
                else {
                    alert(&quot;Learning Aids cannot be displayed on a test or quiz in LockDown Browser mode. They have been set to show in Review mode only.&quot;);
                }
            }
        }

		// 
		if (learningAidsEnabled()) {
			var ulSel = document.getElementById(&quot;ulSelected&quot;);
			if (ulSel) {
				var hasQuestionLevelSettings = false;
				for (var i = 0; i &lt; ulSel.childNodes.length; i++) {
					var pointsBox = GetPointsBox(ulSel.childNodes[i]);
					var editedCustomLearningAidTypeIdCsv = pointsBox.getAttribute(&quot;customLearningAidTypeIdCsv&quot;);
					if (!QuestionCustomLearningAidsState.usesAssignmentSettings(editedCustomLearningAidTypeIdCsv)) {
						hasQuestionLevelSettings = true;
						break;
					}
				}
				var divQuestionLearningAidsNote = document.getElementById(&quot;DivQuestionLearningAidsNote&quot;);
				divQuestionLearningAidsNote.style.display = hasQuestionLevelSettings ? &quot;&quot; : &quot;none&quot;;
			}
		}
	                
		var learningAidsPopup = document.getElementById('ChangeLearningAidsPopup');
		var learningAidsIEShim = document.getElementById('ChangeLearningAidsIEShim');       // needed for IE6, lest drop downs will have precedence over DIV
		if (learningAidsPopup)
		{
			learningAidsPopup.style.display = 'block';
			learningAidsIEShim.style.display = 'block';
		}
		else
			alert('ChangeLearningAidsPopup not found.');
	}







	function handleNoLearningAids()
	{
		var LearningAidsListLabel = document.getElementById('ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel');
		   changeText(LearningAidsListLabel, LOCNS.LocMap.GetRes(&quot;resources&quot;, &quot;None&quot;));
		   var showInReviewModeOnlyDiv = document.getElementById(&quot;ShowInReviewModeOnlyDiv&quot;);
		   showInReviewModeOnlyDiv.className = 'disabled';

		   var reviewModeCheckbox = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot;);
		if (reviewModeCheckbox)
			reviewModeCheckbox.disabled = true;
	}

	function handleExistingLearningAids(learningAidsDisplayString)
	{
		var LearningAidsListLabel = document.getElementById('ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel');
		changeText(LearningAidsListLabel, learningAidsDisplayString);
		
		var showInReviewModeOnlyDiv = document.getElementById(&quot;ShowInReviewModeOnlyDiv&quot;);
		showInReviewModeOnlyDiv.className = '';

		var reviewModeCheckbox = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot;);
		reviewModeCheckbox.disabled = false;
		
	}

	function changeText(labelControl, text)
	{
		var textNode = document.createTextNode(text);

		if (labelControl.childNodes.length == 0)
			labelControl.appendChild(textNode);
		else
			labelControl.replaceChild(textNode, labelControl.childNodes[0]);

	}

	function closeLearningAidsPopup()
	{
		var learningAidsPopup = document.getElementById('ChangeLearningAidsPopup');
		var learningAidsIEShim = document.getElementById('ChangeLearningAidsIEShim');       // needed for IE6, lest drop downs will have precedence over DIV
		if (learningAidsPopup)
		{
			learningAidsIEShim.style.display = 'none';
			learningAidsPopup.style.display = 'none';
		}
		else
			alert('ChangeLearningAidsPopup not found.');
	}

	function LearningAidsDialogOK_onclick()
	{
		closeLearningAidsPopup();

		// CA Ensure that the textual Learning Aids display on the Change Settings screen is updated
		for (var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var checkBoxClientID = eval('LACheckBoxID' + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				learningAids[i].enabled = checkBox.checked;
			}
		}
		UpdateLearningAids();
		UpdateReviewModeCheckboxText();
	}

	// XL-29125: Update ReviewModeCheckbox Text for learning aids
	function UpdateReviewModeCheckboxText() {
        var statCrunchEnabled = hasStatCrunchEnabled();
		var isLDBRequired = $('#ctl00_ctl00_InsideForm_MasterContent_chkUseLockdownBrowser').is(':checked');
		var isNewGenStatCrunchFeaturesEnabled = &quot;false&quot;;
		if (statCrunchEnabled) {
            isNewGenStatCrunchFeaturesEnabled = true;
        }

        if (statCrunchEnabled &amp;&amp; isLDBRequired &amp;&amp; isNewGenStatCrunchFeaturesEnabled) {
            SetReviewModeCheckboxLabelText(&quot;Show StatCrunch in quizzes and tests and all selected learning aids in Review mode&quot;);
		}
		else {
            SetReviewModeCheckboxLabelText(&quot;Show in Review mode only&quot;);
        }
	}

	function SetReviewModeCheckboxLabelText(labelText) {
		$('#ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox').next().text(labelText);
    }

	// Check if StatCrunch is enabled in Learning aids options
	function hasStatCrunchEnabled() {
        var isSecondaryFlow = false;
        if (isSecondaryFlow) {
			return true;
		}
		else {
			var enabledLearningAids = getActiveLearningAids();
			return hasStatCrunch(enabledLearningAids);
		}
	}

	// Obtain list of active learning aids
	function getActiveLearningAids() {
		return learningAids.filter(aid => (aid.enabled == true)
            &amp;&amp; (aid.visible) &amp;&amp; (!aid.isExerciseSpecific || (aid.isExerciseSpecific &amp;&amp; aid.exerciseExists)));
    }

	function LearningAidsDialogCancel_onclick()
	{
		closeLearningAidsPopup();

		// CA Restore previous choices prior to entering the dialog
		for(var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var checkBoxClientID = eval('LACheckBoxID' + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				checkBox.checked = learningAids[i].enabled;
			}
		}

		UpdateLearningAids();
	}
	function getEnabledLearningAids() {
		return learningAids.filter(aid => aid.enabled == true);
	}
	function hasStatCrunch(learningAidArray) {
		var found = learningAidArray.find(aid => aid.name == 'statexplore');
		return found ? true : false;
    }


rowId = new Object();cbId = new Object();LATableRowIDguidedsolution = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidDisplayRow';
rowId[10] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidDisplayRow';LACheckBoxIDguidedsolution = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidCheckBox';
cbId[10] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidCheckBox';LATableRowIDexample = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidDisplayRow';
rowId[11] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidDisplayRow';LACheckBoxIDexample = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidCheckBox';
cbId[11] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidCheckBox';LATableRowIDvideo = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidDisplayRow';
rowId[1] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidDisplayRow';LACheckBoxIDvideo = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidCheckBox';
cbId[1] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidCheckBox';LATableRowIDanimation = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidDisplayRow';
rowId[2] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidDisplayRow';LACheckBoxIDanimation = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidCheckBox';
cbId[2] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidCheckBox';LATableRowIDtextbook = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidDisplayRow';
rowId[5] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidDisplayRow';LACheckBoxIDtextbook = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidCheckBox';
cbId[5] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidCheckBox';LATableRowIDconceptbook = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidDisplayRow';
rowId[31] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidDisplayRow';LACheckBoxIDconceptbook = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidCheckBox';
cbId[31] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidCheckBox';LATableRowIDtextbookextras = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidDisplayRow';
rowId[28] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidDisplayRow';LACheckBoxIDtextbookextras = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidCheckBox';
cbId[28] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidCheckBox';LATableRowIDmathtools = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidDisplayRow';
rowId[32] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidDisplayRow';LACheckBoxIDmathtools = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidCheckBox';
cbId[32] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidCheckBox';LATableRowIDglossary = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidDisplayRow';
rowId[14] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidDisplayRow';LACheckBoxIDglossary = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidCheckBox';
cbId[14] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidCheckBox';LATableRowIDreview = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidDisplayRow';
rowId[24] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidDisplayRow';LACheckBoxIDreview = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidCheckBox';
cbId[24] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidCheckBox';LATableRowIDstatexplore = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidDisplayRow';
rowId[3] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidDisplayRow';LACheckBoxIDstatexplore = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidCheckBox';
cbId[3] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidCheckBox';LATableRowIDgeogebra = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidDisplayRow';
rowId[26] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidDisplayRow';LACheckBoxIDgeogebra = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidCheckBox';
cbId[26] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidCheckBox';LATableRowIDtechhelp = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidDisplayRow';
rowId[23] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidDisplayRow';LACheckBoxIDtechhelp = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidCheckBox';
cbId[23] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidCheckBox';LATableRowIDextrahelp = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidDisplayRow';
rowId[25] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidDisplayRow';LACheckBoxIDextrahelp = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidCheckBox';
cbId[25] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidCheckBox';LATableRowIDcalculator = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidDisplayRow';
rowId[8] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidDisplayRow';LACheckBoxIDcalculator = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidCheckBox';
cbId[8] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidCheckBox';LATableRowIDaskinstructor = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidDisplayRow';
rowId[6] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidDisplayRow';LACheckBoxIDaskinstructor = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidCheckBox';
cbId[6] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidCheckBox';LATableRowIDconnecttotutor = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidDisplayRow';
rowId[33] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidDisplayRow';LACheckBoxIDconnecttotutor = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidCheckBox';
cbId[33] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidCheckBox';LATableRowIDfasttrack = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidDisplayRow';
rowId[4] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidDisplayRow';LACheckBoxIDfasttrack = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidCheckBox';
cbId[4] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidCheckBox';LATableRowIDdemodoc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidDisplayRow';
rowId[7] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidDisplayRow';LACheckBoxIDdemodoc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidCheckBox';
cbId[7] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidCheckBox';LATableRowIDkeyconcepts = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidDisplayRow';
rowId[9] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidDisplayRow';LACheckBoxIDkeyconcepts = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidCheckBox';
cbId[9] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidCheckBox';LATableRowIDgrapher = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidDisplayRow';
rowId[12] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidDisplayRow';LACheckBoxIDgrapher = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidCheckBox';
cbId[12] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidCheckBox';LATableRowIDspreadsheet = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidDisplayRow';
rowId[13] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidDisplayRow';LACheckBoxIDspreadsheet = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidCheckBox';
cbId[13] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidCheckBox';LATableRowIDannuitycalc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidDisplayRow';
rowId[15] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidDisplayRow';LACheckBoxIDannuitycalc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidCheckBox';
cbId[15] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidCheckBox';LATableRowIDschaums = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidDisplayRow';
rowId[16] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidDisplayRow';LACheckBoxIDschaums = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidCheckBox';
cbId[16] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidCheckBox';LATableRowIDinstructortip = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidDisplayRow';
rowId[17] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidDisplayRow';LACheckBoxIDinstructortip = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidCheckBox';
cbId[17] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidCheckBox';LATableRowIDboosterc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidDisplayRow';
rowId[18] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidDisplayRow';LACheckBoxIDboosterc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidCheckBox';
cbId[18] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidCheckBox';LATableRowIDpowerpoint = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidDisplayRow';
rowId[19] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidDisplayRow';LACheckBoxIDpowerpoint = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidCheckBox';
cbId[19] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidCheckBox';LATableRowIDfinancialcalc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidDisplayRow';
rowId[20] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidDisplayRow';LACheckBoxIDfinancialcalc = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidCheckBox';
cbId[20] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidCheckBox';LATableRowIDomexplorertutor = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidDisplayRow';
rowId[21] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidDisplayRow';LACheckBoxIDomexplorertutor = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidCheckBox';
cbId[21] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidCheckBox';LATableRowIDeconspreadsheet = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidDisplayRow';
rowId[22] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidDisplayRow';LACheckBoxIDeconspreadsheet = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidCheckBox';
cbId[22] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidCheckBox';LATableRowIDcalculateexcel = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidDisplayRow';
rowId[27] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidDisplayRow';LACheckBoxIDcalculateexcel = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidCheckBox';
cbId[27] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidCheckBox';LATableRowIDmorehelp = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidDisplayRow';
rowId[29] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidDisplayRow';LACheckBoxIDmorehelp = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidCheckBox';
cbId[29] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidCheckBox';LATableRowIDtemplates = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidDisplayRow';
rowId[30] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidDisplayRow';LACheckBoxIDtemplates = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidCheckBox';
cbId[30] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidCheckBox';LATableRowIDbillofrights = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidDisplayRow';
rowId[34] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidDisplayRow';LACheckBoxIDbillofrights = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidCheckBox';
cbId[34] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidCheckBox';LATableRowIDusconstitution = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidDisplayRow';
rowId[35] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidDisplayRow';LACheckBoxIDusconstitution = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidCheckBox';
cbId[35] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidCheckBox';LATableRowIDimage = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidDisplayRow';
rowId[36] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidDisplayRow';LACheckBoxIDimage = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidCheckBox';
cbId[36] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidCheckBox';LATableRowIDguidedlecture = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidDisplayRow';
rowId[37] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidDisplayRow';LACheckBoxIDguidedlecture = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidCheckBox';
cbId[37] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidCheckBox';LATableRowIDflashcards = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidDisplayRow';
rowId[38] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidDisplayRow';LACheckBoxIDflashcards = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidCheckBox';
cbId[38] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidCheckBox';LATableRowIDlabelingactivity = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidDisplayRow';
rowId[39] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidDisplayRow';LACheckBoxIDlabelingactivity = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidCheckBox';
cbId[39] = 'ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidCheckBox';





	
	
		DevTestingCourse
	






    function getHeaderText(){
        return $('#PageHeaderTitle').get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        let pageTitle = $('#PageHeaderTitle').get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            New Homework  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about New Homework
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener('mousedown', function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener('keydown', function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById('ButtonHelp').setAttribute('aria-expanded', 'false');
            }
        });

        }
    
 
        
        
      
    
    




   
    
    
        
            
                
                    Start
                
                    Select Media and Questions
                
                    Choose Settings
                
                    Define Competency Mapping
            
        
        
    

    
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




	
		
			
				
					Book
					NextGen Media Math Test Book--REAL
				
				
				
	Gradebook Category 
	
						
		Homework

	Homework
					


				
	


				
				
				
					Homework Name
					
				

                

                

				

				

				

				
				

				
	
	Creating assignments for mobile use.    


			
		
	
	
	


	
		
		
			

		

		

				
			    
                
                    
                        Personalization  
                        
                            Options
                        
                        
                        
                        This assignment is personalized for each student using  Skill Builder.
                    
                    
                        
                            
                                
	
                                        
		
                                            Skill Builder
                                        
	
                                    
	
                                        Provide an opportunity for additional targeted practice with   Skill Builder Adaptive Practice.
                                    


                                
	
                                        Personalized Homework
                                    
	
                                        Give automatic credit for questions from sbobjectives mastered in test/quiz:
                                        
		Choose...
		Maintain Order, Preassigned

	
                                         
                                    


                                
                            
                        
                    
                
			    
		
		
		

		

		
		
	
			
				
					This assignment name is already in use in your course, or in one of the members of your course group.Please change it to make it unique.
						
				
			
		

		
		

		
	
		    
		    
		
			    
				    
					    
						    
			
							    
									    
								    
						    
		
					    
				    
			    
		    
	 
            
		

        
            
            The TestGen plug-in is not compatible with the Pearson LockDown Browser.
        
		
		
	
	 





	
	
		
			
				Test Name 
				SampleHW
			
			
				Book 
				NextGen Media Math Test Book--REAL
			
		
		
		
		
			
				
					
						
							Version 3.0 or higher of the TestGen plug-in is required to take this test.
						
						
							Version 3.0 or higher of the TestGen plug-in is required to take this test.
						
						This test will not generate student study plans or item analysis data because it contains one or more questions from a testbank created for another book.
To create tests that generate a study plan or item analysis, download the testbank for this book.
						
						Please click next to continue.
				
			
		
	
		
		
		
		
			Note: You must create or save your test using TestGen version 3.3 or higher.If you are creating tests with the latest version of the TestGen application, you must make sure your students and your lab have installed the latest version of the TestGen plug-in.
			
		
		  
			1. Click Browse to locate and select the TestGen file on your hard drive
			
		
          
               
			We recommend a maximum of 40 questions in your TestGen file.
			
		
		  
			
		
		  
			2. Click Next to upload your TestGen test
		
		
		
	
		
		
		
			
				
					Is your edited test ready to upload?
					
					I have updated my test and I want to upload it now.
					I need to download a copy of the test to make changes to it.
				
				

					
					
						
						Click Browse to locate and select the TestGen file on your hard drive
                        
                         
			            We recommend a maximum of 40 questions in your TestGen file.
			            
		                
						
						
						
						
						
					
					

					
						
						Download the file to your hard drive or preview it using the TestGen Plug-in.
						File:  (0KB)
						Download
						Preview
						
						To upload your edited test, return to this wizard and choose &quot;I have updated my test and I want to upload it now.
						
					
				
			
		
		
	

		
		
			
			1. If you wish to preview the test you are copying or download it to your hard drive, click below.
			File:  (0KB)
			Download
			Preview
			
			2. Click Next to change your test assignment settings or Save to save your copied test
			
		
	

	

	
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook','');
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode','');
    }


		
        
    
	

    
        
            
			
		
					
					    
						    Name
						    SampleHW
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
			SBChapter 
			
				1. The Real Number System
				2. Linear Equations and Inequalities in One Variable
				3. Linear Equations and Inequalities in Two Variables; Functions
				4. MarkD Test Chapter
				5. Chapter not in Study Plan
				6. Drag and Drop Questions
				7. Essay questions
				8. StatCrunch
				9. 6-15-21 Adaptive Diagnostic

			
		
		
			SBSection 
			
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions
		
		
			SBObjective 
			
				All SBO

			
		
		

                        
                        
			
                               
                            
		
		
			Availability
			
                                
				All questions
				Questions that are not in the Study Plan
				Questions that are in the Study Plan
				Questions that are screenreader accessible

			
                            
		
		

                        
                        
						
                    
                    																														
                
	
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                             Show GL questions
                                            
                                        
                                             Show Geogebra LTI questions
                                            
                                        
                                             Show Sylva questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   
		
					Questions
		Media
		
					
					
			
	
	

		
			
			
				
					
						
						
						
						Available Questions
						 
						(210)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 7My Selections (7)
							
							
							    
							
								View Assignment Details
								
							
							
							
					
					
						
							
							Points: 7

							
							
							#
							

							
								
									
									
								Question ID / Media

								
									
								
							
							
								
									SBSection / Book Association
									
								
								 
							Estimated time:189m 38s
							
							
					
				
			
	
		
		
			
			
				
					+ (1.1) CustomQuestion1TechH, Rev, Extra Help (preloaded)1.1.29 (preloaded)Anim (preloaded)(P) Anim(P) Anim, eText 3(P) 1.1.39(P) Video, eText 1(P) Video, eText 2(P) 1.1.45(P) 1.1.47(P) 1.1.51(P) 1.1.53(P) 1.1.55(P) 1.1.59(P) Video, Anim, eText 1(P) 1.1.79(P) 1.1.100 RS file testing(P) 1.1.101 RS file testing(P) 1.1.102 RS file testing(P) 1.1.103 RS file testing missing RS(P) 1.1.104 RS file testing - broken vars(P) Graph review only(P) Review only(P) RTD Review only(P) Data Table question(P) Text and Mobile(P) Text only(P) Mobile only(P) No text, no mobile(P) 1.2.17(P) 1.2.19(P) 1.2.23(P) 1.2.25(P) 1.2.27(P) 1.2.31(P) 1.2.33(P) 1.2.35(P) 1.2.37(P) 1.2.41(P) 1.2.43(P) 1.2.47(P) 1.2.51(P) 1.2.55(P) 1.2.57(P) 1.2.61(P) 1.2.63(P) 1.2.67(P) 1.2.71(P) 1.2.79(P) 1.2.81(P) 1.3.13(P) 1.3.15(P) 1.3.17(P) 1.3.19(P) 1.3.23(P) 1.3.25(P) 1.3.29(P) 1.3.31(P) 1.3.33(P) 1.3.35(P) 1.3.37(P) 1.3.39(P) 1.3.43(P) 1.3.45(P) 1.3.47(P) 1.3.49(P) 1.3.51(P) 1.3.53(P) 1.3.59(P) 1.3.61(P) 1.3.63(P) 1.3.65(P) 1.3.73(P) 1.3.75(P) 1.3.77(P) 1.4.1(P) 1.4.5(P) 1.4.19(P) 1.4.23(P) 1.4.25(P) 1.4.27(P) 1.4.31(P) 1.4.33(P) 1.4.35(P) 1.4.37(P) 1.4.39(P) 1.4.41(P) 1.4.43(P) 1.4.45(P) 1.4.47(P) 1.4.49(P) 1.4.51(P) 1.4.53(P) 1.4.55(P) 1.4.57(P) 1.4.59(P) 1.4.61(P) 1.4.63(P) 1.4.65(P) 1.4.67(P) 1.5.7(P) 1.5.11(P) 1.5.19(P) 1.5.23(P) 1.5.31(P) 1.5.33(P) 1.5.41(P) 1.5.45(P) 1.5.47(P) 1.5.49(P) 1.5.51(P) 1.5.53(P) 1.5.55(P) 1.5.59(P) 1.5.75(P) 1.5.77(P) 1.5.83(P) 1.5.85(P) 1.5.91(P) 1.5.93(P) 1.5.95(P) 1.5.103(P) 1.5.111(P) 1.5.113(P) 1.5.119(P) 1.6.13(P) 1.6.15(P) 1.6.21(P) 1.6.25(P) 1.6.27(P) 1.6.29(P) 1.6.35(P) 1.6.39(P) 1.6.41(P) 1.6.43(P) 1.6.45(P) 1.6.49(P) 1.6.55(P) 1.6.57(P) 1.6.63(P) 1.6.67(P) 1.6.69(P) 1.6.75(P) 1.6.77(P) 1.6.79(P) 1.6.81(P) 1.6.83(P) 1.6.85(P) 1.6.87(P) 1.6.89(P) 1.6.93(P) 1.6.95(P) 1.6.97(P) 1.6.103(P) 1.6.105(P) 1.6.109(P) 1.7.1(P) 1.7.5(P) 1.7.9(P) 1.7.15(P) 1.7.17(P) 1.7.19(P) 1.7.21(P) 1.7.23(P) 1.7.25(P) 1.7.27(P) 1.7.29(P) 1.7.39(P) 1.7.43(P) 1.7.47(P) 1.7.55(P) 1.7.57(P) 1.7.59(P) 1.7.61(P) 1.7.63(P) 1.7.65(P) 1.7.67(P) 1.7.69(P) 1.7.73(P) 1.7.75(P) 1.7.77(P) 1.7.79(P) 1.8.1(P) 1.8.3(P) 1.8.5(P) 1.8.9(P) 1.8.11(P) 1.8.13(P) 1.8.15(P) 1.8.17(P) 1.8.19(P) 1.8.21(P) 1.8.23(P) 1.8.25(P) 1.8.27(P) 1.8.29(P) 1.8.31(P) 1.8.33(P) 1.8.45(P) 1.8.47(P) 1.8.49(P) 1.8.53(P) 1.8.55(P) 1.8.67(P) 1.8.69(P) 1.8.71Test GLGeogebra Q1Geogebra Q2 - always correct
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					1(P) 1.1.39Multiply and divide fractions.11m 42s2(P) 1.1.45Multiply and divide fractions.46m 32s3(P) 1.1.47Multiply and divide fractions.22m 16s4(P) 1.1.51Add and subtract fractions.22m 59s5(P) 1.1.53Add and subtract fractions.15m 18s6(P) 1.1.55Add and subtract fractions.18m 8s7(P) 1.1.59Add and subtract fractions.52m 43s
					
					
						
							Click Questions to select questions and click Media to select media. Click Add to include selected questions or media in this assignment.
						
						Click the checkbox next to individual Media file to select them.
Then click Add to include that media in this assignment.
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student homework
		
							View student practice set
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	+ (1.1) CustomQuestion1 (Add and subtract fractions.)
	




	
		
		Name
		SampleHW
		

		
		Book
		
			
				NextGen Media Math Test Book--REAL
				Review Individual Student Settings
			
			
		
		

		
			Availability Options
		
		
			
			
				
					
					
						Available
						
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						Calendar
					
						
							
								
									Title and navigation
								
									
										
									
								
	
		&lt;&lt;&lt;June 2022>>>
	

							
						
					
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

				
			
		
	

							    
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						
							Time picker
						
							[x]
						
							12:00 AM1:00 AM2:00 AM
						
							3:00 AM4:00 AM5:00 AM
						
							6:00 AM7:00 AM8:00 AM
						
							9:00 AM10:00 AM11:00 AM
						
							12:00 PM1:00 PM2:00 PM
						
							3:00 PM4:00 PM5:00 PM
						
							6:00 PM7:00 PM8:00 PM
						
							9:00 PM10:00 PM11:00 PM
						
					
				
			
		
	

							
						
						
							Current course time:
							12:30pm
							
								Time zone:
								(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi
								
	Change...

							
						
					
					
					
						Due
						
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						Calendar
					
						
							
								
									Title and navigation
								
									
										
									
								
	
		&lt;&lt;&lt;June 2022>>>
	

							
						
					
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

				
			
		
	

							    
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						
							Time picker
						
							[x]
						
							12:00 AM1:00 AM2:00 AM
						
							3:00 AM4:00 AM5:00 AM
						
							6:00 AM7:00 AM8:00 AM
						
							9:00 AM10:00 AM11:00 AM
						
							12:00 PM1:00 PM2:00 PM
						
							3:00 PM4:00 PM5:00 PM
						
							6:00 PM7:00 PM8:00 PM
						
							9:00 PM10:00 PM11:00 PM
						
					
				
			
		
	

							
						
					
				
			
		
		
		
		

			
			SBChapter Associations
			
				
	
					Display with assignments from sbchapters:  Change...
					Note: This assignment covers material from sbchapters 1
					
						
							
							Select SBChapters:
							
								

								1. The Real Number System2. Linear Equations and Inequalities in One Variable3. Linear Equations and Inequalities in Two Variables; Functions4. MarkD Test Chapter5. Chapter not in Study Plan6. Drag and Drop Questions7. Essay questions8. StatCrunch9. 6-15-21 Adaptive Diagnostic
							
							
								
									Cancel
	
									OK
	
								
							
						
						
					
				

				
			
		


		
	Scoring Options



		
		
	
	Late submissions
	Allow students to work and change score after due date 
				
					
		
					Require password 
					
					
	
	

					
		
					Require final submission 
						
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;June 2022>>>
	

									
								
							
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

						
					
				
			
		    
						
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								
									Time picker
								
									[x]
								
									12:00 AM1:00 AM2:00 AM
								
									3:00 AM4:00 AM5:00 AM
								
									6:00 AM7:00 AM8:00 AM
								
									9:00 AM10:00 AM11:00 AM
								
									12:00 PM1:00 PM2:00 PM
								
									3:00 PM4:00 PM5:00 PM
								
									6:00 PM7:00 PM8:00 PM
								
									9:00 PM10:00 PM11:00 PM
								
							
						
					
				
			
		
					
	
	

					
		
					Deduct late submission penalty
						
			From final score
			Per day, from final score

		  
						Penalty
						
						
			%
			Points

		
					
	
	

					
		
							Apply only to questions scored after the due date
						
	
	

				
			


		
		
	
	Partial Credit
	Allow partial credit on questions with multiple parts 



		
			
			
			
			*** DISCONTINUED
		

		
		
	
	Show Work Questions
	
				
					
						Credit for Question:
					
					
						Automatically score question
						%
						
					
					
						Manually score question
					
					
						Credit for Show Work:
					
					
						Automatically score Show Work
						%
						
					
					
						Manually score Show Work
					
					
						Allow students to access SBHelp Me Solve This and SBView an Example 
					
				
			




		
			Access Controls
		

		
	    
	
	LockDown Browser and Proctoring
	
	            
		Do not use LockDown Browser or automated proctoring
		Require LockDown Browser only (no proctoring)
		Require ProctorU Record+

	
	        

        
        

		
	
	
                LockDown Browser
			
	
				
					
						
							Require the Pearson LockDown Browser
						
						    
		Do not use LockDown Browser or automated proctoring
		Require LockDown Browser only (no proctoring)
		Require LockDown Browser with Respondus Monitor Proctoring

	
                        
					
					
		
						    (Warning: The Pearson LockDown Browser is not compatible with mobile devices or Chromebooks. Tests or quizzes requiring the Pearson Lockdown Browser must be taken on desktop or laptop computers.)
						
	
	
					
		
							Do not allow use of calculator
						
	
	
					
		
							Allow use of basic calculator
						
	
	
					
		
							Allow use of scientific calculator
						
	
	
                    
				    
				        
				            You can edit Respondus Monitor Proctoring settings after saving this assignment.
				        
				    
                    
				    
				        
				            
				                Warning:
				                To enable Respondus Monitor Proctoring, go to Instructor Home to complete the license setup.
				            
				        
                    
				
			


        
	    
	    
	
	
	            Automated Proctoring
	        
	
                
                    
                        
                            Require ProctorU Record+
                        
                    
                    
                        
                            (Warning: Tests or quizzes requiring ProctorU Record+ must be taken on desktop or laptop computers using the Chrome browser.)
                        
                    
                    
                        
                            Sensitivity Settings: 
                            Customize ProctorU Settings
                            
                            
                        
                    
                

            



		
		
	
	IP Address Range
	
				Students must access this assignment from the following IP address range:
				
					  
					Change...
					
					Allow students to review this assignment from any IP address
				
			



		
		
			
			Password
			Required password  
		

		
		
			
			Prerequisites
			
				
					None
						Change...
				
			
		

		
		

		
		
	
	Attempts per Assignment
	Limit number of attempts to  



		
		
	
	Number of AttemptsPer Problem
	
				One Attempt (with 1 retry)
				Unlimited Attempts
			


		
		
	
	Attempts per question
	
				Limit number of times students can work each question to
				
			


		
		

		
		
			
			Incomplete Attempt
			
				
				
					
						
						
					
				
				
				
				

				
				
					
						If attempt is interrupted, students may re-access and complete on their own
						If attempt is interrupted, instructors must enable access
					
				
				
				
				

				
				
					
						Restricted Access: Student cannot resume an incomplete attempt until the instructor enables access to it.
						Blocked Access: Student cannot access other assignments or questions while taking a homework.  If the student does not submit the homework, access to all questions and assignments is blocked until the instructor enables access to or deletes the incomplete homework.
					
				
				
				
				
			
		

		
		
	
	Media Access
	
				Require students to work media before answering questions
			



		
		
	Presentation Options



		
		
	
	Review Steps (test)
	
				This is a test of the alternate text specified in SiteBuilder for Review Steps
			




		
		
	
	Lock Correct Answers
	
				Indicate and lock all correct answers after each Check Answer
			



		
		
	
	Question variation
	
				Use the same question values for all students
			



		
		
	
	Save Values
	
				Save question values and student answers
			



		
		
	
	Printing
	
				Allow students to print this homework assignment
			



		
		
	
	Time Limit
	
				Test time allowed (minutes): 
				
		
					
					     Show time remaining during test
				
	
			


		
		
	
	Question display
	
				Scramble question order for each student 
				
		
				Shuffle answer options 
				Randomize variables 
				
	
			


		
	
	Learning Aids
	
				
					
					
						
							
								
									Select Learning Aids:

									
										
											
													
		
															
															
														
		
		SBHelp Me Solve This
	
	
												
													
		
															
															
														
		
		SBView an Example
	
	
												
													
		
															
															
														
		
		SBVideoooo
	
	
												
													
		
															
															
														
		
		SBAnimation
	
	
												
													
		
															
															
														
		
		SBTextbook
	
	
												
													
		
															
															
														
		
		Concept Review
	
	
												
													
		
															
															
														
		
		SBTextbook Extra
	
	
												
													
		
															
															
														
		
		Math Tools
	
	
												
													
		
															
															
														
		
		SBGlossary
	
	
												
													
		
															
															
														
		
		SBReview
	
	
												
													
		
															
															
														
		
		StatCrunch
	
	
												
													
		
															
															
														
		
		Geogebra

	
	
												
													
		
															
															
														
		
		Tech Help
	
	
												
													
		
															
															
														
		
		SBExtra Help
	
	
												
													
												
													
		
															
															
														
		
		Ask My Instructor
	
	
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
		
															
															
														
		
		Instructor Tip
	
	
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
										
									

									
										Note: Instructor has customized learning aids for select questions. Changes made here will not affect these questions.
									
									
									
									
										
											Cancel
	
											  OK
	
										
									
								
							
						
					

				

			    SBHelp Me Solve This, SBView an Example, SBAnimation, Concept Review, Math Tools, Ask My Instructor, Instructor Tip      
			    
			        Change....
			    
				
			    
				
					     Show in Review mode only
				
			



	
	Graphing
	
				Allow students to move points by typing coordinates
			



	
		
			Review Options
		

		
		
			
			Results Display
			
				
	Test Summary shows test score and question results
	Test Summary shows test score only
	Hide score and question results


				
			
		

		
		
			
			Reviewing Test
			
			
				
	Student can review assignment any time after submitting
	Student can review assignment only immediately after submitting
	Student can review submitted assignment any time after due date
	Student can never review submitted assignment


			
		

		
		
			
			Feedback
			Allow students to view feedback while reviewing
		

		
		
	
	Study Plan
	Results are used to update the study plan
				
					
					     Link to mastered and needs study topics
				
			



		
		
			
			Answer Display
			
				Only show student's answers when reviewing
				Show student's and correct answers when reviewing
			
		

		 
		
	
	Print
	Allow students to print the test with the correct answers and their answers while reviewing 



		
		
	Learning Path


		
		
	
	Mastery Score
	
				Minimum score for mastery
				%

			



		
		
	Other


		
		
	
	Importing
	Allow other instructors to import this assignment 


	    
	
   

	
	
		
		
		
	    
			
				
					Edit Test Summary Message
				
				
					Customize the message given to students at the end of the test.
					
					
						
						


					
				
				
					Cancel
					Ok
				
			
		
	
	



	
		
			NameSampleHW
			BookNextGen Media Math Test Book--REAL
		
	
	

	
		
			Select criteria for setting content to Needs Study.   If you wish to remove content from the student's Learning Path but keep it available in Extra Practice, choose Hide from Learning Path. If a content area is not covered in your course, you can leave its criteria blank.
			
				
					
						
						Module
						Topic
						Covered on Test
						Criteria for setting to  Needs Study
						Score
					
					
						 
						 
						 
						 
						


						

   %
					
					
						 
						 
						 
						 
						Apply to Selected

						Apply to Selected

					
				
			
		
	

	
		
				Cancel

			    
					Next

					
				
			    
			        
						Save &amp; Assign

			        
			    
				
					Save

				
			    Back

				
			
		
	
	
	
		

		
			
			
				Cancel

				No

				Yes

			
			
				No, Just Save

				Yes, Take Me There

			
				
			
		
	
	
	
		
		DEV: 404 Save
		
	






    var YesNoCancelPop = {
        _ActionHandler: null,
        Show: function (header, msg, actionHandler, options) {
            //Register popup events
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnYesId&quot;, function () { YesNoCancelPop.DoAction('Yes'); });
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnNoId&quot;, function () { YesNoCancelPop.DoAction('No'); });
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnCancelId&quot;, function () { YesNoCancelPop.DoAction('Cancel'); });
            //Show popup and apply header,body and button text
            $('#mainModal').modal('show');
            $('#headerTxt').html(header);
            $('#bodyTxt').html(msg);
            $('#btnYesId').html(options.yesText);
            $('#btnNoId').html(options.noText);
            $('#btnCancelId').html(options.cancelText);
            //Register parent function in popup side
            if (actionHandler != null)
                YesNoCancelPop.SetActionHandler(actionHandler);

            //Show popup buttons 
            YesNoCancelPop.ShowButtons(options);  
            YesNoCancelPop.CustomStyles(options);
        },
        SetActionHandler: function (actionHandler) {
            YesNoCancelPop._ActionHandler = actionHandler;
        },
        //Get popup (yes/no/cancel) event and pass it to parent
        DoAction: function (action) {
            $('#mainModal').modal('hide');
            if (YesNoCancelPop._ActionHandler)
                YesNoCancelPop._ActionHandler(action);
        },
        ShowButtons: function (options) {
            options.showYesButton ? $(&quot;#btnYesId&quot;).show() : $(&quot;#btnYesId&quot;).hide();
            options.showNoButton ? $(&quot;#btnNoId&quot;).show() : $(&quot;#btnNoId&quot;).hide();
            options.showCancelButton ? $(&quot;#btnCancelId&quot;).show() : $(&quot;#btnCancelId&quot;).hide();
        return true;
        }, CustomStyles: function (options) {
            if (options.width) {
                $(&quot;#innerModel&quot;).width(options.width);
                $(&quot;#popup-hr-bottom&quot;).width(options.width - 40);
                $(&quot;#popup-hr-top&quot;).width(options.width - 40);
            }
            if (options.hideTitle) {
                options.hideTitle ? $(&quot;#headerTxt&quot;).hide() : $(&quot;#headerTxt&quot;).show();
                options.hideTitle ? $(&quot;#popup-hr-top&quot;).hide() : $(&quot;#popup-hr-top&quot;).show();
            }

            if (options.showYesButton || options.showNoButton) {
                $(&quot;#btnGroupCancel&quot;).css(&quot;margin-left&quot;, &quot;10px&quot;);
            }

            if (options.btnYesNo &amp;&amp; options.btnYesNo.alignRight) {
                $(&quot;#btnGroupYesNo&quot;).css(&quot;float&quot;, &quot;right&quot;);
                $(&quot;#btnGroupCancel&quot;).css(&quot;margin-left&quot;, &quot;0&quot;);
            }

            if (options.btnCancel &amp;&amp; options.btnCancel.alignRight){
                $(&quot;#btnGroupCancel&quot;).css(&quot;float&quot;, &quot;right&quot;);
            }

            if (options.paddingTop)
                $(&quot;#modalText&quot;).css(&quot;padding-top&quot;, options.paddingTop);

            if (options.paddingBottom)
                $(&quot;#modalText&quot;).css(&quot;padding-bottom&quot;, options.paddingBottom);

            if (options.btnYesGray)
                $(&quot;#btnYesId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);
            if (options.btnNoGray)
                $(&quot;#btnNoId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);
            if (options.btnCancelGray)
                $(&quot;#btnCancelId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);

            if (options.btnYesPrimary)
                $(&quot;#btnYesId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            if (options.btnNoPrimary)
                $(&quot;#btnNoId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            if (options.btnCancelPrimary)
                $(&quot;#btnCancelId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            
        return true;
        }

        
    }



            
            
                
                
                    
                        
                            
                        
                        
                        
                            
                        
                        
                        
                            
                                
                                    
                                    
                                
                                
		                            
                                
                            
                        
                          
                
            
 

    



                                                                    
                                                            </value>
      <webElementGuid>106d8ee8-87b8-451a-b8eb-8c174051a410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]</value>
      <webElementGuid>51fe31c8-a21d-435b-88d5-3f9abb6e37f7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td</value>
      <webElementGuid>7d4bab08-7d4e-43d7-9204-1282518c36b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook'])[1]/following::td[3]</value>
      <webElementGuid>fd3064fa-a837-485d-b7ba-dd1ecfed1cba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan Manager'])[1]/following::td[3]</value>
      <webElementGuid>5d145180-ea3b-4f5b-af23-2aed706a525c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td</value>
      <webElementGuid>509a5004-eadf-48e0-b696-7ca0bd20382b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = concat(&quot;
                                                                    
                                                                

                                                                
                                                                    
                                                                 
                                                                    
	                                                                   
                                                                        
                                                                        
                                                                        
                                                                 
                                                                        
                                                                    

                                                                  

                                                                

                                                                
	
		
                                                                            
                                                                            
                                                                                
                                                                                    Prabhashi ins1_cert
                                                                                    DevTestingCourse
                                                                                
                                                                                
                                                                                    Prabhashi ins1_cert
                                                                                    NextGen Media Math Test Book--REAL
                                                                                
                                                                                
                                                                                    06/03/22
                                                                                    12:30pm
                                                                                
                                                                            
                                                                            
                                                                            
                                                                        
	


                                                                
                                                                    
                                                                    
                                                                
                                                                
                                                                
	
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [&quot; , &quot;'&quot; , &quot;tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelTabs&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelBookFilter&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelCheckboxFilters&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;], [&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DropDownListChapter&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DropDownListSection&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListObjective&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DropDownListObjective&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>




	
    








	learningAids = new Array();


		
			var learningAidguidedsolution = new Object();
			learningAidguidedsolution.name = &quot; , &quot;'&quot; , &quot;guidedsolution&quot; , &quot;'&quot; , &quot;;
			learningAidguidedsolution.displayText = &quot;SBHelp Me Solve This&quot;;
			learningAidguidedsolution.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_guidedsolution.gif&quot; , &quot;'&quot; , &quot;;
			learningAidguidedsolution.enabled = true;
			learningAidguidedsolution.hasCustomLogic = false;
			learningAidguidedsolution.visible = true;
			learningAidguidedsolution.isExerciseSpecific = true;
			learningAidguidedsolution.exerciseExists = false;
			learningAidguidedsolution.playerParamName = &quot; , &quot;'&quot; , &quot;showguidedsolution&quot; , &quot;'&quot; , &quot;;
			//learningAidguidedsolution.initCapName = learningAidguidedsolution.name.substring(0, 1).toUpperCase() + learningAidguidedsolution.name.substring(1);
			learningAidguidedsolution.typeID = 10;

			learningAids.push(learningAidguidedsolution);


			function ViewSample_guidedsolution()
			{
				popupWindow(&quot;guidedsolution&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/images/helpmesolvethis700.jpg&quot;, {Width:700,Height:460,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidexample = new Object();
			learningAidexample.name = &quot; , &quot;'&quot; , &quot;example&quot; , &quot;'&quot; , &quot;;
			learningAidexample.displayText = &quot;SBView an Example&quot;;
			learningAidexample.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_sampleproblem.gif&quot; , &quot;'&quot; , &quot;;
			learningAidexample.enabled = true;
			learningAidexample.hasCustomLogic = false;
			learningAidexample.visible = true;
			learningAidexample.isExerciseSpecific = true;
			learningAidexample.exerciseExists = false;
			learningAidexample.playerParamName = &quot; , &quot;'&quot; , &quot;showexample&quot; , &quot;'&quot; , &quot;;
			//learningAidexample.initCapName = learningAidexample.name.substring(0, 1).toUpperCase() + learningAidexample.name.substring(1);
			learningAidexample.typeID = 11;

			learningAids.push(learningAidexample);


			function ViewSample_example()
			{
				popupWindow(&quot;example&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/images/showexample700.jpg&quot;, {Width:700,Height:460,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidvideo = new Object();
			learningAidvideo.name = &quot; , &quot;'&quot; , &quot;video&quot; , &quot;'&quot; , &quot;;
			learningAidvideo.displayText = &quot;SBVideoooo&quot;;
			learningAidvideo.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_video.gif&quot; , &quot;'&quot; , &quot;;
			learningAidvideo.enabled = true;
			learningAidvideo.hasCustomLogic = false;
			learningAidvideo.visible = true;
			learningAidvideo.isExerciseSpecific = true;
			learningAidvideo.exerciseExists = false;
			learningAidvideo.playerParamName = &quot; , &quot;'&quot; , &quot;showvideo&quot; , &quot;'&quot; , &quot;;
			//learningAidvideo.initCapName = learningAidvideo.name.substring(0, 1).toUpperCase() + learningAidvideo.name.substring(1);
			learningAidvideo.typeID = 1;

			learningAids.push(learningAidvideo);


			function ViewSample_video()
			{
				popupWindow(&quot;video&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/flash_video_player/player.html?/aw/aw_bittinger_eaconapp_8/video/bke08_0101e01&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidanimation = new Object();
			learningAidanimation.name = &quot; , &quot;'&quot; , &quot;animation&quot; , &quot;'&quot; , &quot;;
			learningAidanimation.displayText = &quot;SBAnimation&quot;;
			learningAidanimation.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_animation.gif&quot; , &quot;'&quot; , &quot;;
			learningAidanimation.enabled = true;
			learningAidanimation.hasCustomLogic = false;
			learningAidanimation.visible = true;
			learningAidanimation.isExerciseSpecific = true;
			learningAidanimation.exerciseExists = false;
			learningAidanimation.playerParamName = &quot; , &quot;'&quot; , &quot;showanimation&quot; , &quot;'&quot; , &quot;;
			//learningAidanimation.initCapName = learningAidanimation.name.substring(0, 1).toUpperCase() + learningAidanimation.name.substring(1);
			learningAidanimation.typeID = 2;

			learningAids.push(learningAidanimation);


			function ViewSample_animation()
			{
				popupWindow(&quot;animation&quot;, &quot;http://media.pearsoncmg.com/aw/aw_mml_shared_1/precalculus/01020141/index.html&quot;, {Width:620,Height:370,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtextbook = new Object();
			learningAidtextbook.name = &quot; , &quot;'&quot; , &quot;textbook&quot; , &quot;'&quot; , &quot;;
			learningAidtextbook.displayText = &quot;SBTextbook&quot;;
			learningAidtextbook.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_textbook.gif&quot; , &quot;'&quot; , &quot;;
			learningAidtextbook.enabled = true;
			learningAidtextbook.hasCustomLogic = false;
			learningAidtextbook.visible = true;
			learningAidtextbook.isExerciseSpecific = true;
			learningAidtextbook.exerciseExists = false;
			learningAidtextbook.playerParamName = &quot; , &quot;'&quot; , &quot;showtextbook&quot; , &quot;'&quot; , &quot;;
			//learningAidtextbook.initCapName = learningAidtextbook.name.substring(0, 1).toUpperCase() + learningAidtextbook.name.substring(1);
			learningAidtextbook.typeID = 5;

			learningAids.push(learningAidtextbook);


			function ViewSample_textbook()
			{
				popupWindow(&quot;textbook&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/mxl_ebook_view_sample/aps08_flash_main.html&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidconceptbook = new Object();
			learningAidconceptbook.name = &quot; , &quot;'&quot; , &quot;conceptbook&quot; , &quot;'&quot; , &quot;;
			learningAidconceptbook.displayText = &quot;Concept Review&quot;;
			learningAidconceptbook.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_conceptbook.png&quot; , &quot;'&quot; , &quot;;
			learningAidconceptbook.enabled = true;
			learningAidconceptbook.hasCustomLogic = false;
			learningAidconceptbook.visible = true;
			learningAidconceptbook.isExerciseSpecific = true;
			learningAidconceptbook.exerciseExists = false;
			learningAidconceptbook.playerParamName = &quot; , &quot;'&quot; , &quot;showconceptbook&quot; , &quot;'&quot; , &quot;;
			//learningAidconceptbook.initCapName = learningAidconceptbook.name.substring(0, 1).toUpperCase() + learningAidconceptbook.name.substring(1);
			learningAidconceptbook.typeID = 31;

			learningAids.push(learningAidconceptbook);


			function ViewSample_conceptbook()
			{
				popupWindow(&quot;conceptbook&quot;, &quot;/support/learningaids/conceptbook.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtextbookextras = new Object();
			learningAidtextbookextras.name = &quot; , &quot;'&quot; , &quot;textbookextras&quot; , &quot;'&quot; , &quot;;
			learningAidtextbookextras.displayText = &quot;SBTextbook Extra&quot;;
			learningAidtextbookextras.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_textbookextras.png&quot; , &quot;'&quot; , &quot;;
			learningAidtextbookextras.enabled = true;
			learningAidtextbookextras.hasCustomLogic = false;
			learningAidtextbookextras.visible = true;
			learningAidtextbookextras.isExerciseSpecific = true;
			learningAidtextbookextras.exerciseExists = false;
			learningAidtextbookextras.playerParamName = &quot; , &quot;'&quot; , &quot;showtextbookextras&quot; , &quot;'&quot; , &quot;;
			//learningAidtextbookextras.initCapName = learningAidtextbookextras.name.substring(0, 1).toUpperCase() + learningAidtextbookextras.name.substring(1);
			learningAidtextbookextras.typeID = 28;

			learningAids.push(learningAidtextbookextras);


			function ViewSample_textbookextras()
			{
				popupWindow(&quot;textbookextras&quot;, &quot;/support/learningaids/textbookextras.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidmathtools = new Object();
			learningAidmathtools.name = &quot; , &quot;'&quot; , &quot;mathtools&quot; , &quot;'&quot; , &quot;;
			learningAidmathtools.displayText = &quot;Math Tools&quot;;
			learningAidmathtools.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_mathtools.png&quot; , &quot;'&quot; , &quot;;
			learningAidmathtools.enabled = true;
			learningAidmathtools.hasCustomLogic = false;
			learningAidmathtools.visible = true;
			learningAidmathtools.isExerciseSpecific = true;
			learningAidmathtools.exerciseExists = false;
			learningAidmathtools.playerParamName = &quot; , &quot;'&quot; , &quot;showmathtools&quot; , &quot;'&quot; , &quot;;
			//learningAidmathtools.initCapName = learningAidmathtools.name.substring(0, 1).toUpperCase() + learningAidmathtools.name.substring(1);
			learningAidmathtools.typeID = 32;

			learningAids.push(learningAidmathtools);


			function ViewSample_mathtools()
			{
				popupWindow(&quot;mathtools&quot;, &quot;/support/learningaids/mathtools.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidglossary = new Object();
			learningAidglossary.name = &quot; , &quot;'&quot; , &quot;glossary&quot; , &quot;'&quot; , &quot;;
			learningAidglossary.displayText = &quot;SBGlossary&quot;;
			learningAidglossary.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_glossary.gif&quot; , &quot;'&quot; , &quot;;
			learningAidglossary.enabled = true;
			learningAidglossary.hasCustomLogic = false;
			learningAidglossary.visible = true;
			learningAidglossary.isExerciseSpecific = true;
			learningAidglossary.exerciseExists = false;
			learningAidglossary.playerParamName = &quot; , &quot;'&quot; , &quot;showglossary&quot; , &quot;'&quot; , &quot;;
			//learningAidglossary.initCapName = learningAidglossary.name.substring(0, 1).toUpperCase() + learningAidglossary.name.substring(1);
			learningAidglossary.typeID = 14;

			learningAids.push(learningAidglossary);


			function ViewSample_glossary()
			{
				popupWindow(&quot;glossary&quot;, &quot;/support/learningaids/glossary.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidreview = new Object();
			learningAidreview.name = &quot; , &quot;'&quot; , &quot;review&quot; , &quot;'&quot; , &quot;;
			learningAidreview.displayText = &quot;SBReview&quot;;
			learningAidreview.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_review.png&quot; , &quot;'&quot; , &quot;;
			learningAidreview.enabled = true;
			learningAidreview.hasCustomLogic = false;
			learningAidreview.visible = true;
			learningAidreview.isExerciseSpecific = true;
			learningAidreview.exerciseExists = false;
			learningAidreview.playerParamName = &quot; , &quot;'&quot; , &quot;showreviewinplayer&quot; , &quot;'&quot; , &quot;;
			//learningAidreview.initCapName = learningAidreview.name.substring(0, 1).toUpperCase() + learningAidreview.name.substring(1);
			learningAidreview.typeID = 24;

			learningAids.push(learningAidreview);


			function ViewSample_review()
			{
				popupWindow(&quot;review&quot;, &quot;/support/learningaids/reviewinplayer.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidstatexplore = new Object();
			learningAidstatexplore.name = &quot; , &quot;'&quot; , &quot;statexplore&quot; , &quot;'&quot; , &quot;;
			learningAidstatexplore.displayText = &quot;StatCrunch&quot;;
			learningAidstatexplore.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_statcrunch.gif&quot; , &quot;'&quot; , &quot;;
			learningAidstatexplore.enabled = true;
			learningAidstatexplore.hasCustomLogic = false;
			learningAidstatexplore.visible = true;
			learningAidstatexplore.isExerciseSpecific = true;
			learningAidstatexplore.exerciseExists = false;
			learningAidstatexplore.playerParamName = &quot; , &quot;'&quot; , &quot;showstatexplore&quot; , &quot;'&quot; , &quot;;
			//learningAidstatexplore.initCapName = learningAidstatexplore.name.substring(0, 1).toUpperCase() + learningAidstatexplore.name.substring(1);
			learningAidstatexplore.typeID = 3;

			learningAids.push(learningAidstatexplore);


			function ViewSample_statexplore()
			{
				popupWindow(&quot;statexplore&quot;, &quot;http://media.pearsoncmg.com/aw/aw_mml_shared_1/statexplore.gif&quot;, {Width:820,Height:560,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidgeogebra = new Object();
			learningAidgeogebra.name = &quot; , &quot;'&quot; , &quot;geogebra&quot; , &quot;'&quot; , &quot;;
			learningAidgeogebra.displayText = &quot;Geogebra\n&quot;;
			learningAidgeogebra.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_geogebra.png&quot; , &quot;'&quot; , &quot;;
			learningAidgeogebra.enabled = true;
			learningAidgeogebra.hasCustomLogic = false;
			learningAidgeogebra.visible = true;
			learningAidgeogebra.isExerciseSpecific = true;
			learningAidgeogebra.exerciseExists = false;
			learningAidgeogebra.playerParamName = &quot; , &quot;'&quot; , &quot;showgeogebra&quot; , &quot;'&quot; , &quot;;
			//learningAidgeogebra.initCapName = learningAidgeogebra.name.substring(0, 1).toUpperCase() + learningAidgeogebra.name.substring(1);
			learningAidgeogebra.typeID = 26;

			learningAids.push(learningAidgeogebra);


			function ViewSample_geogebra()
			{
				popupWindow(&quot;geogebra&quot;, &quot;/support/learningaids/geogebra.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtechhelp = new Object();
			learningAidtechhelp.name = &quot; , &quot;'&quot; , &quot;techhelp&quot; , &quot;'&quot; , &quot;;
			learningAidtechhelp.displayText = &quot;Tech Help&quot;;
			learningAidtechhelp.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_techhelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidtechhelp.enabled = true;
			learningAidtechhelp.hasCustomLogic = false;
			learningAidtechhelp.visible = true;
			learningAidtechhelp.isExerciseSpecific = true;
			learningAidtechhelp.exerciseExists = false;
			learningAidtechhelp.playerParamName = &quot; , &quot;'&quot; , &quot;showtechhelp&quot; , &quot;'&quot; , &quot;;
			//learningAidtechhelp.initCapName = learningAidtechhelp.name.substring(0, 1).toUpperCase() + learningAidtechhelp.name.substring(1);
			learningAidtechhelp.typeID = 23;

			learningAids.push(learningAidtechhelp);


			function ViewSample_techhelp()
			{
				popupWindow(&quot;techhelp&quot;, &quot;/support/learningaids/techhelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidextrahelp = new Object();
			learningAidextrahelp.name = &quot; , &quot;'&quot; , &quot;extrahelp&quot; , &quot;'&quot; , &quot;;
			learningAidextrahelp.displayText = &quot;SBExtra Help&quot;;
			learningAidextrahelp.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_extrahelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidextrahelp.enabled = true;
			learningAidextrahelp.hasCustomLogic = false;
			learningAidextrahelp.visible = true;
			learningAidextrahelp.isExerciseSpecific = true;
			learningAidextrahelp.exerciseExists = false;
			learningAidextrahelp.playerParamName = &quot; , &quot;'&quot; , &quot;showextrahelp&quot; , &quot;'&quot; , &quot;;
			//learningAidextrahelp.initCapName = learningAidextrahelp.name.substring(0, 1).toUpperCase() + learningAidextrahelp.name.substring(1);
			learningAidextrahelp.typeID = 25;

			learningAids.push(learningAidextrahelp);


			function ViewSample_extrahelp()
			{
				popupWindow(&quot;extrahelp&quot;, &quot;/support/learningaids/extrahelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidcalculator = new Object();
			learningAidcalculator.name = &quot; , &quot;'&quot; , &quot;calculator&quot; , &quot;'&quot; , &quot;;
			learningAidcalculator.displayText = &quot;Calculator&quot;;
			learningAidcalculator.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_calc.gif&quot; , &quot;'&quot; , &quot;;
			learningAidcalculator.enabled = false;
			learningAidcalculator.hasCustomLogic = false;
			learningAidcalculator.visible = false;
			learningAidcalculator.isExerciseSpecific = false;
			learningAidcalculator.exerciseExists = false;
			learningAidcalculator.playerParamName = &quot; , &quot;'&quot; , &quot;showcalculator&quot; , &quot;'&quot; , &quot;;
			//learningAidcalculator.initCapName = learningAidcalculator.name.substring(0, 1).toUpperCase() + learningAidcalculator.name.substring(1);
			learningAidcalculator.typeID = 8;

			learningAids.push(learningAidcalculator);


			function ViewSample_calculator()
			{
				popupWindow(&quot;calculator&quot;, &quot;../support/learningaids/calculator_sample.html&quot;, {Width:640,Height:480,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidaskinstructor = new Object();
			learningAidaskinstructor.name = &quot; , &quot;'&quot; , &quot;askinstructor&quot; , &quot;'&quot; , &quot;;
			learningAidaskinstructor.displayText = &quot;Ask My Instructor&quot;;
			learningAidaskinstructor.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_askmyinstrctr.gif&quot; , &quot;'&quot; , &quot;;
			learningAidaskinstructor.enabled = true;
			learningAidaskinstructor.hasCustomLogic = true;
			learningAidaskinstructor.visible = true;
			learningAidaskinstructor.isExerciseSpecific = false;
			learningAidaskinstructor.exerciseExists = false;
			learningAidaskinstructor.playerParamName = &quot; , &quot;'&quot; , &quot;showaskinstructor&quot; , &quot;'&quot; , &quot;;
			//learningAidaskinstructor.initCapName = learningAidaskinstructor.name.substring(0, 1).toUpperCase() + learningAidaskinstructor.name.substring(1);
			learningAidaskinstructor.typeID = 6;

			learningAids.push(learningAidaskinstructor);


			function ViewSample_askinstructor()
			{
				popupWindow(&quot;askinstructor&quot;, &quot;sample_askmyinstructor.aspx&quot;, {Width:530,Height:430,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidconnecttotutor = new Object();
			learningAidconnecttotutor.name = &quot; , &quot;'&quot; , &quot;connecttotutor&quot; , &quot;'&quot; , &quot;;
			learningAidconnecttotutor.displayText = &quot;Connect to a Tutor&quot;;
			learningAidconnecttotutor.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_tutorvista.gif&quot; , &quot;'&quot; , &quot;;
			learningAidconnecttotutor.enabled = false;
			learningAidconnecttotutor.hasCustomLogic = true;
			learningAidconnecttotutor.visible = false;
			learningAidconnecttotutor.isExerciseSpecific = false;
			learningAidconnecttotutor.exerciseExists = false;
			learningAidconnecttotutor.playerParamName = &quot; , &quot;'&quot; , &quot;showconnecttotutor&quot; , &quot;'&quot; , &quot;;
			//learningAidconnecttotutor.initCapName = learningAidconnecttotutor.name.substring(0, 1).toUpperCase() + learningAidconnecttotutor.name.substring(1);
			learningAidconnecttotutor.typeID = 33;

			learningAids.push(learningAidconnecttotutor);


			function ViewSample_connecttotutor()
			{
				popupWindow(&quot;connecttotutor&quot;, &quot;/support/learningaids/tutor_sample.html&quot;, {Width:800,Height:690,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidfasttrack = new Object();
			learningAidfasttrack.name = &quot; , &quot;'&quot; , &quot;fasttrack&quot; , &quot;'&quot; , &quot;;
			learningAidfasttrack.displayText = &quot;Fast Track&quot;;
			learningAidfasttrack.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_fasttrack.gif&quot; , &quot;'&quot; , &quot;;
			learningAidfasttrack.enabled = false;
			learningAidfasttrack.hasCustomLogic = false;
			learningAidfasttrack.visible = false;
			learningAidfasttrack.isExerciseSpecific = true;
			learningAidfasttrack.exerciseExists = false;
			learningAidfasttrack.playerParamName = &quot; , &quot;'&quot; , &quot;showfasttrack&quot; , &quot;'&quot; , &quot;;
			//learningAidfasttrack.initCapName = learningAidfasttrack.name.substring(0, 1).toUpperCase() + learningAidfasttrack.name.substring(1);
			learningAidfasttrack.typeID = 4;

			learningAids.push(learningAidfasttrack);


			function ViewSample_fasttrack()
			{
				popupWindow(&quot;fasttrack&quot;, &quot;http://media.pearsoncmg.com/aw/aw_myeconlab/fasttrack/samplefasttrack.gif&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAiddemodoc = new Object();
			learningAiddemodoc.name = &quot; , &quot;'&quot; , &quot;demodoc&quot; , &quot;'&quot; , &quot;;
			learningAiddemodoc.displayText = &quot;DemoDocs Example&quot;;
			learningAiddemodoc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_demodocs.gif&quot; , &quot;'&quot; , &quot;;
			learningAiddemodoc.enabled = false;
			learningAiddemodoc.hasCustomLogic = false;
			learningAiddemodoc.visible = false;
			learningAiddemodoc.isExerciseSpecific = true;
			learningAiddemodoc.exerciseExists = false;
			learningAiddemodoc.playerParamName = &quot; , &quot;'&quot; , &quot;showdemodoc&quot; , &quot;'&quot; , &quot;;
			//learningAiddemodoc.initCapName = learningAiddemodoc.name.substring(0, 1).toUpperCase() + learningAiddemodoc.name.substring(1);
			learningAiddemodoc.typeID = 7;

			learningAids.push(learningAiddemodoc);


			function ViewSample_demodoc()
			{
				popupWindow(&quot;demodoc&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_mal_shared/sgdd1/index.html&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidkeyconcepts = new Object();
			learningAidkeyconcepts.name = &quot; , &quot;'&quot; , &quot;keyconcepts&quot; , &quot;'&quot; , &quot;;
			learningAidkeyconcepts.displayText = &quot;Key Concepts&quot;;
			learningAidkeyconcepts.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_demodocs.gif&quot; , &quot;'&quot; , &quot;;
			learningAidkeyconcepts.enabled = false;
			learningAidkeyconcepts.hasCustomLogic = false;
			learningAidkeyconcepts.visible = false;
			learningAidkeyconcepts.isExerciseSpecific = true;
			learningAidkeyconcepts.exerciseExists = false;
			learningAidkeyconcepts.playerParamName = &quot; , &quot;'&quot; , &quot;showkeyconcepts&quot; , &quot;'&quot; , &quot;;
			//learningAidkeyconcepts.initCapName = learningAidkeyconcepts.name.substring(0, 1).toUpperCase() + learningAidkeyconcepts.name.substring(1);
			learningAidkeyconcepts.typeID = 9;

			learningAids.push(learningAidkeyconcepts);


			function ViewSample_keyconcepts()
			{
				popupWindow(&quot;keyconcepts&quot;, &quot;http://media.pearsoncmg.com/intl/ema/uk/accounting_mal/sample_anim/mal_animation_sample1.htm?centerwin=yes&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidgrapher = new Object();
			learningAidgrapher.name = &quot; , &quot;'&quot; , &quot;grapher&quot; , &quot;'&quot; , &quot;;
			learningAidgrapher.displayText = &quot;Grapher&quot;;
			learningAidgrapher.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_grapher.gif&quot; , &quot;'&quot; , &quot;;
			learningAidgrapher.enabled = false;
			learningAidgrapher.hasCustomLogic = false;
			learningAidgrapher.visible = false;
			learningAidgrapher.isExerciseSpecific = true;
			learningAidgrapher.exerciseExists = false;
			learningAidgrapher.playerParamName = &quot; , &quot;'&quot; , &quot;showgrapher&quot; , &quot;'&quot; , &quot;;
			//learningAidgrapher.initCapName = learningAidgrapher.name.substring(0, 1).toUpperCase() + learningAidgrapher.name.substring(1);
			learningAidgrapher.typeID = 12;

			learningAids.push(learningAidgrapher);


			function ViewSample_grapher()
			{
				popupWindow(&quot;grapher&quot;, &quot;/support/learningaids/grapher.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidspreadsheet = new Object();
			learningAidspreadsheet.name = &quot; , &quot;'&quot; , &quot;spreadsheet&quot; , &quot;'&quot; , &quot;;
			learningAidspreadsheet.displayText = &quot;Spreadsheet&quot;;
			learningAidspreadsheet.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_spreadsheet.gif&quot; , &quot;'&quot; , &quot;;
			learningAidspreadsheet.enabled = false;
			learningAidspreadsheet.hasCustomLogic = false;
			learningAidspreadsheet.visible = false;
			learningAidspreadsheet.isExerciseSpecific = true;
			learningAidspreadsheet.exerciseExists = false;
			learningAidspreadsheet.playerParamName = &quot; , &quot;'&quot; , &quot;showspreadsheet&quot; , &quot;'&quot; , &quot;;
			//learningAidspreadsheet.initCapName = learningAidspreadsheet.name.substring(0, 1).toUpperCase() + learningAidspreadsheet.name.substring(1);
			learningAidspreadsheet.typeID = 13;

			learningAids.push(learningAidspreadsheet);


			function ViewSample_spreadsheet()
			{
				popupWindow(&quot;spreadsheet&quot;, &quot;/support/learningaids/spreadsheet.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidannuitycalc = new Object();
			learningAidannuitycalc.name = &quot; , &quot;'&quot; , &quot;annuitycalc&quot; , &quot;'&quot; , &quot;;
			learningAidannuitycalc.displayText = &quot;Annuity Calculator&quot;;
			learningAidannuitycalc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_annuitycalc.gif&quot; , &quot;'&quot; , &quot;;
			learningAidannuitycalc.enabled = false;
			learningAidannuitycalc.hasCustomLogic = false;
			learningAidannuitycalc.visible = false;
			learningAidannuitycalc.isExerciseSpecific = true;
			learningAidannuitycalc.exerciseExists = false;
			learningAidannuitycalc.playerParamName = &quot; , &quot;'&quot; , &quot;showannuitycalc&quot; , &quot;'&quot; , &quot;;
			//learningAidannuitycalc.initCapName = learningAidannuitycalc.name.substring(0, 1).toUpperCase() + learningAidannuitycalc.name.substring(1);
			learningAidannuitycalc.typeID = 15;

			learningAids.push(learningAidannuitycalc);


			function ViewSample_annuitycalc()
			{
				popupWindow(&quot;annuitycalc&quot;, &quot;/support/learningaids/annuitycalc.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidschaums = new Object();
			learningAidschaums.name = &quot; , &quot;'&quot; , &quot;schaums&quot; , &quot;'&quot; , &quot;;
			learningAidschaums.displayText = &quot;Step-by-Step Example&quot;;
			learningAidschaums.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_schaum.gif&quot; , &quot;'&quot; , &quot;;
			learningAidschaums.enabled = false;
			learningAidschaums.hasCustomLogic = false;
			learningAidschaums.visible = false;
			learningAidschaums.isExerciseSpecific = true;
			learningAidschaums.exerciseExists = false;
			learningAidschaums.playerParamName = &quot; , &quot;'&quot; , &quot;showschaums&quot; , &quot;'&quot; , &quot;;
			//learningAidschaums.initCapName = learningAidschaums.name.substring(0, 1).toUpperCase() + learningAidschaums.name.substring(1);
			learningAidschaums.typeID = 16;

			learningAids.push(learningAidschaums);


			function ViewSample_schaums()
			{
				popupWindow(&quot;schaums&quot;, &quot;/support/learningaids/schaums.htm&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidinstructortip = new Object();
			learningAidinstructortip.name = &quot; , &quot;'&quot; , &quot;instructortip&quot; , &quot;'&quot; , &quot;;
			learningAidinstructortip.displayText = &quot;Instructor Tip&quot;;
			learningAidinstructortip.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_view_tip.png&quot; , &quot;'&quot; , &quot;;
			learningAidinstructortip.enabled = true;
			learningAidinstructortip.hasCustomLogic = false;
			learningAidinstructortip.visible = true;
			learningAidinstructortip.isExerciseSpecific = false;
			learningAidinstructortip.exerciseExists = false;
			learningAidinstructortip.playerParamName = &quot; , &quot;'&quot; , &quot;showinstructortip&quot; , &quot;'&quot; , &quot;;
			//learningAidinstructortip.initCapName = learningAidinstructortip.name.substring(0, 1).toUpperCase() + learningAidinstructortip.name.substring(1);
			learningAidinstructortip.typeID = 17;

			learningAids.push(learningAidinstructortip);


			function ViewSample_instructortip()
			{
				popupWindow(&quot;instructortip&quot;, &quot;/support/learningaids/instructortip.htm&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidboosterc = new Object();
			learningAidboosterc.name = &quot; , &quot;'&quot; , &quot;boosterc&quot; , &quot;'&quot; , &quot;;
			learningAidboosterc.displayText = &quot;Practice Book&quot;;
			learningAidboosterc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_boosterc.png&quot; , &quot;'&quot; , &quot;;
			learningAidboosterc.enabled = false;
			learningAidboosterc.hasCustomLogic = false;
			learningAidboosterc.visible = false;
			learningAidboosterc.isExerciseSpecific = true;
			learningAidboosterc.exerciseExists = false;
			learningAidboosterc.playerParamName = &quot; , &quot;'&quot; , &quot;showboosterc&quot; , &quot;'&quot; , &quot;;
			//learningAidboosterc.initCapName = learningAidboosterc.name.substring(0, 1).toUpperCase() + learningAidboosterc.name.substring(1);
			learningAidboosterc.typeID = 18;

			learningAids.push(learningAidboosterc);


			function ViewSample_boosterc()
			{
				popupWindow(&quot;boosterc&quot;, &quot;/support/learningaids/boosterc.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidpowerpoint = new Object();
			learningAidpowerpoint.name = &quot; , &quot;'&quot; , &quot;powerpoint&quot; , &quot;'&quot; , &quot;;
			learningAidpowerpoint.displayText = &quot;SBPowerPoint Slides1&quot;;
			learningAidpowerpoint.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_powerpoint.png&quot; , &quot;'&quot; , &quot;;
			learningAidpowerpoint.enabled = false;
			learningAidpowerpoint.hasCustomLogic = false;
			learningAidpowerpoint.visible = false;
			learningAidpowerpoint.isExerciseSpecific = true;
			learningAidpowerpoint.exerciseExists = false;
			learningAidpowerpoint.playerParamName = &quot; , &quot;'&quot; , &quot;showpowerpoint&quot; , &quot;'&quot; , &quot;;
			//learningAidpowerpoint.initCapName = learningAidpowerpoint.name.substring(0, 1).toUpperCase() + learningAidpowerpoint.name.substring(1);
			learningAidpowerpoint.typeID = 19;

			learningAids.push(learningAidpowerpoint);


			function ViewSample_powerpoint()
			{
				popupWindow(&quot;powerpoint&quot;, &quot;/support/learningaids/powerpoint.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidfinancialcalc = new Object();
			learningAidfinancialcalc.name = &quot; , &quot;'&quot; , &quot;financialcalc&quot; , &quot;'&quot; , &quot;;
			learningAidfinancialcalc.displayText = &quot;Financial Calculator&quot;;
			learningAidfinancialcalc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_financialcalc.gif&quot; , &quot;'&quot; , &quot;;
			learningAidfinancialcalc.enabled = false;
			learningAidfinancialcalc.hasCustomLogic = false;
			learningAidfinancialcalc.visible = false;
			learningAidfinancialcalc.isExerciseSpecific = true;
			learningAidfinancialcalc.exerciseExists = false;
			learningAidfinancialcalc.playerParamName = &quot; , &quot;'&quot; , &quot;showfinancialcalc&quot; , &quot;'&quot; , &quot;;
			//learningAidfinancialcalc.initCapName = learningAidfinancialcalc.name.substring(0, 1).toUpperCase() + learningAidfinancialcalc.name.substring(1);
			learningAidfinancialcalc.typeID = 20;

			learningAids.push(learningAidfinancialcalc);


			function ViewSample_financialcalc()
			{
				popupWindow(&quot;financialcalc&quot;, &quot;http://media.pearsoncmg.com/aw/aw_myfinancelab/learningaids/financialcalc.jpg&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidomexplorertutor = new Object();
			learningAidomexplorertutor.name = &quot; , &quot;'&quot; , &quot;omexplorertutor&quot; , &quot;'&quot; , &quot;;
			learningAidomexplorertutor.displayText = &quot;OM Explorer Tutor&quot;;
			learningAidomexplorertutor.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_omexplorertutor.png&quot; , &quot;'&quot; , &quot;;
			learningAidomexplorertutor.enabled = false;
			learningAidomexplorertutor.hasCustomLogic = false;
			learningAidomexplorertutor.visible = false;
			learningAidomexplorertutor.isExerciseSpecific = true;
			learningAidomexplorertutor.exerciseExists = false;
			learningAidomexplorertutor.playerParamName = &quot; , &quot;'&quot; , &quot;showomexplorertutor&quot; , &quot;'&quot; , &quot;;
			//learningAidomexplorertutor.initCapName = learningAidomexplorertutor.name.substring(0, 1).toUpperCase() + learningAidomexplorertutor.name.substring(1);
			learningAidomexplorertutor.typeID = 21;

			learningAids.push(learningAidomexplorertutor);


			function ViewSample_omexplorertutor()
			{
				popupWindow(&quot;omexplorertutor&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_moml_shared/la_samples/omexptutor.jpg&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAideconspreadsheet = new Object();
			learningAideconspreadsheet.name = &quot; , &quot;'&quot; , &quot;econspreadsheet&quot; , &quot;'&quot; , &quot;;
			learningAideconspreadsheet.displayText = &quot;Excel Spreadsheet&quot;;
			learningAideconspreadsheet.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_spreadsheet.gif&quot; , &quot;'&quot; , &quot;;
			learningAideconspreadsheet.enabled = false;
			learningAideconspreadsheet.hasCustomLogic = false;
			learningAideconspreadsheet.visible = false;
			learningAideconspreadsheet.isExerciseSpecific = true;
			learningAideconspreadsheet.exerciseExists = false;
			learningAideconspreadsheet.playerParamName = &quot; , &quot;'&quot; , &quot;showeconexcelspreadsheet&quot; , &quot;'&quot; , &quot;;
			//learningAideconspreadsheet.initCapName = learningAideconspreadsheet.name.substring(0, 1).toUpperCase() + learningAideconspreadsheet.name.substring(1);
			learningAideconspreadsheet.typeID = 22;

			learningAids.push(learningAideconspreadsheet);


			function ViewSample_econspreadsheet()
			{
				popupWindow(&quot;econspreadsheet&quot;, &quot;/support/learningaids/econspreadsheet.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidcalculateexcel = new Object();
			learningAidcalculateexcel.name = &quot; , &quot;'&quot; , &quot;calculateexcel&quot; , &quot;'&quot; , &quot;;
			learningAidcalculateexcel.displayText = &quot;Calculate in Excel\n&quot;;
			learningAidcalculateexcel.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_spreadsheet.gif&quot; , &quot;'&quot; , &quot;;
			learningAidcalculateexcel.enabled = false;
			learningAidcalculateexcel.hasCustomLogic = false;
			learningAidcalculateexcel.visible = false;
			learningAidcalculateexcel.isExerciseSpecific = true;
			learningAidcalculateexcel.exerciseExists = false;
			learningAidcalculateexcel.playerParamName = &quot; , &quot;'&quot; , &quot;showcalculateexcel&quot; , &quot;'&quot; , &quot;;
			//learningAidcalculateexcel.initCapName = learningAidcalculateexcel.name.substring(0, 1).toUpperCase() + learningAidcalculateexcel.name.substring(1);
			learningAidcalculateexcel.typeID = 27;

			learningAids.push(learningAidcalculateexcel);


			function ViewSample_calculateexcel()
			{
				popupWindow(&quot;calculateexcel&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_mal_shared/spreadsheet/scratchsheet.xls&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidmorehelp = new Object();
			learningAidmorehelp.name = &quot; , &quot;'&quot; , &quot;morehelp&quot; , &quot;'&quot; , &quot;;
			learningAidmorehelp.displayText = &quot;SBMore Help&quot;;
			learningAidmorehelp.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_morehelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidmorehelp.enabled = false;
			learningAidmorehelp.hasCustomLogic = false;
			learningAidmorehelp.visible = false;
			learningAidmorehelp.isExerciseSpecific = false;
			learningAidmorehelp.exerciseExists = false;
			learningAidmorehelp.playerParamName = &quot; , &quot;'&quot; , &quot;showmorehelp&quot; , &quot;'&quot; , &quot;;
			//learningAidmorehelp.initCapName = learningAidmorehelp.name.substring(0, 1).toUpperCase() + learningAidmorehelp.name.substring(1);
			learningAidmorehelp.typeID = 29;

			learningAids.push(learningAidmorehelp);


			function ViewSample_morehelp()
			{
				popupWindow(&quot;morehelp&quot;, &quot;/support/learningaids/morehelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtemplates = new Object();
			learningAidtemplates.name = &quot; , &quot;'&quot; , &quot;templates&quot; , &quot;'&quot; , &quot;;
			learningAidtemplates.displayText = &quot;Templates&quot;;
			learningAidtemplates.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_templates.png&quot; , &quot;'&quot; , &quot;;
			learningAidtemplates.enabled = false;
			learningAidtemplates.hasCustomLogic = false;
			learningAidtemplates.visible = false;
			learningAidtemplates.isExerciseSpecific = true;
			learningAidtemplates.exerciseExists = false;
			learningAidtemplates.playerParamName = &quot; , &quot;'&quot; , &quot;showtemplates&quot; , &quot;'&quot; , &quot;;
			//learningAidtemplates.initCapName = learningAidtemplates.name.substring(0, 1).toUpperCase() + learningAidtemplates.name.substring(1);
			learningAidtemplates.typeID = 30;

			learningAids.push(learningAidtemplates);


			function ViewSample_templates()
			{
				popupWindow(&quot;templates&quot;, &quot;/support/learningaids/template.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidbillofrights = new Object();
			learningAidbillofrights.name = &quot; , &quot;'&quot; , &quot;billofrights&quot; , &quot;'&quot; , &quot;;
			learningAidbillofrights.displayText = &quot;Bill of Rights&quot;;
			learningAidbillofrights.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_billofrights.png&quot; , &quot;'&quot; , &quot;;
			learningAidbillofrights.enabled = false;
			learningAidbillofrights.hasCustomLogic = false;
			learningAidbillofrights.visible = false;
			learningAidbillofrights.isExerciseSpecific = true;
			learningAidbillofrights.exerciseExists = false;
			learningAidbillofrights.playerParamName = &quot; , &quot;'&quot; , &quot;showbillofrights&quot; , &quot;'&quot; , &quot;;
			//learningAidbillofrights.initCapName = learningAidbillofrights.name.substring(0, 1).toUpperCase() + learningAidbillofrights.name.substring(1);
			learningAidbillofrights.typeID = 34;

			learningAids.push(learningAidbillofrights);


			function ViewSample_billofrights()
			{
				popupWindow(&quot;billofrights&quot;, &quot;http://media.pearsoncmg.com/ph/chet/chet_criminal_justice_1/resources/billofrights.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidusconstitution = new Object();
			learningAidusconstitution.name = &quot; , &quot;'&quot; , &quot;usconstitution&quot; , &quot;'&quot; , &quot;;
			learningAidusconstitution.displayText = &quot;U.S. Constitution&quot;;
			learningAidusconstitution.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_usconstitution.png&quot; , &quot;'&quot; , &quot;;
			learningAidusconstitution.enabled = false;
			learningAidusconstitution.hasCustomLogic = false;
			learningAidusconstitution.visible = false;
			learningAidusconstitution.isExerciseSpecific = true;
			learningAidusconstitution.exerciseExists = false;
			learningAidusconstitution.playerParamName = &quot; , &quot;'&quot; , &quot;showusconstitution&quot; , &quot;'&quot; , &quot;;
			//learningAidusconstitution.initCapName = learningAidusconstitution.name.substring(0, 1).toUpperCase() + learningAidusconstitution.name.substring(1);
			learningAidusconstitution.typeID = 35;

			learningAids.push(learningAidusconstitution);


			function ViewSample_usconstitution()
			{
				popupWindow(&quot;usconstitution&quot;, &quot;http://media.pearsoncmg.com/ph/chet/chet_criminal_justice_1/resources/constitution.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidimage = new Object();
			learningAidimage.name = &quot; , &quot;'&quot; , &quot;image&quot; , &quot;'&quot; , &quot;;
			learningAidimage.displayText = &quot;Image&quot;;
			learningAidimage.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_textbookextras.png&quot; , &quot;'&quot; , &quot;;
			learningAidimage.enabled = false;
			learningAidimage.hasCustomLogic = false;
			learningAidimage.visible = false;
			learningAidimage.isExerciseSpecific = true;
			learningAidimage.exerciseExists = false;
			learningAidimage.playerParamName = &quot; , &quot;'&quot; , &quot;showimage&quot; , &quot;'&quot; , &quot;;
			//learningAidimage.initCapName = learningAidimage.name.substring(0, 1).toUpperCase() + learningAidimage.name.substring(1);
			learningAidimage.typeID = 36;

			learningAids.push(learningAidimage);


			function ViewSample_image()
			{
				popupWindow(&quot;image&quot;, &quot;/support/learningaids/textbookextras.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidguidedlecture = new Object();
			learningAidguidedlecture.name = &quot; , &quot;'&quot; , &quot;guidedlecture&quot; , &quot;'&quot; , &quot;;
			learningAidguidedlecture.displayText = &quot;Guided Lecture&quot;;
			learningAidguidedlecture.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_powerpoint.png&quot; , &quot;'&quot; , &quot;;
			learningAidguidedlecture.enabled = false;
			learningAidguidedlecture.hasCustomLogic = false;
			learningAidguidedlecture.visible = false;
			learningAidguidedlecture.isExerciseSpecific = true;
			learningAidguidedlecture.exerciseExists = false;
			learningAidguidedlecture.playerParamName = &quot; , &quot;'&quot; , &quot;showguidedlecture&quot; , &quot;'&quot; , &quot;;
			//learningAidguidedlecture.initCapName = learningAidguidedlecture.name.substring(0, 1).toUpperCase() + learningAidguidedlecture.name.substring(1);
			learningAidguidedlecture.typeID = 37;

			learningAids.push(learningAidguidedlecture);


			function ViewSample_guidedlecture()
			{
				popupWindow(&quot;guidedlecture&quot;, &quot;/support/learningaids/powerpoint.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidflashcards = new Object();
			learningAidflashcards.name = &quot; , &quot;'&quot; , &quot;flashcards&quot; , &quot;'&quot; , &quot;;
			learningAidflashcards.displayText = &quot;Flashcards&quot;;
			learningAidflashcards.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_review.png&quot; , &quot;'&quot; , &quot;;
			learningAidflashcards.enabled = false;
			learningAidflashcards.hasCustomLogic = false;
			learningAidflashcards.visible = false;
			learningAidflashcards.isExerciseSpecific = true;
			learningAidflashcards.exerciseExists = false;
			learningAidflashcards.playerParamName = &quot; , &quot;'&quot; , &quot;showflashcards&quot; , &quot;'&quot; , &quot;;
			//learningAidflashcards.initCapName = learningAidflashcards.name.substring(0, 1).toUpperCase() + learningAidflashcards.name.substring(1);
			learningAidflashcards.typeID = 38;

			learningAids.push(learningAidflashcards);


			function ViewSample_flashcards()
			{
				popupWindow(&quot;flashcards&quot;, &quot;/support/learningaids/reviewinplayer.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidlabelingactivity = new Object();
			learningAidlabelingactivity.name = &quot; , &quot;'&quot; , &quot;labelingactivity&quot; , &quot;'&quot; , &quot;;
			learningAidlabelingactivity.displayText = &quot;Labeling Activity&quot;;
			learningAidlabelingactivity.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_morehelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidlabelingactivity.enabled = false;
			learningAidlabelingactivity.hasCustomLogic = false;
			learningAidlabelingactivity.visible = false;
			learningAidlabelingactivity.isExerciseSpecific = true;
			learningAidlabelingactivity.exerciseExists = false;
			learningAidlabelingactivity.playerParamName = &quot; , &quot;'&quot; , &quot;showlabelingactivity&quot; , &quot;'&quot; , &quot;;
			//learningAidlabelingactivity.initCapName = learningAidlabelingactivity.name.substring(0, 1).toUpperCase() + learningAidlabelingactivity.name.substring(1);
			learningAidlabelingactivity.typeID = 39;

			learningAids.push(learningAidlabelingactivity);


			function ViewSample_labelingactivity()
			{
				popupWindow(&quot;labelingactivity&quot;, &quot;/support/learningaids/morehelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	




	var learningAidTypeMap = new Object();
	for (var i = 0; i &lt; learningAids.length; i++) {
		learningAidTypeMap[learningAids[i].typeID] = learningAids[i];
	}

	function GetLearningAidsArray() {
		return learningAids;
	}

	function GetLearningAidByTypeId(typeId) {
		return learningAidTypeMap[typeId];
	}

	function GetLearningAidRowId(typeId) {
		return rowId[typeId]; //
			}

			function GetLearningAidCheckBoxId(typeId) {
				return cbId[typeId]; //
			}

			function IsLearningAidRowHidden(typeId) {
				var rowId = GetLearningAidRowId(typeId);
				var row = document.getElementById(rowId);
				return row.style.display == &quot;none&quot;;
			}

			function GetLearningAidCheckBox(typeId) {
				if (typeof(cbId[typeId]) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; &amp;&amp; cbId[typeId] != null)
					return null;

				return document.getElementById(cbId[typeId]);
			}


			function UpdateLearningAids() {

				var LearningAidsListLabel = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel&quot; , &quot;'&quot; , &quot;);

		// update text list of learning aids in Change Settings page view


		var foundExerciseSpecificAid = false;
		var foundVisibleAid = false;

		for (var j = 0; j &lt; learningAids.length; j++)
			if (learningAids[j].visible &amp;&amp; !learningAids[j].isExerciseSpecific)
				foundVisibleAid = true;

		var ulSel = document.getElementById(&quot;ulSelected&quot;);
		if (ulSel)
		{
			/// Reset existing exercise flags
			for (var j = 0; j &lt; learningAids.length; j++)
				learningAids[j].exerciseExists = false;


			for (var i = 0; i &lt; ulSel.childNodes.length; i++)
			{
				var selectedExercise = getSelectedExercise(ulSel.childNodes[i].id);
				if (selectedExercise.questionType != QuestionTypes.Question)
					continue; // skip questions from other books, custom questions, and media questions

				// CA The following loop checks for visible and aids tied to an exercise, setting flags
				// as appropriate. The flags are later verified to construct the list of aids on the
				// Settings page.

				for (var j = 0; j &lt; learningAids.length; j++)
					if (learningAids[j].visible)
					{
						// CA for exercise-specific learning aids, check to see if the selected exercise has the aid
						if (learningAids[j].isExerciseSpecific &amp;&amp; !learningAids[j].exerciseExists)
						{
							var hasAid = eval(&quot; , &quot;'&quot; , &quot;selectedExercise.hasLearningAidType(&quot; , &quot;'&quot; , &quot; + learningAids[j].typeID + &quot; , &quot;'&quot; , &quot;)&quot; , &quot;'&quot; , &quot;);

							if (hasAid)
							{
								learningAids[j].exerciseExists = true;
								foundExerciseSpecificAid = true;
							}

						}

						foundVisibleAid = true;
					}
			}
		}

		/// CA If no aids are found, display the None heading and error out
		if (!foundVisibleAid &amp;&amp; !foundExerciseSpecificAid)
		{
			handleNoLearningAids();
			return false;
		}


		// CA For the display string on the settings page, in order for an aid to appear
		// it must be visible according to course settings, enabled, and either:
		// - not exercise specific
		// - exercise specific and there exists an exercise associated with the learning aid
		var learningAidsDisplayString = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;


		for(var i = 0; i &lt; learningAids.length; i++)
			if (learningAids[i].visible &amp;&amp; learningAids[i].enabled
				&amp;&amp; (!learningAids[i].isExerciseSpecific || (learningAids[i].isExerciseSpecific &amp;&amp; learningAids[i].exerciseExists)))
			{
				if (learningAidsDisplayString == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
					learningAidsDisplayString = learningAids[i].displayText;
				else
					learningAidsDisplayString += &quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot; + learningAids[i].displayText;
			}

		if (learningAidsDisplayString == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
			handleNoLearningAids();
		else
			handleExistingLearningAids(learningAidsDisplayString);

		var hasDisplayedRow = false;

		// CA show/hide corresponding table rows
		for(var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var tableRowClientID = eval(&quot; , &quot;'&quot; , &quot;LATableRowID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var tableRow = document.getElementById(tableRowClientID);
				var checkBoxClientID = eval(&quot; , &quot;'&quot; , &quot;LACheckBoxID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				// If the learning aid is enabled, ensure that the corresponding box is checked
				if (learningAids[i].enabled)
					checkBox.checked = true;
				else
					checkBox.checked = false;

				// The table row is displayed as per the conditions above, except that both enabled
				// and disabled exercises are displayed.
				if (!learningAids[i].isExerciseSpecific || (learningAids[i].isExerciseSpecific &amp;&amp; learningAids[i].exerciseExists))
				{
					tableRow.style.display = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
					hasDisplayedRow = true;
				}
				else
					tableRow.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;
			}
        }

			    // CA if there are no displayed rows, we must hide the Change link
			    var learningAidsChangeSpan = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsChangeSpan&quot; , &quot;'&quot; , &quot;);
			    if (hasDisplayedRow)
			        learningAidsChangeSpan.style.display = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
			    else
					learningAidsChangeSpan.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;

				UpdateReviewModeCheckboxText();
		
	}

    function  HandleFootNoteVisibility(){

     var statCrunchDataItem = learningAids.find(item=> item.name== &quot; , &quot;'&quot; , &quot;statexplore&quot; , &quot;'&quot; , &quot;)

         
    }




    function LearningAidsChangeLink_onclick() {
        HandleFootNoteVisibility();

        if (!isHW) {
            if (isUseLockdownBrowserChecked() &amp;&amp; isLearningAidReviewModeChecked()) {
				
				
                var enabledLearningAids = getEnabledLearningAids();
				var statCrunchEnabled = hasStatCrunchEnabled();
                var isNewGenStatCrunchFeaturesEnabled = &quot;false&quot;;
                if (statCrunchEnabled) {
                    isNewGenStatCrunchFeaturesEnabled = true;
                }
                if (isNewGenStatCrunchFeaturesEnabled) {
                    if (enabledLearningAids.length > 0 &amp;&amp; !statCrunchEnabled) {
                        alert(&quot;Learning Aids cannot be displayed on a test or quiz in LockDown Browser mode. They have been set to show in Review mode only.&quot;);
                    }
                    if (enabledLearningAids.length > 1 &amp;&amp; statCrunchEnabled) {
                        alert(&quot;StatCrunch is the only learning aid that can be displayed on a test or quiz in LockDown Browser mode. All other learning aids have been set to show in Review Mode only.&quot;);
                    }
                }
                else {
                    alert(&quot;Learning Aids cannot be displayed on a test or quiz in LockDown Browser mode. They have been set to show in Review mode only.&quot;);
                }
            }
        }

		// 
		if (learningAidsEnabled()) {
			var ulSel = document.getElementById(&quot;ulSelected&quot;);
			if (ulSel) {
				var hasQuestionLevelSettings = false;
				for (var i = 0; i &lt; ulSel.childNodes.length; i++) {
					var pointsBox = GetPointsBox(ulSel.childNodes[i]);
					var editedCustomLearningAidTypeIdCsv = pointsBox.getAttribute(&quot;customLearningAidTypeIdCsv&quot;);
					if (!QuestionCustomLearningAidsState.usesAssignmentSettings(editedCustomLearningAidTypeIdCsv)) {
						hasQuestionLevelSettings = true;
						break;
					}
				}
				var divQuestionLearningAidsNote = document.getElementById(&quot;DivQuestionLearningAidsNote&quot;);
				divQuestionLearningAidsNote.style.display = hasQuestionLevelSettings ? &quot;&quot; : &quot;none&quot;;
			}
		}
	                
		var learningAidsPopup = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup&quot; , &quot;'&quot; , &quot;);
		var learningAidsIEShim = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsIEShim&quot; , &quot;'&quot; , &quot;);       // needed for IE6, lest drop downs will have precedence over DIV
		if (learningAidsPopup)
		{
			learningAidsPopup.style.display = &quot; , &quot;'&quot; , &quot;block&quot; , &quot;'&quot; , &quot;;
			learningAidsIEShim.style.display = &quot; , &quot;'&quot; , &quot;block&quot; , &quot;'&quot; , &quot;;
		}
		else
			alert(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup not found.&quot; , &quot;'&quot; , &quot;);
	}







	function handleNoLearningAids()
	{
		var LearningAidsListLabel = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel&quot; , &quot;'&quot; , &quot;);
		   changeText(LearningAidsListLabel, LOCNS.LocMap.GetRes(&quot;resources&quot;, &quot;None&quot;));
		   var showInReviewModeOnlyDiv = document.getElementById(&quot;ShowInReviewModeOnlyDiv&quot;);
		   showInReviewModeOnlyDiv.className = &quot; , &quot;'&quot; , &quot;disabled&quot; , &quot;'&quot; , &quot;;

		   var reviewModeCheckbox = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot;);
		if (reviewModeCheckbox)
			reviewModeCheckbox.disabled = true;
	}

	function handleExistingLearningAids(learningAidsDisplayString)
	{
		var LearningAidsListLabel = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel&quot; , &quot;'&quot; , &quot;);
		changeText(LearningAidsListLabel, learningAidsDisplayString);
		
		var showInReviewModeOnlyDiv = document.getElementById(&quot;ShowInReviewModeOnlyDiv&quot;);
		showInReviewModeOnlyDiv.className = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;

		var reviewModeCheckbox = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot;);
		reviewModeCheckbox.disabled = false;
		
	}

	function changeText(labelControl, text)
	{
		var textNode = document.createTextNode(text);

		if (labelControl.childNodes.length == 0)
			labelControl.appendChild(textNode);
		else
			labelControl.replaceChild(textNode, labelControl.childNodes[0]);

	}

	function closeLearningAidsPopup()
	{
		var learningAidsPopup = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup&quot; , &quot;'&quot; , &quot;);
		var learningAidsIEShim = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsIEShim&quot; , &quot;'&quot; , &quot;);       // needed for IE6, lest drop downs will have precedence over DIV
		if (learningAidsPopup)
		{
			learningAidsIEShim.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;
			learningAidsPopup.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;
		}
		else
			alert(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup not found.&quot; , &quot;'&quot; , &quot;);
	}

	function LearningAidsDialogOK_onclick()
	{
		closeLearningAidsPopup();

		// CA Ensure that the textual Learning Aids display on the Change Settings screen is updated
		for (var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var checkBoxClientID = eval(&quot; , &quot;'&quot; , &quot;LACheckBoxID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				learningAids[i].enabled = checkBox.checked;
			}
		}
		UpdateLearningAids();
		UpdateReviewModeCheckboxText();
	}

	// XL-29125: Update ReviewModeCheckbox Text for learning aids
	function UpdateReviewModeCheckboxText() {
        var statCrunchEnabled = hasStatCrunchEnabled();
		var isLDBRequired = $(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_chkUseLockdownBrowser&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;);
		var isNewGenStatCrunchFeaturesEnabled = &quot;false&quot;;
		if (statCrunchEnabled) {
            isNewGenStatCrunchFeaturesEnabled = true;
        }

        if (statCrunchEnabled &amp;&amp; isLDBRequired &amp;&amp; isNewGenStatCrunchFeaturesEnabled) {
            SetReviewModeCheckboxLabelText(&quot;Show StatCrunch in quizzes and tests and all selected learning aids in Review mode&quot;);
		}
		else {
            SetReviewModeCheckboxLabelText(&quot;Show in Review mode only&quot;);
        }
	}

	function SetReviewModeCheckboxLabelText(labelText) {
		$(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot; , &quot;'&quot; , &quot;).next().text(labelText);
    }

	// Check if StatCrunch is enabled in Learning aids options
	function hasStatCrunchEnabled() {
        var isSecondaryFlow = false;
        if (isSecondaryFlow) {
			return true;
		}
		else {
			var enabledLearningAids = getActiveLearningAids();
			return hasStatCrunch(enabledLearningAids);
		}
	}

	// Obtain list of active learning aids
	function getActiveLearningAids() {
		return learningAids.filter(aid => (aid.enabled == true)
            &amp;&amp; (aid.visible) &amp;&amp; (!aid.isExerciseSpecific || (aid.isExerciseSpecific &amp;&amp; aid.exerciseExists)));
    }

	function LearningAidsDialogCancel_onclick()
	{
		closeLearningAidsPopup();

		// CA Restore previous choices prior to entering the dialog
		for(var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var checkBoxClientID = eval(&quot; , &quot;'&quot; , &quot;LACheckBoxID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				checkBox.checked = learningAids[i].enabled;
			}
		}

		UpdateLearningAids();
	}
	function getEnabledLearningAids() {
		return learningAids.filter(aid => aid.enabled == true);
	}
	function hasStatCrunch(learningAidArray) {
		var found = learningAidArray.find(aid => aid.name == &quot; , &quot;'&quot; , &quot;statexplore&quot; , &quot;'&quot; , &quot;);
		return found ? true : false;
    }


rowId = new Object();cbId = new Object();LATableRowIDguidedsolution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[10] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDguidedsolution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[10] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDexample = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[11] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDexample = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[11] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDvideo = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[1] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDvideo = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[1] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDanimation = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[2] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDanimation = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[2] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtextbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[5] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtextbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[5] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDconceptbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[31] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDconceptbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[31] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtextbookextras = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[28] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtextbookextras = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[28] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDmathtools = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[32] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDmathtools = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[32] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDglossary = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[14] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDglossary = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[14] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDreview = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[24] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDreview = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[24] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDstatexplore = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[3] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDstatexplore = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[3] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDgeogebra = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[26] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDgeogebra = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[26] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtechhelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[23] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtechhelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[23] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDextrahelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[25] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDextrahelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[25] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDcalculator = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[8] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDcalculator = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[8] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDaskinstructor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[6] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDaskinstructor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[6] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDconnecttotutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[33] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDconnecttotutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[33] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDfasttrack = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[4] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDfasttrack = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[4] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDdemodoc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[7] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDdemodoc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[7] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDkeyconcepts = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[9] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDkeyconcepts = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[9] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDgrapher = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[12] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDgrapher = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[12] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[13] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[13] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDannuitycalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[15] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDannuitycalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[15] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDschaums = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[16] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDschaums = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[16] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDinstructortip = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[17] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDinstructortip = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[17] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDboosterc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[18] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDboosterc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[18] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDpowerpoint = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[19] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDpowerpoint = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[19] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDfinancialcalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[20] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDfinancialcalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[20] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDomexplorertutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[21] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDomexplorertutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[21] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDeconspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[22] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDeconspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[22] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDcalculateexcel = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[27] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDcalculateexcel = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[27] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDmorehelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[29] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDmorehelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[29] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtemplates = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[30] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtemplates = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[30] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDbillofrights = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[34] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDbillofrights = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[34] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDusconstitution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[35] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDusconstitution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[35] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDimage = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[36] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDimage = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[36] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDguidedlecture = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[37] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDguidedlecture = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[37] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDflashcards = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[38] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDflashcards = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[38] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDlabelingactivity = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[39] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDlabelingactivity = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[39] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;





	
	
		DevTestingCourse
	






    function getHeaderText(){
        return $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        let pageTitle = $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            New Homework  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about New Homework
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    




   
    
    
        
            
                
                    Start
                
                    Select Media and Questions
                
                    Choose Settings
                
                    Define Competency Mapping
            
        
        
    

    
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




	
		
			
				
					Book
					NextGen Media Math Test Book--REAL
				
				
				
	Gradebook Category 
	
						
		Homework

	Homework
					


				
	


				
				
				
					Homework Name
					
				

                

                

				

				

				

				
				

				
	
	Creating assignments for mobile use.    


			
		
	
	
	


	
		
		
			

		

		

				
			    
                
                    
                        Personalization  
                        
                            Options
                        
                        
                        
                        This assignment is personalized for each student using  Skill Builder.
                    
                    
                        
                            
                                
	
                                        
		
                                            Skill Builder
                                        
	
                                    
	
                                        Provide an opportunity for additional targeted practice with   Skill Builder Adaptive Practice.
                                    


                                
	
                                        Personalized Homework
                                    
	
                                        Give automatic credit for questions from sbobjectives mastered in test/quiz:
                                        
		Choose...
		Maintain Order, Preassigned

	
                                         
                                    


                                
                            
                        
                    
                
			    
		
		
		

		

		
		
	
			
				
					This assignment name is already in use in your course, or in one of the members of your course group.Please change it to make it unique.
						
				
			
		

		
		

		
	
		    
		    
		
			    
				    
					    
						    
			
							    
									    
								    
						    
		
					    
				    
			    
		    
	 
            
		

        
            
            The TestGen plug-in is not compatible with the Pearson LockDown Browser.
        
		
		
	
	 





	
	
		
			
				Test Name 
				SampleHW
			
			
				Book 
				NextGen Media Math Test Book--REAL
			
		
		
		
		
			
				
					
						
							Version 3.0 or higher of the TestGen plug-in is required to take this test.
						
						
							Version 3.0 or higher of the TestGen plug-in is required to take this test.
						
						This test will not generate student study plans or item analysis data because it contains one or more questions from a testbank created for another book.
To create tests that generate a study plan or item analysis, download the testbank for this book.
						
						Please click next to continue.
				
			
		
	
		
		
		
		
			Note: You must create or save your test using TestGen version 3.3 or higher.If you are creating tests with the latest version of the TestGen application, you must make sure your students and your lab have installed the latest version of the TestGen plug-in.
			
		
		  
			1. Click Browse to locate and select the TestGen file on your hard drive
			
		
          
               
			We recommend a maximum of 40 questions in your TestGen file.
			
		
		  
			
		
		  
			2. Click Next to upload your TestGen test
		
		
		
	
		
		
		
			
				
					Is your edited test ready to upload?
					
					I have updated my test and I want to upload it now.
					I need to download a copy of the test to make changes to it.
				
				

					
					
						
						Click Browse to locate and select the TestGen file on your hard drive
                        
                         
			            We recommend a maximum of 40 questions in your TestGen file.
			            
		                
						
						
						
						
						
					
					

					
						
						Download the file to your hard drive or preview it using the TestGen Plug-in.
						File:  (0KB)
						Download
						Preview
						
						To upload your edited test, return to this wizard and choose &quot;I have updated my test and I want to upload it now.
						
					
				
			
		
		
	

		
		
			
			1. If you wish to preview the test you are copying or download it to your hard drive, click below.
			File:  (0KB)
			Download
			Preview
			
			2. Click Next to change your test assignment settings or Save to save your copied test
			
		
	

	

	
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }


		
        
    
	

    
        
            
			
		
					
					    
						    Name
						    SampleHW
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
			SBChapter 
			
				1. The Real Number System
				2. Linear Equations and Inequalities in One Variable
				3. Linear Equations and Inequalities in Two Variables; Functions
				4. MarkD Test Chapter
				5. Chapter not in Study Plan
				6. Drag and Drop Questions
				7. Essay questions
				8. StatCrunch
				9. 6-15-21 Adaptive Diagnostic

			
		
		
			SBSection 
			
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions
		
		
			SBObjective 
			
				All SBO

			
		
		

                        
                        
			
                               
                            
		
		
			Availability
			
                                
				All questions
				Questions that are not in the Study Plan
				Questions that are in the Study Plan
				Questions that are screenreader accessible

			
                            
		
		

                        
                        
						
                    
                    																														
                
	
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                             Show GL questions
                                            
                                        
                                             Show Geogebra LTI questions
                                            
                                        
                                             Show Sylva questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   
		
					Questions
		Media
		
					
					
			
	
	

		
			
			
				
					
						
						
						
						Available Questions
						 
						(210)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 7My Selections (7)
							
							
							    
							
								View Assignment Details
								
							
							
							
					
					
						
							
							Points: 7

							
							
							#
							

							
								
									
									
								Question ID / Media

								
									
								
							
							
								
									SBSection / Book Association
									
								
								 
							Estimated time:189m 38s
							
							
					
				
			
	
		
		
			
			
				
					+ (1.1) CustomQuestion1TechH, Rev, Extra Help (preloaded)1.1.29 (preloaded)Anim (preloaded)(P) Anim(P) Anim, eText 3(P) 1.1.39(P) Video, eText 1(P) Video, eText 2(P) 1.1.45(P) 1.1.47(P) 1.1.51(P) 1.1.53(P) 1.1.55(P) 1.1.59(P) Video, Anim, eText 1(P) 1.1.79(P) 1.1.100 RS file testing(P) 1.1.101 RS file testing(P) 1.1.102 RS file testing(P) 1.1.103 RS file testing missing RS(P) 1.1.104 RS file testing - broken vars(P) Graph review only(P) Review only(P) RTD Review only(P) Data Table question(P) Text and Mobile(P) Text only(P) Mobile only(P) No text, no mobile(P) 1.2.17(P) 1.2.19(P) 1.2.23(P) 1.2.25(P) 1.2.27(P) 1.2.31(P) 1.2.33(P) 1.2.35(P) 1.2.37(P) 1.2.41(P) 1.2.43(P) 1.2.47(P) 1.2.51(P) 1.2.55(P) 1.2.57(P) 1.2.61(P) 1.2.63(P) 1.2.67(P) 1.2.71(P) 1.2.79(P) 1.2.81(P) 1.3.13(P) 1.3.15(P) 1.3.17(P) 1.3.19(P) 1.3.23(P) 1.3.25(P) 1.3.29(P) 1.3.31(P) 1.3.33(P) 1.3.35(P) 1.3.37(P) 1.3.39(P) 1.3.43(P) 1.3.45(P) 1.3.47(P) 1.3.49(P) 1.3.51(P) 1.3.53(P) 1.3.59(P) 1.3.61(P) 1.3.63(P) 1.3.65(P) 1.3.73(P) 1.3.75(P) 1.3.77(P) 1.4.1(P) 1.4.5(P) 1.4.19(P) 1.4.23(P) 1.4.25(P) 1.4.27(P) 1.4.31(P) 1.4.33(P) 1.4.35(P) 1.4.37(P) 1.4.39(P) 1.4.41(P) 1.4.43(P) 1.4.45(P) 1.4.47(P) 1.4.49(P) 1.4.51(P) 1.4.53(P) 1.4.55(P) 1.4.57(P) 1.4.59(P) 1.4.61(P) 1.4.63(P) 1.4.65(P) 1.4.67(P) 1.5.7(P) 1.5.11(P) 1.5.19(P) 1.5.23(P) 1.5.31(P) 1.5.33(P) 1.5.41(P) 1.5.45(P) 1.5.47(P) 1.5.49(P) 1.5.51(P) 1.5.53(P) 1.5.55(P) 1.5.59(P) 1.5.75(P) 1.5.77(P) 1.5.83(P) 1.5.85(P) 1.5.91(P) 1.5.93(P) 1.5.95(P) 1.5.103(P) 1.5.111(P) 1.5.113(P) 1.5.119(P) 1.6.13(P) 1.6.15(P) 1.6.21(P) 1.6.25(P) 1.6.27(P) 1.6.29(P) 1.6.35(P) 1.6.39(P) 1.6.41(P) 1.6.43(P) 1.6.45(P) 1.6.49(P) 1.6.55(P) 1.6.57(P) 1.6.63(P) 1.6.67(P) 1.6.69(P) 1.6.75(P) 1.6.77(P) 1.6.79(P) 1.6.81(P) 1.6.83(P) 1.6.85(P) 1.6.87(P) 1.6.89(P) 1.6.93(P) 1.6.95(P) 1.6.97(P) 1.6.103(P) 1.6.105(P) 1.6.109(P) 1.7.1(P) 1.7.5(P) 1.7.9(P) 1.7.15(P) 1.7.17(P) 1.7.19(P) 1.7.21(P) 1.7.23(P) 1.7.25(P) 1.7.27(P) 1.7.29(P) 1.7.39(P) 1.7.43(P) 1.7.47(P) 1.7.55(P) 1.7.57(P) 1.7.59(P) 1.7.61(P) 1.7.63(P) 1.7.65(P) 1.7.67(P) 1.7.69(P) 1.7.73(P) 1.7.75(P) 1.7.77(P) 1.7.79(P) 1.8.1(P) 1.8.3(P) 1.8.5(P) 1.8.9(P) 1.8.11(P) 1.8.13(P) 1.8.15(P) 1.8.17(P) 1.8.19(P) 1.8.21(P) 1.8.23(P) 1.8.25(P) 1.8.27(P) 1.8.29(P) 1.8.31(P) 1.8.33(P) 1.8.45(P) 1.8.47(P) 1.8.49(P) 1.8.53(P) 1.8.55(P) 1.8.67(P) 1.8.69(P) 1.8.71Test GLGeogebra Q1Geogebra Q2 - always correct
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					1(P) 1.1.39Multiply and divide fractions.11m 42s2(P) 1.1.45Multiply and divide fractions.46m 32s3(P) 1.1.47Multiply and divide fractions.22m 16s4(P) 1.1.51Add and subtract fractions.22m 59s5(P) 1.1.53Add and subtract fractions.15m 18s6(P) 1.1.55Add and subtract fractions.18m 8s7(P) 1.1.59Add and subtract fractions.52m 43s
					
					
						
							Click Questions to select questions and click Media to select media. Click Add to include selected questions or media in this assignment.
						
						Click the checkbox next to individual Media file to select them.
Then click Add to include that media in this assignment.
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student homework
		
							View student practice set
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	+ (1.1) CustomQuestion1 (Add and subtract fractions.)
	




	
		
		Name
		SampleHW
		

		
		Book
		
			
				NextGen Media Math Test Book--REAL
				Review Individual Student Settings
			
			
		
		

		
			Availability Options
		
		
			
			
				
					
					
						Available
						
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						Calendar
					
						
							
								
									Title and navigation
								
									
										
									
								
	
		&lt;&lt;&lt;June 2022>>>
	

							
						
					
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

				
			
		
	

							    
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						
							Time picker
						
							[x]
						
							12:00 AM1:00 AM2:00 AM
						
							3:00 AM4:00 AM5:00 AM
						
							6:00 AM7:00 AM8:00 AM
						
							9:00 AM10:00 AM11:00 AM
						
							12:00 PM1:00 PM2:00 PM
						
							3:00 PM4:00 PM5:00 PM
						
							6:00 PM7:00 PM8:00 PM
						
							9:00 PM10:00 PM11:00 PM
						
					
				
			
		
	

							
						
						
							Current course time:
							12:30pm
							
								Time zone:
								(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi
								
	Change...

							
						
					
					
					
						Due
						
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						Calendar
					
						
							
								
									Title and navigation
								
									
										
									
								
	
		&lt;&lt;&lt;June 2022>>>
	

							
						
					
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

				
			
		
	

							    
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						
							Time picker
						
							[x]
						
							12:00 AM1:00 AM2:00 AM
						
							3:00 AM4:00 AM5:00 AM
						
							6:00 AM7:00 AM8:00 AM
						
							9:00 AM10:00 AM11:00 AM
						
							12:00 PM1:00 PM2:00 PM
						
							3:00 PM4:00 PM5:00 PM
						
							6:00 PM7:00 PM8:00 PM
						
							9:00 PM10:00 PM11:00 PM
						
					
				
			
		
	

							
						
					
				
			
		
		
		
		

			
			SBChapter Associations
			
				
	
					Display with assignments from sbchapters:  Change...
					Note: This assignment covers material from sbchapters 1
					
						
							
							Select SBChapters:
							
								

								1. The Real Number System2. Linear Equations and Inequalities in One Variable3. Linear Equations and Inequalities in Two Variables; Functions4. MarkD Test Chapter5. Chapter not in Study Plan6. Drag and Drop Questions7. Essay questions8. StatCrunch9. 6-15-21 Adaptive Diagnostic
							
							
								
									Cancel
	
									OK
	
								
							
						
						
					
				

				
			
		


		
	Scoring Options



		
		
	
	Late submissions
	Allow students to work and change score after due date 
				
					
		
					Require password 
					
					
	
	

					
		
					Require final submission 
						
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;June 2022>>>
	

									
								
							
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

						
					
				
			
		    
						
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								
									Time picker
								
									[x]
								
									12:00 AM1:00 AM2:00 AM
								
									3:00 AM4:00 AM5:00 AM
								
									6:00 AM7:00 AM8:00 AM
								
									9:00 AM10:00 AM11:00 AM
								
									12:00 PM1:00 PM2:00 PM
								
									3:00 PM4:00 PM5:00 PM
								
									6:00 PM7:00 PM8:00 PM
								
									9:00 PM10:00 PM11:00 PM
								
							
						
					
				
			
		
					
	
	

					
		
					Deduct late submission penalty
						
			From final score
			Per day, from final score

		  
						Penalty
						
						
			%
			Points

		
					
	
	

					
		
							Apply only to questions scored after the due date
						
	
	

				
			


		
		
	
	Partial Credit
	Allow partial credit on questions with multiple parts 



		
			
			
			
			*** DISCONTINUED
		

		
		
	
	Show Work Questions
	
				
					
						Credit for Question:
					
					
						Automatically score question
						%
						
					
					
						Manually score question
					
					
						Credit for Show Work:
					
					
						Automatically score Show Work
						%
						
					
					
						Manually score Show Work
					
					
						Allow students to access SBHelp Me Solve This and SBView an Example 
					
				
			




		
			Access Controls
		

		
	    
	
	LockDown Browser and Proctoring
	
	            
		Do not use LockDown Browser or automated proctoring
		Require LockDown Browser only (no proctoring)
		Require ProctorU Record+

	
	        

        
        

		
	
	
                LockDown Browser
			
	
				
					
						
							Require the Pearson LockDown Browser
						
						    
		Do not use LockDown Browser or automated proctoring
		Require LockDown Browser only (no proctoring)
		Require LockDown Browser with Respondus Monitor Proctoring

	
                        
					
					
		
						    (Warning: The Pearson LockDown Browser is not compatible with mobile devices or Chromebooks. Tests or quizzes requiring the Pearson Lockdown Browser must be taken on desktop or laptop computers.)
						
	
	
					
		
							Do not allow use of calculator
						
	
	
					
		
							Allow use of basic calculator
						
	
	
					
		
							Allow use of scientific calculator
						
	
	
                    
				    
				        
				            You can edit Respondus Monitor Proctoring settings after saving this assignment.
				        
				    
                    
				    
				        
				            
				                Warning:
				                To enable Respondus Monitor Proctoring, go to Instructor Home to complete the license setup.
				            
				        
                    
				
			


        
	    
	    
	
	
	            Automated Proctoring
	        
	
                
                    
                        
                            Require ProctorU Record+
                        
                    
                    
                        
                            (Warning: Tests or quizzes requiring ProctorU Record+ must be taken on desktop or laptop computers using the Chrome browser.)
                        
                    
                    
                        
                            Sensitivity Settings: 
                            Customize ProctorU Settings
                            
                            
                        
                    
                

            



		
		
	
	IP Address Range
	
				Students must access this assignment from the following IP address range:
				
					  
					Change...
					
					Allow students to review this assignment from any IP address
				
			



		
		
			
			Password
			Required password  
		

		
		
			
			Prerequisites
			
				
					None
						Change...
				
			
		

		
		

		
		
	
	Attempts per Assignment
	Limit number of attempts to  



		
		
	
	Number of AttemptsPer Problem
	
				One Attempt (with 1 retry)
				Unlimited Attempts
			


		
		
	
	Attempts per question
	
				Limit number of times students can work each question to
				
			


		
		

		
		
			
			Incomplete Attempt
			
				
				
					
						
						
					
				
				
				
				

				
				
					
						If attempt is interrupted, students may re-access and complete on their own
						If attempt is interrupted, instructors must enable access
					
				
				
				
				

				
				
					
						Restricted Access: Student cannot resume an incomplete attempt until the instructor enables access to it.
						Blocked Access: Student cannot access other assignments or questions while taking a homework.  If the student does not submit the homework, access to all questions and assignments is blocked until the instructor enables access to or deletes the incomplete homework.
					
				
				
				
				
			
		

		
		
	
	Media Access
	
				Require students to work media before answering questions
			



		
		
	Presentation Options



		
		
	
	Review Steps (test)
	
				This is a test of the alternate text specified in SiteBuilder for Review Steps
			




		
		
	
	Lock Correct Answers
	
				Indicate and lock all correct answers after each Check Answer
			



		
		
	
	Question variation
	
				Use the same question values for all students
			



		
		
	
	Save Values
	
				Save question values and student answers
			



		
		
	
	Printing
	
				Allow students to print this homework assignment
			



		
		
	
	Time Limit
	
				Test time allowed (minutes): 
				
		
					
					     Show time remaining during test
				
	
			


		
		
	
	Question display
	
				Scramble question order for each student 
				
		
				Shuffle answer options 
				Randomize variables 
				
	
			


		
	
	Learning Aids
	
				
					
					
						
							
								
									Select Learning Aids:

									
										
											
													
		
															
															
														
		
		SBHelp Me Solve This
	
	
												
													
		
															
															
														
		
		SBView an Example
	
	
												
													
		
															
															
														
		
		SBVideoooo
	
	
												
													
		
															
															
														
		
		SBAnimation
	
	
												
													
		
															
															
														
		
		SBTextbook
	
	
												
													
		
															
															
														
		
		Concept Review
	
	
												
													
		
															
															
														
		
		SBTextbook Extra
	
	
												
													
		
															
															
														
		
		Math Tools
	
	
												
													
		
															
															
														
		
		SBGlossary
	
	
												
													
		
															
															
														
		
		SBReview
	
	
												
													
		
															
															
														
		
		StatCrunch
	
	
												
													
		
															
															
														
		
		Geogebra

	
	
												
													
		
															
															
														
		
		Tech Help
	
	
												
													
		
															
															
														
		
		SBExtra Help
	
	
												
													
												
													
		
															
															
														
		
		Ask My Instructor
	
	
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
		
															
															
														
		
		Instructor Tip
	
	
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
										
									

									
										Note: Instructor has customized learning aids for select questions. Changes made here will not affect these questions.
									
									
									
									
										
											Cancel
	
											  OK
	
										
									
								
							
						
					

				

			    SBHelp Me Solve This, SBView an Example, SBAnimation, Concept Review, Math Tools, Ask My Instructor, Instructor Tip      
			    
			        Change....
			    
				
			    
				
					     Show in Review mode only
				
			



	
	Graphing
	
				Allow students to move points by typing coordinates
			



	
		
			Review Options
		

		
		
			
			Results Display
			
				
	Test Summary shows test score and question results
	Test Summary shows test score only
	Hide score and question results


				
			
		

		
		
			
			Reviewing Test
			
			
				
	Student can review assignment any time after submitting
	Student can review assignment only immediately after submitting
	Student can review submitted assignment any time after due date
	Student can never review submitted assignment


			
		

		
		
			
			Feedback
			Allow students to view feedback while reviewing
		

		
		
	
	Study Plan
	Results are used to update the study plan
				
					
					     Link to mastered and needs study topics
				
			



		
		
			
			Answer Display
			
				Only show student&quot; , &quot;'&quot; , &quot;s answers when reviewing
				Show student&quot; , &quot;'&quot; , &quot;s and correct answers when reviewing
			
		

		 
		
	
	Print
	Allow students to print the test with the correct answers and their answers while reviewing 



		
		
	Learning Path


		
		
	
	Mastery Score
	
				Minimum score for mastery
				%

			



		
		
	Other


		
		
	
	Importing
	Allow other instructors to import this assignment 


	    
	
   

	
	
		
		
		
	    
			
				
					Edit Test Summary Message
				
				
					Customize the message given to students at the end of the test.
					
					
						
						


					
				
				
					Cancel
					Ok
				
			
		
	
	



	
		
			NameSampleHW
			BookNextGen Media Math Test Book--REAL
		
	
	

	
		
			Select criteria for setting content to Needs Study.   If you wish to remove content from the student&quot; , &quot;'&quot; , &quot;s Learning Path but keep it available in Extra Practice, choose Hide from Learning Path. If a content area is not covered in your course, you can leave its criteria blank.
			
				
					
						
						Module
						Topic
						Covered on Test
						Criteria for setting to  Needs Study
						Score
					
					
						 
						 
						 
						 
						


						

   %
					
					
						 
						 
						 
						 
						Apply to Selected

						Apply to Selected

					
				
			
		
	

	
		
				Cancel

			    
					Next

					
				
			    
			        
						Save &amp; Assign

			        
			    
				
					Save

				
			    Back

				
			
		
	
	
	
		

		
			
			
				Cancel

				No

				Yes

			
			
				No, Just Save

				Yes, Take Me There

			
				
			
		
	
	
	
		
		DEV: 404 Save
		
	






    var YesNoCancelPop = {
        _ActionHandler: null,
        Show: function (header, msg, actionHandler, options) {
            //Register popup events
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnYesId&quot;, function () { YesNoCancelPop.DoAction(&quot; , &quot;'&quot; , &quot;Yes&quot; , &quot;'&quot; , &quot;); });
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnNoId&quot;, function () { YesNoCancelPop.DoAction(&quot; , &quot;'&quot; , &quot;No&quot; , &quot;'&quot; , &quot;); });
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnCancelId&quot;, function () { YesNoCancelPop.DoAction(&quot; , &quot;'&quot; , &quot;Cancel&quot; , &quot;'&quot; , &quot;); });
            //Show popup and apply header,body and button text
            $(&quot; , &quot;'&quot; , &quot;#mainModal&quot; , &quot;'&quot; , &quot;).modal(&quot; , &quot;'&quot; , &quot;show&quot; , &quot;'&quot; , &quot;);
            $(&quot; , &quot;'&quot; , &quot;#headerTxt&quot; , &quot;'&quot; , &quot;).html(header);
            $(&quot; , &quot;'&quot; , &quot;#bodyTxt&quot; , &quot;'&quot; , &quot;).html(msg);
            $(&quot; , &quot;'&quot; , &quot;#btnYesId&quot; , &quot;'&quot; , &quot;).html(options.yesText);
            $(&quot; , &quot;'&quot; , &quot;#btnNoId&quot; , &quot;'&quot; , &quot;).html(options.noText);
            $(&quot; , &quot;'&quot; , &quot;#btnCancelId&quot; , &quot;'&quot; , &quot;).html(options.cancelText);
            //Register parent function in popup side
            if (actionHandler != null)
                YesNoCancelPop.SetActionHandler(actionHandler);

            //Show popup buttons 
            YesNoCancelPop.ShowButtons(options);  
            YesNoCancelPop.CustomStyles(options);
        },
        SetActionHandler: function (actionHandler) {
            YesNoCancelPop._ActionHandler = actionHandler;
        },
        //Get popup (yes/no/cancel) event and pass it to parent
        DoAction: function (action) {
            $(&quot; , &quot;'&quot; , &quot;#mainModal&quot; , &quot;'&quot; , &quot;).modal(&quot; , &quot;'&quot; , &quot;hide&quot; , &quot;'&quot; , &quot;);
            if (YesNoCancelPop._ActionHandler)
                YesNoCancelPop._ActionHandler(action);
        },
        ShowButtons: function (options) {
            options.showYesButton ? $(&quot;#btnYesId&quot;).show() : $(&quot;#btnYesId&quot;).hide();
            options.showNoButton ? $(&quot;#btnNoId&quot;).show() : $(&quot;#btnNoId&quot;).hide();
            options.showCancelButton ? $(&quot;#btnCancelId&quot;).show() : $(&quot;#btnCancelId&quot;).hide();
        return true;
        }, CustomStyles: function (options) {
            if (options.width) {
                $(&quot;#innerModel&quot;).width(options.width);
                $(&quot;#popup-hr-bottom&quot;).width(options.width - 40);
                $(&quot;#popup-hr-top&quot;).width(options.width - 40);
            }
            if (options.hideTitle) {
                options.hideTitle ? $(&quot;#headerTxt&quot;).hide() : $(&quot;#headerTxt&quot;).show();
                options.hideTitle ? $(&quot;#popup-hr-top&quot;).hide() : $(&quot;#popup-hr-top&quot;).show();
            }

            if (options.showYesButton || options.showNoButton) {
                $(&quot;#btnGroupCancel&quot;).css(&quot;margin-left&quot;, &quot;10px&quot;);
            }

            if (options.btnYesNo &amp;&amp; options.btnYesNo.alignRight) {
                $(&quot;#btnGroupYesNo&quot;).css(&quot;float&quot;, &quot;right&quot;);
                $(&quot;#btnGroupCancel&quot;).css(&quot;margin-left&quot;, &quot;0&quot;);
            }

            if (options.btnCancel &amp;&amp; options.btnCancel.alignRight){
                $(&quot;#btnGroupCancel&quot;).css(&quot;float&quot;, &quot;right&quot;);
            }

            if (options.paddingTop)
                $(&quot;#modalText&quot;).css(&quot;padding-top&quot;, options.paddingTop);

            if (options.paddingBottom)
                $(&quot;#modalText&quot;).css(&quot;padding-bottom&quot;, options.paddingBottom);

            if (options.btnYesGray)
                $(&quot;#btnYesId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);
            if (options.btnNoGray)
                $(&quot;#btnNoId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);
            if (options.btnCancelGray)
                $(&quot;#btnCancelId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);

            if (options.btnYesPrimary)
                $(&quot;#btnYesId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            if (options.btnNoPrimary)
                $(&quot;#btnNoId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            if (options.btnCancelPrimary)
                $(&quot;#btnCancelId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            
        return true;
        }

        
    }



            
            
                
                
                    
                        
                            
                        
                        
                        
                            
                        
                        
                        
                            
                                
                                    
                                    
                                
                                
		                            
                                
                            
                        
                          
                
            
 

    



                                                                    
                                                            &quot;) or . = concat(&quot;
                                                                    
                                                                

                                                                
                                                                    
                                                                 
                                                                    
	                                                                   
                                                                        
                                                                        
                                                                        
                                                                 
                                                                        
                                                                    

                                                                  

                                                                

                                                                
	
		
                                                                            
                                                                            
                                                                                
                                                                                    Prabhashi ins1_cert
                                                                                    DevTestingCourse
                                                                                
                                                                                
                                                                                    Prabhashi ins1_cert
                                                                                    NextGen Media Math Test Book--REAL
                                                                                
                                                                                
                                                                                    06/03/22
                                                                                    12:30pm
                                                                                
                                                                            
                                                                            
                                                                            
                                                                        
	


                                                                
                                                                    
                                                                    
                                                                
                                                                
                                                                
	
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [&quot; , &quot;'&quot; , &quot;tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelTabs&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelBookFilter&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;tctl00$ctl00$InsideForm$MasterContent$ExFilter$UpdatePanelCheckboxFilters&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;], [&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DropDownListChapter&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DropDownListSection&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListObjective&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;DropDownListObjective&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>




	
    








	learningAids = new Array();


		
			var learningAidguidedsolution = new Object();
			learningAidguidedsolution.name = &quot; , &quot;'&quot; , &quot;guidedsolution&quot; , &quot;'&quot; , &quot;;
			learningAidguidedsolution.displayText = &quot;SBHelp Me Solve This&quot;;
			learningAidguidedsolution.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_guidedsolution.gif&quot; , &quot;'&quot; , &quot;;
			learningAidguidedsolution.enabled = true;
			learningAidguidedsolution.hasCustomLogic = false;
			learningAidguidedsolution.visible = true;
			learningAidguidedsolution.isExerciseSpecific = true;
			learningAidguidedsolution.exerciseExists = false;
			learningAidguidedsolution.playerParamName = &quot; , &quot;'&quot; , &quot;showguidedsolution&quot; , &quot;'&quot; , &quot;;
			//learningAidguidedsolution.initCapName = learningAidguidedsolution.name.substring(0, 1).toUpperCase() + learningAidguidedsolution.name.substring(1);
			learningAidguidedsolution.typeID = 10;

			learningAids.push(learningAidguidedsolution);


			function ViewSample_guidedsolution()
			{
				popupWindow(&quot;guidedsolution&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/images/helpmesolvethis700.jpg&quot;, {Width:700,Height:460,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidexample = new Object();
			learningAidexample.name = &quot; , &quot;'&quot; , &quot;example&quot; , &quot;'&quot; , &quot;;
			learningAidexample.displayText = &quot;SBView an Example&quot;;
			learningAidexample.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_sampleproblem.gif&quot; , &quot;'&quot; , &quot;;
			learningAidexample.enabled = true;
			learningAidexample.hasCustomLogic = false;
			learningAidexample.visible = true;
			learningAidexample.isExerciseSpecific = true;
			learningAidexample.exerciseExists = false;
			learningAidexample.playerParamName = &quot; , &quot;'&quot; , &quot;showexample&quot; , &quot;'&quot; , &quot;;
			//learningAidexample.initCapName = learningAidexample.name.substring(0, 1).toUpperCase() + learningAidexample.name.substring(1);
			learningAidexample.typeID = 11;

			learningAids.push(learningAidexample);


			function ViewSample_example()
			{
				popupWindow(&quot;example&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/images/showexample700.jpg&quot;, {Width:700,Height:460,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidvideo = new Object();
			learningAidvideo.name = &quot; , &quot;'&quot; , &quot;video&quot; , &quot;'&quot; , &quot;;
			learningAidvideo.displayText = &quot;SBVideoooo&quot;;
			learningAidvideo.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_video.gif&quot; , &quot;'&quot; , &quot;;
			learningAidvideo.enabled = true;
			learningAidvideo.hasCustomLogic = false;
			learningAidvideo.visible = true;
			learningAidvideo.isExerciseSpecific = true;
			learningAidvideo.exerciseExists = false;
			learningAidvideo.playerParamName = &quot; , &quot;'&quot; , &quot;showvideo&quot; , &quot;'&quot; , &quot;;
			//learningAidvideo.initCapName = learningAidvideo.name.substring(0, 1).toUpperCase() + learningAidvideo.name.substring(1);
			learningAidvideo.typeID = 1;

			learningAids.push(learningAidvideo);


			function ViewSample_video()
			{
				popupWindow(&quot;video&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/flash_video_player/player.html?/aw/aw_bittinger_eaconapp_8/video/bke08_0101e01&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidanimation = new Object();
			learningAidanimation.name = &quot; , &quot;'&quot; , &quot;animation&quot; , &quot;'&quot; , &quot;;
			learningAidanimation.displayText = &quot;SBAnimation&quot;;
			learningAidanimation.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_animation.gif&quot; , &quot;'&quot; , &quot;;
			learningAidanimation.enabled = true;
			learningAidanimation.hasCustomLogic = false;
			learningAidanimation.visible = true;
			learningAidanimation.isExerciseSpecific = true;
			learningAidanimation.exerciseExists = false;
			learningAidanimation.playerParamName = &quot; , &quot;'&quot; , &quot;showanimation&quot; , &quot;'&quot; , &quot;;
			//learningAidanimation.initCapName = learningAidanimation.name.substring(0, 1).toUpperCase() + learningAidanimation.name.substring(1);
			learningAidanimation.typeID = 2;

			learningAids.push(learningAidanimation);


			function ViewSample_animation()
			{
				popupWindow(&quot;animation&quot;, &quot;http://media.pearsoncmg.com/aw/aw_mml_shared_1/precalculus/01020141/index.html&quot;, {Width:620,Height:370,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtextbook = new Object();
			learningAidtextbook.name = &quot; , &quot;'&quot; , &quot;textbook&quot; , &quot;'&quot; , &quot;;
			learningAidtextbook.displayText = &quot;SBTextbook&quot;;
			learningAidtextbook.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_textbook.gif&quot; , &quot;'&quot; , &quot;;
			learningAidtextbook.enabled = true;
			learningAidtextbook.hasCustomLogic = false;
			learningAidtextbook.visible = true;
			learningAidtextbook.isExerciseSpecific = true;
			learningAidtextbook.exerciseExists = false;
			learningAidtextbook.playerParamName = &quot; , &quot;'&quot; , &quot;showtextbook&quot; , &quot;'&quot; , &quot;;
			//learningAidtextbook.initCapName = learningAidtextbook.name.substring(0, 1).toUpperCase() + learningAidtextbook.name.substring(1);
			learningAidtextbook.typeID = 5;

			learningAids.push(learningAidtextbook);


			function ViewSample_textbook()
			{
				popupWindow(&quot;textbook&quot;, &quot;http://media.pearsoncmg.com/cmg/pmmg_mml_shared/mxl_ebook_view_sample/aps08_flash_main.html&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidconceptbook = new Object();
			learningAidconceptbook.name = &quot; , &quot;'&quot; , &quot;conceptbook&quot; , &quot;'&quot; , &quot;;
			learningAidconceptbook.displayText = &quot;Concept Review&quot;;
			learningAidconceptbook.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_conceptbook.png&quot; , &quot;'&quot; , &quot;;
			learningAidconceptbook.enabled = true;
			learningAidconceptbook.hasCustomLogic = false;
			learningAidconceptbook.visible = true;
			learningAidconceptbook.isExerciseSpecific = true;
			learningAidconceptbook.exerciseExists = false;
			learningAidconceptbook.playerParamName = &quot; , &quot;'&quot; , &quot;showconceptbook&quot; , &quot;'&quot; , &quot;;
			//learningAidconceptbook.initCapName = learningAidconceptbook.name.substring(0, 1).toUpperCase() + learningAidconceptbook.name.substring(1);
			learningAidconceptbook.typeID = 31;

			learningAids.push(learningAidconceptbook);


			function ViewSample_conceptbook()
			{
				popupWindow(&quot;conceptbook&quot;, &quot;/support/learningaids/conceptbook.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtextbookextras = new Object();
			learningAidtextbookextras.name = &quot; , &quot;'&quot; , &quot;textbookextras&quot; , &quot;'&quot; , &quot;;
			learningAidtextbookextras.displayText = &quot;SBTextbook Extra&quot;;
			learningAidtextbookextras.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_textbookextras.png&quot; , &quot;'&quot; , &quot;;
			learningAidtextbookextras.enabled = true;
			learningAidtextbookextras.hasCustomLogic = false;
			learningAidtextbookextras.visible = true;
			learningAidtextbookextras.isExerciseSpecific = true;
			learningAidtextbookextras.exerciseExists = false;
			learningAidtextbookextras.playerParamName = &quot; , &quot;'&quot; , &quot;showtextbookextras&quot; , &quot;'&quot; , &quot;;
			//learningAidtextbookextras.initCapName = learningAidtextbookextras.name.substring(0, 1).toUpperCase() + learningAidtextbookextras.name.substring(1);
			learningAidtextbookextras.typeID = 28;

			learningAids.push(learningAidtextbookextras);


			function ViewSample_textbookextras()
			{
				popupWindow(&quot;textbookextras&quot;, &quot;/support/learningaids/textbookextras.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidmathtools = new Object();
			learningAidmathtools.name = &quot; , &quot;'&quot; , &quot;mathtools&quot; , &quot;'&quot; , &quot;;
			learningAidmathtools.displayText = &quot;Math Tools&quot;;
			learningAidmathtools.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_mathtools.png&quot; , &quot;'&quot; , &quot;;
			learningAidmathtools.enabled = true;
			learningAidmathtools.hasCustomLogic = false;
			learningAidmathtools.visible = true;
			learningAidmathtools.isExerciseSpecific = true;
			learningAidmathtools.exerciseExists = false;
			learningAidmathtools.playerParamName = &quot; , &quot;'&quot; , &quot;showmathtools&quot; , &quot;'&quot; , &quot;;
			//learningAidmathtools.initCapName = learningAidmathtools.name.substring(0, 1).toUpperCase() + learningAidmathtools.name.substring(1);
			learningAidmathtools.typeID = 32;

			learningAids.push(learningAidmathtools);


			function ViewSample_mathtools()
			{
				popupWindow(&quot;mathtools&quot;, &quot;/support/learningaids/mathtools.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidglossary = new Object();
			learningAidglossary.name = &quot; , &quot;'&quot; , &quot;glossary&quot; , &quot;'&quot; , &quot;;
			learningAidglossary.displayText = &quot;SBGlossary&quot;;
			learningAidglossary.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_glossary.gif&quot; , &quot;'&quot; , &quot;;
			learningAidglossary.enabled = true;
			learningAidglossary.hasCustomLogic = false;
			learningAidglossary.visible = true;
			learningAidglossary.isExerciseSpecific = true;
			learningAidglossary.exerciseExists = false;
			learningAidglossary.playerParamName = &quot; , &quot;'&quot; , &quot;showglossary&quot; , &quot;'&quot; , &quot;;
			//learningAidglossary.initCapName = learningAidglossary.name.substring(0, 1).toUpperCase() + learningAidglossary.name.substring(1);
			learningAidglossary.typeID = 14;

			learningAids.push(learningAidglossary);


			function ViewSample_glossary()
			{
				popupWindow(&quot;glossary&quot;, &quot;/support/learningaids/glossary.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidreview = new Object();
			learningAidreview.name = &quot; , &quot;'&quot; , &quot;review&quot; , &quot;'&quot; , &quot;;
			learningAidreview.displayText = &quot;SBReview&quot;;
			learningAidreview.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_review.png&quot; , &quot;'&quot; , &quot;;
			learningAidreview.enabled = true;
			learningAidreview.hasCustomLogic = false;
			learningAidreview.visible = true;
			learningAidreview.isExerciseSpecific = true;
			learningAidreview.exerciseExists = false;
			learningAidreview.playerParamName = &quot; , &quot;'&quot; , &quot;showreviewinplayer&quot; , &quot;'&quot; , &quot;;
			//learningAidreview.initCapName = learningAidreview.name.substring(0, 1).toUpperCase() + learningAidreview.name.substring(1);
			learningAidreview.typeID = 24;

			learningAids.push(learningAidreview);


			function ViewSample_review()
			{
				popupWindow(&quot;review&quot;, &quot;/support/learningaids/reviewinplayer.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidstatexplore = new Object();
			learningAidstatexplore.name = &quot; , &quot;'&quot; , &quot;statexplore&quot; , &quot;'&quot; , &quot;;
			learningAidstatexplore.displayText = &quot;StatCrunch&quot;;
			learningAidstatexplore.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_statcrunch.gif&quot; , &quot;'&quot; , &quot;;
			learningAidstatexplore.enabled = true;
			learningAidstatexplore.hasCustomLogic = false;
			learningAidstatexplore.visible = true;
			learningAidstatexplore.isExerciseSpecific = true;
			learningAidstatexplore.exerciseExists = false;
			learningAidstatexplore.playerParamName = &quot; , &quot;'&quot; , &quot;showstatexplore&quot; , &quot;'&quot; , &quot;;
			//learningAidstatexplore.initCapName = learningAidstatexplore.name.substring(0, 1).toUpperCase() + learningAidstatexplore.name.substring(1);
			learningAidstatexplore.typeID = 3;

			learningAids.push(learningAidstatexplore);


			function ViewSample_statexplore()
			{
				popupWindow(&quot;statexplore&quot;, &quot;http://media.pearsoncmg.com/aw/aw_mml_shared_1/statexplore.gif&quot;, {Width:820,Height:560,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidgeogebra = new Object();
			learningAidgeogebra.name = &quot; , &quot;'&quot; , &quot;geogebra&quot; , &quot;'&quot; , &quot;;
			learningAidgeogebra.displayText = &quot;Geogebra\n&quot;;
			learningAidgeogebra.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_geogebra.png&quot; , &quot;'&quot; , &quot;;
			learningAidgeogebra.enabled = true;
			learningAidgeogebra.hasCustomLogic = false;
			learningAidgeogebra.visible = true;
			learningAidgeogebra.isExerciseSpecific = true;
			learningAidgeogebra.exerciseExists = false;
			learningAidgeogebra.playerParamName = &quot; , &quot;'&quot; , &quot;showgeogebra&quot; , &quot;'&quot; , &quot;;
			//learningAidgeogebra.initCapName = learningAidgeogebra.name.substring(0, 1).toUpperCase() + learningAidgeogebra.name.substring(1);
			learningAidgeogebra.typeID = 26;

			learningAids.push(learningAidgeogebra);


			function ViewSample_geogebra()
			{
				popupWindow(&quot;geogebra&quot;, &quot;/support/learningaids/geogebra.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtechhelp = new Object();
			learningAidtechhelp.name = &quot; , &quot;'&quot; , &quot;techhelp&quot; , &quot;'&quot; , &quot;;
			learningAidtechhelp.displayText = &quot;Tech Help&quot;;
			learningAidtechhelp.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_techhelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidtechhelp.enabled = true;
			learningAidtechhelp.hasCustomLogic = false;
			learningAidtechhelp.visible = true;
			learningAidtechhelp.isExerciseSpecific = true;
			learningAidtechhelp.exerciseExists = false;
			learningAidtechhelp.playerParamName = &quot; , &quot;'&quot; , &quot;showtechhelp&quot; , &quot;'&quot; , &quot;;
			//learningAidtechhelp.initCapName = learningAidtechhelp.name.substring(0, 1).toUpperCase() + learningAidtechhelp.name.substring(1);
			learningAidtechhelp.typeID = 23;

			learningAids.push(learningAidtechhelp);


			function ViewSample_techhelp()
			{
				popupWindow(&quot;techhelp&quot;, &quot;/support/learningaids/techhelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidextrahelp = new Object();
			learningAidextrahelp.name = &quot; , &quot;'&quot; , &quot;extrahelp&quot; , &quot;'&quot; , &quot;;
			learningAidextrahelp.displayText = &quot;SBExtra Help&quot;;
			learningAidextrahelp.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_extrahelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidextrahelp.enabled = true;
			learningAidextrahelp.hasCustomLogic = false;
			learningAidextrahelp.visible = true;
			learningAidextrahelp.isExerciseSpecific = true;
			learningAidextrahelp.exerciseExists = false;
			learningAidextrahelp.playerParamName = &quot; , &quot;'&quot; , &quot;showextrahelp&quot; , &quot;'&quot; , &quot;;
			//learningAidextrahelp.initCapName = learningAidextrahelp.name.substring(0, 1).toUpperCase() + learningAidextrahelp.name.substring(1);
			learningAidextrahelp.typeID = 25;

			learningAids.push(learningAidextrahelp);


			function ViewSample_extrahelp()
			{
				popupWindow(&quot;extrahelp&quot;, &quot;/support/learningaids/extrahelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidcalculator = new Object();
			learningAidcalculator.name = &quot; , &quot;'&quot; , &quot;calculator&quot; , &quot;'&quot; , &quot;;
			learningAidcalculator.displayText = &quot;Calculator&quot;;
			learningAidcalculator.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_calc.gif&quot; , &quot;'&quot; , &quot;;
			learningAidcalculator.enabled = false;
			learningAidcalculator.hasCustomLogic = false;
			learningAidcalculator.visible = false;
			learningAidcalculator.isExerciseSpecific = false;
			learningAidcalculator.exerciseExists = false;
			learningAidcalculator.playerParamName = &quot; , &quot;'&quot; , &quot;showcalculator&quot; , &quot;'&quot; , &quot;;
			//learningAidcalculator.initCapName = learningAidcalculator.name.substring(0, 1).toUpperCase() + learningAidcalculator.name.substring(1);
			learningAidcalculator.typeID = 8;

			learningAids.push(learningAidcalculator);


			function ViewSample_calculator()
			{
				popupWindow(&quot;calculator&quot;, &quot;../support/learningaids/calculator_sample.html&quot;, {Width:640,Height:480,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidaskinstructor = new Object();
			learningAidaskinstructor.name = &quot; , &quot;'&quot; , &quot;askinstructor&quot; , &quot;'&quot; , &quot;;
			learningAidaskinstructor.displayText = &quot;Ask My Instructor&quot;;
			learningAidaskinstructor.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_askmyinstrctr.gif&quot; , &quot;'&quot; , &quot;;
			learningAidaskinstructor.enabled = true;
			learningAidaskinstructor.hasCustomLogic = true;
			learningAidaskinstructor.visible = true;
			learningAidaskinstructor.isExerciseSpecific = false;
			learningAidaskinstructor.exerciseExists = false;
			learningAidaskinstructor.playerParamName = &quot; , &quot;'&quot; , &quot;showaskinstructor&quot; , &quot;'&quot; , &quot;;
			//learningAidaskinstructor.initCapName = learningAidaskinstructor.name.substring(0, 1).toUpperCase() + learningAidaskinstructor.name.substring(1);
			learningAidaskinstructor.typeID = 6;

			learningAids.push(learningAidaskinstructor);


			function ViewSample_askinstructor()
			{
				popupWindow(&quot;askinstructor&quot;, &quot;sample_askmyinstructor.aspx&quot;, {Width:530,Height:430,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidconnecttotutor = new Object();
			learningAidconnecttotutor.name = &quot; , &quot;'&quot; , &quot;connecttotutor&quot; , &quot;'&quot; , &quot;;
			learningAidconnecttotutor.displayText = &quot;Connect to a Tutor&quot;;
			learningAidconnecttotutor.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_tutorvista.gif&quot; , &quot;'&quot; , &quot;;
			learningAidconnecttotutor.enabled = false;
			learningAidconnecttotutor.hasCustomLogic = true;
			learningAidconnecttotutor.visible = false;
			learningAidconnecttotutor.isExerciseSpecific = false;
			learningAidconnecttotutor.exerciseExists = false;
			learningAidconnecttotutor.playerParamName = &quot; , &quot;'&quot; , &quot;showconnecttotutor&quot; , &quot;'&quot; , &quot;;
			//learningAidconnecttotutor.initCapName = learningAidconnecttotutor.name.substring(0, 1).toUpperCase() + learningAidconnecttotutor.name.substring(1);
			learningAidconnecttotutor.typeID = 33;

			learningAids.push(learningAidconnecttotutor);


			function ViewSample_connecttotutor()
			{
				popupWindow(&quot;connecttotutor&quot;, &quot;/support/learningaids/tutor_sample.html&quot;, {Width:800,Height:690,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidfasttrack = new Object();
			learningAidfasttrack.name = &quot; , &quot;'&quot; , &quot;fasttrack&quot; , &quot;'&quot; , &quot;;
			learningAidfasttrack.displayText = &quot;Fast Track&quot;;
			learningAidfasttrack.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_fasttrack.gif&quot; , &quot;'&quot; , &quot;;
			learningAidfasttrack.enabled = false;
			learningAidfasttrack.hasCustomLogic = false;
			learningAidfasttrack.visible = false;
			learningAidfasttrack.isExerciseSpecific = true;
			learningAidfasttrack.exerciseExists = false;
			learningAidfasttrack.playerParamName = &quot; , &quot;'&quot; , &quot;showfasttrack&quot; , &quot;'&quot; , &quot;;
			//learningAidfasttrack.initCapName = learningAidfasttrack.name.substring(0, 1).toUpperCase() + learningAidfasttrack.name.substring(1);
			learningAidfasttrack.typeID = 4;

			learningAids.push(learningAidfasttrack);


			function ViewSample_fasttrack()
			{
				popupWindow(&quot;fasttrack&quot;, &quot;http://media.pearsoncmg.com/aw/aw_myeconlab/fasttrack/samplefasttrack.gif&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAiddemodoc = new Object();
			learningAiddemodoc.name = &quot; , &quot;'&quot; , &quot;demodoc&quot; , &quot;'&quot; , &quot;;
			learningAiddemodoc.displayText = &quot;DemoDocs Example&quot;;
			learningAiddemodoc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_demodocs.gif&quot; , &quot;'&quot; , &quot;;
			learningAiddemodoc.enabled = false;
			learningAiddemodoc.hasCustomLogic = false;
			learningAiddemodoc.visible = false;
			learningAiddemodoc.isExerciseSpecific = true;
			learningAiddemodoc.exerciseExists = false;
			learningAiddemodoc.playerParamName = &quot; , &quot;'&quot; , &quot;showdemodoc&quot; , &quot;'&quot; , &quot;;
			//learningAiddemodoc.initCapName = learningAiddemodoc.name.substring(0, 1).toUpperCase() + learningAiddemodoc.name.substring(1);
			learningAiddemodoc.typeID = 7;

			learningAids.push(learningAiddemodoc);


			function ViewSample_demodoc()
			{
				popupWindow(&quot;demodoc&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_mal_shared/sgdd1/index.html&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidkeyconcepts = new Object();
			learningAidkeyconcepts.name = &quot; , &quot;'&quot; , &quot;keyconcepts&quot; , &quot;'&quot; , &quot;;
			learningAidkeyconcepts.displayText = &quot;Key Concepts&quot;;
			learningAidkeyconcepts.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_demodocs.gif&quot; , &quot;'&quot; , &quot;;
			learningAidkeyconcepts.enabled = false;
			learningAidkeyconcepts.hasCustomLogic = false;
			learningAidkeyconcepts.visible = false;
			learningAidkeyconcepts.isExerciseSpecific = true;
			learningAidkeyconcepts.exerciseExists = false;
			learningAidkeyconcepts.playerParamName = &quot; , &quot;'&quot; , &quot;showkeyconcepts&quot; , &quot;'&quot; , &quot;;
			//learningAidkeyconcepts.initCapName = learningAidkeyconcepts.name.substring(0, 1).toUpperCase() + learningAidkeyconcepts.name.substring(1);
			learningAidkeyconcepts.typeID = 9;

			learningAids.push(learningAidkeyconcepts);


			function ViewSample_keyconcepts()
			{
				popupWindow(&quot;keyconcepts&quot;, &quot;http://media.pearsoncmg.com/intl/ema/uk/accounting_mal/sample_anim/mal_animation_sample1.htm?centerwin=yes&quot;, {Width:935,Height:670,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidgrapher = new Object();
			learningAidgrapher.name = &quot; , &quot;'&quot; , &quot;grapher&quot; , &quot;'&quot; , &quot;;
			learningAidgrapher.displayText = &quot;Grapher&quot;;
			learningAidgrapher.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_grapher.gif&quot; , &quot;'&quot; , &quot;;
			learningAidgrapher.enabled = false;
			learningAidgrapher.hasCustomLogic = false;
			learningAidgrapher.visible = false;
			learningAidgrapher.isExerciseSpecific = true;
			learningAidgrapher.exerciseExists = false;
			learningAidgrapher.playerParamName = &quot; , &quot;'&quot; , &quot;showgrapher&quot; , &quot;'&quot; , &quot;;
			//learningAidgrapher.initCapName = learningAidgrapher.name.substring(0, 1).toUpperCase() + learningAidgrapher.name.substring(1);
			learningAidgrapher.typeID = 12;

			learningAids.push(learningAidgrapher);


			function ViewSample_grapher()
			{
				popupWindow(&quot;grapher&quot;, &quot;/support/learningaids/grapher.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidspreadsheet = new Object();
			learningAidspreadsheet.name = &quot; , &quot;'&quot; , &quot;spreadsheet&quot; , &quot;'&quot; , &quot;;
			learningAidspreadsheet.displayText = &quot;Spreadsheet&quot;;
			learningAidspreadsheet.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_spreadsheet.gif&quot; , &quot;'&quot; , &quot;;
			learningAidspreadsheet.enabled = false;
			learningAidspreadsheet.hasCustomLogic = false;
			learningAidspreadsheet.visible = false;
			learningAidspreadsheet.isExerciseSpecific = true;
			learningAidspreadsheet.exerciseExists = false;
			learningAidspreadsheet.playerParamName = &quot; , &quot;'&quot; , &quot;showspreadsheet&quot; , &quot;'&quot; , &quot;;
			//learningAidspreadsheet.initCapName = learningAidspreadsheet.name.substring(0, 1).toUpperCase() + learningAidspreadsheet.name.substring(1);
			learningAidspreadsheet.typeID = 13;

			learningAids.push(learningAidspreadsheet);


			function ViewSample_spreadsheet()
			{
				popupWindow(&quot;spreadsheet&quot;, &quot;/support/learningaids/spreadsheet.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidannuitycalc = new Object();
			learningAidannuitycalc.name = &quot; , &quot;'&quot; , &quot;annuitycalc&quot; , &quot;'&quot; , &quot;;
			learningAidannuitycalc.displayText = &quot;Annuity Calculator&quot;;
			learningAidannuitycalc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_annuitycalc.gif&quot; , &quot;'&quot; , &quot;;
			learningAidannuitycalc.enabled = false;
			learningAidannuitycalc.hasCustomLogic = false;
			learningAidannuitycalc.visible = false;
			learningAidannuitycalc.isExerciseSpecific = true;
			learningAidannuitycalc.exerciseExists = false;
			learningAidannuitycalc.playerParamName = &quot; , &quot;'&quot; , &quot;showannuitycalc&quot; , &quot;'&quot; , &quot;;
			//learningAidannuitycalc.initCapName = learningAidannuitycalc.name.substring(0, 1).toUpperCase() + learningAidannuitycalc.name.substring(1);
			learningAidannuitycalc.typeID = 15;

			learningAids.push(learningAidannuitycalc);


			function ViewSample_annuitycalc()
			{
				popupWindow(&quot;annuitycalc&quot;, &quot;/support/learningaids/annuitycalc.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidschaums = new Object();
			learningAidschaums.name = &quot; , &quot;'&quot; , &quot;schaums&quot; , &quot;'&quot; , &quot;;
			learningAidschaums.displayText = &quot;Step-by-Step Example&quot;;
			learningAidschaums.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_schaum.gif&quot; , &quot;'&quot; , &quot;;
			learningAidschaums.enabled = false;
			learningAidschaums.hasCustomLogic = false;
			learningAidschaums.visible = false;
			learningAidschaums.isExerciseSpecific = true;
			learningAidschaums.exerciseExists = false;
			learningAidschaums.playerParamName = &quot; , &quot;'&quot; , &quot;showschaums&quot; , &quot;'&quot; , &quot;;
			//learningAidschaums.initCapName = learningAidschaums.name.substring(0, 1).toUpperCase() + learningAidschaums.name.substring(1);
			learningAidschaums.typeID = 16;

			learningAids.push(learningAidschaums);


			function ViewSample_schaums()
			{
				popupWindow(&quot;schaums&quot;, &quot;/support/learningaids/schaums.htm&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidinstructortip = new Object();
			learningAidinstructortip.name = &quot; , &quot;'&quot; , &quot;instructortip&quot; , &quot;'&quot; , &quot;;
			learningAidinstructortip.displayText = &quot;Instructor Tip&quot;;
			learningAidinstructortip.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_view_tip.png&quot; , &quot;'&quot; , &quot;;
			learningAidinstructortip.enabled = true;
			learningAidinstructortip.hasCustomLogic = false;
			learningAidinstructortip.visible = true;
			learningAidinstructortip.isExerciseSpecific = false;
			learningAidinstructortip.exerciseExists = false;
			learningAidinstructortip.playerParamName = &quot; , &quot;'&quot; , &quot;showinstructortip&quot; , &quot;'&quot; , &quot;;
			//learningAidinstructortip.initCapName = learningAidinstructortip.name.substring(0, 1).toUpperCase() + learningAidinstructortip.name.substring(1);
			learningAidinstructortip.typeID = 17;

			learningAids.push(learningAidinstructortip);


			function ViewSample_instructortip()
			{
				popupWindow(&quot;instructortip&quot;, &quot;/support/learningaids/instructortip.htm&quot;, {Width:550,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidboosterc = new Object();
			learningAidboosterc.name = &quot; , &quot;'&quot; , &quot;boosterc&quot; , &quot;'&quot; , &quot;;
			learningAidboosterc.displayText = &quot;Practice Book&quot;;
			learningAidboosterc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_boosterc.png&quot; , &quot;'&quot; , &quot;;
			learningAidboosterc.enabled = false;
			learningAidboosterc.hasCustomLogic = false;
			learningAidboosterc.visible = false;
			learningAidboosterc.isExerciseSpecific = true;
			learningAidboosterc.exerciseExists = false;
			learningAidboosterc.playerParamName = &quot; , &quot;'&quot; , &quot;showboosterc&quot; , &quot;'&quot; , &quot;;
			//learningAidboosterc.initCapName = learningAidboosterc.name.substring(0, 1).toUpperCase() + learningAidboosterc.name.substring(1);
			learningAidboosterc.typeID = 18;

			learningAids.push(learningAidboosterc);


			function ViewSample_boosterc()
			{
				popupWindow(&quot;boosterc&quot;, &quot;/support/learningaids/boosterc.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidpowerpoint = new Object();
			learningAidpowerpoint.name = &quot; , &quot;'&quot; , &quot;powerpoint&quot; , &quot;'&quot; , &quot;;
			learningAidpowerpoint.displayText = &quot;SBPowerPoint Slides1&quot;;
			learningAidpowerpoint.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_powerpoint.png&quot; , &quot;'&quot; , &quot;;
			learningAidpowerpoint.enabled = false;
			learningAidpowerpoint.hasCustomLogic = false;
			learningAidpowerpoint.visible = false;
			learningAidpowerpoint.isExerciseSpecific = true;
			learningAidpowerpoint.exerciseExists = false;
			learningAidpowerpoint.playerParamName = &quot; , &quot;'&quot; , &quot;showpowerpoint&quot; , &quot;'&quot; , &quot;;
			//learningAidpowerpoint.initCapName = learningAidpowerpoint.name.substring(0, 1).toUpperCase() + learningAidpowerpoint.name.substring(1);
			learningAidpowerpoint.typeID = 19;

			learningAids.push(learningAidpowerpoint);


			function ViewSample_powerpoint()
			{
				popupWindow(&quot;powerpoint&quot;, &quot;/support/learningaids/powerpoint.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidfinancialcalc = new Object();
			learningAidfinancialcalc.name = &quot; , &quot;'&quot; , &quot;financialcalc&quot; , &quot;'&quot; , &quot;;
			learningAidfinancialcalc.displayText = &quot;Financial Calculator&quot;;
			learningAidfinancialcalc.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_financialcalc.gif&quot; , &quot;'&quot; , &quot;;
			learningAidfinancialcalc.enabled = false;
			learningAidfinancialcalc.hasCustomLogic = false;
			learningAidfinancialcalc.visible = false;
			learningAidfinancialcalc.isExerciseSpecific = true;
			learningAidfinancialcalc.exerciseExists = false;
			learningAidfinancialcalc.playerParamName = &quot; , &quot;'&quot; , &quot;showfinancialcalc&quot; , &quot;'&quot; , &quot;;
			//learningAidfinancialcalc.initCapName = learningAidfinancialcalc.name.substring(0, 1).toUpperCase() + learningAidfinancialcalc.name.substring(1);
			learningAidfinancialcalc.typeID = 20;

			learningAids.push(learningAidfinancialcalc);


			function ViewSample_financialcalc()
			{
				popupWindow(&quot;financialcalc&quot;, &quot;http://media.pearsoncmg.com/aw/aw_myfinancelab/learningaids/financialcalc.jpg&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidomexplorertutor = new Object();
			learningAidomexplorertutor.name = &quot; , &quot;'&quot; , &quot;omexplorertutor&quot; , &quot;'&quot; , &quot;;
			learningAidomexplorertutor.displayText = &quot;OM Explorer Tutor&quot;;
			learningAidomexplorertutor.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_omexplorertutor.png&quot; , &quot;'&quot; , &quot;;
			learningAidomexplorertutor.enabled = false;
			learningAidomexplorertutor.hasCustomLogic = false;
			learningAidomexplorertutor.visible = false;
			learningAidomexplorertutor.isExerciseSpecific = true;
			learningAidomexplorertutor.exerciseExists = false;
			learningAidomexplorertutor.playerParamName = &quot; , &quot;'&quot; , &quot;showomexplorertutor&quot; , &quot;'&quot; , &quot;;
			//learningAidomexplorertutor.initCapName = learningAidomexplorertutor.name.substring(0, 1).toUpperCase() + learningAidomexplorertutor.name.substring(1);
			learningAidomexplorertutor.typeID = 21;

			learningAids.push(learningAidomexplorertutor);


			function ViewSample_omexplorertutor()
			{
				popupWindow(&quot;omexplorertutor&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_moml_shared/la_samples/omexptutor.jpg&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAideconspreadsheet = new Object();
			learningAideconspreadsheet.name = &quot; , &quot;'&quot; , &quot;econspreadsheet&quot; , &quot;'&quot; , &quot;;
			learningAideconspreadsheet.displayText = &quot;Excel Spreadsheet&quot;;
			learningAideconspreadsheet.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_spreadsheet.gif&quot; , &quot;'&quot; , &quot;;
			learningAideconspreadsheet.enabled = false;
			learningAideconspreadsheet.hasCustomLogic = false;
			learningAideconspreadsheet.visible = false;
			learningAideconspreadsheet.isExerciseSpecific = true;
			learningAideconspreadsheet.exerciseExists = false;
			learningAideconspreadsheet.playerParamName = &quot; , &quot;'&quot; , &quot;showeconexcelspreadsheet&quot; , &quot;'&quot; , &quot;;
			//learningAideconspreadsheet.initCapName = learningAideconspreadsheet.name.substring(0, 1).toUpperCase() + learningAideconspreadsheet.name.substring(1);
			learningAideconspreadsheet.typeID = 22;

			learningAids.push(learningAideconspreadsheet);


			function ViewSample_econspreadsheet()
			{
				popupWindow(&quot;econspreadsheet&quot;, &quot;/support/learningaids/econspreadsheet.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidcalculateexcel = new Object();
			learningAidcalculateexcel.name = &quot; , &quot;'&quot; , &quot;calculateexcel&quot; , &quot;'&quot; , &quot;;
			learningAidcalculateexcel.displayText = &quot;Calculate in Excel\n&quot;;
			learningAidcalculateexcel.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_spreadsheet.gif&quot; , &quot;'&quot; , &quot;;
			learningAidcalculateexcel.enabled = false;
			learningAidcalculateexcel.hasCustomLogic = false;
			learningAidcalculateexcel.visible = false;
			learningAidcalculateexcel.isExerciseSpecific = true;
			learningAidcalculateexcel.exerciseExists = false;
			learningAidcalculateexcel.playerParamName = &quot; , &quot;'&quot; , &quot;showcalculateexcel&quot; , &quot;'&quot; , &quot;;
			//learningAidcalculateexcel.initCapName = learningAidcalculateexcel.name.substring(0, 1).toUpperCase() + learningAidcalculateexcel.name.substring(1);
			learningAidcalculateexcel.typeID = 27;

			learningAids.push(learningAidcalculateexcel);


			function ViewSample_calculateexcel()
			{
				popupWindow(&quot;calculateexcel&quot;, &quot;http://media.pearsoncmg.com/ph/bp/bp_mal_shared/spreadsheet/scratchsheet.xls&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidmorehelp = new Object();
			learningAidmorehelp.name = &quot; , &quot;'&quot; , &quot;morehelp&quot; , &quot;'&quot; , &quot;;
			learningAidmorehelp.displayText = &quot;SBMore Help&quot;;
			learningAidmorehelp.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_morehelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidmorehelp.enabled = false;
			learningAidmorehelp.hasCustomLogic = false;
			learningAidmorehelp.visible = false;
			learningAidmorehelp.isExerciseSpecific = false;
			learningAidmorehelp.exerciseExists = false;
			learningAidmorehelp.playerParamName = &quot; , &quot;'&quot; , &quot;showmorehelp&quot; , &quot;'&quot; , &quot;;
			//learningAidmorehelp.initCapName = learningAidmorehelp.name.substring(0, 1).toUpperCase() + learningAidmorehelp.name.substring(1);
			learningAidmorehelp.typeID = 29;

			learningAids.push(learningAidmorehelp);


			function ViewSample_morehelp()
			{
				popupWindow(&quot;morehelp&quot;, &quot;/support/learningaids/morehelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidtemplates = new Object();
			learningAidtemplates.name = &quot; , &quot;'&quot; , &quot;templates&quot; , &quot;'&quot; , &quot;;
			learningAidtemplates.displayText = &quot;Templates&quot;;
			learningAidtemplates.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_templates.png&quot; , &quot;'&quot; , &quot;;
			learningAidtemplates.enabled = false;
			learningAidtemplates.hasCustomLogic = false;
			learningAidtemplates.visible = false;
			learningAidtemplates.isExerciseSpecific = true;
			learningAidtemplates.exerciseExists = false;
			learningAidtemplates.playerParamName = &quot; , &quot;'&quot; , &quot;showtemplates&quot; , &quot;'&quot; , &quot;;
			//learningAidtemplates.initCapName = learningAidtemplates.name.substring(0, 1).toUpperCase() + learningAidtemplates.name.substring(1);
			learningAidtemplates.typeID = 30;

			learningAids.push(learningAidtemplates);


			function ViewSample_templates()
			{
				popupWindow(&quot;templates&quot;, &quot;/support/learningaids/template.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidbillofrights = new Object();
			learningAidbillofrights.name = &quot; , &quot;'&quot; , &quot;billofrights&quot; , &quot;'&quot; , &quot;;
			learningAidbillofrights.displayText = &quot;Bill of Rights&quot;;
			learningAidbillofrights.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_billofrights.png&quot; , &quot;'&quot; , &quot;;
			learningAidbillofrights.enabled = false;
			learningAidbillofrights.hasCustomLogic = false;
			learningAidbillofrights.visible = false;
			learningAidbillofrights.isExerciseSpecific = true;
			learningAidbillofrights.exerciseExists = false;
			learningAidbillofrights.playerParamName = &quot; , &quot;'&quot; , &quot;showbillofrights&quot; , &quot;'&quot; , &quot;;
			//learningAidbillofrights.initCapName = learningAidbillofrights.name.substring(0, 1).toUpperCase() + learningAidbillofrights.name.substring(1);
			learningAidbillofrights.typeID = 34;

			learningAids.push(learningAidbillofrights);


			function ViewSample_billofrights()
			{
				popupWindow(&quot;billofrights&quot;, &quot;http://media.pearsoncmg.com/ph/chet/chet_criminal_justice_1/resources/billofrights.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidusconstitution = new Object();
			learningAidusconstitution.name = &quot; , &quot;'&quot; , &quot;usconstitution&quot; , &quot;'&quot; , &quot;;
			learningAidusconstitution.displayText = &quot;U.S. Constitution&quot;;
			learningAidusconstitution.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_usconstitution.png&quot; , &quot;'&quot; , &quot;;
			learningAidusconstitution.enabled = false;
			learningAidusconstitution.hasCustomLogic = false;
			learningAidusconstitution.visible = false;
			learningAidusconstitution.isExerciseSpecific = true;
			learningAidusconstitution.exerciseExists = false;
			learningAidusconstitution.playerParamName = &quot; , &quot;'&quot; , &quot;showusconstitution&quot; , &quot;'&quot; , &quot;;
			//learningAidusconstitution.initCapName = learningAidusconstitution.name.substring(0, 1).toUpperCase() + learningAidusconstitution.name.substring(1);
			learningAidusconstitution.typeID = 35;

			learningAids.push(learningAidusconstitution);


			function ViewSample_usconstitution()
			{
				popupWindow(&quot;usconstitution&quot;, &quot;http://media.pearsoncmg.com/ph/chet/chet_criminal_justice_1/resources/constitution.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidimage = new Object();
			learningAidimage.name = &quot; , &quot;'&quot; , &quot;image&quot; , &quot;'&quot; , &quot;;
			learningAidimage.displayText = &quot;Image&quot;;
			learningAidimage.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_textbookextras.png&quot; , &quot;'&quot; , &quot;;
			learningAidimage.enabled = false;
			learningAidimage.hasCustomLogic = false;
			learningAidimage.visible = false;
			learningAidimage.isExerciseSpecific = true;
			learningAidimage.exerciseExists = false;
			learningAidimage.playerParamName = &quot; , &quot;'&quot; , &quot;showimage&quot; , &quot;'&quot; , &quot;;
			//learningAidimage.initCapName = learningAidimage.name.substring(0, 1).toUpperCase() + learningAidimage.name.substring(1);
			learningAidimage.typeID = 36;

			learningAids.push(learningAidimage);


			function ViewSample_image()
			{
				popupWindow(&quot;image&quot;, &quot;/support/learningaids/textbookextras.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidguidedlecture = new Object();
			learningAidguidedlecture.name = &quot; , &quot;'&quot; , &quot;guidedlecture&quot; , &quot;'&quot; , &quot;;
			learningAidguidedlecture.displayText = &quot;Guided Lecture&quot;;
			learningAidguidedlecture.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/learnaid_powerpoint.png&quot; , &quot;'&quot; , &quot;;
			learningAidguidedlecture.enabled = false;
			learningAidguidedlecture.hasCustomLogic = false;
			learningAidguidedlecture.visible = false;
			learningAidguidedlecture.isExerciseSpecific = true;
			learningAidguidedlecture.exerciseExists = false;
			learningAidguidedlecture.playerParamName = &quot; , &quot;'&quot; , &quot;showguidedlecture&quot; , &quot;'&quot; , &quot;;
			//learningAidguidedlecture.initCapName = learningAidguidedlecture.name.substring(0, 1).toUpperCase() + learningAidguidedlecture.name.substring(1);
			learningAidguidedlecture.typeID = 37;

			learningAids.push(learningAidguidedlecture);


			function ViewSample_guidedlecture()
			{
				popupWindow(&quot;guidedlecture&quot;, &quot;/support/learningaids/powerpoint.htm&quot;, {Width:900,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidflashcards = new Object();
			learningAidflashcards.name = &quot; , &quot;'&quot; , &quot;flashcards&quot; , &quot;'&quot; , &quot;;
			learningAidflashcards.displayText = &quot;Flashcards&quot;;
			learningAidflashcards.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_review.png&quot; , &quot;'&quot; , &quot;;
			learningAidflashcards.enabled = false;
			learningAidflashcards.hasCustomLogic = false;
			learningAidflashcards.visible = false;
			learningAidflashcards.isExerciseSpecific = true;
			learningAidflashcards.exerciseExists = false;
			learningAidflashcards.playerParamName = &quot; , &quot;'&quot; , &quot;showflashcards&quot; , &quot;'&quot; , &quot;;
			//learningAidflashcards.initCapName = learningAidflashcards.name.substring(0, 1).toUpperCase() + learningAidflashcards.name.substring(1);
			learningAidflashcards.typeID = 38;

			learningAids.push(learningAidflashcards);


			function ViewSample_flashcards()
			{
				popupWindow(&quot;flashcards&quot;, &quot;/support/learningaids/reviewinplayer.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	
		
			var learningAidlabelingactivity = new Object();
			learningAidlabelingactivity.name = &quot; , &quot;'&quot; , &quot;labelingactivity&quot; , &quot;'&quot; , &quot;;
			learningAidlabelingactivity.displayText = &quot;Labeling Activity&quot;;
			learningAidlabelingactivity.imageUrl = &quot; , &quot;'&quot; , &quot;images/icons/coursemanager_icon_morehelp.png&quot; , &quot;'&quot; , &quot;;
			learningAidlabelingactivity.enabled = false;
			learningAidlabelingactivity.hasCustomLogic = false;
			learningAidlabelingactivity.visible = false;
			learningAidlabelingactivity.isExerciseSpecific = true;
			learningAidlabelingactivity.exerciseExists = false;
			learningAidlabelingactivity.playerParamName = &quot; , &quot;'&quot; , &quot;showlabelingactivity&quot; , &quot;'&quot; , &quot;;
			//learningAidlabelingactivity.initCapName = learningAidlabelingactivity.name.substring(0, 1).toUpperCase() + learningAidlabelingactivity.name.substring(1);
			learningAidlabelingactivity.typeID = 39;

			learningAids.push(learningAidlabelingactivity);


			function ViewSample_labelingactivity()
			{
				popupWindow(&quot;labelingactivity&quot;, &quot;/support/learningaids/morehelp.htm&quot;, {Width:800,Height:600,Scrollbar:false,Statusbar:false});
			}

		
	




	var learningAidTypeMap = new Object();
	for (var i = 0; i &lt; learningAids.length; i++) {
		learningAidTypeMap[learningAids[i].typeID] = learningAids[i];
	}

	function GetLearningAidsArray() {
		return learningAids;
	}

	function GetLearningAidByTypeId(typeId) {
		return learningAidTypeMap[typeId];
	}

	function GetLearningAidRowId(typeId) {
		return rowId[typeId]; //
			}

			function GetLearningAidCheckBoxId(typeId) {
				return cbId[typeId]; //
			}

			function IsLearningAidRowHidden(typeId) {
				var rowId = GetLearningAidRowId(typeId);
				var row = document.getElementById(rowId);
				return row.style.display == &quot;none&quot;;
			}

			function GetLearningAidCheckBox(typeId) {
				if (typeof(cbId[typeId]) == &quot; , &quot;'&quot; , &quot;undefined&quot; , &quot;'&quot; , &quot; &amp;&amp; cbId[typeId] != null)
					return null;

				return document.getElementById(cbId[typeId]);
			}


			function UpdateLearningAids() {

				var LearningAidsListLabel = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel&quot; , &quot;'&quot; , &quot;);

		// update text list of learning aids in Change Settings page view


		var foundExerciseSpecificAid = false;
		var foundVisibleAid = false;

		for (var j = 0; j &lt; learningAids.length; j++)
			if (learningAids[j].visible &amp;&amp; !learningAids[j].isExerciseSpecific)
				foundVisibleAid = true;

		var ulSel = document.getElementById(&quot;ulSelected&quot;);
		if (ulSel)
		{
			/// Reset existing exercise flags
			for (var j = 0; j &lt; learningAids.length; j++)
				learningAids[j].exerciseExists = false;


			for (var i = 0; i &lt; ulSel.childNodes.length; i++)
			{
				var selectedExercise = getSelectedExercise(ulSel.childNodes[i].id);
				if (selectedExercise.questionType != QuestionTypes.Question)
					continue; // skip questions from other books, custom questions, and media questions

				// CA The following loop checks for visible and aids tied to an exercise, setting flags
				// as appropriate. The flags are later verified to construct the list of aids on the
				// Settings page.

				for (var j = 0; j &lt; learningAids.length; j++)
					if (learningAids[j].visible)
					{
						// CA for exercise-specific learning aids, check to see if the selected exercise has the aid
						if (learningAids[j].isExerciseSpecific &amp;&amp; !learningAids[j].exerciseExists)
						{
							var hasAid = eval(&quot; , &quot;'&quot; , &quot;selectedExercise.hasLearningAidType(&quot; , &quot;'&quot; , &quot; + learningAids[j].typeID + &quot; , &quot;'&quot; , &quot;)&quot; , &quot;'&quot; , &quot;);

							if (hasAid)
							{
								learningAids[j].exerciseExists = true;
								foundExerciseSpecificAid = true;
							}

						}

						foundVisibleAid = true;
					}
			}
		}

		/// CA If no aids are found, display the None heading and error out
		if (!foundVisibleAid &amp;&amp; !foundExerciseSpecificAid)
		{
			handleNoLearningAids();
			return false;
		}


		// CA For the display string on the settings page, in order for an aid to appear
		// it must be visible according to course settings, enabled, and either:
		// - not exercise specific
		// - exercise specific and there exists an exercise associated with the learning aid
		var learningAidsDisplayString = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;


		for(var i = 0; i &lt; learningAids.length; i++)
			if (learningAids[i].visible &amp;&amp; learningAids[i].enabled
				&amp;&amp; (!learningAids[i].isExerciseSpecific || (learningAids[i].isExerciseSpecific &amp;&amp; learningAids[i].exerciseExists)))
			{
				if (learningAidsDisplayString == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
					learningAidsDisplayString = learningAids[i].displayText;
				else
					learningAidsDisplayString += &quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot; + learningAids[i].displayText;
			}

		if (learningAidsDisplayString == &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)
			handleNoLearningAids();
		else
			handleExistingLearningAids(learningAidsDisplayString);

		var hasDisplayedRow = false;

		// CA show/hide corresponding table rows
		for(var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var tableRowClientID = eval(&quot; , &quot;'&quot; , &quot;LATableRowID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var tableRow = document.getElementById(tableRowClientID);
				var checkBoxClientID = eval(&quot; , &quot;'&quot; , &quot;LACheckBoxID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				// If the learning aid is enabled, ensure that the corresponding box is checked
				if (learningAids[i].enabled)
					checkBox.checked = true;
				else
					checkBox.checked = false;

				// The table row is displayed as per the conditions above, except that both enabled
				// and disabled exercises are displayed.
				if (!learningAids[i].isExerciseSpecific || (learningAids[i].isExerciseSpecific &amp;&amp; learningAids[i].exerciseExists))
				{
					tableRow.style.display = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
					hasDisplayedRow = true;
				}
				else
					tableRow.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;
			}
        }

			    // CA if there are no displayed rows, we must hide the Change link
			    var learningAidsChangeSpan = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsChangeSpan&quot; , &quot;'&quot; , &quot;);
			    if (hasDisplayedRow)
			        learningAidsChangeSpan.style.display = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
			    else
					learningAidsChangeSpan.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;

				UpdateReviewModeCheckboxText();
		
	}

    function  HandleFootNoteVisibility(){

     var statCrunchDataItem = learningAids.find(item=> item.name== &quot; , &quot;'&quot; , &quot;statexplore&quot; , &quot;'&quot; , &quot;)

         
    }




    function LearningAidsChangeLink_onclick() {
        HandleFootNoteVisibility();

        if (!isHW) {
            if (isUseLockdownBrowserChecked() &amp;&amp; isLearningAidReviewModeChecked()) {
				
				
                var enabledLearningAids = getEnabledLearningAids();
				var statCrunchEnabled = hasStatCrunchEnabled();
                var isNewGenStatCrunchFeaturesEnabled = &quot;false&quot;;
                if (statCrunchEnabled) {
                    isNewGenStatCrunchFeaturesEnabled = true;
                }
                if (isNewGenStatCrunchFeaturesEnabled) {
                    if (enabledLearningAids.length > 0 &amp;&amp; !statCrunchEnabled) {
                        alert(&quot;Learning Aids cannot be displayed on a test or quiz in LockDown Browser mode. They have been set to show in Review mode only.&quot;);
                    }
                    if (enabledLearningAids.length > 1 &amp;&amp; statCrunchEnabled) {
                        alert(&quot;StatCrunch is the only learning aid that can be displayed on a test or quiz in LockDown Browser mode. All other learning aids have been set to show in Review Mode only.&quot;);
                    }
                }
                else {
                    alert(&quot;Learning Aids cannot be displayed on a test or quiz in LockDown Browser mode. They have been set to show in Review mode only.&quot;);
                }
            }
        }

		// 
		if (learningAidsEnabled()) {
			var ulSel = document.getElementById(&quot;ulSelected&quot;);
			if (ulSel) {
				var hasQuestionLevelSettings = false;
				for (var i = 0; i &lt; ulSel.childNodes.length; i++) {
					var pointsBox = GetPointsBox(ulSel.childNodes[i]);
					var editedCustomLearningAidTypeIdCsv = pointsBox.getAttribute(&quot;customLearningAidTypeIdCsv&quot;);
					if (!QuestionCustomLearningAidsState.usesAssignmentSettings(editedCustomLearningAidTypeIdCsv)) {
						hasQuestionLevelSettings = true;
						break;
					}
				}
				var divQuestionLearningAidsNote = document.getElementById(&quot;DivQuestionLearningAidsNote&quot;);
				divQuestionLearningAidsNote.style.display = hasQuestionLevelSettings ? &quot;&quot; : &quot;none&quot;;
			}
		}
	                
		var learningAidsPopup = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup&quot; , &quot;'&quot; , &quot;);
		var learningAidsIEShim = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsIEShim&quot; , &quot;'&quot; , &quot;);       // needed for IE6, lest drop downs will have precedence over DIV
		if (learningAidsPopup)
		{
			learningAidsPopup.style.display = &quot; , &quot;'&quot; , &quot;block&quot; , &quot;'&quot; , &quot;;
			learningAidsIEShim.style.display = &quot; , &quot;'&quot; , &quot;block&quot; , &quot;'&quot; , &quot;;
		}
		else
			alert(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup not found.&quot; , &quot;'&quot; , &quot;);
	}







	function handleNoLearningAids()
	{
		var LearningAidsListLabel = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel&quot; , &quot;'&quot; , &quot;);
		   changeText(LearningAidsListLabel, LOCNS.LocMap.GetRes(&quot;resources&quot;, &quot;None&quot;));
		   var showInReviewModeOnlyDiv = document.getElementById(&quot;ShowInReviewModeOnlyDiv&quot;);
		   showInReviewModeOnlyDiv.className = &quot; , &quot;'&quot; , &quot;disabled&quot; , &quot;'&quot; , &quot;;

		   var reviewModeCheckbox = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot;);
		if (reviewModeCheckbox)
			reviewModeCheckbox.disabled = true;
	}

	function handleExistingLearningAids(learningAidsDisplayString)
	{
		var LearningAidsListLabel = document.getElementById(&quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidsListLabel&quot; , &quot;'&quot; , &quot;);
		changeText(LearningAidsListLabel, learningAidsDisplayString);
		
		var showInReviewModeOnlyDiv = document.getElementById(&quot;ShowInReviewModeOnlyDiv&quot;);
		showInReviewModeOnlyDiv.className = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;

		var reviewModeCheckbox = document.getElementById(&quot;ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot;);
		reviewModeCheckbox.disabled = false;
		
	}

	function changeText(labelControl, text)
	{
		var textNode = document.createTextNode(text);

		if (labelControl.childNodes.length == 0)
			labelControl.appendChild(textNode);
		else
			labelControl.replaceChild(textNode, labelControl.childNodes[0]);

	}

	function closeLearningAidsPopup()
	{
		var learningAidsPopup = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup&quot; , &quot;'&quot; , &quot;);
		var learningAidsIEShim = document.getElementById(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsIEShim&quot; , &quot;'&quot; , &quot;);       // needed for IE6, lest drop downs will have precedence over DIV
		if (learningAidsPopup)
		{
			learningAidsIEShim.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;
			learningAidsPopup.style.display = &quot; , &quot;'&quot; , &quot;none&quot; , &quot;'&quot; , &quot;;
		}
		else
			alert(&quot; , &quot;'&quot; , &quot;ChangeLearningAidsPopup not found.&quot; , &quot;'&quot; , &quot;);
	}

	function LearningAidsDialogOK_onclick()
	{
		closeLearningAidsPopup();

		// CA Ensure that the textual Learning Aids display on the Change Settings screen is updated
		for (var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var checkBoxClientID = eval(&quot; , &quot;'&quot; , &quot;LACheckBoxID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				learningAids[i].enabled = checkBox.checked;
			}
		}
		UpdateLearningAids();
		UpdateReviewModeCheckboxText();
	}

	// XL-29125: Update ReviewModeCheckbox Text for learning aids
	function UpdateReviewModeCheckboxText() {
        var statCrunchEnabled = hasStatCrunchEnabled();
		var isLDBRequired = $(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_chkUseLockdownBrowser&quot; , &quot;'&quot; , &quot;).is(&quot; , &quot;'&quot; , &quot;:checked&quot; , &quot;'&quot; , &quot;);
		var isNewGenStatCrunchFeaturesEnabled = &quot;false&quot;;
		if (statCrunchEnabled) {
            isNewGenStatCrunchFeaturesEnabled = true;
        }

        if (statCrunchEnabled &amp;&amp; isLDBRequired &amp;&amp; isNewGenStatCrunchFeaturesEnabled) {
            SetReviewModeCheckboxLabelText(&quot;Show StatCrunch in quizzes and tests and all selected learning aids in Review mode&quot;);
		}
		else {
            SetReviewModeCheckboxLabelText(&quot;Show in Review mode only&quot;);
        }
	}

	function SetReviewModeCheckboxLabelText(labelText) {
		$(&quot; , &quot;'&quot; , &quot;#ctl00_ctl00_InsideForm_MasterContent_ReviewModeCheckbox&quot; , &quot;'&quot; , &quot;).next().text(labelText);
    }

	// Check if StatCrunch is enabled in Learning aids options
	function hasStatCrunchEnabled() {
        var isSecondaryFlow = false;
        if (isSecondaryFlow) {
			return true;
		}
		else {
			var enabledLearningAids = getActiveLearningAids();
			return hasStatCrunch(enabledLearningAids);
		}
	}

	// Obtain list of active learning aids
	function getActiveLearningAids() {
		return learningAids.filter(aid => (aid.enabled == true)
            &amp;&amp; (aid.visible) &amp;&amp; (!aid.isExerciseSpecific || (aid.isExerciseSpecific &amp;&amp; aid.exerciseExists)));
    }

	function LearningAidsDialogCancel_onclick()
	{
		closeLearningAidsPopup();

		// CA Restore previous choices prior to entering the dialog
		for(var i = 0; i &lt; learningAids.length; i++)
		{
			if (learningAids[i].visible)
			{
				var checkBoxClientID = eval(&quot; , &quot;'&quot; , &quot;LACheckBoxID&quot; , &quot;'&quot; , &quot; + learningAids[i].name);
				var checkBox = document.getElementById(checkBoxClientID);

				checkBox.checked = learningAids[i].enabled;
			}
		}

		UpdateLearningAids();
	}
	function getEnabledLearningAids() {
		return learningAids.filter(aid => aid.enabled == true);
	}
	function hasStatCrunch(learningAidArray) {
		var found = learningAidArray.find(aid => aid.name == &quot; , &quot;'&quot; , &quot;statexplore&quot; , &quot;'&quot; , &quot;);
		return found ? true : false;
    }


rowId = new Object();cbId = new Object();LATableRowIDguidedsolution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[10] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDguidedsolution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[10] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl00_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDexample = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[11] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDexample = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[11] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl01_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDvideo = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[1] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDvideo = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[1] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl02_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDanimation = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[2] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDanimation = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[2] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl03_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtextbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[5] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtextbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[5] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl04_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDconceptbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[31] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDconceptbook = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[31] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl05_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtextbookextras = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[28] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtextbookextras = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[28] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl06_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDmathtools = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[32] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDmathtools = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[32] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl07_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDglossary = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[14] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDglossary = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[14] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl08_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDreview = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[24] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDreview = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[24] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl09_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDstatexplore = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[3] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDstatexplore = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[3] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl10_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDgeogebra = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[26] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDgeogebra = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[26] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl11_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtechhelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[23] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtechhelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[23] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl12_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDextrahelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[25] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDextrahelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[25] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl13_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDcalculator = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[8] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDcalculator = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[8] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl14_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDaskinstructor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[6] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDaskinstructor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[6] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl15_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDconnecttotutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[33] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDconnecttotutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[33] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl16_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDfasttrack = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[4] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDfasttrack = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[4] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl17_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDdemodoc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[7] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDdemodoc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[7] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl18_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDkeyconcepts = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[9] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDkeyconcepts = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[9] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl19_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDgrapher = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[12] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDgrapher = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[12] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl20_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[13] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[13] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl21_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDannuitycalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[15] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDannuitycalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[15] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl22_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDschaums = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[16] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDschaums = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[16] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl23_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDinstructortip = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[17] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDinstructortip = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[17] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl24_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDboosterc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[18] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDboosterc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[18] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl25_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDpowerpoint = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[19] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDpowerpoint = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[19] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl26_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDfinancialcalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[20] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDfinancialcalc = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[20] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl27_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDomexplorertutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[21] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDomexplorertutor = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[21] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl28_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDeconspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[22] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDeconspreadsheet = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[22] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl29_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDcalculateexcel = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[27] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDcalculateexcel = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[27] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl30_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDmorehelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[29] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDmorehelp = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[29] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl31_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDtemplates = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[30] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDtemplates = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[30] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl32_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDbillofrights = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[34] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDbillofrights = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[34] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl33_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDusconstitution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[35] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDusconstitution = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[35] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl34_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDimage = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[36] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDimage = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[36] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl35_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDguidedlecture = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[37] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDguidedlecture = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[37] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl36_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDflashcards = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[38] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDflashcards = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[38] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl37_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;LATableRowIDlabelingactivity = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;
rowId[39] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidDisplayRow&quot; , &quot;'&quot; , &quot;;LACheckBoxIDlabelingactivity = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;
cbId[39] = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_MasterContent_LearningAidDisplayRowRepeater_ctl38_LearningAidCheckBox&quot; , &quot;'&quot; , &quot;;





	
	
		DevTestingCourse
	






    function getHeaderText(){
        return $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        let pageTitle = $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            New Homework  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about New Homework
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    




   
    
    
        
            
                
                    Start
                
                    Select Media and Questions
                
                    Choose Settings
                
                    Define Competency Mapping
            
        
        
    

    
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




	
		
			
				
					Book
					NextGen Media Math Test Book--REAL
				
				
				
	Gradebook Category 
	
						
		Homework

	Homework
					


				
	


				
				
				
					Homework Name
					
				

                

                

				

				

				

				
				

				
	
	Creating assignments for mobile use.    


			
		
	
	
	


	
		
		
			

		

		

				
			    
                
                    
                        Personalization  
                        
                            Options
                        
                        
                        
                        This assignment is personalized for each student using  Skill Builder.
                    
                    
                        
                            
                                
	
                                        
		
                                            Skill Builder
                                        
	
                                    
	
                                        Provide an opportunity for additional targeted practice with   Skill Builder Adaptive Practice.
                                    


                                
	
                                        Personalized Homework
                                    
	
                                        Give automatic credit for questions from sbobjectives mastered in test/quiz:
                                        
		Choose...
		Maintain Order, Preassigned

	
                                         
                                    


                                
                            
                        
                    
                
			    
		
		
		

		

		
		
	
			
				
					This assignment name is already in use in your course, or in one of the members of your course group.Please change it to make it unique.
						
				
			
		

		
		

		
	
		    
		    
		
			    
				    
					    
						    
			
							    
									    
								    
						    
		
					    
				    
			    
		    
	 
            
		

        
            
            The TestGen plug-in is not compatible with the Pearson LockDown Browser.
        
		
		
	
	 





	
	
		
			
				Test Name 
				SampleHW
			
			
				Book 
				NextGen Media Math Test Book--REAL
			
		
		
		
		
			
				
					
						
							Version 3.0 or higher of the TestGen plug-in is required to take this test.
						
						
							Version 3.0 or higher of the TestGen plug-in is required to take this test.
						
						This test will not generate student study plans or item analysis data because it contains one or more questions from a testbank created for another book.
To create tests that generate a study plan or item analysis, download the testbank for this book.
						
						Please click next to continue.
				
			
		
	
		
		
		
		
			Note: You must create or save your test using TestGen version 3.3 or higher.If you are creating tests with the latest version of the TestGen application, you must make sure your students and your lab have installed the latest version of the TestGen plug-in.
			
		
		  
			1. Click Browse to locate and select the TestGen file on your hard drive
			
		
          
               
			We recommend a maximum of 40 questions in your TestGen file.
			
		
		  
			
		
		  
			2. Click Next to upload your TestGen test
		
		
		
	
		
		
		
			
				
					Is your edited test ready to upload?
					
					I have updated my test and I want to upload it now.
					I need to download a copy of the test to make changes to it.
				
				

					
					
						
						Click Browse to locate and select the TestGen file on your hard drive
                        
                         
			            We recommend a maximum of 40 questions in your TestGen file.
			            
		                
						
						
						
						
						
					
					

					
						
						Download the file to your hard drive or preview it using the TestGen Plug-in.
						File:  (0KB)
						Download
						Preview
						
						To upload your edited test, return to this wizard and choose &quot;I have updated my test and I want to upload it now.
						
					
				
			
		
		
	

		
		
			
			1. If you wish to preview the test you are copying or download it to your hard drive, click below.
			File:  (0KB)
			Download
			Preview
			
			2. Click Next to change your test assignment settings or Save to save your copied test
			
		
	

	

	
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }


		
        
    
	

    
        
            
			
		
					
					    
						    Name
						    SampleHW
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
			SBChapter 
			
				1. The Real Number System
				2. Linear Equations and Inequalities in One Variable
				3. Linear Equations and Inequalities in Two Variables; Functions
				4. MarkD Test Chapter
				5. Chapter not in Study Plan
				6. Drag and Drop Questions
				7. Essay questions
				8. StatCrunch
				9. 6-15-21 Adaptive Diagnostic

			
		
		
			SBSection 
			
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions
		
		
			SBObjective 
			
				All SBO

			
		
		

                        
                        
			
                               
                            
		
		
			Availability
			
                                
				All questions
				Questions that are not in the Study Plan
				Questions that are in the Study Plan
				Questions that are screenreader accessible

			
                            
		
		

                        
                        
						
                    
                    																														
                
	
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                             Show GL questions
                                            
                                        
                                             Show Geogebra LTI questions
                                            
                                        
                                             Show Sylva questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   
		
					Questions
		Media
		
					
					
			
	
	

		
			
			
				
					
						
						
						
						Available Questions
						 
						(210)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 7My Selections (7)
							
							
							    
							
								View Assignment Details
								
							
							
							
					
					
						
							
							Points: 7

							
							
							#
							

							
								
									
									
								Question ID / Media

								
									
								
							
							
								
									SBSection / Book Association
									
								
								 
							Estimated time:189m 38s
							
							
					
				
			
	
		
		
			
			
				
					+ (1.1) CustomQuestion1TechH, Rev, Extra Help (preloaded)1.1.29 (preloaded)Anim (preloaded)(P) Anim(P) Anim, eText 3(P) 1.1.39(P) Video, eText 1(P) Video, eText 2(P) 1.1.45(P) 1.1.47(P) 1.1.51(P) 1.1.53(P) 1.1.55(P) 1.1.59(P) Video, Anim, eText 1(P) 1.1.79(P) 1.1.100 RS file testing(P) 1.1.101 RS file testing(P) 1.1.102 RS file testing(P) 1.1.103 RS file testing missing RS(P) 1.1.104 RS file testing - broken vars(P) Graph review only(P) Review only(P) RTD Review only(P) Data Table question(P) Text and Mobile(P) Text only(P) Mobile only(P) No text, no mobile(P) 1.2.17(P) 1.2.19(P) 1.2.23(P) 1.2.25(P) 1.2.27(P) 1.2.31(P) 1.2.33(P) 1.2.35(P) 1.2.37(P) 1.2.41(P) 1.2.43(P) 1.2.47(P) 1.2.51(P) 1.2.55(P) 1.2.57(P) 1.2.61(P) 1.2.63(P) 1.2.67(P) 1.2.71(P) 1.2.79(P) 1.2.81(P) 1.3.13(P) 1.3.15(P) 1.3.17(P) 1.3.19(P) 1.3.23(P) 1.3.25(P) 1.3.29(P) 1.3.31(P) 1.3.33(P) 1.3.35(P) 1.3.37(P) 1.3.39(P) 1.3.43(P) 1.3.45(P) 1.3.47(P) 1.3.49(P) 1.3.51(P) 1.3.53(P) 1.3.59(P) 1.3.61(P) 1.3.63(P) 1.3.65(P) 1.3.73(P) 1.3.75(P) 1.3.77(P) 1.4.1(P) 1.4.5(P) 1.4.19(P) 1.4.23(P) 1.4.25(P) 1.4.27(P) 1.4.31(P) 1.4.33(P) 1.4.35(P) 1.4.37(P) 1.4.39(P) 1.4.41(P) 1.4.43(P) 1.4.45(P) 1.4.47(P) 1.4.49(P) 1.4.51(P) 1.4.53(P) 1.4.55(P) 1.4.57(P) 1.4.59(P) 1.4.61(P) 1.4.63(P) 1.4.65(P) 1.4.67(P) 1.5.7(P) 1.5.11(P) 1.5.19(P) 1.5.23(P) 1.5.31(P) 1.5.33(P) 1.5.41(P) 1.5.45(P) 1.5.47(P) 1.5.49(P) 1.5.51(P) 1.5.53(P) 1.5.55(P) 1.5.59(P) 1.5.75(P) 1.5.77(P) 1.5.83(P) 1.5.85(P) 1.5.91(P) 1.5.93(P) 1.5.95(P) 1.5.103(P) 1.5.111(P) 1.5.113(P) 1.5.119(P) 1.6.13(P) 1.6.15(P) 1.6.21(P) 1.6.25(P) 1.6.27(P) 1.6.29(P) 1.6.35(P) 1.6.39(P) 1.6.41(P) 1.6.43(P) 1.6.45(P) 1.6.49(P) 1.6.55(P) 1.6.57(P) 1.6.63(P) 1.6.67(P) 1.6.69(P) 1.6.75(P) 1.6.77(P) 1.6.79(P) 1.6.81(P) 1.6.83(P) 1.6.85(P) 1.6.87(P) 1.6.89(P) 1.6.93(P) 1.6.95(P) 1.6.97(P) 1.6.103(P) 1.6.105(P) 1.6.109(P) 1.7.1(P) 1.7.5(P) 1.7.9(P) 1.7.15(P) 1.7.17(P) 1.7.19(P) 1.7.21(P) 1.7.23(P) 1.7.25(P) 1.7.27(P) 1.7.29(P) 1.7.39(P) 1.7.43(P) 1.7.47(P) 1.7.55(P) 1.7.57(P) 1.7.59(P) 1.7.61(P) 1.7.63(P) 1.7.65(P) 1.7.67(P) 1.7.69(P) 1.7.73(P) 1.7.75(P) 1.7.77(P) 1.7.79(P) 1.8.1(P) 1.8.3(P) 1.8.5(P) 1.8.9(P) 1.8.11(P) 1.8.13(P) 1.8.15(P) 1.8.17(P) 1.8.19(P) 1.8.21(P) 1.8.23(P) 1.8.25(P) 1.8.27(P) 1.8.29(P) 1.8.31(P) 1.8.33(P) 1.8.45(P) 1.8.47(P) 1.8.49(P) 1.8.53(P) 1.8.55(P) 1.8.67(P) 1.8.69(P) 1.8.71Test GLGeogebra Q1Geogebra Q2 - always correct
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					1(P) 1.1.39Multiply and divide fractions.11m 42s2(P) 1.1.45Multiply and divide fractions.46m 32s3(P) 1.1.47Multiply and divide fractions.22m 16s4(P) 1.1.51Add and subtract fractions.22m 59s5(P) 1.1.53Add and subtract fractions.15m 18s6(P) 1.1.55Add and subtract fractions.18m 8s7(P) 1.1.59Add and subtract fractions.52m 43s
					
					
						
							Click Questions to select questions and click Media to select media. Click Add to include selected questions or media in this assignment.
						
						Click the checkbox next to individual Media file to select them.
Then click Add to include that media in this assignment.
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student homework
		
							View student practice set
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	+ (1.1) CustomQuestion1 (Add and subtract fractions.)
	




	
		
		Name
		SampleHW
		

		
		Book
		
			
				NextGen Media Math Test Book--REAL
				Review Individual Student Settings
			
			
		
		

		
			Availability Options
		
		
			
			
				
					
					
						Available
						
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						Calendar
					
						
							
								
									Title and navigation
								
									
										
									
								
	
		&lt;&lt;&lt;June 2022>>>
	

							
						
					
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

				
			
		
	

							    
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						
							Time picker
						
							[x]
						
							12:00 AM1:00 AM2:00 AM
						
							3:00 AM4:00 AM5:00 AM
						
							6:00 AM7:00 AM8:00 AM
						
							9:00 AM10:00 AM11:00 AM
						
							12:00 PM1:00 PM2:00 PM
						
							3:00 PM4:00 PM5:00 PM
						
							6:00 PM7:00 PM8:00 PM
						
							9:00 PM10:00 PM11:00 PM
						
					
				
			
		
	

							
						
						
							Current course time:
							12:30pm
							
								Time zone:
								(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi
								
	Change...

							
						
					
					
					
						Due
						
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						Calendar
					
						
							
								
									Title and navigation
								
									
										
									
								
	
		&lt;&lt;&lt;June 2022>>>
	

							
						
					
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

				
			
		
	

							    
							
	
		
			RadDatePicker
		
			
				
			
		
			
				
					
						
							Time picker
						
							[x]
						
							12:00 AM1:00 AM2:00 AM
						
							3:00 AM4:00 AM5:00 AM
						
							6:00 AM7:00 AM8:00 AM
						
							9:00 AM10:00 AM11:00 AM
						
							12:00 PM1:00 PM2:00 PM
						
							3:00 PM4:00 PM5:00 PM
						
							6:00 PM7:00 PM8:00 PM
						
							9:00 PM10:00 PM11:00 PM
						
					
				
			
		
	

							
						
					
				
			
		
		
		
		

			
			SBChapter Associations
			
				
	
					Display with assignments from sbchapters:  Change...
					Note: This assignment covers material from sbchapters 1
					
						
							
							Select SBChapters:
							
								

								1. The Real Number System2. Linear Equations and Inequalities in One Variable3. Linear Equations and Inequalities in Two Variables; Functions4. MarkD Test Chapter5. Chapter not in Study Plan6. Drag and Drop Questions7. Essay questions8. StatCrunch9. 6-15-21 Adaptive Diagnostic
							
							
								
									Cancel
	
									OK
	
								
							
						
						
					
				

				
			
		


		
	Scoring Options



		
		
	
	Late submissions
	Allow students to work and change score after due date 
				
					
		
					Require password 
					
					
	
	

					
		
					Require final submission 
						
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								Calendar
							
								
									
										
											Title and navigation
										
											
												
											
										
	
		&lt;&lt;&lt;June 2022>>>
	

									
								
							
	
		
	
		June 2022
	
		
			SMTWTFS
		
	
		
			2930311234
		
			567891011
		
			12131415161718
		
			19202122232425
		
			262728293012
		
			3456789
		
	

	

						
					
				
			
		    
						
			
				
					RadDatePicker
				
					
						
					
				
					
						
							
								
									Time picker
								
									[x]
								
									12:00 AM1:00 AM2:00 AM
								
									3:00 AM4:00 AM5:00 AM
								
									6:00 AM7:00 AM8:00 AM
								
									9:00 AM10:00 AM11:00 AM
								
									12:00 PM1:00 PM2:00 PM
								
									3:00 PM4:00 PM5:00 PM
								
									6:00 PM7:00 PM8:00 PM
								
									9:00 PM10:00 PM11:00 PM
								
							
						
					
				
			
		
					
	
	

					
		
					Deduct late submission penalty
						
			From final score
			Per day, from final score

		  
						Penalty
						
						
			%
			Points

		
					
	
	

					
		
							Apply only to questions scored after the due date
						
	
	

				
			


		
		
	
	Partial Credit
	Allow partial credit on questions with multiple parts 



		
			
			
			
			*** DISCONTINUED
		

		
		
	
	Show Work Questions
	
				
					
						Credit for Question:
					
					
						Automatically score question
						%
						
					
					
						Manually score question
					
					
						Credit for Show Work:
					
					
						Automatically score Show Work
						%
						
					
					
						Manually score Show Work
					
					
						Allow students to access SBHelp Me Solve This and SBView an Example 
					
				
			




		
			Access Controls
		

		
	    
	
	LockDown Browser and Proctoring
	
	            
		Do not use LockDown Browser or automated proctoring
		Require LockDown Browser only (no proctoring)
		Require ProctorU Record+

	
	        

        
        

		
	
	
                LockDown Browser
			
	
				
					
						
							Require the Pearson LockDown Browser
						
						    
		Do not use LockDown Browser or automated proctoring
		Require LockDown Browser only (no proctoring)
		Require LockDown Browser with Respondus Monitor Proctoring

	
                        
					
					
		
						    (Warning: The Pearson LockDown Browser is not compatible with mobile devices or Chromebooks. Tests or quizzes requiring the Pearson Lockdown Browser must be taken on desktop or laptop computers.)
						
	
	
					
		
							Do not allow use of calculator
						
	
	
					
		
							Allow use of basic calculator
						
	
	
					
		
							Allow use of scientific calculator
						
	
	
                    
				    
				        
				            You can edit Respondus Monitor Proctoring settings after saving this assignment.
				        
				    
                    
				    
				        
				            
				                Warning:
				                To enable Respondus Monitor Proctoring, go to Instructor Home to complete the license setup.
				            
				        
                    
				
			


        
	    
	    
	
	
	            Automated Proctoring
	        
	
                
                    
                        
                            Require ProctorU Record+
                        
                    
                    
                        
                            (Warning: Tests or quizzes requiring ProctorU Record+ must be taken on desktop or laptop computers using the Chrome browser.)
                        
                    
                    
                        
                            Sensitivity Settings: 
                            Customize ProctorU Settings
                            
                            
                        
                    
                

            



		
		
	
	IP Address Range
	
				Students must access this assignment from the following IP address range:
				
					  
					Change...
					
					Allow students to review this assignment from any IP address
				
			



		
		
			
			Password
			Required password  
		

		
		
			
			Prerequisites
			
				
					None
						Change...
				
			
		

		
		

		
		
	
	Attempts per Assignment
	Limit number of attempts to  



		
		
	
	Number of AttemptsPer Problem
	
				One Attempt (with 1 retry)
				Unlimited Attempts
			


		
		
	
	Attempts per question
	
				Limit number of times students can work each question to
				
			


		
		

		
		
			
			Incomplete Attempt
			
				
				
					
						
						
					
				
				
				
				

				
				
					
						If attempt is interrupted, students may re-access and complete on their own
						If attempt is interrupted, instructors must enable access
					
				
				
				
				

				
				
					
						Restricted Access: Student cannot resume an incomplete attempt until the instructor enables access to it.
						Blocked Access: Student cannot access other assignments or questions while taking a homework.  If the student does not submit the homework, access to all questions and assignments is blocked until the instructor enables access to or deletes the incomplete homework.
					
				
				
				
				
			
		

		
		
	
	Media Access
	
				Require students to work media before answering questions
			



		
		
	Presentation Options



		
		
	
	Review Steps (test)
	
				This is a test of the alternate text specified in SiteBuilder for Review Steps
			




		
		
	
	Lock Correct Answers
	
				Indicate and lock all correct answers after each Check Answer
			



		
		
	
	Question variation
	
				Use the same question values for all students
			



		
		
	
	Save Values
	
				Save question values and student answers
			



		
		
	
	Printing
	
				Allow students to print this homework assignment
			



		
		
	
	Time Limit
	
				Test time allowed (minutes): 
				
		
					
					     Show time remaining during test
				
	
			


		
		
	
	Question display
	
				Scramble question order for each student 
				
		
				Shuffle answer options 
				Randomize variables 
				
	
			


		
	
	Learning Aids
	
				
					
					
						
							
								
									Select Learning Aids:

									
										
											
													
		
															
															
														
		
		SBHelp Me Solve This
	
	
												
													
		
															
															
														
		
		SBView an Example
	
	
												
													
		
															
															
														
		
		SBVideoooo
	
	
												
													
		
															
															
														
		
		SBAnimation
	
	
												
													
		
															
															
														
		
		SBTextbook
	
	
												
													
		
															
															
														
		
		Concept Review
	
	
												
													
		
															
															
														
		
		SBTextbook Extra
	
	
												
													
		
															
															
														
		
		Math Tools
	
	
												
													
		
															
															
														
		
		SBGlossary
	
	
												
													
		
															
															
														
		
		SBReview
	
	
												
													
		
															
															
														
		
		StatCrunch
	
	
												
													
		
															
															
														
		
		Geogebra

	
	
												
													
		
															
															
														
		
		Tech Help
	
	
												
													
		
															
															
														
		
		SBExtra Help
	
	
												
													
												
													
		
															
															
														
		
		Ask My Instructor
	
	
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
		
															
															
														
		
		Instructor Tip
	
	
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
													
												
										
									

									
										Note: Instructor has customized learning aids for select questions. Changes made here will not affect these questions.
									
									
									
									
										
											Cancel
	
											  OK
	
										
									
								
							
						
					

				

			    SBHelp Me Solve This, SBView an Example, SBAnimation, Concept Review, Math Tools, Ask My Instructor, Instructor Tip      
			    
			        Change....
			    
				
			    
				
					     Show in Review mode only
				
			



	
	Graphing
	
				Allow students to move points by typing coordinates
			



	
		
			Review Options
		

		
		
			
			Results Display
			
				
	Test Summary shows test score and question results
	Test Summary shows test score only
	Hide score and question results


				
			
		

		
		
			
			Reviewing Test
			
			
				
	Student can review assignment any time after submitting
	Student can review assignment only immediately after submitting
	Student can review submitted assignment any time after due date
	Student can never review submitted assignment


			
		

		
		
			
			Feedback
			Allow students to view feedback while reviewing
		

		
		
	
	Study Plan
	Results are used to update the study plan
				
					
					     Link to mastered and needs study topics
				
			



		
		
			
			Answer Display
			
				Only show student&quot; , &quot;'&quot; , &quot;s answers when reviewing
				Show student&quot; , &quot;'&quot; , &quot;s and correct answers when reviewing
			
		

		 
		
	
	Print
	Allow students to print the test with the correct answers and their answers while reviewing 



		
		
	Learning Path


		
		
	
	Mastery Score
	
				Minimum score for mastery
				%

			



		
		
	Other


		
		
	
	Importing
	Allow other instructors to import this assignment 


	    
	
   

	
	
		
		
		
	    
			
				
					Edit Test Summary Message
				
				
					Customize the message given to students at the end of the test.
					
					
						
						


					
				
				
					Cancel
					Ok
				
			
		
	
	



	
		
			NameSampleHW
			BookNextGen Media Math Test Book--REAL
		
	
	

	
		
			Select criteria for setting content to Needs Study.   If you wish to remove content from the student&quot; , &quot;'&quot; , &quot;s Learning Path but keep it available in Extra Practice, choose Hide from Learning Path. If a content area is not covered in your course, you can leave its criteria blank.
			
				
					
						
						Module
						Topic
						Covered on Test
						Criteria for setting to  Needs Study
						Score
					
					
						 
						 
						 
						 
						


						

   %
					
					
						 
						 
						 
						 
						Apply to Selected

						Apply to Selected

					
				
			
		
	

	
		
				Cancel

			    
					Next

					
				
			    
			        
						Save &amp; Assign

			        
			    
				
					Save

				
			    Back

				
			
		
	
	
	
		

		
			
			
				Cancel

				No

				Yes

			
			
				No, Just Save

				Yes, Take Me There

			
				
			
		
	
	
	
		
		DEV: 404 Save
		
	






    var YesNoCancelPop = {
        _ActionHandler: null,
        Show: function (header, msg, actionHandler, options) {
            //Register popup events
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnYesId&quot;, function () { YesNoCancelPop.DoAction(&quot; , &quot;'&quot; , &quot;Yes&quot; , &quot;'&quot; , &quot;); });
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnNoId&quot;, function () { YesNoCancelPop.DoAction(&quot; , &quot;'&quot; , &quot;No&quot; , &quot;'&quot; , &quot;); });
            $(&quot;#mainModal&quot;).on(&quot;click&quot;, &quot;#btnCancelId&quot;, function () { YesNoCancelPop.DoAction(&quot; , &quot;'&quot; , &quot;Cancel&quot; , &quot;'&quot; , &quot;); });
            //Show popup and apply header,body and button text
            $(&quot; , &quot;'&quot; , &quot;#mainModal&quot; , &quot;'&quot; , &quot;).modal(&quot; , &quot;'&quot; , &quot;show&quot; , &quot;'&quot; , &quot;);
            $(&quot; , &quot;'&quot; , &quot;#headerTxt&quot; , &quot;'&quot; , &quot;).html(header);
            $(&quot; , &quot;'&quot; , &quot;#bodyTxt&quot; , &quot;'&quot; , &quot;).html(msg);
            $(&quot; , &quot;'&quot; , &quot;#btnYesId&quot; , &quot;'&quot; , &quot;).html(options.yesText);
            $(&quot; , &quot;'&quot; , &quot;#btnNoId&quot; , &quot;'&quot; , &quot;).html(options.noText);
            $(&quot; , &quot;'&quot; , &quot;#btnCancelId&quot; , &quot;'&quot; , &quot;).html(options.cancelText);
            //Register parent function in popup side
            if (actionHandler != null)
                YesNoCancelPop.SetActionHandler(actionHandler);

            //Show popup buttons 
            YesNoCancelPop.ShowButtons(options);  
            YesNoCancelPop.CustomStyles(options);
        },
        SetActionHandler: function (actionHandler) {
            YesNoCancelPop._ActionHandler = actionHandler;
        },
        //Get popup (yes/no/cancel) event and pass it to parent
        DoAction: function (action) {
            $(&quot; , &quot;'&quot; , &quot;#mainModal&quot; , &quot;'&quot; , &quot;).modal(&quot; , &quot;'&quot; , &quot;hide&quot; , &quot;'&quot; , &quot;);
            if (YesNoCancelPop._ActionHandler)
                YesNoCancelPop._ActionHandler(action);
        },
        ShowButtons: function (options) {
            options.showYesButton ? $(&quot;#btnYesId&quot;).show() : $(&quot;#btnYesId&quot;).hide();
            options.showNoButton ? $(&quot;#btnNoId&quot;).show() : $(&quot;#btnNoId&quot;).hide();
            options.showCancelButton ? $(&quot;#btnCancelId&quot;).show() : $(&quot;#btnCancelId&quot;).hide();
        return true;
        }, CustomStyles: function (options) {
            if (options.width) {
                $(&quot;#innerModel&quot;).width(options.width);
                $(&quot;#popup-hr-bottom&quot;).width(options.width - 40);
                $(&quot;#popup-hr-top&quot;).width(options.width - 40);
            }
            if (options.hideTitle) {
                options.hideTitle ? $(&quot;#headerTxt&quot;).hide() : $(&quot;#headerTxt&quot;).show();
                options.hideTitle ? $(&quot;#popup-hr-top&quot;).hide() : $(&quot;#popup-hr-top&quot;).show();
            }

            if (options.showYesButton || options.showNoButton) {
                $(&quot;#btnGroupCancel&quot;).css(&quot;margin-left&quot;, &quot;10px&quot;);
            }

            if (options.btnYesNo &amp;&amp; options.btnYesNo.alignRight) {
                $(&quot;#btnGroupYesNo&quot;).css(&quot;float&quot;, &quot;right&quot;);
                $(&quot;#btnGroupCancel&quot;).css(&quot;margin-left&quot;, &quot;0&quot;);
            }

            if (options.btnCancel &amp;&amp; options.btnCancel.alignRight){
                $(&quot;#btnGroupCancel&quot;).css(&quot;float&quot;, &quot;right&quot;);
            }

            if (options.paddingTop)
                $(&quot;#modalText&quot;).css(&quot;padding-top&quot;, options.paddingTop);

            if (options.paddingBottom)
                $(&quot;#modalText&quot;).css(&quot;padding-bottom&quot;, options.paddingBottom);

            if (options.btnYesGray)
                $(&quot;#btnYesId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);
            if (options.btnNoGray)
                $(&quot;#btnNoId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);
            if (options.btnCancelGray)
                $(&quot;#btnCancelId&quot;).removeClass(&quot;btn-primary&quot;).addClass(&quot;btn-default&quot;);

            if (options.btnYesPrimary)
                $(&quot;#btnYesId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            if (options.btnNoPrimary)
                $(&quot;#btnNoId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            if (options.btnCancelPrimary)
                $(&quot;#btnCancelId&quot;).removeClass(&quot;btn-default&quot;).addClass(&quot;btn-primary&quot;);
            
        return true;
        }

        
    }



            
            
                
                
                    
                        
                            
                        
                        
                        
                            
                        
                        
                        
                            
                                
                                    
                                    
                                
                                
		                            
                                
                            
                        
                          
                
            
 

    



                                                                    
                                                            &quot;))]</value>
      <webElementGuid>e370c82f-d83d-4891-a00e-7be616f0452e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
