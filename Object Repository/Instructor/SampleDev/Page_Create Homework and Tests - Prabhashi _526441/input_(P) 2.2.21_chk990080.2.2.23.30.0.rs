<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 2.2.21_chk990080.2.2.23.30.0</name>
   <tag></tag>
   <elementGuidId>21aee965-01c5-47e7-9f2a-49e559dd9ec9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.2.2.23.30.0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>93e37fc0-4d51-4c59-8073-e404906ad698</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>5df149bf-c31e-42ed-966a-0738bdf462e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.2.2.23.30.0</value>
      <webElementGuid>17d598d5-6dc2-48b2-99b2-f6112c90075a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 2.2.23 (Solve equations using the multiplication property.)</value>
      <webElementGuid>63631f75-42fa-4937-92c6-3139ea1bbfff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.2.2.23.30.0&quot;)</value>
      <webElementGuid>6fa0c81a-2f29-4145-a6cd-bf6800e06268</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.2.2.23.30.0']</value>
      <webElementGuid>5aced258-0956-4b72-8959-a9b5a9c66977</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.2.2.23.30.0']/div/div[2]/input</value>
      <webElementGuid>b1b62729-f553-4f90-9406-2c309e005c2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[8]/div/div[2]/input</value>
      <webElementGuid>166530e4-3486-4794-aa9a-48fb5aa24603</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.2.2.23.30.0' and @title = '(P) 2.2.23 (Solve equations using the multiplication property.)']</value>
      <webElementGuid>6cbe9df8-a71e-47f7-90e6-b1abe85d1ff6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
