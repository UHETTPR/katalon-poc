<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1. The Real Number System2. Linear E_06cce4</name>
   <tag></tag>
   <elementGuidId>c30f1850-c5cb-4a9e-826e-c0331afefb06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListChapter']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListChapter</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>5f91d0db-9596-48b2-acde-1695905bb9d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter</value>
      <webElementGuid>f95e59e6-dea6-4957-80c8-00da31fdb45c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter\',\'\')', 0)</value>
      <webElementGuid>97d0f5b0-1e1b-4d78-a0a8-cfce6c4dd8a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListChapter</value>
      <webElementGuid>5fefafb8-8206-4ee4-b750-40ca01f1b43c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>e052742d-5aff-4b81-ae30-4532fd3fe7fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>0ba10630-0381-46e6-ab53-f2f8bfbd512e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				1. The Real Number System
				2. Linear Equations and Inequalities in One Variable
				3. Linear Equations and Inequalities in Two Variables; Functions
				4. MarkD Test Chapter
				5. Chapter not in Study Plan
				6. Drag and Drop Questions
				7. Essay questions
				8. StatCrunch
				9. 6-15-21 Adaptive Diagnostic

			</value>
      <webElementGuid>85defe67-40d0-4e32-a954-13042908ad63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListChapter&quot;)</value>
      <webElementGuid>1d4d9c13-c6c3-446d-8542-77aeb26ed58e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListChapter']</value>
      <webElementGuid>e69220fe-7f6b-4bf8-bb2e-d62c5284cd5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrChapter']/td[2]/div/select</value>
      <webElementGuid>aa897cdc-b7ea-4cad-91ba-690731f6f3c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[1]</value>
      <webElementGuid>1d0b8555-b536-4606-95f8-579aee397088</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change...'])[1]/following::select[1]</value>
      <webElementGuid>a9aa068a-f7e6-4d68-8cd9-bfa231459f9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/preceding::select[1]</value>
      <webElementGuid>1588e2b5-a0b4-4df1-aa67-14caa7f49b58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[2]</value>
      <webElementGuid>ae671b05-0d95-4b77-94f7-fc15e5a97d11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>f0aaa291-7fb7-4004-8099-2829a9cc13f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter' and @id = 'DropDownListChapter' and (text() = '
				1. The Real Number System
				2. Linear Equations and Inequalities in One Variable
				3. Linear Equations and Inequalities in Two Variables; Functions
				4. MarkD Test Chapter
				5. Chapter not in Study Plan
				6. Drag and Drop Questions
				7. Essay questions
				8. StatCrunch
				9. 6-15-21 Adaptive Diagnostic

			' or . = '
				1. The Real Number System
				2. Linear Equations and Inequalities in One Variable
				3. Linear Equations and Inequalities in Two Variables; Functions
				4. MarkD Test Chapter
				5. Chapter not in Study Plan
				6. Drag and Drop Questions
				7. Essay questions
				8. StatCrunch
				9. 6-15-21 Adaptive Diagnostic

			')]</value>
      <webElementGuid>7217d19a-828b-432b-a314-b669ba97e531</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
