<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) 1.3.31_chk990080.1.3.33.70.0</name>
   <tag></tag>
   <elementGuidId>25f69c30-e515-41c0-809a-998e4b045ed0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.1.3.33.70.0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>04abf973-126b-4ec4-94a7-37dbb78fa0ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>79bcab3c-bf56-40f3-9c17-671a71682e1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.1.3.33.70.0</value>
      <webElementGuid>f2add4fa-7486-4ac7-bc12-c0a8b22d1755</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 1.3.33 (Evaluate algebraic expressions.)</value>
      <webElementGuid>ab86aeb2-f9b4-410a-89de-7cf5a1ca30cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.1.3.33.70.0&quot;)</value>
      <webElementGuid>b6736d1b-9274-491f-80a5-10f61fc49a71</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.1.3.33.70.0']</value>
      <webElementGuid>aae91ab8-c4c4-4e09-8d5b-1bde0de078d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.1.3.33.70.0']/div/div[2]/input</value>
      <webElementGuid>831254a3-06be-4cd1-88c5-d7c05c236b72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[9]/div/div[2]/input</value>
      <webElementGuid>b9e4b085-cd0a-4c4d-8998-9fd38c2fd751</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.1.3.33.70.0' and @title = '(P) 1.3.33 (Evaluate algebraic expressions.)']</value>
      <webElementGuid>591a3ea6-1c60-4879-914b-19b9bde376b8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
