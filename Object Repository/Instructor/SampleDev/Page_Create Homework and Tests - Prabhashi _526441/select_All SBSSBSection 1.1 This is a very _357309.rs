<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 1.1 This is a very _357309</name>
   <tag></tag>
   <elementGuidId>8cd8a49f-49a7-4234-99f7-b0b980deacde</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b6174e47-7006-48df-9a45-1508df310e18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>f1771556-c5bc-4de5-860b-c06ff4978307</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>21283897-874a-42d0-978c-3b7049a93eb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>65d530a9-66c2-439f-bb23-13111bbdfa99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>9f533c69-7171-4141-82d2-5e2686806f66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>eaa9bc96-46f5-4dce-a489-e43e3b1e4cf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions</value>
      <webElementGuid>85e2ff55-bfb4-418e-8f12-d404a34802d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>de9b668f-40fb-4ba8-ae1f-a9dd376bf8a6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>4e40fa5e-d4ac-4eaf-84bb-707b8b0a2c07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>f0455bb7-d4e2-4da5-84cf-848c3e253cb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>a0c92953-ee5a-4689-a814-9d0eb481ffaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>da1c8d72-08e7-4eda-b971-5e063cf1e400</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>d50e09f1-a828-4c8d-83d7-07e2ca9b76a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>6403affe-dd51-405c-927f-62f292cc11cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>4a4fa192-a121-4ee5-8e52-70b313bf53df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions' or . = '
				All SBS
				
				
				
				
				
				
				
				

			SBSection 1.1: This is a very long section title.  This is a very long section title....SBSection 1.2: Exponents, Order of Operations, and InequalitySBSection 1.3: Variables, Expressions, and EquationsSBSection 1.4: Real Numbers and the Number LineSBSection 1.5: Adding and Subtracting Real NumbersSBSection 1.6: Multiplying and Dividing Real NumbersSBSection 1.7: Properties of Real NumbersSBSection 1.8: Simplifying Expressions')]</value>
      <webElementGuid>f41694d8-53aa-42db-9d15-c76f190050f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
