<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(P) Anim, eText 3_chk990080.1.1.39.21.0</name>
   <tag></tag>
   <elementGuidId>8ef3f086-8f51-4edd-986d-bd0c7857512c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990080.1.1.39.21.0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e064461e-2d40-424a-bd07-e5e5468b21c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>25db94ee-5188-40cb-a653-eb4341ad6539</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990080.1.1.39.21.0</value>
      <webElementGuid>430d2616-722a-4bb7-910b-9aa1a0427017</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(P) 1.1.39 (Multiply and divide fractions.)</value>
      <webElementGuid>77076793-0bfb-46aa-a11d-acaab94215ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990080.1.1.39.21.0&quot;)</value>
      <webElementGuid>6a255e85-cba9-4e6c-9316-9f2786a716b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990080.1.1.39.21.0']</value>
      <webElementGuid>9417e364-3d65-408b-a60d-1d6a5f01fd07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990080.1.1.39.21.0']/div/div[2]/input</value>
      <webElementGuid>d9714aeb-1874-4c58-aecf-5212d7e205b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/div/div[2]/input</value>
      <webElementGuid>1822b79a-3dec-4654-b746-9e325aaa6528</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990080.1.1.39.21.0' and @title = '(P) 1.1.39 (Multiply and divide fractions.)']</value>
      <webElementGuid>42f7444c-4647-4947-96ed-e5c083b2cdf6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
