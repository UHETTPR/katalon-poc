<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose --StandardCoordinatorMember</name>
   <tag></tag>
   <elementGuidId>5d691be3-1408-43d5-8c95-83a37d47e115</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpCourseType']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpCourseType</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f5dfba59-1550-4ebc-b2be-efc20f0a86f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpCourseType</value>
      <webElementGuid>e98ccba6-4499-4359-9cff-6f9508e411cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpCourseType</value>
      <webElementGuid>a7a341fb-81c0-4b87-8582-f811aeca5ea4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>SetCourseType();</value>
      <webElementGuid>30996697-f9f4-4bf4-a191-3cf801825c79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>6e1ac22c-8e27-4885-a3e1-9caf9e3e8768</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	-- Choose --
	Standard
	Coordinator
	Member

</value>
      <webElementGuid>8ec36901-0917-4e8c-84ad-037a3248e39c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpCourseType&quot;)</value>
      <webElementGuid>fbb769b8-b576-495f-b6d7-629625b29a45</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpCourseType']</value>
      <webElementGuid>cdc58082-72a2-40df-a2e9-9ed66dd6be24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivContainer']/table/tbody/tr/td[2]/select</value>
      <webElementGuid>70c38864-8d9c-4cfc-90b7-b89cf222d808</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Type of course to create'])[1]/following::select[1]</value>
      <webElementGuid>7fbc2939-9329-42d9-ad2b-64184896d471</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Aids and Test Options'])[2]/following::select[1]</value>
      <webElementGuid>0f9f3685-0a17-4664-b6c9-576351e97573</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about course types'])[1]/preceding::select[1]</value>
      <webElementGuid>7f457349-1ba9-43e2-acd6-7be87ace3672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course name'])[1]/preceding::select[1]</value>
      <webElementGuid>96bf2662-c5df-459e-b65d-1ae1dedc2d9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>8cafaa4e-56d6-46ae-8b2c-293749d404b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpCourseType' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpCourseType' and (text() = '
	-- Choose --
	Standard
	Coordinator
	Member

' or . = '
	-- Choose --
	Standard
	Coordinator
	Member

')]</value>
      <webElementGuid>d16227a4-a8ab-48c5-83e6-bf063810e6ec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
