<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --   eT1 Math Test Bo_84b084</name>
   <tag></tag>
   <elementGuidId>83f9959f-b1e5-4e57-9303-7eec2a67ccee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>d479c90a-9c1d-4b38-897a-9fd6a4ac3211</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
      <webElementGuid>b61add89-49f1-443d-953f-923f74527b88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      <webElementGuid>1ce05830-ffa7-421f-8aa1-256e2989ec4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
      <webElementGuid>72fab13a-860e-4226-bf24-c6830401361d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>9be27c74-b52b-4d23-ba17-cb69b4027102</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				  -- Choose Book --  
				
				
				
				 eT1 Math Test Book 10 - Non Course Aware 10
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				A&amp;B All Browser Check book
				A&amp;B Flash Browser Check book
				A&amp;B Realplayer Browser Check book
				A&amp;B Testgen Browser Check book
				AA Site Builder KT-Dula
				AaSam Site Builder 10
				AB_Sam_regressuk
				ACR NextGen Media Math Test Book--ACE
				Adams: Calculus, 6e ENHANCED
				Adaptive HW Math Test Book
				ADP: Algebra II Online Course
				ADP: Algebra II Online Course (old/original)
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Akst: Basic Mathematics through Applications, 3e ENHANCED
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Fundamental Mathematics through Applications, 3e ENHANCED
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Introductory Algebra through Applications, 1e ENHANCED
				Akst: Introductory Algebra Through Applications, 2e
				Akst: Introductory Algebra Through Applications, 3e
				ala smoke 6-11
				Alec's Delta Book (VOY)
				Alec's Delta Book:  Non-combo
				Alicia's 2level test book
				Alicia's Berk book
				Alicia's blah book
				Alicia's blah book
				Alicia's blah book
				Alicia's blah book
				Alicia's blah book
				Alicia's blah book REAL MOBILE DEVICES
				Alicia's book June 2013, testing admin imports
				Alicia's fake finance book for testing
				Alicia's MNL Pharmacology XL style
				Alicia's new book 5-5-13
				Alicia's new sbnetdemo test book
				Alicia's other June 2013 book for admin import testing
				Alicia's sbasp book (for sbnet testing)
				Alicia's sbtest book
				Alicia's sbtest book 2000
				Alicia's sbtest book COPY DO NOT USE
				Alicia's small test book
				Alicia's small test book
				Alicia's testing book for Book part importing
				Alicia's tiny test book
				Alicias Ex No Bug Test
				All Wizard Book
				Alleyoop College Prep 2011
				Alleyoop College Prep 2012
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, Expanded 7e ENHANCED
				Angel: Elementary Algebra for College Students, 7e ENHANCED
				Angel: Elementary Algebra with Early Graphing for College Students, 3e ENHANCED
				Angel: Elementary and Intermediate Algebra for College Students, 3e (ENHANCED)
				Angel: Elementary and Intermediate Algebra for College Students, 4e
				Angel: Intermediate Algebra for College Students, 7e (ENHANCED)
				AusNextGenRH1CRM
				Automated Preload Master Course book
				Automated Test Book 1 - NextGen Media Math Test Book--REAL
				Automated Test Book 3 - MyMathLab test book A3
				Bade/Parkin: Essential Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics
				Bade/Parkin: Foundations of Macroeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics
				Bade/Parkin: Foundations of Microeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, MATH REVIEW
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Econ, Social &amp; Life Sciences, 10e ENHANCED
				Barnett: Calculus for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics, 10e ENHANCED
				Barnett: Finite Mathematics, 10e ENHANCED
				Barnett: Finite Mathematics, 12e
				BBEP: Algebra &amp; Trigonometry, 2nd Edition
				BBEP: Algebra &amp; Trigonometry, A Unit Circle Approach
				BBEP: College Algebra, 2nd Edition
				BBEP: Precalculus: Graphs &amp; Models 2nd Edition
				BBEP: Precalculus: Graphs &amp; Models, A Unit Circle Approach
				BBEP: Trigonometry: Graphs &amp; Models, 2nd Edition
				Beecher: Algebra &amp; Trigonometry
				Beecher: Algebra &amp; Trigonometry, 2e ENHANCED
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: College Algebra
				Beecher: College Algebra, 2e ENHANCED
				Beecher: Precalculus, 2e ENHANCED
				Beecher: Precalculus, 3e
				Bennett: Statistical Reasoning for Everyday Life
				Bennett: Using and Understanding Mathematics, 3e ENHANCED
				Bennett: Using and Understanding Mathematics, 4e
				Berenson: Basic Business Statistics, 13e
				Betsy Learning Aids 1-24-15
				Betsy No Publisher test book
				Betsy Player Function Copy
				Betsy Player Function Copy
				Betsy Smoketest 1-19-10
				Betsy Smoketest 11-19-09
				Betsy Smoketest 12-20
				Betsy Smoketest 3-3-10
				Betsy Smoketest 9-28
				Betsy Smoketests 5-4
				Billstein: A Problem Solving Approach to Math for Elem School Teachers, 8e
				Billstein: Mathematics for Elementary School Teachers, 10e
				Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Basic Mathematics with Early Integers ENHANCED
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e ENHANCED
				Bittinger: Basic Mathematics, 11e
				Bittinger: Basic Mathematics, 8th Edition
				Bittinger: Basic Mathematics, 9e
				Bittinger: Basic Mathematics, 9th Edition
				Bittinger: Calculus and Its Applications, 10e
				Bittinger: Calculus and Its Applications, 8e ENHANCED
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: Calculus and Its Applications, 9e DEMO
				Bittinger: College Algebra: Graphs &amp; Models, 3e ENHANCED
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 5th Edition
				Bittinger: Developmental Mathematics, 6th Edition
				Bittinger: Developmental Mathematics, 6th Edition ENHANCED
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 5e
				Bittinger: Elementary Algebra, Concepts &amp; Applications, 6th Edition
				Bittinger: Elementary Algebra, Concepts &amp; Applications, 7e
				Bittinger: Elementary Algebra, Concepts and Applications, 8e
				Bittinger: Elementary Algebra: Graphs &amp; Models, 1e ENHANCED
				Bittinger: Elementary and Intermediate Algebra, Concepts &amp; Applications, 2e
				Bittinger: Elementary and Intermediate Algebra, Concepts &amp; Applications, 3e
				Bittinger: Elementary and Intermediate Algebra, Graphs &amp; Models, 2nd Edition
				Bittinger: Elementary and Intermediate Algebra, Graphs and Models
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3rd Edition
				Bittinger: Fundamentals of College Algebra
				Bittinger: Intermediate Algebra, 8th Edition
				Bittinger: Intermediate Algebra, 9th Edition
				Bittinger: Intermediate Algebra, Alternate Version, 8th Ed
				Bittinger: Intermediate Algebra, Concepts &amp; Applications, 5e
				Bittinger: Intermediate Algebra, Graphs &amp; Models
				Bittinger: Intermediate Algebra: Concepts &amp; Applications, 6th Edition
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2nd Edition
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Introductory &amp; Intermediate Algebra, 2nd Edition
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 8th Edition
				Bittinger: Introductory Algebra, 9th Edition
				Bittinger: Introductory and Intermediate Algebra: A Combined Approach
				Bittinger: Prealgebra and Introductory Algebra
				Bittinger: Prealgebra custom-Houston CC
				Bittinger: Prealgebra, 3rd Edition
				Bittinger: Prealgebra, 4th Edition
				Bittinger: Prealgebra, 4th Edition ENHANCED
				Bittinger: Prealgebra, 5e ENHANCED
				Bittinger: Precalculus, Graphs &amp; Models, 3e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Blah book TOL-BG
				Blitzer:  College Algebra, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 2e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra for College Students, 5e ENHANCED
				Blitzer: College Algebra Essentials, 2e ENHANCED
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra: An Early Functions Approach, 2e
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Intermediate Algebra, 4e ENHANCED
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra, 4e ENHANCED
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Precalculus, 2e ENHANCED
				Blitzer: Precalculus, 4e
				Blitzer: Precalculus, 5e
				Blitzer: Thinking Mathematically, 3e ENHANCED
				Blitzer: Thinking Mathematically, 4e
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 3e
				Bock: Stats: Modeling the World
				Bock: Stats: Modeling the World, 3e
				Briggs/Cochran: Calculus
				Briggs/Cochran: Calculus Early Transcendentals
				Briggs/Cochran: Calculus Early Transcendentals DEMO
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus, 2e
				Briggs/Cochran: Calculus, 3e
				CA Test Comment 16517 - hassamplehw/hassampletests
				CA TEST Comment 21460 - Knewton  Available
				CA TEST Finance Book (ngfinance)
				CA TEST Knewton Book 1
				CA TEST Knewton Book 2 - smaller
				CA TEST Math 1
				CA TEST Math 3
				CA TEST NG Math Media 1
				cacomment14605
				Carson: Elementary Algebra with Early Systems of Equations, ENHANCED
				Carson: Elementary Algebra, 2e ENHANCED
				Carson: Elementary Algebra, 3e
				Carson: Elementary Algebra, ENHANCED
				Carson: Elementary and Intermediate Algebra, ENHANCED
				Carson: Intermediate Algebra, ENHANCED
				Carson: Prealgebra
				Carson: Prealgebra, 3e
				catestreqc12327
				CCNG Trigsted Beginning Algebra Demo Test Book
				CCNG Trigsted Intermediate Alg Test Book
				Chalk Test Course for Deltanet Server (Bittinger Prealg 5e 2008)
				Chris Feb Tier 3 Test Math
				Chris Feb Tier 3 Test Math
				Chris Feb Tier 3 Test Math XL [Laksith]
				Chris Feb Tier 3 Test Math XL [Laksith] C
				Chris Feb Tier 3 Test Math XL [Laksith]_D
				Chris Feb Tier 3 Test Math XL [Laksith]_E
				Chris Feb Tier 3 Test Math XL [Laksith]_F
				Chris Feb Tier 3 Test Math XL [Laksith]_G
				Chris Feb Tier 3 Test Math XL [Laksith]_H
				Chris Feb Tier 3 Test Math XL [Laksith]_L
				Chris Feb Tier 3 Test Math_J
				Chris Feb Tier 3 Test Math_upe5
				Chris T3 Test Feb
				Cleaves:  Business Math, 8e
				Cleaves: Business Math, 9e
				Cleaves: College Mathematics, 10e
				College of Central Florida: MAT0012/MAT0024 Prealgebra &amp; Introductory Algebra
				Consortium for Foundation Math: Algebraic, Graphical &amp; Trig. Problem Solving
				Consortium: Algebraic, Graphical &amp; Trigonometric Problem Solving 2e
				Consortium: Intro to Algebraic, Graphical &amp; Numeric Problem Solving, 2nd Edition
				Consortium: Prealgebra, 1st Edition
				Content Test Book from SBNetPreview (mxlpreview)
				Conversion of Tarbuck 11e
				De Veaux Velleman Intro Stats 1e - MOCK
				De Veaux: Intro Stats
				De Veaux: Intro Stats, 2e
				De Veaux: Intro Stats, 3e
				De Veaux: Intro Stats, 4e
				De Veaux: Stats: Data and Models ENHANCED
				Demana: Precalculus: Functions &amp; Graphs, 4th Edition
				Demana: Precalculus: Functions and Graphs, 5th Edition
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				Drag and Drop Lessons
				Dugopolski: College Algebra &amp; Trigonometry, 3rd Edition
				Dugopolski: College Algebra &amp; Trigonometry, 5e
				Dugopolski: College Algebra, 3rd Edition
				Dugopolski: College Algebra, 4e
				Dugopolski: College Algebra, 5e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus ENHANCED Re-release
				Dugopolski: Precalculus with Limits: Functions &amp; Graphs
				Dugopolski: Precalculus, 3rd Edition
				Dugopolski: Precalculus: Functions and Graphs
				Dugopolski: Precalculus: Functions and Graphs 2e
				Dugopolski: Trigonometry
				Dugopolski: Trigonometry, 2e
				DV Italian Book L2
				DV Italian Book L2
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Elementary Algebra: Concepts &amp; Apps: 5e
				Eranga Palette Test
				Essay Book 1 
				eT1 Math Test Book
				eT1 Math Test Book - PPE
				eT1 Math Test Book 3
				eT1 Math Test Book K2
				eT1 Math Test Book Kethees_New
				eT1 Math Test Book Kethish
				eT1 Math Test Book Lak2
				eT1 Math Test Book Lak3
				eT2 Disaggregated - NON course aware test book
				eT2 Math Test Book
				eT2 Math Test Book - CERT
				eT2 Math Test Book - CERT with Print ID
				eT2 Math Test Book - PPE
				eT2 Math Test Book - PPE RH
				eText Integration Trigsted College Alg Test Book
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e Media Update
				Florida CLAST Guide
				Gaze: Thinking Quantitatively: Communicating with Numbers, 2e
				Georgia Perimeter College Math 0096: Essential Mathematics
				Gitman:  Principles of Managerial Finance, 11th Edition UPDATE
				Gitman: Principles of Managerial Finance Brief, 3rd Edition
				Gitman: Principles of Managerial Finance, 10th Edition
				Global Edition NextGenRH1CRM International Edition
				Gould: Essential Statistics: Exploring the World Through Data
				Greenwell: Calculus for the Life Sciences
				Harrisburg Area CC: Martin-Gay Introductory Algebra
				Harshbarger: College Algebra In Context, 2e ENHANCED
				Harshbarger: College Algebra In Context, 3e
				Harshbarger: College Algebra in Context, ENHANCED
				Hass: University Calculus Alternate Edition
				Hass: University Calculus ENHANCED
				Hornsby: A Graphical Approach to Precalculus with Limits, A Unit Circle App, 4e
				Hornsby: College Algebra &amp; Trigonometry, 3rd Edition
				Hornsby: College Algebra, 2nd Edition
				Hornsby: College Algebra, 3rd Edition
				Hornsby: Precalculus with Limits, 3rd Edition
				Hornsby: Precalculus, 3rd Edition
				Hubbard: Money, the Financial System, and the Economy, 5th Edition, MATH REVIEW
				Hutchison: Mathematics for New Technologies
				Intellipro Small Book
				Intellipro Test Book - Niro
				Intellipro Very Small Book
				International Edition NextGenRH1CRM
				IPRO - Lial Beg Algebra - MBA09D - 1 XXX
				IPRO - Lial Beg Algebra - MBA09D - 2
				IPRO - Lial Beg Algebra - MBA09D - 3
				IPRO NextGen Media Math Test Book
				IPRO-Irv-XLTestbk1
				IPRO_Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				irv xl title 98
				Irv-UKHighEd-MMLGlobal-1xl
				irv-xl-title-100
				IrvXL 3Level Book
				IrvXL-eTextBk-1
				IrvXL-eTextBk1-1
				IrvXL-eTextBk2-1
				IrvXL-IrvKnewtBk3-1
				IrvXL-IrvPreLDCds-1
				irvxl-knewtonbk1
				IrvXL-KnewtonBk2
				IrvXL-KnewtonBk2
				IrvXL-ReviewStepsBk
				irvxl-testpolicy1
				IrvXLpreld1x
				IrvXLpreldx2
				Janus Math NextGen2
				Janus Suja 111
				Johnston/Mathews: Calculus
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e ENHANCED
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 4e
				Jordan: Integrated Arithmetic and Basic Algebra, 2nd Edition
				Just-In-Time Online Algebra &amp; Trigonometry
				Just-In-Time Online Algebra (2002)
				Kaplan Math 103
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math Enabling Test
				Knewton Math Shutdown Test Book
				Knewton Math test book
				Knewton Math test book2
				Knewton test book 1 (mgpia3e partial) TEST
				Knewton tiny test book
				Knewton tiny test book-Essay Ishara
				KT Book 4
				KT Book1 XL Title
				Lah 3L book
				Lah 3L book1
				Lah 3L Custom pool
				Lah 3Lvl Book3
				Lah Book For Standards
				LAH Custom Pool Order
				LAH ESSAY BOOK
				LAH MATH 150724
				Lah NG Math Media
				Lah XL ONE
				Lah_Main_Math2
				lah_preloaded
				Lah_RepDep_X1
				LAH01ESSAY
				LAH02ESSAY
				LAH04ESSAY
				LAH05ESSAY
				LAH06ESSAY
				LAH07ESSAY
				LAH08ESSAY
				LAH09ESSAY
				LAH11ESSAY
				LAH12ESSAY
				Lahiru 2Level Complete Book
				Lahiru RepDep2
				Lahiru Reporting Department Test
				Larson, Elementary Statistics, 3e DEMO
				Larson: Elementary Statistics: Picturing the World, 4e
				Larson: Elementary Statistics: Picturing the World, 6e
				Lay: Linear Algebra and Its Applications, 3rd Edition
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Lial: Algebra for College Students, 4th Edition
				Lial: Algebra for College Students, 5th Edition
				Lial: Basic College Mathematics, 6th Edition
				Lial: Basic College Mathematics, 7e ENHANCED
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Beginning &amp; Intermediate Algebra, 5e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra, 10e (Retired)
				Lial: Beginning Algebra, 11e-1
				Lial: Beginning Algebra, 8th Edition
				Lial: Beginning Algebra, 9e ENHANCED
				Lial: Beginning Algebra, 9th Edition DEMO  ENHANCED
				Lial: Beginning and Intermediate Algebra, 2nd Edition
				Lial: Beginning and Intermediate Algebra, 3e
				Lial: Calculus with Applications, 7e ENHANCED
				Lial: Calculus with Applications, 7th Edition
				Lial: Calculus with Applications, 8e ENHANCED
				Lial: Calculus with Applications, 9e
				Lial: Calculus with Applications, Brief 7e ENHANCED
				Lial: Calculus with Applications, Brief Version, 8e ENHANCED
				Lial: College Algebra and Trigonometry, 2nd Edition
				Lial: College Algebra and Trigonometry, 3e ENHANCED
				Lial: College Algebra, 10e
				Lial: College Algebra, 11e
				Lial: College Algebra, 8th Edition
				Lial: College Algebra, 9e ENHANCED
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics, 4e
				Lial: Essential Mathematics
				Lial: Essential Mathematics, 2e ENHANCED
				Lial: Essentials of College Algebra Alternate Edition
				Lial: Finite Math and Calculus, 7/e, ENHANCED
				Lial: Finite Mathematics and Calculus with Applications, 6th Edition ENHANCED
				Lial: Finite Mathematics, 7th Edition
				Lial: Finite Mathematics, 7th Edition ENHANCED
				Lial: Finite Mathematics, 8e ENHANCED
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e ENHANCED
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 8th Edition
				Lial: Intermediate Algebra, 9e
				Lial: Intermediate Algebra, 9th Edition
				Lial: Intermediate Algebra, Alternate, 8th Edition
				Lial: Introductory Algebra, 7th Edition
				Lial: Introductory Algebra, 8e ENHANCED
				Lial: Introductory and Intermediate Algebra, 2nd Edition
				Lial: Mathematics with Applications, 8th Edition
				Lial: Mathematics with Applications, Finite Version, 8e
				Lial: Prealgebra
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra and Introductory Algebra
				Lial: Prealgebra, 2nd Edition
				Lial: Prealgebra, 4e
				Lial: Precalculus with Limits
				Lial: Precalculus, 2nd Edition
				Lial: Precalculus, 3e ENHANCED
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Lial: Trigonometry, 7th Edition
				Lial: Trigonometry, 8e ENHANCED
				Lial: Trigonometry, 9e
				Long: Mathematical Reasoning for Elementary Teachers, 3rd Edition
				Long: Mathematical Reasoning for Elementary Teachers, 6e
				Mapping test: NEW edition
				Mapping test: PREVIOUS edition
				Martin-Gay: Algebra 1 partial (copied from live sbnet)_upe3
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Basic College Mathematics, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e ENHANCED
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 4e ENHANCED
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e ENHANCED
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra and Introductory Algebra, ENHANCED
				Martin-Gay: Prealgebra, 4e ENHANCED
				Martin-Gay: Prealgebra, 5e
				Martin-Gay: Prealgebra, 6e
				Martin-Gay: Prealgebra, 8e
				Math Preloaded Course Test Book 1
				Math Preloaded Course Test Book 2
				Math Preloaded Course Test Book 3
				Math Preloaded Course Test Book 4
				Math Preloaded Course Test Book 5
				Math TG Only
				Math US Ruch
				Mathematica V8 testing (SBnetpreview)
				Mathspace Master TOC of Exercises (version 1)
				Mathspace preloaded
				McClave: Statistics, 11e
				Media Test Book
				Melissa Honig's Testing Book
				Miller: Business Mathematics, 11e
				Miller: Business Mathematics, 9th Edition
				Miller: Economics Today, 12th Edition, MATH REVIEW
				Miller: Economics Today: MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: Study Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, Study Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, Study Edition, MATH REVIEW
				Miller: Mathematical Ideas, 10th Edition ENHANCED
				Miller: Mathematical Ideas, Expanded 10th Edition
				Miller: Mathematical Ideas, Expanded 10th Edition ENHANCED
				Miller: Mathematical Ideas: 9th Edition
				Mishkin: The Economics of Money, Banking, &amp; Financial Markets, 7th Ed, MATH REV.
				MML-CoCo: Tobey Intermediate Algebra, 6e
				MMT TG Calc Test
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyFoundationsLab Prototype
				MyMathLab for Basic Mathematics and Algebra
				MyMathLab test book A (from Blitzer TM5)
				MyMathLab test book A2
				MyMathLab test book A3  - Lilani
				MyMathLab test book B (from Briggs Calc)
				MyMathLab test book C (2-level Tannenbaum)
				MyMathLab test book C2
				MyMathLab test book C3
				MyMathLab test book D (Triola 11)
				MyMathLab test book D2  MOBILE DEVICES
				MyMathLab test book D3
				MyMathLab test book Intl 1 three-level
				MyMathLab test book Intl 2 two-level
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				National University
				New Assignment Creation Wizard
				New Book Test
				New Book Test 2
				New Book Test 6
				New Import Test
				NextGen Global Edition
				NextGen Media Math Test Book
				NextGen Media Math Test Book (VOY)
				NextGen Media Math Test Book - Ipro
				NextGen Media Math Test Book Copy
				NextGen Media Math Test Book--ACR
				NextGen Media Math Test Book--hmz1
				NextGen Media Math Test Book--Kethees
				NextGen Media Math Test Book--Lak
				NextGen Media Math Test Book--MLMNG2202
				NextGen Media Math Test Book--MLMNG2202_02
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL 123
				NextGen Media Math Test Book--REAL--Essay
				NextGen Media Math Test Book--REAL--Kasun
				NextGen Media Math Test Book--REAL--Rush
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL-Thilini
				NextGen Media Math Test Book--REAL_BigInt1
				NextGen Media Math Test Book--REAL_RH
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--REAL_VirajC
				NextGen Media Math Test Book--REAL_VirajC9
				NextGen Media Math Test Book--REAL_VirajCJuly
				NextGen Media Math Test Book--REALL
				NextGen Media Math Test Book--sample
				NextGen Media Math Test Book--Shehan
				NextGen Media Math Test Book--Standards
				NextGen Media Math Test Book--storytestings7
				NextGen Media Math Test Book--testsamplpe
				NextGen Media Math Test Book--XXL
				NextGen Media Math Test Book-_MLNG3034_1
				NextGen Media Math Test Book-Essay Ishara
				NextGen Media Math Test Book-One Chapter_MLMNG3034_2
				NextGen Media Math Test BookRH
				NextGen Media Math Test BookRH
				NextGen Test1CRMBC_Au
				NextGen1 Global Edition
				NextGen1 Global Edition - RC
				NG Math Media 21
				Notest Book
				NSM Delta Demo
				O'Daffer: Mathematics for Elementary Teachers, 2nd Edition
				O'Daffer: Mathematics for Elementary Teachers, 3e ENHANCED
				Old SB Mapping Test New
				Old SB Mapping Test Previous
				Parkin: Economics, 6th Edition, MATH REVIEW
				Parkin: Macroeconomics, 6th edition, MATH REVIEW
				Parkin: Microeconomics, 6th Edition, MATH REVIEW
				Perloff: Microeconomics, 3rd Edition, MATH REVIEW
				Phil's Test Book
				Pirnot: Mathematics All Around, 2nd Edition
				Pirnot: Mathematics All Around, 5e
				PM sbnet test book 1
				Prior: Prealgebra, 1e
				Ratti: Precalculus
				Ratti: Precalculus: A Unit Circle Approach
				Req 54 Test 2 level math
				Req 54 Test Alec
				Req 54 Test book
				Req54Test-no 0 section numbers
				RH_1Ruch1
				Ritter/Silber/Udell: Prin. of Money, Banking &amp; Financial Markets 11e MATH REVIEW
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning Algebra with Applications &amp; Visualization, 1e ENHANCED
				Rockswold: Beginning and Intermediate Algebra ENHANCED
				Rockswold: College Algebra and Trigonometry
				Rockswold: College Algebra and Trigonometry through Modeling and Vis, 2nd Ed.
				Rockswold: College Algebra through Modeling and Visualization, 2e
				Rockswold: College Algebra, 4e
				Rockswold: College Algebra, 5e
				Rockswold: Intermediate Algebra
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e ENHANCED
				Rockswold: Precalculus through Modeling and Visualization, 2nd Edition
				Ruffin/Gregory: Principles of Macroeconomics, 7th Edition MATH REVIEW
				Ruffin/Gregory: Principles of Microeconomics, 7th Edition, MATH REVIEW
				Runtime Beta Test Book
				Rupert/Anderson: Federal Taxation 2019 Individuals 32e
				Ruth C-L Test Book
				Ruth C-L Test Book 10
				Ruth C-L Test Book 12
				Ruth C-L Test Book 6
				Ruth C-L Test Book ng
				Ruth Test Book 2 (publish)
				Ruth's Test Book
				Ruth's test book
				Ruth's Test Book for MXL Player
				Ruwan Fake Book
				Salzman: Mathematics for Business, 7th Edition
				Sam Final Test book A
				Sam Knewton July A
				Sam Knewton July B
				Sam Math Test Book
				Sam Math Test Book-Awanthi
				Sam New Book 8-28
				Sam New Book 8_28(2)
				Sam Oct Release 2015
				Sam Test Book
				Sam Test Code1
				Sample Math Book vineetha(2)
				SD Grapher Demo Problems
				Section Media Only Math
				Sharpe: Business Statistics
				Sharpe: Business Statistics, 3e
				SI Notation testing (from sbtest)
				SPPTesting
				Squires: Basic Mathematics
				Squires: Developmental Math
				Squires: Developmental Math: Basic Math, Introductory &amp; Intermediate Algebra, 2e
				StatCrunch Test Book for Dev
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Statistics Online, 1e DEMO chapter 10
				Sullivan: Algebra &amp; Trigonometry EGU, 4th Edition
				Sullivan: Algebra &amp; Trigonometry, 7e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: College Algebra EGU, 4th Edition
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra, 7e ENHANCED
				Sullivan: College Algebra, 8e ENHANCED
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: College Algebra: Concepts Through Functions Corequisite, 4e (2019)
				Sullivan: College Algebra: Concepts through Functions, 1e ENHANCED
				Sullivan: Elementary Algebra, 2e
				Sullivan: Fundamentals of Statistics, 2e ENHANCED
				Sullivan: Fundamentals of Statistics, 3e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Intermediate Algebra, 1e (2007)
				Sullivan: Intermediate Algebra, 1e (2007) DEMO
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Precalculus, 7e ENHANCED
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle ENHANCED
				Sullivan: Precalculus: Concepts through Functions, Unit Circle ENHANCED
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Trigonometry, 7e ENHANCED
				Sullivan: Trigonometry: A Unit Circle Approach, 8e ENHANCED
				Sullivan: Trigonometry: A Unit Circle Approach, 9e
				TDD_Fundamentals
				TEST - Rockswold Interactive Dev Math 1e
				TEST 11/e - objectives
				Test book for Math 1473
				Test book for reporting depts 3
				TEST Gitman: Finance 11th Edition
				TEST INTELLIPRO Irv BK L2
				TEST INTELLIPRO Irv BK L3
				TEST INTELLIPRO Irv Lial: Calc w Apps, 9e (pt-BR)
				TEST INTELLIPRO Irv NextgenMath BK
				TEST INTELLIPRO Irv NG NoPub wPools BK
				Test_Awa_Book
				TEST_INTELLIPRO_BOOK_1
				TFtestbook
				Thomas' Calc demo
				Thomas' Calculus 11e DEMO
				Thomas' Calculus Early Transcendentals Media Upgrade, 11e ENHANCED
				Thomas' Calculus Media Upgrade, 11e
				Thomas' Calculus, 12e
				Thomas/Finney/Weir/Giordano: Thomas' Calculus Early Transcendentals, Updated 10e
				Thomas/Finney/Weir/Giordano: Thomas' Calculus, Updated 10th Edition
				Thomasson: Introductory &amp; Intermediate Algebra 3e
				Tiny Tara Test Text
				Tobey/Slater Intermediate Algebra, 5e (2006)
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 5e ENHANCED
				Tobey: Basic College Mathematics, 6e
				Tobey: Beginning Algebra, 6e ENHANCED
				Tobey: Beginning Algebra: Early Graphing 1e (2006)
				Tobey: Essentials of Basic College Mathematics, 1e ENHANCED
				Tobey: Essentials of Basic Collge Mathematics, 2e
				TOC Builder:XL19096-Real
				Trigsted-XL-Irv-i3
				Trigsted: College Algebra 2e 
				Trigsted: College Algebra DUPE FOR SBTEST
				Trigsted: College Algebra DUPE FOR SBTEST
				Trigsted: College Algebra DUPE FOR SBTEST--1
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra DUPE FOR SBTEST--irv2
				Trigsted: College Algebra DUPE FOR SBTEST--PM
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra, 3e Interactive
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Triola: Elementary Statistics Using Excel
				Triola: Elementary Statistics Using Excel, 2nd Edition
				Triola: Elementary Statistics Using Excel, 3e ENHANCED
				Triola: Elementary Statistics Using Excel, 4e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics Using the Graphing Calculator ENHANCED
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 10e ENHANCED
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 9e ENHANCED
				Triola: Essentials of Statistics
				Triola: Essentials of Statistics, 2e ENHANCED
				Triola: Essentials of Statistics, 3e
				Triola: Essentials of Statistics, 5e
				Tro, Introductory Chemistry, 5e
				UK Higher Ed Test Book
				UK Schools Test Book
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				Valencia Community College - Bittinger: Prealgebra
				Voy's Test Book
				VW copy of Alicia's test book
				vw etext2 test
				VW Import TOC new test
				VW Knewton1 copy
				VW TEST Prentice Hall Economics
				vw testing xlsb-1
				Waldman: Microeconomics, MATH REVIEW
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 5th Edition
				Weiss: Elementary Statistics, 6e ENHANCED
				Weiss: Elementary Statistics, 8e
				Weiss: Introductory Statistics, 6th Edition
				Weiss: Introductory Statistics, 7e ENHANCED
				Weiss: Introductory Statistics, 8e
				Weiss: Introductory Statistics, 9e
				Win Math 1
				WinKnewtonTest2XL
				WinKnewtonTest3XL
				Wizard test
				Workspace Test Book
				x MathXL Freelancer Test Book
				XL Demo Psych
				XL Load Testbook
				XL Load Testbook - Copy
				XLN Tara Sampler
				Young: Finite Mathematics, 3rd Edition
				ZZIA Interactive testing

			</value>
      <webElementGuid>d5407a6e-3866-4e4c-97f6-0d374a120255</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
      <webElementGuid>4c7d71f0-7bea-4d3c-bfe8-37158d008e1f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      <webElementGuid>fc3bf423-3674-4e83-99ad-969800b1dafa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
      <webElementGuid>58e33be9-4d95-4308-825e-3d657a6410c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
      <webElementGuid>5de67a88-ce6c-4d02-839e-6ea1ddcf412f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
      <webElementGuid>33c3e5d1-03ee-491c-86c3-bd740d2df10c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>6dbb67ff-6de6-476e-abd6-1705c62934dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				  -- Choose Book --  
				
				
				
				 eT1 Math Test Book 10 - Non Course Aware 10
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				A&amp;B All Browser Check book
				A&amp;B Flash Browser Check book
				A&amp;B Realplayer Browser Check book
				A&amp;B Testgen Browser Check book
				AA Site Builder KT-Dula
				AaSam Site Builder 10
				AB_Sam_regressuk
				ACR NextGen Media Math Test Book--ACE
				Adams: Calculus, 6e ENHANCED
				Adaptive HW Math Test Book
				ADP: Algebra II Online Course
				ADP: Algebra II Online Course (old/original)
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Akst: Basic Mathematics through Applications, 3e ENHANCED
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Fundamental Mathematics through Applications, 3e ENHANCED
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Introductory Algebra through Applications, 1e ENHANCED
				Akst: Introductory Algebra Through Applications, 2e
				Akst: Introductory Algebra Through Applications, 3e
				ala smoke 6-11
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book (VOY)
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book:  Non-combo
				Alicia&quot; , &quot;'&quot; , &quot;s 2level test book
				Alicia&quot; , &quot;'&quot; , &quot;s Berk book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book REAL MOBILE DEVICES
				Alicia&quot; , &quot;'&quot; , &quot;s book June 2013, testing admin imports
				Alicia&quot; , &quot;'&quot; , &quot;s fake finance book for testing
				Alicia&quot; , &quot;'&quot; , &quot;s MNL Pharmacology XL style
				Alicia&quot; , &quot;'&quot; , &quot;s new book 5-5-13
				Alicia&quot; , &quot;'&quot; , &quot;s new sbnetdemo test book
				Alicia&quot; , &quot;'&quot; , &quot;s other June 2013 book for admin import testing
				Alicia&quot; , &quot;'&quot; , &quot;s sbasp book (for sbnet testing)
				Alicia&quot; , &quot;'&quot; , &quot;s sbtest book
				Alicia&quot; , &quot;'&quot; , &quot;s sbtest book 2000
				Alicia&quot; , &quot;'&quot; , &quot;s sbtest book COPY DO NOT USE
				Alicia&quot; , &quot;'&quot; , &quot;s small test book
				Alicia&quot; , &quot;'&quot; , &quot;s small test book
				Alicia&quot; , &quot;'&quot; , &quot;s testing book for Book part importing
				Alicia&quot; , &quot;'&quot; , &quot;s tiny test book
				Alicias Ex No Bug Test
				All Wizard Book
				Alleyoop College Prep 2011
				Alleyoop College Prep 2012
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, Expanded 7e ENHANCED
				Angel: Elementary Algebra for College Students, 7e ENHANCED
				Angel: Elementary Algebra with Early Graphing for College Students, 3e ENHANCED
				Angel: Elementary and Intermediate Algebra for College Students, 3e (ENHANCED)
				Angel: Elementary and Intermediate Algebra for College Students, 4e
				Angel: Intermediate Algebra for College Students, 7e (ENHANCED)
				AusNextGenRH1CRM
				Automated Preload Master Course book
				Automated Test Book 1 - NextGen Media Math Test Book--REAL
				Automated Test Book 3 - MyMathLab test book A3
				Bade/Parkin: Essential Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics
				Bade/Parkin: Foundations of Macroeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics
				Bade/Parkin: Foundations of Microeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, MATH REVIEW
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Econ, Social &amp; Life Sciences, 10e ENHANCED
				Barnett: Calculus for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics, 10e ENHANCED
				Barnett: Finite Mathematics, 10e ENHANCED
				Barnett: Finite Mathematics, 12e
				BBEP: Algebra &amp; Trigonometry, 2nd Edition
				BBEP: Algebra &amp; Trigonometry, A Unit Circle Approach
				BBEP: College Algebra, 2nd Edition
				BBEP: Precalculus: Graphs &amp; Models 2nd Edition
				BBEP: Precalculus: Graphs &amp; Models, A Unit Circle Approach
				BBEP: Trigonometry: Graphs &amp; Models, 2nd Edition
				Beecher: Algebra &amp; Trigonometry
				Beecher: Algebra &amp; Trigonometry, 2e ENHANCED
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: College Algebra
				Beecher: College Algebra, 2e ENHANCED
				Beecher: Precalculus, 2e ENHANCED
				Beecher: Precalculus, 3e
				Bennett: Statistical Reasoning for Everyday Life
				Bennett: Using and Understanding Mathematics, 3e ENHANCED
				Bennett: Using and Understanding Mathematics, 4e
				Berenson: Basic Business Statistics, 13e
				Betsy Learning Aids 1-24-15
				Betsy No Publisher test book
				Betsy Player Function Copy
				Betsy Player Function Copy
				Betsy Smoketest 1-19-10
				Betsy Smoketest 11-19-09
				Betsy Smoketest 12-20
				Betsy Smoketest 3-3-10
				Betsy Smoketest 9-28
				Betsy Smoketests 5-4
				Billstein: A Problem Solving Approach to Math for Elem School Teachers, 8e
				Billstein: Mathematics for Elementary School Teachers, 10e
				Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Basic Mathematics with Early Integers ENHANCED
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e ENHANCED
				Bittinger: Basic Mathematics, 11e
				Bittinger: Basic Mathematics, 8th Edition
				Bittinger: Basic Mathematics, 9e
				Bittinger: Basic Mathematics, 9th Edition
				Bittinger: Calculus and Its Applications, 10e
				Bittinger: Calculus and Its Applications, 8e ENHANCED
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: Calculus and Its Applications, 9e DEMO
				Bittinger: College Algebra: Graphs &amp; Models, 3e ENHANCED
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 5th Edition
				Bittinger: Developmental Mathematics, 6th Edition
				Bittinger: Developmental Mathematics, 6th Edition ENHANCED
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 5e
				Bittinger: Elementary Algebra, Concepts &amp; Applications, 6th Edition
				Bittinger: Elementary Algebra, Concepts &amp; Applications, 7e
				Bittinger: Elementary Algebra, Concepts and Applications, 8e
				Bittinger: Elementary Algebra: Graphs &amp; Models, 1e ENHANCED
				Bittinger: Elementary and Intermediate Algebra, Concepts &amp; Applications, 2e
				Bittinger: Elementary and Intermediate Algebra, Concepts &amp; Applications, 3e
				Bittinger: Elementary and Intermediate Algebra, Graphs &amp; Models, 2nd Edition
				Bittinger: Elementary and Intermediate Algebra, Graphs and Models
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3rd Edition
				Bittinger: Fundamentals of College Algebra
				Bittinger: Intermediate Algebra, 8th Edition
				Bittinger: Intermediate Algebra, 9th Edition
				Bittinger: Intermediate Algebra, Alternate Version, 8th Ed
				Bittinger: Intermediate Algebra, Concepts &amp; Applications, 5e
				Bittinger: Intermediate Algebra, Graphs &amp; Models
				Bittinger: Intermediate Algebra: Concepts &amp; Applications, 6th Edition
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2nd Edition
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Introductory &amp; Intermediate Algebra, 2nd Edition
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 8th Edition
				Bittinger: Introductory Algebra, 9th Edition
				Bittinger: Introductory and Intermediate Algebra: A Combined Approach
				Bittinger: Prealgebra and Introductory Algebra
				Bittinger: Prealgebra custom-Houston CC
				Bittinger: Prealgebra, 3rd Edition
				Bittinger: Prealgebra, 4th Edition
				Bittinger: Prealgebra, 4th Edition ENHANCED
				Bittinger: Prealgebra, 5e ENHANCED
				Bittinger: Precalculus, Graphs &amp; Models, 3e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Blah book TOL-BG
				Blitzer:  College Algebra, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 2e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra for College Students, 5e ENHANCED
				Blitzer: College Algebra Essentials, 2e ENHANCED
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra: An Early Functions Approach, 2e
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Intermediate Algebra, 4e ENHANCED
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra, 4e ENHANCED
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Precalculus, 2e ENHANCED
				Blitzer: Precalculus, 4e
				Blitzer: Precalculus, 5e
				Blitzer: Thinking Mathematically, 3e ENHANCED
				Blitzer: Thinking Mathematically, 4e
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 3e
				Bock: Stats: Modeling the World
				Bock: Stats: Modeling the World, 3e
				Briggs/Cochran: Calculus
				Briggs/Cochran: Calculus Early Transcendentals
				Briggs/Cochran: Calculus Early Transcendentals DEMO
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus, 2e
				Briggs/Cochran: Calculus, 3e
				CA Test Comment 16517 - hassamplehw/hassampletests
				CA TEST Comment 21460 - Knewton  Available
				CA TEST Finance Book (ngfinance)
				CA TEST Knewton Book 1
				CA TEST Knewton Book 2 - smaller
				CA TEST Math 1
				CA TEST Math 3
				CA TEST NG Math Media 1
				cacomment14605
				Carson: Elementary Algebra with Early Systems of Equations, ENHANCED
				Carson: Elementary Algebra, 2e ENHANCED
				Carson: Elementary Algebra, 3e
				Carson: Elementary Algebra, ENHANCED
				Carson: Elementary and Intermediate Algebra, ENHANCED
				Carson: Intermediate Algebra, ENHANCED
				Carson: Prealgebra
				Carson: Prealgebra, 3e
				catestreqc12327
				CCNG Trigsted Beginning Algebra Demo Test Book
				CCNG Trigsted Intermediate Alg Test Book
				Chalk Test Course for Deltanet Server (Bittinger Prealg 5e 2008)
				Chris Feb Tier 3 Test Math
				Chris Feb Tier 3 Test Math
				Chris Feb Tier 3 Test Math XL [Laksith]
				Chris Feb Tier 3 Test Math XL [Laksith] C
				Chris Feb Tier 3 Test Math XL [Laksith]_D
				Chris Feb Tier 3 Test Math XL [Laksith]_E
				Chris Feb Tier 3 Test Math XL [Laksith]_F
				Chris Feb Tier 3 Test Math XL [Laksith]_G
				Chris Feb Tier 3 Test Math XL [Laksith]_H
				Chris Feb Tier 3 Test Math XL [Laksith]_L
				Chris Feb Tier 3 Test Math_J
				Chris Feb Tier 3 Test Math_upe5
				Chris T3 Test Feb
				Cleaves:  Business Math, 8e
				Cleaves: Business Math, 9e
				Cleaves: College Mathematics, 10e
				College of Central Florida: MAT0012/MAT0024 Prealgebra &amp; Introductory Algebra
				Consortium for Foundation Math: Algebraic, Graphical &amp; Trig. Problem Solving
				Consortium: Algebraic, Graphical &amp; Trigonometric Problem Solving 2e
				Consortium: Intro to Algebraic, Graphical &amp; Numeric Problem Solving, 2nd Edition
				Consortium: Prealgebra, 1st Edition
				Content Test Book from SBNetPreview (mxlpreview)
				Conversion of Tarbuck 11e
				De Veaux Velleman Intro Stats 1e - MOCK
				De Veaux: Intro Stats
				De Veaux: Intro Stats, 2e
				De Veaux: Intro Stats, 3e
				De Veaux: Intro Stats, 4e
				De Veaux: Stats: Data and Models ENHANCED
				Demana: Precalculus: Functions &amp; Graphs, 4th Edition
				Demana: Precalculus: Functions and Graphs, 5th Edition
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				Drag and Drop Lessons
				Dugopolski: College Algebra &amp; Trigonometry, 3rd Edition
				Dugopolski: College Algebra &amp; Trigonometry, 5e
				Dugopolski: College Algebra, 3rd Edition
				Dugopolski: College Algebra, 4e
				Dugopolski: College Algebra, 5e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus ENHANCED Re-release
				Dugopolski: Precalculus with Limits: Functions &amp; Graphs
				Dugopolski: Precalculus, 3rd Edition
				Dugopolski: Precalculus: Functions and Graphs
				Dugopolski: Precalculus: Functions and Graphs 2e
				Dugopolski: Trigonometry
				Dugopolski: Trigonometry, 2e
				DV Italian Book L2
				DV Italian Book L2
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Elementary Algebra: Concepts &amp; Apps: 5e
				Eranga Palette Test
				Essay Book 1 
				eT1 Math Test Book
				eT1 Math Test Book - PPE
				eT1 Math Test Book 3
				eT1 Math Test Book K2
				eT1 Math Test Book Kethees_New
				eT1 Math Test Book Kethish
				eT1 Math Test Book Lak2
				eT1 Math Test Book Lak3
				eT2 Disaggregated - NON course aware test book
				eT2 Math Test Book
				eT2 Math Test Book - CERT
				eT2 Math Test Book - CERT with Print ID
				eT2 Math Test Book - PPE
				eT2 Math Test Book - PPE RH
				eText Integration Trigsted College Alg Test Book
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e Media Update
				Florida CLAST Guide
				Gaze: Thinking Quantitatively: Communicating with Numbers, 2e
				Georgia Perimeter College Math 0096: Essential Mathematics
				Gitman:  Principles of Managerial Finance, 11th Edition UPDATE
				Gitman: Principles of Managerial Finance Brief, 3rd Edition
				Gitman: Principles of Managerial Finance, 10th Edition
				Global Edition NextGenRH1CRM International Edition
				Gould: Essential Statistics: Exploring the World Through Data
				Greenwell: Calculus for the Life Sciences
				Harrisburg Area CC: Martin-Gay Introductory Algebra
				Harshbarger: College Algebra In Context, 2e ENHANCED
				Harshbarger: College Algebra In Context, 3e
				Harshbarger: College Algebra in Context, ENHANCED
				Hass: University Calculus Alternate Edition
				Hass: University Calculus ENHANCED
				Hornsby: A Graphical Approach to Precalculus with Limits, A Unit Circle App, 4e
				Hornsby: College Algebra &amp; Trigonometry, 3rd Edition
				Hornsby: College Algebra, 2nd Edition
				Hornsby: College Algebra, 3rd Edition
				Hornsby: Precalculus with Limits, 3rd Edition
				Hornsby: Precalculus, 3rd Edition
				Hubbard: Money, the Financial System, and the Economy, 5th Edition, MATH REVIEW
				Hutchison: Mathematics for New Technologies
				Intellipro Small Book
				Intellipro Test Book - Niro
				Intellipro Very Small Book
				International Edition NextGenRH1CRM
				IPRO - Lial Beg Algebra - MBA09D - 1 XXX
				IPRO - Lial Beg Algebra - MBA09D - 2
				IPRO - Lial Beg Algebra - MBA09D - 3
				IPRO NextGen Media Math Test Book
				IPRO-Irv-XLTestbk1
				IPRO_Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				irv xl title 98
				Irv-UKHighEd-MMLGlobal-1xl
				irv-xl-title-100
				IrvXL 3Level Book
				IrvXL-eTextBk-1
				IrvXL-eTextBk1-1
				IrvXL-eTextBk2-1
				IrvXL-IrvKnewtBk3-1
				IrvXL-IrvPreLDCds-1
				irvxl-knewtonbk1
				IrvXL-KnewtonBk2
				IrvXL-KnewtonBk2
				IrvXL-ReviewStepsBk
				irvxl-testpolicy1
				IrvXLpreld1x
				IrvXLpreldx2
				Janus Math NextGen2
				Janus Suja 111
				Johnston/Mathews: Calculus
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e ENHANCED
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 4e
				Jordan: Integrated Arithmetic and Basic Algebra, 2nd Edition
				Just-In-Time Online Algebra &amp; Trigonometry
				Just-In-Time Online Algebra (2002)
				Kaplan Math 103
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math Enabling Test
				Knewton Math Shutdown Test Book
				Knewton Math test book
				Knewton Math test book2
				Knewton test book 1 (mgpia3e partial) TEST
				Knewton tiny test book
				Knewton tiny test book-Essay Ishara
				KT Book 4
				KT Book1 XL Title
				Lah 3L book
				Lah 3L book1
				Lah 3L Custom pool
				Lah 3Lvl Book3
				Lah Book For Standards
				LAH Custom Pool Order
				LAH ESSAY BOOK
				LAH MATH 150724
				Lah NG Math Media
				Lah XL ONE
				Lah_Main_Math2
				lah_preloaded
				Lah_RepDep_X1
				LAH01ESSAY
				LAH02ESSAY
				LAH04ESSAY
				LAH05ESSAY
				LAH06ESSAY
				LAH07ESSAY
				LAH08ESSAY
				LAH09ESSAY
				LAH11ESSAY
				LAH12ESSAY
				Lahiru 2Level Complete Book
				Lahiru RepDep2
				Lahiru Reporting Department Test
				Larson, Elementary Statistics, 3e DEMO
				Larson: Elementary Statistics: Picturing the World, 4e
				Larson: Elementary Statistics: Picturing the World, 6e
				Lay: Linear Algebra and Its Applications, 3rd Edition
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Lial: Algebra for College Students, 4th Edition
				Lial: Algebra for College Students, 5th Edition
				Lial: Basic College Mathematics, 6th Edition
				Lial: Basic College Mathematics, 7e ENHANCED
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Beginning &amp; Intermediate Algebra, 5e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra, 10e (Retired)
				Lial: Beginning Algebra, 11e-1
				Lial: Beginning Algebra, 8th Edition
				Lial: Beginning Algebra, 9e ENHANCED
				Lial: Beginning Algebra, 9th Edition DEMO  ENHANCED
				Lial: Beginning and Intermediate Algebra, 2nd Edition
				Lial: Beginning and Intermediate Algebra, 3e
				Lial: Calculus with Applications, 7e ENHANCED
				Lial: Calculus with Applications, 7th Edition
				Lial: Calculus with Applications, 8e ENHANCED
				Lial: Calculus with Applications, 9e
				Lial: Calculus with Applications, Brief 7e ENHANCED
				Lial: Calculus with Applications, Brief Version, 8e ENHANCED
				Lial: College Algebra and Trigonometry, 2nd Edition
				Lial: College Algebra and Trigonometry, 3e ENHANCED
				Lial: College Algebra, 10e
				Lial: College Algebra, 11e
				Lial: College Algebra, 8th Edition
				Lial: College Algebra, 9e ENHANCED
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics, 4e
				Lial: Essential Mathematics
				Lial: Essential Mathematics, 2e ENHANCED
				Lial: Essentials of College Algebra Alternate Edition
				Lial: Finite Math and Calculus, 7/e, ENHANCED
				Lial: Finite Mathematics and Calculus with Applications, 6th Edition ENHANCED
				Lial: Finite Mathematics, 7th Edition
				Lial: Finite Mathematics, 7th Edition ENHANCED
				Lial: Finite Mathematics, 8e ENHANCED
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e ENHANCED
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 8th Edition
				Lial: Intermediate Algebra, 9e
				Lial: Intermediate Algebra, 9th Edition
				Lial: Intermediate Algebra, Alternate, 8th Edition
				Lial: Introductory Algebra, 7th Edition
				Lial: Introductory Algebra, 8e ENHANCED
				Lial: Introductory and Intermediate Algebra, 2nd Edition
				Lial: Mathematics with Applications, 8th Edition
				Lial: Mathematics with Applications, Finite Version, 8e
				Lial: Prealgebra
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra and Introductory Algebra
				Lial: Prealgebra, 2nd Edition
				Lial: Prealgebra, 4e
				Lial: Precalculus with Limits
				Lial: Precalculus, 2nd Edition
				Lial: Precalculus, 3e ENHANCED
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Lial: Trigonometry, 7th Edition
				Lial: Trigonometry, 8e ENHANCED
				Lial: Trigonometry, 9e
				Long: Mathematical Reasoning for Elementary Teachers, 3rd Edition
				Long: Mathematical Reasoning for Elementary Teachers, 6e
				Mapping test: NEW edition
				Mapping test: PREVIOUS edition
				Martin-Gay: Algebra 1 partial (copied from live sbnet)_upe3
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Basic College Mathematics, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e ENHANCED
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 4e ENHANCED
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e ENHANCED
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra and Introductory Algebra, ENHANCED
				Martin-Gay: Prealgebra, 4e ENHANCED
				Martin-Gay: Prealgebra, 5e
				Martin-Gay: Prealgebra, 6e
				Martin-Gay: Prealgebra, 8e
				Math Preloaded Course Test Book 1
				Math Preloaded Course Test Book 2
				Math Preloaded Course Test Book 3
				Math Preloaded Course Test Book 4
				Math Preloaded Course Test Book 5
				Math TG Only
				Math US Ruch
				Mathematica V8 testing (SBnetpreview)
				Mathspace Master TOC of Exercises (version 1)
				Mathspace preloaded
				McClave: Statistics, 11e
				Media Test Book
				Melissa Honig&quot; , &quot;'&quot; , &quot;s Testing Book
				Miller: Business Mathematics, 11e
				Miller: Business Mathematics, 9th Edition
				Miller: Economics Today, 12th Edition, MATH REVIEW
				Miller: Economics Today: MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: Study Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, Study Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, Study Edition, MATH REVIEW
				Miller: Mathematical Ideas, 10th Edition ENHANCED
				Miller: Mathematical Ideas, Expanded 10th Edition
				Miller: Mathematical Ideas, Expanded 10th Edition ENHANCED
				Miller: Mathematical Ideas: 9th Edition
				Mishkin: The Economics of Money, Banking, &amp; Financial Markets, 7th Ed, MATH REV.
				MML-CoCo: Tobey Intermediate Algebra, 6e
				MMT TG Calc Test
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyFoundationsLab Prototype
				MyMathLab for Basic Mathematics and Algebra
				MyMathLab test book A (from Blitzer TM5)
				MyMathLab test book A2
				MyMathLab test book A3  - Lilani
				MyMathLab test book B (from Briggs Calc)
				MyMathLab test book C (2-level Tannenbaum)
				MyMathLab test book C2
				MyMathLab test book C3
				MyMathLab test book D (Triola 11)
				MyMathLab test book D2  MOBILE DEVICES
				MyMathLab test book D3
				MyMathLab test book Intl 1 three-level
				MyMathLab test book Intl 2 two-level
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				National University
				New Assignment Creation Wizard
				New Book Test
				New Book Test 2
				New Book Test 6
				New Import Test
				NextGen Global Edition
				NextGen Media Math Test Book
				NextGen Media Math Test Book (VOY)
				NextGen Media Math Test Book - Ipro
				NextGen Media Math Test Book Copy
				NextGen Media Math Test Book--ACR
				NextGen Media Math Test Book--hmz1
				NextGen Media Math Test Book--Kethees
				NextGen Media Math Test Book--Lak
				NextGen Media Math Test Book--MLMNG2202
				NextGen Media Math Test Book--MLMNG2202_02
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL 123
				NextGen Media Math Test Book--REAL--Essay
				NextGen Media Math Test Book--REAL--Kasun
				NextGen Media Math Test Book--REAL--Rush
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL-Thilini
				NextGen Media Math Test Book--REAL_BigInt1
				NextGen Media Math Test Book--REAL_RH
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--REAL_VirajC
				NextGen Media Math Test Book--REAL_VirajC9
				NextGen Media Math Test Book--REAL_VirajCJuly
				NextGen Media Math Test Book--REALL
				NextGen Media Math Test Book--sample
				NextGen Media Math Test Book--Shehan
				NextGen Media Math Test Book--Standards
				NextGen Media Math Test Book--storytestings7
				NextGen Media Math Test Book--testsamplpe
				NextGen Media Math Test Book--XXL
				NextGen Media Math Test Book-_MLNG3034_1
				NextGen Media Math Test Book-Essay Ishara
				NextGen Media Math Test Book-One Chapter_MLMNG3034_2
				NextGen Media Math Test BookRH
				NextGen Media Math Test BookRH
				NextGen Test1CRMBC_Au
				NextGen1 Global Edition
				NextGen1 Global Edition - RC
				NG Math Media 21
				Notest Book
				NSM Delta Demo
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 2nd Edition
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 3e ENHANCED
				Old SB Mapping Test New
				Old SB Mapping Test Previous
				Parkin: Economics, 6th Edition, MATH REVIEW
				Parkin: Macroeconomics, 6th edition, MATH REVIEW
				Parkin: Microeconomics, 6th Edition, MATH REVIEW
				Perloff: Microeconomics, 3rd Edition, MATH REVIEW
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Pirnot: Mathematics All Around, 2nd Edition
				Pirnot: Mathematics All Around, 5e
				PM sbnet test book 1
				Prior: Prealgebra, 1e
				Ratti: Precalculus
				Ratti: Precalculus: A Unit Circle Approach
				Req 54 Test 2 level math
				Req 54 Test Alec
				Req 54 Test book
				Req54Test-no 0 section numbers
				RH_1Ruch1
				Ritter/Silber/Udell: Prin. of Money, Banking &amp; Financial Markets 11e MATH REVIEW
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning Algebra with Applications &amp; Visualization, 1e ENHANCED
				Rockswold: Beginning and Intermediate Algebra ENHANCED
				Rockswold: College Algebra and Trigonometry
				Rockswold: College Algebra and Trigonometry through Modeling and Vis, 2nd Ed.
				Rockswold: College Algebra through Modeling and Visualization, 2e
				Rockswold: College Algebra, 4e
				Rockswold: College Algebra, 5e
				Rockswold: Intermediate Algebra
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e ENHANCED
				Rockswold: Precalculus through Modeling and Visualization, 2nd Edition
				Ruffin/Gregory: Principles of Macroeconomics, 7th Edition MATH REVIEW
				Ruffin/Gregory: Principles of Microeconomics, 7th Edition, MATH REVIEW
				Runtime Beta Test Book
				Rupert/Anderson: Federal Taxation 2019 Individuals 32e
				Ruth C-L Test Book
				Ruth C-L Test Book 10
				Ruth C-L Test Book 12
				Ruth C-L Test Book 6
				Ruth C-L Test Book ng
				Ruth Test Book 2 (publish)
				Ruth&quot; , &quot;'&quot; , &quot;s Test Book
				Ruth&quot; , &quot;'&quot; , &quot;s test book
				Ruth&quot; , &quot;'&quot; , &quot;s Test Book for MXL Player
				Ruwan Fake Book
				Salzman: Mathematics for Business, 7th Edition
				Sam Final Test book A
				Sam Knewton July A
				Sam Knewton July B
				Sam Math Test Book
				Sam Math Test Book-Awanthi
				Sam New Book 8-28
				Sam New Book 8_28(2)
				Sam Oct Release 2015
				Sam Test Book
				Sam Test Code1
				Sample Math Book vineetha(2)
				SD Grapher Demo Problems
				Section Media Only Math
				Sharpe: Business Statistics
				Sharpe: Business Statistics, 3e
				SI Notation testing (from sbtest)
				SPPTesting
				Squires: Basic Mathematics
				Squires: Developmental Math
				Squires: Developmental Math: Basic Math, Introductory &amp; Intermediate Algebra, 2e
				StatCrunch Test Book for Dev
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Statistics Online, 1e DEMO chapter 10
				Sullivan: Algebra &amp; Trigonometry EGU, 4th Edition
				Sullivan: Algebra &amp; Trigonometry, 7e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: College Algebra EGU, 4th Edition
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra, 7e ENHANCED
				Sullivan: College Algebra, 8e ENHANCED
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: College Algebra: Concepts Through Functions Corequisite, 4e (2019)
				Sullivan: College Algebra: Concepts through Functions, 1e ENHANCED
				Sullivan: Elementary Algebra, 2e
				Sullivan: Fundamentals of Statistics, 2e ENHANCED
				Sullivan: Fundamentals of Statistics, 3e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Intermediate Algebra, 1e (2007)
				Sullivan: Intermediate Algebra, 1e (2007) DEMO
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Precalculus, 7e ENHANCED
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle ENHANCED
				Sullivan: Precalculus: Concepts through Functions, Unit Circle ENHANCED
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Trigonometry, 7e ENHANCED
				Sullivan: Trigonometry: A Unit Circle Approach, 8e ENHANCED
				Sullivan: Trigonometry: A Unit Circle Approach, 9e
				TDD_Fundamentals
				TEST - Rockswold Interactive Dev Math 1e
				TEST 11/e - objectives
				Test book for Math 1473
				Test book for reporting depts 3
				TEST Gitman: Finance 11th Edition
				TEST INTELLIPRO Irv BK L2
				TEST INTELLIPRO Irv BK L3
				TEST INTELLIPRO Irv Lial: Calc w Apps, 9e (pt-BR)
				TEST INTELLIPRO Irv NextgenMath BK
				TEST INTELLIPRO Irv NG NoPub wPools BK
				Test_Awa_Book
				TEST_INTELLIPRO_BOOK_1
				TFtestbook
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e ENHANCED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 12e
				Thomas/Finney/Weir/Giordano: Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, Updated 10e
				Thomas/Finney/Weir/Giordano: Thomas&quot; , &quot;'&quot; , &quot; Calculus, Updated 10th Edition
				Thomasson: Introductory &amp; Intermediate Algebra 3e
				Tiny Tara Test Text
				Tobey/Slater Intermediate Algebra, 5e (2006)
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 5e ENHANCED
				Tobey: Basic College Mathematics, 6e
				Tobey: Beginning Algebra, 6e ENHANCED
				Tobey: Beginning Algebra: Early Graphing 1e (2006)
				Tobey: Essentials of Basic College Mathematics, 1e ENHANCED
				Tobey: Essentials of Basic Collge Mathematics, 2e
				TOC Builder:XL19096-Real
				Trigsted-XL-Irv-i3
				Trigsted: College Algebra 2e 
				Trigsted: College Algebra DUPE FOR SBTEST
				Trigsted: College Algebra DUPE FOR SBTEST
				Trigsted: College Algebra DUPE FOR SBTEST--1
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra DUPE FOR SBTEST--irv2
				Trigsted: College Algebra DUPE FOR SBTEST--PM
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra, 3e Interactive
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Triola: Elementary Statistics Using Excel
				Triola: Elementary Statistics Using Excel, 2nd Edition
				Triola: Elementary Statistics Using Excel, 3e ENHANCED
				Triola: Elementary Statistics Using Excel, 4e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics Using the Graphing Calculator ENHANCED
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 10e ENHANCED
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 9e ENHANCED
				Triola: Essentials of Statistics
				Triola: Essentials of Statistics, 2e ENHANCED
				Triola: Essentials of Statistics, 3e
				Triola: Essentials of Statistics, 5e
				Tro, Introductory Chemistry, 5e
				UK Higher Ed Test Book
				UK Schools Test Book
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				Valencia Community College - Bittinger: Prealgebra
				Voy&quot; , &quot;'&quot; , &quot;s Test Book
				VW copy of Alicia&quot; , &quot;'&quot; , &quot;s test book
				vw etext2 test
				VW Import TOC new test
				VW Knewton1 copy
				VW TEST Prentice Hall Economics
				vw testing xlsb-1
				Waldman: Microeconomics, MATH REVIEW
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 5th Edition
				Weiss: Elementary Statistics, 6e ENHANCED
				Weiss: Elementary Statistics, 8e
				Weiss: Introductory Statistics, 6th Edition
				Weiss: Introductory Statistics, 7e ENHANCED
				Weiss: Introductory Statistics, 8e
				Weiss: Introductory Statistics, 9e
				Win Math 1
				WinKnewtonTest2XL
				WinKnewtonTest3XL
				Wizard test
				Workspace Test Book
				x MathXL Freelancer Test Book
				XL Demo Psych
				XL Load Testbook
				XL Load Testbook - Copy
				XLN Tara Sampler
				Young: Finite Mathematics, 3rd Edition
				ZZIA Interactive testing

			&quot;) or . = concat(&quot;
				  -- Choose Book --  
				
				
				
				 eT1 Math Test Book 10 - Non Course Aware 10
				*Algebra 1 Common Core (2012)
				*Algebra 2 Common Core (2012)
				*Prentice Hall Algebra 1 ©2011
				*Universal Course: Algebra I
				A&amp;B All Browser Check book
				A&amp;B Flash Browser Check book
				A&amp;B Realplayer Browser Check book
				A&amp;B Testgen Browser Check book
				AA Site Builder KT-Dula
				AaSam Site Builder 10
				AB_Sam_regressuk
				ACR NextGen Media Math Test Book--ACE
				Adams: Calculus, 6e ENHANCED
				Adaptive HW Math Test Book
				ADP: Algebra II Online Course
				ADP: Algebra II Online Course (old/original)
				Agresti: Statistics: The Art and Science of Learning from Data, 2e
				Akst: Basic Mathematics through Applications, 3e ENHANCED
				Akst: Basic Mathematics Through Applications, 4e
				Akst: Fundamental Mathematics through Applications, 3e ENHANCED
				Akst: Fundamental Mathematics Through Applications, 4e
				Akst: Introductory Algebra through Applications, 1e ENHANCED
				Akst: Introductory Algebra Through Applications, 2e
				Akst: Introductory Algebra Through Applications, 3e
				ala smoke 6-11
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book (VOY)
				Alec&quot; , &quot;'&quot; , &quot;s Delta Book:  Non-combo
				Alicia&quot; , &quot;'&quot; , &quot;s 2level test book
				Alicia&quot; , &quot;'&quot; , &quot;s Berk book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book
				Alicia&quot; , &quot;'&quot; , &quot;s blah book REAL MOBILE DEVICES
				Alicia&quot; , &quot;'&quot; , &quot;s book June 2013, testing admin imports
				Alicia&quot; , &quot;'&quot; , &quot;s fake finance book for testing
				Alicia&quot; , &quot;'&quot; , &quot;s MNL Pharmacology XL style
				Alicia&quot; , &quot;'&quot; , &quot;s new book 5-5-13
				Alicia&quot; , &quot;'&quot; , &quot;s new sbnetdemo test book
				Alicia&quot; , &quot;'&quot; , &quot;s other June 2013 book for admin import testing
				Alicia&quot; , &quot;'&quot; , &quot;s sbasp book (for sbnet testing)
				Alicia&quot; , &quot;'&quot; , &quot;s sbtest book
				Alicia&quot; , &quot;'&quot; , &quot;s sbtest book 2000
				Alicia&quot; , &quot;'&quot; , &quot;s sbtest book COPY DO NOT USE
				Alicia&quot; , &quot;'&quot; , &quot;s small test book
				Alicia&quot; , &quot;'&quot; , &quot;s small test book
				Alicia&quot; , &quot;'&quot; , &quot;s testing book for Book part importing
				Alicia&quot; , &quot;'&quot; , &quot;s tiny test book
				Alicias Ex No Bug Test
				All Wizard Book
				Alleyoop College Prep 2011
				Alleyoop College Prep 2012
				Angel: A Survey of Mathematics with Applications, 11e
				Angel: A Survey of Mathematics with Applications, 8e
				Angel: A Survey of Mathematics with Applications, Expanded 7e ENHANCED
				Angel: Elementary Algebra for College Students, 7e ENHANCED
				Angel: Elementary Algebra with Early Graphing for College Students, 3e ENHANCED
				Angel: Elementary and Intermediate Algebra for College Students, 3e (ENHANCED)
				Angel: Elementary and Intermediate Algebra for College Students, 4e
				Angel: Intermediate Algebra for College Students, 7e (ENHANCED)
				AusNextGenRH1CRM
				Automated Preload Master Course book
				Automated Test Book 1 - NextGen Media Math Test Book--REAL
				Automated Test Book 3 - MyMathLab test book A3
				Bade/Parkin: Essential Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Economics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics
				Bade/Parkin: Foundations of Macroeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Macroeconomics, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics
				Bade/Parkin: Foundations of Microeconomics, 2nd Edition, MATH REVIEW
				Bade/Parkin: Foundations of Microeconomics, MATH REVIEW
				Barnett: Calculus for Business, Econ, Life/Social Sciences, 11e
				Barnett: Calculus for Business, Econ, Social &amp; Life Sciences, 10e ENHANCED
				Barnett: Calculus for Business, Economics, Life/Social Sciences, 12e
				Barnett: College Mathematics, 10e ENHANCED
				Barnett: Finite Mathematics, 10e ENHANCED
				Barnett: Finite Mathematics, 12e
				BBEP: Algebra &amp; Trigonometry, 2nd Edition
				BBEP: Algebra &amp; Trigonometry, A Unit Circle Approach
				BBEP: College Algebra, 2nd Edition
				BBEP: Precalculus: Graphs &amp; Models 2nd Edition
				BBEP: Precalculus: Graphs &amp; Models, A Unit Circle Approach
				BBEP: Trigonometry: Graphs &amp; Models, 2nd Edition
				Beecher: Algebra &amp; Trigonometry
				Beecher: Algebra &amp; Trigonometry, 2e ENHANCED
				Beecher: Algebra &amp; Trigonometry, 3e
				Beecher: College Algebra
				Beecher: College Algebra, 2e ENHANCED
				Beecher: Precalculus, 2e ENHANCED
				Beecher: Precalculus, 3e
				Bennett: Statistical Reasoning for Everyday Life
				Bennett: Using and Understanding Mathematics, 3e ENHANCED
				Bennett: Using and Understanding Mathematics, 4e
				Berenson: Basic Business Statistics, 13e
				Betsy Learning Aids 1-24-15
				Betsy No Publisher test book
				Betsy Player Function Copy
				Betsy Player Function Copy
				Betsy Smoketest 1-19-10
				Betsy Smoketest 11-19-09
				Betsy Smoketest 12-20
				Betsy Smoketest 3-3-10
				Betsy Smoketest 9-28
				Betsy Smoketests 5-4
				Billstein: A Problem Solving Approach to Math for Elem School Teachers, 8e
				Billstein: Mathematics for Elementary School Teachers, 10e
				Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				Bittinger: Algebra and Trigonometry: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Basic Mathematics with Early Integers ENHANCED
				Bittinger: Basic Mathematics with Early Integers, 3e
				Bittinger: Basic Mathematics, 10e ENHANCED
				Bittinger: Basic Mathematics, 11e
				Bittinger: Basic Mathematics, 8th Edition
				Bittinger: Basic Mathematics, 9e
				Bittinger: Basic Mathematics, 9th Edition
				Bittinger: Calculus and Its Applications, 10e
				Bittinger: Calculus and Its Applications, 8e ENHANCED
				Bittinger: Calculus and Its Applications, 9e
				Bittinger: Calculus and Its Applications, 9e DEMO
				Bittinger: College Algebra: Graphs &amp; Models, 3e ENHANCED
				Bittinger: College Algebra: Graphs and Models, 4e
				Bittinger: Developmental Mathematics, 5th Edition
				Bittinger: Developmental Mathematics, 6th Edition
				Bittinger: Developmental Mathematics, 6th Edition ENHANCED
				Bittinger: Developmental Mathematics, 7e
				Bittinger: Elementary &amp; Intermediate Algebra, Concepts and Applications, 5e
				Bittinger: Elementary Algebra, Concepts &amp; Applications, 6th Edition
				Bittinger: Elementary Algebra, Concepts &amp; Applications, 7e
				Bittinger: Elementary Algebra, Concepts and Applications, 8e
				Bittinger: Elementary Algebra: Graphs &amp; Models, 1e ENHANCED
				Bittinger: Elementary and Intermediate Algebra, Concepts &amp; Applications, 2e
				Bittinger: Elementary and Intermediate Algebra, Concepts &amp; Applications, 3e
				Bittinger: Elementary and Intermediate Algebra, Graphs &amp; Models, 2nd Edition
				Bittinger: Elementary and Intermediate Algebra, Graphs and Models
				Bittinger: Foundations of Mathematics
				Bittinger: Fundamental Mathematics, 3rd Edition
				Bittinger: Fundamentals of College Algebra
				Bittinger: Intermediate Algebra, 8th Edition
				Bittinger: Intermediate Algebra, 9th Edition
				Bittinger: Intermediate Algebra, Alternate Version, 8th Ed
				Bittinger: Intermediate Algebra, Concepts &amp; Applications, 5e
				Bittinger: Intermediate Algebra, Graphs &amp; Models
				Bittinger: Intermediate Algebra: Concepts &amp; Applications, 6th Edition
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2e ENHANCED
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 2nd Edition
				Bittinger: Intermediate Algebra: Graphs &amp; Models, 3e ENHANCED
				Bittinger: Introductory &amp; Intermediate Algebra, 2nd Edition
				Bittinger: Introductory Algebra, 10e
				Bittinger: Introductory Algebra, 8th Edition
				Bittinger: Introductory Algebra, 9th Edition
				Bittinger: Introductory and Intermediate Algebra: A Combined Approach
				Bittinger: Prealgebra and Introductory Algebra
				Bittinger: Prealgebra custom-Houston CC
				Bittinger: Prealgebra, 3rd Edition
				Bittinger: Prealgebra, 4th Edition
				Bittinger: Prealgebra, 4th Edition ENHANCED
				Bittinger: Prealgebra, 5e ENHANCED
				Bittinger: Precalculus, Graphs &amp; Models, 3e ENHANCED
				Bittinger: Precalculus: Graphs &amp; Models, 4e
				Blah book TOL-BG
				Blitzer:  College Algebra, 3e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 2e ENHANCED
				Blitzer: Algebra &amp; Trigonometry, 4e
				Blitzer: Algebra for College Students, 5e ENHANCED
				Blitzer: College Algebra Essentials, 2e ENHANCED
				Blitzer: College Algebra Essentials, 3e
				Blitzer: College Algebra Essentials, 4e
				Blitzer: College Algebra with Integrated Review, 7e
				Blitzer: College Algebra, 4e
				Blitzer: College Algebra, 5e
				Blitzer: College Algebra, 6e
				Blitzer: College Algebra, 7e
				Blitzer: College Algebra: An Early Functions Approach, 2e
				Blitzer: Essentials of Introductory &amp; Intermediate Algebra
				Blitzer: Intermediate Algebra for College Students, 5e
				Blitzer: Intermediate Algebra, 4e ENHANCED
				Blitzer: Introductory &amp; Intermediate Algebra for College Students, 4e
				Blitzer: Introductory Algebra, 4e ENHANCED
				Blitzer: Pathways to College Mathematics, 1e
				Blitzer: Precalculus, 2e ENHANCED
				Blitzer: Precalculus, 4e
				Blitzer: Precalculus, 5e
				Blitzer: Thinking Mathematically, 3e ENHANCED
				Blitzer: Thinking Mathematically, 4e
				Blitzer: Thinking Mathematically, 5e
				Blitzer: Thinking Mathematically, 6e
				Blitzer: Thinking Mathematically, 7e with Integrated Review
				Blitzer: Trigonometry, 3e
				Bock: Stats: Modeling the World
				Bock: Stats: Modeling the World, 3e
				Briggs/Cochran: Calculus
				Briggs/Cochran: Calculus Early Transcendentals
				Briggs/Cochran: Calculus Early Transcendentals DEMO
				Briggs/Cochran: Calculus Early Transcendentals, 2e
				Briggs/Cochran: Calculus, 2e
				Briggs/Cochran: Calculus, 3e
				CA Test Comment 16517 - hassamplehw/hassampletests
				CA TEST Comment 21460 - Knewton  Available
				CA TEST Finance Book (ngfinance)
				CA TEST Knewton Book 1
				CA TEST Knewton Book 2 - smaller
				CA TEST Math 1
				CA TEST Math 3
				CA TEST NG Math Media 1
				cacomment14605
				Carson: Elementary Algebra with Early Systems of Equations, ENHANCED
				Carson: Elementary Algebra, 2e ENHANCED
				Carson: Elementary Algebra, 3e
				Carson: Elementary Algebra, ENHANCED
				Carson: Elementary and Intermediate Algebra, ENHANCED
				Carson: Intermediate Algebra, ENHANCED
				Carson: Prealgebra
				Carson: Prealgebra, 3e
				catestreqc12327
				CCNG Trigsted Beginning Algebra Demo Test Book
				CCNG Trigsted Intermediate Alg Test Book
				Chalk Test Course for Deltanet Server (Bittinger Prealg 5e 2008)
				Chris Feb Tier 3 Test Math
				Chris Feb Tier 3 Test Math
				Chris Feb Tier 3 Test Math XL [Laksith]
				Chris Feb Tier 3 Test Math XL [Laksith] C
				Chris Feb Tier 3 Test Math XL [Laksith]_D
				Chris Feb Tier 3 Test Math XL [Laksith]_E
				Chris Feb Tier 3 Test Math XL [Laksith]_F
				Chris Feb Tier 3 Test Math XL [Laksith]_G
				Chris Feb Tier 3 Test Math XL [Laksith]_H
				Chris Feb Tier 3 Test Math XL [Laksith]_L
				Chris Feb Tier 3 Test Math_J
				Chris Feb Tier 3 Test Math_upe5
				Chris T3 Test Feb
				Cleaves:  Business Math, 8e
				Cleaves: Business Math, 9e
				Cleaves: College Mathematics, 10e
				College of Central Florida: MAT0012/MAT0024 Prealgebra &amp; Introductory Algebra
				Consortium for Foundation Math: Algebraic, Graphical &amp; Trig. Problem Solving
				Consortium: Algebraic, Graphical &amp; Trigonometric Problem Solving 2e
				Consortium: Intro to Algebraic, Graphical &amp; Numeric Problem Solving, 2nd Edition
				Consortium: Prealgebra, 1st Edition
				Content Test Book from SBNetPreview (mxlpreview)
				Conversion of Tarbuck 11e
				De Veaux Velleman Intro Stats 1e - MOCK
				De Veaux: Intro Stats
				De Veaux: Intro Stats, 2e
				De Veaux: Intro Stats, 3e
				De Veaux: Intro Stats, 4e
				De Veaux: Stats: Data and Models ENHANCED
				Demana: Precalculus: Functions &amp; Graphs, 4th Edition
				Demana: Precalculus: Functions and Graphs, 5th Edition
				Demana: Precalculus: Graphical, Numerical, Algebraic, 7e
				Drag and Drop Lessons
				Dugopolski: College Algebra &amp; Trigonometry, 3rd Edition
				Dugopolski: College Algebra &amp; Trigonometry, 5e
				Dugopolski: College Algebra, 3rd Edition
				Dugopolski: College Algebra, 4e
				Dugopolski: College Algebra, 5e
				Dugopolski: Fundamentals of Precalculus
				Dugopolski: Fundamentals of Precalculus ENHANCED Re-release
				Dugopolski: Precalculus with Limits: Functions &amp; Graphs
				Dugopolski: Precalculus, 3rd Edition
				Dugopolski: Precalculus: Functions and Graphs
				Dugopolski: Precalculus: Functions and Graphs 2e
				Dugopolski: Trigonometry
				Dugopolski: Trigonometry, 2e
				DV Italian Book L2
				DV Italian Book L2
				EEK Phase 3 IRA
				EEK Trigsted College Algebra 2e IRA TEST
				Elementary Algebra: Concepts &amp; Apps: 5e
				Eranga Palette Test
				Essay Book 1 
				eT1 Math Test Book
				eT1 Math Test Book - PPE
				eT1 Math Test Book 3
				eT1 Math Test Book K2
				eT1 Math Test Book Kethees_New
				eT1 Math Test Book Kethish
				eT1 Math Test Book Lak2
				eT1 Math Test Book Lak3
				eT2 Disaggregated - NON course aware test book
				eT2 Math Test Book
				eT2 Math Test Book - CERT
				eT2 Math Test Book - CERT with Print ID
				eT2 Math Test Book - PPE
				eT2 Math Test Book - PPE RH
				eText Integration Trigsted College Alg Test Book
				Finney/Demana: Calculus Graphical, Numerical, Algebraic, 5e
				Finney: Calculus: Graphical, Numerical, Algebraic, 3e Media Update
				Florida CLAST Guide
				Gaze: Thinking Quantitatively: Communicating with Numbers, 2e
				Georgia Perimeter College Math 0096: Essential Mathematics
				Gitman:  Principles of Managerial Finance, 11th Edition UPDATE
				Gitman: Principles of Managerial Finance Brief, 3rd Edition
				Gitman: Principles of Managerial Finance, 10th Edition
				Global Edition NextGenRH1CRM International Edition
				Gould: Essential Statistics: Exploring the World Through Data
				Greenwell: Calculus for the Life Sciences
				Harrisburg Area CC: Martin-Gay Introductory Algebra
				Harshbarger: College Algebra In Context, 2e ENHANCED
				Harshbarger: College Algebra In Context, 3e
				Harshbarger: College Algebra in Context, ENHANCED
				Hass: University Calculus Alternate Edition
				Hass: University Calculus ENHANCED
				Hornsby: A Graphical Approach to Precalculus with Limits, A Unit Circle App, 4e
				Hornsby: College Algebra &amp; Trigonometry, 3rd Edition
				Hornsby: College Algebra, 2nd Edition
				Hornsby: College Algebra, 3rd Edition
				Hornsby: Precalculus with Limits, 3rd Edition
				Hornsby: Precalculus, 3rd Edition
				Hubbard: Money, the Financial System, and the Economy, 5th Edition, MATH REVIEW
				Hutchison: Mathematics for New Technologies
				Intellipro Small Book
				Intellipro Test Book - Niro
				Intellipro Very Small Book
				International Edition NextGenRH1CRM
				IPRO - Lial Beg Algebra - MBA09D - 1 XXX
				IPRO - Lial Beg Algebra - MBA09D - 2
				IPRO - Lial Beg Algebra - MBA09D - 3
				IPRO NextGen Media Math Test Book
				IPRO-Irv-XLTestbk1
				IPRO_Bittinger: Algebra &amp; Trigonometry Graphs and Models, 5e
				irv xl title 98
				Irv-UKHighEd-MMLGlobal-1xl
				irv-xl-title-100
				IrvXL 3Level Book
				IrvXL-eTextBk-1
				IrvXL-eTextBk1-1
				IrvXL-eTextBk2-1
				IrvXL-IrvKnewtBk3-1
				IrvXL-IrvPreLDCds-1
				irvxl-knewtonbk1
				IrvXL-KnewtonBk2
				IrvXL-KnewtonBk2
				IrvXL-ReviewStepsBk
				irvxl-testpolicy1
				IrvXLpreld1x
				IrvXLpreldx2
				Janus Math NextGen2
				Janus Suja 111
				Johnston/Mathews: Calculus
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 3e ENHANCED
				Jordan: Integrated Arithmetic &amp; Basic Algebra, 4e
				Jordan: Integrated Arithmetic and Basic Algebra, 2nd Edition
				Just-In-Time Online Algebra &amp; Trigonometry
				Just-In-Time Online Algebra (2002)
				Kaplan Math 103
				Kaplan MM201-A
				Kaplan MM201-B
				Knewton Math Enabling Test
				Knewton Math Shutdown Test Book
				Knewton Math test book
				Knewton Math test book2
				Knewton test book 1 (mgpia3e partial) TEST
				Knewton tiny test book
				Knewton tiny test book-Essay Ishara
				KT Book 4
				KT Book1 XL Title
				Lah 3L book
				Lah 3L book1
				Lah 3L Custom pool
				Lah 3Lvl Book3
				Lah Book For Standards
				LAH Custom Pool Order
				LAH ESSAY BOOK
				LAH MATH 150724
				Lah NG Math Media
				Lah XL ONE
				Lah_Main_Math2
				lah_preloaded
				Lah_RepDep_X1
				LAH01ESSAY
				LAH02ESSAY
				LAH04ESSAY
				LAH05ESSAY
				LAH06ESSAY
				LAH07ESSAY
				LAH08ESSAY
				LAH09ESSAY
				LAH11ESSAY
				LAH12ESSAY
				Lahiru 2Level Complete Book
				Lahiru RepDep2
				Lahiru Reporting Department Test
				Larson, Elementary Statistics, 3e DEMO
				Larson: Elementary Statistics: Picturing the World, 4e
				Larson: Elementary Statistics: Picturing the World, 6e
				Lay: Linear Algebra and Its Applications, 3rd Edition
				Lesmeister: Math Basics for the Health Care Professional, 3e
				Lial: Algebra for College Students, 4th Edition
				Lial: Algebra for College Students, 5th Edition
				Lial: Basic College Mathematics, 6th Edition
				Lial: Basic College Mathematics, 7e ENHANCED
				Lial: Basic College Mathematics, 8e
				Lial: Basic College Mathematics, 9e
				Lial: Beginning &amp; Intermediate Algebra, 5e
				Lial: Beginning &amp; Intermediate Algebra, 6e
				Lial: Beginning Algebra, 10e (Retired)
				Lial: Beginning Algebra, 11e-1
				Lial: Beginning Algebra, 8th Edition
				Lial: Beginning Algebra, 9e ENHANCED
				Lial: Beginning Algebra, 9th Edition DEMO  ENHANCED
				Lial: Beginning and Intermediate Algebra, 2nd Edition
				Lial: Beginning and Intermediate Algebra, 3e
				Lial: Calculus with Applications, 7e ENHANCED
				Lial: Calculus with Applications, 7th Edition
				Lial: Calculus with Applications, 8e ENHANCED
				Lial: Calculus with Applications, 9e
				Lial: Calculus with Applications, Brief 7e ENHANCED
				Lial: Calculus with Applications, Brief Version, 8e ENHANCED
				Lial: College Algebra and Trigonometry, 2nd Edition
				Lial: College Algebra and Trigonometry, 3e ENHANCED
				Lial: College Algebra, 10e
				Lial: College Algebra, 11e
				Lial: College Algebra, 8th Edition
				Lial: College Algebra, 9e ENHANCED
				Lial: Developmental Mathematics, 2e
				Lial: Developmental Mathematics, 3e
				Lial: Developmental Mathematics, 4e
				Lial: Essential Mathematics
				Lial: Essential Mathematics, 2e ENHANCED
				Lial: Essentials of College Algebra Alternate Edition
				Lial: Finite Math and Calculus, 7/e, ENHANCED
				Lial: Finite Mathematics and Calculus with Applications, 6th Edition ENHANCED
				Lial: Finite Mathematics, 7th Edition
				Lial: Finite Mathematics, 7th Edition ENHANCED
				Lial: Finite Mathematics, 8e ENHANCED
				Lial: Finite Mathematics, 9e
				Lial: Intermediate Algebra, 10e ENHANCED
				Lial: Intermediate Algebra, 8e
				Lial: Intermediate Algebra, 8th Edition
				Lial: Intermediate Algebra, 9e
				Lial: Intermediate Algebra, 9th Edition
				Lial: Intermediate Algebra, Alternate, 8th Edition
				Lial: Introductory Algebra, 7th Edition
				Lial: Introductory Algebra, 8e ENHANCED
				Lial: Introductory and Intermediate Algebra, 2nd Edition
				Lial: Mathematics with Applications, 8th Edition
				Lial: Mathematics with Applications, Finite Version, 8e
				Lial: Prealgebra
				Lial: Prealgebra &amp; Introductory Algebra, 3e
				Lial: Prealgebra and Introductory Algebra
				Lial: Prealgebra, 2nd Edition
				Lial: Prealgebra, 4e
				Lial: Precalculus with Limits
				Lial: Precalculus, 2nd Edition
				Lial: Precalculus, 3e ENHANCED
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Precalculus, 6e
				Lial: Trigonometry, 10e
				Lial: Trigonometry, 11e
				Lial: Trigonometry, 12e
				Lial: Trigonometry, 7th Edition
				Lial: Trigonometry, 8e ENHANCED
				Lial: Trigonometry, 9e
				Long: Mathematical Reasoning for Elementary Teachers, 3rd Edition
				Long: Mathematical Reasoning for Elementary Teachers, 6e
				Mapping test: NEW edition
				Mapping test: PREVIOUS edition
				Martin-Gay: Algebra 1 partial (copied from live sbnet)_upe3
				Martin-Gay: Algebra 2
				Martin-Gay: Algebra Foundations, 2e
				Martin-Gay: Basic College Mathematics, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 3e ENHANCED
				Martin-Gay: Beginning &amp; Intermediate Algebra, 4e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 5e
				Martin-Gay: Beginning &amp; Intermediate Algebra, 6e
				Martin-Gay: Beginning Algebra, 4e ENHANCED
				Martin-Gay: Beginning Algebra, 5e
				Martin-Gay: Beginning Algebra, 7e
				Martin-Gay: Developmental Mathematics, 2e
				Martin-Gay: Intermediate Algebra, 4e ENHANCED
				Martin-Gay: Intermediate Algebra, 5e
				Martin-Gay: Intermediate Algebra, 7e
				Martin-Gay: Intermediate Algebra, 7e hb ADARRA TEST
				Martin-Gay: Intermediate Algebra: A Graphing Approach, 3e ENHANCED
				Martin-Gay: Introductory Algebra, 3e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 2e
				Martin-Gay: Prealgebra &amp; Introductory Algebra, 3e
				Martin-Gay: Prealgebra and Introductory Algebra, ENHANCED
				Martin-Gay: Prealgebra, 4e ENHANCED
				Martin-Gay: Prealgebra, 5e
				Martin-Gay: Prealgebra, 6e
				Martin-Gay: Prealgebra, 8e
				Math Preloaded Course Test Book 1
				Math Preloaded Course Test Book 2
				Math Preloaded Course Test Book 3
				Math Preloaded Course Test Book 4
				Math Preloaded Course Test Book 5
				Math TG Only
				Math US Ruch
				Mathematica V8 testing (SBnetpreview)
				Mathspace Master TOC of Exercises (version 1)
				Mathspace preloaded
				McClave: Statistics, 11e
				Media Test Book
				Melissa Honig&quot; , &quot;'&quot; , &quot;s Testing Book
				Miller: Business Mathematics, 11e
				Miller: Business Mathematics, 9th Edition
				Miller: Economics Today, 12th Edition, MATH REVIEW
				Miller: Economics Today: MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: Study Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Macro View, Study Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, MyEconLab Edition, MATH REVIEW
				Miller: Economics Today: The Micro View, Study Edition, MATH REVIEW
				Miller: Mathematical Ideas, 10th Edition ENHANCED
				Miller: Mathematical Ideas, Expanded 10th Edition
				Miller: Mathematical Ideas, Expanded 10th Edition ENHANCED
				Miller: Mathematical Ideas: 9th Edition
				Mishkin: The Economics of Money, Banking, &amp; Financial Markets, 7th Ed, MATH REV.
				MML-CoCo: Tobey Intermediate Algebra, 6e
				MMT TG Calc Test
				MXL Player Content Test Book
				MXL Player Functional Test Book
				MyFoundationsLab Prototype
				MyMathLab for Basic Mathematics and Algebra
				MyMathLab test book A (from Blitzer TM5)
				MyMathLab test book A2
				MyMathLab test book A3  - Lilani
				MyMathLab test book B (from Briggs Calc)
				MyMathLab test book C (2-level Tannenbaum)
				MyMathLab test book C2
				MyMathLab test book C3
				MyMathLab test book D (Triola 11)
				MyMathLab test book D2  MOBILE DEVICES
				MyMathLab test book D3
				MyMathLab test book Intl 1 three-level
				MyMathLab test book Intl 2 two-level
				MyMathTest: Developmental Mathematics
				Nassau CC: Lial Intermediate Algebra 9e with Trig 7e
				National University
				New Assignment Creation Wizard
				New Book Test
				New Book Test 2
				New Book Test 6
				New Import Test
				NextGen Global Edition
				NextGen Media Math Test Book
				NextGen Media Math Test Book (VOY)
				NextGen Media Math Test Book - Ipro
				NextGen Media Math Test Book Copy
				NextGen Media Math Test Book--ACR
				NextGen Media Math Test Book--hmz1
				NextGen Media Math Test Book--Kethees
				NextGen Media Math Test Book--Lak
				NextGen Media Math Test Book--MLMNG2202
				NextGen Media Math Test Book--MLMNG2202_02
				NextGen Media Math Test Book--REAL
				NextGen Media Math Test Book--REAL 123
				NextGen Media Math Test Book--REAL--Essay
				NextGen Media Math Test Book--REAL--Kasun
				NextGen Media Math Test Book--REAL--Rush
				NextGen Media Math Test Book--REAL-CATEST
				NextGen Media Math Test Book--REAL-Thilini
				NextGen Media Math Test Book--REAL_BigInt1
				NextGen Media Math Test Book--REAL_RH
				NextGen Media Math Test Book--REAL_upetha
				NextGen Media Math Test Book--REAL_VirajC
				NextGen Media Math Test Book--REAL_VirajC9
				NextGen Media Math Test Book--REAL_VirajCJuly
				NextGen Media Math Test Book--REALL
				NextGen Media Math Test Book--sample
				NextGen Media Math Test Book--Shehan
				NextGen Media Math Test Book--Standards
				NextGen Media Math Test Book--storytestings7
				NextGen Media Math Test Book--testsamplpe
				NextGen Media Math Test Book--XXL
				NextGen Media Math Test Book-_MLNG3034_1
				NextGen Media Math Test Book-Essay Ishara
				NextGen Media Math Test Book-One Chapter_MLMNG3034_2
				NextGen Media Math Test BookRH
				NextGen Media Math Test BookRH
				NextGen Test1CRMBC_Au
				NextGen1 Global Edition
				NextGen1 Global Edition - RC
				NG Math Media 21
				Notest Book
				NSM Delta Demo
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 2nd Edition
				O&quot; , &quot;'&quot; , &quot;Daffer: Mathematics for Elementary Teachers, 3e ENHANCED
				Old SB Mapping Test New
				Old SB Mapping Test Previous
				Parkin: Economics, 6th Edition, MATH REVIEW
				Parkin: Macroeconomics, 6th edition, MATH REVIEW
				Parkin: Microeconomics, 6th Edition, MATH REVIEW
				Perloff: Microeconomics, 3rd Edition, MATH REVIEW
				Phil&quot; , &quot;'&quot; , &quot;s Test Book
				Pirnot: Mathematics All Around, 2nd Edition
				Pirnot: Mathematics All Around, 5e
				PM sbnet test book 1
				Prior: Prealgebra, 1e
				Ratti: Precalculus
				Ratti: Precalculus: A Unit Circle Approach
				Req 54 Test 2 level math
				Req 54 Test Alec
				Req 54 Test book
				Req54Test-no 0 section numbers
				RH_1Ruch1
				Ritter/Silber/Udell: Prin. of Money, Banking &amp; Financial Markets 11e MATH REVIEW
				Rockswold/Krieger: Interactive Developmental Mathematics, 1e
				Rockswold: Beginning &amp; Intermediate Algebra, 3e
				Rockswold: Beginning Algebra with Applications &amp; Visualization, 1e ENHANCED
				Rockswold: Beginning and Intermediate Algebra ENHANCED
				Rockswold: College Algebra and Trigonometry
				Rockswold: College Algebra and Trigonometry through Modeling and Vis, 2nd Ed.
				Rockswold: College Algebra through Modeling and Visualization, 2e
				Rockswold: College Algebra, 4e
				Rockswold: College Algebra, 5e
				Rockswold: Intermediate Algebra
				Rockswold: Intermediate Algebra with Applications &amp; Visualization, 2e ENHANCED
				Rockswold: Precalculus through Modeling and Visualization, 2nd Edition
				Ruffin/Gregory: Principles of Macroeconomics, 7th Edition MATH REVIEW
				Ruffin/Gregory: Principles of Microeconomics, 7th Edition, MATH REVIEW
				Runtime Beta Test Book
				Rupert/Anderson: Federal Taxation 2019 Individuals 32e
				Ruth C-L Test Book
				Ruth C-L Test Book 10
				Ruth C-L Test Book 12
				Ruth C-L Test Book 6
				Ruth C-L Test Book ng
				Ruth Test Book 2 (publish)
				Ruth&quot; , &quot;'&quot; , &quot;s Test Book
				Ruth&quot; , &quot;'&quot; , &quot;s test book
				Ruth&quot; , &quot;'&quot; , &quot;s Test Book for MXL Player
				Ruwan Fake Book
				Salzman: Mathematics for Business, 7th Edition
				Sam Final Test book A
				Sam Knewton July A
				Sam Knewton July B
				Sam Math Test Book
				Sam Math Test Book-Awanthi
				Sam New Book 8-28
				Sam New Book 8_28(2)
				Sam Oct Release 2015
				Sam Test Book
				Sam Test Code1
				Sample Math Book vineetha(2)
				SD Grapher Demo Problems
				Section Media Only Math
				Sharpe: Business Statistics
				Sharpe: Business Statistics, 3e
				SI Notation testing (from sbtest)
				SPPTesting
				Squires: Basic Mathematics
				Squires: Developmental Math
				Squires: Developmental Math: Basic Math, Introductory &amp; Intermediate Algebra, 2e
				StatCrunch Test Book for Dev
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 1e
				Sullivan/Woodbury: Interactive Statistics: Informed Decisions Using Data, 2e
				Sullivan/Woodbury: Statistics Online, 1e DEMO chapter 10
				Sullivan: Algebra &amp; Trigonometry EGU, 4th Edition
				Sullivan: Algebra &amp; Trigonometry, 7e ENHANCED
				Sullivan: Algebra &amp; Trigonometry, 8e
				Sullivan: Algebra &amp; Trigonometry, 9e
				Sullivan: College Algebra EGU, 4th Edition
				Sullivan: College Algebra, 11e
				Sullivan: College Algebra, 7e ENHANCED
				Sullivan: College Algebra, 8e ENHANCED
				Sullivan: College Algebra: Concepts through Functions 2e
				Sullivan: College Algebra: Concepts Through Functions Corequisite, 4e (2019)
				Sullivan: College Algebra: Concepts through Functions, 1e ENHANCED
				Sullivan: Elementary Algebra, 2e
				Sullivan: Fundamentals of Statistics, 2e ENHANCED
				Sullivan: Fundamentals of Statistics, 3e
				Sullivan: Fundamentals of Statistics, 5e
				Sullivan: Intermediate Algebra, 1e (2007)
				Sullivan: Intermediate Algebra, 1e (2007) DEMO
				Sullivan: Intermediate Algebra, 2e
				Sullivan: Precalculus, 7e ENHANCED
				Sullivan: Precalculus, 8e
				Sullivan: Precalculus: Concepts through Functions, Right Triangle ENHANCED
				Sullivan: Precalculus: Concepts through Functions, Unit Circle ENHANCED
				Sullivan: Statistics: Informed Decisions Using Data, 2e
				Sullivan: Statistics: Informed Decisions Using Data, 4e
				Sullivan: Trigonometry, 7e ENHANCED
				Sullivan: Trigonometry: A Unit Circle Approach, 8e ENHANCED
				Sullivan: Trigonometry: A Unit Circle Approach, 9e
				TDD_Fundamentals
				TEST - Rockswold Interactive Dev Math 1e
				TEST 11/e - objectives
				Test book for Math 1473
				Test book for reporting depts 3
				TEST Gitman: Finance 11th Edition
				TEST INTELLIPRO Irv BK L2
				TEST INTELLIPRO Irv BK L3
				TEST INTELLIPRO Irv Lial: Calc w Apps, 9e (pt-BR)
				TEST INTELLIPRO Irv NextgenMath BK
				TEST INTELLIPRO Irv NG NoPub wPools BK
				Test_Awa_Book
				TEST_INTELLIPRO_BOOK_1
				TFtestbook
				Thomas&quot; , &quot;'&quot; , &quot; Calc demo
				Thomas&quot; , &quot;'&quot; , &quot; Calculus 11e DEMO
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals Media Upgrade, 11e ENHANCED
				Thomas&quot; , &quot;'&quot; , &quot; Calculus Media Upgrade, 11e
				Thomas&quot; , &quot;'&quot; , &quot; Calculus, 12e
				Thomas/Finney/Weir/Giordano: Thomas&quot; , &quot;'&quot; , &quot; Calculus Early Transcendentals, Updated 10e
				Thomas/Finney/Weir/Giordano: Thomas&quot; , &quot;'&quot; , &quot; Calculus, Updated 10th Edition
				Thomasson: Introductory &amp; Intermediate Algebra 3e
				Tiny Tara Test Text
				Tobey/Slater Intermediate Algebra, 5e (2006)
				Tobey/Slater: Basic College Math, 5e DEMO
				Tobey: Basic College Mathematics, 5e ENHANCED
				Tobey: Basic College Mathematics, 6e
				Tobey: Beginning Algebra, 6e ENHANCED
				Tobey: Beginning Algebra: Early Graphing 1e (2006)
				Tobey: Essentials of Basic College Mathematics, 1e ENHANCED
				Tobey: Essentials of Basic Collge Mathematics, 2e
				TOC Builder:XL19096-Real
				Trigsted-XL-Irv-i3
				Trigsted: College Algebra 2e 
				Trigsted: College Algebra DUPE FOR SBTEST
				Trigsted: College Algebra DUPE FOR SBTEST
				Trigsted: College Algebra DUPE FOR SBTEST--1
				Trigsted: College Algebra DUPE FOR SBTEST--irv
				Trigsted: College Algebra DUPE FOR SBTEST--irv2
				Trigsted: College Algebra DUPE FOR SBTEST--PM
				Trigsted: College Algebra Interactive
				Trigsted: College Algebra, 3e Interactive
				Trigsted: College Algebra, 3e Interactive Demo
				Trigsted: Interactive Reading Assignment Chapter 3 - Functions
				Triola: Elementary Statistics Using Excel
				Triola: Elementary Statistics Using Excel, 2nd Edition
				Triola: Elementary Statistics Using Excel, 3e ENHANCED
				Triola: Elementary Statistics Using Excel, 4e
				Triola: Elementary Statistics Using Excel, 5e
				Triola: Elementary Statistics Using Excel, 6e
				Triola: Elementary Statistics Using the Graphing Calculator ENHANCED
				Triola: Elementary Statistics Using the TI 83/84 Plus Calculator, 5e
				Triola: Elementary Statistics, 10e ENHANCED
				Triola: Elementary Statistics, 12e
				Triola: Elementary Statistics, 9e ENHANCED
				Triola: Essentials of Statistics
				Triola: Essentials of Statistics, 2e ENHANCED
				Triola: Essentials of Statistics, 3e
				Triola: Essentials of Statistics, 5e
				Tro, Introductory Chemistry, 5e
				UK Higher Ed Test Book
				UK Schools Test Book
				Universal Domain Graph Diagnostic Exercises
				University of Alberta custom Canadian DeVeaux
				University of Louisville: ENG 101/102/201: Engineering Analysis I, II, III
				Valencia Community College - Bittinger: Prealgebra
				Voy&quot; , &quot;'&quot; , &quot;s Test Book
				VW copy of Alicia&quot; , &quot;'&quot; , &quot;s test book
				vw etext2 test
				VW Import TOC new test
				VW Knewton1 copy
				VW TEST Prentice Hall Economics
				vw testing xlsb-1
				Waldman: Microeconomics, MATH REVIEW
				Washington: Basic Technical Mathematics with Calculus, 8e
				Washington: Basic Technical Mathematics, 8th Edition
				Washington: Basic Technical Mathematics, 9e
				Washington: Introduction to Technical Mathematics, 5e
				Weiss: Elementary Statistics, 5th Edition
				Weiss: Elementary Statistics, 6e ENHANCED
				Weiss: Elementary Statistics, 8e
				Weiss: Introductory Statistics, 6th Edition
				Weiss: Introductory Statistics, 7e ENHANCED
				Weiss: Introductory Statistics, 8e
				Weiss: Introductory Statistics, 9e
				Win Math 1
				WinKnewtonTest2XL
				WinKnewtonTest3XL
				Wizard test
				Workspace Test Book
				x MathXL Freelancer Test Book
				XL Demo Psych
				XL Load Testbook
				XL Load Testbook - Copy
				XLN Tara Sampler
				Young: Finite Mathematics, 3rd Edition
				ZZIA Interactive testing

			&quot;))]</value>
      <webElementGuid>d876355e-fa36-4285-b917-a359d4aa5291</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
