<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_TestCourse (1)</name>
   <tag></tag>
   <elementGuidId>1803fa33-1541-4830-b7f5-4b2d240c3a3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='${GlobalVariable.CourseID}'])[1]/preceding::a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.Outside > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bcacf139-55c4-41dc-82d4-04340955bf95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:ChangeCourse(3127431)</value>
      <webElementGuid>c8a4c129-2895-4c30-8966-3b705b7ca28e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestCourse (1)</value>
      <webElementGuid>a2216288-310a-45d9-9f5d-502bbd584d4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridCourses&quot;)/tbody[1]/tr[3]/td[@class=&quot;Outside&quot;]/a[1]</value>
      <webElementGuid>25c4f145-e44c-46e6-bb99-885393b3a3fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses']/tbody/tr[3]/td[3]/a</value>
      <webElementGuid>33a25f8d-c62c-49ef-97b6-86a3975e428c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'TestCourse (1)')])[2]</value>
      <webElementGuid>e61c1b4d-ba70-4329-aff0-651867baa0a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Current Courses'])[1]/following::a[1]</value>
      <webElementGuid>243457ae-ddc6-4e0e-a35d-cd0902b8d592</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/following::a[3]</value>
      <webElementGuid>1e626e5a-0aab-4993-b971-1d744cdf92e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL1V-1153-20L7-3Y95'])[1]/preceding::a[1]</value>
      <webElementGuid>815aa435-f185-43de-bc60-db157fcdff5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ins1 (Primary)'])[1]/preceding::a[2]</value>
      <webElementGuid>d50f651d-59f3-4f77-b486-90d1c103536c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:ChangeCourse(3127431)')]</value>
      <webElementGuid>0b080f12-d5cf-47fe-9cf2-74ddcc2248dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/a</value>
      <webElementGuid>5b706a9d-deb8-483d-96d3-4201a0b362b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:ChangeCourse(3127431)' and (text() = 'TestCourse (1)' or . = 'TestCourse (1)')]</value>
      <webElementGuid>c446a6ca-6e0f-4e26-9507-7a6348558457</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
