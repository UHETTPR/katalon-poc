<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_TestCourse (1) 0                      _2b8dd2</name>
   <tag></tag>
   <elementGuidId>db974ee9-b27b-48bc-a1ce-2aad5284a7e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ctl00_ctl00_InsideForm_BreadCrumb1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_BreadCrumb1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>86e1e6c0-3c4c-4c1f-a013-3e4ea840698f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_BreadCrumb1</value>
      <webElementGuid>aae35650-79f5-4f18-b129-975d83204e0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>breadcrumb</value>
      <webElementGuid>d9eb87aa-cb93-4c56-86ed-13d1f387869e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


    
        
            TestCourse (1) [0]
            
        
        
          
              
              Manage Course List
          
          
          TestCourse (1) [0]
        
    




     
    

    
        
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Instructor Home
                    
                
                
                
            
        
    
    
     
        .print-icon:hover{
             background-color: rgba(217, 217, 217, 0.65);
             padding-top: 10px;
             padding-bottom: 5px;
             padding-left: 8px;
             padding-right: 1px;
             border-radius: 16px
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener('mousedown', function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener('keydown', function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById('ButtonHelp').setAttribute('aria-expanded', 'false');
            }
        });

        }
    

</value>
      <webElementGuid>aed26f18-487b-4653-ba0d-615e2388390e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_BreadCrumb1&quot;)</value>
      <webElementGuid>bd8183ce-c8fa-46fc-8345-ad1981741fea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_BreadCrumb1']</value>
      <webElementGuid>247c3d68-d8f2-423e-bea8-a9dc0bfcb096</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_PnlBreadCrumb']/div/span[2]</value>
      <webElementGuid>1695967d-e990-4c8b-a121-1714bb3a498c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Instructor Resources'])[1]/following::span[2]</value>
      <webElementGuid>d0c6c081-8927-47fd-8ddc-ed236767e3db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook'])[1]/following::span[2]</value>
      <webElementGuid>46ad7ba1-dfcf-45d7-a52d-01f9bbd3b2a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>b0e74d64-7c1f-4991-844d-ec7f6e614641</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ctl00_ctl00_InsideForm_BreadCrumb1' and (text() = concat(&quot;


    
        
            TestCourse (1) [0]
            
        
        
          
              
              Manage Course List
          
          
          TestCourse (1) [0]
        
    




     
    

    
        
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Instructor Home
                    
                
                
                
            
        
    
    
     
        .print-icon:hover{
             background-color: rgba(217, 217, 217, 0.65);
             padding-top: 10px;
             padding-bottom: 5px;
             padding-left: 8px;
             padding-right: 1px;
             border-radius: 16px
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    

&quot;) or . = concat(&quot;


    
        
            TestCourse (1) [0]
            
        
        
          
              
              Manage Course List
          
          
          TestCourse (1) [0]
        
    




     
    

    
        
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Instructor Home
                    
                
                
                
            
        
    
    
     
        .print-icon:hover{
             background-color: rgba(217, 217, 217, 0.65);
             padding-top: 10px;
             padding-bottom: 5px;
             padding-left: 8px;
             padding-right: 1px;
             border-radius: 16px
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    

&quot;))]</value>
      <webElementGuid>b91636ef-51b7-4ca9-adce-b9439ad409aa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
