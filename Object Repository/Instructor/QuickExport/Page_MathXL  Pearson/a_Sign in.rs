<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign in</name>
   <tag></tag>
   <elementGuidId>90928db4-73ac-4e6f-9e0e-c90363e78212</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.button.button-big-icon.bg-color-match-medium</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0ce66bf2-954e-460d-9375-b686047d40ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')</value>
      <webElementGuid>0d528a82-7977-48e3-b4b4-657b78bfe250</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://www.mathxl.com/login_mxl.htm</value>
      <webElementGuid>7aced799-ea70-4f69-b1a0-bd0b1ed049cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium</value>
      <webElementGuid>fa7007ff-5ebc-407c-be71-05bf492750cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>99875ec4-e3d4-4558-b949-1a60ae99d103</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths cssfilters&quot;]/body[@class=&quot;has-announcement green has-two-or-less-featured-link-categories&quot;]/section[@class=&quot;hero hero--bg-img uses-mask mask-is--gradient-top random-image js--random-bg&quot;]/div[@class=&quot;wrapper no-col-stack @480.col-stack&quot;]/div[@class=&quot;col col-5&quot;]/div[@class=&quot;sign-in&quot;]/p[2]/a[@class=&quot;button button-big-icon bg-color-match-medium&quot;]</value>
      <webElementGuid>43d51c77-4a14-408c-9f74-72aa6bc4f66e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      <webElementGuid>c23a0048-89c7-4960-b5cf-cc6a682722b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign in')]</value>
      <webElementGuid>c26f556f-9419-4045-82b6-b719573a7b16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/following::a[1]</value>
      <webElementGuid>107cd685-7f8d-425d-9188-552552dc0f05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn more'])[1]/following::a[1]</value>
      <webElementGuid>aff2effe-bef5-4309-8154-f49793905635</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot username or password?'])[1]/preceding::a[1]</value>
      <webElementGuid>ce53fdd2-b5e3-4702-a29e-a8d4e5ddaa9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MyLab Math/MyLab Statistics users, sign in here'])[1]/preceding::a[2]</value>
      <webElementGuid>27959ae6-0971-4590-88b9-9b0da314b71a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>daed1c4d-06eb-4da8-817d-bcd6f9a9ad6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://www.mathxl.com/login_mxl.htm']</value>
      <webElementGuid>07a9fd2c-f3cf-4a97-b16b-cef590f862e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/p[2]/a</value>
      <webElementGuid>036623ab-fb85-4945-8cc0-704254bb2960</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://www.mathxl.com/login_mxl.htm' and (text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>a426b7f2-775f-4413-9ba2-2a56349026b3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
