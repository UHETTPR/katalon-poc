<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Export Data</name>
   <tag></tag>
   <elementGuidId>459013a6-c34c-4ad1-998a-e0f7a82a1722</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#imgBtnDownload</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='imgBtnDownload']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>64899afa-6a6e-4f29-9b34-75c5d1899e73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0)</value>
      <webElementGuid>4cfbec7a-a7ce-4bca-acda-b5e63e46a088</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>imgBtnDownload</value>
      <webElementGuid>a2368ad9-bc2a-4e0e-9442-06332acacbe0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return DownloadData();</value>
      <webElementGuid>0e212057-89e0-42d4-8d49-b7bc45d4b29b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary </value>
      <webElementGuid>f8d529ca-66a1-4121-989a-7351e4ef240c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Export Data
	</value>
      <webElementGuid>8df83e45-49dc-4e8a-bbb1-2d499186e638</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;imgBtnDownload&quot;)</value>
      <webElementGuid>66a1ce03-dd71-4c86-aafb-21d51055205c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='imgBtnDownload']</value>
      <webElementGuid>2891980c-1f97-48a4-b9bf-507fcbdc9744</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PanelWizardRegular']/div/div[2]/a</value>
      <webElementGuid>c942d1b8-0b70-48cc-bca7-35f488632142</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Export Data')]</value>
      <webElementGuid>670ce5ce-c0c5-469c-8fd4-78e17d8120af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Done'])[1]/following::a[1]</value>
      <webElementGuid>a1f1c6c7-decf-45fb-82d7-d22faa36e8df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Include inactive students'])[1]/following::a[2]</value>
      <webElementGuid>af185b51-525f-46cb-9fe6-7e1396392ef5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::a[1]</value>
      <webElementGuid>41171d55-3097-427b-8447-0c6820c48e34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Policy'])[1]/preceding::a[2]</value>
      <webElementGuid>5cecd72a-43c9-443b-b366-0a1675efb404</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0)')])[19]</value>
      <webElementGuid>f6ad5211-1bc6-40b1-85c1-4fab0f835237</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/a</value>
      <webElementGuid>e974ed63-4b7c-4e55-985e-b3cd0d31b408</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0)' and @id = 'imgBtnDownload' and (text() = 'Export Data
	' or . = 'Export Data
	')]</value>
      <webElementGuid>b51f2ea3-1035-47dd-b42c-a7eb4ee8d83c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
