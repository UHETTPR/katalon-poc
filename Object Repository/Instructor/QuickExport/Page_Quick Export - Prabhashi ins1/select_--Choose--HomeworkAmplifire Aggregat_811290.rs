<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Choose--HomeworkAmplifire Aggregat_811290</name>
   <tag></tag>
   <elementGuidId>118a9f35-8616-46fd-92a1-d25a5143cfcf</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpAnalysisAssignment' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpAnalysisAssignment' and (text() = '
	--Choose--
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

' or . = '
	--Choose--
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpAnalysisAssignment' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpAnalysisAssignment' and (text() = '
	--Choose--
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

' or . = '
	--Choose--
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpAnalysisAssignment</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>baa50f4b-f5e6-4fad-be55-30bae48b56e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpAnalysisAssignment</value>
      <webElementGuid>05386b22-744b-4c18-9b0d-f65bf0f435d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpAnalysisAssignment</value>
      <webElementGuid>74b374fe-66b1-4cda-b131-58222d63f7f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>44045929-9d75-465c-95da-7b640e06ebd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ChangeAnalysisAssignment();</value>
      <webElementGuid>d12eb581-fca8-41e7-b70f-cb99c75709df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	--Choose--
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

</value>
      <webElementGuid>6917e097-aff6-4776-8fd6-96aea31aaf7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpAnalysisAssignment&quot;)</value>
      <webElementGuid>c41e6a3b-3ea6-416e-b1da-47dc6b55c7f4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpAnalysisAssignment']</value>
      <webElementGuid>9790813d-aae8-42b8-b222-2a7570a8e633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='trAnalysisAssignment']/td[2]/select</value>
      <webElementGuid>42bdc891-3607-454e-8846-21c8be8462e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignment'])[1]/following::select[1]</value>
      <webElementGuid>78e820c7-d149-4e30-ae91-ada95f6f712e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item summary and scores by student'])[1]/following::select[1]</value>
      <webElementGuid>0437a04e-4aef-469f-a5e8-7f39614c8c2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item Associations'])[1]/preceding::select[1]</value>
      <webElementGuid>bf6d64b3-44c0-4057-a39e-50896d9ce2ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/preceding::select[1]</value>
      <webElementGuid>d8ce6079-238d-4a53-95fa-4f00c4eaef2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/select</value>
      <webElementGuid>aa126cb2-efa4-4eb7-9d59-547a92ab6811</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpAnalysisAssignment' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpAnalysisAssignment' and (text() = '
	--Choose--
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

' or . = '
	--Choose--
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

')]</value>
      <webElementGuid>4a1e7202-bdb9-4699-bb73-9180cf62ac27</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
