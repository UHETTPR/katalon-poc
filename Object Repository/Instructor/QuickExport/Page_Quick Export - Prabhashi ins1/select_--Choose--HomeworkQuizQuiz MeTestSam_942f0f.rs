<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Choose--HomeworkQuizQuiz MeTestSam_942f0f</name>
   <tag></tag>
   <elementGuidId>c35997b7-02d5-459f-929c-140ad797cae5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpExportOptions</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpExportOptions']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>d2a186bf-031a-4e2f-813f-ff2c0c557044</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpExportOptions</value>
      <webElementGuid>a125a4c8-4a7b-4256-9b59-d89bb7af6a3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpExportOptions</value>
      <webElementGuid>15fb9ecc-25de-43c2-92f3-b1e739b77143</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control export-options-width</value>
      <webElementGuid>4499783f-a582-4ac9-8371-5b55ead4e96f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>showOptions();</value>
      <webElementGuid>a5989816-e0bc-4281-95d6-f948c8570ac1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	--Choose--
	Homework
	Quiz
	Quiz Me
	Test
	Sample Tests
	Other
	Study Plan
	Overview of student averages
	Item Analysis

</value>
      <webElementGuid>462c1578-d597-4fc3-bb9d-ab8299d3dd23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpExportOptions&quot;)</value>
      <webElementGuid>13705fd3-f0e5-4a4a-bb87-ae9d23620bf5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpExportOptions']</value>
      <webElementGuid>3f205a0c-424b-4623-9b56-ea71557ca642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/table/tbody/tr/td[2]/select</value>
      <webElementGuid>f0cac466-74d5-4dfa-9cfa-fa01dc6bbe29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Set'])[1]/following::select[1]</value>
      <webElementGuid>15982955-1704-42a7-91a0-2dfd4cb0feb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change delimiter...'])[1]/following::select[1]</value>
      <webElementGuid>6506f2d7-71c5-481f-8034-c13eee408b15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Content Areas'])[1]/preceding::select[1]</value>
      <webElementGuid>7910ee27-49e8-422c-acf0-aad60daf5ccc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spreadsheet Layout'])[1]/preceding::select[2]</value>
      <webElementGuid>918b1428-b1ca-4e7f-aed7-460ced92a936</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>6f4c9c30-6f33-4ded-a04a-b16e0859bc90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpExportOptions' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpExportOptions' and (text() = '
	--Choose--
	Homework
	Quiz
	Quiz Me
	Test
	Sample Tests
	Other
	Study Plan
	Overview of student averages
	Item Analysis

' or . = '
	--Choose--
	Homework
	Quiz
	Quiz Me
	Test
	Sample Tests
	Other
	Study Plan
	Overview of student averages
	Item Analysis

')]</value>
      <webElementGuid>c76e2fe6-5a68-4e58-8edb-4191de45ff80</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
