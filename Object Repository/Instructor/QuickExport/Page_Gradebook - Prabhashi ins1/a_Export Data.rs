<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Export Data</name>
   <tag></tag>
   <elementGuidId>bd6a8c42-0433-4562-9559-cef39d24a865</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[4]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>981e26ee-fc4b-4e06-a014-cb6645790a92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/ExportData.aspx?sender=gradebook.aspx</value>
      <webElementGuid>65ceda5f-bbd4-4456-ad7d-2b26ae7075e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Export Data</value>
      <webElementGuid>52e53cbb-80ce-49b4-b00f-92dfed1ae023</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>a6793598-bbea-47c9-b5c1-528c567db3c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Export Data</value>
      <webElementGuid>b74fd1ed-2a65-4759-ba96-de1b6e4a5ac1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_toolbar1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-link&quot;]/a[1]</value>
      <webElementGuid>f69dab90-88d6-430e-8905-6b662ad35b21</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[4]/a</value>
      <webElementGuid>d227fa98-9261-43f5-a555-77f33f739625</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Export Data')]</value>
      <webElementGuid>b99dcd46-f2f6-4152-b2d8-69d72f90034f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Weights'])[1]/following::a[2]</value>
      <webElementGuid>f99d7e8a-9340-4634-bbd2-e88fb96da24e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manage Incompletes'])[1]/following::a[3]</value>
      <webElementGuid>83341dee-d1b4-4be6-9154-3f0adb208108</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Results By'])[1]/preceding::a[2]</value>
      <webElementGuid>65d4bf2c-d2bf-4495-ab90-bb1a7eb2804f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Path'])[1]/preceding::a[2]</value>
      <webElementGuid>8581df3e-bc74-4a39-aa23-c891ca62c283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Export Data']/parent::*</value>
      <webElementGuid>cf74f9f7-e3a5-4fb2-bef8-424ac58d6552</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/ExportData.aspx?sender=gradebook.aspx')]</value>
      <webElementGuid>3491e621-4746-486a-b9fc-e834fac462d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div/ul/li[4]/a</value>
      <webElementGuid>74dc4f13-df84-47d5-9c3d-d882cdfcc80a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/ExportData.aspx?sender=gradebook.aspx' and (text() = 'Export Data' or . = 'Export Data')]</value>
      <webElementGuid>a351ac0e-25a8-431e-a5e6-54c58044209f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
