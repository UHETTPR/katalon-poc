<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_OfflineQuiz</name>
   <tag></tag>
   <elementGuidId>4617b3f8-ff4e-4230-9830-43791b653430</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tbody[@id='assignbody']/tr[2]/td[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>tr.alt > td.Outside</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>e10ec8b3-e5ce-40cd-bbd0-1994524c567f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Outside</value>
      <webElementGuid>28403275-ec41-4692-8339-60001016ce15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>valign</name>
      <type>Main</type>
      <value>top</value>
      <webElementGuid>f1018415-407a-4a88-b4c0-c153dc4bb34c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    OfflineQuiz
                </value>
      <webElementGuid>00ed57a3-923d-47de-8f90-20b5833f0f58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assignbody&quot;)/tr[@class=&quot;alt&quot;]/td[@class=&quot;Outside&quot;]</value>
      <webElementGuid>5ec26395-0910-4697-a391-8d6be9d995cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='assignbody']/tr[2]/td[3]</value>
      <webElementGuid>509db3f8-bf46-494b-ab5d-0d8daec57a10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go'])[1]/following::td[3]</value>
      <webElementGuid>1083fce3-f9ab-49a5-b0e7-5bfed3157a15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/following::td[6]</value>
      <webElementGuid>5a43b460-6356-4343-942f-436fa5a3fd5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quiz'])[1]/preceding::td[1]</value>
      <webElementGuid>e61d817b-0a5c-4b25-ae15-c3916e51e535</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go'])[2]/preceding::td[4]</value>
      <webElementGuid>bb0744c1-a404-4a96-bbbc-28af9d67ccd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OfflineQuiz']/parent::*</value>
      <webElementGuid>24984c9d-107b-4109-87c4-1b1897f5671c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[3]</value>
      <webElementGuid>72bd908d-134b-4018-bcc3-448ee734b08f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                    OfflineQuiz
                ' or . = '
                    OfflineQuiz
                ')]</value>
      <webElementGuid>cd05ac94-b042-48d9-9aa9-f10a681fd269</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
