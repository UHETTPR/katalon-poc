<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Manage Offline Items</name>
   <tag></tag>
   <elementGuidId>74fdb783-e60b-48de-aa17-afb72ddca3b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2[@id='PageHeaderTitle']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#PageHeaderTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>7fecf6bb-10e4-402f-b673-bce98786bb72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PageHeaderTitle</value>
      <webElementGuid>ac86c234-6e71-4603-b206-44fef35a3729</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>integrationheaderstyle PageHeaderTitle header-component-float-left header-font-size</value>
      <webElementGuid>2c1d7a8e-364f-4a8a-9a54-9b5552aa6156</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Manage Offline Items</value>
      <webElementGuid>46834e24-4075-4321-9e1f-f729c2a7a1d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PageHeaderTitle&quot;)</value>
      <webElementGuid>2f49b27c-6d42-4cb0-b445-8c28cbd7684d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h2[@id='PageHeaderTitle']</value>
      <webElementGuid>c20bcbb1-fb80-48d5-a152-dff1bc3d7892</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div/div/div[2]/h2</value>
      <webElementGuid>da32cc99-7483-4afb-a36a-43b259df2852</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MML Global Test Book'])[1]/following::h2[1]</value>
      <webElementGuid>61b107ec-9a43-4aba-9efe-3ee95c012190</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prabhashi ins1'])[3]/following::h2[1]</value>
      <webElementGuid>710d3c1c-bf5a-42dd-a11d-dd15247fd45e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/preceding::h2[1]</value>
      <webElementGuid>c7840795-3ae6-461c-aaf0-5b5a96c013d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[3]/preceding::h2[1]</value>
      <webElementGuid>bc572697-14ea-40a7-9619-505244a10d94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Manage Offline Items']/parent::*</value>
      <webElementGuid>d261d8b1-02a0-4996-8601-79813304715c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>31226b94-b433-4c1b-92fd-5fa3edb8089e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[@id = 'PageHeaderTitle' and (text() = '
            Manage Offline Items' or . = '
            Manage Offline Items')]</value>
      <webElementGuid>5cc5e5fe-6ed8-408a-8555-74372f01bee2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
