<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html_Manage Offline Items - Prabhashi ins1 _d67bb4</name>
   <tag></tag>
   <elementGuidId>799fa787-4841-48fb-899d-738aa8c71f7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='']/parent::*</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>html</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>html</value>
      <webElementGuid>6ef02c6e-caa0-441b-853f-3f0a1f193205</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/1999/xhtml</value>
      <webElementGuid>90745757-560e-423f-beef-f457a756ea1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en</value>
      <webElementGuid>1eeae842-8e87-4ad5-9217-43232c8bd252</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	Manage Offline Items - Prabhashi ins1

         
             var gtmaccount = &quot;GTM-5JX93LK&quot;;
             var gtmpreview = &quot;env-2&quot;;
             var gtmauth = &quot;SVJdwwG3ki5lLhJ_zIzvuA&quot;;
             var dataLayer = [];
        
  
        
    

        
       
       
    
    
        TABLE.grid TD 
            { 
              vertical-align:middle;
              height: 34px;

            }
    
	
	    function GoBack()
	    {
	        deltaBack();
	        return false;
	    }

	    function DoAction(cbxActionID, assignmentId, assignmentType)
	    {   
	        var cbxAction=document.getElementById(cbxActionID);
	        var selAction= cbxAction.options[cbxAction.selectedIndex].value;
	        if (selAction==&quot;Change Scores&quot;)
	        {
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=step3&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Edit Item Info&quot;)
	        {
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=step2&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Copy Offline&quot;)
	        {
	            //location=&quot;CopyOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;externalId=&quot; + assignmentId + &quot;&amp;back=ManageOfflineItems.aspx&quot;;
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=copy&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Delete Offline&quot;)
	        {
	            location=&quot;AssignmentsDelete.aspx?back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else
	        {
	            // debug
	            //location=&quot;OfflineItemSummary.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;back=ManageOfflineItems.aspx&quot;;
	        }
	        return false;
	    }
		
		
	    /*=======================================
         * INIT
         *=======================================
         */
	    // Chain to body onload
	    if (document.getElementById &amp;&amp; document.createElement) {    
	        if (window.addEventListener) {
	            window.addEventListener(&quot;load&quot;,Init,true);
	        } else if (window.attachEvent){
	            window.attachEvent(&quot;onload&quot;,Init);
	        }
	    }

	    function Init()
	    {
	        // handle omitted assignments
	        var tbl = document.getElementById(&quot;GridAssignments&quot;);
	        if (tbl) 
	        {
	            var rows = tbl.getElementsByTagName(&quot;tr&quot;);
	            for (var i = 0; i &lt; rows.length; i++)
	                if (rows[i].getAttribute(&quot;omit&quot;) == &quot;1&quot;)
	                    rows[i].className += &quot; omit&quot;;
	        }
	    }
	





(window.BOOMR_mq=window.BOOMR_mq||[]).push([&quot;addVar&quot;,{&quot;rua.upush&quot;:&quot;false&quot;,&quot;rua.cpush&quot;:&quot;false&quot;,&quot;rua.upre&quot;:&quot;false&quot;,&quot;rua.cpre&quot;:&quot;true&quot;,&quot;rua.uprl&quot;:&quot;false&quot;,&quot;rua.cprl&quot;:&quot;false&quot;,&quot;rua.cprf&quot;:&quot;false&quot;,&quot;rua.trans&quot;:&quot;SJ-8413d74b-58f9-49bf-a874-5e8706e383f8&quot;,&quot;rua.cook&quot;:&quot;true&quot;,&quot;rua.ims&quot;:&quot;false&quot;,&quot;rua.ufprl&quot;:&quot;false&quot;,&quot;rua.cfprl&quot;:&quot;true&quot;,&quot;rua.isuxp&quot;:&quot;false&quot;,&quot;rua.texp&quot;:&quot;norulematch&quot;}]);
                              !function(a){var e=&quot;https://s.go-mpulse.net/boomerang/&quot;,t=&quot;addEventListener&quot;;if(&quot;False&quot;==&quot;True&quot;)a.BOOMR_config=a.BOOMR_config||{},a.BOOMR_config.PageParams=a.BOOMR_config.PageParams||{},a.BOOMR_config.PageParams.pci=!0,e=&quot;https://s2.go-mpulse.net/boomerang/&quot;;if(window.BOOMR_API_key=&quot;B7LE9-GF2JD-PPN44-7ULXU-DPSH4&quot;,function(){function n(e){a.BOOMR_onload=e&amp;&amp;e.timeStamp||(new Date).getTime()}if(!a.BOOMR||!a.BOOMR.version&amp;&amp;!a.BOOMR.snippetExecuted){a.BOOMR=a.BOOMR||{},a.BOOMR.snippetExecuted=!0;var i,_,o,r=document.createElement(&quot;iframe&quot;);if(a[t])a[t](&quot;load&quot;,n,!1);else if(a.attachEvent)a.attachEvent(&quot;onload&quot;,n);r.src=&quot;javascript:void(0)&quot;,r.title=&quot;&quot;,r.role=&quot;presentation&quot;,(r.frameElement||r).style.cssText=&quot;width:0;height:0;border:0;display:none;&quot;,o=document.getElementsByTagName(&quot;script&quot;)[0],o.parentNode.insertBefore(r,o);try{_=r.contentWindow.document}catch(O){i=document.domain,r.src=&quot;javascript:var d=document.open();d.domain='&quot;+i+&quot;';void(0);&quot;,_=r.contentWindow.document}_.open()._l=function(){var a=this.createElement(&quot;script&quot;);if(i)this.domain=i;a.id=&quot;boomr-if-as&quot;,a.src=e+&quot;B7LE9-GF2JD-PPN44-7ULXU-DPSH4&quot;,BOOMR_lstart=(new Date).getTime(),this.body.appendChild(a)},_.write(&quot;&lt;bo&quot;+'dy onload=&quot;document._l();&quot;>'),_.close()}}(),&quot;&quot;.length>0)if(a&amp;&amp;&quot;performance&quot;in a&amp;&amp;a.performance&amp;&amp;&quot;function&quot;==typeof a.performance.setResourceTimingBufferSize)a.performance.setResourceTimingBufferSize();!function(){if(BOOMR=a.BOOMR||{},BOOMR.plugins=BOOMR.plugins||{},!BOOMR.plugins.AK){var e=&quot;true&quot;==&quot;true&quot;?1:0,t=&quot;cookiepresent&quot;,n=&quot;2ruo3vyxggvteyt3huda-f-288e55f37-clientnsv4-s.akamaihd.net&quot;,i=&quot;false&quot;==&quot;true&quot;?2:1,_={&quot;ak.v&quot;:&quot;32&quot;,&quot;ak.cp&quot;:&quot;1248014&quot;,&quot;ak.ai&quot;:parseInt(&quot;749947&quot;,10),&quot;ak.ol&quot;:&quot;0&quot;,&quot;ak.cr&quot;:94,&quot;ak.ipv&quot;:4,&quot;ak.proto&quot;:&quot;h2&quot;,&quot;ak.rid&quot;:&quot;8f02d43&quot;,&quot;ak.r&quot;:31130,&quot;ak.a2&quot;:e,&quot;ak.m&quot;:&quot;x&quot;,&quot;ak.n&quot;:&quot;essl&quot;,&quot;ak.bpcip&quot;:&quot;212.104.237.0&quot;,&quot;ak.cport&quot;:15367,&quot;ak.gh&quot;:&quot;104.75.84.58&quot;,&quot;ak.quicv&quot;:&quot;&quot;,&quot;ak.tlsv&quot;:&quot;tls1.3&quot;,&quot;ak.0rtt&quot;:&quot;&quot;,&quot;ak.csrc&quot;:&quot;-&quot;,&quot;ak.acc&quot;:&quot;&quot;,&quot;ak.t&quot;:&quot;1652243718&quot;,&quot;ak.ak&quot;:&quot;hOBiQwZUYzCg5VSAfCLimQ==JSvUV7lR8ws3FlLtfHMNEIMeD8M/xaKWPgwPMc4aHnCeUfTacVoU0h5uBEV4DDsMQk+LoUk8yryrbgVaJ9WTaiRZXBdlzLJyVxOxqbc1CoNKs1kZ3EjHXj+FW8QW5yvmeHwUThlJqZf+iFTTyHFgzHFaszcdwkgOInHHPYDNoD8+zDLQITmHSopGC74rvFGr5htxI3hiMTKdd+yxtHvtSKDWbZwh2fmB4Lzvu4tA0Svxs1vMYgHEaCjm0k39aNhU7ZXB+TehpGt7PE8lxDuV1pZScVI19HwEnr8JO5JQ4gK+sYddwIILiDaksAOKJGK4pE+FVReKYABRKVoW8lLUqYLJl04BahTxGjB15UuFNsw7Lc986bS/t692/aUVHO2H3rhlzarDxnkGIzLfsFVUKGFdDJHbHFeHGgNaIC28zqs=&quot;,&quot;ak.pv&quot;:&quot;1&quot;,&quot;ak.dpoabenc&quot;:&quot;&quot;,&quot;ak.tf&quot;:i};if(&quot;&quot;!==t)_[&quot;ak.ruds&quot;]=t;var o={i:!1,av:function(e){var t=&quot;http.initiator&quot;;if(e&amp;&amp;(!e[t]||&quot;spa_hard&quot;===e[t]))_[&quot;ak.feo&quot;]=void 0!==a.aFeoApplied?1:0,BOOMR.addVar(_)},rv:function(){var a=[&quot;ak.bpcip&quot;,&quot;ak.cport&quot;,&quot;ak.cr&quot;,&quot;ak.csrc&quot;,&quot;ak.gh&quot;,&quot;ak.ipv&quot;,&quot;ak.m&quot;,&quot;ak.n&quot;,&quot;ak.ol&quot;,&quot;ak.proto&quot;,&quot;ak.quicv&quot;,&quot;ak.tlsv&quot;,&quot;ak.0rtt&quot;,&quot;ak.r&quot;,&quot;ak.acc&quot;,&quot;ak.t&quot;,&quot;ak.tf&quot;];BOOMR.removeVar(a)}};BOOMR.plugins.AK={akVars:_,akDNSPreFetchDomain:n,init:function(){if(!o.i){var a=BOOMR.subscribe;a(&quot;before_beacon&quot;,o.av,null,null),a(&quot;onbeacon&quot;,o.rv,null,null),o.i=!0}return this},is_complete:function(){return!0}}}}()}(window);#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}

    
        &lt;iframe src=&quot;https://www.googletagmanager.com/ns.html?id=GTM-5JX93LK&amp;gtm_auth=SVJdwwG3ki5lLhJ_zIzvuA&amp;gtm_preview=env-2&amp;gtm_cookies_win=x&quot;
        height=&quot;0&quot; width=&quot;0&quot; style=&quot;display:none;visibility:hidden&quot;>&lt;/iframe>
    
	
    Skip Navigation

	








//&lt;![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>







//&lt;![CDATA[
var BaseUrl=&quot;../&quot;;
var BaseAbsUrl=&quot;https://mylabglobal.pearson.com/&quot;;
var BackUrl=&quot;gradebook.aspx&quot;;
var LocDateParts= { &quot;ttam&quot;: &quot;AM&quot;, &quot;ttpm&quot;: &quot;PM&quot;};
var LocClockPattern=&quot;dd/MM/yy HH:mm&quot;;
var StrUtils_DecimalSeparator=&quot;.&quot;;
LOCNS.LocMap.RegRes(&quot;common/formtracker.js&quot;,&quot;LeaveAlert&quot;,&quot;Your changes will not be saved.&quot;);
LOCNS.LocMap.RegRes(&quot;common/formtracker.js&quot;,&quot;LeaveConfirm&quot;,&quot;Are you sure you want to navigate away from this page?\n\n\n\n{0}\n\n\n\nPress OK to continue, or Cancel to stay on the current page.&quot;);
LOCNS.LocMap.RegRes(&quot;common/popups_1.js&quot;,&quot;PopupBlockedWarning&quot;,&quot;Your browser is blocking pop-ups, and they need to be allowed before working in your course. \nPlease disable your pop-up blocker now.&quot;);
LOCNS.LocMap.RegRes(&quot;resources&quot;,&quot;Cancel&quot;,&quot;Cancel&quot;);
LOCNS.LocMap.RegRes(&quot;resources&quot;,&quot;OK&quot;,&quot;OK&quot;);
//]]>







	

		
	
	

        
        
        
            
        
           
                         
                             
			

				
					
						
						Hide Navigation
					
				

                 
                        GlobalTestCourse
                

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnrol in CourseEdit AccountLog Out|HelpHelpLegendTours &amp; trainingMyLab Math Global SupportBrowser Check|11/05/22 10:05
				

			
		
        
        

		    
			    
			    
			    
		    

		    STUDENTCourse HomeStudy PlanCalendarHomework and TestsResultsStudent ResourcesINSTRUCTORInstructor HomeCourse ManagerHome Page ManagerAssignment ManagerStudy Plan ManagerGradebookInstructor Resources

	    
                             
        
		        
		            
                    
                        
                            
                                                     
                                
                                
	
                                    
                                        
                                        
                                        


    
        
            GlobalTestCourse [1]
            
        
        
          
              
              Manage Course List
          
          
          06/05_Downtime_GlobalPreBvt [0]Coord: GlobalCoordTestCourse [0]GlobalTestCourse [1]SampleCourse [0]
        
    




     
    

|GRADEBOOK
                                    
                                    
                                


                                
                                
                                
                                 
                                      
                                                
                                                
                                                    
                                                        Prabhashi ins1
                                                        GlobalTestCourse
                                                    
                                                    
                                                        Prabhashi ins1
                                                        MML Global Test Book
                                                    
                                                    
                                                        11/05/22
                                                        10:05
                                                    
                                                
                                                
                                                
                                    
                            
                        
                    
					
							
							    
    
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$InsideForm$MasterContent$ScriptManager1', 'aspnetForm', [], [], [], 90, 'ctl00$ctl00');
//]]>



    


       
           
        
            
        
        
           
            
            Manage Offline Items   
           
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Manage Offline Items
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener('mousedown', function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener('keydown', function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById('ButtonHelp').setAttribute('aria-expanded', 'false');
            }
        });

        }
    
 
        
        
      
    
    



    
             
	

    
    
	
		
			
				Ch. ItemCategoryDateActions 
			
		
			
				
                    1‑4
                
                    
                
                    OfflineTest
                
                    Test
                
                    11/05/22
                
                        
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				
                
                    Go
				
                
			
				
                    1‑4
                
                    
                
                    OfflineQuiz
                
                    Quiz
                
                    11/05/22
                
                        
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				
                
                    Go
				
                
			
		
	




    
    Done

    
    


                                 
        			 
                    
                        
                                
                                   
                                        This course is based on MML Global Test BookTerms of Use|Privacy Policy|Copyright 2022 Pearson  All Rights Reserved.
                                              
                                
                            
                                    

                            
                                               
                        
                    
                     
                
	    
                       
                        
         

        
        
	


        
	    [+]
	

 
       
	

	

	

       
        
            var gaaccount = &quot;UA-48773414-1&quot;;
            var environment = &quot;global&quot;;
    
	
	
	
		

		

		

	
			
        
            var initialjsclockServerDateTime = &quot;Wed, 11 May 2022 10:05:18&quot;;
            function showLoading(fromTimer)
            {
                var el = document.getElementById(&quot;loading&quot;);
                // don't show this if it's already been hidden

                if(AllowShowPleaseWait &amp;&amp; el)
                {
                    el.style.display=&quot;&quot;;  
                }
            }
        
            function CourseChanged(select, originalSelectedValue) {
                //var courseId = item.Value;
                var courseId = select.options[select.selectedIndex].value;
                var loc = 'ChangeCourse.aspx'
                //JS: this param needs to be removed.
                //It is passed into the assignment manager in the Qstring
                //if we don't remove it, it keeps getting passed along.
                var tmplocation=location.pathname;
                tmplocation=removeParam(tmplocation,&quot;scId&quot;);
        
                // CA same for experiment assignment id, which generates a popup after the user creates an experiment assignment and clicks on the appropriate link
                tmplocation = removeParam(tmplocation, &quot;experimentAssignmentId&quot;);

                // Remove contentAreaID in case it's not valid in the new course
                tmplocation = removeParam(tmplocation, &quot;ContentAreaID&quot;);

                // XL-3896 - Targeted UI Changes: Remove isForceAssignmentManager because it is not valid in the new course
                tmplocation = removeParam(tmplocation, &quot;isForceAssignmentManager&quot;);
        
                loc = loc + '?courseId='+courseId+'&amp;target='+escape(tmplocation);
        
                //This is to fix the JS &quot;Unspecificed Error&quot; that occurs when the form tracking is on
                //and the user changes the course and clicks cancel on the prompt.
                //http://aspnet.4guysfromrolla.com/articles/042005-1.aspx
                try
                {
                    location = loc;
                }
                catch (err)
                {
                    //set it back to original.  They hit cancel.
                    select.selectedIndex=originalSelectedValue;
                }
            }
      
            function AlertChooseBookMyCourses(newlocation)
            {
                alert(&quot;The course or book you were just viewing is not in your course list.  Please choose the course you would like to work in from the My Courses page.&quot;);
                if (newlocation)
                    location = newlocation; // Comment 9610
                else
                    location.reload(); // comment 9402
            }
      
            function AlertChooseBook()
            {
                alert(&quot;The course or book you were just viewing is not in your course list.  Please choose the course you would like to work in from the course drop down&quot;);
                location.reload(); // comment 7928
            }
      
            function AlertCourseChanged()
            {
                alert(&quot;The course or book you were just viewing as a student is not in your course list.  Choose the course you want to work in from the course drop down.&quot;);
                location.reload(); // comment 7928
            }
      
            function AlertCourseChangedEmptyCurrent()
            {
                alert(&quot;You have chosen not to show any courses in this drop down menu. To add courses to this list, go to Manage Course List and flag the courses as current courses.&quot;);
                location.reload(); // requested in C07-12
            }
      
            var navVisible = true;
            wizardUrl = &quot;../BrowserCheck/BrowserCheck.aspx&quot;;
            supportLink = &quot;&quot;;
            var navUrl = &quot;../Service/LeftNavStateUpdate.aspx&quot;

            

            function initLeftNav()
            {
                LeftNavStateUpdate.init(navUrl);  
            }

            function onNavCallback()
            {
                var navDiv = document.getElementById('leftnavcontainer');
                var navShowDiv = document.getElementById('leftnavshow');  
  
                if (navDiv.style.display==&quot;none&quot;) 
                {
                    navDiv.style.display = &quot;block&quot;;
                    navShowDiv.style.display = &quot;none&quot;;
                    navVisible = true;
                }
                else
                {
                    navDiv.style.display = &quot;none&quot;;
                    navShowDiv.style.display = &quot;block&quot;;          
                    navVisible = false;
                }
            }


            function toggleNav_OLD()
            {
                var navDiv = document.getElementById('leftnavcontainer');
  
                var navCurrentlyVisible = (navDiv.style.display != &quot;none&quot;);

                LeftNavStateUpdate.doNavCallback(!navCurrentlyVisible, onNavCallback);   
            }

            function goMyCourses() 
            {
                location = &quot;MyCourses.aspx&quot;;
                return false;
            }

            function goCourseHome()
            {
                var courseHomeUrl = &quot;&quot;;

                if (courseHomeUrl != &quot;&quot;)
                    LeftNavStateUpdate.doNavCallback(false, null, courseHomeUrl);    
            }

            function goStudyPlan()
            {
                location = &quot;../Student/StudyPlanRecommendations.aspx&quot;;
            }

            function goStudyPlanHomepage() 
            {
                location = &quot;../Student/StudyPlanRecommendations.aspx&quot;;
            }

            function setupClock()
            {
                jsclockServerDateTime = &quot;Wed, 11 May 2022 10:05:18&quot;;
                jsclockElementId = &quot;toptime&quot;;
                jsclockConstantUpdate = true;
                isSelfStudyCourse = &quot;False&quot;;
                
  
                // check if it's there just in case we're not on IntelliPage
                if (typeof(initClock)==&quot;function&quot;)
                    initClock();
            }

            function ShowStudentCalendar()
            {
                var loc = '../Student/MyDashboard.aspx'
                location=loc;
            }

            var myNavParams = myNavParams || {
                productId: 44,
                userId: 744891,
                userName: &quot;Prabhashi ins1&quot;,
                authSystemId: 1,
                courseId: 68825,
                bookId: 990213,
                backdoor: 'False',
                nextgen: 'True',
                lockdownbrowsertype: 'None',
                currentCulture: &quot;en-IE&quot;,
                showBanner: true,
                showLeftNavigation: true,
                leftNavState: 'False',
                navigationVersion: 'v1',
                userRole: 'Instructor',
                hasCustomBanner : true,
                pageLinkMode : &quot;Default&quot;,
                isCC : false,
                showPlayerHeader : false,
                showBrowserCheck: true,
                IsSelfStudy: false,
                hideNavigationText: &quot;Hide Navigation&quot;,
                showNavigationText: &quot;Show Navigation&quot;,
                institutionKeyId: &quot;&quot;,
                school: &quot;SMS Default Institution&quot;,
                authSystemPartnerId: &quot;&quot;,
                bookCode: &quot;hedmathstest&quot;,
                disciplineID: &quot;30&quot;,
                userOrigin: 'MathXL',
                authsystemtypeId: 'Sms',
                supportNewNavigation: true,
                supportMobile: true,
                isMyFoundations: false,
                currentLocaleFormat: 'MM/DD/YY',
                IPv4: '212.104.237.215',
                IPv6: 'NA',
                SessionID: 'hncfii2gclba5im025uflpew',
                ProductName: 'MyLab Math Global',
                ProductPlatformCode: 'Standalone',
                TransactionLocalDate: '2022-05-11T10:05:18.831',
                IsTelemetryEnabled: 'True',
                NoNameScreenReaderText: &quot;&quot;
            };

            var siteheader = '';
            var sitenav = '';
            var printheader = 'ctl00_ctl00_InsideForm_printheader';

        
        

    
    
    
    
        setupClock();
    
        
    
	
	

     
         var pnlBreadCrumbId = 'ctl00_ctl00_InsideForm_PnlBreadCrumb';
         var pnlGBBreadCrumbId = 'ctl00_ctl00_InsideForm_PnlGradebookBreadCrumb';
     
    

        var navigation_key;
        function setCurrentNavigationKey(navKey) {
            //window.localStorage['lastNavKey'] = navKey;
            navigation_key = navKey;
        }

        setCurrentNavigationKey(35);
    
    
    
        $(document).ready(function () {
            $(&quot;#GridAssignments tbody&quot;).attr('id', 'assignbody');
        });

    
    

    
    
		
		    
            var currentMaster = &quot;default&quot;;
		
            var navigationData = {&quot;TopNavigation&quot;:[{&quot;Key&quot;:49,&quot;NameResourceKey&quot;:&quot;USERNAME&quot;,&quot;TooltipResourceKey&quot;:&quot;USERNAME&quot;,&quot;Name&quot;:&quot;prabhashi_ins1&quot;,&quot;Url&quot;:&quot;#&quot;,&quot;Tooltip&quot;:&quot;prabhashi_ins1&quot;,&quot;SubMenuData&quot;:[{&quot;Key&quot;:0,&quot;NameResourceKey&quot;:&quot;Master.MyCourses&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.MyCourses&quot;,&quot;Name&quot;:&quot;My Courses&quot;,&quot;Url&quot;:&quot;javascript:goMyCourses();&quot;,&quot;Tooltip&quot;:&quot;My Courses&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:1,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:52,&quot;NameResourceKey&quot;:&quot;Master.EnrollinCourse&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Enrollincourse&quot;,&quot;Name&quot;:&quot;Enrol in Course&quot;,&quot;Url&quot;:&quot;/student/enrollwizard.aspx&quot;,&quot;Tooltip&quot;:&quot;Enrol in Course&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:50,&quot;NameResourceKey&quot;:&quot;Master.EditAccount&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.EditAccount&quot;,&quot;Name&quot;:&quot;Edit Account&quot;,&quot;Url&quot;:&quot;javascript:DoHelp('https://register.pearsoncmg.com/userprofile/up_login.jsp');&quot;,&quot;Tooltip&quot;:&quot;Edit Account&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:53,&quot;NameResourceKey&quot;:&quot;Master.Logout&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Logout&quot;,&quot;Name&quot;:&quot;Log Out&quot;,&quot;Url&quot;:&quot;/logout.aspx&quot;,&quot;Tooltip&quot;:&quot;Log Out&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:43,&quot;NameResourceKey&quot;:&quot;Master.Help&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Help&quot;,&quot;Name&quot;:&quot;Help&quot;,&quot;Url&quot;:&quot;&quot;,&quot;Tooltip&quot;:&quot;Help&quot;,&quot;SubMenuData&quot;:[{&quot;Key&quot;:44,&quot;NameResourceKey&quot;:&quot;Master.Help&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Help&quot;,&quot;Name&quot;:&quot;Help&quot;,&quot;Url&quot;:&quot;javascript:doHelp('toc_instructor_math');&quot;,&quot;Tooltip&quot;:&quot;Help&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:46,&quot;NameResourceKey&quot;:&quot;Master.Legend&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Legend&quot;,&quot;Name&quot;:&quot;Legend&quot;,&quot;Url&quot;:&quot;javascript:doHelp('legends');&quot;,&quot;Tooltip&quot;:&quot;Legend&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:59,&quot;NameResourceKey&quot;:&quot;Master.TakeaTourIntl&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.TakeaTourIntl&quot;,&quot;Name&quot;:&quot;Tours &amp; training&quot;,&quot;Url&quot;:&quot;javascript:doHelp('instr_tours');&quot;,&quot;Tooltip&quot;:&quot;Tours &amp; training&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:57,&quot;NameResourceKey&quot;:&quot;Master.ProductSupport&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.ProductSupport&quot;,&quot;Name&quot;:&quot;MyLab Math Global Support&quot;,&quot;Url&quot;:&quot;javascript:doHelp('instr_support');&quot;,&quot;Tooltip&quot;:&quot;MyLab Math Global Support&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:48,&quot;NameResourceKey&quot;:&quot;BrowserCheck&quot;,&quot;TooltipResourceKey&quot;:&quot;BrowserCheck&quot;,&quot;Name&quot;:&quot;Browser Check&quot;,&quot;Url&quot;:&quot;Javascript:goWizardCourseNG(68825,true,'en-IE','None');&quot;,&quot;Tooltip&quot;:&quot;Browser Check&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:&quot;images/icon_helptop_school.gif&quot;,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:55,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:&quot;05/11/2022 10:05:18&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;05/11/2022 10:05:18&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;LeftNavigation&quot;:[{&quot;Key&quot;:41,&quot;NameResourceKey&quot;:&quot;Master.Student&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;STUDENT&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:true,&quot;IsSeparator&quot;:false,&quot;Order&quot;:2,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:2,&quot;NameResourceKey&quot;:&quot;Master.CourseHome&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.CourseHome&quot;,&quot;Name&quot;:&quot;Course Home&quot;,&quot;Url&quot;:&quot;/Student/MyDashboard.aspx&quot;,&quot;Tooltip&quot;:&quot;Course Home&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:3,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:12,&quot;NameResourceKey&quot;:&quot;Master.StudyPlan&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudyPlan&quot;,&quot;Name&quot;:&quot;Study Plan&quot;,&quot;Url&quot;:&quot;javascript:goStudyPlanHomepage();&quot;,&quot;Tooltip&quot;:&quot;Study Plan&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:5,&quot;NameResourceKey&quot;:&quot;Master.Calendar&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Calendar&quot;,&quot;Name&quot;:&quot;Calendar&quot;,&quot;Url&quot;:&quot;/Student/Calendar.aspx&quot;,&quot;Tooltip&quot;:&quot;Calendar&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:10,&quot;NameResourceKey&quot;:&quot;Master.HomeworkTests&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomeworkTests&quot;,&quot;Name&quot;:&quot;Homework and Tests&quot;,&quot;Url&quot;:&quot;/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1&quot;,&quot;Tooltip&quot;:&quot;Homework and Tests&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:11,&quot;NameResourceKey&quot;:&quot;Master.Results&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Results&quot;,&quot;Name&quot;:&quot;Results&quot;,&quot;Url&quot;:&quot;/Student/Results.aspx&quot;,&quot;Tooltip&quot;:&quot;Results&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:15,&quot;NameResourceKey&quot;:&quot;Master.StudentResources&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudentResources&quot;,&quot;Name&quot;:&quot;Student Resources&quot;,&quot;Url&quot;:&quot;/Student/Resources.aspx?resourceId=Student Resources&quot;,&quot;Tooltip&quot;:&quot;Student Resources&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:58,&quot;NameResourceKey&quot;:&quot;&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;&quot;,&quot;Url&quot;:&quot;#empty&quot;,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:42,&quot;NameResourceKey&quot;:&quot;Master.Instructor&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;INSTRUCTOR&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:true,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:60,&quot;NameResourceKey&quot;:&quot;Master.InstructorHome&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.InstructorHome&quot;,&quot;Name&quot;:&quot;Instructor Home&quot;,&quot;Url&quot;:&quot;/Instructor/InstructorHome.aspx&quot;,&quot;Tooltip&quot;:&quot;Instructor Home&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:20,&quot;NameResourceKey&quot;:&quot;Master.CourseManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.CourseManager&quot;,&quot;Name&quot;:&quot;Course Manager&quot;,&quot;Url&quot;:&quot;/Instructor/CourseManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Course Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:24,&quot;NameResourceKey&quot;:&quot;Master.HomePageManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomePageManager&quot;,&quot;Name&quot;:&quot;Home Page Manager&quot;,&quot;Url&quot;:&quot;/Instructor/HomePageManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Home Page Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:27,&quot;NameResourceKey&quot;:&quot;Master.HomeworkTestManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomeworkTestManager&quot;,&quot;Name&quot;:&quot;Assignment Manager&quot;,&quot;Url&quot;:&quot;/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1&quot;,&quot;Tooltip&quot;:&quot;Assignment Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:&quot;Master.HomeworkTestManagerAltText&quot;,&quot;AlternateText&quot;:null},{&quot;Key&quot;:32,&quot;NameResourceKey&quot;:&quot;Master.StudyPlanManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudyPlanManager&quot;,&quot;Name&quot;:&quot;Study Plan Manager&quot;,&quot;Url&quot;:&quot;/Instructor/StudyPlanManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Study Plan Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:35,&quot;NameResourceKey&quot;:&quot;Master.Gradebook&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Gradebook&quot;,&quot;Name&quot;:&quot;Gradebook&quot;,&quot;Url&quot;:&quot;/Instructor/Gradebook.aspx&quot;,&quot;Tooltip&quot;:&quot;Gradebook&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:36,&quot;NameResourceKey&quot;:&quot;Master.InstructorResources&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.InstructorResources&quot;,&quot;Name&quot;:&quot;Instructor Resources&quot;,&quot;Url&quot;:&quot;/Student/Resources.aspx?resourceId=Instructor Resources&quot;,&quot;Tooltip&quot;:&quot;Instructor Resources&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;ProductHeaderInfo&quot;:{&quot;Key&quot;:56,&quot;NameResourceKey&quot;:&quot;&quot;,&quot;TooltipResourceKey&quot;:&quot;&quot;,&quot;Name&quot;:&quot;MyLab Math Global&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:&quot;/support/customheader/product_global/newtoplogo.svg&quot;,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}};

            if (window.addEventListener) {
                window.addEventListener(&quot;load&quot;, function() {
                    setTimeout(function() {
                        //Hide the address bar.
                        window.scrollTo(0, 1);
                    }, 0);
                });
            } 

            // On dropdown open
            $(document).on('shown.bs.dropdown', function(event) {
                var dropdown = $(event.target);
		        
                // Set aria-expanded to true
                dropdown.find('.dropdown-toggle').attr('aria-expanded', true);
                    
                // Set focus on the first link in the dropdown
                //setTimeout(function() {
                //    dropdown.find('.dropdown-menu li:first-child a').focus(); //focus to the first child in dropdown
                //}, 10);
            });

            // On dropdown close
            $(document).on('hidden.bs.dropdown', function(event) {
                var dropdown = $(event.target);
		       
                // Set aria-expanded to false        
                dropdown.find('.dropdown-toggle').attr('aria-expanded', false);
    
                // Set focus back to dropdown toggle
                dropdown.find('.dropdown-toggle').focus();
            }); 

        

	
	
	initLeftNav(); 

    

    
    

        $(&quot;.dropdown.course-selector&quot;).on(&quot;shown.bs.dropdown&quot;, function() {
            $(this).find(&quot;.dropdown-menu li.active a&quot;).focus()
        });

        $(&quot;.dropdown.course-selector .dropdown-menu li a&quot;).click(function () {
            if($(this).attr('id') != 'manageCourses')
            {
                var cID = $(this).data('cid');
                OnCourseChanged(cID);
            }
        });

        //modified the CourseChanged function according to new course dropdown functionality
        function OnCourseChanged(selectedCID) {

            var loc = 'ChangeCourse.aspx'
            //JS: this param needs to be removed.
            //It is passed into the assignment manager in the Qstring
            //if we don't remove it, it keeps getting passed along.
            var tmplocation=location.pathname;
            tmplocation=removeParam(tmplocation,&quot;scId&quot;);
        
            // CA same for experiment assignment id, which generates a popup after the user creates an experiment assignment and clicks on the appropriate link
            tmplocation = removeParam(tmplocation, &quot;experimentAssignmentId&quot;);

            // Remove contentAreaID in case it's not valid in the new course
            tmplocation = removeParam(tmplocation, &quot;ContentAreaID&quot;);

            // XL-3896 - Targeted UI Changes: Remove isForceAssignmentManager because it is not valid in the new course
            tmplocation = removeParam(tmplocation, &quot;isForceAssignmentManager&quot;);
        
            loc = loc + '?courseId='+selectedCID+'&amp;target='+escape(tmplocation);
        
            //This is to fix the JS &quot;Unspecificed Error&quot; that occurs when the form tracking is on
            //and the user changes the course and clicks cancel on the prompt.
            //http://aspnet.4guysfromrolla.com/articles/042005-1.aspx
            try
            {
                
                location.href = loc;
            }
            catch (err)
            {
                console.log(&quot;catch&quot;);
                console.log(err);
            }
        }

    


   
	


//&lt;![CDATA[
AdminActionMemberWarning = &quot;The action you have taken will be applied to all the members in your group.  Are you sure you want to proceed?&quot;; Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function(){
                            if (window.addEventListener) { 
                                window.addEventListener('load',_imgButtonInit,true);
                              } else if (window.attachEvent){
                                window.attachEvent('onload',_imgButtonInit);
                              }
                        });//]]>

addImageButton(&quot;btnGo&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;ignore&quot;,false,&quot;GoGroup&quot;,&quot;__GoGroup_SelectedName&quot;,false,false,true,false);
addImageButton(&quot;imgBtnBack&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;ignore&quot;,false,&quot;&quot;,&quot;___SelectedName&quot;,false,false,false,false);

(function(){var a=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(5);window.peTrackerObject=PETracker.init(a.trackerId,a.config)})();(function(){if(window.peTrackerObject&amp;&amp;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(7)){google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(8)&amp;&amp;window.peTrackerObject.setProperty(&quot;loginSessionId&quot;,&quot;hncfii2gclba5im025uflpew&quot;);var b=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(41);if(b){switch(&quot;usertelemetry&quot;.toLocaleLowerCase()){case &quot;usertelemetry&quot;:window.PETPageId||(window.peTrackerObject.generatePageId(),b.payload.pageVisitId=window.PETPageId);b.payload.gaClientId=b.payload.gaClientId||window.PETPageId;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(42)!==window.PETPageId&amp;&amp;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(43)&amp;&amp;window.peTrackerObject.setProperty(&quot;gaClientId&quot;,
&quot;undefined&quot;);window.peTrackerObject.setProperty(&quot;personId&quot;,&quot;744891&quot;);var c=JSON.parse(localStorage.getItem(&quot;absdk-geolocation&quot;)||&quot;{}&quot;);c.sessionId!==google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(44)&amp;&amp;window.peTrackerObject.getMyGeo(function(d,a){d||window.dataLayer.push({event:&quot;usersessionenvironmentcontext&quot;,ipV4:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(45)||a.ipv4,ipV6:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(46)||a.ipv6,countryCode:a.geo.country_code,geoNetworkContinentCode:a.geo.continent,geoNetworkSubContinentCode:&quot;NA&quot;,geoNetworkCountryCode:a.geo.country,
geoNetworkRegionCode:a.geo.subdivision,geoNetworkCityName:a.geo.city,geoNetworkLatitude:(a.geo.latitude||0).toString(),geoNetworkLongitude:(a.geo.longitude||0).toString()})});c={messageTypeCode:&quot;UserTelemetryPageActivity&quot;,originatingSystemCode:&quot;xl&quot;,namespaceCode:&quot;Telemetry&quot;,messageVersion:&quot;1.1.0&quot;,personId:&quot;744891&quot;,environmentCode:&quot;PRD&quot;,loginSessionId:&quot;hncfii2gclba5im025uflpew&quot;,gaClientId:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(47)||window.PETPageId};window.peTrackerObject.initTelemetry(c)}window.peTrackerObject.sendActivity(b.payload,
b.config)}}})();(function(){if(window.peTrackerObject){var peGtmBuiltInEvent=&quot;usertelemetry&quot;;var timestamp=new Date;var transactionLocalDt=null;if(timestamp.toString().indexOf(&quot;GMT&quot;)>-1)transactionLocalDt=(new Date(timestamp.toString().split(&quot;GMT&quot;)[0]+&quot; UTC&quot;)).toISOString().split(&quot;Z&quot;)[0];else transactionLocalDt=timestamp.getFullYear()+&quot;-&quot;+(&quot;00&quot;+(timestamp.getMonth()+1)).slice(-2)+&quot;-&quot;+(&quot;00&quot;+timestamp.getDate()).slice(-2)+&quot;T&quot;+(&quot;00&quot;+timestamp.getHours()).slice(-2)+&quot;:&quot;+(&quot;00&quot;+timestamp.getMinutes()).slice(-2)+
&quot;:&quot;+(&quot;00&quot;+timestamp.getSeconds()).slice(-2)+&quot;:&quot;+(&quot;000&quot;+timestamp.getMilliseconds()).slice(-3);switch(peGtmBuiltInEvent){case &quot;gtm.linkClick&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(69)||&quot;Manage Offline Items&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(110)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;gtm.click&quot;:var clicktype=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(112);var tagtype=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(114);
if(clicktype)switch(clicktype.toLocaleLowerCase()){case &quot;checkbox&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:&quot;gradebook&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(165)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;radio&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:&quot;gradebook&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(216)||&quot;NA&quot;,
eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;button&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(238)||google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(242),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(253)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break}else if(tagtype){tagtype=tagtype.toLocaleLowerCase();if(tagtype===&quot;img&quot;)window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,
{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(275),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(286)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});else if(tagtype===&quot;i&quot;)if(&quot;undefined&quot;.indexOf(&quot;fa&quot;)>-1)window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(308),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(319)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()})}break}}})();
   


(function(){document.querySelectorAll(&quot;select&quot;);var c=document.querySelectorAll(&quot;select&quot;).length,b=function(a){&quot;select&quot;==a.target.tagName.toLowerCase()&amp;&amp;(a=a.target.options[a.target.selectedIndex],window.dataLayer.push({event:&quot;selectionMade&quot;,selectedElement:a}))};document.addEventListener(&quot;change&quot;,b,!0);for(b=0;b&lt;=c;b++);})();(function(){function b(){var a=new c;a.addEventListener(&quot;readystatechange&quot;,function(d){},!0);return a}var c=window.XMLHttpRequest;window.XMLHttpRequest=b})();/html[1]</value>
      <webElementGuid>a3a033cd-03d3-4246-a01b-6137b74ee186</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]</value>
      <webElementGuid>4846a2e0-d67a-41c5-bf8f-b4c2402eb4c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='']/parent::*</value>
      <webElementGuid>25c2eade-dce2-41c8-96d1-1af02755100b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//html</value>
      <webElementGuid>a83676c8-73de-470e-87f2-83f031a31e15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//html[(text() = concat(&quot;
	Manage Offline Items - Prabhashi ins1

         
             var gtmaccount = &quot;GTM-5JX93LK&quot;;
             var gtmpreview = &quot;env-2&quot;;
             var gtmauth = &quot;SVJdwwG3ki5lLhJ_zIzvuA&quot;;
             var dataLayer = [];
        
  
        
    

        
       
       
    
    
        TABLE.grid TD 
            { 
              vertical-align:middle;
              height: 34px;

            }
    
	
	    function GoBack()
	    {
	        deltaBack();
	        return false;
	    }

	    function DoAction(cbxActionID, assignmentId, assignmentType)
	    {   
	        var cbxAction=document.getElementById(cbxActionID);
	        var selAction= cbxAction.options[cbxAction.selectedIndex].value;
	        if (selAction==&quot;Change Scores&quot;)
	        {
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=step3&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Edit Item Info&quot;)
	        {
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=step2&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Copy Offline&quot;)
	        {
	            //location=&quot;CopyOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;externalId=&quot; + assignmentId + &quot;&amp;back=ManageOfflineItems.aspx&quot;;
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=copy&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Delete Offline&quot;)
	        {
	            location=&quot;AssignmentsDelete.aspx?back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else
	        {
	            // debug
	            //location=&quot;OfflineItemSummary.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;back=ManageOfflineItems.aspx&quot;;
	        }
	        return false;
	    }
		
		
	    /*=======================================
         * INIT
         *=======================================
         */
	    // Chain to body onload
	    if (document.getElementById &amp;&amp; document.createElement) {    
	        if (window.addEventListener) {
	            window.addEventListener(&quot;load&quot;,Init,true);
	        } else if (window.attachEvent){
	            window.attachEvent(&quot;onload&quot;,Init);
	        }
	    }

	    function Init()
	    {
	        // handle omitted assignments
	        var tbl = document.getElementById(&quot;GridAssignments&quot;);
	        if (tbl) 
	        {
	            var rows = tbl.getElementsByTagName(&quot;tr&quot;);
	            for (var i = 0; i &lt; rows.length; i++)
	                if (rows[i].getAttribute(&quot;omit&quot;) == &quot;1&quot;)
	                    rows[i].className += &quot; omit&quot;;
	        }
	    }
	





(window.BOOMR_mq=window.BOOMR_mq||[]).push([&quot;addVar&quot;,{&quot;rua.upush&quot;:&quot;false&quot;,&quot;rua.cpush&quot;:&quot;false&quot;,&quot;rua.upre&quot;:&quot;false&quot;,&quot;rua.cpre&quot;:&quot;true&quot;,&quot;rua.uprl&quot;:&quot;false&quot;,&quot;rua.cprl&quot;:&quot;false&quot;,&quot;rua.cprf&quot;:&quot;false&quot;,&quot;rua.trans&quot;:&quot;SJ-8413d74b-58f9-49bf-a874-5e8706e383f8&quot;,&quot;rua.cook&quot;:&quot;true&quot;,&quot;rua.ims&quot;:&quot;false&quot;,&quot;rua.ufprl&quot;:&quot;false&quot;,&quot;rua.cfprl&quot;:&quot;true&quot;,&quot;rua.isuxp&quot;:&quot;false&quot;,&quot;rua.texp&quot;:&quot;norulematch&quot;}]);
                              !function(a){var e=&quot;https://s.go-mpulse.net/boomerang/&quot;,t=&quot;addEventListener&quot;;if(&quot;False&quot;==&quot;True&quot;)a.BOOMR_config=a.BOOMR_config||{},a.BOOMR_config.PageParams=a.BOOMR_config.PageParams||{},a.BOOMR_config.PageParams.pci=!0,e=&quot;https://s2.go-mpulse.net/boomerang/&quot;;if(window.BOOMR_API_key=&quot;B7LE9-GF2JD-PPN44-7ULXU-DPSH4&quot;,function(){function n(e){a.BOOMR_onload=e&amp;&amp;e.timeStamp||(new Date).getTime()}if(!a.BOOMR||!a.BOOMR.version&amp;&amp;!a.BOOMR.snippetExecuted){a.BOOMR=a.BOOMR||{},a.BOOMR.snippetExecuted=!0;var i,_,o,r=document.createElement(&quot;iframe&quot;);if(a[t])a[t](&quot;load&quot;,n,!1);else if(a.attachEvent)a.attachEvent(&quot;onload&quot;,n);r.src=&quot;javascript:void(0)&quot;,r.title=&quot;&quot;,r.role=&quot;presentation&quot;,(r.frameElement||r).style.cssText=&quot;width:0;height:0;border:0;display:none;&quot;,o=document.getElementsByTagName(&quot;script&quot;)[0],o.parentNode.insertBefore(r,o);try{_=r.contentWindow.document}catch(O){i=document.domain,r.src=&quot;javascript:var d=document.open();d.domain=&quot; , &quot;'&quot; , &quot;&quot;+i+&quot;&quot; , &quot;'&quot; , &quot;;void(0);&quot;,_=r.contentWindow.document}_.open()._l=function(){var a=this.createElement(&quot;script&quot;);if(i)this.domain=i;a.id=&quot;boomr-if-as&quot;,a.src=e+&quot;B7LE9-GF2JD-PPN44-7ULXU-DPSH4&quot;,BOOMR_lstart=(new Date).getTime(),this.body.appendChild(a)},_.write(&quot;&lt;bo&quot;+&quot; , &quot;'&quot; , &quot;dy onload=&quot;document._l();&quot;>&quot; , &quot;'&quot; , &quot;),_.close()}}(),&quot;&quot;.length>0)if(a&amp;&amp;&quot;performance&quot;in a&amp;&amp;a.performance&amp;&amp;&quot;function&quot;==typeof a.performance.setResourceTimingBufferSize)a.performance.setResourceTimingBufferSize();!function(){if(BOOMR=a.BOOMR||{},BOOMR.plugins=BOOMR.plugins||{},!BOOMR.plugins.AK){var e=&quot;true&quot;==&quot;true&quot;?1:0,t=&quot;cookiepresent&quot;,n=&quot;2ruo3vyxggvteyt3huda-f-288e55f37-clientnsv4-s.akamaihd.net&quot;,i=&quot;false&quot;==&quot;true&quot;?2:1,_={&quot;ak.v&quot;:&quot;32&quot;,&quot;ak.cp&quot;:&quot;1248014&quot;,&quot;ak.ai&quot;:parseInt(&quot;749947&quot;,10),&quot;ak.ol&quot;:&quot;0&quot;,&quot;ak.cr&quot;:94,&quot;ak.ipv&quot;:4,&quot;ak.proto&quot;:&quot;h2&quot;,&quot;ak.rid&quot;:&quot;8f02d43&quot;,&quot;ak.r&quot;:31130,&quot;ak.a2&quot;:e,&quot;ak.m&quot;:&quot;x&quot;,&quot;ak.n&quot;:&quot;essl&quot;,&quot;ak.bpcip&quot;:&quot;212.104.237.0&quot;,&quot;ak.cport&quot;:15367,&quot;ak.gh&quot;:&quot;104.75.84.58&quot;,&quot;ak.quicv&quot;:&quot;&quot;,&quot;ak.tlsv&quot;:&quot;tls1.3&quot;,&quot;ak.0rtt&quot;:&quot;&quot;,&quot;ak.csrc&quot;:&quot;-&quot;,&quot;ak.acc&quot;:&quot;&quot;,&quot;ak.t&quot;:&quot;1652243718&quot;,&quot;ak.ak&quot;:&quot;hOBiQwZUYzCg5VSAfCLimQ==JSvUV7lR8ws3FlLtfHMNEIMeD8M/xaKWPgwPMc4aHnCeUfTacVoU0h5uBEV4DDsMQk+LoUk8yryrbgVaJ9WTaiRZXBdlzLJyVxOxqbc1CoNKs1kZ3EjHXj+FW8QW5yvmeHwUThlJqZf+iFTTyHFgzHFaszcdwkgOInHHPYDNoD8+zDLQITmHSopGC74rvFGr5htxI3hiMTKdd+yxtHvtSKDWbZwh2fmB4Lzvu4tA0Svxs1vMYgHEaCjm0k39aNhU7ZXB+TehpGt7PE8lxDuV1pZScVI19HwEnr8JO5JQ4gK+sYddwIILiDaksAOKJGK4pE+FVReKYABRKVoW8lLUqYLJl04BahTxGjB15UuFNsw7Lc986bS/t692/aUVHO2H3rhlzarDxnkGIzLfsFVUKGFdDJHbHFeHGgNaIC28zqs=&quot;,&quot;ak.pv&quot;:&quot;1&quot;,&quot;ak.dpoabenc&quot;:&quot;&quot;,&quot;ak.tf&quot;:i};if(&quot;&quot;!==t)_[&quot;ak.ruds&quot;]=t;var o={i:!1,av:function(e){var t=&quot;http.initiator&quot;;if(e&amp;&amp;(!e[t]||&quot;spa_hard&quot;===e[t]))_[&quot;ak.feo&quot;]=void 0!==a.aFeoApplied?1:0,BOOMR.addVar(_)},rv:function(){var a=[&quot;ak.bpcip&quot;,&quot;ak.cport&quot;,&quot;ak.cr&quot;,&quot;ak.csrc&quot;,&quot;ak.gh&quot;,&quot;ak.ipv&quot;,&quot;ak.m&quot;,&quot;ak.n&quot;,&quot;ak.ol&quot;,&quot;ak.proto&quot;,&quot;ak.quicv&quot;,&quot;ak.tlsv&quot;,&quot;ak.0rtt&quot;,&quot;ak.r&quot;,&quot;ak.acc&quot;,&quot;ak.t&quot;,&quot;ak.tf&quot;];BOOMR.removeVar(a)}};BOOMR.plugins.AK={akVars:_,akDNSPreFetchDomain:n,init:function(){if(!o.i){var a=BOOMR.subscribe;a(&quot;before_beacon&quot;,o.av,null,null),a(&quot;onbeacon&quot;,o.rv,null,null),o.i=!0}return this},is_complete:function(){return!0}}}}()}(window);#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}

    
        &lt;iframe src=&quot;https://www.googletagmanager.com/ns.html?id=GTM-5JX93LK&amp;gtm_auth=SVJdwwG3ki5lLhJ_zIzvuA&amp;gtm_preview=env-2&amp;gtm_cookies_win=x&quot;
        height=&quot;0&quot; width=&quot;0&quot; style=&quot;display:none;visibility:hidden&quot;>&lt;/iframe>
    
	
    Skip Navigation

	








//&lt;![CDATA[
var theForm = document.forms[&quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>







//&lt;![CDATA[
var BaseUrl=&quot;../&quot;;
var BaseAbsUrl=&quot;https://mylabglobal.pearson.com/&quot;;
var BackUrl=&quot;gradebook.aspx&quot;;
var LocDateParts= { &quot;ttam&quot;: &quot;AM&quot;, &quot;ttpm&quot;: &quot;PM&quot;};
var LocClockPattern=&quot;dd/MM/yy HH:mm&quot;;
var StrUtils_DecimalSeparator=&quot;.&quot;;
LOCNS.LocMap.RegRes(&quot;common/formtracker.js&quot;,&quot;LeaveAlert&quot;,&quot;Your changes will not be saved.&quot;);
LOCNS.LocMap.RegRes(&quot;common/formtracker.js&quot;,&quot;LeaveConfirm&quot;,&quot;Are you sure you want to navigate away from this page?\n\n\n\n{0}\n\n\n\nPress OK to continue, or Cancel to stay on the current page.&quot;);
LOCNS.LocMap.RegRes(&quot;common/popups_1.js&quot;,&quot;PopupBlockedWarning&quot;,&quot;Your browser is blocking pop-ups, and they need to be allowed before working in your course. \nPlease disable your pop-up blocker now.&quot;);
LOCNS.LocMap.RegRes(&quot;resources&quot;,&quot;Cancel&quot;,&quot;Cancel&quot;);
LOCNS.LocMap.RegRes(&quot;resources&quot;,&quot;OK&quot;,&quot;OK&quot;);
//]]>







	

		
	
	

        
        
        
            
        
           
                         
                             
			

				
					
						
						Hide Navigation
					
				

                 
                        GlobalTestCourse
                

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnrol in CourseEdit AccountLog Out|HelpHelpLegendTours &amp; trainingMyLab Math Global SupportBrowser Check|11/05/22 10:05
				

			
		
        
        

		    
			    
			    
			    
		    

		    STUDENTCourse HomeStudy PlanCalendarHomework and TestsResultsStudent ResourcesINSTRUCTORInstructor HomeCourse ManagerHome Page ManagerAssignment ManagerStudy Plan ManagerGradebookInstructor Resources

	    
                             
        
		        
		            
                    
                        
                            
                                                     
                                
                                
	
                                    
                                        
                                        
                                        


    
        
            GlobalTestCourse [1]
            
        
        
          
              
              Manage Course List
          
          
          06/05_Downtime_GlobalPreBvt [0]Coord: GlobalCoordTestCourse [0]GlobalTestCourse [1]SampleCourse [0]
        
    




     
    

|GRADEBOOK
                                    
                                    
                                


                                
                                
                                
                                 
                                      
                                                
                                                
                                                    
                                                        Prabhashi ins1
                                                        GlobalTestCourse
                                                    
                                                    
                                                        Prabhashi ins1
                                                        MML Global Test Book
                                                    
                                                    
                                                        11/05/22
                                                        10:05
                                                    
                                                
                                                
                                                
                                    
                            
                        
                    
					
							
							    
    
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>



    


       
           
        
            
        
        
           
            
            Manage Offline Items   
           
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Manage Offline Items
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    



    
             
	

    
    
	
		
			
				Ch. ItemCategoryDateActions 
			
		
			
				
                    1‑4
                
                    
                
                    OfflineTest
                
                    Test
                
                    11/05/22
                
                        
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				
                
                    Go
				
                
			
				
                    1‑4
                
                    
                
                    OfflineQuiz
                
                    Quiz
                
                    11/05/22
                
                        
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				
                
                    Go
				
                
			
		
	




    
    Done

    
    


                                 
        			 
                    
                        
                                
                                   
                                        This course is based on MML Global Test BookTerms of Use|Privacy Policy|Copyright 2022 Pearson  All Rights Reserved.
                                              
                                
                            
                                    

                            
                                               
                        
                    
                     
                
	    
                       
                        
         

        
        
	


        
	    [+]
	

 
       
	

	

	

       
        
            var gaaccount = &quot;UA-48773414-1&quot;;
            var environment = &quot;global&quot;;
    
	
	
	
		

		

		

	
			
        
            var initialjsclockServerDateTime = &quot;Wed, 11 May 2022 10:05:18&quot;;
            function showLoading(fromTimer)
            {
                var el = document.getElementById(&quot;loading&quot;);
                // don&quot; , &quot;'&quot; , &quot;t show this if it&quot; , &quot;'&quot; , &quot;s already been hidden

                if(AllowShowPleaseWait &amp;&amp; el)
                {
                    el.style.display=&quot;&quot;;  
                }
            }
        
            function CourseChanged(select, originalSelectedValue) {
                //var courseId = item.Value;
                var courseId = select.options[select.selectedIndex].value;
                var loc = &quot; , &quot;'&quot; , &quot;ChangeCourse.aspx&quot; , &quot;'&quot; , &quot;
                //JS: this param needs to be removed.
                //It is passed into the assignment manager in the Qstring
                //if we don&quot; , &quot;'&quot; , &quot;t remove it, it keeps getting passed along.
                var tmplocation=location.pathname;
                tmplocation=removeParam(tmplocation,&quot;scId&quot;);
        
                // CA same for experiment assignment id, which generates a popup after the user creates an experiment assignment and clicks on the appropriate link
                tmplocation = removeParam(tmplocation, &quot;experimentAssignmentId&quot;);

                // Remove contentAreaID in case it&quot; , &quot;'&quot; , &quot;s not valid in the new course
                tmplocation = removeParam(tmplocation, &quot;ContentAreaID&quot;);

                // XL-3896 - Targeted UI Changes: Remove isForceAssignmentManager because it is not valid in the new course
                tmplocation = removeParam(tmplocation, &quot;isForceAssignmentManager&quot;);
        
                loc = loc + &quot; , &quot;'&quot; , &quot;?courseId=&quot; , &quot;'&quot; , &quot;+courseId+&quot; , &quot;'&quot; , &quot;&amp;target=&quot; , &quot;'&quot; , &quot;+escape(tmplocation);
        
                //This is to fix the JS &quot;Unspecificed Error&quot; that occurs when the form tracking is on
                //and the user changes the course and clicks cancel on the prompt.
                //http://aspnet.4guysfromrolla.com/articles/042005-1.aspx
                try
                {
                    location = loc;
                }
                catch (err)
                {
                    //set it back to original.  They hit cancel.
                    select.selectedIndex=originalSelectedValue;
                }
            }
      
            function AlertChooseBookMyCourses(newlocation)
            {
                alert(&quot;The course or book you were just viewing is not in your course list.  Please choose the course you would like to work in from the My Courses page.&quot;);
                if (newlocation)
                    location = newlocation; // Comment 9610
                else
                    location.reload(); // comment 9402
            }
      
            function AlertChooseBook()
            {
                alert(&quot;The course or book you were just viewing is not in your course list.  Please choose the course you would like to work in from the course drop down&quot;);
                location.reload(); // comment 7928
            }
      
            function AlertCourseChanged()
            {
                alert(&quot;The course or book you were just viewing as a student is not in your course list.  Choose the course you want to work in from the course drop down.&quot;);
                location.reload(); // comment 7928
            }
      
            function AlertCourseChangedEmptyCurrent()
            {
                alert(&quot;You have chosen not to show any courses in this drop down menu. To add courses to this list, go to Manage Course List and flag the courses as current courses.&quot;);
                location.reload(); // requested in C07-12
            }
      
            var navVisible = true;
            wizardUrl = &quot;../BrowserCheck/BrowserCheck.aspx&quot;;
            supportLink = &quot;&quot;;
            var navUrl = &quot;../Service/LeftNavStateUpdate.aspx&quot;

            

            function initLeftNav()
            {
                LeftNavStateUpdate.init(navUrl);  
            }

            function onNavCallback()
            {
                var navDiv = document.getElementById(&quot; , &quot;'&quot; , &quot;leftnavcontainer&quot; , &quot;'&quot; , &quot;);
                var navShowDiv = document.getElementById(&quot; , &quot;'&quot; , &quot;leftnavshow&quot; , &quot;'&quot; , &quot;);  
  
                if (navDiv.style.display==&quot;none&quot;) 
                {
                    navDiv.style.display = &quot;block&quot;;
                    navShowDiv.style.display = &quot;none&quot;;
                    navVisible = true;
                }
                else
                {
                    navDiv.style.display = &quot;none&quot;;
                    navShowDiv.style.display = &quot;block&quot;;          
                    navVisible = false;
                }
            }


            function toggleNav_OLD()
            {
                var navDiv = document.getElementById(&quot; , &quot;'&quot; , &quot;leftnavcontainer&quot; , &quot;'&quot; , &quot;);
  
                var navCurrentlyVisible = (navDiv.style.display != &quot;none&quot;);

                LeftNavStateUpdate.doNavCallback(!navCurrentlyVisible, onNavCallback);   
            }

            function goMyCourses() 
            {
                location = &quot;MyCourses.aspx&quot;;
                return false;
            }

            function goCourseHome()
            {
                var courseHomeUrl = &quot;&quot;;

                if (courseHomeUrl != &quot;&quot;)
                    LeftNavStateUpdate.doNavCallback(false, null, courseHomeUrl);    
            }

            function goStudyPlan()
            {
                location = &quot;../Student/StudyPlanRecommendations.aspx&quot;;
            }

            function goStudyPlanHomepage() 
            {
                location = &quot;../Student/StudyPlanRecommendations.aspx&quot;;
            }

            function setupClock()
            {
                jsclockServerDateTime = &quot;Wed, 11 May 2022 10:05:18&quot;;
                jsclockElementId = &quot;toptime&quot;;
                jsclockConstantUpdate = true;
                isSelfStudyCourse = &quot;False&quot;;
                
  
                // check if it&quot; , &quot;'&quot; , &quot;s there just in case we&quot; , &quot;'&quot; , &quot;re not on IntelliPage
                if (typeof(initClock)==&quot;function&quot;)
                    initClock();
            }

            function ShowStudentCalendar()
            {
                var loc = &quot; , &quot;'&quot; , &quot;../Student/MyDashboard.aspx&quot; , &quot;'&quot; , &quot;
                location=loc;
            }

            var myNavParams = myNavParams || {
                productId: 44,
                userId: 744891,
                userName: &quot;Prabhashi ins1&quot;,
                authSystemId: 1,
                courseId: 68825,
                bookId: 990213,
                backdoor: &quot; , &quot;'&quot; , &quot;False&quot; , &quot;'&quot; , &quot;,
                nextgen: &quot; , &quot;'&quot; , &quot;True&quot; , &quot;'&quot; , &quot;,
                lockdownbrowsertype: &quot; , &quot;'&quot; , &quot;None&quot; , &quot;'&quot; , &quot;,
                currentCulture: &quot;en-IE&quot;,
                showBanner: true,
                showLeftNavigation: true,
                leftNavState: &quot; , &quot;'&quot; , &quot;False&quot; , &quot;'&quot; , &quot;,
                navigationVersion: &quot; , &quot;'&quot; , &quot;v1&quot; , &quot;'&quot; , &quot;,
                userRole: &quot; , &quot;'&quot; , &quot;Instructor&quot; , &quot;'&quot; , &quot;,
                hasCustomBanner : true,
                pageLinkMode : &quot;Default&quot;,
                isCC : false,
                showPlayerHeader : false,
                showBrowserCheck: true,
                IsSelfStudy: false,
                hideNavigationText: &quot;Hide Navigation&quot;,
                showNavigationText: &quot;Show Navigation&quot;,
                institutionKeyId: &quot;&quot;,
                school: &quot;SMS Default Institution&quot;,
                authSystemPartnerId: &quot;&quot;,
                bookCode: &quot;hedmathstest&quot;,
                disciplineID: &quot;30&quot;,
                userOrigin: &quot; , &quot;'&quot; , &quot;MathXL&quot; , &quot;'&quot; , &quot;,
                authsystemtypeId: &quot; , &quot;'&quot; , &quot;Sms&quot; , &quot;'&quot; , &quot;,
                supportNewNavigation: true,
                supportMobile: true,
                isMyFoundations: false,
                currentLocaleFormat: &quot; , &quot;'&quot; , &quot;MM/DD/YY&quot; , &quot;'&quot; , &quot;,
                IPv4: &quot; , &quot;'&quot; , &quot;212.104.237.215&quot; , &quot;'&quot; , &quot;,
                IPv6: &quot; , &quot;'&quot; , &quot;NA&quot; , &quot;'&quot; , &quot;,
                SessionID: &quot; , &quot;'&quot; , &quot;hncfii2gclba5im025uflpew&quot; , &quot;'&quot; , &quot;,
                ProductName: &quot; , &quot;'&quot; , &quot;MyLab Math Global&quot; , &quot;'&quot; , &quot;,
                ProductPlatformCode: &quot; , &quot;'&quot; , &quot;Standalone&quot; , &quot;'&quot; , &quot;,
                TransactionLocalDate: &quot; , &quot;'&quot; , &quot;2022-05-11T10:05:18.831&quot; , &quot;'&quot; , &quot;,
                IsTelemetryEnabled: &quot; , &quot;'&quot; , &quot;True&quot; , &quot;'&quot; , &quot;,
                NoNameScreenReaderText: &quot;&quot;
            };

            var siteheader = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            var sitenav = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            var printheader = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_printheader&quot; , &quot;'&quot; , &quot;;

        
        

    
    
    
    
        setupClock();
    
        
    
	
	

     
         var pnlBreadCrumbId = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_PnlBreadCrumb&quot; , &quot;'&quot; , &quot;;
         var pnlGBBreadCrumbId = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_PnlGradebookBreadCrumb&quot; , &quot;'&quot; , &quot;;
     
    

        var navigation_key;
        function setCurrentNavigationKey(navKey) {
            //window.localStorage[&quot; , &quot;'&quot; , &quot;lastNavKey&quot; , &quot;'&quot; , &quot;] = navKey;
            navigation_key = navKey;
        }

        setCurrentNavigationKey(35);
    
    
    
        $(document).ready(function () {
            $(&quot;#GridAssignments tbody&quot;).attr(&quot; , &quot;'&quot; , &quot;id&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;assignbody&quot; , &quot;'&quot; , &quot;);
        });

    
    

    
    
		
		    
            var currentMaster = &quot;default&quot;;
		
            var navigationData = {&quot;TopNavigation&quot;:[{&quot;Key&quot;:49,&quot;NameResourceKey&quot;:&quot;USERNAME&quot;,&quot;TooltipResourceKey&quot;:&quot;USERNAME&quot;,&quot;Name&quot;:&quot;prabhashi_ins1&quot;,&quot;Url&quot;:&quot;#&quot;,&quot;Tooltip&quot;:&quot;prabhashi_ins1&quot;,&quot;SubMenuData&quot;:[{&quot;Key&quot;:0,&quot;NameResourceKey&quot;:&quot;Master.MyCourses&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.MyCourses&quot;,&quot;Name&quot;:&quot;My Courses&quot;,&quot;Url&quot;:&quot;javascript:goMyCourses();&quot;,&quot;Tooltip&quot;:&quot;My Courses&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:1,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:52,&quot;NameResourceKey&quot;:&quot;Master.EnrollinCourse&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Enrollincourse&quot;,&quot;Name&quot;:&quot;Enrol in Course&quot;,&quot;Url&quot;:&quot;/student/enrollwizard.aspx&quot;,&quot;Tooltip&quot;:&quot;Enrol in Course&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:50,&quot;NameResourceKey&quot;:&quot;Master.EditAccount&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.EditAccount&quot;,&quot;Name&quot;:&quot;Edit Account&quot;,&quot;Url&quot;:&quot;javascript:DoHelp(&quot; , &quot;'&quot; , &quot;https://register.pearsoncmg.com/userprofile/up_login.jsp&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Edit Account&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:53,&quot;NameResourceKey&quot;:&quot;Master.Logout&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Logout&quot;,&quot;Name&quot;:&quot;Log Out&quot;,&quot;Url&quot;:&quot;/logout.aspx&quot;,&quot;Tooltip&quot;:&quot;Log Out&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:43,&quot;NameResourceKey&quot;:&quot;Master.Help&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Help&quot;,&quot;Name&quot;:&quot;Help&quot;,&quot;Url&quot;:&quot;&quot;,&quot;Tooltip&quot;:&quot;Help&quot;,&quot;SubMenuData&quot;:[{&quot;Key&quot;:44,&quot;NameResourceKey&quot;:&quot;Master.Help&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Help&quot;,&quot;Name&quot;:&quot;Help&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;toc_instructor_math&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Help&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:46,&quot;NameResourceKey&quot;:&quot;Master.Legend&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Legend&quot;,&quot;Name&quot;:&quot;Legend&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;legends&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Legend&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:59,&quot;NameResourceKey&quot;:&quot;Master.TakeaTourIntl&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.TakeaTourIntl&quot;,&quot;Name&quot;:&quot;Tours &amp; training&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;instr_tours&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Tours &amp; training&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:57,&quot;NameResourceKey&quot;:&quot;Master.ProductSupport&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.ProductSupport&quot;,&quot;Name&quot;:&quot;MyLab Math Global Support&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;instr_support&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;MyLab Math Global Support&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:48,&quot;NameResourceKey&quot;:&quot;BrowserCheck&quot;,&quot;TooltipResourceKey&quot;:&quot;BrowserCheck&quot;,&quot;Name&quot;:&quot;Browser Check&quot;,&quot;Url&quot;:&quot;Javascript:goWizardCourseNG(68825,true,&quot; , &quot;'&quot; , &quot;en-IE&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;None&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Browser Check&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:&quot;images/icon_helptop_school.gif&quot;,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:55,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:&quot;05/11/2022 10:05:18&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;05/11/2022 10:05:18&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;LeftNavigation&quot;:[{&quot;Key&quot;:41,&quot;NameResourceKey&quot;:&quot;Master.Student&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;STUDENT&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:true,&quot;IsSeparator&quot;:false,&quot;Order&quot;:2,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:2,&quot;NameResourceKey&quot;:&quot;Master.CourseHome&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.CourseHome&quot;,&quot;Name&quot;:&quot;Course Home&quot;,&quot;Url&quot;:&quot;/Student/MyDashboard.aspx&quot;,&quot;Tooltip&quot;:&quot;Course Home&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:3,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:12,&quot;NameResourceKey&quot;:&quot;Master.StudyPlan&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudyPlan&quot;,&quot;Name&quot;:&quot;Study Plan&quot;,&quot;Url&quot;:&quot;javascript:goStudyPlanHomepage();&quot;,&quot;Tooltip&quot;:&quot;Study Plan&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:5,&quot;NameResourceKey&quot;:&quot;Master.Calendar&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Calendar&quot;,&quot;Name&quot;:&quot;Calendar&quot;,&quot;Url&quot;:&quot;/Student/Calendar.aspx&quot;,&quot;Tooltip&quot;:&quot;Calendar&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:10,&quot;NameResourceKey&quot;:&quot;Master.HomeworkTests&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomeworkTests&quot;,&quot;Name&quot;:&quot;Homework and Tests&quot;,&quot;Url&quot;:&quot;/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1&quot;,&quot;Tooltip&quot;:&quot;Homework and Tests&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:11,&quot;NameResourceKey&quot;:&quot;Master.Results&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Results&quot;,&quot;Name&quot;:&quot;Results&quot;,&quot;Url&quot;:&quot;/Student/Results.aspx&quot;,&quot;Tooltip&quot;:&quot;Results&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:15,&quot;NameResourceKey&quot;:&quot;Master.StudentResources&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudentResources&quot;,&quot;Name&quot;:&quot;Student Resources&quot;,&quot;Url&quot;:&quot;/Student/Resources.aspx?resourceId=Student Resources&quot;,&quot;Tooltip&quot;:&quot;Student Resources&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:58,&quot;NameResourceKey&quot;:&quot;&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;&quot;,&quot;Url&quot;:&quot;#empty&quot;,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:42,&quot;NameResourceKey&quot;:&quot;Master.Instructor&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;INSTRUCTOR&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:true,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:60,&quot;NameResourceKey&quot;:&quot;Master.InstructorHome&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.InstructorHome&quot;,&quot;Name&quot;:&quot;Instructor Home&quot;,&quot;Url&quot;:&quot;/Instructor/InstructorHome.aspx&quot;,&quot;Tooltip&quot;:&quot;Instructor Home&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:20,&quot;NameResourceKey&quot;:&quot;Master.CourseManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.CourseManager&quot;,&quot;Name&quot;:&quot;Course Manager&quot;,&quot;Url&quot;:&quot;/Instructor/CourseManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Course Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:24,&quot;NameResourceKey&quot;:&quot;Master.HomePageManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomePageManager&quot;,&quot;Name&quot;:&quot;Home Page Manager&quot;,&quot;Url&quot;:&quot;/Instructor/HomePageManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Home Page Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:27,&quot;NameResourceKey&quot;:&quot;Master.HomeworkTestManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomeworkTestManager&quot;,&quot;Name&quot;:&quot;Assignment Manager&quot;,&quot;Url&quot;:&quot;/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1&quot;,&quot;Tooltip&quot;:&quot;Assignment Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:&quot;Master.HomeworkTestManagerAltText&quot;,&quot;AlternateText&quot;:null},{&quot;Key&quot;:32,&quot;NameResourceKey&quot;:&quot;Master.StudyPlanManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudyPlanManager&quot;,&quot;Name&quot;:&quot;Study Plan Manager&quot;,&quot;Url&quot;:&quot;/Instructor/StudyPlanManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Study Plan Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:35,&quot;NameResourceKey&quot;:&quot;Master.Gradebook&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Gradebook&quot;,&quot;Name&quot;:&quot;Gradebook&quot;,&quot;Url&quot;:&quot;/Instructor/Gradebook.aspx&quot;,&quot;Tooltip&quot;:&quot;Gradebook&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:36,&quot;NameResourceKey&quot;:&quot;Master.InstructorResources&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.InstructorResources&quot;,&quot;Name&quot;:&quot;Instructor Resources&quot;,&quot;Url&quot;:&quot;/Student/Resources.aspx?resourceId=Instructor Resources&quot;,&quot;Tooltip&quot;:&quot;Instructor Resources&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;ProductHeaderInfo&quot;:{&quot;Key&quot;:56,&quot;NameResourceKey&quot;:&quot;&quot;,&quot;TooltipResourceKey&quot;:&quot;&quot;,&quot;Name&quot;:&quot;MyLab Math Global&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:&quot;/support/customheader/product_global/newtoplogo.svg&quot;,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}};

            if (window.addEventListener) {
                window.addEventListener(&quot;load&quot;, function() {
                    setTimeout(function() {
                        //Hide the address bar.
                        window.scrollTo(0, 1);
                    }, 0);
                });
            } 

            // On dropdown open
            $(document).on(&quot; , &quot;'&quot; , &quot;shown.bs.dropdown&quot; , &quot;'&quot; , &quot;, function(event) {
                var dropdown = $(event.target);
		        
                // Set aria-expanded to true
                dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-toggle&quot; , &quot;'&quot; , &quot;).attr(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, true);
                    
                // Set focus on the first link in the dropdown
                //setTimeout(function() {
                //    dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-menu li:first-child a&quot; , &quot;'&quot; , &quot;).focus(); //focus to the first child in dropdown
                //}, 10);
            });

            // On dropdown close
            $(document).on(&quot; , &quot;'&quot; , &quot;hidden.bs.dropdown&quot; , &quot;'&quot; , &quot;, function(event) {
                var dropdown = $(event.target);
		       
                // Set aria-expanded to false        
                dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-toggle&quot; , &quot;'&quot; , &quot;).attr(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, false);
    
                // Set focus back to dropdown toggle
                dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-toggle&quot; , &quot;'&quot; , &quot;).focus();
            }); 

        

	
	
	initLeftNav(); 

    

    
    

        $(&quot;.dropdown.course-selector&quot;).on(&quot;shown.bs.dropdown&quot;, function() {
            $(this).find(&quot;.dropdown-menu li.active a&quot;).focus()
        });

        $(&quot;.dropdown.course-selector .dropdown-menu li a&quot;).click(function () {
            if($(this).attr(&quot; , &quot;'&quot; , &quot;id&quot; , &quot;'&quot; , &quot;) != &quot; , &quot;'&quot; , &quot;manageCourses&quot; , &quot;'&quot; , &quot;)
            {
                var cID = $(this).data(&quot; , &quot;'&quot; , &quot;cid&quot; , &quot;'&quot; , &quot;);
                OnCourseChanged(cID);
            }
        });

        //modified the CourseChanged function according to new course dropdown functionality
        function OnCourseChanged(selectedCID) {

            var loc = &quot; , &quot;'&quot; , &quot;ChangeCourse.aspx&quot; , &quot;'&quot; , &quot;
            //JS: this param needs to be removed.
            //It is passed into the assignment manager in the Qstring
            //if we don&quot; , &quot;'&quot; , &quot;t remove it, it keeps getting passed along.
            var tmplocation=location.pathname;
            tmplocation=removeParam(tmplocation,&quot;scId&quot;);
        
            // CA same for experiment assignment id, which generates a popup after the user creates an experiment assignment and clicks on the appropriate link
            tmplocation = removeParam(tmplocation, &quot;experimentAssignmentId&quot;);

            // Remove contentAreaID in case it&quot; , &quot;'&quot; , &quot;s not valid in the new course
            tmplocation = removeParam(tmplocation, &quot;ContentAreaID&quot;);

            // XL-3896 - Targeted UI Changes: Remove isForceAssignmentManager because it is not valid in the new course
            tmplocation = removeParam(tmplocation, &quot;isForceAssignmentManager&quot;);
        
            loc = loc + &quot; , &quot;'&quot; , &quot;?courseId=&quot; , &quot;'&quot; , &quot;+selectedCID+&quot; , &quot;'&quot; , &quot;&amp;target=&quot; , &quot;'&quot; , &quot;+escape(tmplocation);
        
            //This is to fix the JS &quot;Unspecificed Error&quot; that occurs when the form tracking is on
            //and the user changes the course and clicks cancel on the prompt.
            //http://aspnet.4guysfromrolla.com/articles/042005-1.aspx
            try
            {
                
                location.href = loc;
            }
            catch (err)
            {
                console.log(&quot;catch&quot;);
                console.log(err);
            }
        }

    


   
	


//&lt;![CDATA[
AdminActionMemberWarning = &quot;The action you have taken will be applied to all the members in your group.  Are you sure you want to proceed?&quot;; Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function(){
                            if (window.addEventListener) { 
                                window.addEventListener(&quot; , &quot;'&quot; , &quot;load&quot; , &quot;'&quot; , &quot;,_imgButtonInit,true);
                              } else if (window.attachEvent){
                                window.attachEvent(&quot; , &quot;'&quot; , &quot;onload&quot; , &quot;'&quot; , &quot;,_imgButtonInit);
                              }
                        });//]]>

addImageButton(&quot;btnGo&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;ignore&quot;,false,&quot;GoGroup&quot;,&quot;__GoGroup_SelectedName&quot;,false,false,true,false);
addImageButton(&quot;imgBtnBack&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;ignore&quot;,false,&quot;&quot;,&quot;___SelectedName&quot;,false,false,false,false);

(function(){var a=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(5);window.peTrackerObject=PETracker.init(a.trackerId,a.config)})();(function(){if(window.peTrackerObject&amp;&amp;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(7)){google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(8)&amp;&amp;window.peTrackerObject.setProperty(&quot;loginSessionId&quot;,&quot;hncfii2gclba5im025uflpew&quot;);var b=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(41);if(b){switch(&quot;usertelemetry&quot;.toLocaleLowerCase()){case &quot;usertelemetry&quot;:window.PETPageId||(window.peTrackerObject.generatePageId(),b.payload.pageVisitId=window.PETPageId);b.payload.gaClientId=b.payload.gaClientId||window.PETPageId;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(42)!==window.PETPageId&amp;&amp;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(43)&amp;&amp;window.peTrackerObject.setProperty(&quot;gaClientId&quot;,
&quot;undefined&quot;);window.peTrackerObject.setProperty(&quot;personId&quot;,&quot;744891&quot;);var c=JSON.parse(localStorage.getItem(&quot;absdk-geolocation&quot;)||&quot;{}&quot;);c.sessionId!==google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(44)&amp;&amp;window.peTrackerObject.getMyGeo(function(d,a){d||window.dataLayer.push({event:&quot;usersessionenvironmentcontext&quot;,ipV4:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(45)||a.ipv4,ipV6:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(46)||a.ipv6,countryCode:a.geo.country_code,geoNetworkContinentCode:a.geo.continent,geoNetworkSubContinentCode:&quot;NA&quot;,geoNetworkCountryCode:a.geo.country,
geoNetworkRegionCode:a.geo.subdivision,geoNetworkCityName:a.geo.city,geoNetworkLatitude:(a.geo.latitude||0).toString(),geoNetworkLongitude:(a.geo.longitude||0).toString()})});c={messageTypeCode:&quot;UserTelemetryPageActivity&quot;,originatingSystemCode:&quot;xl&quot;,namespaceCode:&quot;Telemetry&quot;,messageVersion:&quot;1.1.0&quot;,personId:&quot;744891&quot;,environmentCode:&quot;PRD&quot;,loginSessionId:&quot;hncfii2gclba5im025uflpew&quot;,gaClientId:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(47)||window.PETPageId};window.peTrackerObject.initTelemetry(c)}window.peTrackerObject.sendActivity(b.payload,
b.config)}}})();(function(){if(window.peTrackerObject){var peGtmBuiltInEvent=&quot;usertelemetry&quot;;var timestamp=new Date;var transactionLocalDt=null;if(timestamp.toString().indexOf(&quot;GMT&quot;)>-1)transactionLocalDt=(new Date(timestamp.toString().split(&quot;GMT&quot;)[0]+&quot; UTC&quot;)).toISOString().split(&quot;Z&quot;)[0];else transactionLocalDt=timestamp.getFullYear()+&quot;-&quot;+(&quot;00&quot;+(timestamp.getMonth()+1)).slice(-2)+&quot;-&quot;+(&quot;00&quot;+timestamp.getDate()).slice(-2)+&quot;T&quot;+(&quot;00&quot;+timestamp.getHours()).slice(-2)+&quot;:&quot;+(&quot;00&quot;+timestamp.getMinutes()).slice(-2)+
&quot;:&quot;+(&quot;00&quot;+timestamp.getSeconds()).slice(-2)+&quot;:&quot;+(&quot;000&quot;+timestamp.getMilliseconds()).slice(-3);switch(peGtmBuiltInEvent){case &quot;gtm.linkClick&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(69)||&quot;Manage Offline Items&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(110)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;gtm.click&quot;:var clicktype=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(112);var tagtype=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(114);
if(clicktype)switch(clicktype.toLocaleLowerCase()){case &quot;checkbox&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:&quot;gradebook&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(165)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;radio&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:&quot;gradebook&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(216)||&quot;NA&quot;,
eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;button&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(238)||google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(242),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(253)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break}else if(tagtype){tagtype=tagtype.toLocaleLowerCase();if(tagtype===&quot;img&quot;)window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,
{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(275),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(286)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});else if(tagtype===&quot;i&quot;)if(&quot;undefined&quot;.indexOf(&quot;fa&quot;)>-1)window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(308),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(319)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()})}break}}})();
   


(function(){document.querySelectorAll(&quot;select&quot;);var c=document.querySelectorAll(&quot;select&quot;).length,b=function(a){&quot;select&quot;==a.target.tagName.toLowerCase()&amp;&amp;(a=a.target.options[a.target.selectedIndex],window.dataLayer.push({event:&quot;selectionMade&quot;,selectedElement:a}))};document.addEventListener(&quot;change&quot;,b,!0);for(b=0;b&lt;=c;b++);})();(function(){function b(){var a=new c;a.addEventListener(&quot;readystatechange&quot;,function(d){},!0);return a}var c=window.XMLHttpRequest;window.XMLHttpRequest=b})();/html[1]&quot;) or . = concat(&quot;
	Manage Offline Items - Prabhashi ins1

         
             var gtmaccount = &quot;GTM-5JX93LK&quot;;
             var gtmpreview = &quot;env-2&quot;;
             var gtmauth = &quot;SVJdwwG3ki5lLhJ_zIzvuA&quot;;
             var dataLayer = [];
        
  
        
    

        
       
       
    
    
        TABLE.grid TD 
            { 
              vertical-align:middle;
              height: 34px;

            }
    
	
	    function GoBack()
	    {
	        deltaBack();
	        return false;
	    }

	    function DoAction(cbxActionID, assignmentId, assignmentType)
	    {   
	        var cbxAction=document.getElementById(cbxActionID);
	        var selAction= cbxAction.options[cbxAction.selectedIndex].value;
	        if (selAction==&quot;Change Scores&quot;)
	        {
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=step3&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Edit Item Info&quot;)
	        {
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=step2&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Copy Offline&quot;)
	        {
	            //location=&quot;CopyOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;externalId=&quot; + assignmentId + &quot;&amp;back=ManageOfflineItems.aspx&quot;;
	            location=&quot;AddOfflineItem.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;action=copy&amp;back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else if (selAction==&quot;Delete Offline&quot;)
	        {
	            location=&quot;AssignmentsDelete.aspx?back=ManageOfflineItems.aspx%3Fback%3DGradebook.aspx&quot;;
	        }
	        else
	        {
	            // debug
	            //location=&quot;OfflineItemSummary.aspx?type=&quot; + assignmentType + &quot;&amp;assignmentId=&quot; + assignmentId + &quot;&amp;back=ManageOfflineItems.aspx&quot;;
	        }
	        return false;
	    }
		
		
	    /*=======================================
         * INIT
         *=======================================
         */
	    // Chain to body onload
	    if (document.getElementById &amp;&amp; document.createElement) {    
	        if (window.addEventListener) {
	            window.addEventListener(&quot;load&quot;,Init,true);
	        } else if (window.attachEvent){
	            window.attachEvent(&quot;onload&quot;,Init);
	        }
	    }

	    function Init()
	    {
	        // handle omitted assignments
	        var tbl = document.getElementById(&quot;GridAssignments&quot;);
	        if (tbl) 
	        {
	            var rows = tbl.getElementsByTagName(&quot;tr&quot;);
	            for (var i = 0; i &lt; rows.length; i++)
	                if (rows[i].getAttribute(&quot;omit&quot;) == &quot;1&quot;)
	                    rows[i].className += &quot; omit&quot;;
	        }
	    }
	





(window.BOOMR_mq=window.BOOMR_mq||[]).push([&quot;addVar&quot;,{&quot;rua.upush&quot;:&quot;false&quot;,&quot;rua.cpush&quot;:&quot;false&quot;,&quot;rua.upre&quot;:&quot;false&quot;,&quot;rua.cpre&quot;:&quot;true&quot;,&quot;rua.uprl&quot;:&quot;false&quot;,&quot;rua.cprl&quot;:&quot;false&quot;,&quot;rua.cprf&quot;:&quot;false&quot;,&quot;rua.trans&quot;:&quot;SJ-8413d74b-58f9-49bf-a874-5e8706e383f8&quot;,&quot;rua.cook&quot;:&quot;true&quot;,&quot;rua.ims&quot;:&quot;false&quot;,&quot;rua.ufprl&quot;:&quot;false&quot;,&quot;rua.cfprl&quot;:&quot;true&quot;,&quot;rua.isuxp&quot;:&quot;false&quot;,&quot;rua.texp&quot;:&quot;norulematch&quot;}]);
                              !function(a){var e=&quot;https://s.go-mpulse.net/boomerang/&quot;,t=&quot;addEventListener&quot;;if(&quot;False&quot;==&quot;True&quot;)a.BOOMR_config=a.BOOMR_config||{},a.BOOMR_config.PageParams=a.BOOMR_config.PageParams||{},a.BOOMR_config.PageParams.pci=!0,e=&quot;https://s2.go-mpulse.net/boomerang/&quot;;if(window.BOOMR_API_key=&quot;B7LE9-GF2JD-PPN44-7ULXU-DPSH4&quot;,function(){function n(e){a.BOOMR_onload=e&amp;&amp;e.timeStamp||(new Date).getTime()}if(!a.BOOMR||!a.BOOMR.version&amp;&amp;!a.BOOMR.snippetExecuted){a.BOOMR=a.BOOMR||{},a.BOOMR.snippetExecuted=!0;var i,_,o,r=document.createElement(&quot;iframe&quot;);if(a[t])a[t](&quot;load&quot;,n,!1);else if(a.attachEvent)a.attachEvent(&quot;onload&quot;,n);r.src=&quot;javascript:void(0)&quot;,r.title=&quot;&quot;,r.role=&quot;presentation&quot;,(r.frameElement||r).style.cssText=&quot;width:0;height:0;border:0;display:none;&quot;,o=document.getElementsByTagName(&quot;script&quot;)[0],o.parentNode.insertBefore(r,o);try{_=r.contentWindow.document}catch(O){i=document.domain,r.src=&quot;javascript:var d=document.open();d.domain=&quot; , &quot;'&quot; , &quot;&quot;+i+&quot;&quot; , &quot;'&quot; , &quot;;void(0);&quot;,_=r.contentWindow.document}_.open()._l=function(){var a=this.createElement(&quot;script&quot;);if(i)this.domain=i;a.id=&quot;boomr-if-as&quot;,a.src=e+&quot;B7LE9-GF2JD-PPN44-7ULXU-DPSH4&quot;,BOOMR_lstart=(new Date).getTime(),this.body.appendChild(a)},_.write(&quot;&lt;bo&quot;+&quot; , &quot;'&quot; , &quot;dy onload=&quot;document._l();&quot;>&quot; , &quot;'&quot; , &quot;),_.close()}}(),&quot;&quot;.length>0)if(a&amp;&amp;&quot;performance&quot;in a&amp;&amp;a.performance&amp;&amp;&quot;function&quot;==typeof a.performance.setResourceTimingBufferSize)a.performance.setResourceTimingBufferSize();!function(){if(BOOMR=a.BOOMR||{},BOOMR.plugins=BOOMR.plugins||{},!BOOMR.plugins.AK){var e=&quot;true&quot;==&quot;true&quot;?1:0,t=&quot;cookiepresent&quot;,n=&quot;2ruo3vyxggvteyt3huda-f-288e55f37-clientnsv4-s.akamaihd.net&quot;,i=&quot;false&quot;==&quot;true&quot;?2:1,_={&quot;ak.v&quot;:&quot;32&quot;,&quot;ak.cp&quot;:&quot;1248014&quot;,&quot;ak.ai&quot;:parseInt(&quot;749947&quot;,10),&quot;ak.ol&quot;:&quot;0&quot;,&quot;ak.cr&quot;:94,&quot;ak.ipv&quot;:4,&quot;ak.proto&quot;:&quot;h2&quot;,&quot;ak.rid&quot;:&quot;8f02d43&quot;,&quot;ak.r&quot;:31130,&quot;ak.a2&quot;:e,&quot;ak.m&quot;:&quot;x&quot;,&quot;ak.n&quot;:&quot;essl&quot;,&quot;ak.bpcip&quot;:&quot;212.104.237.0&quot;,&quot;ak.cport&quot;:15367,&quot;ak.gh&quot;:&quot;104.75.84.58&quot;,&quot;ak.quicv&quot;:&quot;&quot;,&quot;ak.tlsv&quot;:&quot;tls1.3&quot;,&quot;ak.0rtt&quot;:&quot;&quot;,&quot;ak.csrc&quot;:&quot;-&quot;,&quot;ak.acc&quot;:&quot;&quot;,&quot;ak.t&quot;:&quot;1652243718&quot;,&quot;ak.ak&quot;:&quot;hOBiQwZUYzCg5VSAfCLimQ==JSvUV7lR8ws3FlLtfHMNEIMeD8M/xaKWPgwPMc4aHnCeUfTacVoU0h5uBEV4DDsMQk+LoUk8yryrbgVaJ9WTaiRZXBdlzLJyVxOxqbc1CoNKs1kZ3EjHXj+FW8QW5yvmeHwUThlJqZf+iFTTyHFgzHFaszcdwkgOInHHPYDNoD8+zDLQITmHSopGC74rvFGr5htxI3hiMTKdd+yxtHvtSKDWbZwh2fmB4Lzvu4tA0Svxs1vMYgHEaCjm0k39aNhU7ZXB+TehpGt7PE8lxDuV1pZScVI19HwEnr8JO5JQ4gK+sYddwIILiDaksAOKJGK4pE+FVReKYABRKVoW8lLUqYLJl04BahTxGjB15UuFNsw7Lc986bS/t692/aUVHO2H3rhlzarDxnkGIzLfsFVUKGFdDJHbHFeHGgNaIC28zqs=&quot;,&quot;ak.pv&quot;:&quot;1&quot;,&quot;ak.dpoabenc&quot;:&quot;&quot;,&quot;ak.tf&quot;:i};if(&quot;&quot;!==t)_[&quot;ak.ruds&quot;]=t;var o={i:!1,av:function(e){var t=&quot;http.initiator&quot;;if(e&amp;&amp;(!e[t]||&quot;spa_hard&quot;===e[t]))_[&quot;ak.feo&quot;]=void 0!==a.aFeoApplied?1:0,BOOMR.addVar(_)},rv:function(){var a=[&quot;ak.bpcip&quot;,&quot;ak.cport&quot;,&quot;ak.cr&quot;,&quot;ak.csrc&quot;,&quot;ak.gh&quot;,&quot;ak.ipv&quot;,&quot;ak.m&quot;,&quot;ak.n&quot;,&quot;ak.ol&quot;,&quot;ak.proto&quot;,&quot;ak.quicv&quot;,&quot;ak.tlsv&quot;,&quot;ak.0rtt&quot;,&quot;ak.r&quot;,&quot;ak.acc&quot;,&quot;ak.t&quot;,&quot;ak.tf&quot;];BOOMR.removeVar(a)}};BOOMR.plugins.AK={akVars:_,akDNSPreFetchDomain:n,init:function(){if(!o.i){var a=BOOMR.subscribe;a(&quot;before_beacon&quot;,o.av,null,null),a(&quot;onbeacon&quot;,o.rv,null,null),o.i=!0}return this},is_complete:function(){return!0}}}}()}(window);#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}

    
        &lt;iframe src=&quot;https://www.googletagmanager.com/ns.html?id=GTM-5JX93LK&amp;gtm_auth=SVJdwwG3ki5lLhJ_zIzvuA&amp;gtm_preview=env-2&amp;gtm_cookies_win=x&quot;
        height=&quot;0&quot; width=&quot;0&quot; style=&quot;display:none;visibility:hidden&quot;>&lt;/iframe>
    
	
    Skip Navigation

	








//&lt;![CDATA[
var theForm = document.forms[&quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>







//&lt;![CDATA[
var BaseUrl=&quot;../&quot;;
var BaseAbsUrl=&quot;https://mylabglobal.pearson.com/&quot;;
var BackUrl=&quot;gradebook.aspx&quot;;
var LocDateParts= { &quot;ttam&quot;: &quot;AM&quot;, &quot;ttpm&quot;: &quot;PM&quot;};
var LocClockPattern=&quot;dd/MM/yy HH:mm&quot;;
var StrUtils_DecimalSeparator=&quot;.&quot;;
LOCNS.LocMap.RegRes(&quot;common/formtracker.js&quot;,&quot;LeaveAlert&quot;,&quot;Your changes will not be saved.&quot;);
LOCNS.LocMap.RegRes(&quot;common/formtracker.js&quot;,&quot;LeaveConfirm&quot;,&quot;Are you sure you want to navigate away from this page?\n\n\n\n{0}\n\n\n\nPress OK to continue, or Cancel to stay on the current page.&quot;);
LOCNS.LocMap.RegRes(&quot;common/popups_1.js&quot;,&quot;PopupBlockedWarning&quot;,&quot;Your browser is blocking pop-ups, and they need to be allowed before working in your course. \nPlease disable your pop-up blocker now.&quot;);
LOCNS.LocMap.RegRes(&quot;resources&quot;,&quot;Cancel&quot;,&quot;Cancel&quot;);
LOCNS.LocMap.RegRes(&quot;resources&quot;,&quot;OK&quot;,&quot;OK&quot;);
//]]>







	

		
	
	

        
        
        
            
        
           
                         
                             
			

				
					
						
						Hide Navigation
					
				

                 
                        GlobalTestCourse
                

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnrol in CourseEdit AccountLog Out|HelpHelpLegendTours &amp; trainingMyLab Math Global SupportBrowser Check|11/05/22 10:05
				

			
		
        
        

		    
			    
			    
			    
		    

		    STUDENTCourse HomeStudy PlanCalendarHomework and TestsResultsStudent ResourcesINSTRUCTORInstructor HomeCourse ManagerHome Page ManagerAssignment ManagerStudy Plan ManagerGradebookInstructor Resources

	    
                             
        
		        
		            
                    
                        
                            
                                                     
                                
                                
	
                                    
                                        
                                        
                                        


    
        
            GlobalTestCourse [1]
            
        
        
          
              
              Manage Course List
          
          
          06/05_Downtime_GlobalPreBvt [0]Coord: GlobalCoordTestCourse [0]GlobalTestCourse [1]SampleCourse [0]
        
    




     
    

|GRADEBOOK
                                    
                                    
                                


                                
                                
                                
                                 
                                      
                                                
                                                
                                                    
                                                        Prabhashi ins1
                                                        GlobalTestCourse
                                                    
                                                    
                                                        Prabhashi ins1
                                                        MML Global Test Book
                                                    
                                                    
                                                        11/05/22
                                                        10:05
                                                    
                                                
                                                
                                                
                                    
                            
                        
                    
					
							
							    
    
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>



    


       
           
        
            
        
        
           
            
            Manage Offline Items   
           
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Manage Offline Items
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    



    
             
	

    
    
	
		
			
				Ch. ItemCategoryDateActions 
			
		
			
				
                    1‑4
                
                    
                
                    OfflineTest
                
                    Test
                
                    11/05/22
                
                        
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				
                
                    Go
				
                
			
				
                    1‑4
                
                    
                
                    OfflineQuiz
                
                    Quiz
                
                    11/05/22
                
                        
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				
                
                    Go
				
                
			
		
	




    
    Done

    
    


                                 
        			 
                    
                        
                                
                                   
                                        This course is based on MML Global Test BookTerms of Use|Privacy Policy|Copyright 2022 Pearson  All Rights Reserved.
                                              
                                
                            
                                    

                            
                                               
                        
                    
                     
                
	    
                       
                        
         

        
        
	


        
	    [+]
	

 
       
	

	

	

       
        
            var gaaccount = &quot;UA-48773414-1&quot;;
            var environment = &quot;global&quot;;
    
	
	
	
		

		

		

	
			
        
            var initialjsclockServerDateTime = &quot;Wed, 11 May 2022 10:05:18&quot;;
            function showLoading(fromTimer)
            {
                var el = document.getElementById(&quot;loading&quot;);
                // don&quot; , &quot;'&quot; , &quot;t show this if it&quot; , &quot;'&quot; , &quot;s already been hidden

                if(AllowShowPleaseWait &amp;&amp; el)
                {
                    el.style.display=&quot;&quot;;  
                }
            }
        
            function CourseChanged(select, originalSelectedValue) {
                //var courseId = item.Value;
                var courseId = select.options[select.selectedIndex].value;
                var loc = &quot; , &quot;'&quot; , &quot;ChangeCourse.aspx&quot; , &quot;'&quot; , &quot;
                //JS: this param needs to be removed.
                //It is passed into the assignment manager in the Qstring
                //if we don&quot; , &quot;'&quot; , &quot;t remove it, it keeps getting passed along.
                var tmplocation=location.pathname;
                tmplocation=removeParam(tmplocation,&quot;scId&quot;);
        
                // CA same for experiment assignment id, which generates a popup after the user creates an experiment assignment and clicks on the appropriate link
                tmplocation = removeParam(tmplocation, &quot;experimentAssignmentId&quot;);

                // Remove contentAreaID in case it&quot; , &quot;'&quot; , &quot;s not valid in the new course
                tmplocation = removeParam(tmplocation, &quot;ContentAreaID&quot;);

                // XL-3896 - Targeted UI Changes: Remove isForceAssignmentManager because it is not valid in the new course
                tmplocation = removeParam(tmplocation, &quot;isForceAssignmentManager&quot;);
        
                loc = loc + &quot; , &quot;'&quot; , &quot;?courseId=&quot; , &quot;'&quot; , &quot;+courseId+&quot; , &quot;'&quot; , &quot;&amp;target=&quot; , &quot;'&quot; , &quot;+escape(tmplocation);
        
                //This is to fix the JS &quot;Unspecificed Error&quot; that occurs when the form tracking is on
                //and the user changes the course and clicks cancel on the prompt.
                //http://aspnet.4guysfromrolla.com/articles/042005-1.aspx
                try
                {
                    location = loc;
                }
                catch (err)
                {
                    //set it back to original.  They hit cancel.
                    select.selectedIndex=originalSelectedValue;
                }
            }
      
            function AlertChooseBookMyCourses(newlocation)
            {
                alert(&quot;The course or book you were just viewing is not in your course list.  Please choose the course you would like to work in from the My Courses page.&quot;);
                if (newlocation)
                    location = newlocation; // Comment 9610
                else
                    location.reload(); // comment 9402
            }
      
            function AlertChooseBook()
            {
                alert(&quot;The course or book you were just viewing is not in your course list.  Please choose the course you would like to work in from the course drop down&quot;);
                location.reload(); // comment 7928
            }
      
            function AlertCourseChanged()
            {
                alert(&quot;The course or book you were just viewing as a student is not in your course list.  Choose the course you want to work in from the course drop down.&quot;);
                location.reload(); // comment 7928
            }
      
            function AlertCourseChangedEmptyCurrent()
            {
                alert(&quot;You have chosen not to show any courses in this drop down menu. To add courses to this list, go to Manage Course List and flag the courses as current courses.&quot;);
                location.reload(); // requested in C07-12
            }
      
            var navVisible = true;
            wizardUrl = &quot;../BrowserCheck/BrowserCheck.aspx&quot;;
            supportLink = &quot;&quot;;
            var navUrl = &quot;../Service/LeftNavStateUpdate.aspx&quot;

            

            function initLeftNav()
            {
                LeftNavStateUpdate.init(navUrl);  
            }

            function onNavCallback()
            {
                var navDiv = document.getElementById(&quot; , &quot;'&quot; , &quot;leftnavcontainer&quot; , &quot;'&quot; , &quot;);
                var navShowDiv = document.getElementById(&quot; , &quot;'&quot; , &quot;leftnavshow&quot; , &quot;'&quot; , &quot;);  
  
                if (navDiv.style.display==&quot;none&quot;) 
                {
                    navDiv.style.display = &quot;block&quot;;
                    navShowDiv.style.display = &quot;none&quot;;
                    navVisible = true;
                }
                else
                {
                    navDiv.style.display = &quot;none&quot;;
                    navShowDiv.style.display = &quot;block&quot;;          
                    navVisible = false;
                }
            }


            function toggleNav_OLD()
            {
                var navDiv = document.getElementById(&quot; , &quot;'&quot; , &quot;leftnavcontainer&quot; , &quot;'&quot; , &quot;);
  
                var navCurrentlyVisible = (navDiv.style.display != &quot;none&quot;);

                LeftNavStateUpdate.doNavCallback(!navCurrentlyVisible, onNavCallback);   
            }

            function goMyCourses() 
            {
                location = &quot;MyCourses.aspx&quot;;
                return false;
            }

            function goCourseHome()
            {
                var courseHomeUrl = &quot;&quot;;

                if (courseHomeUrl != &quot;&quot;)
                    LeftNavStateUpdate.doNavCallback(false, null, courseHomeUrl);    
            }

            function goStudyPlan()
            {
                location = &quot;../Student/StudyPlanRecommendations.aspx&quot;;
            }

            function goStudyPlanHomepage() 
            {
                location = &quot;../Student/StudyPlanRecommendations.aspx&quot;;
            }

            function setupClock()
            {
                jsclockServerDateTime = &quot;Wed, 11 May 2022 10:05:18&quot;;
                jsclockElementId = &quot;toptime&quot;;
                jsclockConstantUpdate = true;
                isSelfStudyCourse = &quot;False&quot;;
                
  
                // check if it&quot; , &quot;'&quot; , &quot;s there just in case we&quot; , &quot;'&quot; , &quot;re not on IntelliPage
                if (typeof(initClock)==&quot;function&quot;)
                    initClock();
            }

            function ShowStudentCalendar()
            {
                var loc = &quot; , &quot;'&quot; , &quot;../Student/MyDashboard.aspx&quot; , &quot;'&quot; , &quot;
                location=loc;
            }

            var myNavParams = myNavParams || {
                productId: 44,
                userId: 744891,
                userName: &quot;Prabhashi ins1&quot;,
                authSystemId: 1,
                courseId: 68825,
                bookId: 990213,
                backdoor: &quot; , &quot;'&quot; , &quot;False&quot; , &quot;'&quot; , &quot;,
                nextgen: &quot; , &quot;'&quot; , &quot;True&quot; , &quot;'&quot; , &quot;,
                lockdownbrowsertype: &quot; , &quot;'&quot; , &quot;None&quot; , &quot;'&quot; , &quot;,
                currentCulture: &quot;en-IE&quot;,
                showBanner: true,
                showLeftNavigation: true,
                leftNavState: &quot; , &quot;'&quot; , &quot;False&quot; , &quot;'&quot; , &quot;,
                navigationVersion: &quot; , &quot;'&quot; , &quot;v1&quot; , &quot;'&quot; , &quot;,
                userRole: &quot; , &quot;'&quot; , &quot;Instructor&quot; , &quot;'&quot; , &quot;,
                hasCustomBanner : true,
                pageLinkMode : &quot;Default&quot;,
                isCC : false,
                showPlayerHeader : false,
                showBrowserCheck: true,
                IsSelfStudy: false,
                hideNavigationText: &quot;Hide Navigation&quot;,
                showNavigationText: &quot;Show Navigation&quot;,
                institutionKeyId: &quot;&quot;,
                school: &quot;SMS Default Institution&quot;,
                authSystemPartnerId: &quot;&quot;,
                bookCode: &quot;hedmathstest&quot;,
                disciplineID: &quot;30&quot;,
                userOrigin: &quot; , &quot;'&quot; , &quot;MathXL&quot; , &quot;'&quot; , &quot;,
                authsystemtypeId: &quot; , &quot;'&quot; , &quot;Sms&quot; , &quot;'&quot; , &quot;,
                supportNewNavigation: true,
                supportMobile: true,
                isMyFoundations: false,
                currentLocaleFormat: &quot; , &quot;'&quot; , &quot;MM/DD/YY&quot; , &quot;'&quot; , &quot;,
                IPv4: &quot; , &quot;'&quot; , &quot;212.104.237.215&quot; , &quot;'&quot; , &quot;,
                IPv6: &quot; , &quot;'&quot; , &quot;NA&quot; , &quot;'&quot; , &quot;,
                SessionID: &quot; , &quot;'&quot; , &quot;hncfii2gclba5im025uflpew&quot; , &quot;'&quot; , &quot;,
                ProductName: &quot; , &quot;'&quot; , &quot;MyLab Math Global&quot; , &quot;'&quot; , &quot;,
                ProductPlatformCode: &quot; , &quot;'&quot; , &quot;Standalone&quot; , &quot;'&quot; , &quot;,
                TransactionLocalDate: &quot; , &quot;'&quot; , &quot;2022-05-11T10:05:18.831&quot; , &quot;'&quot; , &quot;,
                IsTelemetryEnabled: &quot; , &quot;'&quot; , &quot;True&quot; , &quot;'&quot; , &quot;,
                NoNameScreenReaderText: &quot;&quot;
            };

            var siteheader = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            var sitenav = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            var printheader = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_printheader&quot; , &quot;'&quot; , &quot;;

        
        

    
    
    
    
        setupClock();
    
        
    
	
	

     
         var pnlBreadCrumbId = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_PnlBreadCrumb&quot; , &quot;'&quot; , &quot;;
         var pnlGBBreadCrumbId = &quot; , &quot;'&quot; , &quot;ctl00_ctl00_InsideForm_PnlGradebookBreadCrumb&quot; , &quot;'&quot; , &quot;;
     
    

        var navigation_key;
        function setCurrentNavigationKey(navKey) {
            //window.localStorage[&quot; , &quot;'&quot; , &quot;lastNavKey&quot; , &quot;'&quot; , &quot;] = navKey;
            navigation_key = navKey;
        }

        setCurrentNavigationKey(35);
    
    
    
        $(document).ready(function () {
            $(&quot;#GridAssignments tbody&quot;).attr(&quot; , &quot;'&quot; , &quot;id&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;assignbody&quot; , &quot;'&quot; , &quot;);
        });

    
    

    
    
		
		    
            var currentMaster = &quot;default&quot;;
		
            var navigationData = {&quot;TopNavigation&quot;:[{&quot;Key&quot;:49,&quot;NameResourceKey&quot;:&quot;USERNAME&quot;,&quot;TooltipResourceKey&quot;:&quot;USERNAME&quot;,&quot;Name&quot;:&quot;prabhashi_ins1&quot;,&quot;Url&quot;:&quot;#&quot;,&quot;Tooltip&quot;:&quot;prabhashi_ins1&quot;,&quot;SubMenuData&quot;:[{&quot;Key&quot;:0,&quot;NameResourceKey&quot;:&quot;Master.MyCourses&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.MyCourses&quot;,&quot;Name&quot;:&quot;My Courses&quot;,&quot;Url&quot;:&quot;javascript:goMyCourses();&quot;,&quot;Tooltip&quot;:&quot;My Courses&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:1,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:52,&quot;NameResourceKey&quot;:&quot;Master.EnrollinCourse&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Enrollincourse&quot;,&quot;Name&quot;:&quot;Enrol in Course&quot;,&quot;Url&quot;:&quot;/student/enrollwizard.aspx&quot;,&quot;Tooltip&quot;:&quot;Enrol in Course&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:50,&quot;NameResourceKey&quot;:&quot;Master.EditAccount&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.EditAccount&quot;,&quot;Name&quot;:&quot;Edit Account&quot;,&quot;Url&quot;:&quot;javascript:DoHelp(&quot; , &quot;'&quot; , &quot;https://register.pearsoncmg.com/userprofile/up_login.jsp&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Edit Account&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:53,&quot;NameResourceKey&quot;:&quot;Master.Logout&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Logout&quot;,&quot;Name&quot;:&quot;Log Out&quot;,&quot;Url&quot;:&quot;/logout.aspx&quot;,&quot;Tooltip&quot;:&quot;Log Out&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:43,&quot;NameResourceKey&quot;:&quot;Master.Help&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Help&quot;,&quot;Name&quot;:&quot;Help&quot;,&quot;Url&quot;:&quot;&quot;,&quot;Tooltip&quot;:&quot;Help&quot;,&quot;SubMenuData&quot;:[{&quot;Key&quot;:44,&quot;NameResourceKey&quot;:&quot;Master.Help&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Help&quot;,&quot;Name&quot;:&quot;Help&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;toc_instructor_math&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Help&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:46,&quot;NameResourceKey&quot;:&quot;Master.Legend&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Legend&quot;,&quot;Name&quot;:&quot;Legend&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;legends&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Legend&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:59,&quot;NameResourceKey&quot;:&quot;Master.TakeaTourIntl&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.TakeaTourIntl&quot;,&quot;Name&quot;:&quot;Tours &amp; training&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;instr_tours&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Tours &amp; training&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:57,&quot;NameResourceKey&quot;:&quot;Master.ProductSupport&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.ProductSupport&quot;,&quot;Name&quot;:&quot;MyLab Math Global Support&quot;,&quot;Url&quot;:&quot;javascript:doHelp(&quot; , &quot;'&quot; , &quot;instr_support&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;MyLab Math Global Support&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:48,&quot;NameResourceKey&quot;:&quot;BrowserCheck&quot;,&quot;TooltipResourceKey&quot;:&quot;BrowserCheck&quot;,&quot;Name&quot;:&quot;Browser Check&quot;,&quot;Url&quot;:&quot;Javascript:goWizardCourseNG(68825,true,&quot; , &quot;'&quot; , &quot;en-IE&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;None&quot; , &quot;'&quot; , &quot;);&quot;,&quot;Tooltip&quot;:&quot;Browser Check&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:&quot;images/icon_helptop_school.gif&quot;,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:54,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:null,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:true,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:55,&quot;NameResourceKey&quot;:&quot;#&quot;,&quot;TooltipResourceKey&quot;:&quot;#&quot;,&quot;Name&quot;:&quot;05/11/2022 10:05:18&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;05/11/2022 10:05:18&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;LeftNavigation&quot;:[{&quot;Key&quot;:41,&quot;NameResourceKey&quot;:&quot;Master.Student&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;STUDENT&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:true,&quot;IsSeparator&quot;:false,&quot;Order&quot;:2,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:2,&quot;NameResourceKey&quot;:&quot;Master.CourseHome&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.CourseHome&quot;,&quot;Name&quot;:&quot;Course Home&quot;,&quot;Url&quot;:&quot;/Student/MyDashboard.aspx&quot;,&quot;Tooltip&quot;:&quot;Course Home&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:3,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:12,&quot;NameResourceKey&quot;:&quot;Master.StudyPlan&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudyPlan&quot;,&quot;Name&quot;:&quot;Study Plan&quot;,&quot;Url&quot;:&quot;javascript:goStudyPlanHomepage();&quot;,&quot;Tooltip&quot;:&quot;Study Plan&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:5,&quot;NameResourceKey&quot;:&quot;Master.Calendar&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Calendar&quot;,&quot;Name&quot;:&quot;Calendar&quot;,&quot;Url&quot;:&quot;/Student/Calendar.aspx&quot;,&quot;Tooltip&quot;:&quot;Calendar&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:10,&quot;NameResourceKey&quot;:&quot;Master.HomeworkTests&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomeworkTests&quot;,&quot;Name&quot;:&quot;Homework and Tests&quot;,&quot;Url&quot;:&quot;/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1&quot;,&quot;Tooltip&quot;:&quot;Homework and Tests&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:11,&quot;NameResourceKey&quot;:&quot;Master.Results&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Results&quot;,&quot;Name&quot;:&quot;Results&quot;,&quot;Url&quot;:&quot;/Student/Results.aspx&quot;,&quot;Tooltip&quot;:&quot;Results&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:15,&quot;NameResourceKey&quot;:&quot;Master.StudentResources&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudentResources&quot;,&quot;Name&quot;:&quot;Student Resources&quot;,&quot;Url&quot;:&quot;/Student/Resources.aspx?resourceId=Student Resources&quot;,&quot;Tooltip&quot;:&quot;Student Resources&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:58,&quot;NameResourceKey&quot;:&quot;&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;&quot;,&quot;Url&quot;:&quot;#empty&quot;,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:42,&quot;NameResourceKey&quot;:&quot;Master.Instructor&quot;,&quot;TooltipResourceKey&quot;:null,&quot;Name&quot;:&quot;INSTRUCTOR&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:&quot;&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:true,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:60,&quot;NameResourceKey&quot;:&quot;Master.InstructorHome&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.InstructorHome&quot;,&quot;Name&quot;:&quot;Instructor Home&quot;,&quot;Url&quot;:&quot;/Instructor/InstructorHome.aspx&quot;,&quot;Tooltip&quot;:&quot;Instructor Home&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:20,&quot;NameResourceKey&quot;:&quot;Master.CourseManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.CourseManager&quot;,&quot;Name&quot;:&quot;Course Manager&quot;,&quot;Url&quot;:&quot;/Instructor/CourseManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Course Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:24,&quot;NameResourceKey&quot;:&quot;Master.HomePageManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomePageManager&quot;,&quot;Name&quot;:&quot;Home Page Manager&quot;,&quot;Url&quot;:&quot;/Instructor/HomePageManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Home Page Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:27,&quot;NameResourceKey&quot;:&quot;Master.HomeworkTestManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.HomeworkTestManager&quot;,&quot;Name&quot;:&quot;Assignment Manager&quot;,&quot;Url&quot;:&quot;/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1&quot;,&quot;Tooltip&quot;:&quot;Assignment Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:&quot;Master.HomeworkTestManagerAltText&quot;,&quot;AlternateText&quot;:null},{&quot;Key&quot;:32,&quot;NameResourceKey&quot;:&quot;Master.StudyPlanManager&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.StudyPlanManager&quot;,&quot;Name&quot;:&quot;Study Plan Manager&quot;,&quot;Url&quot;:&quot;/Instructor/StudyPlanManager.aspx&quot;,&quot;Tooltip&quot;:&quot;Study Plan Manager&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:35,&quot;NameResourceKey&quot;:&quot;Master.Gradebook&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.Gradebook&quot;,&quot;Name&quot;:&quot;Gradebook&quot;,&quot;Url&quot;:&quot;/Instructor/Gradebook.aspx&quot;,&quot;Tooltip&quot;:&quot;Gradebook&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null},{&quot;Key&quot;:36,&quot;NameResourceKey&quot;:&quot;Master.InstructorResources&quot;,&quot;TooltipResourceKey&quot;:&quot;Master.InstructorResources&quot;,&quot;Name&quot;:&quot;Instructor Resources&quot;,&quot;Url&quot;:&quot;/Student/Resources.aspx?resourceId=Instructor Resources&quot;,&quot;Tooltip&quot;:&quot;Instructor Resources&quot;,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:null,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}],&quot;ProductHeaderInfo&quot;:{&quot;Key&quot;:56,&quot;NameResourceKey&quot;:&quot;&quot;,&quot;TooltipResourceKey&quot;:&quot;&quot;,&quot;Name&quot;:&quot;MyLab Math Global&quot;,&quot;Url&quot;:null,&quot;Tooltip&quot;:null,&quot;SubMenuData&quot;:null,&quot;IsHeader&quot;:false,&quot;IsSeparator&quot;:false,&quot;Order&quot;:0,&quot;ImageUrl&quot;:&quot;/support/customheader/product_global/newtoplogo.svg&quot;,&quot;AlternateNameResourceKey&quot;:null,&quot;AlternateText&quot;:null}};

            if (window.addEventListener) {
                window.addEventListener(&quot;load&quot;, function() {
                    setTimeout(function() {
                        //Hide the address bar.
                        window.scrollTo(0, 1);
                    }, 0);
                });
            } 

            // On dropdown open
            $(document).on(&quot; , &quot;'&quot; , &quot;shown.bs.dropdown&quot; , &quot;'&quot; , &quot;, function(event) {
                var dropdown = $(event.target);
		        
                // Set aria-expanded to true
                dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-toggle&quot; , &quot;'&quot; , &quot;).attr(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, true);
                    
                // Set focus on the first link in the dropdown
                //setTimeout(function() {
                //    dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-menu li:first-child a&quot; , &quot;'&quot; , &quot;).focus(); //focus to the first child in dropdown
                //}, 10);
            });

            // On dropdown close
            $(document).on(&quot; , &quot;'&quot; , &quot;hidden.bs.dropdown&quot; , &quot;'&quot; , &quot;, function(event) {
                var dropdown = $(event.target);
		       
                // Set aria-expanded to false        
                dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-toggle&quot; , &quot;'&quot; , &quot;).attr(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, false);
    
                // Set focus back to dropdown toggle
                dropdown.find(&quot; , &quot;'&quot; , &quot;.dropdown-toggle&quot; , &quot;'&quot; , &quot;).focus();
            }); 

        

	
	
	initLeftNav(); 

    

    
    

        $(&quot;.dropdown.course-selector&quot;).on(&quot;shown.bs.dropdown&quot;, function() {
            $(this).find(&quot;.dropdown-menu li.active a&quot;).focus()
        });

        $(&quot;.dropdown.course-selector .dropdown-menu li a&quot;).click(function () {
            if($(this).attr(&quot; , &quot;'&quot; , &quot;id&quot; , &quot;'&quot; , &quot;) != &quot; , &quot;'&quot; , &quot;manageCourses&quot; , &quot;'&quot; , &quot;)
            {
                var cID = $(this).data(&quot; , &quot;'&quot; , &quot;cid&quot; , &quot;'&quot; , &quot;);
                OnCourseChanged(cID);
            }
        });

        //modified the CourseChanged function according to new course dropdown functionality
        function OnCourseChanged(selectedCID) {

            var loc = &quot; , &quot;'&quot; , &quot;ChangeCourse.aspx&quot; , &quot;'&quot; , &quot;
            //JS: this param needs to be removed.
            //It is passed into the assignment manager in the Qstring
            //if we don&quot; , &quot;'&quot; , &quot;t remove it, it keeps getting passed along.
            var tmplocation=location.pathname;
            tmplocation=removeParam(tmplocation,&quot;scId&quot;);
        
            // CA same for experiment assignment id, which generates a popup after the user creates an experiment assignment and clicks on the appropriate link
            tmplocation = removeParam(tmplocation, &quot;experimentAssignmentId&quot;);

            // Remove contentAreaID in case it&quot; , &quot;'&quot; , &quot;s not valid in the new course
            tmplocation = removeParam(tmplocation, &quot;ContentAreaID&quot;);

            // XL-3896 - Targeted UI Changes: Remove isForceAssignmentManager because it is not valid in the new course
            tmplocation = removeParam(tmplocation, &quot;isForceAssignmentManager&quot;);
        
            loc = loc + &quot; , &quot;'&quot; , &quot;?courseId=&quot; , &quot;'&quot; , &quot;+selectedCID+&quot; , &quot;'&quot; , &quot;&amp;target=&quot; , &quot;'&quot; , &quot;+escape(tmplocation);
        
            //This is to fix the JS &quot;Unspecificed Error&quot; that occurs when the form tracking is on
            //and the user changes the course and clicks cancel on the prompt.
            //http://aspnet.4guysfromrolla.com/articles/042005-1.aspx
            try
            {
                
                location.href = loc;
            }
            catch (err)
            {
                console.log(&quot;catch&quot;);
                console.log(err);
            }
        }

    


   
	


//&lt;![CDATA[
AdminActionMemberWarning = &quot;The action you have taken will be applied to all the members in your group.  Are you sure you want to proceed?&quot;; Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function(){
                            if (window.addEventListener) { 
                                window.addEventListener(&quot; , &quot;'&quot; , &quot;load&quot; , &quot;'&quot; , &quot;,_imgButtonInit,true);
                              } else if (window.attachEvent){
                                window.attachEvent(&quot; , &quot;'&quot; , &quot;onload&quot; , &quot;'&quot; , &quot;,_imgButtonInit);
                              }
                        });//]]>

addImageButton(&quot;btnGo&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;ignore&quot;,false,&quot;GoGroup&quot;,&quot;__GoGroup_SelectedName&quot;,false,false,true,false);
addImageButton(&quot;imgBtnBack&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;&quot;,&quot;ignore&quot;,false,&quot;&quot;,&quot;___SelectedName&quot;,false,false,false,false);

(function(){var a=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(5);window.peTrackerObject=PETracker.init(a.trackerId,a.config)})();(function(){if(window.peTrackerObject&amp;&amp;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(7)){google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(8)&amp;&amp;window.peTrackerObject.setProperty(&quot;loginSessionId&quot;,&quot;hncfii2gclba5im025uflpew&quot;);var b=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(41);if(b){switch(&quot;usertelemetry&quot;.toLocaleLowerCase()){case &quot;usertelemetry&quot;:window.PETPageId||(window.peTrackerObject.generatePageId(),b.payload.pageVisitId=window.PETPageId);b.payload.gaClientId=b.payload.gaClientId||window.PETPageId;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(42)!==window.PETPageId&amp;&amp;google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(43)&amp;&amp;window.peTrackerObject.setProperty(&quot;gaClientId&quot;,
&quot;undefined&quot;);window.peTrackerObject.setProperty(&quot;personId&quot;,&quot;744891&quot;);var c=JSON.parse(localStorage.getItem(&quot;absdk-geolocation&quot;)||&quot;{}&quot;);c.sessionId!==google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(44)&amp;&amp;window.peTrackerObject.getMyGeo(function(d,a){d||window.dataLayer.push({event:&quot;usersessionenvironmentcontext&quot;,ipV4:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(45)||a.ipv4,ipV6:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(46)||a.ipv6,countryCode:a.geo.country_code,geoNetworkContinentCode:a.geo.continent,geoNetworkSubContinentCode:&quot;NA&quot;,geoNetworkCountryCode:a.geo.country,
geoNetworkRegionCode:a.geo.subdivision,geoNetworkCityName:a.geo.city,geoNetworkLatitude:(a.geo.latitude||0).toString(),geoNetworkLongitude:(a.geo.longitude||0).toString()})});c={messageTypeCode:&quot;UserTelemetryPageActivity&quot;,originatingSystemCode:&quot;xl&quot;,namespaceCode:&quot;Telemetry&quot;,messageVersion:&quot;1.1.0&quot;,personId:&quot;744891&quot;,environmentCode:&quot;PRD&quot;,loginSessionId:&quot;hncfii2gclba5im025uflpew&quot;,gaClientId:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(47)||window.PETPageId};window.peTrackerObject.initTelemetry(c)}window.peTrackerObject.sendActivity(b.payload,
b.config)}}})();(function(){if(window.peTrackerObject){var peGtmBuiltInEvent=&quot;usertelemetry&quot;;var timestamp=new Date;var transactionLocalDt=null;if(timestamp.toString().indexOf(&quot;GMT&quot;)>-1)transactionLocalDt=(new Date(timestamp.toString().split(&quot;GMT&quot;)[0]+&quot; UTC&quot;)).toISOString().split(&quot;Z&quot;)[0];else transactionLocalDt=timestamp.getFullYear()+&quot;-&quot;+(&quot;00&quot;+(timestamp.getMonth()+1)).slice(-2)+&quot;-&quot;+(&quot;00&quot;+timestamp.getDate()).slice(-2)+&quot;T&quot;+(&quot;00&quot;+timestamp.getHours()).slice(-2)+&quot;:&quot;+(&quot;00&quot;+timestamp.getMinutes()).slice(-2)+
&quot;:&quot;+(&quot;00&quot;+timestamp.getSeconds()).slice(-2)+&quot;:&quot;+(&quot;000&quot;+timestamp.getMilliseconds()).slice(-3);switch(peGtmBuiltInEvent){case &quot;gtm.linkClick&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(69)||&quot;Manage Offline Items&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(110)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;gtm.click&quot;:var clicktype=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(112);var tagtype=google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(114);
if(clicktype)switch(clicktype.toLocaleLowerCase()){case &quot;checkbox&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:&quot;gradebook&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(165)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;radio&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:&quot;gradebook&quot;,eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(216)||&quot;NA&quot;,
eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break;case &quot;button&quot;:window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(238)||google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(242),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(253)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});break}else if(tagtype){tagtype=tagtype.toLocaleLowerCase();if(tagtype===&quot;img&quot;)window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,
{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(275),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(286)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()});else if(tagtype===&quot;i&quot;)if(&quot;undefined&quot;.indexOf(&quot;fa&quot;)>-1)window.peTrackerObject.saveTelemetryAction(&quot;event&quot;,{eventCategory:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(308),eventAction:&quot;Manage Offline Items&quot;,eventLabel:google_tag_manager[&quot;GTM-5JX93LK&quot;].macro(319)||&quot;NA&quot;,eventValue:&quot;NA&quot;,userLocalEventDt:transactionLocalDt,eventUnixTimestamp:Date.now()})}break}}})();
   


(function(){document.querySelectorAll(&quot;select&quot;);var c=document.querySelectorAll(&quot;select&quot;).length,b=function(a){&quot;select&quot;==a.target.tagName.toLowerCase()&amp;&amp;(a=a.target.options[a.target.selectedIndex],window.dataLayer.push({event:&quot;selectionMade&quot;,selectedElement:a}))};document.addEventListener(&quot;change&quot;,b,!0);for(b=0;b&lt;=c;b++);})();(function(){function b(){var a=new c;a.addEventListener(&quot;readystatechange&quot;,function(d){},!0);return a}var c=window.XMLHttpRequest;window.XMLHttpRequest=b})();/html[1]&quot;))]</value>
      <webElementGuid>fe902df1-2677-4a3e-8a16-880d5d3f98f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
