<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose --Change ScoresEdit Item I_a68bb0</name>
   <tag></tag>
   <elementGuidId>68908103-0192-4c38-a2f6-d4c1c719fb7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='daT490470']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#daT490470</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>623f954d-1374-4cb6-93f7-9a2beb86224f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$GridAssignments$ctl02$daT490470</value>
      <webElementGuid>8a907322-f474-46ef-ace1-c0b6dd81fe15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>daT490470</value>
      <webElementGuid>bb21a54e-623e-452a-8c6e-b41529b8b9ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>actionmenu</value>
      <webElementGuid>7661679f-5c0d-4288-858b-d38e3e0ecaa8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				</value>
      <webElementGuid>e7e0f997-0d65-4543-86f9-a04376eadc58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;daT490470&quot;)</value>
      <webElementGuid>74c9f4b3-1e43-49c6-ac8f-274a6c3ad93c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='daT490470']</value>
      <webElementGuid>9d5694c4-2c34-4368-89c8-14a60b67e391</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='assignbody']/tr/td[6]/select</value>
      <webElementGuid>32492c97-5788-4780-a2e5-6d2830fbfd9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/following::select[1]</value>
      <webElementGuid>bc81c415-9fd7-4782-8b25-2da2b27c0e62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OfflineTest'])[1]/following::select[1]</value>
      <webElementGuid>dca8c117-22b9-4c3d-8fa9-25b998aa197d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go'])[1]/preceding::select[1]</value>
      <webElementGuid>16d8eb96-4c5d-42bb-9c67-9901bf9ed893</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OfflineQuiz'])[1]/preceding::select[1]</value>
      <webElementGuid>1b74227a-f7e1-4a80-8f12-20b1117e08d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>a1f76624-94e5-41ff-9912-77758a6073e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$GridAssignments$ctl02$daT490470' and @id = 'daT490470' and (text() = '
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				' or . = '
					    -- Choose --    
					Change Scores
					Edit Item Info
					Copy
					Delete

				')]</value>
      <webElementGuid>f80aec16-324b-44d3-ae60-15b76c78f7e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
