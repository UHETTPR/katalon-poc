<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Save</name>
   <tag></tag>
   <elementGuidId>1dac06e5-0bb3-40c0-b23c-545945c5331b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='imgBtnSave']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#imgBtnSave</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>eb5193aa-af1b-444d-a0ca-a1a4af8fedba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$imgBtnSave','')</value>
      <webElementGuid>5c95afea-c3d6-4794-8b0f-864fa866c5ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>imgBtnSave</value>
      <webElementGuid>bd59c531-b5d6-467c-bfbb-09ebed095e5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return DoSave();</value>
      <webElementGuid>48288a50-3c30-44de-9b70-99bda3d42cf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary </value>
      <webElementGuid>12779e5a-1dac-4e05-9e91-2e1367fe5af2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Save
		</value>
      <webElementGuid>4b944a9a-58d3-49d4-8a9f-46204c9054fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;imgBtnSave&quot;)</value>
      <webElementGuid>c42370b9-2e0f-4b45-9877-e7ccbd0493e5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='imgBtnSave']</value>
      <webElementGuid>b3a2ca05-e069-4762-8543-dfd991ecfe32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='divbtnSave']/a</value>
      <webElementGuid>a24e1f20-9541-4aba-9f44-13d4556ea98c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Save')]</value>
      <webElementGuid>01383e1a-735a-422f-b39d-ba49208ec116</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Points total must be an integer from 1 to 9999.'])[1]/following::a[1]</value>
      <webElementGuid>a8dfe094-8ca2-4d8d-8d94-133eef46ba20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='prabhashi_ins1'])[1]/following::a[1]</value>
      <webElementGuid>d73db8f9-83ee-4c2e-bf0d-2f806ebc60d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::a[1]</value>
      <webElementGuid>de65bf8a-abb0-4d68-866b-e9b43a571711</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RadDatePicker'])[1]/preceding::a[2]</value>
      <webElementGuid>73f2e255-3c1b-405e-ae72-f73c066f099e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Save']/parent::*</value>
      <webElementGuid>b4e460d8-e5e9-43ef-b52b-afb79858c1f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$imgBtnSave','')&quot;)]</value>
      <webElementGuid>dc54cb13-667c-4f83-ad6c-09c6b273fac9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[4]/div/a</value>
      <webElementGuid>f45f4e6a-119b-4301-8c6a-070a58f54d1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:__doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$imgBtnSave&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)&quot;) and @id = 'imgBtnSave' and (text() = 'Save
		' or . = 'Save
		')]</value>
      <webElementGuid>a66ae2ae-b6fd-4399-8762-64148ff93cb2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
