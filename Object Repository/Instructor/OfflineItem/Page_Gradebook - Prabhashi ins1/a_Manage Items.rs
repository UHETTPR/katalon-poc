<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Manage Items</name>
   <tag></tag>
   <elementGuidId>2862697a-639b-4867-ad5d-12a9d4ae1a71</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Manage Items')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1ae817e1-c6b9-4342-831c-874561b56d9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/ManageOfflineItems.aspx?back=gradebook.aspx</value>
      <webElementGuid>6b566f7d-738c-4e53-9be6-d2a00256d718</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Manage Items</value>
      <webElementGuid>6c7c7d2f-6272-431d-b7f2-e7b30448834c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_toolbar1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[2]/a[1]</value>
      <webElementGuid>9585d04e-d059-4ba2-a66c-b0681a227a2a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[3]/div/div[2]/ul/li[2]/a</value>
      <webElementGuid>c8d70880-2e4d-477c-a94a-fd807bf7993f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Manage Items')]</value>
      <webElementGuid>70f83831-ed50-4389-89ec-25bfd84f0a5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Items'])[1]/following::a[1]</value>
      <webElementGuid>090c0f2c-c749-4ebf-9d94-d1ff322e86b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Weights'])[1]/following::a[3]</value>
      <webElementGuid>d0ea9f12-4b4f-4b44-a007-3899fde30d50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export Data'])[1]/preceding::a[1]</value>
      <webElementGuid>e8af3891-b34c-4d62-8c60-efa992855dbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Results By'])[1]/preceding::a[3]</value>
      <webElementGuid>87aed629-cbe0-4cc0-86d4-8a02616091aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Manage Items']/parent::*</value>
      <webElementGuid>0e01154e-599a-4c82-b326-07efa1769d12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/ManageOfflineItems.aspx?back=gradebook.aspx')]</value>
      <webElementGuid>ffa0a8a4-1288-4e47-b27b-c5ab8b117cf0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[2]/a</value>
      <webElementGuid>e387854b-812c-4b31-8b5d-e113fe40338f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/ManageOfflineItems.aspx?back=gradebook.aspx' and (text() = 'Manage Items' or . = 'Manage Items')]</value>
      <webElementGuid>81a9975b-d0fe-4cc7-b14a-00605e7bf2ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
