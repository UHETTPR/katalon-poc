<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_All Assignments</name>
   <tag></tag>
   <elementGuidId>1aef6ab6-d595-4162-a907-9e3bc4dcfc3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='ctl00_ctl00_InsideForm_MasterContent_TabControlGB_TabUl']/li/a/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.dropdown-toggle > span.selected-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ecaaf8e5-1351-4091-b5c8-69f9f21f8f4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>selected-item</value>
      <webElementGuid>ef575623-1b22-4524-b7c8-a69ebff82129</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Assignments</value>
      <webElementGuid>7e23fc55-0155-40fd-8033-0637e465e4f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TabControlGB_TabUl&quot;)/li[@class=&quot;dropdown-behavior active&quot;]/a[@class=&quot;dropdown-toggle&quot;]/span[@class=&quot;selected-item&quot;]</value>
      <webElementGuid>5a2cfe6a-ca47-471c-b04c-072ec7367445</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ctl00_ctl00_InsideForm_MasterContent_TabControlGB_TabUl']/li/a/span[2]</value>
      <webElementGuid>6195b3f9-f9c3-4752-8209-508675c5fbe2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View:'])[1]/following::span[1]</value>
      <webElementGuid>8ed6d4da-83f7-4212-885a-0f997fbef1ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export Data'])[1]/following::span[3]</value>
      <webElementGuid>f9b4bc0b-5a35-4af7-b8f7-743df6422509</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[2]/preceding::span[2]</value>
      <webElementGuid>68576c0d-6bfc-4c58-9e34-c3cd6141d1de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[1]/preceding::span[2]</value>
      <webElementGuid>bfdc9d0f-ff04-477f-802c-772cc1d4c72f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Assignments']/parent::*</value>
      <webElementGuid>382ab6cc-6765-487c-9219-41c8ce72f502</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a/span[2]</value>
      <webElementGuid>0845039b-fd87-4859-83aa-584b6a5e89ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'All Assignments' or . = 'All Assignments')]</value>
      <webElementGuid>0411a5fe-9e3b-46b1-88a4-c5e08834f888</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
