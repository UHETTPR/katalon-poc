<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Add Items</name>
   <tag></tag>
   <elementGuidId>91902e9f-9bac-4ce4-9ec4-e2b36c60a179</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Add Items')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > ul > li > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1d60388c-0696-4090-bab6-94dcf0a1c5f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/AddOfflineItem.aspx?back=gradebook.aspx</value>
      <webElementGuid>b90f496c-45c6-4f7f-b3be-75c4da5c2ae9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Items</value>
      <webElementGuid>ff978768-c958-4ee9-88ef-3a3029876a45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_toolbar1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[1]/a[1]</value>
      <webElementGuid>2c0f32e2-3d3b-4f60-aa2f-5c6abd2ef550</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[3]/div/div[2]/ul/li/a</value>
      <webElementGuid>cd6dc388-7f3f-4d18-a4f1-d1026451eb3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Add Items')]</value>
      <webElementGuid>69a0a1ea-d7bf-4a77-9da3-29c723829818</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Weights'])[1]/following::a[2]</value>
      <webElementGuid>f522f583-50a2-4561-897f-80c05d973ff6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manage Incompletes'])[1]/following::a[3]</value>
      <webElementGuid>41b86650-6f09-45e0-bd4a-f5c3ae5586c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manage Items'])[1]/preceding::a[1]</value>
      <webElementGuid>20fa418f-afb3-4b20-a664-6f90a223f99b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export Data'])[1]/preceding::a[2]</value>
      <webElementGuid>0ee153a5-f97f-4342-8ba0-60c774d14e55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add Items']/parent::*</value>
      <webElementGuid>116aef7d-64e0-4ed7-bf04-c3345a72e05e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/AddOfflineItem.aspx?back=gradebook.aspx')]</value>
      <webElementGuid>98bad997-77a5-4174-969b-65a88811e98e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>27daf6a2-0607-4682-86d8-c78270a29733</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/AddOfflineItem.aspx?back=gradebook.aspx' and (text() = 'Add Items' or . = 'Add Items')]</value>
      <webElementGuid>b7d7a508-b1f5-403d-be46-88d375139711</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
