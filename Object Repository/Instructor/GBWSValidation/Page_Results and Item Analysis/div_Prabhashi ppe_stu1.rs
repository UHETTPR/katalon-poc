<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Prabhashi ppe_stu1</name>
   <tag></tag>
   <elementGuidId>047137ea-2b57-4dc6-84ab-e1e4d0e1fc16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_StudentPager_studentInfoDiv']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_StudentPager_studentInfoDiv</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4711e1c3-4800-439d-8335-415f04f7f70b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_StudentPager_studentInfoDiv</value>
      <webElementGuid>ad3d267f-fdfc-40ca-946b-f1987e65f1b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>student-navigation-student-info-container left-div-border</value>
      <webElementGuid>10e38240-403e-450a-a6d2-498c2e80f4aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		        
		            
		                
		            
		            Prabhashi ppe_stu1
                    
                
		    </value>
      <webElementGuid>b8a19c5e-ca3e-41f5-bf4f-b38dfdd56e9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_StudentPager_studentInfoDiv&quot;)</value>
      <webElementGuid>89467c9f-dbcc-465d-adf0-0c08dfe9a27b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_StudentPager_studentInfoDiv']</value>
      <webElementGuid>8b7424fa-e1f3-4f89-91d9-c05a9f69f8a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div[6]/nav/div/div</value>
      <webElementGuid>e1a68c2a-5f7e-4386-bb90-3742c103e22a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::div[3]</value>
      <webElementGuid>a087edb0-d782-4c45-9176-0bea7b4f3147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Results and Item Analysis'])[1]/following::div[3]</value>
      <webElementGuid>59b14478-4133-4ad9-a391-048eeb0cea52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Student'])[1]/preceding::div[1]</value>
      <webElementGuid>66cd332c-6309-4c27-864a-7621e7d91089</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/nav/div/div</value>
      <webElementGuid>7e1375be-7e1b-4ce0-97fd-4e22b696318f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ctl00_ctl00_InsideForm_MasterContent_StudentPager_studentInfoDiv' and (text() = '
		        
		            
		                
		            
		            Prabhashi ppe_stu1
                    
                
		    ' or . = '
		        
		            
		                
		            
		            Prabhashi ppe_stu1
                    
                
		    ')]</value>
      <webElementGuid>c502e05f-1da8-4a42-82e3-05159971a63c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
