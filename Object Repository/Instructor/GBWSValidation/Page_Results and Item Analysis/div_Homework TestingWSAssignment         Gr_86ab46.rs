<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Homework TestingWSAssignment         Gr_86ab46</name>
   <tag></tag>
   <elementGuidId>b9a42389-adfb-47de-b58f-499f3e60a782</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_PnlInstructorAssignment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id = 'ctl00_ctl00_InsideForm_MasterContent_PnlInstructorAssignment' and (text() = '
	
         Homework: TestingWSAssignment
         Gradebook Score: 78%
         
    
' or . = '
	
         Homework: TestingWSAssignment
         Gradebook Score: 78%
         
    
')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b034cdd1-b15e-4ed5-8637-69e133d512e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_PnlInstructorAssignment</value>
      <webElementGuid>947c0c9d-d381-46b3-9a36-d7992fc748f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	
         Homework: TestingWSAssignment
         Gradebook Score: 78%
         
    
</value>
      <webElementGuid>9b1e0cb9-3187-4cad-889a-386e480f491d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_PnlInstructorAssignment&quot;)</value>
      <webElementGuid>893a86cf-8363-4d1d-ae93-bd0b6f183bfb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PnlInstructorAssignment']</value>
      <webElementGuid>403d32b0-23f8-4f40-b89c-641049356363</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div[8]</value>
      <webElementGuid>47e1e0dd-b1d9-4bb1-b43e-92a565df6e96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item Analysis'])[1]/following::div[1]</value>
      <webElementGuid>5c081d0d-9c2a-4b78-b688-e44288d8dcaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Results'])[2]/following::div[1]</value>
      <webElementGuid>ccfe3dbf-969c-4988-988c-3826ad236f52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[8]</value>
      <webElementGuid>e3a4bd03-7a6b-445a-b970-d2c8ce20bd58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ctl00_ctl00_InsideForm_MasterContent_PnlInstructorAssignment' and (text() = '
	
         Homework: TestingWSAssignment
         Gradebook Score: 78%
         
    
' or . = '
	
         Homework: TestingWSAssignment
         Gradebook Score: 78%
         
    
')]</value>
      <webElementGuid>f5650ea4-79db-497f-86bb-bcdba6862962</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
