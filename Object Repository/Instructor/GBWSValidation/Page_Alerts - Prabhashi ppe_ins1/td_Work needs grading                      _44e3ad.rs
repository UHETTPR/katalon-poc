<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Work needs grading                      _44e3ad</name>
   <tag></tag>
   <elementGuidId>6fdb7198-4bc8-4233-b82f-9d1578969f61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                        
                        Work needs grading  
                        
                        
                            
                            1 
                            
                            

                    ' or . = '
                        
                        Work needs grading  
                        
                        
                            
                            1 
                            
                            

                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>105f48ff-2ebf-4b74-b502-6e24c4fa8b60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Work needs grading  
                        
                        
                            
                            1 
                            
                            

                    </value>
      <webElementGuid>151d8040-493d-4afd-ae3b-0872f360964c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;_content&quot;)/div[1]/table[@class=&quot;alert_count&quot;]/tbody[1]/tr[1]/td[3]</value>
      <webElementGuid>1c25fc7d-ae89-44b8-9962-808e5a45d01c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>fac4fdaf-2a32-4789-9e45-a1f5b816a580</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Work needs grading  
                        
                        
                            
                            1 
                            
                            

                    </value>
      <webElementGuid>4091bd13-f6fa-484e-950e-a6c140f4be9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;_content&quot;)/div[1]/table[@class=&quot;alert_count&quot;]/tbody[1]/tr[1]/td[3]</value>
      <webElementGuid>1d02fbd9-6e08-42c8-92ab-e0a0876d6e71</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='_content']/div/table/tbody/tr/td[3]</value>
      <webElementGuid>e382bace-39b2-493c-9323-90ca4e37fbc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Update'])[1]/following::td[1]</value>
      <webElementGuid>7bee7acb-6008-4a9a-8133-c970f2b3e696</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View archived alerts'])[1]/preceding::td[6]</value>
      <webElementGuid>b909c928-2c8a-4f94-b71b-444e22800168</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]</value>
      <webElementGuid>e6967202-15fb-4125-a431-74f6d3a1b710</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                        
                        Work needs grading  
                        
                        
                            
                            1 
                            
                            

                    ' or . = '
                        
                        Work needs grading  
                        
                        
                            
                            1 
                            
                            

                    ')]</value>
      <webElementGuid>5638d514-782d-44f7-ad75-f6217fa641c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
