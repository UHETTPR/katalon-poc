<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Work needs grading                      _3a89d0</name>
   <tag></tag>
   <elementGuidId>7c20a4dd-6228-4088-b4d2-7a0197171ae7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                        
                        Work needs grading  
                        
                        
                            
                            0 
                            
                            

                    ' or . = '
                        
                        Work needs grading  
                        
                        
                            
                            0 
                            
                            

                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>1b5a0c4d-81f2-4def-9620-638f838f7a81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Work needs grading  
                        
                        
                            
                            0 
                            
                            

                    </value>
      <webElementGuid>8dbcbc6c-46f5-41b0-bfdd-405b22a28677</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;_content&quot;)/div[1]/table[@class=&quot;alert_count&quot;]/tbody[1]/tr[1]/td[3]</value>
      <webElementGuid>6392b52e-4d8b-4d38-a36e-12906de2124e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>bcad83f4-f2ca-4f84-bbc4-946d6a0e3aa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Work needs grading  
                        
                        
                            
                            0 
                            
                            

                    </value>
      <webElementGuid>1321e8c4-c084-4181-a6b1-900a4d76e2db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;_content&quot;)/div[1]/table[@class=&quot;alert_count&quot;]/tbody[1]/tr[1]/td[3]</value>
      <webElementGuid>e968710b-af11-4754-b6ac-2543936ebf1b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='_content']/div/table/tbody/tr/td[3]</value>
      <webElementGuid>88d46951-59dc-4fdf-993d-babc20dd8304</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Update'])[1]/following::td[1]</value>
      <webElementGuid>4029e669-cb38-47dc-bbbf-73d9da1aeefb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View archived alerts'])[1]/preceding::td[6]</value>
      <webElementGuid>bbe63257-37db-4821-8fa6-828c5ec69fc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]</value>
      <webElementGuid>96e143f2-1595-4ac4-aee4-14d43b50f9f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                        
                        Work needs grading  
                        
                        
                            
                            0 
                            
                            

                    ' or . = '
                        
                        Work needs grading  
                        
                        
                            
                            0 
                            
                            

                    ')]</value>
      <webElementGuid>fd27b7fb-be9d-4fe0-9d01-1a0058b0c964</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
