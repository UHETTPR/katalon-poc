<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Next Page_xlicon-arrow-last-page</name>
   <tag></tag>
   <elementGuidId>b792248a-70f7-40b5-a354-3cb7cdf828b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.xlicon-arrow-last-page</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='gbpglastpage']/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>86680d02-559f-4c00-9e7f-bcf9887fd577</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlicon-arrow-last-page</value>
      <webElementGuid>02ecd2bc-98cb-485f-b8a5-7002bd43c371</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9c586c98-871b-4683-9250-2742151e4589</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebook&quot;)/thead[1]/tr[@class=&quot;assignment-Pager-TR&quot;]/th[@class=&quot;assignment-pagination&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;gradebookpagercontainer&quot;]/ul[@class=&quot;gradebookpager&quot;]/li[2]/a[@id=&quot;gbpglastpage&quot;]/i[@class=&quot;xlicon-arrow-last-page&quot;]</value>
      <webElementGuid>1520ec40-e11e-4fc0-962b-a8f6b12cf540</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='gbpglastpage']/i</value>
      <webElementGuid>6ccf8314-961f-4cdd-b184-9c50e6fce3d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[4]/i</value>
      <webElementGuid>a698d781-54b5-45aa-8df8-4022cf331b3f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
