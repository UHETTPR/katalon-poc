<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Homework</name>
   <tag></tag>
   <elementGuidId>5764884f-ad9e-41f0-8f1f-6c0aa389be6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Homework')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2292678e-3cd9-424f-a240-7d7edc9ece06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView(Homework)</value>
      <webElementGuid>f33e27fd-b45b-40d8-a6a6-df68fbce9bf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Homework</value>
      <webElementGuid>8fa0df31-0f07-4523-93bd-3bce3fe1073e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/div[@class=&quot;xlbootstrap3&quot;]/app-root[1]/app-gradebook[1]/app-instructor-links[1]/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[1]/li[1]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[2]/a[1]</value>
      <webElementGuid>1c094eea-90de-431c-824d-c4341fbbf595</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]/app-root/app-gradebook/app-instructor-links/table/tbody/tr/td/ul/li/div/div[2]/ul/li[2]/a</value>
      <webElementGuid>131d0d13-0217-4da8-9446-7cd79fce5b24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Homework')])[2]</value>
      <webElementGuid>3414bef5-e7f4-4558-890c-29e1f8c63c82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[1]/following::a[1]</value>
      <webElementGuid>04999575-326a-4f51-994d-21e8e8aa3379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/following::a[2]</value>
      <webElementGuid>3ada6a0a-a530-4645-a495-19f7428830cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quizzes'])[1]/preceding::a[1]</value>
      <webElementGuid>44ceb1e9-3f0a-4344-86cf-8cc50f38811b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tests'])[1]/preceding::a[2]</value>
      <webElementGuid>d76ff3ba-1f77-4338-9404-138e353ce359</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Homework']/parent::*</value>
      <webElementGuid>351e8ab0-21e2-4a4b-b948-278d55682db5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:goView(Homework)')]</value>
      <webElementGuid>2f1b0d3f-8bec-4f3b-8a15-c0450bd667b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[2]/a</value>
      <webElementGuid>02f18c49-4675-4fe5-a6b9-e103be3f03d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:goView(Homework)' and (text() = 'Homework' or . = 'Homework')]</value>
      <webElementGuid>a379e00d-3382-4e62-a252-e2c7c196260b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
