<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign in</name>
   <tag></tag>
   <elementGuidId>5b1cad87-3140-4879-9d8f-f38372b814ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.button.button-big-icon.bg-color-match-medium</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ae362290-e1dc-4d1e-8a37-5bc87f3fbdf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')</value>
      <webElementGuid>64120f08-103f-4dab-968a-20b6c76ab336</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://www.mathxl.com/login_mxl.htm</value>
      <webElementGuid>34146ebd-cd55-4926-b54f-5bc90e81280a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium</value>
      <webElementGuid>92b13b1d-2f8d-4200-b5ab-d9b3615b9583</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>8742a382-d698-48f4-8a33-e7548713e748</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths cssfilters&quot;]/body[@class=&quot;has-announcement green has-two-or-less-featured-link-categories&quot;]/section[@class=&quot;hero hero--bg-img uses-mask mask-is--gradient-top random-image js--random-bg&quot;]/div[@class=&quot;wrapper no-col-stack @480.col-stack&quot;]/div[@class=&quot;col col-5&quot;]/div[@class=&quot;sign-in&quot;]/p[2]/a[@class=&quot;button button-big-icon bg-color-match-medium&quot;]</value>
      <webElementGuid>804493bd-2c3b-45dd-9764-86d0d1355379</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>005eefc8-b200-4182-9a40-2c1c18cf2c3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')</value>
      <webElementGuid>245d14e2-e65f-4b20-a80b-25544bc5ef51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://www.mathxl.com/login_mxl.htm</value>
      <webElementGuid>1cc760ee-625c-4091-b322-1d6d83037951</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium</value>
      <webElementGuid>5a9d1f8a-db72-43e8-835a-2a7ba073b507</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>804e3dde-6756-486e-aade-e9ac3ffa5ddf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths cssfilters&quot;]/body[@class=&quot;has-announcement green has-two-or-less-featured-link-categories&quot;]/section[@class=&quot;hero hero--bg-img uses-mask mask-is--gradient-top random-image js--random-bg&quot;]/div[@class=&quot;wrapper no-col-stack @480.col-stack&quot;]/div[@class=&quot;col col-5&quot;]/div[@class=&quot;sign-in&quot;]/p[2]/a[@class=&quot;button button-big-icon bg-color-match-medium&quot;]</value>
      <webElementGuid>1a0c3411-9070-4621-a961-74021e902bdc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      <webElementGuid>9160ed49-f639-424c-a070-d40438577dd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign in')]</value>
      <webElementGuid>0eef8fb4-1e0b-4233-a0e7-ca5943bb4b33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/following::a[1]</value>
      <webElementGuid>ba0f5380-fc9f-4453-97e1-80bc550dd75d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn more'])[1]/following::a[1]</value>
      <webElementGuid>e130487b-688f-415e-a24b-b056587b6ade</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot username or password?'])[1]/preceding::a[1]</value>
      <webElementGuid>61d0cf64-af4b-4a5a-a2db-1dfc489cfedb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MyLab Math/MyLab Statistics users, sign in here'])[1]/preceding::a[2]</value>
      <webElementGuid>5bff040a-3e43-426d-9cff-5808e7bb201b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>eaa4823a-12ab-440a-ae26-e88d55b0da4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://www.mathxl.com/login_mxl.htm']</value>
      <webElementGuid>9ea0ef84-11a3-4a99-a8ee-abd4e82f12e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/p[2]/a</value>
      <webElementGuid>de280370-8e83-40ab-b319-2166291c1f2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://www.mathxl.com/login_mxl.htm' and (text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>1fec10f6-5e2d-4eae-8538-10ccebfabd81</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
