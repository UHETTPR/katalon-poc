<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Gradebook</name>
   <tag></tag>
   <elementGuidId>24914ed9-c135-41f7-8bf2-0d4a95a15a14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='storeNavData(&quot;35&quot;)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7d61371b-3ed2-48aa-aeec-d0449e41fc74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/Gradebook.aspx</value>
      <webElementGuid>b9a8260e-b04b-46d3-9199-9f74094bf3cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>3b428204-0f08-4174-95d3-2b5bdf8ab25a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>b46f3db2-841b-4427-b76f-6a876961282b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;35&quot;)</value>
      <webElementGuid>cdc6868c-5c31-46cc-a9b0-a6ad67869253</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Gradebook</value>
      <webElementGuid>75c8b497-0beb-46ce-bcbc-52ee22994cc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Gradebook</value>
      <webElementGuid>09d447d6-c973-4855-9bf0-bdad30272daf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>d5a66d2a-c864-4ffa-a94a-6a45ae22a656</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d8b73e35-0bc9-4b59-b107-5c85e0a22991</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/Gradebook.aspx</value>
      <webElementGuid>a3a6afe0-c4d4-430b-a31c-268067ba4c1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>3726de5c-5c8a-43a1-abc7-9fe2c65d7744</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>4ce8365a-a0f7-43ff-8b78-3b07e837993f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;35&quot;)</value>
      <webElementGuid>d2436fcd-65a2-48a1-8c49-46ca20e1bee6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Gradebook</value>
      <webElementGuid>c718e65d-2fd3-4f23-8d71-0368cc5ad424</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Gradebook</value>
      <webElementGuid>dbf1a4a3-a1b6-47f0-924e-3042d4d9ac96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>72faa84f-48fb-490a-b7df-dedfea593ac5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;35&quot;)']</value>
      <webElementGuid>0b007a35-daed-4f30-9aae-1217bae18c43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[14]/a</value>
      <webElementGuid>9126c57f-a5b1-47b1-8550-c07a24e001b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Gradebook')]</value>
      <webElementGuid>5c9b0f32-9fc8-498f-b832-09e4de715803</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan Manager'])[1]/following::a[1]</value>
      <webElementGuid>9143c352-0c61-4504-b1e7-394682300db9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignment Manager'])[1]/following::a[2]</value>
      <webElementGuid>e8b4ca59-751f-4fd4-829e-fb0bda62f0fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HEDTestCourse [1]'])[1]/preceding::a[2]</value>
      <webElementGuid>eddc66a6-d71e-4bf3-8f2d-013cd386417b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manage Course List'])[1]/preceding::a[3]</value>
      <webElementGuid>8c74e3cd-8aa3-400f-ac97-989ce2570261</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Gradebook']/parent::*</value>
      <webElementGuid>fea4b6a4-0942-45ef-a68d-5caca98860d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/Gradebook.aspx')]</value>
      <webElementGuid>7644649f-954c-46b7-a562-8ba9a23303e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[14]/a</value>
      <webElementGuid>844ecebf-ca90-49c2-a6ea-3dc10c22b24a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/Gradebook.aspx' and @title = 'Gradebook' and (text() = 'Gradebook' or . = 'Gradebook')]</value>
      <webElementGuid>e37cad6e-4fca-422f-b7f0-b1a95177084e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
