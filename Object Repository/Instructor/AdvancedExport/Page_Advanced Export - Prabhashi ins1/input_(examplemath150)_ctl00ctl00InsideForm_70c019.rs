<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_(examplemath150)_ctl00ctl00InsideForm_70c019</name>
   <tag></tag>
   <elementGuidId>560bfe29-2305-47d3-beae-cf573f31dc09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_TextExportName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextExportName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e7aa9cd0-862b-45b9-bad1-21493989ddc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$TextExportName</value>
      <webElementGuid>9a750cf3-a84a-4d87-8bef-79b7032fe14c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>7528d57f-9772-4af9-8713-20c2ee3fc833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>150</value>
      <webElementGuid>b8a31289-806b-45d2-a44d-5f341b2ea459</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_TextExportName</value>
      <webElementGuid>1c03a31f-4f26-4c59-98da-f5564020ab96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>0bd82213-968c-41e3-a005-ee15e7a49043</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TextExportName&quot;)</value>
      <webElementGuid>8ac28521-a14b-400a-bdd6-321f406e394b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_TextExportName']</value>
      <webElementGuid>6643e4f7-9e16-44b8-858f-d586ccf5c74b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='gradebookexportoptions']/tr/td[2]/input</value>
      <webElementGuid>0b56008c-2af8-48c0-84a2-384be1808ffd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/input</value>
      <webElementGuid>dcefeb56-f554-4c40-8dad-b9b4125b55f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'ctl00$ctl00$InsideForm$MasterContent$TextExportName' and @type = 'text' and @id = 'ctl00_ctl00_InsideForm_MasterContent_TextExportName']</value>
      <webElementGuid>6f34a159-6072-463d-962c-6d5894567de1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
