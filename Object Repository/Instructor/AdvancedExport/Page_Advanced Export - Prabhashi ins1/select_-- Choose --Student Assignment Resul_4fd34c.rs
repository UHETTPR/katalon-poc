<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose --Student Assignment Resul_4fd34c</name>
   <tag></tag>
   <elementGuidId>4965ff5f-2b6c-4969-b13b-c12ed3c19868</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_DropDownListExportType</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_DropDownListExportType']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>03f5da89-8a76-4603-9943-5bb2c9c09dc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$DropDownListExportType</value>
      <webElementGuid>0b49ccbf-4515-437f-86e5-f258be8eae6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_DropDownListExportType</value>
      <webElementGuid>d1e24291-6df6-4ea2-a431-3786acf016d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>1cfd43f2-d105-4a40-a03b-5e673fe13a7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ChangeExportType();</value>
      <webElementGuid>5489c202-62bd-401e-8ccb-fe22c33c97ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	-- Choose --
	Student Assignment Results
	Student Performance by SBChapter
	Item Analysis

</value>
      <webElementGuid>9e27b3db-2cad-4239-92c2-eaf3080ca780</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_DropDownListExportType&quot;)</value>
      <webElementGuid>131d6bdf-2cd0-4b9a-817a-e18605c37f3f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_DropDownListExportType']</value>
      <webElementGuid>9d21625f-6bf6-4854-9ca5-81ef03d84a39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='gradebookexportoptions']/tr[2]/td[2]/select</value>
      <webElementGuid>4529751d-2a2e-4c78-8fc9-19aaff5d7b83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export Type'])[1]/following::select[1]</value>
      <webElementGuid>9d6652e6-f86d-4685-9497-73ab0f7734b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spreadsheet Layout'])[1]/preceding::select[1]</value>
      <webElementGuid>3d388a57-7726-4b97-881b-d0eecc3be8e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spreadsheet Layout'])[2]/preceding::select[2]</value>
      <webElementGuid>232b2280-789e-4d33-ab20-14499aaf046d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>fc51e455-22ce-42c3-afe3-4a5360fbda9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$DropDownListExportType' and @id = 'ctl00_ctl00_InsideForm_MasterContent_DropDownListExportType' and (text() = '
	-- Choose --
	Student Assignment Results
	Student Performance by SBChapter
	Item Analysis

' or . = '
	-- Choose --
	Student Assignment Results
	Student Performance by SBChapter
	Item Analysis

')]</value>
      <webElementGuid>22118f23-624b-40f3-afed-b2751fe95948</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
