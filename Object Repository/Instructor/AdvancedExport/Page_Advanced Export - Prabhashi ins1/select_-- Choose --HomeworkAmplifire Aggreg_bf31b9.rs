<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose --HomeworkAmplifire Aggreg_bf31b9</name>
   <tag></tag>
   <elementGuidId>06f1ae43-930b-494c-8ec0-678219ab7f1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_DropDownListAnalysisAssignment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_DropDownListAnalysisAssignment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>72c0c6ee-9de6-4b31-a046-447836f1108b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$DropDownListAnalysisAssignment</value>
      <webElementGuid>becc42fe-f5f3-4fae-ba86-1708bf43c210</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_DropDownListAnalysisAssignment</value>
      <webElementGuid>7f2fb801-0729-4048-9486-ed62e04dd470</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>ec65c4e7-a2df-42be-ac18-f29baf64b64e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ChangeAnalysisAssignment();</value>
      <webElementGuid>a946754e-a4c7-4f39-8033-73ba2bb453fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	-- Choose --
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

</value>
      <webElementGuid>6e2fccc3-ab0d-4438-9b47-2ac3ce9fc9dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_DropDownListAnalysisAssignment&quot;)</value>
      <webElementGuid>1e66ab22-f79a-4cd8-8f0a-ee33cfcef454</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_DropDownListAnalysisAssignment']</value>
      <webElementGuid>b2bbff8d-cb20-42f5-80c7-0903426080a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='trAnalysisAssignment']/td[2]/select</value>
      <webElementGuid>f6af7823-330e-4cd9-892b-915bcd508451</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignment'])[1]/following::select[1]</value>
      <webElementGuid>9d0e9770-6c41-4268-a355-9cc311d29845</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Content Area'])[1]/following::select[2]</value>
      <webElementGuid>03e8fb97-68e8-4335-bb2a-1dffcfd8ad2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item Associations'])[1]/preceding::select[1]</value>
      <webElementGuid>22cbc1b9-8f43-4731-b5b4-51a11ddd1396</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Students'])[1]/preceding::select[1]</value>
      <webElementGuid>731b597d-a211-4e50-a57f-b4b75670d793</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[8]/td[2]/select</value>
      <webElementGuid>7862b825-fd0b-4f6f-b20f-df97fe988f59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$DropDownListAnalysisAssignment' and @id = 'ctl00_ctl00_InsideForm_MasterContent_DropDownListAnalysisAssignment' and (text() = '
	-- Choose --
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

' or . = '
	-- Choose --
	***Homework***
	Amplifire Aggregate Supply and Aggregate Demand
	Amplifire Basic Principles of Economics
	Animation assignment PA
	media with question
	Sample IRA
	StatCrunch assignment PA
	Preassigned Homework
	TestingHomeworkAssignment
	***Quizzes***
	TestingQuizAssignment
	***Tests***
	Maintain Order, Preassigned
	TestingTestAssignment

')]</value>
      <webElementGuid>cb07a4ac-2a9f-41d0-a103-0a24cb26d83a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
