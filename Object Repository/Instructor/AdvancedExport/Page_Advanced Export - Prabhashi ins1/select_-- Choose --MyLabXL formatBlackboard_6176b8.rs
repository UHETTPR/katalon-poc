<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose --MyLabXL formatBlackboard_6176b8</name>
   <tag></tag>
   <elementGuidId>28fba218-99fe-4548-9979-f7ba4a4d464d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_DropDownListLayout</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_DropDownListLayout']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>caafcf36-f8ea-41a8-a4c5-e3351e13f072</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$DropDownListLayout</value>
      <webElementGuid>18f60eb3-2c3c-4430-bde5-00c28c74d251</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_DropDownListLayout</value>
      <webElementGuid>2acfc968-b1a2-476c-b41f-84f203ed56ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>8c5c829e-3bed-47ba-a558-5a1bd1c30e2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ChangeExportLayout();</value>
      <webElementGuid>9136cd20-378f-488f-b14b-034a1a7f2604</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	-- Choose --
	MyLab/XL format
	Blackboard 8/9 format
	Blackboard-MyLab/Mastering format
	Canvas format
	Desire2Learn and Moodle format

</value>
      <webElementGuid>c931d690-b864-4b77-abf8-6937a9984a5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_DropDownListLayout&quot;)</value>
      <webElementGuid>751620e0-a4f1-47a5-969e-82e7dcf8b780</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_DropDownListLayout']</value>
      <webElementGuid>70a59622-3307-49be-aaf0-d74667c62970</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='trSpreadsheetLayout']/td[2]/select</value>
      <webElementGuid>9e0142bc-09f0-4d2f-8f41-468fd5699a44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spreadsheet Layout'])[1]/following::select[1]</value>
      <webElementGuid>565c2693-6f78-46b9-8fd0-21b1b3593104</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export Type'])[1]/following::select[2]</value>
      <webElementGuid>1d79f1b0-278f-465a-b452-94d6608aeebb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spreadsheet Layout'])[2]/preceding::select[1]</value>
      <webElementGuid>9ff96a9e-0d99-42f3-b5c2-f4678cf197c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Summary only'])[1]/preceding::select[1]</value>
      <webElementGuid>6e7dfe6e-fda7-4d5b-9f8f-245af41c06c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[2]/select</value>
      <webElementGuid>f666dcb5-44e6-4ea6-8481-495eff08edf0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$DropDownListLayout' and @id = 'ctl00_ctl00_InsideForm_MasterContent_DropDownListLayout' and (text() = '
	-- Choose --
	MyLab/XL format
	Blackboard 8/9 format
	Blackboard-MyLab/Mastering format
	Canvas format
	Desire2Learn and Moodle format

' or . = '
	-- Choose --
	MyLab/XL format
	Blackboard 8/9 format
	Blackboard-MyLab/Mastering format
	Canvas format
	Desire2Learn and Moodle format

')]</value>
      <webElementGuid>0234608a-d747-42da-b9e0-13d53c0f408f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
