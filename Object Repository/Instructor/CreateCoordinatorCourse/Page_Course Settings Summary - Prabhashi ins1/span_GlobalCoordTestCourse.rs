<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_GlobalCoordTestCourse</name>
   <tag></tag>
   <elementGuidId>efb46cb8-b956-4516-9016-ab88814680ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_lblCourseNameHeader']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_lblCourseNameHeader</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>aa7089f2-14ab-4883-a361-078516fb17c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_lblCourseNameHeader</value>
      <webElementGuid>7cbaf6bd-5aac-4b97-bf0c-8d413dfce539</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>GlobalCoordTestCourse</value>
      <webElementGuid>34b302fd-afa9-4873-80d8-572c3e00ce9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_lblCourseNameHeader&quot;)</value>
      <webElementGuid>9a0e5d47-fec1-42c1-b1db-c1d4385c4199</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_lblCourseNameHeader']</value>
      <webElementGuid>92b75f27-7cb8-4227-8850-54ba42e940c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='CourseDetailsTable']/tbody/tr/td[2]/span</value>
      <webElementGuid>d56daf3f-b632-4726-b054-cf9769ffb165</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course name'])[1]/following::span[1]</value>
      <webElementGuid>3932d316-7b24-4983-b72f-e35a78f6a6fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::span[1]</value>
      <webElementGuid>2d616603-c187-457f-86cf-0f85de4dc151</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course ID'])[1]/preceding::span[1]</value>
      <webElementGuid>f4b6f401-83e1-4e58-92a5-a24d19d267e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL01-H146-50L8-61X4'])[1]/preceding::span[1]</value>
      <webElementGuid>05a2208b-d9e3-4675-a3cf-e7b12b7ec78f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/span</value>
      <webElementGuid>f5c8e11a-a34b-4675-962a-ce7c8e26b05c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ctl00_ctl00_InsideForm_MasterContent_lblCourseNameHeader' and (text() = 'GlobalCoordTestCourse' or . = 'GlobalCoordTestCourse')]</value>
      <webElementGuid>94f1512e-9c27-4e4b-86ed-e73bc37363ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
