<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Course Settings Summary</name>
   <tag></tag>
   <elementGuidId>d85dbea1-117f-4c4e-87fd-4f049c252907</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2[@id='PageHeaderTitle']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#PageHeaderTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>a8f64a16-80d1-45eb-b8f2-0436185ff2b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PageHeaderTitle</value>
      <webElementGuid>43a44c5a-48aa-47e7-a124-0844d54d48c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>integrationheaderstyle PageHeaderTitle header-component-float-left header-font-size</value>
      <webElementGuid>c1aee0da-8924-4bbe-9e29-7c33817b4efc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Course Settings Summary</value>
      <webElementGuid>c94dbd0f-6b37-4ac6-b2d8-089309083287</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PageHeaderTitle&quot;)</value>
      <webElementGuid>7a9f7ca0-633a-4705-a0e5-59755789c6d3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h2[@id='PageHeaderTitle']</value>
      <webElementGuid>08891d09-ccf4-44b2-a4a2-ceb3bfc982f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div/div/div[2]/h2</value>
      <webElementGuid>97311322-cde9-4b0c-bc19-69bfe9b5e866</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MML Global Test Book'])[1]/following::h2[1]</value>
      <webElementGuid>2bd8b588-7e34-43b1-9a60-ac1d6c5a6af8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prabhashi ins1'])[3]/following::h2[1]</value>
      <webElementGuid>3c089a4d-1161-4044-93f3-5ea761fff7fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/preceding::h2[1]</value>
      <webElementGuid>f8fcf3ef-a3e2-4043-94db-10eaabf2d50f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[3]/preceding::h2[1]</value>
      <webElementGuid>d2b93937-6ea0-4477-908e-8ab13f77772a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Course Settings Summary']/parent::*</value>
      <webElementGuid>2847ef1c-d636-43a5-a0e1-21d1fb47c3b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>23caa259-e090-400c-ae5e-4b1509d5bbed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[@id = 'PageHeaderTitle' and (text() = '
            Course Settings Summary' or . = '
            Course Settings Summary')]</value>
      <webElementGuid>76d8e582-f783-4230-a12d-475531dd277a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
