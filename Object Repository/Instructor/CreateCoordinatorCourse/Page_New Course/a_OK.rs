<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_OK</name>
   <tag></tag>
   <elementGuidId>9f393bf7-d94d-4f14-bb85-847a379b4b74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='okButton0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#okButton0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>c3f990f2-45eb-486e-aac4-7911403518eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#!</value>
      <webElementGuid>46784a36-3419-4b86-a37a-05a32fe530c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>okButton0</value>
      <webElementGuid>5fbac8ad-08de-4c13-94c5-d971d748c0de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>b1507b3a-a7a7-4d66-b3b4-b3dade9ea629</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>3397887d-127f-473f-a80a-f1cdcdd7768d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;okButton0&quot;)</value>
      <webElementGuid>b607642f-efdf-497a-b826-c90d7198e041</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='okButton0']</value>
      <webElementGuid>cdd408b8-cf44-441b-adaa-cdb6f000746d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'OK')])[2]</value>
      <webElementGuid>74d1d018-7e0a-4861-9876-b9ccfa22c76e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/following::a[1]</value>
      <webElementGuid>f696272c-fb69-47c5-b710-1512d14855e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'okButton0', '&quot;', ')')])[1]/following::a[2]</value>
      <webElementGuid>0ca0b7f2-f15e-423a-bf86-8576765dc592</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#!')])[2]</value>
      <webElementGuid>f21d8676-bc86-439d-aa8d-f6d06653437a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a[2]</value>
      <webElementGuid>032cd605-85fc-4c84-a3e7-9d870758ca20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#!' and @id = 'okButton0' and (text() = 'OK' or . = 'OK')]</value>
      <webElementGuid>443f78e5-74aa-41bc-98f1-55f491601400</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
