<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose Book --  AdamsEssex Calcul_16ae9d</name>
   <tag></tag>
   <elementGuidId>62967f34-3536-4610-9cf2-d8e48d7b7c44</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>469659e5-326a-4c5a-9221-eac1139add91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpBookList</value>
      <webElementGuid>c6444081-2151-4bdf-8252-990bb3544414</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpBookList</value>
      <webElementGuid>e1b5939c-8b05-439c-ac7c-5e77ab2e923c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>UpdateBookCover();</value>
      <webElementGuid>3344f6c1-5007-4b9b-bb90-d9aae71486fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>8c26320b-9bec-4157-a065-7f287673c716</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				-- Choose Book --  
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Aston Uni Barrow Custom MyLab Math Global 2019
				Barrow: Statistics for Economics, Accounting and Business Studies, 6e
				Berenson: Basic Business Statistics, 4e
				Bryson/Willis, Foundation Maths for Biosciences
				Cicchitelli, D'Urso, Minozzo: Statistics
				Croft, Foundation Maths 6e - DSP
				Custom MyMathLab Global for UWA (2015)
				eT1 Global Test Book
				eT2 Global Test Book
				Introduction to mathematics
				IPRO-Irv-XLTestbk1
				Jacques, Mathematics for Economics and Business 9e
				Kemp, Royal Holloway Custom MyLab Math 2019, 1e
				La Trobe University: BUS1BAN Business Analytics
				Levine: Business Statistics: A First Course, 6e: International Edition
				Levine: Statistics for Managers: Using Microsoft Excel, 7e, Global Edition
				MML Global Test Book
				Mowbray: Mathematics for Physicists and Astronomers
				MyMathLab test book A2
				Newbold: Statistics for Business and Economics: Global Edition, 8e
				NextGen Media Math Test BookRH
				Roehampton University: Maths in the Life Sciences
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Shihab, Numeracy in Nursing and Healthcare 1e - EMA
				Swansea University: Wedlake Custom MLG 2018
				Sydsæter, Hammond, Strøm: Essential Maths for Economic Analysis, 4e

			</value>
      <webElementGuid>05edfd5f-fc57-459f-bbfc-5fac6d73f6a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpBookList&quot;)</value>
      <webElementGuid>be9ab3d5-e018-42e9-941f-3bc982d4c84a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpBookList']</value>
      <webElementGuid>9f952355-2aa3-4378-b4df-7750b54293c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep1NewCourse']/select</value>
      <webElementGuid>e3e1fe33-c2ff-4c97-8301-2702ea394901</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy a course by specifying its Course ID'])[1]/following::select[1]</value>
      <webElementGuid>94812cdb-4de3-4048-8f9b-df5208048e77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a course to copy'])[1]/preceding::select[2]</value>
      <webElementGuid>0054055c-a04f-4717-97e0-231b361121f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>278f24ce-a19d-4243-813c-1d3527cc5fcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpBookList' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpBookList' and (text() = concat(&quot;
				-- Choose Book --  
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Aston Uni Barrow Custom MyLab Math Global 2019
				Barrow: Statistics for Economics, Accounting and Business Studies, 6e
				Berenson: Basic Business Statistics, 4e
				Bryson/Willis, Foundation Maths for Biosciences
				Cicchitelli, D&quot; , &quot;'&quot; , &quot;Urso, Minozzo: Statistics
				Croft, Foundation Maths 6e - DSP
				Custom MyMathLab Global for UWA (2015)
				eT1 Global Test Book
				eT2 Global Test Book
				Introduction to mathematics
				IPRO-Irv-XLTestbk1
				Jacques, Mathematics for Economics and Business 9e
				Kemp, Royal Holloway Custom MyLab Math 2019, 1e
				La Trobe University: BUS1BAN Business Analytics
				Levine: Business Statistics: A First Course, 6e: International Edition
				Levine: Statistics for Managers: Using Microsoft Excel, 7e, Global Edition
				MML Global Test Book
				Mowbray: Mathematics for Physicists and Astronomers
				MyMathLab test book A2
				Newbold: Statistics for Business and Economics: Global Edition, 8e
				NextGen Media Math Test BookRH
				Roehampton University: Maths in the Life Sciences
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Shihab, Numeracy in Nursing and Healthcare 1e - EMA
				Swansea University: Wedlake Custom MLG 2018
				Sydsæter, Hammond, Strøm: Essential Maths for Economic Analysis, 4e

			&quot;) or . = concat(&quot;
				-- Choose Book --  
				Adams/Essex: Calculus: A Complete Course, Eighth Edition 
				Aston Uni Barrow Custom MyLab Math Global 2019
				Barrow: Statistics for Economics, Accounting and Business Studies, 6e
				Berenson: Basic Business Statistics, 4e
				Bryson/Willis, Foundation Maths for Biosciences
				Cicchitelli, D&quot; , &quot;'&quot; , &quot;Urso, Minozzo: Statistics
				Croft, Foundation Maths 6e - DSP
				Custom MyMathLab Global for UWA (2015)
				eT1 Global Test Book
				eT2 Global Test Book
				Introduction to mathematics
				IPRO-Irv-XLTestbk1
				Jacques, Mathematics for Economics and Business 9e
				Kemp, Royal Holloway Custom MyLab Math 2019, 1e
				La Trobe University: BUS1BAN Business Analytics
				Levine: Business Statistics: A First Course, 6e: International Edition
				Levine: Statistics for Managers: Using Microsoft Excel, 7e, Global Edition
				MML Global Test Book
				Mowbray: Mathematics for Physicists and Astronomers
				MyMathLab test book A2
				Newbold: Statistics for Business and Economics: Global Edition, 8e
				NextGen Media Math Test BookRH
				Roehampton University: Maths in the Life Sciences
				Savage: University of Portsmouth Custom-Foundation Mathematics 1E
				Shihab, Numeracy in Nursing and Healthcare 1e - EMA
				Swansea University: Wedlake Custom MLG 2018
				Sydsæter, Hammond, Strøm: Essential Maths for Economic Analysis, 4e

			&quot;))]</value>
      <webElementGuid>0435b307-ef84-43f1-bcaf-479e82393506</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
