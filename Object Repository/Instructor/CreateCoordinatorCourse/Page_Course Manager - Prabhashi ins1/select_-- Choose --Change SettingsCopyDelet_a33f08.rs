<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_-- Choose --Change SettingsCopyDelet_a33f08</name>
   <tag></tag>
   <elementGuidId>3dd651e8-fc83-479d-96e4-19160bed5632</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68837']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68837</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>31306aa9-acca-4271-9080-25c1e53315b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$gridCourses$ctl04$da68837</value>
      <webElementGuid>b8848791-39e8-4532-aa14-2e48447528f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68837</value>
      <webElementGuid>fb0cf5eb-21c0-43e7-b34b-618b9b93d6bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>actionmenu</value>
      <webElementGuid>a0186b69-4d1a-40c7-a840-0d5d371de0e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				Member List
				--------------------------------
				Browser Check

			</value>
      <webElementGuid>2c7231ab-b8cb-4d38-929b-f5fa35d4450e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68837&quot;)</value>
      <webElementGuid>64c0633c-6940-438e-8dee-e591c6c61b68</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68837']</value>
      <webElementGuid>6071fcb8-71dc-4f27-a27c-a928fffbfba9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridCourses']/tbody/tr[4]/td[9]/select</value>
      <webElementGuid>5bd07100-30cb-491a-9e2c-3c616b19aef9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ins1 (Primary)'])[2]/following::select[1]</value>
      <webElementGuid>49669a0f-7879-4a15-9284-9b92abc7cd1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL01-H145-50L8-51X4'])[1]/following::select[1]</value>
      <webElementGuid>740f2aa9-ffc6-4524-8849-6e265936db6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go'])[2]/preceding::select[1]</value>
      <webElementGuid>3231711a-1fa7-466e-8fa2-339a4a5cc5a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GlobalTestCourse'])[1]/preceding::select[1]</value>
      <webElementGuid>936c0623-5156-4c1e-9bad-a570a13d0539</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[9]/select</value>
      <webElementGuid>0ecf5cd0-a30f-4ca7-93c0-3f99df578329</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$gridCourses$ctl04$da68837' and @id = 'ctl00_ctl00_InsideForm_MasterContent_gridCourses_ctl04_da68837' and (text() = '
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				Member List
				--------------------------------
				Browser Check

			' or . = '
				-- Choose --
				Change Settings
				Copy
				Delete
				--------------------------------
				Create Assignments
				Create Announcements
				View Course Grades
				Member List
				--------------------------------
				Browser Check

			')]</value>
      <webElementGuid>76fef8bb-fc89-4e7c-b9ab-0218eb54ccd7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
