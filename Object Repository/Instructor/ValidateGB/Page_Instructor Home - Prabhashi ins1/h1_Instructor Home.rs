<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Instructor Home</name>
   <tag></tag>
   <elementGuidId>300ac1fc-5138-40f8-b59a-79757de25b8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div[2]/div/header/div[2]/div/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-6 > h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>c2dd4733-4951-4ec4-bcbb-d01f774c4b74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Instructor Home</value>
      <webElementGuid>f1229693-6cc2-4ca0-942f-90b03c7b35ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/header[1]/div[@class=&quot;pilot row&quot;]/div[@class=&quot;col-xs-6&quot;]/h1[1]</value>
      <webElementGuid>d03ac4d6-0711-4745-b3d7-367fdbf59d40</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/header/div[2]/div/h1</value>
      <webElementGuid>2cab8443-9f24-423b-83b7-276cf9b09aa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MML Global Test Book'])[1]/following::h1[1]</value>
      <webElementGuid>2004994b-e0c5-47ed-9877-9d5cb659ebc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prabhashi ins1'])[3]/following::h1[1]</value>
      <webElementGuid>820afa4f-c714-46bb-92a0-04da270b4bca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Student Home'])[1]/preceding::h1[1]</value>
      <webElementGuid>27399994-9de5-4b23-8bdb-b4bed84674ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue to My Course'])[1]/preceding::h1[1]</value>
      <webElementGuid>283d3d77-fa84-4837-8801-cc64a4a9c5bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h1</value>
      <webElementGuid>46526778-7405-4031-b74d-b55987b280a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      <webElementGuid>1c349aef-3998-4096-8f5e-298fcf3c80ca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
