<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>th_TestingHomeworkAssignment</name>
   <tag></tag>
   <elementGuidId>0b5be2c3-0284-4958-829e-a55d9799c5fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[@title = 'TestingHomeworkAssignment' and (text() = 'TestingHomeworkAssignment' or . = 'TestingHomeworkAssignment')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
      <webElementGuid>26284281-2e58-436a-87c5-453751bb7321</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sortableColumnHeader</value>
      <webElementGuid>056bfc09-8a5c-4277-b5c1-5d9cd499a0da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>TestingHomeworkAssignment</value>
      <webElementGuid>c16cd81f-4b4c-455c-996d-b2150919aa63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>this.blur(); return SortTableColumn('gbbody',5,false,5,null,'','alternatingRow', this);</value>
      <webElementGuid>5d072893-3050-4a67-9817-692c97d54a0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestingHomeworkAssignment</value>
      <webElementGuid>29920f64-41f3-41e4-a438-b388d050390c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebook&quot;)/thead[1]/tr[2]/th[@class=&quot;sortableColumnHeader&quot;]</value>
      <webElementGuid>e8617340-b88b-4492-8ae0-7f764819176c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//th[@onclick=&quot;this.blur(); return SortTableColumn('gbbody',5,false,5,null,'','alternatingRow', this);&quot;]</value>
      <webElementGuid>8c08d495-2a49-4b1c-8828-46ea5a4aaa85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='gradebook']/thead/tr[2]/th[6]</value>
      <webElementGuid>9a2eddd3-99e7-4697-aef3-a53b419abdd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preassigned Homework'])[1]/following::th[1]</value>
      <webElementGuid>0b36923f-fd56-49f7-92f4-17e8f3f2247b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Maintain Order, Preassigned'])[1]/following::th[2]</value>
      <webElementGuid>4ce0d333-fe80-4b19-9ff8-3d88fb2add2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingQuizAssignment'])[1]/preceding::th[1]</value>
      <webElementGuid>19b05c44-c6ac-4ca8-9ed0-14927a94e7c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[6]</value>
      <webElementGuid>bf1d6f32-abd1-4ccb-a47d-d1e2ba2326ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[@title = 'TestingHomeworkAssignment' and (text() = 'TestingHomeworkAssignment' or . = 'TestingHomeworkAssignment')]</value>
      <webElementGuid>e5aac2a6-927f-426a-aa10-f4e37134605d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
