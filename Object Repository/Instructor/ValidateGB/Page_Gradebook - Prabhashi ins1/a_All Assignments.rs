<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_All Assignments</name>
   <tag></tag>
   <elementGuidId>dcaa65d2-17dc-4fb8-81c6-dde5b07676fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > ul > li > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'All Assignments')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3d5b8c2b-8b07-4731-b6cc-52f8c57047be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView(AllAssignments);</value>
      <webElementGuid>d89afa55-eed7-41c6-bd70-0f5311f4de29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Assignments</value>
      <webElementGuid>eb3c933e-de77-462e-8b9b-050b09298ff8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;instructorlinks&quot;)/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[1]/li[2]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[1]/a[1]</value>
      <webElementGuid>b19d2b36-f791-4240-b85e-ac694c2b8db6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='instructorlinks']/table/tbody/tr/td/ul/li[2]/div/div[2]/ul/li/a</value>
      <webElementGuid>3d56d556-80a1-438b-ba38-66e43d73eff3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'All Assignments')]</value>
      <webElementGuid>73cc6983-4fa5-47f8-8798-35bbe299abae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/following::a[1]</value>
      <webElementGuid>0245a136-cac7-4ac9-9a1b-226e8c8a7b31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Path'])[1]/following::a[2]</value>
      <webElementGuid>3f55af8c-3f3f-431d-88ed-dd0c0e01173a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>0f7b0d4b-79b8-4165-b663-5c7d6558c416</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quizzes'])[1]/preceding::a[2]</value>
      <webElementGuid>a0bc11cb-5004-4535-af21-50e15278ed4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Assignments']/parent::*</value>
      <webElementGuid>80295355-e8b8-425b-993d-d0cec1f7964c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:goView(AllAssignments);')]</value>
      <webElementGuid>3148ca47-a12b-4fc5-995c-287ce7067793</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>c8a3bbe9-be0b-4236-a6cd-2e4bcc25a2f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:goView(AllAssignments);' and (text() = 'All Assignments' or . = 'All Assignments')]</value>
      <webElementGuid>34f97a37-4816-4d2f-a56a-e815762a66fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
