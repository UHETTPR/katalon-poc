<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>th_Show class metrics</name>
   <tag></tag>
   <elementGuidId>5a8eacd5-c930-43a3-a935-1451e97d4941</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='gradebook']/thead/tr/th</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>th</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
      <webElementGuid>389d9af1-3223-4456-a611-df46ed205286</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>77210d6b-bd07-4237-a56e-1ef43b5f9891</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            
                                
                                
                                    Show class metrics
                            
                        
                    </value>
      <webElementGuid>15aba619-c2b0-4438-b070-5486e02e1510</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebook&quot;)/thead[1]/tr[@class=&quot;assignment-Pager-TR&quot;]/th[1]</value>
      <webElementGuid>9fa1d2bb-6b4f-45c2-bb35-9525613fab7d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='gradebook']/thead/tr/th</value>
      <webElementGuid>caf531a2-56e1-4f42-ae15-dfccf9f54a73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[2]/following::th[1]</value>
      <webElementGuid>993835a1-899c-4d9e-93ff-e5c9e08af187</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[1]/following::th[1]</value>
      <webElementGuid>e780f46c-743c-4ad7-afff-f4b10fa9b49b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enrolment List'])[1]/preceding::th[2]</value>
      <webElementGuid>59d120ad-8ac3-49eb-be5e-8bfc9732a4c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th</value>
      <webElementGuid>e955c80d-f177-4740-ad6e-a4f7d3bdf7c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[(text() = '
                        
                            
                                
                                
                                    Show class metrics
                            
                        
                    ' or . = '
                        
                            
                                
                                
                                    Show class metrics
                            
                        
                    ')]</value>
      <webElementGuid>23c5e0f6-994b-431f-8182-568e2c6c8c95</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
