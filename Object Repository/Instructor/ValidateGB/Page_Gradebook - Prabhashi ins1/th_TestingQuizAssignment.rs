<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>th_TestingQuizAssignment</name>
   <tag></tag>
   <elementGuidId>136648c4-8a39-45b7-855d-e4c39a4128d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[@title = 'TestingQuizAssignment' and (text() = 'TestingQuizAssignment' or . = 'TestingQuizAssignment')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
      <webElementGuid>488d28b8-0293-42a1-92ed-edffe2f4603e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sortableColumnHeader</value>
      <webElementGuid>584f6673-2682-4b95-a2f9-4a630fd65403</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>TestingQuizAssignment</value>
      <webElementGuid>1f50367c-b3a9-4ac0-8efe-6e3d7d8695bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>this.blur(); return SortTableColumn('gbbody',6,false,6,null,'','alternatingRow', this);</value>
      <webElementGuid>4d7fef6a-9263-452c-847d-cd347d3a4dbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestingQuizAssignment</value>
      <webElementGuid>1dc867fa-4465-4cdd-93df-0888f95a73ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebook&quot;)/thead[1]/tr[2]/th[@class=&quot;sortableColumnHeader&quot;]</value>
      <webElementGuid>00273fe1-10fa-43ff-887f-3ee776f4f76a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//th[@onclick=&quot;this.blur(); return SortTableColumn('gbbody',6,false,6,null,'','alternatingRow', this);&quot;]</value>
      <webElementGuid>e06349c7-39a5-4f2e-9c57-481eabf45b34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='gradebook']/thead/tr[2]/th[7]</value>
      <webElementGuid>958b4ea1-de64-4526-b46d-d5c399855861</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingHomeworkAssignment'])[1]/following::th[1]</value>
      <webElementGuid>d715e24c-06d2-44f1-a97e-f7a1f2e2402e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preassigned Homework'])[1]/following::th[2]</value>
      <webElementGuid>6101dc9d-cd5c-439a-a015-9d9252379354</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingTestAssignment'])[1]/preceding::th[1]</value>
      <webElementGuid>3367b590-da21-4fe2-99a0-1af7c1dd5cca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[7]</value>
      <webElementGuid>60783d05-2264-4f80-a890-7be9ff96cf13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[@title = 'TestingQuizAssignment' and (text() = 'TestingQuizAssignment' or . = 'TestingQuizAssignment')]</value>
      <webElementGuid>7449d317-f16f-4e35-adcd-7a00346c2fe0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
