<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Instructor Home</name>
   <tag></tag>
   <elementGuidId>b0d3e347-06cd-4cec-a425-05576a1081f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-6 > h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div[2]/div/header/div[2]/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>7852e7b3-8e10-4b3d-b749-be64630ea89f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Instructor Home</value>
      <webElementGuid>036a0685-8f7a-4135-bc9a-c9fb250e52e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/header[1]/div[@class=&quot;pilot row&quot;]/div[@class=&quot;col-xs-6&quot;]/h1[1]</value>
      <webElementGuid>428090ca-dbfd-4597-8b26-eca77f6e06cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/header/div[2]/div/h1</value>
      <webElementGuid>ddc787e1-de62-4794-ac00-3af2427845fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MML Global Test Book'])[1]/following::h1[1]</value>
      <webElementGuid>de87dd51-1e35-4008-9405-c27c3a7db13c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hiranthi Insnew'])[3]/following::h1[1]</value>
      <webElementGuid>89e3f8bd-0d9b-4655-8c85-d69667a425a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Student Home'])[1]/preceding::h1[1]</value>
      <webElementGuid>52eccdb8-f909-4ab2-a633-3bbd495d9a99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue to My Course'])[1]/preceding::h1[1]</value>
      <webElementGuid>af5b9b39-6529-4384-861d-c8b360a18ca1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h1</value>
      <webElementGuid>bbd62384-6a8b-4693-b9ee-8d05a18f7c00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Instructor Home' or . = 'Instructor Home')]</value>
      <webElementGuid>762d039c-0ef7-48d1-a691-cbc67126bb72</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
