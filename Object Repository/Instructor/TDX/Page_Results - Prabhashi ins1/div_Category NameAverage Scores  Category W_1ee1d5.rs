<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Category NameAverage Scores  Category W_1ee1d5</name>
   <tag></tag>
   <elementGuidId>5a0d4c8a-48d7-4bf7-b208-b2a01f928aad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='overallScoreCalculation']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#overallScoreCalculation</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row collapse in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>overallScoreCalculation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>showHideCalculationLink</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            
		
			
				
					Category NameAverage Scores  ×Category Weight  =Points EarnedTime Spent
				
			
				
					 Homework75 %30 pts22.50  pts&lt;1min
				
					 Quiz50 %20 pts10.00  pts&lt;1min
				
					 Test100 %50 pts50.00  pts&lt;1min
				
			
				
					 Total100 pts82.50  pts1min
				
			
		
	
                        
                        
                            Your Overall Score is a weighted average calculated using your average score for each category and the category's weight in points.
                        
                        
                        
                    
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;overallScoreCalculation&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='overallScoreCalculation']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='overallScoreContainer']/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hide Calculation'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Percent'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'overallScoreCalculation' and (text() = concat(&quot;
                    
                        
                            
		
			
				
					Category NameAverage Scores  ×Category Weight  =Points EarnedTime Spent
				
			
				
					 Homework75 %30 pts22.50  pts&lt;1min
				
					 Quiz50 %20 pts10.00  pts&lt;1min
				
					 Test100 %50 pts50.00  pts&lt;1min
				
			
				
					 Total100 pts82.50  pts1min
				
			
		
	
                        
                        
                            Your Overall Score is a weighted average calculated using your average score for each category and the category&quot; , &quot;'&quot; , &quot;s weight in points.
                        
                        
                        
                    
                    
                &quot;) or . = concat(&quot;
                    
                        
                            
		
			
				
					Category NameAverage Scores  ×Category Weight  =Points EarnedTime Spent
				
			
				
					 Homework75 %30 pts22.50  pts&lt;1min
				
					 Quiz50 %20 pts10.00  pts&lt;1min
				
					 Test100 %50 pts50.00  pts&lt;1min
				
			
				
					 Total100 pts82.50  pts1min
				
			
		
	
                        
                        
                            Your Overall Score is a weighted average calculated using your average score for each category and the category&quot; , &quot;'&quot; , &quot;s weight in points.
                        
                        
                        
                    
                    
                &quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
