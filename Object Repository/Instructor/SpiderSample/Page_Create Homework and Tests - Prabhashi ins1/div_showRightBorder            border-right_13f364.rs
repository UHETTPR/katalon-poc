<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_showRightBorder            border-right_13f364</name>
   <tag></tag>
   <elementGuidId>595af3a2-bbb6-4d25-b0a3-b1efe8556af2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b1efb800-d82d-48df-9b3c-24f4d77738aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG</value>
      <webElementGuid>7aeb2c6b-75c7-4bfe-99af-2ec0275e017f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook','');
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode','');
    }


		
        
    
	

    
        
            
			
					
					    
						    Name
						    TestQuiz
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
	SBChapter 
	
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	


	SBSection 
	
		All SBS
		SBSection 1.1: This is a very long section title.  This is a very long section title....
		SBSection 1.2: Exponents, Order of Operations, and Inequality
		SBSection 1.3: Variables, Expressions, and Equations
		SBSection 1.4: Real Numbers and the Number Line
		SBSection 1.5: Adding and Subtracting Real Numbers
		SBSection 1.6: Multiplying and Dividing Real Numbers
		SBSection 1.7: Properties of Real Numbers
		SBSection 1.8: Simplifying Expressions

	


	SBObjective 
	
		All SBO
		Evaluate algebraic expressions.
		Write word phrases as algebraic expressions.
		Decide whether a number is a solution of an equation.
		Write word statements as equations.
		Identify as an expression or an equation.

	



                        
                        
	
                               
                            


	Availability
	
                                
		All questions
		Questions that are not in the Study Plan
		Questions that are in the Study Plan
		Questions that are screenreader accessible

	
                            



                        
                        
						
                    
                    																														
                
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   

		
			
			
				
					
						
						
						
						
							Available Questions
						
						 
						(25)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 0My Selections (0)
							
							Pooling options
							    
							
								View question details
								
							
							
							
					
					
						
							
							Points: 0

							
							
							#
							

							
								
									
									
								Question ID

								
									
								
							
							
								
									SBObjective
									
								
								 
							Estimated time:0s
							
							
					
				
			
	
		
		
			
			
				
					(P) 1.3.13(P) 1.3.15(P) 1.3.17(P) 1.3.19(P) 1.3.23(P) 1.3.25(P) 1.3.29(P) 1.3.31(P) 1.3.33(P) 1.3.35(P) 1.3.37(P) 1.3.39(P) 1.3.43(P) 1.3.45(P) 1.3.47(P) 1.3.49(P) 1.3.51(P) 1.3.53(P) 1.3.59(P) 1.3.61(P) 1.3.63(P) 1.3.65(P) 1.3.73(P) 1.3.75(P) 1.3.77
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					
					
					
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student test
		
							View student quiz
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	(P) 1.3.47 (Write word phrases as algebraic expressions.)
	
</value>
      <webElementGuid>ac6508ff-86f5-4df3-ab15-c7ddedc6835d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG&quot;)</value>
      <webElementGuid>444976cf-60e4-42e8-baa0-09b5397991b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG']</value>
      <webElementGuid>49ff2071-5fcb-408e-9514-d6b36f67c713</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivPage2']/div[2]</value>
      <webElementGuid>19f42559-8217-424a-9c73-0ec88d82297e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[2]/following::div[1]</value>
      <webElementGuid>8873b867-c7d5-4027-b74d-482cc1632774</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[2]/following::div[1]</value>
      <webElementGuid>28a06261-f295-4d69-8eb2-421b9c11547b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]</value>
      <webElementGuid>86e9fa7e-971a-485b-9acc-4a457d9e1c2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG' and (text() = concat(&quot;
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }


		
        
    
	

    
        
            
			
					
					    
						    Name
						    TestQuiz
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
	SBChapter 
	
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	


	SBSection 
	
		All SBS
		SBSection 1.1: This is a very long section title.  This is a very long section title....
		SBSection 1.2: Exponents, Order of Operations, and Inequality
		SBSection 1.3: Variables, Expressions, and Equations
		SBSection 1.4: Real Numbers and the Number Line
		SBSection 1.5: Adding and Subtracting Real Numbers
		SBSection 1.6: Multiplying and Dividing Real Numbers
		SBSection 1.7: Properties of Real Numbers
		SBSection 1.8: Simplifying Expressions

	


	SBObjective 
	
		All SBO
		Evaluate algebraic expressions.
		Write word phrases as algebraic expressions.
		Decide whether a number is a solution of an equation.
		Write word statements as equations.
		Identify as an expression or an equation.

	



                        
                        
	
                               
                            


	Availability
	
                                
		All questions
		Questions that are not in the Study Plan
		Questions that are in the Study Plan
		Questions that are screenreader accessible

	
                            



                        
                        
						
                    
                    																														
                
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   

		
			
			
				
					
						
						
						
						
							Available Questions
						
						 
						(25)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 0My Selections (0)
							
							Pooling options
							    
							
								View question details
								
							
							
							
					
					
						
							
							Points: 0

							
							
							#
							

							
								
									
									
								Question ID

								
									
								
							
							
								
									SBObjective
									
								
								 
							Estimated time:0s
							
							
					
				
			
	
		
		
			
			
				
					(P) 1.3.13(P) 1.3.15(P) 1.3.17(P) 1.3.19(P) 1.3.23(P) 1.3.25(P) 1.3.29(P) 1.3.31(P) 1.3.33(P) 1.3.35(P) 1.3.37(P) 1.3.39(P) 1.3.43(P) 1.3.45(P) 1.3.47(P) 1.3.49(P) 1.3.51(P) 1.3.53(P) 1.3.59(P) 1.3.61(P) 1.3.63(P) 1.3.65(P) 1.3.73(P) 1.3.75(P) 1.3.77
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					
					
					
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student test
		
							View student quiz
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	(P) 1.3.47 (Write word phrases as algebraic expressions.)
	
&quot;) or . = concat(&quot;
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }


		
        
    
	

    
        
            
			
					
					    
						    Name
						    TestQuiz
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
	SBChapter 
	
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	


	SBSection 
	
		All SBS
		SBSection 1.1: This is a very long section title.  This is a very long section title....
		SBSection 1.2: Exponents, Order of Operations, and Inequality
		SBSection 1.3: Variables, Expressions, and Equations
		SBSection 1.4: Real Numbers and the Number Line
		SBSection 1.5: Adding and Subtracting Real Numbers
		SBSection 1.6: Multiplying and Dividing Real Numbers
		SBSection 1.7: Properties of Real Numbers
		SBSection 1.8: Simplifying Expressions

	


	SBObjective 
	
		All SBO
		Evaluate algebraic expressions.
		Write word phrases as algebraic expressions.
		Decide whether a number is a solution of an equation.
		Write word statements as equations.
		Identify as an expression or an equation.

	



                        
                        
	
                               
                            


	Availability
	
                                
		All questions
		Questions that are not in the Study Plan
		Questions that are in the Study Plan
		Questions that are screenreader accessible

	
                            



                        
                        
						
                    
                    																														
                
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   

		
			
			
				
					
						
						
						
						
							Available Questions
						
						 
						(25)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 0My Selections (0)
							
							Pooling options
							    
							
								View question details
								
							
							
							
					
					
						
							
							Points: 0

							
							
							#
							

							
								
									
									
								Question ID

								
									
								
							
							
								
									SBObjective
									
								
								 
							Estimated time:0s
							
							
					
				
			
	
		
		
			
			
				
					(P) 1.3.13(P) 1.3.15(P) 1.3.17(P) 1.3.19(P) 1.3.23(P) 1.3.25(P) 1.3.29(P) 1.3.31(P) 1.3.33(P) 1.3.35(P) 1.3.37(P) 1.3.39(P) 1.3.43(P) 1.3.45(P) 1.3.47(P) 1.3.49(P) 1.3.51(P) 1.3.53(P) 1.3.59(P) 1.3.61(P) 1.3.63(P) 1.3.65(P) 1.3.73(P) 1.3.75(P) 1.3.77
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					
					
					
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student test
		
							View student quiz
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	(P) 1.3.47 (Write word phrases as algebraic expressions.)
	
&quot;))]</value>
      <webElementGuid>33faa9d2-80d6-44f4-9f0f-8d3f99eef9eb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
