<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pooling optionsView question details</name>
   <tag></tag>
   <elementGuidId>e3ffb65c-f3f2-4458-a417-5d586c45e985</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ctl00_ctl00_InsideForm_MasterContent_tdSelectedQuestionsHeader']/table/tbody/tr/td/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4bb6f1b7-e9b2-4836-84f2-34ccea7bedce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>StandOut</value>
      <webElementGuid>ca462375-2c68-42c4-9c96-b2cc0b3804ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							Pooling options
							    
							
								View question details
								
							
							</value>
      <webElementGuid>ecc94e6a-4664-45e0-94c1-d6d76615c20c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_tdSelectedQuestionsHeader&quot;)/table[@class=&quot;listheader xlbootstrap3&quot;]/tbody[1]/tr[1]/td[@class=&quot;bold&quot;]/div[@class=&quot;StandOut&quot;]</value>
      <webElementGuid>57bd8743-55cd-4889-bb55-2095d82006d3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ctl00_ctl00_InsideForm_MasterContent_tdSelectedQuestionsHeader']/table/tbody/tr/td/div[3]</value>
      <webElementGuid>a21a7d92-8e74-4d02-b74a-3beab3fed816</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Questions:'])[1]/following::div[2]</value>
      <webElementGuid>ecbe128a-0b74-47ea-a061-31b87c3764d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/table/tbody/tr/td/div[3]</value>
      <webElementGuid>5f2f1ea7-307e-438e-a0db-988ffae42547</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
							Pooling options
							    
							
								View question details
								
							
							' or . = '
							Pooling options
							    
							
								View question details
								
							
							')]</value>
      <webElementGuid>7fa8db72-62f3-46df-b4da-a99f47d14f8d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
