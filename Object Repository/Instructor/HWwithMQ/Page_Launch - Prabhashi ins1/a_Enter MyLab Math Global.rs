<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Enter MyLab Math Global</name>
   <tag></tag>
   <elementGuidId>a098dd92-3192-4a67-89c4-0147d738791e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='BtnEnter']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#BtnEnter</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>96f97757-ef63-4fe2-838f-0df4de8b1797</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>836b7dbf-3864-4c27-b067-cef1f882570f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0)</value>
      <webElementGuid>36c192cb-01fb-4a60-8597-d9ecdd785d0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BtnEnter</value>
      <webElementGuid>0097471f-b2bb-4358-8b62-f47792d34b92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>LoadSite();return false</value>
      <webElementGuid>2098b486-7683-473c-aa57-5c2ed305eeca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>9e0d0829-f1c3-4f9e-a093-4efb5568a10c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>buttons</value>
      <webElementGuid>ad6dae1d-4259-452e-9cca-0423632db595</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Enter MyLab Math Global
</value>
      <webElementGuid>0a838f91-c158-45f7-a7fc-4b761ee985ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BtnEnter&quot;)</value>
      <webElementGuid>38db94a1-2482-43a1-832d-836575ad5340</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='BtnEnter']</value>
      <webElementGuid>e840ed73-79fa-4449-b917-c8ef5c7874ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[4]/div/div[3]/a</value>
      <webElementGuid>89b77d6c-152e-4fbb-b103-b8d2aa2afbe0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Enter MyLab Math Global')]</value>
      <webElementGuid>5cfc62e1-32f6-4376-84bc-96aff4d575c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to MyLab Math Global!'])[1]/following::a[1]</value>
      <webElementGuid>45d48496-6962-440d-945e-ccf66389e29b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to MyLab Math Global Log In'])[1]/following::a[1]</value>
      <webElementGuid>816a12b4-9fbc-4b4b-a509-c88452ab5f33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='[+]'])[1]/preceding::a[1]</value>
      <webElementGuid>54d2612c-7a3c-49a4-ba86-e988cb21bfba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Enter MyLab Math Global']/parent::*</value>
      <webElementGuid>7071ce0f-2de2-417e-9c34-4027032e9be0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0)')])[2]</value>
      <webElementGuid>0ff305fe-cbc9-4422-9c09-efa87f45e3bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a</value>
      <webElementGuid>6748e876-60e0-4127-a659-28a588a34b38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0)' and @id = 'BtnEnter' and (text() = 'Enter MyLab Math Global
' or . = 'Enter MyLab Math Global
')]</value>
      <webElementGuid>26a2daea-02e6-4027-b85e-de4740c9c74d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
