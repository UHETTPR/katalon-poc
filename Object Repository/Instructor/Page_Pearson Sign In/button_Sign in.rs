<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign in</name>
   <tag></tag>
   <elementGuidId>19923648-b015-489d-a878-34f3e465a634</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='mainButton']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#mainButton</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5bc45cf7-c668-4b85-9c68-654c58271d2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>2dbe8ad4-127f-4997-9d76-4c114864faf2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mainButton</value>
      <webElementGuid>2a7f38e9-5e6c-4e06-9b62-ad2b0a9a43be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>!signingIn</value>
      <webElementGuid>4d143a76-6f98-473f-b4db-7fb14a265269</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-btn__cta--btn_xlarge full-width ng-binding ng-scope</value>
      <webElementGuid>2406d96d-5b66-4e09-9b8e-85dfabdc78bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>getCustomButtonClassname()</value>
      <webElementGuid>b0ef366a-be4a-40fb-b50f-68f0f89b454a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>c5baf8db-5c3d-46ba-a7d9-684ef5f744a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainButton&quot;)</value>
      <webElementGuid>013f8a56-0e79-49c5-ba89-91273f436b1b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='mainButton']</value>
      <webElementGuid>61ad8a71-5408-4617-ad9b-825a9d276450</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mainForm']/div[5]/button</value>
      <webElementGuid>43e1832b-47bb-41f0-97a6-c6e4ad5bc4a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your username or password?'])[1]/following::button[1]</value>
      <webElementGuid>dd320bc7-6522-4ef5-a773-119cab1a1bf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password is hidden'])[1]/following::button[1]</value>
      <webElementGuid>9eaaa3c0-23a1-4823-9f47-1a7b14e07ab6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::button[1]</value>
      <webElementGuid>28aba55c-8c47-4e9f-aec1-20aaee83d292</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/button</value>
      <webElementGuid>f16a615d-66c4-42f8-a8f1-6ded97b5076f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @id = 'mainButton' and (text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>9035443f-c32c-4f0a-b96e-ba4b960bbdf4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
