<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_DryRunTestCourse9100</name>
   <tag></tag>
   <elementGuidId>74ed16c3-fe69-4a82-a2ef-6d24c8a7ab15</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PanelBreadCrumb']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_PanelBreadCrumb > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>494f395c-5993-413e-832a-df5a88fd947a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		DryRunTestCourse9100
	</value>
      <webElementGuid>885c49ec-d885-4da5-99ee-703398848de0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_PanelBreadCrumb&quot;)/div[1]</value>
      <webElementGuid>8825efc7-9edc-4bf4-984b-f2801483db20</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PanelBreadCrumb']/div</value>
      <webElementGuid>27cce435-658f-4669-9100-3cad41d27ac9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NextGen Media Math Test Book--REAL'])[1]/following::div[8]</value>
      <webElementGuid>ef578ec2-12e0-421e-af58-cd82d49f13c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prabhashi ins1'])[3]/following::div[8]</value>
      <webElementGuid>9e42683c-4d4a-40a0-a30b-3713013ce1d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Homework'])[1]/preceding::div[2]</value>
      <webElementGuid>b95da800-f29a-412c-9059-cf3b6aea444c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/preceding::div[4]</value>
      <webElementGuid>2e0a213c-1262-4476-9fb7-e36b51ff628f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div/div</value>
      <webElementGuid>a5ca5572-5e90-4709-b8c9-cd8d3c8ee923</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
		DryRunTestCourse9100
	' or . = '
		DryRunTestCourse9100
	')]</value>
      <webElementGuid>4ae50fc9-4a21-4392-8718-ac9aa8548948</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
