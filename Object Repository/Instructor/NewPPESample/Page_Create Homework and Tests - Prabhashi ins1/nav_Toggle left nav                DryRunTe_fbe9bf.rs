<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nav_Toggle left nav                DryRunTe_fbe9bf</name>
   <tag></tag>
   <elementGuidId>ac8313b2-996a-4f1c-8586-09e331967182</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='mainTopNavContainer']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#mainTopNavContainer</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>nav</value>
      <webElementGuid>a0d923e9-0466-4cf0-a220-39338f39a9a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mainTopNavContainer</value>
      <webElementGuid>bf9585a3-37f5-4b9c-8bfd-2a6f7daddf48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar navbar-inverse navbar-fixed-top </value>
      <webElementGuid>7b0af491-9a27-4c97-ab6e-ead87c1c315c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>banner</value>
      <webElementGuid>0fbb5002-e48d-4d2e-a998-0ccd913010fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Course</value>
      <webElementGuid>065ae300-001b-4e47-9f23-639320d75f03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			

				
					
						
						Toggle left nav
					
				
                DryRunTestCourse9100 

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnroll in CourseEdit AccountLog Out|HelpHelpLegendTake a TourMathXL SupportBrowser Check|04/20/22 1:20 PM
				

			
		</value>
      <webElementGuid>ed2faecc-a20d-415d-abc1-30cddc6a6e22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainTopNavContainer&quot;)</value>
      <webElementGuid>9799562e-0db2-4bfd-b4c4-b9c0a30daaf1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//nav[@id='mainTopNavContainer']</value>
      <webElementGuid>5985776d-3a96-409d-8de3-8b63b28ee891</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='aspnetForm']/div[3]/div/nav</value>
      <webElementGuid>aa1be985-4526-4ac8-a8bf-744f45061b8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to main content'])[1]/following::nav[1]</value>
      <webElementGuid>0dc75d4d-7ffa-4572-9199-bf46f032b409</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav</value>
      <webElementGuid>e0987562-da59-42b9-b814-7d28d179a591</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//nav[@id = 'mainTopNavContainer' and (text() = '
			

				
					
						
						Toggle left nav
					
				
                DryRunTestCourse9100 

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnroll in CourseEdit AccountLog Out|HelpHelpLegendTake a TourMathXL SupportBrowser Check|04/20/22 1:20 PM
				

			
		' or . = '
			

				
					
						
						Toggle left nav
					
				
                DryRunTestCourse9100 

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnroll in CourseEdit AccountLog Out|HelpHelpLegendTake a TourMathXL SupportBrowser Check|04/20/22 1:20 PM
				

			
		')]</value>
      <webElementGuid>51d20dc3-d99e-4f71-81c8-0a914a39eb4d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
