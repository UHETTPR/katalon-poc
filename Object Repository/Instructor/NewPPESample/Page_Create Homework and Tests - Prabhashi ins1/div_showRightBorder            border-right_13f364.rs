<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_showRightBorder            border-right_13f364</name>
   <tag></tag>
   <elementGuidId>b0f59b4b-4534-43d5-850a-ff4dba23e898</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>fc2ea1c5-ee7b-4d4e-b8fd-eca135d4469a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG</value>
      <webElementGuid>153c1ccb-968e-4d31-8d90-0412ec5fc71d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook','');
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode','');
    }


		
        
    
	

    
        
            
			
					
					    
						    Name
						    sample
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
	SBChapter 
	
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	


	SBSection 
	
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	


	SBObjective 
	
		All SBO
		Identify numbers used to solve equations.
		Solve equations using the multiplication property.
		Simplify equations first, then solve.
		Write an equation from a statement.

	



                        
                        
	
                               
                            


	Availability
	
                                
		All questions
		Questions that are not in the Study Plan
		Questions that are in the Study Plan
		Questions that are screenreader accessible

	
                            



                        
                        
						
                    
                    																														
                
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   

		
			
			
				
					
						
						
						
						
							Available Questions
						
						 
						(25)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 10My Selections (10)
							
							Pooling options
							    
							
								View question details
								
							
							
							
					
					
						
							
							Points: 10

							
							
							#
							

							
								
									
									
								Question ID

								
									
								
							
							
								
									SBObjective
									
								
								 
							Estimated time:200m 40s
							
							
					
				
			
	
		
		
			
			
				
					(P) 2.2.7(P) 2.2.11(P) 2.2.13(P) 2.2.15(P) 2.2.17(P) 2.2.19(P) 2.2.21(P) 2.2.23(P) 2.2.25(P) 2.2.27(P) 2.2.31(P) 2.2.35(P) 2.2.37(P) 2.2.39(P) 2.2.41(P) 2.2.45(P) 2.2.47(P) 2.2.49(P) 2.2.51(P) 2.2.53(P) 2.2.57(P) 2.2.59(P) 2.2.61(P) 2.2.65(P) 2.2.69
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					1(P) 2.2.7Identify numbers used to solve equations.31m 15s2(P) 2.2.11Identify numbers used to solve equations.58m 6s3(P) 2.2.13Identify numbers used to solve equations.5m 50s4(P) 2.2.15Identify numbers used to solve equations.5m 50s5(P) 2.2.17Identify numbers used to solve equations.19m 44s6(P) 2.2.19Identify numbers used to solve equations.2m 22s7(P) 2.2.21Identify numbers used to solve equations.25m 54s8(P) 2.2.23Solve equations using the multiplication property.8m 38s9(P) 2.2.25Solve equations using the multiplication property.5m 4s10(P) 2.2.27Solve equations using the multiplication property.37m 57s
					
					
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student test
		
							View student quiz
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	(P) 2.2.25 (Solve equations using the multiplication property.)
	
</value>
      <webElementGuid>66e714f0-17d8-4357-9bdf-7b861b7a299d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG&quot;)</value>
      <webElementGuid>5b8b9413-88ff-4599-a790-229a4eca6a73</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG']</value>
      <webElementGuid>d47172a5-f5d0-42c7-b77c-28eab31331c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_DivPage2']/div[2]</value>
      <webElementGuid>3147ddf3-408e-41ef-9968-9aa5395835ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[2]/following::div[1]</value>
      <webElementGuid>cf1b7c08-2061-44bc-b981-71fcb787b1aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download'])[2]/following::div[1]</value>
      <webElementGuid>f0115834-3d5e-42e9-9cb7-82ac2391a735</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]</value>
      <webElementGuid>6460434b-f951-48a3-aa29-ea9db8b24519</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ctl00_ctl00_InsideForm_MasterContent_pnlStep2NONTG' and (text() = concat(&quot;
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }


		
        
    
	

    
        
            
			
					
					    
						    Name
						    sample
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
	SBChapter 
	
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	


	SBSection 
	
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	


	SBObjective 
	
		All SBO
		Identify numbers used to solve equations.
		Solve equations using the multiplication property.
		Simplify equations first, then solve.
		Write an equation from a statement.

	



                        
                        
	
                               
                            


	Availability
	
                                
		All questions
		Questions that are not in the Study Plan
		Questions that are in the Study Plan
		Questions that are screenreader accessible

	
                            



                        
                        
						
                    
                    																														
                
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   

		
			
			
				
					
						
						
						
						
							Available Questions
						
						 
						(25)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 10My Selections (10)
							
							Pooling options
							    
							
								View question details
								
							
							
							
					
					
						
							
							Points: 10

							
							
							#
							

							
								
									
									
								Question ID

								
									
								
							
							
								
									SBObjective
									
								
								 
							Estimated time:200m 40s
							
							
					
				
			
	
		
		
			
			
				
					(P) 2.2.7(P) 2.2.11(P) 2.2.13(P) 2.2.15(P) 2.2.17(P) 2.2.19(P) 2.2.21(P) 2.2.23(P) 2.2.25(P) 2.2.27(P) 2.2.31(P) 2.2.35(P) 2.2.37(P) 2.2.39(P) 2.2.41(P) 2.2.45(P) 2.2.47(P) 2.2.49(P) 2.2.51(P) 2.2.53(P) 2.2.57(P) 2.2.59(P) 2.2.61(P) 2.2.65(P) 2.2.69
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					1(P) 2.2.7Identify numbers used to solve equations.31m 15s2(P) 2.2.11Identify numbers used to solve equations.58m 6s3(P) 2.2.13Identify numbers used to solve equations.5m 50s4(P) 2.2.15Identify numbers used to solve equations.5m 50s5(P) 2.2.17Identify numbers used to solve equations.19m 44s6(P) 2.2.19Identify numbers used to solve equations.2m 22s7(P) 2.2.21Identify numbers used to solve equations.25m 54s8(P) 2.2.23Solve equations using the multiplication property.8m 38s9(P) 2.2.25Solve equations using the multiplication property.5m 4s10(P) 2.2.27Solve equations using the multiplication property.37m 57s
					
					
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student test
		
							View student quiz
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	(P) 2.2.25 (Solve equations using the multiplication property.)
	
&quot;) or . = concat(&quot;
	

	

	


    .showRightBorder
    {
        border-right: #d0d0d0 1px solid;
    }
    .hideRightBorder 
    {
        border-right: none;    
    }
    .showLeftBorder 
    {
        border-left: #d0d0d0 1px solid;    
    }
    .hideLeftBorder 
    {
        border-left: none;    
    }


    function DoLinkChangedBook()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeBook&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }
    function DoLinkChangedQuestionSelectionMode()
    {
        __doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ExFilter$LinkChangeQuestionSelectionMode&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;);
    }


		
        
    
	

    
        
            
			
					
					    
						    Name
						    sample
					    		                        
					    
						    Book
						    NextGen Media Math Test Book--REAL 
                                
                                Change...
                                
                                
					    	
                        
                        
						
	SBChapter 
	
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	


	SBSection 
	
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	


	SBObjective 
	
		All SBO
		Identify numbers used to solve equations.
		Solve equations using the multiplication property.
		Simplify equations first, then solve.
		Write an equation from a statement.

	



                        
                        
	
                               
                            


	Availability
	
                                
		All questions
		Questions that are not in the Study Plan
		Questions that are in the Study Plan
		Questions that are screenreader accessible

	
                            



                        
                        
						
                    
                    																														
                
        
	     
        
        
            
		
                    
                            
                            
			
                                
                                    Question Source
                                    
                                    
				
                                         Show publisher questions
                                                                
                                    
			
                                    
                                             Show Custom 1 questions
                                            
                                        
                                             Show Custom 2 questions
                                            
                                        
                                             Show Preloaded questions
                                            
                                        
                                    
				
                                         Show additional test bank questions
                                        
                                    
			
                                    
				
                                         Show custom questions (+) for this book                                    
                                        
                                         Show other custom questions Refine Selection ...
                                        
                                    
			
                                    
                                    
				
                                        (+) Create my own questions
                                    
			
                                
                            
		
                        
                    
                        
                        
                    
                
	
        
	
    
 

	
	

	   

		
			
			
				
					
						
						
						
						
							Available Questions
						
						 
						(25)
					
					
						
							
								
								
								
								
									Question ID
								
							
						
						

					
				
			 
			
			
			
				
					
						Questions: 10My Selections (10)
							
							Pooling options
							    
							
								View question details
								
							
							
							
					
					
						
							
							Points: 10

							
							
							#
							

							
								
									
									
								Question ID

								
									
								
							
							
								
									SBObjective
									
								
								 
							Estimated time:200m 40s
							
							
					
				
			
	
		
		
			
			
				
					(P) 2.2.7(P) 2.2.11(P) 2.2.13(P) 2.2.15(P) 2.2.17(P) 2.2.19(P) 2.2.21(P) 2.2.23(P) 2.2.25(P) 2.2.27(P) 2.2.31(P) 2.2.35(P) 2.2.37(P) 2.2.39(P) 2.2.41(P) 2.2.45(P) 2.2.47(P) 2.2.49(P) 2.2.51(P) 2.2.53(P) 2.2.57(P) 2.2.59(P) 2.2.61(P) 2.2.65(P) 2.2.69
					
				
			
			
				
					Add
	
					
						Remove
	
					
				
				
					Pool
	
					
						Unpool
	
					
				
			
			
			

				 

				
					1(P) 2.2.7Identify numbers used to solve equations.31m 15s2(P) 2.2.11Identify numbers used to solve equations.58m 6s3(P) 2.2.13Identify numbers used to solve equations.5m 50s4(P) 2.2.15Identify numbers used to solve equations.5m 50s5(P) 2.2.17Identify numbers used to solve equations.19m 44s6(P) 2.2.19Identify numbers used to solve equations.2m 22s7(P) 2.2.21Identify numbers used to solve equations.25m 54s8(P) 2.2.23Solve equations using the multiplication property.8m 38s9(P) 2.2.25Solve equations using the multiplication property.5m 4s10(P) 2.2.27Solve equations using the multiplication property.37m 57s
					
					
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					
					
					
				
			
	
		
		
		
		
		   Preview &amp; Add
		
		   
		
		
				
					
						
							Preview &amp; Remove
		
							
							View student test
		
							View student quiz
		
							
						
						
							Sort All
		
							
		
							
		
							
		
						
					
				
			
	
	
		
	
	(P) 2.2.25 (Solve equations using the multiplication property.)
	
&quot;))]</value>
      <webElementGuid>cac92f2d-959a-4936-861c-f7f3da75fbda</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
