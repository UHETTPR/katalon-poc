<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Choose questions on the left and click _f2c49c</name>
   <tag></tag>
   <elementGuidId>66a33642-90e8-4ec9-85e4-ba29760153ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='divAddInstr']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#divAddInstr</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dfedb5fb-9d94-4f5e-a6ab-edac23d02594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>divAddInstr</value>
      <webElementGuid>2150bde8-5c14-4cbd-8963-84e80ae35c99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					</value>
      <webElementGuid>e77801dc-b98c-4bee-bb94-223052f0f935</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;divAddInstr&quot;)</value>
      <webElementGuid>b2a9922f-6997-4c49-8d22-030f0b36c479</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='divAddInstr']</value>
      <webElementGuid>2e2b9963-5ec2-46c3-a49b-14d7697c7f35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='divSelection']/div</value>
      <webElementGuid>683499fc-8759-458c-967a-ffe77b7f7b35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unpool'])[1]/following::div[2]</value>
      <webElementGuid>6f06f9bd-9d7c-41fc-a8cf-07ab1131ea3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pool'])[1]/following::div[3]</value>
      <webElementGuid>ee0a25f6-b9c3-423a-965f-c9e87d26309c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview &amp; Add'])[1]/preceding::div[3]</value>
      <webElementGuid>58bec048-8ee0-47a5-a5c9-73b23e01bb11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Choose questions on the left and click Add to include them in this assignment.']/parent::*</value>
      <webElementGuid>86f33e8c-f2c2-4298-a3d1-63158a6dd575</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/div/div</value>
      <webElementGuid>22ccce5d-781a-4615-a2f8-cf38fd8b1a8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'divAddInstr' and (text() = '
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					' or . = '
						Choose questions on the left and click Add to include them in this assignment.

						
							Choose multiple questions on the left and click the Pool button to group them as one question.
						
					')]</value>
      <webElementGuid>9b24454c-2f8a-4279-b523-c8211a047928</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
