<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Toggle left nav                DryRunTe_c14d00</name>
   <tag></tag>
   <elementGuidId>a800278a-b7fc-45d9-a27f-6160790c35f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='mainTopNavContainer']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.container-fluid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f4d039b3-e8e1-45a6-a318-0999c333843c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container-fluid</value>
      <webElementGuid>d9b319f8-1025-4f73-ba67-40742cdbdbc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

				
					
						
						Toggle left nav
					
				
                DryRunTestCourse9100 

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnroll in CourseEdit AccountLog Out|HelpHelpLegendTake a TourMathXL SupportBrowser Check|04/20/22 1:24 PM
				

			</value>
      <webElementGuid>8db6438d-57c7-4252-afe0-e41af24e49c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainTopNavContainer&quot;)/div[@class=&quot;container-fluid&quot;]</value>
      <webElementGuid>85f90daf-bccb-4efb-a618-114a0f29eaf3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='mainTopNavContainer']/div</value>
      <webElementGuid>d0916bc4-ceb7-4c18-82fc-6fa14400eae4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to main content'])[1]/following::div[5]</value>
      <webElementGuid>b899ea94-c128-4942-a4c6-5e1cf3cdc80f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div</value>
      <webElementGuid>6d99b74c-1c20-445b-86d4-ffed7fa6249b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

				
					
						
						Toggle left nav
					
				
                DryRunTestCourse9100 

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnroll in CourseEdit AccountLog Out|HelpHelpLegendTake a TourMathXL SupportBrowser Check|04/20/22 1:24 PM
				

			' or . = '

				
					
						
						Toggle left nav
					
				
                DryRunTestCourse9100 

				
					
						Toggle navigation
						
					
				

				
					
					Prabhashi ins1My CoursesEnroll in CourseEdit AccountLog Out|HelpHelpLegendTake a TourMathXL SupportBrowser Check|04/20/22 1:24 PM
				

			')]</value>
      <webElementGuid>5f501d61-a6e6-4522-bb5c-c8d3071d8405</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
