<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1. The Real Number System2. Linear E_06cce4_1</name>
   <tag></tag>
   <elementGuidId>86588ea7-ffe1-432e-bea4-9f31de697a44</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListChapter']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListChapter</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f69b3a7c-188a-409c-9622-fef387eb822e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter</value>
      <webElementGuid>a8e7e88d-a8e9-4d85-b23b-64a80b8613e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter\',\'\')', 0)</value>
      <webElementGuid>c8d74661-2521-49fa-b4b2-67988bd8d10b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListChapter</value>
      <webElementGuid>5d9cf3c7-a024-481c-bd8c-8855b47efb84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>8d4cf3c2-9800-4e8e-947d-c910621fbcf8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	</value>
      <webElementGuid>3ecf1300-c9e9-4413-a13f-4cee7b36bd84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListChapter&quot;)</value>
      <webElementGuid>f1f8a4b1-c7aa-4f8b-87eb-1be5ee1c500e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListChapter']</value>
      <webElementGuid>5553c37c-98c6-4fd2-8892-9d6674086f7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrChapter']/td[2]/div/select</value>
      <webElementGuid>643123c9-021d-4ad5-914e-d8f877031186</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[1]</value>
      <webElementGuid>c9c4e0b4-f26a-4433-b8a5-f6e949ab6d4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change...'])[1]/following::select[1]</value>
      <webElementGuid>2d56bb44-63ae-4697-91cb-e6a32d399d25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/preceding::select[1]</value>
      <webElementGuid>a7aab3ce-9a63-4544-9ba8-dcd3479d5857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[2]</value>
      <webElementGuid>7cc33b6e-99b3-4763-a77f-74d1ba6c88de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>3733e742-d2a6-498a-b14f-ea1e1a68feec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter' and @id = 'DropDownListChapter' and (text() = '
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	' or . = '
		1. The Real Number System
		2. Linear Equations and Inequalities in One Variable
		3. Linear Equations and Inequalities in Two Variables; Functions
		4. MarkD Test Chapter
		5. Chapter not in Study Plan
		6. Drag and Drop Questions
		7. Essay questions
		8. StatCrunch

	')]</value>
      <webElementGuid>fa6769a5-499b-413c-9e12-f83e9990e00c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
