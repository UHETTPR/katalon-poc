<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 1.1 This is a very _357309</name>
   <tag></tag>
   <elementGuidId>a338db81-d8c3-43a0-8de2-73445d3c5893</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>51fa3f9e-037a-4d1c-9f51-06abce2f0873</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>fe128ecb-0994-4792-bf36-b534be43b6a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>6c04da06-566c-4821-94cd-c6f6196cc982</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>09562e39-145a-4d33-8ace-10a5e832c258</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>84078809-4053-4b8c-9b16-274223aa32d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All SBS
		SBSection 1.1: This is a very long section title.  This is a very long section title....
		SBSection 1.2: Exponents, Order of Operations, and Inequality
		SBSection 1.3: Variables, Expressions, and Equations
		SBSection 1.4: Real Numbers and the Number Line
		SBSection 1.5: Adding and Subtracting Real Numbers
		SBSection 1.6: Multiplying and Dividing Real Numbers
		SBSection 1.7: Properties of Real Numbers
		SBSection 1.8: Simplifying Expressions

	</value>
      <webElementGuid>ebc67d68-a828-4f37-ab46-9e2bcbefa8e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>e8d13c71-a070-4101-a5cd-bb4fb30fd978</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>cbcd47b6-d811-47f2-ae22-6795d9a6f487</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>bc38f47d-c526-4ac7-bb1e-74ab48844e93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>28cca0f4-be38-4996-b892-a2604d29b6f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>41bad658-8092-48fd-ab14-5bc2b98db191</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>a4709e0d-d319-443a-9e67-0534e30f2706</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>73866911-cdc5-4011-9901-4d4c952c4a93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>5e6800a9-9c84-4cb4-9ac1-4cb254d5b4bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All SBS
		SBSection 1.1: This is a very long section title.  This is a very long section title....
		SBSection 1.2: Exponents, Order of Operations, and Inequality
		SBSection 1.3: Variables, Expressions, and Equations
		SBSection 1.4: Real Numbers and the Number Line
		SBSection 1.5: Adding and Subtracting Real Numbers
		SBSection 1.6: Multiplying and Dividing Real Numbers
		SBSection 1.7: Properties of Real Numbers
		SBSection 1.8: Simplifying Expressions

	' or . = '
		All SBS
		SBSection 1.1: This is a very long section title.  This is a very long section title....
		SBSection 1.2: Exponents, Order of Operations, and Inequality
		SBSection 1.3: Variables, Expressions, and Equations
		SBSection 1.4: Real Numbers and the Number Line
		SBSection 1.5: Adding and Subtracting Real Numbers
		SBSection 1.6: Multiplying and Dividing Real Numbers
		SBSection 1.7: Properties of Real Numbers
		SBSection 1.8: Simplifying Expressions

	')]</value>
      <webElementGuid>1743d5ad-4857-4211-aa0d-e09c92511d96</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
