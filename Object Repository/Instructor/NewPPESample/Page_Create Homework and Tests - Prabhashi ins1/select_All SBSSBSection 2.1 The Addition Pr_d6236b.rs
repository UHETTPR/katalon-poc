<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SBSSBSection 2.1 The Addition Pr_d6236b</name>
   <tag></tag>
   <elementGuidId>70352b6d-a64b-42df-be19-b8183fc88859</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b9b4bbf8-4d27-44e7-8dc8-ab1cb56d8c1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>ec25d368-0f6a-4a0e-8467-663f26a3eef1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection\',\'\')', 0)</value>
      <webElementGuid>c2d81bc1-e21e-40e1-b071-73373f717c96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>88d5492e-4e5f-4d83-aac3-d3d76ffc6706</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>2551f925-7339-4628-87ad-a6450b0b1e8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	</value>
      <webElementGuid>7fde1c66-ed28-492a-ae0a-6236171bd6af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>f91e1cd9-c558-475e-b0a7-098237035d33</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>e5f4dc3a-57d6-458e-afc4-a7cf4e436a35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>b32a79c7-e338-44d4-9e84-9069899cb5d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBSection'])[1]/following::select[1]</value>
      <webElementGuid>00faf31a-b42e-4987-953a-9d4d54556362</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBChapter'])[1]/following::select[2]</value>
      <webElementGuid>d1f8ff93-6572-47cb-b90a-09fa45cf6cc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SBObjective'])[1]/preceding::select[1]</value>
      <webElementGuid>d4912447-dbdf-41d4-a929-07a99d6478de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>10de5cbb-40f1-495b-9ed7-7eeb94fa6585</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>340a3ef2-3b12-4d3c-94d4-012202620eb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	' or . = '
		All SBS
		SBSection 2.1: The Addition Property of Equality
		SBSection 2.2: The Multiplication Property of Equality
		SBSection 2.3: More on Solving Linear Equations
		SBSection 2.4: An Introduction to Applications of Linear Equations
		SBSection 2.5: Formulas and Applications from Geometry
		SBSection 2.6: Ratios and Proportions
		SBSection 2.7: More about Problem Solving
		SBSection 2.8: Solving Linear Inequalities

	')]</value>
      <webElementGuid>4a49468e-740f-4b18-b88a-cc216583caff</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
