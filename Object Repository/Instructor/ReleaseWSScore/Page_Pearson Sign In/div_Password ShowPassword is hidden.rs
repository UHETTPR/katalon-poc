<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Password ShowPassword is hidden</name>
   <tag></tag>
   <elementGuidId>17c0c734-35ba-4702-bebc-a204ba87aa71</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='mainForm']/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e0389b6d-6918-432c-9d05-009e5d2ddb15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-form--group</value>
      <webElementGuid>c7c23de0-1a75-495a-aef4-bd1960eb56e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>showKidFriendlyIcons ? 'indented' : ''</value>
      <webElementGuid>1ae48f4a-c08d-448c-b3bd-0113656ddb04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Password ShowPassword is hidden</value>
      <webElementGuid>59469c06-3300-42cc-91f4-abc6dcb39a83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainForm&quot;)/div[@class=&quot;pe-form--group&quot;]</value>
      <webElementGuid>bbf1593c-d1f1-4999-9772-4332613e1698</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mainForm']/div[3]</value>
      <webElementGuid>6b29f3da-bd5c-47e2-aa99-f02a5d23fd9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='@'])[1]/following::div[3]</value>
      <webElementGuid>bee1fc95-a7c4-479d-854f-00fecedce821</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::div[5]</value>
      <webElementGuid>cd087d1b-c49d-4296-95fc-3fa12be7d22b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[3]</value>
      <webElementGuid>548cee7d-0fdd-4046-b4a0-67ac6048b54b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Password ShowPassword is hidden' or . = 'Password ShowPassword is hidden')]</value>
      <webElementGuid>3dbb9238-d8d9-4646-8a19-8e9f2f2e9eb0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
