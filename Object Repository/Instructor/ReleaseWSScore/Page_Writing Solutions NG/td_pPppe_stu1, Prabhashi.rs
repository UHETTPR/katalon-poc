<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_pPppe_stu1, Prabhashi</name>
   <tag></tag>
   <elementGuidId>bf473b11-a728-41f2-9771-df3e01597e27</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'pPppe_stu1, Prabhashi' or . = 'pPppe_stu1, Prabhashi')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>a743d6bb-e3cc-4f52-a0fe-49c2bcc814e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>table_table_wsol_body_cell__1bGII</value>
      <webElementGuid>279879e6-ef97-4594-b25d-fed10b68ea6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>pPppe_stu1, Prabhashi</value>
      <webElementGuid>274bb8bf-003b-40e4-8032-797a3e4c07fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wsol-wrapper-sec&quot;]/div[@class=&quot;p-third-container&quot;]/div[@class=&quot;wsol-summary-container&quot;]/div[@class=&quot;wsol-grading-studentList-table&quot;]/table[@class=&quot;table_table_wsol__32OPW undefined&quot;]/tbody[1]/tr[@class=&quot;table_table_wsol_body_row__1rwvn&quot;]/td[@class=&quot;table_table_wsol_body_cell__1bGII&quot;]</value>
      <webElementGuid>568b242b-6e5f-48a5-8cc5-e80a3a40ecb6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[4]/div/div[2]/table/tbody/tr/td[2]</value>
      <webElementGuid>9484fa93-02ee-4f58-a3ad-9e88539d349d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::td[2]</value>
      <webElementGuid>30551375-affc-4b28-93f0-0367ba22d18c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Score'])[1]/following::td[2]</value>
      <webElementGuid>bb585b3c-15cd-42b8-b5e7-c86cd631c5c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submitted'])[1]/preceding::td[3]</value>
      <webElementGuid>e551908d-7381-4207-8773-7279d98a671d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='page 1'])[1]/preceding::td[5]</value>
      <webElementGuid>b5be9444-65e4-4679-8e40-b6b3f9ea072a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]</value>
      <webElementGuid>e5e891ed-4b35-4a4c-bed8-0c10beac2bb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'pPppe_stu1, Prabhashi' or . = 'pPppe_stu1, Prabhashi')]</value>
      <webElementGuid>bcb7d6d2-f8f9-4dc6-9f46-d3a256add8b7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
