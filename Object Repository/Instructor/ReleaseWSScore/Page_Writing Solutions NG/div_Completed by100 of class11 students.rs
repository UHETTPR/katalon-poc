<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Completed by100 of class11 students</name>
   <tag></tag>
   <elementGuidId>a5119a74-ad0a-4dca-b29b-c123dd0a0217</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Completed by100% of class1/1 students' or . = 'Completed by100% of class1/1 students')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.wsol-gs-bt-sub-status > div.wsol-gs-sub-box</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5ae2a43c-db13-42c7-906d-53ecc8df0965</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wsol-gs-sub-box</value>
      <webElementGuid>f82f14bb-c42d-48e0-8054-50f08cbbabab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Completed by100% of class1/1 students</value>
      <webElementGuid>6049d816-87b8-47f0-9e60-049a8a1ade75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wsol-wrapper-sec&quot;]/div[@class=&quot;p-third-container&quot;]/div[@class=&quot;wsol-summary-container&quot;]/div[@class=&quot;wsol-summary-header-main&quot;]/div[@class=&quot;wsol-summary-header-sec-bottom&quot;]/div[@class=&quot;wsol-gs-bt-sub-status&quot;]/div[@class=&quot;wsol-gs-sub-box&quot;]</value>
      <webElementGuid>a202461e-33ee-41a4-8608-614a04b76878</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[4]/div/div/div[3]/div/div</value>
      <webElementGuid>1dc92314-6bb1-45db-ae9b-0889c60c001e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='%'])[1]/following::div[6]</value>
      <webElementGuid>fdb8f866-b9a0-4c02-8532-c1095d007e23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Writing Assignment'])[1]/following::div[10]</value>
      <webElementGuid>22378a46-924e-4991-b88e-0eefdcaabb17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manual Scoring'])[1]/preceding::div[9]</value>
      <webElementGuid>da3e83ba-3471-456e-883c-38a2a4ece7ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Score and release students', &quot;'&quot;, ' work.')])[1]/preceding::div[11]</value>
      <webElementGuid>55b24ac9-256c-4a1b-a72c-a9cc5288e451</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div</value>
      <webElementGuid>8bbf8e21-454d-4c23-b048-0e6891a18894</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Completed by100% of class1/1 students' or . = 'Completed by100% of class1/1 students')]</value>
      <webElementGuid>d5383e5e-ab50-4c4c-beae-554199c86e43</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
