<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Student Answer</name>
   <tag></tag>
   <elementGuidId>825fc88c-1b07-4218-ae2f-0f6886012feb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = 'Student Answer' or . = 'Student Answer')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#tinymceContent > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>0c1a6274-a8e1-41d9-b2d4-dea6a7a4d5c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Student Answer</value>
      <webElementGuid>d349cbfb-baf0-4ecb-9781-b7517d261ed6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tinymceContent&quot;)/p[1]</value>
      <webElementGuid>247eabb5-7c48-4c45-ba6e-90e0e3c3b2e1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='tinymceContent']/p</value>
      <webElementGuid>6d8f44c0-1687-40e8-807a-c33c52323f8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submission'])[2]/following::p[1]</value>
      <webElementGuid>ab4371ea-59ac-456b-830f-b904ce8f92d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submission'])[1]/following::p[4]</value>
      <webElementGuid>e57a198e-0c37-4f48-9313-3ad433ddded9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Release Score'])[1]/preceding::p[1]</value>
      <webElementGuid>dbc05663-1763-48b5-b63e-0eac4b10071c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/preceding::p[1]</value>
      <webElementGuid>345e55a4-a52f-4597-b2ea-3789ff710f25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/p</value>
      <webElementGuid>1f1f55de-95cf-4975-8d66-7559c8cf309d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Student Answer' or . = 'Student Answer')]</value>
      <webElementGuid>3682a54e-5b30-42cc-97d0-491331076558</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
