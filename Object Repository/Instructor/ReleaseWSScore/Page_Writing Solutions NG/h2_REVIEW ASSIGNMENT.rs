<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_REVIEW ASSIGNMENT</name>
   <tag></tag>
   <elementGuidId>fdff97e0-8bcb-4bef-9a6a-1f026272975c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2[(text() = 'REVIEW ASSIGNMENT' or . = 'REVIEW ASSIGNMENT')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h2.h5.wsol-header-value</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>ea48d9a1-f4e9-4162-9934-69d4b04fe04a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>h5 wsol-header-value</value>
      <webElementGuid>7b5aca19-01c1-4f28-a7b0-c5cc20f86607</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>REVIEW ASSIGNMENT</value>
      <webElementGuid>351f97bd-6121-4a75-aff0-a352c71ed5b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wsol-wrapper-sec&quot;]/div[@class=&quot;wsol-header-gradient-container&quot;]/div[@class=&quot;wsol-header-gradient&quot;]/div[@class=&quot;p-third-container&quot;]/div[@class=&quot;wsol-header&quot;]/div[@class=&quot;wsol-header-text&quot;]/h2[@class=&quot;h5 wsol-header-value&quot;]</value>
      <webElementGuid>7e2eee3f-5781-4dff-aa18-f60d8cccffc7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div/div/h2</value>
      <webElementGuid>80f2234d-4e9f-4b69-848c-56b9f5f363a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Student'])[1]/preceding::h2[1]</value>
      <webElementGuid>5cf1142d-3d2c-486f-bbbe-92f371361fa9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='REVIEW ASSIGNMENT']/parent::*</value>
      <webElementGuid>cfd016be-5a1a-4991-a991-8e1d258ab962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>93340ab6-e271-4bb3-ba86-af5cf46991b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'REVIEW ASSIGNMENT' or . = 'REVIEW ASSIGNMENT')]</value>
      <webElementGuid>52caaa08-0307-4905-9e9c-98fbfa87b393</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
