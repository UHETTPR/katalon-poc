<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_TestingWSAssignment</name>
   <tag></tag>
   <elementGuidId>3a16fc5a-586e-4ce1-bf11-d126003c20cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>table.grid > tbody > tr > td</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Writing Assignment'])[1]/preceding::td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>ee18544b-abee-4100-96ff-ebed44259f0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    TestingWSAssignment
                    
                </value>
      <webElementGuid>9945ef03-1896-4d77-8417-6aaec5a50c56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;_content&quot;)/div[4]/table[@class=&quot;grid&quot;]/tbody[1]/tr[1]/td[1]</value>
      <webElementGuid>c483c4f4-8328-4a21-bdac-37e5803d085d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>140f8fc4-92d4-457e-a0d9-871ebff675be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    TestingWSAssignment
                    
                </value>
      <webElementGuid>75cfb1d7-0f63-4f53-bb81-e8a4d8a8b1c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;_content&quot;)/div[4]/table[@class=&quot;grid&quot;]/tbody[1]/tr[1]/td[1]</value>
      <webElementGuid>516faf84-2fc6-4afa-80dc-6ce90f6ec2a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='_content']/div[4]/table/tbody/tr/td</value>
      <webElementGuid>e3ec1a8a-2b0e-4e76-beed-b465be8b542d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='# of Submissions'])[1]/following::td[1]</value>
      <webElementGuid>82aa0516-a982-4b66-872c-5c36ff010f33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oldest Submission'])[1]/following::td[1]</value>
      <webElementGuid>d14cd6a2-da87-4f55-b76e-48b90efac620</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Writing Assignment'])[1]/preceding::td[1]</value>
      <webElementGuid>96cadc40-14c1-4655-87de-47c861818a82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/table/tbody/tr/td</value>
      <webElementGuid>810707d7-587a-41a1-8f74-a66e922ac3b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                    
                    TestingWSAssignment
                    
                ' or . = '
                    
                    TestingWSAssignment
                    
                ')]</value>
      <webElementGuid>305c509f-8158-4fdd-b7ae-866c3c0968a8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
