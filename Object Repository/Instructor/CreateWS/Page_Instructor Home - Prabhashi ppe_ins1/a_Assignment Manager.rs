<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Assignment Manager</name>
   <tag></tag>
   <elementGuidId>b3d2119a-c0c8-4046-a711-27faefc4a26d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='storeNavData(&quot;27&quot;)']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7cfd6861-f118-49df-a7ec-b1ff8ff5521e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1</value>
      <webElementGuid>79dde17c-0679-46b4-a18d-0ce253ac3afa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>8766f5c8-1c6e-4a57-b49b-0d8e0e010550</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>39399d25-0b88-4fb6-addf-790cf3299790</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;27&quot;)</value>
      <webElementGuid>69c7a8c4-9026-43f4-8930-fecbd20e3a41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>cf0d60b8-5a61-426e-b0ee-97ec453f2297</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>aac0e389-cb52-4920-915a-5be2e587c051</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>11e10b4c-d380-4008-bdad-98527ce502e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>55c39b45-7e36-4014-88b6-03c4824480be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1</value>
      <webElementGuid>89645882-3da4-4d16-a4c5-93f55c97e253</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>633b12bb-7001-44d2-bc0b-ae27ee8c17f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>f7a015b9-3097-4db9-ad39-f71d66ca36ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;27&quot;)</value>
      <webElementGuid>70d5d574-b0c6-4f63-90a0-fb0635dbb22b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>a2a2c3a6-9908-4a81-8634-28592e9401bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>c81800ef-c9a6-456a-a939-b5c86a800a93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>31955694-488f-4bda-84b6-220ed36e50f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5309f70e-0f07-435a-abf0-d081f163c038</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1</value>
      <webElementGuid>81f0273c-46dc-4abf-889e-41d7ee5d7a17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>9d91d8cc-a093-4c8c-8073-568116ceb247</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>d6bad153-2a76-422b-abff-2837e513ec55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;27&quot;)</value>
      <webElementGuid>4064e19d-1d0a-45b9-9ca4-f5954a5677f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>4a8fec15-7dfb-4226-80ea-28051113b063</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>e258d629-6816-4fec-a351-ce59b312ef28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>f6df9020-748a-46ff-9ea6-f16d1c7ca02a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>42efac06-41c4-4bba-8a7f-eea23c74bc1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1</value>
      <webElementGuid>cfab0c44-af1d-4a31-bbab-265c631695d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>a75622a4-5c6d-4b4a-a86b-39c3dbbc6e37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>693f9def-5817-4a52-ab74-dd9384d5a41e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;27&quot;)</value>
      <webElementGuid>33ea7260-76f2-42a4-8e0d-60929282849d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>7406c932-943b-495e-8240-b28dbb2d4bc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assignment Manager</value>
      <webElementGuid>b14e8622-7f20-40d7-b281-e113c16883a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>bca5bbb1-ccb8-4520-a531-c8fc442420f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;27&quot;)']</value>
      <webElementGuid>34c4ff12-93cf-4865-82c8-f3d7771bd984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[12]/a</value>
      <webElementGuid>dc2a0f2c-ce4f-44cb-82b3-a7f0f9119652</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Assignment Manager')]</value>
      <webElementGuid>6a54aa53-cf0b-4794-9b21-b506f819c9da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home Page Manager'])[1]/following::a[1]</value>
      <webElementGuid>303155f5-b700-45d9-9e19-c89200e49c69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course Manager'])[1]/following::a[2]</value>
      <webElementGuid>581936bf-23af-4e6a-b317-45a85a1f0219</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan Manager'])[1]/preceding::a[1]</value>
      <webElementGuid>a4f9efae-d1bc-4e7e-9302-707af2e3aba3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook'])[1]/preceding::a[2]</value>
      <webElementGuid>e7a700c2-f301-445a-9dcf-a89fa8fd408d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Assignment Manager']/parent::*</value>
      <webElementGuid>9e728d46-3121-4def-9105-880027ceca5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1')]</value>
      <webElementGuid>5ab50ae7-c5d8-4189-a8ce-7b7385c2dde3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/a</value>
      <webElementGuid>19ea8959-0fc6-4546-bf05-58676b39b054</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/AssignmentManager.aspx?type=-1&amp;ch=-1' and @title = 'Assignment Manager' and (text() = 'Assignment Manager' or . = 'Assignment Manager')]</value>
      <webElementGuid>ec04e7d4-a308-42c1-a567-16c3ab44c1f8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
