<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Prabhashi ppe_ins1                      _edb108</name>
   <tag></tag>
   <elementGuidId>047b4291-4d68-4b4b-8b75-82a048b8b33c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='leftnavtable']/tbody/tr[2]/td</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>509e90ac-8e05-4e2f-8ba4-3116a7f59931</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>valign</name>
      <type>Main</type>
      <value>top</value>
      <webElementGuid>3460a373-b3ed-4b67-959e-f1e0012c8363</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>100%</value>
      <webElementGuid>2fe0bd2c-8d24-4680-9099-31f8ebf0f7e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>100%</value>
      <webElementGuid>015ce6e5-6122-4f90-b64b-b643b3ce5f2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                    
                                                                

                                                                
                                                                    
                                                                 
                                                                    
	                                                                   
                                                                        
                                                                        
                                                                        
                                                                 
                                                                        
                                                                    

                                                                  

                                                                

                                                                
	
		
                                                                            
                                                                            
                                                                                
                                                                                    Prabhashi ppe_ins1
                                                                                    IntPPETestCourse
                                                                                
                                                                                
                                                                                    Prabhashi ppe_ins1
                                                                                    NextGen Media Math Test Book--REAL
                                                                                
                                                                                
                                                                                    06/16/22
                                                                                    9:44am
                                                                                
                                                                            
                                                                            
                                                                            
                                                                        
	


                                                                
                                                                    
                                                                    
                                                                
                                                                
                                                                
	
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$InsideForm$MasterContent$ScriptManager1', 'aspnetForm', [], ['ctl00$ctl00$InsideForm$MasterContent$SettingsCtrl$imgBtnUpdateCoverage',''], [], 90, 'ctl00$ctl00');
//]]>



	IntPPETestCourse
	


       
           
        
            
        
        
           
            
            New Writing Assignment   
           
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
    
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener('mousedown', function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener('keydown', function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById('ButtonHelp').setAttribute('aria-expanded', 'false');
            }
        });

        }
    
 
        
        
      
    
    


	
    

	
           
        
        
            
                
                    
                        
                                
                                    Start
                                
                            
                    
                    
                        
                                
                                    Create Writing Assignment
                                
                            
                    
                    
                        
                                
                                    Choose Settings
                                
                            
                    
                
            
        
                
        
		
		
				

			
		
		
			
				
					
					
				
			
		

		

	
	

	


                                                                    
                                                            </value>
      <webElementGuid>3ee47944-8a91-4e94-b9f9-1ee1bbc72730</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]</value>
      <webElementGuid>888c197f-158a-4627-b3ba-04204113edb6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td</value>
      <webElementGuid>90e08ba8-81d2-4730-b385-28d07fc378aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook'])[1]/following::td[3]</value>
      <webElementGuid>f7406f79-7f44-43b9-9db5-6b68238257a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan Manager'])[1]/following::td[3]</value>
      <webElementGuid>40e4af64-69b5-43fd-94be-2eeb0a16470a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td</value>
      <webElementGuid>6bf2d4e2-168d-4050-991d-3fe4b31d6132</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = concat(&quot;
                                                                    
                                                                

                                                                
                                                                    
                                                                 
                                                                    
	                                                                   
                                                                        
                                                                        
                                                                        
                                                                 
                                                                        
                                                                    

                                                                  

                                                                

                                                                
	
		
                                                                            
                                                                            
                                                                                
                                                                                    Prabhashi ppe_ins1
                                                                                    IntPPETestCourse
                                                                                
                                                                                
                                                                                    Prabhashi ppe_ins1
                                                                                    NextGen Media Math Test Book--REAL
                                                                                
                                                                                
                                                                                    06/16/22
                                                                                    9:44am
                                                                                
                                                                            
                                                                            
                                                                            
                                                                        
	


                                                                
                                                                    
                                                                    
                                                                
                                                                
                                                                
	
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$SettingsCtrl$imgBtnUpdateCoverage&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>



	IntPPETestCourse
	


       
           
        
            
        
        
           
            
            New Writing Assignment   
           
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
    
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    


	
    

	
           
        
        
            
                
                    
                        
                                
                                    Start
                                
                            
                    
                    
                        
                                
                                    Create Writing Assignment
                                
                            
                    
                    
                        
                                
                                    Choose Settings
                                
                            
                    
                
            
        
                
        
		
		
				

			
		
		
			
				
					
					
				
			
		

		

	
	

	


                                                                    
                                                            &quot;) or . = concat(&quot;
                                                                    
                                                                

                                                                
                                                                    
                                                                 
                                                                    
	                                                                   
                                                                        
                                                                        
                                                                        
                                                                 
                                                                        
                                                                    

                                                                  

                                                                

                                                                
	
		
                                                                            
                                                                            
                                                                                
                                                                                    Prabhashi ppe_ins1
                                                                                    IntPPETestCourse
                                                                                
                                                                                
                                                                                    Prabhashi ppe_ins1
                                                                                    NextGen Media Math Test Book--REAL
                                                                                
                                                                                
                                                                                    06/16/22
                                                                                    9:44am
                                                                                
                                                                            
                                                                            
                                                                            
                                                                        
	


                                                                
                                                                    
                                                                    
                                                                
                                                                
                                                                
	
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$SettingsCtrl$imgBtnUpdateCoverage&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>



	IntPPETestCourse
	


       
           
        
            
        
        
           
            
            New Writing Assignment   
           
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
    
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    


	
    

	
           
        
        
            
                
                    
                        
                                
                                    Start
                                
                            
                    
                    
                        
                                
                                    Create Writing Assignment
                                
                            
                    
                    
                        
                                
                                    Choose Settings
                                
                            
                    
                
            
        
                
        
		
		
				

			
		
		
			
				
					
					
				
			
		

		

	
	

	


                                                                    
                                                            &quot;))]</value>
      <webElementGuid>f6c14738-53aa-4431-a71e-1ac1f85a125e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
