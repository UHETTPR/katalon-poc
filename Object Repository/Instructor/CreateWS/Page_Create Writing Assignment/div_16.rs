<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_16</name>
   <tag></tag>
   <elementGuidId>786f655a-9716-49f2-bffb-3bb99c0dccf3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='day16']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#day16</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1b1f2cd0-509f-4bd0-96ee-184a413f7466</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-cal-cell-square  </value>
      <webElementGuid>fc70384b-e71f-4134-85f4-a3db15881af3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>day16</value>
      <webElementGuid>c8ed0a17-0312-4529-8811-12fcd4c9291f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>gridcell</value>
      <webElementGuid>da7e7d5d-0c33-4dd0-89e2-c97f4ff1dbb4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Thursday June 16</value>
      <webElementGuid>ae554018-43fa-46cc-a1bf-8b86a5f5e8ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-current</name>
      <type>Main</type>
      <value>date</value>
      <webElementGuid>a1fe50e4-ab0f-4ace-a967-a24e88308bca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>38f47a5c-52ca-4dd4-9b4f-f74e123e4324</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>16</value>
      <webElementGuid>69420587-2ad8-4b11-a6a5-8df665caf11a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;day16&quot;)</value>
      <webElementGuid>a2c7d81b-2f76-4889-af27-8dd1c150389c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/iframe_Choose Settings_wsDashboardFrame_1_2</value>
      <webElementGuid>3c28ce08-b988-4a3f-9bdc-5c18c5cca21d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='day16']</value>
      <webElementGuid>260df8a8-bb5a-4b23-99e7-2d341e557f30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wsol-datetime-container']/div/div/div/div[2]/div/div/div[3]/div[3]/div[5]/div/div</value>
      <webElementGuid>ed392371-20ac-4236-bece-753c23e96f60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S'])[2]/following::div[55]</value>
      <webElementGuid>87737f3e-186d-47a5-b45e-d92de7ccb1d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='F'])[1]/following::div[56]</value>
      <webElementGuid>c5b61256-c1f2-429a-9aa4-db99c046d3a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Time(Asia/Calcutta)'])[1]/preceding::div[47]</value>
      <webElementGuid>da30dc76-4f94-4879-bf78-2d062dea2e5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Due'])[1]/preceding::div[50]</value>
      <webElementGuid>7207cb73-ee50-49bc-90ef-0ff42a7e7206</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='16']/parent::*</value>
      <webElementGuid>ef952554-6e61-4793-ae8e-cd22e0543ce3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[3]/div[5]/div/div</value>
      <webElementGuid>c6fdbe66-50f9-4008-8205-fa05d4026aa9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'day16' and (text() = '16' or . = '16')]</value>
      <webElementGuid>b94c35c3-4cc1-4c38-91bf-5788ea29193f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
