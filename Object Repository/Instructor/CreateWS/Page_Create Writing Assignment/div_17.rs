<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_17</name>
   <tag></tag>
   <elementGuidId>32cde633-456c-4146-96cb-f7ce2d3c18d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='day17']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#day17</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f1c0adb9-35a3-4ecd-a114-8fa0ccb3f77e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-cal-cell-square  </value>
      <webElementGuid>9f434f0d-0ef2-4e33-b839-6c58df0bfe10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>day17</value>
      <webElementGuid>6ff0f17d-028c-4785-8f75-422533b75c75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>gridcell</value>
      <webElementGuid>26674638-80c6-47b6-ba47-f8efa610193f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Friday June 17</value>
      <webElementGuid>f1e1f44a-2922-47bb-91ed-4c71cad8f2ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>5ee152d4-ba1d-4afb-95b4-4bd6c145fb34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>17</value>
      <webElementGuid>cd407a3a-e78c-4104-a7ce-3132b0666751</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;day17&quot;)</value>
      <webElementGuid>2261c471-c385-43b2-8b0e-813e4665af72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Instructor/CreateWS/Page_Create Writing Assignment/iframe_Choose Settings_wsDashboardFrame_1_2</value>
      <webElementGuid>a60b828e-d272-42e0-9e68-5a4bcac62648</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='day17']</value>
      <webElementGuid>6e4b41fc-ab67-4265-964f-c46616bf53d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wsol-datetime-container']/div/div[3]/div/div[2]/div/div/div[3]/div[3]/div[6]/div/div</value>
      <webElementGuid>6dab101e-e0a4-4683-b0ec-8fc0ea7508ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='S'])[2]/following::div[58]</value>
      <webElementGuid>bdaa3107-ed93-44e4-bfa4-590d9dc7c4a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='F'])[1]/following::div[59]</value>
      <webElementGuid>c2ecf7b5-e8d7-4d2b-85c9-b704494094dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Time(Asia/Calcutta)'])[2]/preceding::div[44]</value>
      <webElementGuid>51109b1b-137b-4dba-92bc-f4152be0265e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Objective'])[1]/preceding::div[47]</value>
      <webElementGuid>601d9223-cd63-480c-b237-283cdf207a74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='17']/parent::*</value>
      <webElementGuid>c889088c-cfb3-433b-9b67-aa3845f49a6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[6]/div/div</value>
      <webElementGuid>9937ae4a-4e1b-45e0-aef1-c02b95f44b45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'day17' and (text() = '17' or . = '17')]</value>
      <webElementGuid>af055fc5-5e3f-4af6-b8ef-6d9882342a1d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
