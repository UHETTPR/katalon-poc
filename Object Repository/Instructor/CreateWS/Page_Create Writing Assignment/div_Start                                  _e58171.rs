<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Start                                  _e58171</name>
   <tag></tag>
   <elementGuidId>a6f3ff3d-d1ee-4db2-837f-8f7354fe3a1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1e627cb4-b750-44bc-ac0c-154516ca8709</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
           
        
        
            
                
                    
                        
                                
                                    Start
                                
                            
                    
                    
                        
                                
                                    Create Writing Assignment
                                
                            
                    
                    
                        
                                
                                    Choose Settings
                                
                            
                    
                
            
        
                
        
		
		
				

			
		
		
			
				
					
					
				
			
		

	</value>
      <webElementGuid>8683a21a-7411-4db8-b046-c64991bb8de7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/div[2]</value>
      <webElementGuid>9df1ea2b-7f55-45d8-9710-3828e0cfc029</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]</value>
      <webElementGuid>dc726054-4551-4a7a-af26-d5d56e737ab0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/following::div[1]</value>
      <webElementGuid>7ba63dcc-c08f-46d4-af11-6acca6a16a7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Writing Assignment'])[1]/following::div[4]</value>
      <webElementGuid>5f050a7c-03c5-4841-a33f-fa09ebdeb39b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]</value>
      <webElementGuid>6bc099a2-1e80-4443-b097-b9ba66188b18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
           
        
        
            
                
                    
                        
                                
                                    Start
                                
                            
                    
                    
                        
                                
                                    Create Writing Assignment
                                
                            
                    
                    
                        
                                
                                    Choose Settings
                                
                            
                    
                
            
        
                
        
		
		
				

			
		
		
			
				
					
					
				
			
		

	' or . = '
           
        
        
            
                
                    
                        
                                
                                    Start
                                
                            
                    
                    
                        
                                
                                    Create Writing Assignment
                                
                            
                    
                    
                        
                                
                                    Choose Settings
                                
                            
                    
                
            
        
                
        
		
		
				

			
		
		
			
				
					
					
				
			
		

	')]</value>
      <webElementGuid>81743ff3-f274-48a6-8f84-c2b9cc9c2ab1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
