<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Username_username</name>
   <tag></tag>
   <elementGuidId>b755332b-e32b-4797-9a8a-acc017b8d340</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='username']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#username</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e835e81f-3417-46cc-8d50-5e15fd2df7eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>3a88ace8-8d7d-42bc-b53c-74cb7d76cccc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>eea2792d-e308-42da-9767-4914e0694c7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>user.username</value>
      <webElementGuid>54cca37f-2ebc-492d-b964-c24bfe551395</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>hasError('username') ? 'pe-textInput--input_error' : 'pe-textInput'</value>
      <webElementGuid>52b8eead-f86d-4fa4-8640-f9134f43e2b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>b5f8dede-5892-487a-8171-39515b1fd1f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>59114adf-4811-4764-a676-df8625c72b22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>17a979ba-6f80-4bc5-9c10-58e826e6bc8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a00f4d71-5789-4dd9-b131-cbde14ea64cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>67bcb4dc-6b5b-4f3d-bae6-e4cd5978e190</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>usernameChanged()</value>
      <webElementGuid>4139077b-b94d-44f3-864e-9bcf19f836fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required pe-textInput</value>
      <webElementGuid>b398ddc2-f35d-4ebf-a103-5bdee9a30347</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>9e5506d2-f8bb-402d-b6f6-438a190a4fed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>02eb1abb-7da7-4068-ab76-d983d1abce67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d4ed0df6-eda7-49e2-908b-fb274015c6c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>c0cd523b-80c9-4219-b592-83084dc83dfe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>3a3df236-0104-4c12-b101-5a9d0981a6cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>user.username</value>
      <webElementGuid>cf50cad8-0d5e-4599-b9cf-15ff111887ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>hasError('username') ? 'pe-textInput--input_error' : 'pe-textInput'</value>
      <webElementGuid>e4518915-8921-4dc6-9111-1974f62a7fa5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>5e4107ed-167b-4043-a17b-bf05e8641ff6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>2a376e9f-587a-4b72-9de7-35229af28e88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>5e40301d-0e6e-4641-ad28-ee1a39265498</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>fe61437d-1b59-4b6b-9b45-f15c6d0ce90b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>f7f8e95b-7e90-4402-8ff2-b5d522150ded</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>usernameChanged()</value>
      <webElementGuid>fcf1bf24-2216-49a5-ad3f-b724b1909161</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required pe-textInput ng-animate ng-touched-add ng-untouched-remove</value>
      <webElementGuid>76eaea96-12a0-4a48-9125-72b0c8716bad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>b10ae346-d2c9-47d7-9a17-399f43c9760c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ng-animate</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>905f7c7f-d361-4895-b1cc-7e3aa3df428b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>2bc80ea9-f298-420d-95bf-0a75d6babc34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>18fab0da-2920-4dc8-8fbd-2507487a5873</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>229f37c1-d832-49dd-8fee-7fd83836b185</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>f8746e80-eb82-4174-8ebe-c271c9053e8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>user.username</value>
      <webElementGuid>d47abd79-c464-47c6-8e7c-d4958be18f5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>hasError('username') ? 'pe-textInput--input_error' : 'pe-textInput'</value>
      <webElementGuid>9523a68a-9879-4f8a-b655-2b60ff173ef1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>2790632c-7a8b-4103-9aa4-8c4bf12b991f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>52bde5c7-fd3f-4f89-9830-bd8a6bb472cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>7660a6ad-a5a2-4a95-bd91-4c91e80d38a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>eef934f5-7084-4792-944f-5b86e687aee7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>6e51ab2a-baa1-4351-aaec-390599d7c0b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>usernameChanged()</value>
      <webElementGuid>f9e5706e-5987-459f-849e-d4605c72200d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required pe-textInput</value>
      <webElementGuid>2708e6c4-3142-4a45-b6ce-9bb0db2769d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>8e0875a7-a568-4b35-907f-8aae84370356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>b634f016-6f04-42f9-96fa-3fb73152a4a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>229426d8-2fa7-4897-8679-f7b9f7031eeb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>b64ea35a-8f71-4918-bd28-483884b056d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>6d12abbd-7509-48c3-a991-641d748a7043</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>user.username</value>
      <webElementGuid>32e42f1c-f1cc-40ff-9223-fdbced409342</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>hasError('username') ? 'pe-textInput--input_error' : 'pe-textInput'</value>
      <webElementGuid>51729b46-2b95-4544-9999-deb45b2a377b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>c169b4ee-8a66-4f7e-aec0-61a919318837</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>bf0c73a2-39fd-4a90-b65e-e7fb4dc50fa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>52c272fa-6790-4ce0-a6a5-c6ac6512941f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a92b9934-d5b2-4812-b1a1-e35be7364076</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>04ada045-d856-4a8c-91d1-b3f8368c1c11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>usernameChanged()</value>
      <webElementGuid>1152ee60-49d7-4606-80d4-62d6442d76e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required pe-textInput</value>
      <webElementGuid>8f322b29-8445-4fc3-b993-d68c2d5322c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>1ffc5d55-87db-4c14-a840-6f2ac39bdfea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>25ced6ff-74de-4517-b3fa-6f6f955fc814</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e49d0784-a9a6-4dbf-b469-73f4058e22eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>fdf7ac43-3ec1-4f60-a9cf-ce6e8d6b62bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>49807352-b450-4401-bf63-33ae97f1ebd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>user.username</value>
      <webElementGuid>d0fdb8d9-ba1f-4480-a1bf-cb694eddf2ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>hasError('username') ? 'pe-textInput--input_error' : 'pe-textInput'</value>
      <webElementGuid>a6a818c7-097c-47d1-8d58-9dd8b82870d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>246dc2a8-38e7-4d31-93a4-5d0ffb3c5269</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>762d3f8b-a510-4a40-bf68-c8c207fd1c17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>a06e8d13-96d7-4f9b-a681-8134909d96fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>2a8cf944-fb59-43fb-8b51-7aeca0cf5c3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>def355cb-0672-4518-b397-dcfe7b7b90eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-change</name>
      <type>Main</type>
      <value>usernameChanged()</value>
      <webElementGuid>016737cb-24ee-48ff-96f9-fc507bdf4436</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required pe-textInput</value>
      <webElementGuid>57e5512c-bf6d-4b5f-a7ab-a21fa3f2b678</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>874da863-3997-4d37-b37d-be4ef438bb55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>b6731f3b-8c9b-49fa-8b81-1b8bd65185e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='username']</value>
      <webElementGuid>951a7d05-6be3-4616-9aad-3908ec714b7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mainForm']/div[2]/div/input</value>
      <webElementGuid>c8f0e816-6910-4fa0-8519-4e54bc5b2d36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>f2903589-33e9-42ba-8fd1-98a083685cba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'username' and @name = 'username']</value>
      <webElementGuid>16720f41-db24-495b-96b1-80e122ecc508</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
