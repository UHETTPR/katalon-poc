<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Estimated time0s_chk990081.1.3.6.25.0</name>
   <tag></tag>
   <elementGuidId>8da677f8-a0f4-45e7-b0bf-d0f65d1d29ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990081.1.3.6.25.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1ee00a7b-d779-438b-9ce4-d38df578718c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>5c394b5f-a54f-4b55-9334-458204b459d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990081.1.3.6.25.0</value>
      <webElementGuid>775f5fb1-12d4-4372-ba5b-8788718f8fd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>1.3.6 (Section 1.3: The Economic Way of Thinking)</value>
      <webElementGuid>17f8f62d-9719-4a44-abd8-3f57b3f67747</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990081.1.3.6.25.0&quot;)</value>
      <webElementGuid>69bc3cb0-3a74-4f49-b0d5-8948c8322715</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990081.1.3.6.25.0']</value>
      <webElementGuid>708224e3-8f02-4670-b304-6b2b6491cec7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990081.1.3.6.25.0']/div/div[2]/input</value>
      <webElementGuid>c4e51a9b-4fd3-4d55-a837-bd7e794c612c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/div[2]/input</value>
      <webElementGuid>72772504-d799-4096-b3ba-47bab21d6561</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990081.1.3.6.25.0' and @title = '1.3.6 (Section 1.3: The Economic Way of Thinking)']</value>
      <webElementGuid>8b3be50e-9f0f-47cd-9091-dd9f138be625</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
