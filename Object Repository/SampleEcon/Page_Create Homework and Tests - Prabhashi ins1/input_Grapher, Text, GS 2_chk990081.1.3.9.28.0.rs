<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Grapher, Text, GS 2_chk990081.1.3.9.28.0</name>
   <tag></tag>
   <elementGuidId>97fdf160-00ea-4dd5-a3ee-afb9cce3ee0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990081.1.3.9.28.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1b90fe14-297e-4268-b4a1-358c3ef371d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>b824f535-e9bd-427c-aefa-f210575d7a40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990081.1.3.9.28.0</value>
      <webElementGuid>ef4411f2-40e9-4086-9f7f-6d4e701b82e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>1.3.9 (Section 1.3: The Economic Way of Thinking)</value>
      <webElementGuid>c53b90b6-3159-4520-9344-83cfed2e60dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990081.1.3.9.28.0&quot;)</value>
      <webElementGuid>62a39c19-937b-403e-8f05-3418e19a471f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990081.1.3.9.28.0']</value>
      <webElementGuid>2bd41793-71f9-4b37-9c18-67884b8ffbfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990081.1.3.9.28.0']/div/div[2]/input</value>
      <webElementGuid>ca89f9ac-f45c-4773-8ae7-bf619995bad7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/div/div[2]/input</value>
      <webElementGuid>eae57c5a-1fc8-4265-97fb-6e857edd5448</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990081.1.3.9.28.0' and @title = '1.3.9 (Section 1.3: The Economic Way of Thinking)']</value>
      <webElementGuid>0fea53bd-8d40-465e-be1f-c06a64b95130</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
