<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Estimated time0s_chk990081.2.2.5.37.0</name>
   <tag></tag>
   <elementGuidId>30b83f9a-9cd6-4cda-97c1-42c800588b98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk990081.2.2.5.37.0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b49387cb-00f9-49c1-b75d-773ee24dd6fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>f3d12e58-3e0e-44ff-81a7-5873d123107a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk990081.2.2.5.37.0</value>
      <webElementGuid>2338ad9e-ac5b-416d-af4a-5c6f3678913a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>2.2.5 (Section 2.2: Using Resources Efficiently)</value>
      <webElementGuid>57cee40f-2764-4a39-bc9e-bfa99556b30e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk990081.2.2.5.37.0&quot;)</value>
      <webElementGuid>753715d9-e05c-40c2-b465-510e2620fe2f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk990081.2.2.5.37.0']</value>
      <webElementGuid>59d41c9c-9c6f-4012-b62d-560b0d8c41b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='990081.2.2.5.37.0']/div/div[2]/input</value>
      <webElementGuid>57f4b0ac-505e-42dd-9c07-2a8748de14a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div/div[2]/input</value>
      <webElementGuid>1c128baf-e9c7-4b97-95e6-31d6767110df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk990081.2.2.5.37.0' and @title = '2.2.5 (Section 2.2: Using Resources Efficiently)']</value>
      <webElementGuid>63b34fed-7bd2-49e8-8708-e755c886b52d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
