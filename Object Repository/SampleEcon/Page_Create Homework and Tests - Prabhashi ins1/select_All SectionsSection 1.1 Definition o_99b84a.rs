<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SectionsSection 1.1 Definition o_99b84a</name>
   <tag></tag>
   <elementGuidId>2ccf7acc-aa71-4a82-b4a3-cb09b0b93ed2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>adc6d0bc-ceef-4245-b010-0b3be477d367</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>cffba083-7fdd-4810-a677-7d7fdd3bc7ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>d511cffb-2eb4-4991-9621-ffc48fed0597</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>d35cbfd7-8c65-46f6-bb7b-10a8747e676e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>var selValue = this.options[this.selectedIndex].value; ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection',''); ExSelectionsIntf.ChangedSection(selValue); }); return false;</value>
      <webElementGuid>24eae1ad-d028-48bd-acf1-638667dc567b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>119ad45f-aef9-4396-abbf-a3c6681bddd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				All Sections
				
				
				
				
				
				
				
				

			Section 1.1: Definition of EconomicsSection 1.2: Two Big Microeconomic QuestionsSection 1.3: The Economic Way of ThinkingSection 1.4: Economics: A Social ScienceSection 1.5: Appendix: Graphing DataSection 1.6: Appendix: Graphs Used in Economic ModelsSection 1.7: Appendix: The Slope of a RelationshipSection 1.8: Appendix: Graphing Relationships Among More Than Two Variables</value>
      <webElementGuid>e254d78c-304e-41e2-b6f8-d22eac66f7ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>958cb54f-f114-4b07-a9ed-3aba98cdf178</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>c5b3e923-3481-4b96-8663-f1e8c80f15b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>6905961a-ceeb-473c-a3cd-493a18520590</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section'])[1]/following::select[1]</value>
      <webElementGuid>7913faa1-bf21-4ee5-8330-f4e72d4b3b7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter'])[1]/following::select[2]</value>
      <webElementGuid>59c18803-9c79-494c-aae1-a11901f07027</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[1]</value>
      <webElementGuid>c9154167-bf6f-436e-b0ee-986c2f9c4de6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>f5b12e25-c35b-43d9-9c0b-1c996bc7ee7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
				All Sections
				
				
				
				
				
				
				
				

			Section 1.1: Definition of EconomicsSection 1.2: Two Big Microeconomic QuestionsSection 1.3: The Economic Way of ThinkingSection 1.4: Economics: A Social ScienceSection 1.5: Appendix: Graphing DataSection 1.6: Appendix: Graphs Used in Economic ModelsSection 1.7: Appendix: The Slope of a RelationshipSection 1.8: Appendix: Graphing Relationships Among More Than Two Variables' or . = '
				All Sections
				
				
				
				
				
				
				
				

			Section 1.1: Definition of EconomicsSection 1.2: Two Big Microeconomic QuestionsSection 1.3: The Economic Way of ThinkingSection 1.4: Economics: A Social ScienceSection 1.5: Appendix: Graphing DataSection 1.6: Appendix: Graphs Used in Economic ModelsSection 1.7: Appendix: The Slope of a RelationshipSection 1.8: Appendix: Graphing Relationships Among More Than Two Variables')]</value>
      <webElementGuid>cdb8d37b-db84-4e9b-b9a7-08867a1ed7b9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
