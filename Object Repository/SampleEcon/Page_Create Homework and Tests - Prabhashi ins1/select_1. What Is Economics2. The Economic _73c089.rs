<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_1. What Is Economics2. The Economic _73c089</name>
   <tag></tag>
   <elementGuidId>d212eb16-ccc5-4d36-aeb8-9a1984b9d64d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListChapter</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListChapter']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>cd376827-e7c0-4e47-b3b2-baa5c4b1b651</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter</value>
      <webElementGuid>d9f3efac-3d88-4034-8b97-5229e41c2eb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter','') }); return false;setTimeout('__doPostBack(\'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter\',\'\')', 0)</value>
      <webElementGuid>a4779712-574a-4cd1-9352-90c3101e2350</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListChapter</value>
      <webElementGuid>a4b72f0c-47f9-4e2a-be6e-9c70bbb859ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>d762ef51-c4fa-4c15-8c50-9c593585c0eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>0fc41cfd-4fe4-4f2a-a2fd-c5af6cb54788</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				1. What Is Economics?
				2. The Economic Problem
				3. Melissa Honig's Chapter
				E. MyEconLab Experiments
				D. Digital Interactives
				DSM. Dynamic Study Modules

			</value>
      <webElementGuid>dacfe05d-7685-44f8-8ce1-3ba153a19cfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListChapter&quot;)</value>
      <webElementGuid>168a0ec1-cd29-4bce-a3c1-b6b995b8063a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListChapter']</value>
      <webElementGuid>e0d8840b-50ef-4835-95e3-b8b33a09c5eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrChapter']/td[2]/div/select</value>
      <webElementGuid>0b720e35-89dc-4e92-b444-fb980a90ebd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter'])[1]/following::select[1]</value>
      <webElementGuid>02f034f5-bd52-4783-a922-f05aaf62bd1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change...'])[1]/following::select[1]</value>
      <webElementGuid>7b9004f8-c4c5-4a3f-b1c9-cf9e20541b97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section'])[1]/preceding::select[1]</value>
      <webElementGuid>8b6d7c54-e804-4092-ab66-a80bf66c0ec4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[2]</value>
      <webElementGuid>3bfc4a0f-5f59-4fcd-b27f-450c81d76e06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/select</value>
      <webElementGuid>6211b8d1-ffd9-4b2f-bb9f-7ffe7cf050c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListChapter' and @id = 'DropDownListChapter' and (text() = concat(&quot;
				1. What Is Economics?
				2. The Economic Problem
				3. Melissa Honig&quot; , &quot;'&quot; , &quot;s Chapter
				E. MyEconLab Experiments
				D. Digital Interactives
				DSM. Dynamic Study Modules

			&quot;) or . = concat(&quot;
				1. What Is Economics?
				2. The Economic Problem
				3. Melissa Honig&quot; , &quot;'&quot; , &quot;s Chapter
				E. MyEconLab Experiments
				D. Digital Interactives
				DSM. Dynamic Study Modules

			&quot;))]</value>
      <webElementGuid>0c43852c-2afb-4be2-b958-389ae5af3616</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
