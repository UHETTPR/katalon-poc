<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_All SectionsSection 2.1 Production P_24cb40</name>
   <tag></tag>
   <elementGuidId>152bcf00-a91e-48bf-a40d-979ff25e7b75</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#DropDownListSection</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='DropDownListSection']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b74aa4d2-2db1-4b9c-b80b-350cc74e2820</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection</value>
      <webElementGuid>e569f120-04f6-4000-a08b-4a918b96bb9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>DropDownListSection</value>
      <webElementGuid>508c5e6e-29a8-4c10-b26d-9620b53f6446</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wizardselect bigdrop</value>
      <webElementGuid>30dc83df-9fd6-47cf-9e5e-e58d33805a0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>var selValue = this.options[this.selectedIndex].value; ExSelectionsIntf.CheckAddSelections(function(){ __doPostBack('ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection',''); ExSelectionsIntf.ChangedSection(selValue); }); return false;</value>
      <webElementGuid>94ed9662-9b65-4b48-95be-4cb12ecbe7a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		All Sections
		Section 2.1: Production Possibilities and Opportunity Cost
		Section 2.2: Using Resources Efficiently
		Section 2.3: Economic Growth
		Section 2.4: Gains from Trade

	</value>
      <webElementGuid>7cabff46-6350-4cf4-b8ee-5f33e9678c83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DropDownListSection&quot;)</value>
      <webElementGuid>323b2426-34c0-4884-b83d-6cf07adaa05b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='DropDownListSection']</value>
      <webElementGuid>c6148fd5-9284-4a6e-962f-4dd9eca95d67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_ExFilter_TrSection']/td[2]/div/select</value>
      <webElementGuid>6ae5cb4d-6046-4bd9-b69f-596bcd49d598</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section'])[1]/following::select[1]</value>
      <webElementGuid>7ba1c7cd-6756-45f3-8ae8-fe78a44dc88d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter'])[1]/following::select[2]</value>
      <webElementGuid>d6157937-aa63-4405-a336-dadeca4587c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability'])[1]/preceding::select[1]</value>
      <webElementGuid>4a146ea5-60bc-48ef-9a5e-92b8f467afe1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/div/select</value>
      <webElementGuid>da4a26f7-bcb6-432f-8ff8-ac4db20ea2e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$ExFilter$DropDownListSection' and @id = 'DropDownListSection' and (text() = '
		All Sections
		Section 2.1: Production Possibilities and Opportunity Cost
		Section 2.2: Using Resources Efficiently
		Section 2.3: Economic Growth
		Section 2.4: Gains from Trade

	' or . = '
		All Sections
		Section 2.1: Production Possibilities and Opportunity Cost
		Section 2.2: Using Resources Efficiently
		Section 2.3: Economic Growth
		Section 2.4: Gains from Trade

	')]</value>
      <webElementGuid>8d89310d-f883-47c1-8e73-44286f9e530c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
