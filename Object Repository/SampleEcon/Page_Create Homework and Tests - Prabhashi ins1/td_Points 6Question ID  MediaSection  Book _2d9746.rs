<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Points 6Question ID  MediaSection  Book _2d9746</name>
   <tag></tag>
   <elementGuidId>0b3cf15c-42ec-4cbe-a560-8e5328b183d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='ctl00_ctl00_InsideForm_MasterContent_tdSelectedQuestionsHeader']/table/tbody/tr[2]/td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>4270e427-efdc-4e62-b590-59a48b5bc6b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							
							Points: 6

							
							
							#
							

							
								
									
									
								Question ID / Media

								
									
								
							
							
								
									Section / Book Association
									
								
								 
							Estimated time:153m 11s
							
							</value>
      <webElementGuid>bd9af7e7-b761-45ce-b200-501d742c092b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_tdSelectedQuestionsHeader&quot;)/table[@class=&quot;listheader xlbootstrap3&quot;]/tbody[1]/tr[2]/td[1]</value>
      <webElementGuid>4c54ca88-9412-490d-b665-4700b7e891a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='ctl00_ctl00_InsideForm_MasterContent_tdSelectedQuestionsHeader']/table/tbody/tr[2]/td</value>
      <webElementGuid>0976d52e-6cfb-432d-ab65-26681f70ef58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Assignment Details'])[1]/following::td[1]</value>
      <webElementGuid>23c767a6-7b78-483e-98fb-b3956867bf66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/table/tbody/tr[2]/td</value>
      <webElementGuid>5b66a373-92da-4d01-8789-cbccca1d8ec6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
							
							Points: 6

							
							
							#
							

							
								
									
									
								Question ID / Media

								
									
								
							
							
								
									Section / Book Association
									
								
								 
							Estimated time:153m 11s
							
							' or . = '
							
							Points: 6

							
							
							#
							

							
								
									
									
								Question ID / Media

								
									
								
							
							
								
									Section / Book Association
									
								
								 
							Estimated time:153m 11s
							
							')]</value>
      <webElementGuid>8689f786-4fec-4c11-94b8-97a13c0980d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
