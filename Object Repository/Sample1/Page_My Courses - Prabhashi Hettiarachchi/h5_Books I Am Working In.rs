<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Books I Am Working In</name>
   <tag></tag>
   <elementGuidId>1fd92af0-1369-47d8-804a-694f58d76ae0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_divBackground']/table/tbody/tr/td/h5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>5d7b5eb8-a472-4e98-8fe5-dec2e8a92218</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Books I Am Working In</value>
      <webElementGuid>24f46443-bbee-4e2f-b15d-94df1390417a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_divBackground&quot;)/table[@class=&quot;table container&quot;]/tbody[1]/tr[1]/td[1]/h5[1]</value>
      <webElementGuid>06bd6daa-b957-4d34-aae7-b53fad549efa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_divBackground']/table/tbody/tr/td/h5</value>
      <webElementGuid>3cbbec5b-ba78-4040-a4dd-f66d3c69165e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SMS Default Institution'])[1]/following::h5[1]</value>
      <webElementGuid>e5212f2e-d1d0-4ee1-bdfb-277af31b19b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to MathXL, Prabhashi'])[1]/following::h5[1]</value>
      <webElementGuid>281148a7-4dcc-480b-8885-67a95e298f3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NextGen Media Math Test Book--REAL'])[1]/preceding::h5[1]</value>
      <webElementGuid>806ac36d-593f-470e-ba9c-2a7263250820</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enroll in a new course'])[1]/preceding::h5[1]</value>
      <webElementGuid>76ae8c48-753c-44e5-a31d-a838e0f04e51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Books I Am Working In']/parent::*</value>
      <webElementGuid>f29cf9be-ad8d-44fb-a05d-283180c9c36e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5</value>
      <webElementGuid>baa6d31f-2644-4cdf-a3ee-3f9414dfa848</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Books I Am Working In' or . = 'Books I Am Working In')]</value>
      <webElementGuid>7d4f0499-324b-4f62-a489-6dfda11012f3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
