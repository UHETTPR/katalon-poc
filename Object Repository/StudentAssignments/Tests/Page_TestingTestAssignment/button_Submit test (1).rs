<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit test (1)</name>
   <tag></tag>
   <elementGuidId>c08ae6a8-ad0b-454b-9fc8-a663eb8be0cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_7</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_dijit-bootstrap_Button_') and @type = 'button' and (text() = 'Submit test' or . = 'Submit test')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6c8c04d8-3fd4-402f-9d06-15cbd38f1ec3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b8a27540-b723-4b04-8e05-78e2ebfc8ce2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>238693c7-bfb4-4fc9-b563-031ec6faf330</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>bfba9e86-b848-47ea-ae6c-c73721f65863</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_7</value>
      <webElementGuid>45825f23-5424-48f7-b323-94cb5f10adc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_7</value>
      <webElementGuid>d79e4b51-bb76-4539-b5a3-554af9e748e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit test</value>
      <webElementGuid>5e51b616-67a9-4348-9076-bdabd3f792ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_7&quot;)</value>
      <webElementGuid>0fd9cc86-a527-40f4-ba84-826695cf82c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentAssignments/Tests/Page_TestingTestAssignment/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40</value>
      <webElementGuid>05ed9549-ebbd-42c8-86b0-cb7a738d5d49</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_7']</value>
      <webElementGuid>c983c825-3e32-4d27-89aa-c07aa39a61be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_ConfirmationDialog_0']/div/div[4]/div[2]/button[2]</value>
      <webElementGuid>0ba29dcb-e3cb-4d23-8aed-507de09477f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]</value>
      <webElementGuid>2ab5fc9e-ea37-4ddd-9a29-bcbde19d43e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='pop-up content ends'])[1]/following::button[2]</value>
      <webElementGuid>f6f2ea40-41f1-40f6-91d7-00eef1d5eb49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Submit test']/parent::*</value>
      <webElementGuid>8b0457c9-207f-4b1b-844f-c5c5c0ebd842</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button[2]</value>
      <webElementGuid>7965fda3-3359-4ee1-9766-004fa4a83d15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_7' and (text() = 'Submit test' or . = 'Submit test')]</value>
      <webElementGuid>e79cc08d-c2c3-42a7-b345-1801691a9383</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
