<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Return to Homework_activityFrame</name>
   <tag></tag>
   <elementGuidId>cdc34939-86af-4680-ac0a-21604585a078</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#activityFrame</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='activityFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>b380277a-020f-4cb6-8756-7082bc0fef4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>285ba4df-ec30-4641-882d-25da870f9621</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>a0211a93-b56e-49f3-840d-2d2245073b56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://xlitemcert.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true</value>
      <webElementGuid>fdb200f2-3321-4e99-b5ba-dd7be26f6044</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>27866bd2-b61e-458c-89fd-ad11fac852fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Homework Overview : Homework: TestingHomeworkAssignment</value>
      <webElementGuid>af4b06e0-44c4-4247-b7a6-1f3825004987</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>webkitallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>f2c226d8-5783-4e75-8997-da56e0cefd7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mozallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>b4fcbaf7-8806-4c19-be8d-68b9cf0708b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>f7cb1ee0-bf2b-4689-ae82-19caba927dd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;activityFrame&quot;)</value>
      <webElementGuid>7019da1a-4788-4604-93f2-60852f11b3a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentAssignments/Homework/Page_Do Homework - TestingHomeworkAssignment/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40</value>
      <webElementGuid>4bcfe644-7f11-40e6-b52b-c359d859e644</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='activityFrame']</value>
      <webElementGuid>e518e528-06f8-4db0-825b-e5b87a5280ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assignmentViewer']/div/div[3]/table/tbody/tr/td/div/iframe</value>
      <webElementGuid>065c974c-3f81-48e5-ae4e-dd607d02ccb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>2665a5cb-5c43-4a77-9449-c0c93b30cb62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@id = 'activityFrame' and @src = 'https://xlitemcert.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true' and @title = 'Homework Overview : Homework: TestingHomeworkAssignment']</value>
      <webElementGuid>5cf30314-84f4-403e-a622-97c7605abe3f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
