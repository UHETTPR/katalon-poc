<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign in</name>
   <tag></tag>
   <elementGuidId>81a78e53-b4c8-475d-a738-fcc8d3b08f9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#mainButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='mainButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>332f0334-e54c-4c42-b28b-fce87f50b48b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>0c0a63d9-2bf4-4712-8c7e-e7ebab08342a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mainButton</value>
      <webElementGuid>d5952ea5-d902-4f3f-9af7-ad22735312ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-if</name>
      <type>Main</type>
      <value>!signingIn</value>
      <webElementGuid>5b280f87-fdf7-4e61-ba8c-13b18b4082da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pe-btn__cta--btn_xlarge full-width ng-binding ng-scope</value>
      <webElementGuid>e58e521e-70d9-460c-a42a-e53f26839cd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-class</name>
      <type>Main</type>
      <value>getCustomButtonClassname()</value>
      <webElementGuid>d53c2ba8-d1b4-4ac5-ad2a-0e7c276eb002</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>fa11c6b7-cbb9-42d4-a269-f1abbaf16290</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainButton&quot;)</value>
      <webElementGuid>0a301cdc-9919-4bdb-aa4e-9d7e2d69731e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='mainButton']</value>
      <webElementGuid>65e9e7e6-2258-4d9d-93a7-8e37ca929dfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mainForm']/div[5]/button</value>
      <webElementGuid>31d6ac23-fc02-4e1e-9196-e9b937e58d72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your username or password?'])[1]/following::button[1]</value>
      <webElementGuid>3ce00a19-8003-4eba-9ef3-5099d6759f19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password is hidden'])[1]/following::button[1]</value>
      <webElementGuid>ff4697b8-3ee6-474d-9f2e-4e51f1c40bac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::button[1]</value>
      <webElementGuid>8c143fbb-6b33-43f5-8fc8-c94313162e8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/button</value>
      <webElementGuid>ed4db630-4a85-4c95-a0b7-471976d3e0bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @id = 'mainButton' and (text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>8be0b505-6298-4096-9ac5-05d30a3b12b9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
