<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_All Assignments</name>
   <tag></tag>
   <elementGuidId>36a6726d-e4ca-4491-a4b5-6ac5b4e7ca4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.PagerText</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='gridtitle']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b3c54ba4-cc63-495d-ad0d-9dbb2d68e6aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PagerText</value>
      <webElementGuid>7e2f62e7-dc5c-4e82-8d06-1ab8b42fc6f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					            All Assignments
					        </value>
      <webElementGuid>8e6dcf22-4d90-4082-9ea6-20cd80033947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gridtitle&quot;)/span[@class=&quot;PagerText&quot;]</value>
      <webElementGuid>229423d9-20f6-4117-8362-53140f76af00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//h3[@id='gridtitle']/span</value>
      <webElementGuid>58a6a11b-2fd3-454c-8cb2-f89562a9b9f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[2]/following::span[1]</value>
      <webElementGuid>784dc252-dd61-44e0-9b71-d6c65a281932</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[1]/following::span[2]</value>
      <webElementGuid>c41df3e1-3305-4d1c-bd4d-14902466736c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Due'])[1]/preceding::span[1]</value>
      <webElementGuid>b28bfb9d-722d-43bd-a5d2-8f1192a461c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prereq'])[1]/preceding::span[1]</value>
      <webElementGuid>c3b06ce4-a8ed-4171-a2ca-656621277ebe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3/span</value>
      <webElementGuid>109d7476-aed5-4f32-bc0b-14862caa2fac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
					            All Assignments
					        ' or . = '
					            All Assignments
					        ')]</value>
      <webElementGuid>1991994b-7a4d-4a64-9479-69a7d876511c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
