<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Warning            You are reviewing yo_14d351</name>
   <tag></tag>
   <elementGuidId>33fd3400-6b47-4644-aa15-9ad6d94cb2da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#dvAlertWarning</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dvAlertWarning']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4865bb0d-7db3-4653-b103-6901b67013fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dvAlertWarning</value>
      <webElementGuid>9ff7024c-0776-4ab5-8ee2-ed2ddc2abd50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-warning</value>
      <webElementGuid>3314f48b-ac29-44df-93d9-13c02630f8b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Warning:
   
        
	 You are reviewing your homework. Any work you do here will NOT change your score.</value>
      <webElementGuid>99b2dc90-11d0-40bc-8c30-3f1055c8fa9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dvAlertWarning&quot;)</value>
      <webElementGuid>5fde3926-692c-40df-ad28-9a3d899ee174</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='dvAlertWarning']</value>
      <webElementGuid>43cb6019-7856-49fa-b47c-961c710c9446</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div[3]</value>
      <webElementGuid>cd8838b6-ac2d-44d2-b175-44281e60e783</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::div[1]</value>
      <webElementGuid>e9c9be8f-6bbb-425c-a0b5-67bf15cc3ebe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Review Homework'])[1]/following::div[1]</value>
      <webElementGuid>92d58a11-027c-40de-9068-362a5f4664ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[3]</value>
      <webElementGuid>1744f6ff-c89e-4258-917a-26f31fab6345</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'dvAlertWarning' and (text() = '
        Warning:
   
        
	 You are reviewing your homework. Any work you do here will NOT change your score.' or . = '
        Warning:
   
        
	 You are reviewing your homework. Any work you do here will NOT change your score.')]</value>
      <webElementGuid>be350f98-2824-49e7-9ba7-fc85c6a31cd7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
