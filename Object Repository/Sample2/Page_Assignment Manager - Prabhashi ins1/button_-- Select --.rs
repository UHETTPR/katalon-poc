<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_-- Select --</name>
   <tag></tag>
   <elementGuidId>8c185936-6083-40b1-8402-75ac90d867b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@id='ActionList'])[9]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1304b9be-f521-4770-9ebe-446435bc2fc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ActionList</value>
      <webElementGuid>566538c1-aba9-4081-b9b9-12c5544491e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ac95fea6-a75d-4446-8c08-5cfd06fec3e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default dropdown-toggle</value>
      <webElementGuid>5995a851-0e6a-4261-a904-bf5c0761f8cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>78edbe87-6ce4-4e0f-aba3-0ff571cd4a8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>75f3ba54-3166-4cf6-bf4b-0458a234808f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>7834e3db-5fed-4e73-b7b4-cdf294e20e55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	        -- Select --
		    
	    </value>
      <webElementGuid>c5bf1bee-494f-4b7f-94fe-dd1aa7b33f5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[10]/td[@class=&quot;grid-Assignment-Action-Dropdown&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;btn-group dropdown-behavior action-dropdown-controller&quot;]/button[@id=&quot;ActionList&quot;]</value>
      <webElementGuid>824ae957-cbf7-4bb8-b354-6b23389c9cd9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@id='ActionList'])[9]</value>
      <webElementGuid>406086e9-ba37-4728-854e-525a65a4700c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[10]/td[10]/div/div/button</value>
      <webElementGuid>218b1950-19aa-4927-a561-1ac13b8f087a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[9]/following::button[1]</value>
      <webElementGuid>1bee339f-af1d-4c96-bd74-bd8a40354c8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SampleWS2'])[1]/following::button[1]</value>
      <webElementGuid>4ee798f2-e359-4c39-aa7c-d21ccf464d09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit...'])[9]/preceding::button[1]</value>
      <webElementGuid>5b02871b-95b8-4ca5-9dbb-c24a032a36e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[10]/td[10]/div/div/button</value>
      <webElementGuid>ec51201d-d695-40a6-a1c7-edb3a036f8a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'ActionList' and @type = 'button' and (text() = '
	        -- Select --
		    
	    ' or . = '
	        -- Select --
		    
	    ')]</value>
      <webElementGuid>61f9818e-58c2-4583-9a57-bb59bf7db3f7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
