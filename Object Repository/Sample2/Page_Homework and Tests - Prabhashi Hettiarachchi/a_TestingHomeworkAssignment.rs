<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_TestingHomeworkAssignment</name>
   <tag></tag>
   <elementGuidId>4daca586-c1b1-476d-b038-b4e9133f3329</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[9]/th/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>45941031-cc7d-4ff8-a39b-0b0471f136a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doHomework(19688800, false, true);</value>
      <webElementGuid>69349a12-5d95-45e4-8f29-b64cdcf34d55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestingHomeworkAssignment</value>
      <webElementGuid>1d009fec-3ad7-468e-912c-08a04f278f4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[9]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>21e95cb0-b04d-48c2-9703-11cd2b00743d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[9]/th/a</value>
      <webElementGuid>e82d6cdf-1b2a-4c52-adf5-765d7da42bee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'TestingHomeworkAssignment')]</value>
      <webElementGuid>a7942c57-c165-4dca-88e9-25e00fe78d02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[9]/following::a[1]</value>
      <webElementGuid>22261f3f-364d-48be-9fc3-64c68aa438ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preassigned Homework'])[1]/following::a[1]</value>
      <webElementGuid>e0a9edb3-cbd5-4ae9-990f-84d237abea19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quiz'])[1]/preceding::a[1]</value>
      <webElementGuid>b82bdf0e-f840-4a9d-b765-6f59b4b47afa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingQuizAssignment'])[1]/preceding::a[1]</value>
      <webElementGuid>558bb6c0-77e9-4204-88eb-7b7724bb9cd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='TestingHomeworkAssignment']/parent::*</value>
      <webElementGuid>1005086a-68bb-44f5-8a23-2d28cb4a5d47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doHomework(19688800, false, true);')]</value>
      <webElementGuid>8dd73f8e-4e61-404e-9b26-670e5e05eb41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[9]/th/a</value>
      <webElementGuid>623336a4-9610-462b-abad-ca7fcdd65037</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doHomework(19688800, false, true);' and (text() = 'TestingHomeworkAssignment' or . = 'TestingHomeworkAssignment')]</value>
      <webElementGuid>51290872-8a95-4bc3-bfb5-e501de97b120</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
