<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_TestingWSAssignment</name>
   <tag></tag>
   <elementGuidId>77b90744-6e21-44a6-9b78-bafacd7e3a43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'TestingWSAssignment')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e8d27f32-0e0f-4834-a9b8-73f5821f0e8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doWritingSpace('do',81877601);</value>
      <webElementGuid>bcea5034-fbbf-4b1e-a478-1e61c246b028</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestingWSAssignment</value>
      <webElementGuid>6fefbfe5-0736-4f60-b6a2-8d084ddd492c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[9]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>3ede54cc-4eab-414b-9cbe-6dfa77e43d02</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[9]/th/a</value>
      <webElementGuid>23338aa0-a039-4ad1-b7fa-6eb34d8477f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'TestingWSAssignment')]</value>
      <webElementGuid>86e134a8-df09-4020-abd8-7a91671c4b61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[9]/following::a[1]</value>
      <webElementGuid>bfb731da-e7f6-4313-99a1-0b41a96fc72f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='media with question'])[1]/following::a[1]</value>
      <webElementGuid>9d53e069-f0d3-4a66-bf06-52e5b7e44b10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/preceding::a[1]</value>
      <webElementGuid>9dba390a-92d1-489c-bd55-57f1e7d9feae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prerequisite Warning'])[1]/preceding::a[1]</value>
      <webElementGuid>320d63f4-753f-4ebb-969c-19c3d7c77cc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='TestingWSAssignment']/parent::*</value>
      <webElementGuid>0f0c880f-4876-475b-8a0a-9e5a1d18cfae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:doWritingSpace('do',81877601);&quot;)]</value>
      <webElementGuid>790fac1a-1ee3-43c1-98eb-a17e00d05d51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[9]/th/a</value>
      <webElementGuid>ed3a4553-02a4-46d9-8fa5-1a903418181f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:doWritingSpace(&quot; , &quot;'&quot; , &quot;do&quot; , &quot;'&quot; , &quot;,81877601);&quot;) and (text() = 'TestingWSAssignment' or . = 'TestingWSAssignment')]</value>
      <webElementGuid>514a801c-bc48-48f5-aeb0-57db1cc74a19</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
