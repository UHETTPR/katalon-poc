<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Close Assignment</name>
   <tag></tag>
   <elementGuidId>16f43bec-48e3-4d64-b784-efc6bb0e6899</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@href = '#' and (text() = 'Close Assignment' or . = 'Close Assignment')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.st-submit-actions > div.st-rub-link > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>aa5af3b4-2f70-4e38-bdc2-7c4379826668</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>bc69c9e8-5728-4eb3-b1fb-647d75baba87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Close Assignment</value>
      <webElementGuid>ff0f9e16-373a-4f5f-a101-315e3be7885b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wsol-wrapper&quot;]/div[@class=&quot;p-container&quot;]/div[@class=&quot;wsol-row&quot;]/div[@class=&quot;wsol-st-head-wrap&quot;]/div[@class=&quot;st-actions&quot;]/div[@class=&quot;st-submit-actions&quot;]/div[@class=&quot;st-rub-link&quot;]/a[1]</value>
      <webElementGuid>2bed05bc-290c-4977-abac-b6afd9628e39</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div[2]/div/div[2]/div/div/a</value>
      <webElementGuid>4cfe0006-55c0-4267-9ba1-60a290c8f558</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Close Assignment')]</value>
      <webElementGuid>a139160e-cd9f-4902-8ebd-8c6699039dcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supporting files'])[1]/following::a[2]</value>
      <webElementGuid>1d048d41-eccf-433a-b385-c60fb632536d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your score is based on:'])[1]/following::a[3]</value>
      <webElementGuid>7df0820c-2283-4ae3-a6e4-39bd0dbdf258</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saved'])[1]/preceding::a[4]</value>
      <webElementGuid>006daee3-cb4d-4dbf-aed7-7bd1f336a4ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Close Assignment']/parent::*</value>
      <webElementGuid>586e0941-9a5b-4a21-a488-ef9aa78aad23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[2]</value>
      <webElementGuid>365cd67e-320a-4a54-bb1f-66a64a8e459e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/a</value>
      <webElementGuid>46bc347e-9832-4aa8-9e88-e0074320c531</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'Close Assignment' or . = 'Close Assignment')]</value>
      <webElementGuid>0419ff73-c5fe-4d2a-98ca-051d23bf7de1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
