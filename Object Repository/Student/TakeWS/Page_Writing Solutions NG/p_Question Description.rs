<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Question Description</name>
   <tag></tag>
   <elementGuidId>a8d01e3c-8a86-44f7-8aa8-5db8662cef2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = 'Question Description' or . = 'Question Description')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.st-instructions-val > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>91869d48-cd80-4b5f-91a1-4a0ac2f17d77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Question Description</value>
      <webElementGuid>c2e73bae-833d-417f-8d96-acf011997d20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wsolHiddenInfo&quot;)/div[@class=&quot;wsol-stu-details-sub-info&quot;]/span[@class=&quot;st-instructions-val&quot;]/p[1]</value>
      <webElementGuid>3678dbdf-1a4e-45de-9a5b-9259e43ab84b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wsolHiddenInfo']/div/span[2]/p</value>
      <webElementGuid>69391308-5509-4772-aeed-8c2c15c8d41d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Instructions'])[1]/following::p[1]</value>
      <webElementGuid>de74c564-6a30-454a-b72d-d398155e3477</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning objective'])[1]/following::p[2]</value>
      <webElementGuid>3f7a05df-3ac4-4463-a3b9-22bb9021dbfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your score is based on:'])[1]/preceding::p[1]</value>
      <webElementGuid>f31e4f57-5600-4dc7-95b4-2266d4196005</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supporting files'])[1]/preceding::p[1]</value>
      <webElementGuid>1a0f864a-02ac-4185-89a8-ccc356959bf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Question Description']/parent::*</value>
      <webElementGuid>b8a0d152-1a06-4ed9-8284-7ccea48c8475</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/p</value>
      <webElementGuid>521e6fb2-b7b9-438c-886f-df9ebeab51b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Question Description' or . = 'Question Description')]</value>
      <webElementGuid>38ceb7ba-69ca-4ccb-ade4-e06e10795ae2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
