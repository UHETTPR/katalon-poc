<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Correct 5</name>
   <tag></tag>
   <elementGuidId>26baf6f3-8bf1-442f-8397-3938a588b306</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='exPanel']/section/ul/li[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>0daa9dc0-abcb-488e-846c-81378d4283fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Correct: 5</value>
      <webElementGuid>f45134ea-54bf-4986-abb6-19bdab16f008</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exPanel&quot;)/section[@class=&quot;table-score-details overviewtotalstest&quot;]/ul[1]/li[2]</value>
      <webElementGuid>5bc9c0eb-1e92-40b2-bdd4-d7b4dd808acc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='exPanel']/section/ul/li[2]</value>
      <webElementGuid>f7c724df-e753-4a45-9ddc-fa3c48b4d536</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Questions: 5'])[1]/following::li[1]</value>
      <webElementGuid>2b43812e-59d7-4f76-8fff-e8cc02860444</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='will'])[1]/following::li[2]</value>
      <webElementGuid>78ed28d9-7707-4a33-940d-12c9cc7cd2a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partial Credit: 0'])[1]/preceding::li[1]</value>
      <webElementGuid>4f5fa680-f122-4889-8075-080ea5befea1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Incorrect: 0'])[1]/preceding::li[2]</value>
      <webElementGuid>111999aa-222b-44af-85d1-da515f2a125c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Correct: 5']/parent::*</value>
      <webElementGuid>36697792-9360-4812-909c-981ed3533453</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[2]</value>
      <webElementGuid>74a93cb7-d12f-4792-9806-cabdb4b81ee2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Correct: 5' or . = 'Correct: 5')]</value>
      <webElementGuid>83e41bef-fb50-43fc-916e-3891668d99e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
