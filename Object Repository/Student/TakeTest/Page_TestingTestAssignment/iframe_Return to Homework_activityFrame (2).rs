<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Return to Homework_activityFrame (2)</name>
   <tag></tag>
   <elementGuidId>52549c0a-6c9e-4ff3-8e08-6846d2904b19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#activityFrame</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='activityFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>fb3f6060-6fa7-4f52-beff-fb8b79dd0bf3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>2c51ceed-0427-4f19-8359-a412a01743df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>052ef9d0-3134-4b41-8b32-2c3d67e80771</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://xlitemppe.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true</value>
      <webElementGuid>bd07881d-c9f1-4116-834a-d20e86e2d722</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>3561a4f3-c897-4785-b98e-6323bcac7fec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Test Overview : TestingTestAssignment</value>
      <webElementGuid>3edbda2a-3b7e-4e92-8d01-97c819fd4eaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>webkitallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>b5472ac6-19ac-4ddf-81cd-6c566082b319</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mozallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3af91cd7-6315-484f-b08b-e89f55089a11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e2e90af7-f0c8-492d-aab3-aea6b5b06d8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;activityFrame&quot;)</value>
      <webElementGuid>fa2a3ee0-56ae-42df-869b-5fa2407d804f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Student/TakeTest/Page_TestingTestAssignment/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40_1_2</value>
      <webElementGuid>8bb4805c-361f-43e7-b207-ef492d05b4f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='activityFrame']</value>
      <webElementGuid>50f63a7f-94b8-43d2-8604-9f7703b2e8db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assignmentViewer']/div/div[3]/table/tbody/tr/td/div/iframe</value>
      <webElementGuid>61a54a87-903c-4603-bc20-62d114fb48e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>aebaedce-67fd-4408-a602-ef5273db9d0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@id = 'activityFrame' and @src = 'https://xlitemppe.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true' and @title = 'Test Overview : TestingTestAssignment']</value>
      <webElementGuid>395874ab-a3bb-41aa-a9d7-dfeb35449ffe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
