<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_55</name>
   <tag></tag>
   <elementGuidId>4d154130-1517-44c3-b894-8a0001f56fb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='row241553503']/td[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>cef57173-c26d-4dae-ae8d-4d070dd22599</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nowrap</value>
      <webElementGuid>728b53f8-118b-45ea-be64-5cd00683e79f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>5/5</value>
      <webElementGuid>d5de052e-0364-4268-a425-1b3131aa7b18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row241553503&quot;)/td[@class=&quot;nowrap&quot;]</value>
      <webElementGuid>aa598b8f-43c8-4e85-8c60-fcd87296b26a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row241553503']/td[2]</value>
      <webElementGuid>16fdc7be-7c8e-4503-9649-de79c616e173</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingTestAssignment'])[1]/following::td[1]</value>
      <webElementGuid>1536cc30-81b5-4db0-90fe-7dfc838d0342</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='&lt;1min'])[3]/preceding::td[2]</value>
      <webElementGuid>40dc7e18-e06a-4c72-8c7d-a95a7c39e8f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='5/5']/parent::*</value>
      <webElementGuid>5dffeeb2-3a19-4ca1-976d-8d3da06092d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div/table/tbody/tr/td[2]</value>
      <webElementGuid>8ae9aa7b-1613-49fe-a831-c715f215c3a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '5/5' or . = '5/5')]</value>
      <webElementGuid>2bcca98b-fb95-418f-9ce1-98454eb0cb8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
