<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Homework and Tests</name>
   <tag></tag>
   <elementGuidId>1627daf2-14fd-42d2-afa4-73b086b7ba72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='storeNavData(&quot;10&quot;)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>c7899f38-7b6f-4f51-bc73-879d86311a3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1</value>
      <webElementGuid>ab8053ab-9032-492d-892d-b9a0e0f1bd8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>99f363bd-caa7-4821-8e75-6d145ff2bf34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>e84eaf39-89b0-44e7-9e53-6ca5aee641d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;10&quot;)</value>
      <webElementGuid>fa3a96f3-72c5-4dd6-804e-e693abab9c9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>033dd648-0ea8-4a84-b031-7aa3fb13df38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>85c598d7-de98-4043-a086-feb5903e20cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>25950c82-725c-40da-8c13-81d80b565b3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4abc0237-d65d-4f3c-b7c1-e49d4e6901b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1</value>
      <webElementGuid>a9382dca-44e0-4074-a017-d4c744f3fd64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>90d28d3d-9ddf-414b-b654-a751fc247ba5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>ee58002d-c258-4a2f-8a48-f8cd07a4bb37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;10&quot;)</value>
      <webElementGuid>2e54b646-b5de-466b-9e20-bc73196044d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>d7a3eccc-12ec-4e59-89f5-12d95fa4b672</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>7f33c68f-b417-4c17-b266-b1b15fba39cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>6b8b965a-2efe-4ee4-8102-83b406d1562e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;10&quot;)']</value>
      <webElementGuid>1b804d56-71f8-431a-ba85-cea246165eb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[4]/a</value>
      <webElementGuid>719511d4-4608-48a6-a39a-276eb7a2e880</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Homework and Tests')]</value>
      <webElementGuid>d28b96f7-5268-4931-823e-12c81d956cb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Calendar'])[1]/following::a[1]</value>
      <webElementGuid>942930d1-fc0f-451e-9283-846eec6f2fb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course Home'])[1]/following::a[2]</value>
      <webElementGuid>ee31fe5c-42ff-49d8-8a2a-b006c7226486</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Results'])[1]/preceding::a[1]</value>
      <webElementGuid>9d22a390-7d2c-4e25-9947-0fc615ac8be9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[1]/preceding::a[2]</value>
      <webElementGuid>43be27bb-3e17-4da0-84e0-5c55d2d1dbac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Homework and Tests']/parent::*</value>
      <webElementGuid>b835edfb-bca0-4548-b694-ec25a2303e44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1')]</value>
      <webElementGuid>b6e0cc68-b972-4a66-9aea-69cdb21269cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a</value>
      <webElementGuid>b6036dc4-85cc-4b34-8f64-6645bcb8df07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1' and @title = 'Homework and Tests' and (text() = 'Homework and Tests' or . = 'Homework and Tests')]</value>
      <webElementGuid>a80081eb-fdbc-48b9-9a27-f9fc93085471</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
