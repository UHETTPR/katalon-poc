<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Go to Results</name>
   <tag></tag>
   <elementGuidId>0dd7a7c7-ce21-4536-ac16-da6543a2e913</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#BtnResults</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='BtnResults']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>33a4b565-bbbf-41aa-bceb-4c1f4058e9a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goResults();</value>
      <webElementGuid>b024a94c-d9ac-480b-a192-93e203e742de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BtnResults</value>
      <webElementGuid>ae5bb705-0120-4c0b-9e4f-e3859d33635c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>bfbcbebc-defe-4aec-833d-1a6953b458af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Go to Results
</value>
      <webElementGuid>9769f656-49b4-437e-b47a-25ebaf7e6614</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BtnResults&quot;)</value>
      <webElementGuid>9c45bb36-3363-4477-9423-9c5dbb1571bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bceee62b-1acf-4ff2-8af4-3b3e68123437</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goResults();</value>
      <webElementGuid>c2c6d6e1-731e-4fa5-9466-bf4a11c37dbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BtnResults</value>
      <webElementGuid>711453de-af5b-49c5-a1b9-d7fca0af97fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default </value>
      <webElementGuid>4db182b4-7661-4d68-9200-f7d0da70ecbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Go to Results
</value>
      <webElementGuid>a08cd5bd-8bef-4adf-b5eb-6098b828016f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BtnResults&quot;)</value>
      <webElementGuid>2734cacb-1057-4362-9f1b-dffcc2b4ddbf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='BtnResults']</value>
      <webElementGuid>1b461729-168c-4aff-b621-67f0b1586b00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//p[@id='ctl00_ctl00_InsideForm_MasterContent_buttonBarSummary']/a[2]</value>
      <webElementGuid>02b418da-f5e6-4fae-9cb1-7cf78a2d530f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Go to Results')]</value>
      <webElementGuid>b7961600-9004-4c2e-bab8-4c76ce1d4ba3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Review Test'])[1]/following::a[1]</value>
      <webElementGuid>c07851a3-550f-45c4-baa9-6da90a77982c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/preceding::a[1]</value>
      <webElementGuid>19f1eea6-f378-487c-8c54-66888aa7afda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::a[2]</value>
      <webElementGuid>ab3a37af-6174-4eec-8dc4-0a87642de21f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Go to Results']/parent::*</value>
      <webElementGuid>5b555d16-57dd-40e8-ad2f-539e408955af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:goResults();')]</value>
      <webElementGuid>3df2e704-650d-4f8b-92ec-87fef790ef76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>eb73179f-0e43-4e26-a849-fd8ec7754683</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:goResults();' and @id = 'BtnResults' and (text() = 'Go to Results
' or . = 'Go to Results
')]</value>
      <webElementGuid>3c26fbe4-a982-4939-8b16-56f814e30d38</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
