<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Correct 4</name>
   <tag></tag>
   <elementGuidId>1e92c82f-ad67-4fb2-a056-698982431c24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='exPanel']/section/ul/li[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>0538871f-46d3-4e8f-994b-4f0aaa32e39d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Correct: 4</value>
      <webElementGuid>af4cae49-728f-46bb-a8a2-6e83d4f7ec2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exPanel&quot;)/section[@class=&quot;table-score-details overviewtotalstest&quot;]/ul[1]/li[2]</value>
      <webElementGuid>04e4728c-b099-4dd4-9338-449f1dc3136f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='exPanel']/section/ul/li[2]</value>
      <webElementGuid>d0878476-568d-49e2-b4bc-70e099d4b143</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Questions: 5'])[1]/following::li[1]</value>
      <webElementGuid>ffa233cb-7f72-430c-b7b4-25812a88b2f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='will'])[1]/following::li[2]</value>
      <webElementGuid>c2a255b7-14d5-48ec-87af-7663a5b841ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partial Credit: 0'])[1]/preceding::li[1]</value>
      <webElementGuid>5ee5d493-35cd-4b9f-8bed-a2ab31e51e3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Incorrect: 0'])[1]/preceding::li[2]</value>
      <webElementGuid>00116a65-3162-42f2-8454-13a172c2c567</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Correct: 4']/parent::*</value>
      <webElementGuid>e1a0d4fc-dca7-448c-b370-555af98f07a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[2]</value>
      <webElementGuid>35d7bf48-08a8-4f33-82f2-76aa10995ae9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Correct: 4' or . = 'Correct: 4')]</value>
      <webElementGuid>5c7948e1-e929-4ea0-b3eb-aa5825ae927c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
