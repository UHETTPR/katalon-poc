<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Homework_ctl00ctl00InsideFormMasterCo_96c347</name>
   <tag></tag>
   <elementGuidId>80620c37-351c-4a56-9a91-c140738cf680</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_gridAssignments_ctl11_cbHide</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments_ctl11_cbHide']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>473abd3c-6f7c-45d2-bef4-d255eecd43d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_gridAssignments_ctl11_cbHide</value>
      <webElementGuid>d8ffd4e7-6e97-4cfc-a531-c3c32c0440d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>4399f02f-1c6c-441d-861b-092b45a1d699</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$gridAssignments$ctl11$cbHide</value>
      <webElementGuid>e5000deb-b5d5-4a7a-b066-4de4a313bb36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>istrackchanges</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>56b69483-b57b-4aa1-8047-87b2992f5f9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments_ctl11_cbHide&quot;)</value>
      <webElementGuid>13146d83-5dbe-4b82-874b-9da1b03b46c5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments_ctl11_cbHide']</value>
      <webElementGuid>a7362fec-902d-47df-98b8-bfeefc0deee2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[11]/td/input</value>
      <webElementGuid>45647f4b-7a94-4001-9bb4-058b73c2d7d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[11]/td/input</value>
      <webElementGuid>19d1923c-46db-495a-8dbb-229d203bc09d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ctl00_ctl00_InsideForm_MasterContent_gridAssignments_ctl11_cbHide' and @type = 'checkbox' and @name = 'ctl00$ctl00$InsideForm$MasterContent$gridAssignments$ctl11$cbHide']</value>
      <webElementGuid>ab3dc4e0-eb28-4e3f-a829-e4d4778d16a1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
