<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Delete Results</name>
   <tag></tag>
   <elementGuidId>8ec8b997-cb7b-4756-a7e5-de19d2961e26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[5]/div/div[2]/ul/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>530a1900-2ca6-468d-b982-ec11a1f9d0dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:MoreToolsClick('Delete Results')</value>
      <webElementGuid>c2edd2e5-416a-445f-99ea-13caf0a59998</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete Results</value>
      <webElementGuid>895f7369-bad0-40ab-923f-2a283d6247cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_toolbar1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[3]/a[1]</value>
      <webElementGuid>ab95cbd7-201a-436d-92d0-cdd2487eff56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[5]/div/div[2]/ul/li[3]/a</value>
      <webElementGuid>f3e5611d-986b-4c4e-830d-28e5c731c480</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Delete Results')]</value>
      <webElementGuid>fc4bad87-49d3-446a-9d3c-633e7321e9aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear Study Plan'])[1]/following::a[1]</value>
      <webElementGuid>b245af9c-95ca-418a-88f6-3386e7b013a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add/Edit Student IDs'])[1]/following::a[2]</value>
      <webElementGuid>34ca874e-0a76-4ec5-8784-8a862f75681f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Lowest Scores'])[1]/preceding::a[1]</value>
      <webElementGuid>65566fbf-ee4f-4a8e-b944-a2fe44e94f46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit Roster'])[1]/preceding::a[2]</value>
      <webElementGuid>06a2e52f-c497-40dd-b612-a1ec7857e427</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Delete Results']/parent::*</value>
      <webElementGuid>1ba4c7b6-7e28-405d-961d-3834f4839a7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:MoreToolsClick('Delete Results')&quot;)]</value>
      <webElementGuid>6ac12ff9-dbf8-4eab-9068-9a140139415c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[3]/a</value>
      <webElementGuid>205f071b-5dff-4763-9719-dd5b88caaea7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:MoreToolsClick(&quot; , &quot;'&quot; , &quot;Delete Results&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Delete Results' or . = 'Delete Results')]</value>
      <webElementGuid>ab0dbeda-b539-4e02-8a3d-c9b275f909f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
