<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign in</name>
   <tag></tag>
   <elementGuidId>548fa1fe-4e18-4c4d-bdfa-b8f2d1622650</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.button.button-big-icon.bg-color-match-medium</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7c137a1e-7beb-4f5a-ad45-f9275703ed1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')</value>
      <webElementGuid>01dcf2dc-3da8-4ff2-b87d-c471659b346a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://www.mathxl.com/login_mxl.htm</value>
      <webElementGuid>ae2c52cb-1e59-4c5d-ad81-b5006c6d757a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium</value>
      <webElementGuid>81c70a90-638f-4269-becf-6a9770304c50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>bf3fe8d7-d926-45aa-b0c9-c0f7837a0fce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths cssfilters&quot;]/body[@class=&quot;has-announcement green has-two-or-less-featured-link-categories&quot;]/section[@class=&quot;hero hero--bg-img uses-mask mask-is--gradient-top random-image js--random-bg&quot;]/div[@class=&quot;wrapper no-col-stack @480.col-stack&quot;]/div[@class=&quot;col col-5&quot;]/div[@class=&quot;sign-in&quot;]/p[2]/a[@class=&quot;button button-big-icon bg-color-match-medium&quot;]</value>
      <webElementGuid>dacbede5-6838-4d03-bd12-bd37ebb15c12</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;return showCompatibilityMsg('http://www.mathxl.com/login_mxl.htm')&quot;]</value>
      <webElementGuid>e1fa4628-6c56-41cb-98d5-a369b3fd57a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign in')]</value>
      <webElementGuid>052c77ba-ee28-4c3a-ac2a-493671c8302e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/following::a[1]</value>
      <webElementGuid>1c0badef-3ac0-4eb6-a189-d6d7c1375b01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn more'])[1]/following::a[1]</value>
      <webElementGuid>762d8e48-9768-46a0-9445-f4eb830f57bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot username or password?'])[1]/preceding::a[1]</value>
      <webElementGuid>4b60e43d-0ff2-4e3e-bae2-5e02535d33ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MyLab Math/MyLab Statistics users, sign in here'])[1]/preceding::a[2]</value>
      <webElementGuid>3aae1c9c-d97b-40e9-bb55-bbd45c5e0ec9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>cb1101e3-b780-4b61-83c4-0697e0aca996</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://www.mathxl.com/login_mxl.htm']</value>
      <webElementGuid>45e4e03f-59b7-4d3d-a22a-40fe144c0078</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/p[2]/a</value>
      <webElementGuid>f18d4ade-6f4e-4e5b-8ddb-4de7f58b53f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://www.mathxl.com/login_mxl.htm' and (text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>871b893d-d932-4dcd-80cb-4c0aa3072a8b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
