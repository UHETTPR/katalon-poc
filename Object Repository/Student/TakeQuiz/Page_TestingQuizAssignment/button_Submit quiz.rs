<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit quiz</name>
   <tag></tag>
   <elementGuidId>0e43d7df-2a65-4218-a324-be80a0d7d53f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='xl_dijit-bootstrap_Button_3']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5130e219-6831-4074-96c5-438d4919e5af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>btnClose</value>
      <webElementGuid>5a1368ac-cce3-4e2e-a1a1-2c83a6015a15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btnClose</value>
      <webElementGuid>8358eb23-d8db-448a-b5ee-a6b0ef39c92d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-type</name>
      <type>Main</type>
      <value>xl.dijit-bootstrap.Button</value>
      <webElementGuid>ae4da951-5de2-4f78-bfa1-3965db228233</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dtabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>d8125c5e-0788-4ee2-b68d-63cd1d1dc999</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>520df829-fea4-49f9-a84c-daea7d0cb411</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>def060fc-f7f1-4684-92f1-7bb33121e90d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_3</value>
      <webElementGuid>140e36c4-4a77-41bb-a30e-e1b52b16bb5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_3</value>
      <webElementGuid>e0ff3dbd-0a69-4a2a-9ca7-d77d7047d3f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>1dac88c3-f194-447d-a92a-372cd153e124</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Submit quiz</value>
      <webElementGuid>ed86bb8c-a49c-4ef4-9367-cdeb1d9c79cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit quiz</value>
      <webElementGuid>47da39f9-d060-485f-95d9-54e9d8fe8053</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_3&quot;)</value>
      <webElementGuid>dde1abe8-62cf-49a0-bb16-6e8db0afc217</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Student/TakeQuiz/Page_TestingQuizAssignment/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>48c1f67b-c148-45be-a2bd-beca8163daa1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_3']</value>
      <webElementGuid>7657a434-ccb8-49a7-94d6-69a4725fff70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_0']/div/div[3]/div[3]/div[2]/button</value>
      <webElementGuid>c42fd2f0-58b5-46fe-9842-0a3e140ab8c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[4]/following::button[1]</value>
      <webElementGuid>4dcdf7ee-f32e-4486-b9e4-2f1730878ad1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help center'])[1]/following::button[1]</value>
      <webElementGuid>9d660175-eae0-4d5c-8afb-e31e3a1afda7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/preceding::button[1]</value>
      <webElementGuid>a2121fb3-1fab-48ab-9b4b-119970bf22d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=','])[1]/preceding::button[2]</value>
      <webElementGuid>3860cbe3-59f7-4900-800e-95d55b03b0d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Submit quiz']/parent::*</value>
      <webElementGuid>c7419c55-4797-4465-bae1-d17c02dd3fb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>fc1a1d2f-c870-44d4-ba44-8dbeee1ea43f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_3' and (text() = 'Submit quiz' or . = 'Submit quiz')]</value>
      <webElementGuid>ed7dbcc7-4100-4015-9b94-0fde222dce7c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
