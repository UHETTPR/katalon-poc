<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Return to Homework_activityFrame (2)</name>
   <tag></tag>
   <elementGuidId>949d028f-1be8-445b-a988-4cedf46da632</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#activityFrame</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='activityFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>ad09f9c9-2936-49ea-b771-4c81f7a92e17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>88b86d81-e388-48ed-b59d-4eb67c4f3b61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>d17d21e5-194f-4542-8d6b-e12cb4f648e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://xlitemppe.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true</value>
      <webElementGuid>a03d8ed8-6192-4ab7-9366-fcd459f069bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>e3025c70-290a-42f2-89b2-c9588b55ea93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Test Overview : TestingQuizAssignment</value>
      <webElementGuid>58cb5167-654e-40cd-9149-f1bfc1f0fddf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>webkitallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>1954a2c3-c6a7-429a-ac74-6549c8f95102</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mozallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>eb6ff8bc-abe5-4cd1-987e-ef76e8582d14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3c439517-b50e-4784-8c78-951d774fd335</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;activityFrame&quot;)</value>
      <webElementGuid>92f75d99-dc24-4c3a-851f-83afe76d4b0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Student/TakeQuiz/Page_TestingQuizAssignment/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40_1_2</value>
      <webElementGuid>71ad1c9e-3d9d-41cb-bad9-1a2b7b53128d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='activityFrame']</value>
      <webElementGuid>bdf4f8a0-46b1-4f6e-85f8-dd12e0851bd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assignmentViewer']/div/div[3]/table/tbody/tr/td/div/iframe</value>
      <webElementGuid>38a1dfce-ad7c-43a7-992b-29d6050064b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>a6dbb56a-c6a3-49e3-be11-2d3553255951</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@id = 'activityFrame' and @src = 'https://xlitemppe.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true' and @title = 'Test Overview : TestingQuizAssignment']</value>
      <webElementGuid>0398a795-fd5b-4ca3-a2bd-692b709933d5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
