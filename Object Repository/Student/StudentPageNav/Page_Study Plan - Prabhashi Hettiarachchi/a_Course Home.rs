<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Course Home</name>
   <tag></tag>
   <elementGuidId>fda2fa1e-9a5c-4b4d-b551-5d0b05a1cc59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='storeNavData(&quot;2&quot;)']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.menu-item > a.navA</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9d987f2f-8837-4695-968a-ebeba3d20414</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Student/MyDashboard.aspx</value>
      <webElementGuid>4ceea0a9-0dd1-4f04-a7db-2f2d31043703</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>27bcce4b-0c75-4725-9c96-5675459dffc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>8b123c87-8178-4931-9ba9-98d51a6fb785</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;2&quot;)</value>
      <webElementGuid>68105293-4c05-4306-984a-63475ab0fdd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Course Home</value>
      <webElementGuid>5c85a236-641a-407b-a297-029edea5149f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Course Home</value>
      <webElementGuid>9584405e-2fcf-49c1-8ac0-5e1a6b1d197f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>d58515f2-5ef1-4e84-abda-c17f73840400</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;2&quot;)']</value>
      <webElementGuid>c74c7e1c-a0e5-4015-9536-5d90ea6e3edb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[2]/a</value>
      <webElementGuid>d66aa4c6-b6ef-4cae-81cd-9ba103eff5da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Course Home')]</value>
      <webElementGuid>f871b497-cc0f-4bd1-99aa-bd2840661854</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='STUDENT'])[1]/following::a[1]</value>
      <webElementGuid>5001c8f5-041a-46c8-9150-bae6520abf2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[2]/following::a[1]</value>
      <webElementGuid>0d22da07-500a-40d7-a3d3-61561be1680c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Calendar'])[1]/preceding::a[1]</value>
      <webElementGuid>8fcf9c32-8ad0-426e-9dce-0e6a1849ee37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework and Tests'])[1]/preceding::a[2]</value>
      <webElementGuid>76ce9ed7-3672-488e-94db-7f7cb7d24597</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Course Home']/parent::*</value>
      <webElementGuid>1ee507d0-dfb8-4000-8530-2eb6cbcb7c74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/Student/MyDashboard.aspx')])[2]</value>
      <webElementGuid>5b1b0f8d-5a2f-4c3a-8e07-e47c29268177</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/a</value>
      <webElementGuid>591eb202-ed3e-4290-8df8-593e9bed626a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Student/MyDashboard.aspx' and @title = 'Course Home' and (text() = 'Course Home' or . = 'Course Home')]</value>
      <webElementGuid>f26492dc-ad3f-4b94-8da3-5deb5fdff798</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
