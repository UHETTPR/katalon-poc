<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Homework and Tests</name>
   <tag></tag>
   <elementGuidId>cf0ed83d-c2ba-4033-8424-c963fca49712</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='storeNavData(&quot;10&quot;)']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b5351681-8a38-4f8b-a5df-44fab0ed3f22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1</value>
      <webElementGuid>43d1d2db-5c4f-4efd-844e-9741c4552ebc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>94082858-68f9-4705-b0e4-9a007e3d01b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>fddabbce-f852-4150-ab4f-1855decef0e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;10&quot;)</value>
      <webElementGuid>5d0abe64-af51-4f3c-b470-33290d596cdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>755b81b7-1f32-40b5-b740-69367a1b379a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>f3d750e6-e60d-4b84-9396-45da5b26ee25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>f1b93ea3-d31f-41ff-8be2-9e6114dada70</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;10&quot;)']</value>
      <webElementGuid>6c4f59e9-928f-4520-bff2-e5e93c5d09c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[4]/a</value>
      <webElementGuid>18cc6eff-bce6-4c26-9d76-348b4d445e9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Homework and Tests')]</value>
      <webElementGuid>c057563a-67f9-4c91-8bb3-25c78bf551ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Calendar'])[1]/following::a[1]</value>
      <webElementGuid>73a69863-7447-446a-98d6-c03fc8f03c05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course Home'])[1]/following::a[2]</value>
      <webElementGuid>fe28c6ef-012d-44e6-98dc-2e4b6029ac9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Results'])[1]/preceding::a[1]</value>
      <webElementGuid>e7db9b55-2ef8-4fa8-9f0a-237465b0c568</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[1]/preceding::a[2]</value>
      <webElementGuid>0252e8c2-0a40-4ff1-9d05-f65dae033ba7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Homework and Tests']/parent::*</value>
      <webElementGuid>8141fddf-111f-4ed8-809c-2ba1404ce19c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1')]</value>
      <webElementGuid>3c978d25-df35-42cb-8a38-a6c1127b3181</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a</value>
      <webElementGuid>8c3b5ecb-80f2-4585-9e96-54289e36fed9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1' and @title = 'Homework and Tests' and (text() = 'Homework and Tests' or . = 'Homework and Tests')]</value>
      <webElementGuid>540fe40e-2dc4-4bef-992b-1b5d6b725a1a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
