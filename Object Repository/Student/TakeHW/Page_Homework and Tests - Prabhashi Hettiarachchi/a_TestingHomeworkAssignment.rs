<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_TestingHomeworkAssignment</name>
   <tag></tag>
   <elementGuidId>f33f9cd7-22ca-40b6-b17b-6c99e134703f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'TestingHomeworkAssignment')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9be84d81-a329-4574-91c5-2f30e8ff33e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doHomework(624552696, false, true);</value>
      <webElementGuid>fd1c3204-89c9-466d-9aee-55dc746e5af0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>TestingHomeworkAssignment</value>
      <webElementGuid>76cfcc49-c1e1-441f-a0e5-ab82df26c4f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[9]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>3ec6c582-e9a0-4c50-aaf3-bd1cfbfe58b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[9]/th/a</value>
      <webElementGuid>eedbee40-3058-454c-a8d4-189163b941ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'TestingHomeworkAssignment')]</value>
      <webElementGuid>5f8fb940-24ae-4237-849e-b4930a4c9ebc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[9]/following::a[1]</value>
      <webElementGuid>69ef6091-c86d-413c-907d-e40fa7e2478d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preassigned Homework'])[1]/following::a[1]</value>
      <webElementGuid>38bfb531-50e3-4941-b7ce-95159f516391</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quiz'])[1]/preceding::a[1]</value>
      <webElementGuid>16c8c2d9-64d9-44e3-a30c-4c59fee731eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TestingQuizAssignment'])[1]/preceding::a[1]</value>
      <webElementGuid>84515054-04db-4733-8f5b-738c41a4dd42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='TestingHomeworkAssignment']/parent::*</value>
      <webElementGuid>4e697656-49cc-4c49-8ebe-fc1b94f8274b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doHomework(624552696, false, true);')]</value>
      <webElementGuid>f6cd426b-f705-4779-92dd-4dd2ee5e1e9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[9]/th/a</value>
      <webElementGuid>c08d677d-c591-4114-81c0-8a7327ccf147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doHomework(624552696, false, true);' and (text() = 'TestingHomeworkAssignment' or . = 'TestingHomeworkAssignment')]</value>
      <webElementGuid>6c24187f-0461-4181-94bc-092feefdf2b9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
