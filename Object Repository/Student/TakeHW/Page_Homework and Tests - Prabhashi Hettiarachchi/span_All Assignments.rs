<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_All Assignments</name>
   <tag></tag>
   <elementGuidId>38278e60-09ce-4c0d-a11b-6804dc24c248</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='gridtitle']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.PagerText</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>be1080a5-a654-4536-ae02-fcad5f10aba0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>PagerText</value>
      <webElementGuid>92ad8143-9e62-4e24-ae65-b51823338ca1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					            All Assignments
					        </value>
      <webElementGuid>ae40f8eb-f753-44ee-abd9-c016e44ee8a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gridtitle&quot;)/span[@class=&quot;PagerText&quot;]</value>
      <webElementGuid>608657f0-8d52-4d75-b824-e4f5ef86be0c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//h3[@id='gridtitle']/span</value>
      <webElementGuid>e6ed1cac-15fb-4512-b547-d686ee0c1137</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[2]/following::span[1]</value>
      <webElementGuid>f67432cb-a978-47e1-830e-8ca46d769fac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All SBC'])[1]/following::span[2]</value>
      <webElementGuid>abd80fff-b325-4350-825a-51d53192a17a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Due'])[1]/preceding::span[1]</value>
      <webElementGuid>e62c894a-ea0e-4595-a19c-dacf5e78ce2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prereq'])[1]/preceding::span[1]</value>
      <webElementGuid>cab4ad27-5e9b-40b0-84d5-1de102d87113</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3/span</value>
      <webElementGuid>438eade3-3fea-439b-b979-45ce07a79f40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
					            All Assignments
					        ' or . = '
					            All Assignments
					        ')]</value>
      <webElementGuid>d38120e7-a6b7-4f58-bb86-99927230c3a4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
